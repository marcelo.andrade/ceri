@extends('layout.inicio')

@section('content')
    <figure><img src="{{asset('images/logo06.png')}}" alt="seri" class="center-block"/></figure>
    @if($status=='SUCESSO')
        <h2 class="text-center">Cadastro confirmado com sucesso!</h2>
    @elseif($status=='ERRO')
        <h2 class="text-center">Erro!</h2>
    @endif
    <div class="wrapper row">
        <div class="col-md-3 hidden-xs hidden-sm"></div>
        <div id="register" class="box-start col-md-6 col-sm-12 col-xs-12">
            <div class="default-register clearfix">
            	@if($status=='SUCESSO')
                    <div class="alert single alert-success">
                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                        <div class="menssagem">
                            Olá <b>{{$usuario->no_usuario}}</b><br /><br />
                            Seu cadastro foi confirmado com sucesso, agora você pode acessar o sistema normalmente.<br /><br />
                            <a href="{{url('/')}}" class="btn btn-black">Acessar o sistema</a>
                        </div>
                    </div>
            	@elseif($status=='ERRO')
                    <div class="alert single alert-warning">
                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                        <div class="menssagem">
                            O código de confirmação não foi encontrado.
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-3 hidden-xs hidden-sm"></div>
    </div>
@endsection
