@extends('layout.inicio')

@section('scripts')
@endsection

@section('content')
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
            <div class="box-start col-md-4 col-sm-6">
                <div class="default-login">
                    <h2>Assinar arquivo</h2>
                    <p>Selecione um arquivo abaixo.</p>
                    <form name="form-assinar-arquivo" action="assinar-arquivos" method="POST" enctype="multipart/form-data">
	                    {{csrf_field()}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                                <div class="menssagem">
		                            @foreach ($errors->all() as $error)
		    	                        <?php
		    	                        switch ($error) {
											case '7001':
		    	                        		echo 'Erro ao enviar o arquivo';
		    	                        		break;
		    	                        	default:
		    	                        		echo $error;
		    	                        		break;
		    	                        }
		    	                        ?>
		    	                        <br />
        		                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Selecione um arquivo</label>
                            <div class="arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecionar o arquivo</span><span class="fileinput-exists">Alterar</span><input type="file" name="arquivo"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                            </div>
                        </div>
                        <br />
 						<input type="submit" class="assinar btn btn-primary btn-block" value="Assinar o arquivo" />
                    </form>
				</div>
            </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
@endsection