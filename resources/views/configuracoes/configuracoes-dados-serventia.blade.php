@extends('configuracoes.configuracoes')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.dadosserventia.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
    @if( $usuario->in_usuario_master == 'S' && $usuario->id_tipo_usuario == 2 )
        @php
            $nu_cep = '';
            $no_endereco = '';
            $nu_endereco = '';
            $no_bairro = '';
            $no_complemento = '';
            $id_estado = '';
            $id_cidade = '';
            $id_tipo_telefone_serventia = 0;
            $nu_ddd_serventia = '';
            $nu_telefone_serventia = '';

            if (count($usuario->pessoa->enderecos)>0) {
                $nu_cep = $usuario->pessoa->enderecos[0]->nu_cep;
                $no_endereco = $usuario->pessoa->enderecos[0]->no_endereco;
                $nu_endereco = $usuario->pessoa->enderecos[0]->nu_endereco;
                $no_bairro = $usuario->pessoa->enderecos[0]->no_bairro;
                $no_complemento = $usuario->pessoa->enderecos[0]->no_complemento;
                $id_estado = $usuario->pessoa->enderecos[0]->cidade->id_estado;
                $id_cidade = $usuario->pessoa->enderecos[0]->id_cidade;
                $no_cidade = $usuario->pessoa->enderecos[0]->cidade->no_cidade;
            }

            if (count($usuario->usuario_serventia->serventia->pessoa->telefones)>0) {
                $id_tipo_telefone_serventia = $usuario->usuario_serventia->serventia->pessoa->telefones[0]->id_tipo_telefone;
                $nu_ddd_serventia = $usuario->usuario_serventia->serventia->pessoa->telefones[0]->nu_ddd;
                $nu_telefone_serventia = $usuario->usuario_serventia->serventia->pessoa->telefones[0]->nu_telefone;
            }
        @endphp
        <form name="form-dados-serventia" action="" class="clearfix">
            <input type="hidden" name="tp_pessoa" value="{{$usuario->pessoa->tp_pessoa}}">
            <div class="erros alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Dados da serventia</legend>
                    <div class="col-md-12">
                        <div class="form-group row clearfix">
                            <div class="col-md-4">
                                <label>Tipo de telefone</label>
                                <select name="id_tipo_telefone_serventia" class="form-control">
                                    <option value="0">Tipo de telefone</option>
                                    <option value="1" @if($id_tipo_telefone_serventia==1) selected @endif>Residencial
                                    </option>
                                    <option value="2" @if($id_tipo_telefone_serventia==2) selected @endif>Comercial
                                    </option>
                                    <option value="3" @if($id_tipo_telefone_serventia==3) selected @endif>Celular
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>DDD</label>
                                <input type="text" name="nu_ddd_serventia" class="form-control" data-mask="99"
                                       value="{{$nu_ddd_serventia}}"/>
                            </div>
                            <div class="col-md-5">
                                <label>Número do telefone</label>
                                <input type="text" name="nu_telefone_serventia" class="form-control"
                                       data-mask="9999-9999?9" value="{{$nu_telefone_serventia}}"/>
                            </div>
                            <div class="col-md-6">
                                <label>E-mail</label>
                                <input type="email" name="email_pessoa_serventia" class="form-control"
                                       value="{{ $usuario->usuario_serventia->serventia->pessoa->no_email_pessoa }}"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Nome da serventia</label>
                                <input name="no_serventia" class="form-control" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->no_serventia }}">
                            </div>

                            <div class="col-md-6">
                                <label>Tipo de serventia</label>
                                <select name="id_tipo_serventia" class="form-control" disabled>
                                    <option value="0">{{$usuario->usuario_serventia->serventia->tipo_serventia->no_tipo_serventia}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Hora de início expediente</label>
                                <input name="hora_inicio_expediente" class="form-control" data-mask="99:99" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->hora_inicio_expediente }}">
                            </div>
                            <div class="col-md-6">
                                <label>Hora de término expediente</label>
                                <input name="hora_termino_expediente" class="form-control" data-mask="99:99" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->hora_termino_expediente }}">
                            </div>
                        </div>

                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Hora de início do almoço</label>
                                <input name="hora_inicio_almoco" class="form-control" data-mask="99:99" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->hora_inicio_almoco }}">
                            </div>
                            <div class="col-md-6">
                                <label>Hora de término do almoço</label>
                                <input name="hora_termino_almoco" class="form-control" data-mask="99:99" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->hora_termino_almoco }}">
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Nome oficial</label>
                                <input name="no_oficial" class="form-control" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->no_oficial }}">
                            </div>
                            <div class="col-md-6">
                                <label>Nome substituto</label>
                                <input name="no_substituto" class="form-control" type="text"
                                       value="{{ $usuario->usuario_serventia->serventia->no_substituto }}">
                            </div>
                        </div>
                        <input type="hidden" name="id_serventia" id="id_serventia"
                               value="{{$usuario->usuario_serventia->serventia->id_serventia}}">
                    </div>
                    <div class="'col-md-12">
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Código CNS</label>
                                <input type="text" name="codigo_cns" id="codigo_cns" class="form-control" value="{{ $usuario->usuario_serventia->serventia->serventia_cliente->codigo_cns }}" />
                            </div>
                            <div class="col-md-6">
                                <label>DV CNS</label>
                                <input type="text" name="dv_codigo_cns" id="dv_codigo_cns" class="form-control" value="{{ $usuario->usuario_serventia->serventia->serventia_cliente->dv_codigo_cns }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row clearfix">
                            <div class="col-md-8">
                                <label>Endereço</label>
                                <input type="text" name="no_endereco_serventia" class="form-control"
                                       value="{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->no_endereco}}"/>
                            </div>
                            <div class="col-md-2">
                                <label>Número</label>
                                <input type="text" name="nu_endereco_serventia" class="form-control"
                                       value="{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->nu_endereco}}"/>
                            </div>
                            <div class="form-group col-md-2">
                                <label>CEP</label>
                                <input type="text" name="nu_cep_serventia" class="form-control" data-mask="99999-999"
                                       value="{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->nu_cep}}"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Bairro</label>
                                <input type="text" name="no_bairro_serventia" class="form-control"
                                       value="{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->no_bairro}}"/>
                            </div>
                            <div class="col-md-6">
                                <label>Complemento</label>
                                <input type="text" name="no_complemento_serventia" class="form-control"
                                       value="{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->no_complemento}}"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label>Estado</label>
                                <select name="id_estado_serventia" class="form-control">
                                    <option value="0">Selecione o estado</option>
                                    @if(count($estados)>0)
                                        @foreach($estados as $estado)
                                            <option value="{{$estado->id_estado}}"
                                                    @if($usuario->usuario_serventia->serventia->pessoa->enderecos[0]->cidade->id_estado==$estado->id_estado) selected @endif>{{$estado->no_estado}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Cidade</label>
                                <select name="id_cidade_serventia" class="form-control" @if($id_cidade=='') disabled @endif>
                                    <option value="0">Selecione uma cidade</option>
                                    @if($id_cidade>0)
                                        <option value="{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->cidade->id_cidade}}" selected>{{$usuario->usuario_serventia->serventia->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id_endereco_serventia" value="{{ $usuario->usuario_serventia->serventia->pessoa->enderecos[0]->id_endereco }}"/>
                    </div>
                </fieldset>
            </div>
            <div class="botoes">
                <input type="reset" class="back btn btn-primary pull-left" value="Cancelar" />
                <input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações" />
            </div>
        </form>
    @endif
@endsection