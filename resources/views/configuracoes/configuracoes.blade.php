@extends('layout.comum')

@section('content')
	<div class="container">
        <div class="configuracoes panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Configurações <span class="small">/ Minha conta</span></h4>
            </div>
            <div class="panel-body no-padding">
                <nav class="col-md-3">
                    <ul class="nav nav-pills nav-stacked">
                    	<li @if(strstr(Route::current()->getPath(),'dados-pessoais')) class="active" @endif><a href="dados-pessoais">Dados pessoais</a></li>
                    	<li @if(strstr(Route::current()->getPath(),'dados-acesso')) class="active" @endif><a href="dados-acesso">Dados de acesso</a></li>
                        @if ($usuario->id_tipo_usuario==2)
                        	<li @if(strstr(Route::current()->getPath(),'dados-serventia')) class="active" @endif><a href="dados-serventia">Dados da serventia</a></li>
                            @if($usuario->in_usuario_master == 'S')
                                <li @if(strstr(Route::current()->getPath(),'dados-bancarios')) class="active" @endif><a href="dados-bancarios">Dados bancários</a></li>
                            @endif
                            <li @if(strstr(Route::current()->getPath(),'certificado')) class="active" @endif><a href="certificado">Certificado digital</a></li>
                        	<li @if(strstr(Route::current()->getPath(),'dados-produtos')) class="active" @endif><a href="dados-produtos">Valor dos serviços</a></li>
                        @endif
                        {{--@if (Auth::User()->id_tipo_usuario==2)
                                    <li @if(strstr(Route::current()->getPath(),'selos')) class="active" @endif><a href="selos">Importar selos</a></li>
                        @endif--}}
                    </ul>
                </nav>
                <div class="content col-md-9">
                    <div class="carregando flutuante text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                	@yield('sub-content')
                </div>
            </div>
        </div>
    </div>
@endsection