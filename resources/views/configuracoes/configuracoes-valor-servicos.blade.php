@extends('configuracoes.configuracoes')

@section('sub-content')
	<form name="form-valor-servicos" method="post" action="" class="clearfix">
        <div class="erros alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados dos produtos</legend>
                <div class="col-md-12">
                    <div class="form-group row clearfix">
                    	<div class="col-md-4">
                        	<label> Valor do AR</label>
                            <input type="text" name="valor-ar" class="form-control real" />
                        </div>
                        <div class="col-md-4">
                        	<label>Valor Averbação na prefeitura</label>
                            <input type="text" name="valor-averbacao" class="form-control real" />
                        </div>
                        <div class="col-md-4">
                        	<label>Valor ISSQN</label>
                            <input type="text" name="valor-issqn" class="form-control real" />
                        </div>
                    </div>
            		<div class="botoes">
            			<input type="reset" class="back btn btn-primary pull-left" value="Cancelar" />
            	    	<input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações" />
            	   	</div>
                </div>
            </fieldset>
        </div>
	</form>
@endsection