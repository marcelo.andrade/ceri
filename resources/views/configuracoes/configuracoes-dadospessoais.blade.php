@extends('configuracoes.configuracoes')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.dadospessoais.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
	<form name="form-dados-pessoais" method="post" action="" class="clearfix">
		<input type="hidden" name="tp_pessoa" value="{{$usuario->pessoa->tp_pessoa}}">
		<div class="erros alert alert-danger" style="display:none">
		    <i class="icon glyphicon glyphicon-remove pull-left"></i>
		    <div class="menssagem"></div>
		</div>
		<div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados pessoais</legend>
                <div class="col-md-12">
                	<?php
				    $id_tipo_telefone = 0;
				    $nu_ddd = '';
				    $nu_telefone = '';
				    if (count($usuario->pessoa->telefones)>0) {
				    	$id_tipo_telefone = $usuario->pessoa->telefones[0]->id_tipo_telefone;
				    	$nu_ddd = $usuario->pessoa->telefones[0]->nu_ddd;
				    	$nu_telefone = $usuario->pessoa->telefones[0]->nu_telefone;
				    }

				    $nu_cep = '';
				    $no_endereco = '';
				    $nu_endereco = '';
				    $no_bairro = '';
				    $no_complemento = '';
				    $id_estado = '';
				    $id_cidade = '';

				    if (count($usuario->pessoa->enderecos)>0) {
				    	$nu_cep = $usuario->pessoa->enderecos[0]->nu_cep;
					    $no_endereco = $usuario->pessoa->enderecos[0]->no_endereco;
					    $nu_endereco = $usuario->pessoa->enderecos[0]->nu_endereco;
					    $no_bairro = $usuario->pessoa->enderecos[0]->no_bairro;
					    $no_complemento = $usuario->pessoa->enderecos[0]->no_complemento;
					    $id_estado = $usuario->pessoa->enderecos[0]->cidade->id_estado;
					    $id_cidade = $usuario->pessoa->enderecos[0]->id_cidade;
					    $no_cidade = $usuario->pessoa->enderecos[0]->cidade->no_cidade;
				    }



                    $id_banco		= '';
					/*dd( $usuario->usuario_serventia->serventia );

				    if (count( $usuario->pessoa->serventia->serventia_cliente)>0)
					{
				    	$codigo_cns 	= $pessoa_ativa->pessoa->serventia->serventia_cliente->codigo_cns;
                        $dv_codigo_cns 	= $pessoa_ativa->pessoa->serventia->serventia_cliente->dv_codigo_cns;
					}else{
                        $codigo_cns 	= '';
                        $dv_codigo_cns 	= '';
					}*/
				    ?>
					@if ($usuario->pessoa->tp_pessoa=='F')
						<div class="pessoa-fisica">
						    <div class="form-group">
						    	<label>Nome completo</label>
						        <input type="text" name="no_pessoa" class="form-control" value="{{$usuario->pessoa->no_pessoa}}" />
						    </div>
						    <div class="form-group row clearfix">
						        <div class="col-md-4">
						        	<label>CPF</label>
						            <input type="text" name="nu_cpf_cnpj" class="form-control" value="{{$usuario->pessoa->nu_cpf_cnpj}}" disabled />
						        </div>
						        <div class="col-md-4">
						        	<label>Gênero</label>
						            <select name="tp_sexo" class="form-control">
						                <option value="0">Selecione o gênero</option>
						                <option value="F" @if($usuario->pessoa->tp_sexo=='F') selected @endif>Feminino</option>
						                <option value="M" @if($usuario->pessoa->tp_sexo=='M') selected @endif>Masculino</option>
						            </select>
						        </div>
						        <div class="col-md-4">
						        	<label>Data de nascimento</label>
						            <input type="text" name="dt_nascimento" class="form-control" data-mask="99/99/9999" value="{{Carbon\Carbon::parse($usuario->pessoa->dt_nascimento)->format('d/m/Y')}}" />
						        </div>
						    </div>
						    <div class="form-group row clearfix">
						        <div class="col-md-4">
						        	<label>Tipo de telefone</label>
						            <select name="id_tipo_telefone" class="form-control">
						                <option value="0">Tipo de telefone</option>
						                <option value="1" @if($id_tipo_telefone==1) selected @endif>Residencial</option>
						                <option value="2" @if($id_tipo_telefone==2) selected @endif>Comercial</option>
						                <option value="3" @if($id_tipo_telefone==3) selected @endif>Celular</option>
						            </select>
						        </div>
						        <div class="col-md-2">
						        	<label>DDD</label>
						            <input type="text" name="nu_ddd" class="form-control" data-mask="99" value="{{$nu_ddd}}" />
						        </div>
						        <div class="col-md-5">
						        	<label>Número do telefone</label>
						            <input type="text" name="nu_telefone" class="form-control" data-mask="9999-9999?9" value="{{$nu_telefone}}" />
						        </div>
						    </div>
						</div>
					@elseif ($usuario->pessoa->tp_pessoa=='J')
						<div class="pessoa-juridica">
						    <div class="form-group row clearfix">
						        <div class="col-md-6">
						        	<label>CNPJ</label>
						            <input type="text" name="nu_cpf_cnpj" class="form-control" value="{{$usuario->pessoa->nu_cpf_cnpj}}" disabled />
						        </div>
						        <div class="col-md-6">
						        	<label>Inscrição Municipal</label>
						            <input type="text" name="no_inscricao_municipal" class="form-control" value="{{$usuario->pessoa->no_inscricao_municipal}}" disabled />
						        </div>
						    </div>
						    <div class="form-group row clearfix">
						        <div class="col-md-6">
						        	<label>Razão social</label>
						            <input type="text" name="no_pessoa" class="form-control" value="{{$usuario->pessoa->no_pessoa}}" />
						        </div>
						        <div class="col-md-6">
						        	<label>Nome fantasia</label>
						            <input type="text" name="no_fantasia" class="form-control" value="{{$usuario->pessoa->no_fantasia}}" />
						        </div>
						    </div>
						    <div class="form-group row clearfix">
						        <div class="col-md-5">
						        	<label>Tipo de telefone</label>
						            <select name="id_tipo_telefone" class="form-control">
						                <option value="0">Tipo de telefone</option>
						                <option value="1" @if($id_tipo_telefone==1) selected @endif>Residencial</option>
						                <option value="2" @if($id_tipo_telefone==2) selected @endif>Comercial</option>
						                <option value="3" @if($id_tipo_telefone==3) selected @endif>Celular</option>
						            </select>
						        </div>
						        <div class="col-md-2">
						        	<label>DDD</label>
						            <input type="text" name="nu_ddd" class="form-control" data-mask="99" value="{{$nu_ddd}}" />
						        </div>
						        <div class="col-md-5">
						        	<label>Número do telefone</label>
						            <input type="text" name="nu_telefone" class="form-control" data-mask="9999-9999?9" value="{{$nu_telefone}}" />
						        </div>
						    </div>
						</div>
					@endif
				</div>
			</fieldset>
		</div>
		<div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados pessoais</legend>
                <div class="col-md-12">
					<div class="form-group row clearfix">
					    <div class="col-md-8">
					    	<label>Endereço</label>
					        <input type="text" name="no_endereco" class="form-control" value="{{$no_endereco}}" />
					    </div>
					    <div class="col-md-2">
					    	<label>Número</label>
					        <input type="text" name="nu_endereco" class="form-control" value="{{$nu_endereco}}" />
					    </div>
						<div class="form-group col-md-2">
							<label>CEP</label>
							<input type="text" name="nu_cep" class="form-control" data-mask="99999-999" value="{{$nu_cep}}" />
						</div>
					</div>
					<div class="form-group row clearfix">
					    <div class="col-md-6">
					    	<label>Bairro</label>
					        <input type="text" name="no_bairro" class="form-control" value="{{$no_bairro}}" />
					    </div>
					    <div class="col-md-6">
					    	<label>Complemento</label>
					        <input type="text" name="no_complemento" class="form-control" value="{{$no_complemento}}" />
					    </div>
					</div>
					<div class="form-group row clearfix">
					    <div class="col-md-6">
					    	<label>Estado</label>
					        <select name="id_estado" class="form-control">
					            <option value="0">Selecione o estado</option>
					            @if(count($estados)>0)
					                @foreach($estados as $estado)
					                    <option value="{{$estado->id_estado}}" @if($id_estado==$estado->id_estado) selected @endif>{{$estado->no_estado}}</option>
					                @endforeach
					            @endif
					        </select>
					    </div>
					    <div class="col-md-6">
					    	<label>Cidade</label>
					        <select name="id_cidade" class="form-control" @if($id_cidade=='') disabled @endif>
					            <option value="0">Selecione uma cidade</option>
					            @if($id_cidade>0)
					            	<option value="{{$id_cidade}}" selected>{{$no_cidade}}</option>
					            @endif
					        </select>
					    </div>
					</div>
				</div>
			</fieldset>
		</div>

		<div class="botoes">
			<input type="reset" class="back btn btn-primary pull-left" value="Cancelar" />
	    	<input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações" />
	   	</div>
	</form>
@endsection