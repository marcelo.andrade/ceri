@extends('configuracoes.configuracoes')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.dadosacesso.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
	<form name="form-dados-acesso" method="post" action="" class="clearfix">
        <div class="erros alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados de acesso</legend>
                <div class="col-md-12">
            		<div class="form-group">
                    	<label>E-mail</label>
                        <input type="text" name="email_usuario" class="form-control" value="{{$usuario->email_usuario}}" disabled />
                    </div>
                    <div class="form-group row clearfix">
                    	<div class="col-md-4">
                        	<label>Senha atual</label>
                            <input type="password" name="senha_usuario" class="form-control" />
                        </div>
                        <div class="col-md-4">
                        	<label>Nova senha</label>
                            <input type="password" name="nova_senha_usuario" class="form-control" />
                        </div>
                        <div class="col-md-4">
                        	<label>Confirme nova a senha</label>
                            <input type="password" name="nova_senha_usuario2" class="form-control" />
                        </div>
                    </div>
            		<div class="botoes">
            			<input type="reset" class="back btn btn-primary pull-left" value="Cancelar" />
            	    	<input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações" />
            	   	</div>
                </div>
            </fieldset>
        </div>
	</form>
@endsection