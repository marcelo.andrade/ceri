@extends('configuracoes.configuracoes')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.certificado.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
	<form name="form-certificado" method="post" action="" class="clearfix">
		@if($usuario->usuario_serventia->serventia->serventia_certificado)
			<div class="fieldset-group clearfix">
	            <fieldset>
	                <legend>Certificado atual</legend>
	                <div class="col-md-12">
	                	<div class="panel table-rounded">
					        <table id="dados-certificado" class="table table-striped table-bordered small">
					            <tr>
					                <td>Nome:</td>
					                <td>{{$usuario->usuario_serventia->serventia->serventia_certificado->no_comum}}</td>
					            </tr>
					            @if($usuario->usuario_serventia->serventia->serventia_certificado->no_email)
					                <tr>
					                    <td>E-mail:</td>
					                    <td>{{$usuario->usuario_serventia->serventia->serventia_certificado->no_email}}</td>
					                </tr>
					            @endif
					            <tr>
					                <td>CPF/CNPJ:</td>
					                <td>{{$usuario->usuario_serventia->serventia->serventia_certificado->nu_cpf_cnpj}}</td>
					            </tr>
					            <tr>
					                <td>Tipo Certificado:</td>
					                <td>{{$usuario->usuario_serventia->serventia->serventia_certificado->tp_certificado}}</td>
					            </tr>
					            <tr>
					                <td>Autoridade Raiz:</td>
					                <td>{{$usuario->usuario_serventia->serventia->serventia_certificado->no_autoridade_raiz}}</td>
					            </tr>
					            <tr>
					                <td>Autoridade Certificadora:</td>
					                <td>{{$usuario->usuario_serventia->serventia->serventia_certificado->no_autoridade_certificadora}}</td>
					            </tr>
					            <tr>
					                <td>Válido de:</td>
					                <td>{{Carbon\Carbon::parse($usuario->usuario_serventia->serventia->serventia_certificado->dt_validade_ini)->format('d/m/Y')}}</td>
					            </tr>
					            <tr>
					                <td>Valido até:</td>
					                <td>{{Carbon\Carbon::parse($usuario->usuario_serventia->serventia->serventia_certificado->dt_validade_fim)->format('d/m/Y')}}</td>
					            </tr>
					            <tr>
					                <td>Data de envio:</td>
					                <td>{{Carbon\Carbon::parse($usuario->usuario_serventia->serventia->serventia_certificado->dt_cadastro)->format('d/m/Y H:i')}}</td>
					            </tr>
					        </table>
					    </div>
				        @if($usuario->usuario_serventia->serventia->serventia_certificado->dt_validade_fim>Carbon\Carbon::now())
				        	<div class="alert alert-success small single">
						        <i class="icon glyphicon glyphicon-ok pull-left"></i>
						        <div class="menssagem"><b>Ótimo!</b> O certificado selecionado acima é válido.</div>
						    </div>
				        @else
					        <div class="alert alert-danger small single">
						        <i class="icon glyphicon glyphicon-remove pull-left"></i>
						        <div class="menssagem"><b>Importante!</b> O certificado selecionado acima expirou, selecione um novo certificado.</div>
						    </div>
						@endif
	                </div>
	            </fieldset>
	        </div>
	        <div class="fieldset-group clearfix">
	            <fieldset>
	                <legend>Enviar novo arquivo</legend>
	                <div class="col-md-12">
		                <div class="erros alert alert-danger" style="display:none">
						    <i class="icon glyphicon glyphicon-remove pull-left"></i>
						    <div class="menssagem"></div>
						</div>
				        <div class="certificado fileinput fileinput-new input-group" data-provides="fileinput">
							<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
							<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecionar novo certificado</span><span class="fileinput-exists">Alterar</span><input type="file" name="no_arquivo"></span>
							<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
						</div>
						<div class="alert alert-warning small single">
					        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
					        <div class="menssagem"><b>Observação!</b> Somente arquivos do tipo <b>".pfx"</b> são suportados.</div>
					    </div>
					    <div class="alert alert-danger small single">
					        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
					        <div class="menssagem"><b>Importante!</b> Ao enviar um novo arquivo, o certificado anterior será desativado de sua conta.</div>
					    </div>
					</div>
				</fieldset>
			</div>
	    @else
	    	<div class="fieldset-group clearfix">
	            <fieldset>
	                <legend>Enviar novo arquivo</legend>
	                <div class="col-md-12">
		                <div class="erros alert alert-danger" style="display:none">
						    <i class="icon glyphicon glyphicon-remove pull-left"></i>
						    <div class="menssagem"></div>
						</div>
						<div class="certificado fileinput fileinput-new input-group" data-provides="fileinput">
							<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
							<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecionar certificado</span><span class="fileinput-exists">Alterar</span><input type="file" name="no_arquivo"></span>
							<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
						</div>
						<div class="alert alert-warning small single">
					        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
					        <div class="menssagem"><b>Observação!</b> Somente arquivos do tipo <b>".pfx"</b> são suportados.</div>
					    </div>
					</div>
				</fieldset>
			</div>
	    @endif
		<div class="botoes">
			<input type="reset" class="back btn btn-primary pull-left" value="Cancelar" />
	    	<input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações" />
	   	</div>
	</form>
@endsection