@extends('configuracoes.configuracoes')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.dadosprodutos.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
	<form name="form-dados-produtos" method="post" action="" class="clearfix">
        {{csrf_field()}}
        <div class="erros alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados dos produtos</legend>
                <div class="col-md-12">
                    <div class="form-group row clearfix">
                        <div class="form-group row clearfix">
                            <input type="hidden" name="id_serventia" id="id_serventia"
                                   value="{{$usuario->usuario_serventia->serventia->id_serventia}}">
                        </div>
                        <div class="fieldset-group clearfix ">
                            <fieldset>
                                <legend>
                                    <div class="checkbox checkbox-info">
                                        <input name="check_valor_ar" id="check_valor_ar" class="styled" type="checkbox">
                                        <label for="check_valor_ar"><b>Alterar valor do AR</b></label>
                                    </div>
                                </legend>
                                <div class="col-md-4">
                                    <label class="small">Valor - AR (Mão Própria)</label>
                                    <input type="text" name="valor_ar" class="form-control real"
                                           value="{{$valor_produto_item['ar']}}" disabled/>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Data de Início da nova vigência</label>
                                    <input type="text" name="dt_ini_vigencia_ar" class="form-control data_futura" readonly disabled>
                                </div>
                            </fieldset>
                        </div>
                        <div class="fieldset-group clearfix">
                            <fieldset>
                                <legend>
                                    <div class="checkbox checkbox-info">
                                        <input name="check_valor_averbacao" id="check_valor_averbacao" class="styled" type="checkbox">
                                        <label for="check_valor_averbacao"><b>Alterar valor de Averbação na prefeitura</b></label>
                                    </div>
                                </legend>
                                <div class="col-md-4">
                                    <label class="small">Valor - Averbação (Legalização)</label>
                                    <input type="text" name="valor_averbacao" class="form-control real"
                                           value="{{$valor_produto_item['averbacao']}}" disabled/>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Data de Início da nova vigência</label>
                                    <input type="text" name="dt_ini_vigencia_averbacao" class="form-control data_futura" readonly disabled>
                                </div>
                            </fieldset>
                        </div>
                        <div class="fieldset-group clearfix">
                            <fieldset>
                                <legend>
                                    <div class="checkbox checkbox-info">
                                        <input name="check_taxa_issqn" id="check_taxa_issqn" class="styled" type="checkbox">
                                        <label for="check_taxa_issqn"><b>Alterar valor da taxa ISSQN</b></label>
                                    </div>
                                </legend>
                                <div class="col-md-4">
                                    <label class="small">Valor (Taxa - ISSQN)</label>
                                    <select name="id_taxa_issqn" class="form-control" disabled>
                                        @foreach($tabela_preco as $issqn)
                                            <option value="{{$issqn->codigo_tabela_preco}}"{{($tabela_preco_serventia->id_tabela_preco == $issqn->codigo_tabela_preco)?' selected':''}}>{{substr($issqn->no_tabela_preco, -8)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Data de Início da nova vigência</label>
                                    <input type="text" name="dt_ini_vigencia_issqn" class="form-control data_futura" readonly disabled>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-12 alert alert-warning">
                        <div class="checkbox checkbox-warning">
                            <input name="check_confirmacao" id="confirmacao" class="styled" type="checkbox">
                            <label for="confirmacao">Estou ciente que as alterações de valores são abertas apenas
                                para os usuários Masters e são de inteira responsabilidade do cartório.</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="botoes">
                            <input type="reset" class="back btn btn-primary pull-left" value="Cancelar"/>
                            <input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações"/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
	</form>
@endsection
