@extends('configuracoes.configuracoes')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.dadosbancarios.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
    @if( $usuario->in_usuario_master == 'S' && $usuario->id_tipo_usuario == 2 )
        <?php
        if (count($usuario->usuario_serventia->serventia->pessoa->banco_pessoa)>0)
        {
            $id_banco				= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->id_banco;
            $id_pessoa				= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->id_pessoa;
            $nu_agencia				= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->nu_agencia;
            $nu_dv_agencia			= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->nu_dv_agencia;
            $nu_conta				= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->nu_conta;
            $nu_dv_conta			= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->nu_dv_conta;
            $tipo_conta				= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->tipo_conta;
            $nu_variacao			= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->nu_variacao;
            $in_tipo_pessoa_conta 	= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->in_tipo_pessoa_conta;
            $nu_cpf_cnpj_conta		= $usuario->usuario_serventia->serventia->pessoa->banco_pessoa->nu_cpf_cnpj_conta;
        } else {
            $id_banco				= '';
            $id_pessoa				= '';
            $nu_agencia				= '';
            $nu_dv_agencia			= '';
            $nu_conta				= '';
            $nu_dv_conta			= '';
            $tipo_conta				= '';
            $nu_variacao			= '';
            $in_tipo_pessoa_conta 	= '';
            $nu_cpf_cnpj_conta 		= '';
        }
        ?>
        <form name="form-dados-bancarios" method="post" action="" class="clearfix">
            <input type="hidden" name="tp_pessoa" value="{{$usuario->pessoa->tp_pessoa}}">
            <div class="erros alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Dados Bancários</legend>
                    <div class="col-md-12">
                        <div class="form-group row clearfix">
                            <div class="col-md-3">
                                <label>Código do Banco</label>
                                <select name="id_banco_codigo" class="form-control">
                                    <option value="0">Selecione o banco</option>
                                    @if(count($codigo_bancos)>0)
                                        @foreach($codigo_bancos as $banco)
                                            <option value="{{$banco->id_banco}}" @if($id_banco==$banco->id_banco) selected="selected" @endif>{{$banco->codigo_banco}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-9">
                                <label>Nome do Banco</label>
                                <select name="id_banco" class="form-control" >
                                    <option value="0">Selecione o banco</option>
                                    @if(count($bancos)>0)
                                        @foreach($bancos as $banco)
                                            <option value="{{$banco->id_banco}}" @if($id_banco==$banco->id_banco) selected="selected" @endif>{{$banco->no_banco}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row clearfix">
                            <div class="col-md-3">
                                <label>Número da Agência</label>
                                <input name="nu_agencia" class="form-control"  type="text" value="<?php echo $nu_agencia ?>" maxlength="4">
                            </div>
                            <div class="col-md-3">
                                <label>Dígito da Agência</label>
                                <input name="nu_dv_agencia" class="form-control"  type="text" value="<?php echo $nu_dv_agencia ?>" maxlength="1">
                            </div>
                            <div class="col-md-3">
                                <label>Número da Conta</label>
                                <input name="nu_conta" class="form-control"  type="text" value="<?php echo $nu_conta ?>" maxlength="10">
                            </div>
                            <div class="col-md-3">
                                <label>Dígito Conta</label>
                                <input name="nu_dv_conta" class="form-control"  type="text" value="<?php echo $nu_dv_conta ?>" maxlength="1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row clearfix">
                            <div class="col-md-3">
                                <label>Tipo de conta</label>
                                <select name="tipo_conta" class="form-control" >
                                    <option value="0">Selecione</option>
                                    <option value="C" @if($tipo_conta=='C') selected="selected" @endif >CORRENTE</option>
                                    <option value="P" @if($tipo_conta=='P') selected="selected" @endif >POUPANÇA</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Variação</label>
                                <input name="nu_variacao" class="form-control"  type="text" value="<?php echo $nu_variacao ?>">
                            </div>
                            <div id="tipo-pessoa">
                                <div class="col-md-3">
                                    <label>Tipo de pessoa</label>
                                    <select name="tp_pessoa" class="form-control">
                                        <option value="F"  @if($in_tipo_pessoa_conta=='F') selected="selected" @endif>Física</option>
                                        <option value="J"  @if($in_tipo_pessoa_conta=='J') selected="selected" @endif>Jurídica</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="bp-pessoa-fisica" @if($in_tipo_pessoa_conta=='J') style="display: none;" @endif>
                                        <label>CPF</label>
                                        <input type="text" data-mask="999.999.999-99" class="form-control" name="nu_cpf_cnpj_conta" value="{{$nu_cpf_cnpj_conta}}" @if($in_tipo_pessoa_conta=='J') disabled @endif/>
                                    </div>
                                    <div class="bp-pessoa-juridica" @if($in_tipo_pessoa_conta=='F' or $in_tipo_pessoa_conta=='') style="display: none;" @endif>
                                        <label>CNPJ</label>
                                        <input type="text" data-mask="99.999.999/9999-99" class="form-control" name="nu_cpf_cnpj_conta" id="nu_cpf_cnpj_conta" value="{{$nu_cpf_cnpj_conta}}" @if($in_tipo_pessoa_conta=='F' or $in_tipo_pessoa_conta=='') disabled @endif/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="botoes">
                <input type="reset" class="back btn btn-primary pull-left" value="Cancelar" />
                <input type="submit" class="continue btn btn-success pull-right" value="Salvar alterações" />
            </div>
        </form>
    @endif
@endsection