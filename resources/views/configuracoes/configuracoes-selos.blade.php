@extends('configuracoes.configuracoes')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.selos.js')}}?v=<?=time();?>"></script>
@endsection

@section('sub-content')
	<button data-target="#novo-selo" data-toggle="modal" class="new btn btn-success" type="button">
		Importar novo selo digital
	</button>
	@include('configuracoes.configuracoes-selos-historico')

	<div id="novo-selo" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header gradient01">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Importar novo selo digital</h4>
				</div>
				<div class="modal-body">
					<div class="carregando flutuante text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
					<div class="form">
						<form name="form-selos" method="post" action="" class="clearfix">
							<div class="fieldset-group clearfix">
								<fieldset>
									<legend>Enviar novo arquivo</legend>
									<div class="col-md-12">
										<div class="erros alert alert-danger" style="display:none">
											<i class="icon glyphicon glyphicon-remove pull-left"></i>
											<div class="menssagem"></div>
										</div>
										<div class="selos fileinput fileinput-new input-group" data-provides="fileinput">
											<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
											<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Selecionar selos</span><span class="fileinput-exists">Alterar</span><input type="file" name="no_arquivo"></span>
											<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
										</div>
										<div class="alert alert-warning small single">
											<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
											<div class="menssagem"><b>Observação!</b> Somente arquivos do tipo <b>".xml"</b> são suportados.</div>
										</div>
									</div>
								</fieldset>
							</div>
							<div class="modal-footer">
								<button type="button" class="back btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
								<input type="submit" class="continue btn btn-success pull-right" value="Importar arquivo" />
							</div>
						</form>

					</div>
				</div>

			</div>
		</div>
	</div>








@endsection