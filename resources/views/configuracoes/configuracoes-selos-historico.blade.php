<br />
<div class="panel table-rounded">
    <table class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th>Nome do Arquivo</th>
            <th>Data da compra</th>
            <th>Selos importados</th>
        </tr>
        </thead>
        <tbody>
        @if (count($todos_selos_entrada)>0)
            @foreach ($todos_selos_entrada as $selos)
                <tr>
                    <td>{{$selos->no_arquivo}}</td>
                    <td>{{$selos->dt_cadastro}}</td>
                    <td>{{$selos->lote_selos->sum('nu_quantidade_selo')}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum certidão foi encontrada.
                        </div>
                    </div>
                </td>
            </tr>
        @endif


        </tbody>
    </table>
</div>