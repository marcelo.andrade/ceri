@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.contato.js')}}"></script>
@endsection

@section('content')
	<div class="container">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading gradient01">
							<h4>Formulário de contato / <small>Solicitação</small></h4>
						</div>
						<div class="panel-body">
							<div class="container">
								<div class="row">
									<form class="clearfix" name="form-contato" id="form-contato">
										<div class="carregando" style="display: none">
											<div class="text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
										</div>
										<div class="erros alert alert-danger" style="display:none">
											<i class="icon glyphicon glyphicon-remove "></i>
											<div class="menssagem"></div>
										</div>
										<div class="form-group col-md-12">
											<div class="col-md-6">
												<label>Área</label>
												<select class="form-control" name="area" id="area">
													<option value="0">Selecione</option>
													<option value="suporte">Suporte</option>
													<option value="financeiro">Financeiro</option>
													<option value="reclamacao">Reclamações</option>
													<option value="sugestao">Sugestões</option>
												</select>
											</div>
										</div>
										<div class="form-group col-md-12">
											<div class="col-md-6">
												<label>Título</label>
												<input class="form-control" value="" name="titulo" id="titulo" type="text">
											</div>
										</div>
										<div class="form-group col-md-12">
											<div class="col-md-6">
												<label>Descrição</label>
												<textarea rows="10" class="form-control" name="descricao" id="descricao"></textarea>
											</div>
										</div>
										<div class="form-group col-md-12">
											<div class="col-md-2">
												<input type="button"  class="btn btn-primary" name="btnEnviarContato" id="btnEnviarContato" value="Enviar solicitação">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection