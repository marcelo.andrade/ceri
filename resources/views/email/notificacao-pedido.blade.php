<div style="background:#F5F5F5; font-size:13px; font-family:\'Trebuchet MS\', Arial, Helvetica, sans-serif; padding:10px">
	<h2 style="margin:0 0 10px 0;"><img src="{{asset('images/logo06.png')}}" alt="CERI" /></h2>
	<div style="background:#FFF;border:1px solid #005071; padding:15px">
        Prezado(a) <b><?=$no_pessoa;?></b>,<br /><br />
       	Você tem uma nova resposta para o seu pedido com o protocolo número: <?=$pedido->protocolo_pedido;?><br /><br />
       	Para acompanhar sua solicitação acesse a Central Eletrônica de Registro de Imóveis - CERI no link abaixo<br /><br />
        <a href="{{$url_email}}"><b>&raquo; Acessar o site CERI</b></a>
	</div>
	<p style="line-height:20px;font-size:11px;margin-bottom:0">Este é um e-mail automático, por favor não o responda.<br /> 2017 &copy; CERI.</p>
</div>
