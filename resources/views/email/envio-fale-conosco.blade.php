<div style="background:#F5F5F5; font-size:13px; font-family:\'Trebuchet MS\', Arial, Helvetica, sans-serif; padding:10px;">
	<h2 style="margin:0 0 10px 0;"><img src="{{asset('images/logo06.png')}}" alt="CERI" /></h2>
	<div align="center" style="text-align: center"><h1>Formulário de Contato</h1></div>
	<div style="background:#FFF;border:1px solid #005071; padding:15px">
		<table width="100%" style="padding:15px; text-align: left" >
			<tr>
				<th>Nome do cliente</th>
				<th>E-mail</th>
				<th>Telefone</th>
			</tr>
			<tr>
				<td><?php echo $dados['nome']; ?></td>
				<td><?php echo $dados['email']; ?></td>
				<td><?php echo $dados['telefone']; ?></td>
			</tr>
		</table>
	</div>
	<br /><br />
	<div style="background:#FFF;border:1px solid #005071; padding:15px">
		<h3>Título da solicitação</h3>
		<?php echo $dados['titulo']; ?>,
		<br /><br />
		<h3>Descrição</h3>
		<?php echo $dados['descricao']; ?>.
	</div>
</div>
