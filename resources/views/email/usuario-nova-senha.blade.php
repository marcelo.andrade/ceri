<div style="background:#F5F5F5; font-size:13px; font-family:\'Trebuchet MS\', Arial, Helvetica, sans-serif; padding:10px">
	<h2 style="margin:0 0 10px 0;"><img src="{{asset('images/logo06.png')}}" alt="CERI" /></h2>
	<div style="background:#FFF;border:1px solid #005071; padding:15px">
        Olá <b><?=$pessoa->no_pessoa;?></b>.<br /><br />
        Uma nova senha foi gerada com sucesso na Central Eletrônica de Registro de Imóveis - CERI.<br /><br />
       	<b>Senha gerada automaticamente:</b> {{$senha_gerada}}<br /><br />
        <a href="{{URL::to('/')}}"><b>&raquo; Acessar o sistema</b></a>
	</div>
	<p style="line-height:20px;font-size:11px;margin-bottom:0">Este é um e-mail automático, por favor não o responda.<br /> 2017 &copy; CERI.</p>
</div>
