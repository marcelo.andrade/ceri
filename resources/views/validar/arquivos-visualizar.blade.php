@if($arquivo_grupo_produto->id_usuario_certificado>0)
    <div class="alert alert-success clearfix">
        <i class="icon glyphicon glyphicon-ok pull-left"></i>
        <div class="menssagem pull-left">
            O documento foi assinado digitalmente por:<br />
            <b>{{$arquivo_grupo_produto->usuario_certificado->no_comum}}</b><br />
            <b>Data da assinatura:</b> {{Carbon\Carbon::parse($arquivo_grupo_produto->dt_ass_digital)->format('d/m/Y \à\s H:i:s')}}
        </div>
        <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#visualizar-certificado" data-hasharquivo="{{$hash_arquivo}}" data-noarquivo="{{$arquivo_grupo_produto->no_arquivo}}"><i class="fa fa-lock"></i> Ver certificado</button>
    </div>
@endif
@if(in_array($arquivo_grupo_produto->no_extensao,array('pdf','jpg','png','bmp','gif')))
	@if($arquivo_grupo_produto->no_extensao=='pdf')
        <object data="{{URL::to('/validar/visualizar-arquivo/render/'.$hash_arquivo.'/'.$arquivo_grupo_produto->no_arquivo)}}" type="application/pdf" class="resultado-pdf @if($arquivo_grupo_produto->id_usuario_certificado>0) assinado @endif">
            <p>Seu navegador não tem um plugin pra PDF</p>
        </object>
	@else
    	<img src="{{URL::to('/validar/visualizar-arquivo/render/'.$hash_arquivo.'/'.$arquivo_grupo_produto->no_arquivo)}}" class="img-responsive" />
    @endif
@else
    <div class="alert alert-warning single">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem">
            O arquivo não pode ser visualizado, por favor, faça o download.
        </div>
    </div>
@endif