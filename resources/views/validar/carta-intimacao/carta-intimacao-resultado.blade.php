<div class="resultado fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Resultado da carta de intimação</legend>
            <div class="erros-resultado alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix" style="margin-bottom: 10px">
                <div class="col-md-6">
                    <fieldset>
                        <legend>Devedor(es)</legend>
                        <div class="form-group col-md-12" style="margin-top: 5px; margin-bottom: 10px">
                            @foreach($arquivo_grupo_produto as $devedor)
                                {{$devedor->no_devedor}} <br />
                            @endforeach
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <legend>Carta de intimação</legend>
                        <div class="form-group col-md-12">
                            <div class="btn-group" style="margin-top: 5px; margin-bottom: 10px">
                                <button type="button" class="btn btn-sm btn-primary"
                                        data-toggle="modal" data-target="#visualizar-arquivo"
                                        data-hasharquivo="{{$hash_arquivo}}"
                                        data-idarquivo="{{$arquivo_grupo_produto[0]->id_arquivo_grupo_produto}}"
                                        data-noarquivo="{{$arquivo_grupo_produto[0]->no_arquivo}}"
                                        data-noextensao="{{$arquivo_grupo_produto[0]->no_extensao}}">{{$arquivo_grupo_produto[0]->no_arquivo}}
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
    </fieldset>
</div>