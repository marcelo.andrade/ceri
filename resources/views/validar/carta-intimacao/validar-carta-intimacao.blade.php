@extends('layout.home')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.validar.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.validar.carta.intimacao.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        <div class="wrapper row">
            <div class="col-md-4 hidden-xs hidden-sm"></div>
            <div class="box-start col-md-4 col-sm-6">
                <div class="default-login">
                    <h3>Validar carta de intimação</h3>
                    <p>Digite o número do protocolo abaixo.</p>
                    <form name="form-validar-carta-intimacao" method="post" action="">
                        <div class="erros alert alert-danger" style="display:none">
                            <i class="icon glyphicon glyphicon-remove pull-left"></i>
                            <div class="menssagem"></div>
                        </div>
                        <div class="form-group">
                            <label>Número do protocolo</label>
                            <input type="text" name="protocolo_pedido_arquivo" class="form-control numero_protocolo_arquivo"/>
                        </div>
                        <input type="submit" class="access btn btn-primary btn-block" value="Validar" />
                    </form>
                </div>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm"></div>
        </div>
    </div>
    <div id="resultado-carta-intimacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Carta de intimação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="visualizar-arquivo" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Arquivo - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <a id="arquivo-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="visualizar-certificado" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Certificado do arquivo - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection