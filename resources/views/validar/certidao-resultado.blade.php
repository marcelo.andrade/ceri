<div class="resultado fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Resultado da certidão</legend>
        <form name="form-novo-resultado" method="post" action="" class="clearfix" enctype="multipart/form-data">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}"/>
            <div class="erros-resultado alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-4">
                    <div class="col-md-3">
                        <div>
                            @if($pedido->pedido_resultado->in_positivo=='S')
                                <label>
                                    POSITIVO
                                </label>
                            @endif
                        </div>
                        <div>
                            @if($pedido->pedido_resultado->in_positivo=='N')
                                <label>
                                    NEGATIVO
                                </label>
                            @endif
                        </div>
                    </div>
                </div>
                <?php
                switch ($pedido->pedido_resultado->in_positivo) {
                case 'S':
                ?>
                <div class="resultado-positivo col-md-8">
                    @if($pedido->pedido_resultado->de_resultado!='')
                        <div class="fieldset-group">
                            <fieldset>
                                <legend>
                                    <label for="digitar">
                                        <b>Observação</b>
                                    </label>
                                </legend>
                                <div class="form-group col-md-12">
                                    <div class="well">
                                        {{$pedido->pedido_resultado->de_resultado}}
                                    </div>
                                </div>
                                <?php
                                /*
                                <div class="form-group col-md-12">
                                    <button type="button" data-toggle="modal" data-target="#imprimir-resultado-certidao" data-idpedido="{{$pedido->id_pedido}}" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-print"></span> Imprimir Resultado
                                    </button>
                                </div>
                                */
                                ?>
                            </fieldset>
                        </div>
                    @endif
                    @if(count($arquivos_resultado)>0)
                        <div class="fieldset-group col-md-12">
                            <fieldset>
                                <legend>
                                    <div>
                                        <label for="upload">
                                            <b>Upload do resultado</b>
                                        </label>
                                    </div>
                                </legend>
                                <div class="col-md-12">
                                    <div id="arquivos-resultado" class="col-md-12">
                                        @foreach($arquivos_resultado as $arquivo)
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#visualizar-arquivo"
                                                        data-hasharquivo="{{array_search($arquivo->id_arquivo_grupo_produto,$hash_arquivos)}}"
                                                        data-noarquivo="{{$arquivo->no_arquivo}}"
                                                        data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                @if($arquivo->in_ass_digital=='S')
                                                    <button type="button" class="icone-assinatura btn btn-success"
                                                            data-toggle="modal" data-target="#visualizar-certificado"
                                                            data-hasharquivo="{{array_search($arquivo->id_arquivo_grupo_produto,$hash_arquivos)}}"
                                                            data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i>
                                                    </button>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    @endif
                </div>
                <?php
                break;
                case 'N':
                ?>
                <div class="resultado-negativo col-md-8">
                    <fieldset>
                        <legend>Resultado</legend>
                        <div class="form-group col-md-12">
                            <div class="well" style="margin-bottom:0">Não foram localizados registros com a chave <label
                                        class="label label-danger label-sm">{{$pedido->certidao->de_chave_certidao}}</label>
                                pesquisada nesta serventia.
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="button" data-toggle="modal" data-target="#imprimir-resultado-certidao"
                                    data-idpedido="{{$pedido->id_pedido}}" class="btn btn-primary">
                                <span class="glyphicon glyphicon-print"></span> Imprimir Resultado
                            </button>
                        </div>
                    </fieldset>
                </div>
                <?php
                break;
                }
                ?>
            </div>
        </form>
    </fieldset>
</div>