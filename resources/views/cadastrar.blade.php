@extends('layout.home')

@section('scripts')
@endsection

@section('content')
    <div class="container">
        <div class="wrapper row">
            <div class="box-start col-md-4 col-sm-4 col-xs-12">
                <div class="default-select-register clearfix">
                    <h2>Cartório</h2>
                    <br />
                    <div class="text-center">
                        <i class="fa fa-home"></i><br /><br />
                        <a href="{{url('/cadastrar/cartorio')}}" class="btn btn-primary disabled">Cadastre-se como cartório</a>
                    </div>
                </div>
            </div>
            <div class="box-start col-md-4 col-sm-4 col-xs-12">
                <div class="default-select-register clearfix">
                    <h2>Usuário comum</h2>
                    <br />
                    <div class="text-center">
                        <i class="fa fa-user"></i><br /><br />
                        <a href="{{url('/cadastrar/usuario')}}" class="btn btn-primary">Cadastre-se como usuário</a>
                    </div>
                </div>
            </div>
            <div class="box-start col-md-4 col-sm-4 col-xs-12">
                <div class="default-select-register clearfix">
                    <h2>Judiciário</h2>
                    <br />
                    <div class="text-center">
                        <i class="fa fa-balance-scale menor"></i><br /><br />
                        <a href="{{--url('/cadastrar/judiciario')--}}javascript:void(0);" class="btn btn-primary disabled">Cadastre-se como judiciário</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection