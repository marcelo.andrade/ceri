@extends('layout.home')

@section('scripts')
@endsection

@section('content')
    <div class="container">
        <h2 class="titulo">
            INSTITUCIONAL
            <p class="info">Saiba mais sobre a Central Eletrônica de Registro de Imóveis do Estado De Mato Grosso do Sul</p>
        </h2>        
        <p>A Central Eletrônica de Registro de Imóveis do Estado de Mato Grosso do Sul (CERI-MS) foi criada para operacionalização do Sistema de Registro Eletrônico de Imóveis (SREI), previsto pela Lei nº 11.977/2009 e regulamentado pelo Provimento nº 47, de 19 de junho de 2015, da Corregedoria Nacional de Justiça.</p>
        <p>Em 22 de novembro de 2016, o Tribunal de Justiça do Estado de Mato Grosso do Sul (TJMS) publicou o Provimento nº 146, que trata da regulamentação da CERI-MS, <a href="{{url('/documentos/caderno-unificado.pdf')}}" target="_blank">clique aqui</a>  para mais detalhes.</p>
        <p>Serão disponibilizados pela CERI-MS todos os serviços cartoriais de registro de imóveis, por meio eletrônico, entre eles a recepção e envio de notificação de alienação fiduciária, contratos e escrituras, a expedição de certidões, a pesquisa para localização de imóveis e a visualização de matrículas on-line entre outros.</p>
        <p>Além do público em geral, o Poder Judiciário e a Administração Pública e Instituições Financeiras também poderão se beneficiarão dos serviços, pois terão a sua disposição um instrumento totalmente integrado aos cartórios de forma on-line, trazendo maior eficiência para a comunicação com os cartórios de todo o Estado de Mato Grosso do Sul.</p>
        <p>A CERI-MS será administrada pela Anoreg - Associação dos Notários e Registradores de Mato Grosso do Sul (ANOREG-MS), associação com sede em Campo Grande, entidade representativa dos registradores imobiliários do estado.</p><br/>
        <div class="text-center">
            <iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqQlJUd2NsN3RpY28/preview" width="880" height="490" allowfullscreen  frameborder="0"></iframe>
        </div>
    </div>
@endsection