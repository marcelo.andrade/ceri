<ul class="nav nav-tabs panel-primary" id="tabs_comunicacao_detalhe">
    <li class="active"><a data-toggle="tab" href="#vendedor_detalhe">Dados do vendedor</a></li>
    <li><a data-toggle="tab" href="#comprador_detalhe">Dados do comprador</a></li>
    <li><a data-toggle="tab" href="#veiculo_detalhe">Dados do veículo</a></li>
    <li><a data-toggle="tab" href="#arquivos_detalhe">Arquivos</a></li>
</ul>

<div class="tab-content">
    <div id="vendedor_detalhe" class="tab-pane fade in active">
        {{--DADOS DO VENDEDOR--}}
        <br />
        <div class="col-md-12 fieldset-group clearfix">
            <div class="fieldset-group clearfix">
                {{--<fieldset>
                    <legend>Dados do vendedor</legend>--}}
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="radio radio-inline">
                            <input type="radio" value="F"  @if( $resultado->operacao_parte[0]->parte_operacao_veiculo->pessoa->tp_pessoa == 'F') {{ 'checked=checked' }} @endif >
                            <label class="small" for="">
                                CPF
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <input type="radio" value="J" @if( $resultado->operacao_parte[0]->parte_operacao_veiculo->pessoa->tp_pessoa == 'J') {{ 'checked=checked' }} @endif >
                            <label class="small" for="">
                                CNPJ
                            </label>
                        </div>
                    </div>

                    <div class="form-group row clearfix">
                        <div class="col-md-4">
                            <label class="small">Documento identificador <span></span></label>
                            <input type="text" disabled="disabled" class="form-control" name="nu_cpf_cnpj_vendedor" value="{{$resultado->operacao_parte[0]->parte_operacao_veiculo->nu_cpf_cnpj}}" disabled>
                        </div>
                        <div class="col-md-8">
                            <label class="small"><span>Nome</span></label>
                            <input type="text" placeholder="" class="form-control" name="no_vendedor" maxlength="80" value="{{$resultado->operacao_parte[0]->parte_operacao_veiculo->no_parte_operacao_veiculo}}" disabled>
                        </div>
                    </div>

                    <div class="form-group row clearfix">
                        <div class="col-md-8">
                            <label class="small">E-mail</label>
                            <input type="text" placeholder="" class="form-control" name="no_email_vendedor" maxlength="50" value="{{$resultado->operacao_parte[0]->parte_operacao_veiculo->pessoa->no_email_pessoa}}" disabled>
                        </div>
                        @if( $resultado->operacao_parte[0]->parte_operacao_veiculo->pessoa->tp_pessoa == 'F')
                            <div class="col-md-4 .rg_vendedor" id="rg_vendedor">
                                <label class="small">RG <span></span></label>
                                <input type="text"  class="form-control" name="rg_vendedor" maxlength="14" value="{{$resultado->operacao_parte[0]->parte_operacao_veiculo->nu_rg}}" disabled>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="fieldset-group col-md-12">
                    <fieldset>
                        <legend>Telefone(s) selecionados</legend>
                        <div id="telefones_vendedor_escolhidas">
                            @foreach ($resultado->operacao_parte[0]->parte_operacao_veiculo->pessoa->telefones as $telefone )
                                <div class="telefones btn-group" >
                                    <button class="btn btn-sm btn-primary" type="button"> <?php echo $telefone->nu_ddd.'-'.$telefone->nu_telefone ?></button>
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
                {{--</fieldset>--}}

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="comprador_detalhe" class="tab-pane fade">
        {{--DADOS DO COMPRADOR--}}
        <br />
        <div class="col-md-12 fieldset-group clearfix">
            <div class="fieldset-group clearfix">
                {{--<fieldset>
                    <legend>Dados do vendedor</legend>--}}
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="radio radio-inline">
                            <input type="radio" value="F" id="" name="tp_pessoa_vendedor"  @if( $resultado->operacao_parte[1]->parte_operacao_veiculo->pessoa->tp_pessoa == 'F') {{ 'checked' }} @endif >
                            <label class="small" for="tp_pessoa_f">
                                CPF
                            </label>
                        </div>
                        <div class="radio radio-inline">
                            <input type="radio" value="J" id="" name="tp_pessoa_vendedor" @if( $resultado->operacao_parte[1]->parte_operacao_veiculo->pessoa->tp_pessoa == 'J') {{ 'checked' }} @endif >
                            <label class="small" for="tp_pessoa_j">
                                CNPJ
                            </label>
                        </div>
                    </div>

                    <div class="form-group row clearfix">
                        <div class="col-md-4">
                            <label class="small">Documento identificador <span></span></label>
                            <input type="text" disabled="disabled" class="form-control" name="nu_cpf_cnpj_vendedor" value="{{$resultado->operacao_parte[1]->parte_operacao_veiculo->nu_cpf_cnpj}}" disabled>
                        </div>
                        <div class="col-md-8">
                            <label class="small"><span>Nome</span></label>
                            <input type="text" placeholder="" class="form-control" name="no_vendedor" maxlength="80" value="{{$resultado->operacao_parte[1]->parte_operacao_veiculo->no_parte_operacao_veiculo}}" disabled>
                        </div>
                    </div>

                    <div class="form-group row clearfix">
                        <div class="col-md-8">
                            <label class="small">E-mail</label>
                            <input type="text" placeholder="" class="form-control" name="no_email_vendedor" maxlength="50" value="{{$resultado->operacao_parte[1]->parte_operacao_veiculo->pessoa->no_email_pessoa}}" disabled>
                        </div>
                        @if( $resultado->operacao_parte[1]->parte_operacao_veiculo->pessoa->tp_pessoa == 'F')
                            <div class="col-md-4 .rg_vendedor" id="rg_vendedor">
                                <label class="small">RG <span></span></label>
                                <input type="text"  class="form-control" name="rg_vendedor" maxlength="14" value="{{$resultado->operacao_parte[1]->parte_operacao_veiculo->nu_rg}}" disabled>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="fieldset-group col-md-12">
                    <fieldset>
                        <legend>Telefone(s) selecionados</legend>
                        <div id="telefones_vendedor_escolhidas">
                            @foreach ($resultado->operacao_parte[1]->parte_operacao_veiculo->pessoa->telefones as $telefone )
                                <div class="telefones btn-group" >
                                    <button class="btn btn-sm btn-primary" type="button"> <?php echo $telefone->nu_ddd.'-'.$telefone->nu_telefone ?></button>
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
                {{--</fieldset>--}}

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="veiculo_detalhe" class="tab-pane fade">
        {{--DADOS DO VEICULO--}}
        <br />
        <div class="col-md-12 fieldset-group clearfix">
            <div class="fieldset-group clearfix">

                {{--<fieldset>
                    <legend>Dados do veículo</legend>--}}
                <div class="form-group row clearfix">
                    <div class="col-md-4">
                        <label class="small">Placa</label>
                        <input type="text" name="placa" id="placa" class="form-control placa" value="{{ $resultado->veiculo->placa }}" disabled>
                    </div>
                    <div class="col-md-4">
                        <label class="small">Nº do CRV</label>
                        <input type="text" name="crv" id="crv" class="form-control crv" value="{{ $resultado->veiculo->crv }}" disabled>
                    </div>
                    <div class="col-md-4">
                        <label class="small">Nº Chassi</label>
                        <input type="text" name="chassi" id="chassi" class="form-control chassi" value="{{ $resultado->veiculo->chassi }}" disabled>
                    </div>
                </div>

                <div class="form-group row clearfix">
                    <div class="col-md-4">
                        <label class="small">Nº Renavam</label>
                        <input type="text" name="renavam" id="renavam" class="form-control renavam" value="{{ $resultado->veiculo->renavam }}" disabled>
                    </div>
                    <div class="col-md-4">
                        <label class="small">Cor</label>
                        <select id="cor" name="cor" class="form-control" disabled>
                            <option value="0">{{ $resultado->veiculo->cor->no_cor_veiculo }}</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="small">Ano fabricação</label>
                        <select id="nu_ano_fabricacao" name="nu_ano_fabricacao" class="form-control" disabled>
                            <option value="0">{{ $resultado->veiculo->nu_ano_fabricacao }}</option>

                        </select>

                    </div>
                    <div class="col-md-2">
                        <label class="small">Ano modelo</label>
                        <select id="nu_ano_modelo" name="nu_ano_modelo" class="form-control" disabled>
                            <option value="0">{{ $resultado->veiculo->nu_ano_modelo }}</option>

                        </select>
                    </div>
                </div>

                <div class="form-group row clearfix">
                    <div class="col-md-4">
                        <label class="small">Marca</label>
                        <select class="form-control" disabled>
                            <option value="0">{{ $resultado->veiculo->modelo->marca->no_marca_veiculo }}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="small">Modelo</label>
                        <select class="form-control" disabled>
                            <option value="0">{{ $resultado->veiculo->modelo->no_modelo_veiculo }}</option>
                        </select>
                    </div>
                </div>
                {{--</fieldset>--}}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="arquivos_detalhe" class="tab-pane fade">
        <br />
        <fieldset>
            <legend>Documentos do vendedor</legend>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table class="table table-striped table-bordered small">
                            <thead>
                                <tr class="gradient01">
                                    <th class="col-md-10">Tipo de arquivo</th>
                                    <th>Arquivo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $resultado->operacao_parte[0]->parte_arquivos as $arquivo )
                                    @if( $arquivo->arquivo_grupo_produto->tipo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto != $tipo_arquivo_crv )
                                        <tr>
                                            <td>{{ $arquivo->arquivo_grupo_produto->tipo_arquivo_grupo_produto->no_tipo_arquivo  }}</td>
                                            <td>
                                                <a id="arquivo-download" href="{{URL::to('/arquivos/download/detran/comunicado-venda/'.$arquivo->arquivo_grupo_produto->id_arquivo_grupo_produto)}}" target="_blank" href="" class="btn btn-success btn-sm pull-left" ><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Documentos do comprador</legend>
            <div class="fieldset-group clearfix">
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="panel table-rounded">
                            <table class="table table-striped table-bordered small">
                                <thead>
                                <tr class="gradient01">
                                    <th  class="col-md-10">Tipo de arquivo</th>
                                    <th>Arquivo</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $resultado->operacao_parte[1]->parte_arquivos as $arquivo )
                                    <tr>
                                        <td>{{ $arquivo->arquivo_grupo_produto->tipo_arquivo_grupo_produto->no_tipo_arquivo  }}</td>
                                        <td>
                                            <a id="arquivo-download" href="{{URL::to('/arquivos/download/detran/comunicado-venda/'.$arquivo->arquivo_grupo_produto->id_arquivo_grupo_produto)}}" target="_blank" href="" class="btn btn-success btn-sm pull-left" ><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Documento do veículo</legend>
            <div class="fieldset-group clearfix">
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="panel table-rounded">
                            <table class="table table-striped table-bordered small">
                                <thead>
                                <tr class="gradient01">
                                    <th class="col-md-10">Tipo de arquivo</th>
                                    <th >Arquivo</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $resultado->operacao_parte[0]->parte_arquivos as $arquivo )
                                    @if( $arquivo->arquivo_grupo_produto->tipo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto == $tipo_arquivo_crv )
                                        <tr>
                                            <td>{{ $arquivo->arquivo_grupo_produto->tipo_arquivo_grupo_produto->no_tipo_arquivo  }}</td>
                                            <td>
                                                <a id="arquivo-download" href="{{URL::to('/arquivos/download/detran/comunicado-venda/'.$arquivo->arquivo_grupo_produto->id_arquivo_grupo_produto)}}" target="_blank" href="" class="btn btn-success btn-sm pull-left" ><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>