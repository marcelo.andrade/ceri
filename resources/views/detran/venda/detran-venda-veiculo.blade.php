@extends('layout.comum')

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Comunicação de venda de veículo</h4>
            </div>
            <div class="panel-body">
                <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#nova-venda">
                    Nova venda
                </button>
                @include('detran.venda.detran-historico')
            </div>
        </div>
    </div>

@endsection