@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.detran.venda.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')

    <div class="container">

        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Comunicação de venda <span class="small">/ Nova</span></h4>
            </div>
            <div class="panel-body" id="filtro-nova_comunicacao">
                <button data-target="#nova_comunicacao" data-toggle="modal" class="new btn btn-success" type="button">
                    Nova comunicação
                </button>
            </div>
        </div>
        <div class="panel table-rounded">
            <table class="table table-striped table-bordered small">
                <thead>
                <tr class="gradient01">
                    <th>Protocolo</th>
                    <th>Data da operação</th>
                    <th>Placa do veículo</th>
                    <th>Nº do CRV</th>
                    <th>Nº Chassi</th>
                    <th>Renavam</th>
                    <th>Nome do vendedor</th>
                    <th>Nome do comprador</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @if (count($todos_veiculos_venda)>0)
                    @foreach ($todos_veiculos_venda as $venda)
                        <tr>
                            <td>{{ $venda->protocolo_pedido }}</td>
                            <td>{{ formatar_data_hora($venda->dt_operacao) }}</td>
                            <td>{{ $venda->placa }}</td>
                            <td>{{ $venda->crv }}</td>
                            <td>{{ $venda->chassi }}</td>
                            <td>{{ $venda->renavam }}</td>
                            <td>{{ $venda->vendedor }}</td>
                            <td>{{ $venda->comprador }}</td>
                            <td class="options">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu black">
                                        <li><a href="#" data-toggle="modal" data-target="#detalhes-venda"    data-id_operacao_veiculo="{{$venda->id_operacao_veiculo}}" data-protocolo="{{$venda->numero_protocolo}}" >Ver detalhes</a></li>
                                        {{--<li><a href="#" data-toggle="modal" data-target="#observacoes-venda" data-id_operacao_veiculo="{{$venda->id_operacao_veiculo}}" data-protocolo="{{$venda->numero_protocolo}}" >Imprimir comprovante</a></li>--}}
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">
                            <div class="single alert alert-danger">
                                <i class="glyphicon glyphicon-remove"></i>
                                <div class="mensagem">
                                    Nenhuma comunicação foi encontrada.
                                </div>
                            </div>
                        </td>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>

    <div id="nova_comunicacao" class="modal fade"  data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nova comunicação</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-comunicacao" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="enviar-comunicacao btn btn-success" name="enviar-comunicacao" id="enviar-comunicacao" >Salvar comunicação</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-venda" class="modal fade"  data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhas da comunicação de venda do veículo</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-comunicacao" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>

                </div>
            </div>
        </div>
    </div>
@endsection