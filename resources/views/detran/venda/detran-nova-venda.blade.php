<?php $ano = date("Y"); ?>
<input type="hidden" name="id_veiculo" id="id_veiculo" value="0" />
<input type="hidden" name="id_pessoa_vendedor" id="id_pessoa_vendedor" value="0" />
<input type="hidden" name="id_pessoa_comprador" id="id_pessoa_comprador" value="0" />
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>

<ul class="nav nav-tabs panel-primary" id="tabs_comunicacao">
    <li class="active"><a data-toggle="tab" href="#vendedor">Dados do vendedor</a></li>
    <li><a data-toggle="tab" href="#comprador">Dados do comprador</a></li>
    <li><a data-toggle="tab" href="#veiculo">Dados do veículo</a></li>
    <li><a data-toggle="tab" href="#arquivos">Arquivos</a></li>
</ul>

<div class="tab-content">
    <div id="vendedor" class="tab-pane fade in active">
        {{--DADOS DO VENDEDOR--}}
        <br />
        <div class="col-md-12 fieldset-group clearfix">
            <div class="fieldset-group clearfix">
                {{--<fieldset>
                    <legend>Dados do vendedor</legend>--}}
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="radio radio-inline">
                                <input type="radio" value="F" id="tp_pessoa_f" name="tp_pessoa_vendedor">
                                <label class="small" for="tp_pessoa_f">
                                    CPF
                                </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" value="J" id="tp_pessoa_j" name="tp_pessoa_vendedor">
                                <label class="small" for="tp_pessoa_j">
                                    CNPJ
                                </label>
                            </div>
                        </div>

                        <div class="form-group row clearfix">
                            <div class="col-md-4">
                                <label class="small">Documento identificador <span></span></label>
                                <input type="text" disabled="disabled" class="form-control" name="nu_cpf_cnpj_vendedor">
                            </div>
                            <div class="col-md-8">
                                <label class="small"><span>Nome</span></label>
                                <input type="text" placeholder="" class="form-control" name="no_vendedor" maxlength="80">
                            </div>
                        </div>

                        <div class="form-group row clearfix">
                            <div class="col-md-8">
                                <label class="small">E-mail</label>
                                <input type="text" placeholder="" class="form-control" name="no_email_vendedor" maxlength="50">
                            </div>
                            <div class="col-md-4 .rg_vendedor" id="div_rg_vendedor">
                                <label class="small">RG <span></span></label>
                                <input type="text"  class="form-control" name="rg_numero_vendedor" id="rg_numero_vendedor" maxlength="14">
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label class="small">Tipo de telefone</label>
                                <select name="id_tipo_telefone_vendedor" class="form-control">
                                    <option value="0">Tipo de telefone</option>
                                    <option value="1">Residencial</option>
                                    <option value="2">Comercial</option>
                                    <option value="3">Celular</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="small">DDD</label>
                                <input type="text" name="nu_ddd_vendedor" class="form-control" data-mask="99" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="small">Número do telefone</label>
                                <div class="input-group">
                                    <input type="text" name="nu_telefone_vendedor" class="form-control" data-mask="99999999?9">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary btn-inline incluir_telefone_vendedor" type="button">
                                            Adicionar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fieldset-group col-md-12">
                        <fieldset>
                            <legend>Telefone(s) selecionados</legend>
                            <div id="telefones_vendedor_escolhidas">

                            </div>
                        </fieldset>
                    </div>
                {{--</fieldset>--}}

            </div>
        </div>
    </div>
    <div id="comprador" class="tab-pane fade">
        {{--DADOS DO COMPRADOR--}}
        <br />
        <div class="col-md-12 fieldset-group clearfix">
            <div class="fieldset-group clearfix">
                {{--<fieldset>
                    <legend>Dados do comprador</legend>--}}
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="radio radio-inline">
                                <input type="radio" value="F" id="tp_pessoa_f_comprador" name="tp_pessoa_comprador">
                                <label class="small" for="tp_pessoa_f_comprador">
                                    CPF
                                </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" value="J" id="tp_pessoa_j_comprador" name="tp_pessoa_comprador">
                                <label class="small" for="tp_pessoa_j_comprador">
                                    CNPJ
                                </label>
                            </div>
                        </div>

                        <div class="form-group row clearfix">
                            <div class="col-md-4">
                                <label class="small">Documento identificador <span></span></label>
                                <input type="text" disabled="disabled" class="form-control" name="nu_cpf_cnpj_comprador">
                            </div>
                            <div class="col-md-8">
                                <label class="small"><span>Nome</span></label>
                                <input type="text" placeholder="" class="form-control" name="no_comprador" maxlength="80">
                            </div>
                        </div>

                        <div class="form-group row clearfix">
                            <div class="col-md-8">
                                <label class="small">E-mail</label>
                                <input type="text" placeholder="" class="form-control" name="no_email_comprador" maxlength="50">
                            </div>
                            <div class="col-md-4 .rg_comprador"  id="rg_comprador">
                                <label class="small">RG </label>
                                <input type="text"  class="form-control" name="rg_numero_comprador" maxlength="14">
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label class="small">Tipo de telefone</label>
                                <select name="id_tipo_telefone_comprador" class="form-control">
                                    <option value="0">Tipo de telefone</option>
                                    <option value="1">Residencial</option>
                                    <option value="2">Comercial</option>
                                    <option value="3">Celular</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="small">DDD</label>
                                <input type="text" name="nu_ddd_comprador" class="form-control" data-mask="99" placeholder="">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="small">Número do telefone</label>
                                <div class="input-group">
                                    <input type="text" name="nu_telefone_comprador" class="form-control" data-mask="99999999?9">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary btn-inline incluir_telefone_comprador" type="button">
                                            Adicionar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fieldset-group col-md-12">
                        <fieldset>
                            <legend>Telefone(s) selecionados</legend>
                            <div id="telefones_comprador_escolhidas">

                            </div>
                        </fieldset>
                    </div>
                {{--</fieldset>--}}
            </div>
        </div>
    </div>
    <div id="veiculo" class="tab-pane fade">
        {{--DADOS DO VEICULO--}}
        <br />
        <div class="col-md-12 fieldset-group clearfix">
            <div class="fieldset-group clearfix">

                {{--<fieldset>
                    <legend>Dados do veículo</legend>--}}
                    <div class="form-group row clearfix">
                        <div class="col-md-4">
                            <label class="small">Placa</label>
                            <input type="text" name="placa" id="placa" class="form-control placa">
                        </div>
                        <div class="col-md-4">
                            <label class="small">Nº do CRV</label>
                            <input type="text" name="crv" id="crv" class="form-control crv">
                        </div>
                        <div class="col-md-4">
                            <label class="small">Nº Chassi</label>
                            <input type="text" name="chassi" id="chassi" class="form-control chassi">
                        </div>
                    </div>

                    <div class="form-group row clearfix">
                        <div class="col-md-4">
                            <label class="small">Nº Renavam</label>
                            <input type="text" name="renavam" id="renavam" class="form-control renavam">
                        </div>
                        <div class="col-md-4">
                            <label class="small">Cor</label>
                            @if(count($todas_cores)>0)
                                <select id="id_cor_veiculo" name="id_cor_veiculo" class="form-control">
                                    <option value="0">Selecione</option>
                                    @foreach($todas_cores as $cores)
                                        <option value="{{$cores->id_cor_veiculo}}">{{$cores->no_cor_veiculo}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div class="col-md-2">
                            <label class="small">Ano fabricação</label>
                            <select id="nu_ano_fabricacao" name="nu_ano_fabricacao" class="form-control">
                                <option value="0">Selecione</option>
                                @for($i=$ano;$i>=1920;$i--)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>

                        </div>
                        <div class="col-md-2">
                            <label class="small">Ano modelo</label>
                            <select id="nu_ano_modelo" name="nu_ano_modelo" class="form-control">
                                <option value="0">Selecione</option>
                                @for($i=$ano+1;$i>=1920;$i--)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="form-group row clearfix">
                        <div class="col-md-4">
                            <label class="small">Marca</label>
                            @if(count($todas_marcas)>0)
                                <select id="id_marca_veiculo" name="id_marca_veiculo" class="form-control">
                                    <option value="0">Selecione</option>
                                    @foreach($todas_marcas as $marcas)
                                        <option value="{{$marcas->id_marca_veiculo}}">{{$marcas->no_marca_veiculo}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label class="small">Modelo</label>
                            @if(count($todas_cores)>0)
                                <select id="id_modelo_veiculo" name="id_modelo_veiculo" class="form-control" disabled>
                                    <option value="0">Selecione uma marca</option>
                                </select>
                            @endif
                        </div>
                    </div>
                {{--</fieldset>--}}
            </div>
        </div>
    </div>
    <div id="arquivos" class="tab-pane fade">
        <br />
        <fieldset>
            <legend>Documentos do vendedor</legend>
            <div class="fieldset-group clearfix">
                <div id="arquivos_pf_vendedor">
                    <div class="col-md-6" >
                        <label class="small">Arquivo do CPF</label>
                        <div class="cpf_vendedor arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_cpf_vendedor">
                            </span>
                            <a href="#" id="excluir_cpf_vendedor" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <label class="small">Arquivo do RG</label>
                        <div class="rg_vendedor arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_rg_vendedor">
                            </span>
                            <a href="#" id="excluir_rg_vendedor" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                </div>
                <div id="arquivos_pj_vendedor">
                    <div class="col-md-6" >
                        <label class="small">Arquivo do Contrato social</label>
                        <div class="contrato_social_vendedor arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_contrato_social_vendedor">
                            </span>
                            <a href="#" id="excluir_contrato_social_vendedor" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <label class="small">Arquivo do Cartão CNPJ</label>
                        <div class="cartao_cnpj_vendedor arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_crt_cnpj_vendedor">
                            </span>
                            <a href="#" id="excluir_cartao_cnpj_vendedor" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <label class="small">Arquivo do Comprovante de endereço</label>
                    <div class="comprovante_endereco_vendedor arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="button input-group-addon btn-file">
                            <span class="fileinput-new">Selecionar</span>
                            <span class="fileinput-exists">Alterar</span>
                            <input type="file" name="arquivo_comprovante_endereco_vendedor">
                        </span>
                        <a href="#" id="excluir_comprovante_endereco_vendedor" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Documentos do comprador</legend>
            <div class="fieldset-group clearfix">
                <div id="arquivos_pf_comprador">
                    <div class="col-md-6" >
                        <label class="small">Arquivo do CPF</label>
                        <div class="cpf_comprador arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_cpf_comprador">
                            </span>
                            <a href="#" id="excluir_cpf_comprador" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <label class="small">Arquivo do RG</label>
                        <div class="rg_comprador arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_rg_comprador">
                            </span>
                            <a href="#" id="excluir_rg_comprador" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                </div>
                <div id="arquivos_pj_comprador">
                    <div class="col-md-6" >
                        <label class="small">Arquivo do Contrato social</label>
                        <div class="contrato_social_comprador arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_contrato_social_comprador">
                            </span>
                            <a href="#" id="excluir_contrato_social_comprador" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-6" >
                            <label class="small">Arquivo do Cartão CNPJ</label>
                            <div class="cartao_cnpj_comprador fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="button input-group-addon btn-file">
                                    <span class="fileinput-new">Selecionar</span>
                                    <span class="fileinput-exists">Alterar</span>
                                    <input type="file" name="arquivo_cartao_cnpj_comprador" id="arquivo_cartao_cnpj_comprador">
                                </span>
                                <a href="#" id="excluir_cartao_cnpj_comprador" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <label class="small">Arquivo do Comprovante de endereço</label>
                    <div class="comprovante_endereco_comprador arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="button input-group-addon btn-file">
                            <span class="fileinput-new">Selecionar</span>
                            <span class="fileinput-exists">Alterar</span>
                            <input type="file" name="arquivo_comprovante_endereco_comprador">
                        </span>
                        <a href="#" id="excluir_comprovante_endereco_comprador" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Documento do veículo</legend>
            <div class="fieldset-group clearfix">
                <div id="dados_veiculo">
                    <div class="col-md-6" >
                        <label class="small">Arquivo do CRV</label>
                        <div class="crv_arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="button input-group-addon btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Alterar</span>
                                <input type="file" name="arquivo_veiculo_crv" id="arquivo_veiculo_crv">
                            </span>
                            <a href="#" id="excluir_crv_comprador" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <input type="button" value="teste" class=".teste" name="teste" id="teste" />

    </div>
</div>