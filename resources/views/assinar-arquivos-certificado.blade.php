@extends('layout.inicio')

@section('scripts')
	<script type="text/javascript" src="{{asset('lacuna/js/jquery.blockUI.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('lacuna/js/lacuna-web-pki-2.5.0.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
            <div class="box-start col-md-4 col-sm-6">
                <div class="default-login">
                    <h2>Assinar com certificado</h2>
                    <p>Clique em selecionar um certificado abaixo.</p>
                    <form id="signForm" action="finalizar-assinar-arquivos" method="POST">
	                    {{csrf_field()}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                                <div class="menssagem">
		                            @foreach ($errors->all() as $error)
		    	                        <?php
		    	                        switch ($error) {
		    	                        	case '6001':
		    	                        		echo 'O CPF do certificado autenticado não foi encontrado em nosso banco de dados.';
		    	                        		break;
		    	                        	case '6002':
		    	                        		echo 'Um erro ocorreu com seu certificado, por favor, confira se o mesmo ainda é válido.';
		    	                        		break;
											case '6003':
		    	                        		echo 'O CNPJ do certificado autenticado não foi encontrado em nosso banco de dados.';
		    	                        		break;
		    	                        	default:
		    	                        		echo $error;
		    	                        		break;
		    	                        }
		    	                        ?>
		    	                        <br />
        		                    @endforeach
                                </div>
                            </div>
                        @endif
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                        <input type="hidden" name="nome_arquivo" value="<?php echo $nome_arquivo; ?>">
                        <div class="form-group">
                            <label>Arquivo à ser assinado</label>
                            <input type="text" value="{{$nome_arquivo_original}}" class="form-control" disabled />
                        </div>
                        <div class="form-group">
                            <label for="certificateSelect">Selecione um certificado</label>
                            <select id="certificateSelect" class="form-control"></select>
                        </div>
                        <br />
 						<div class="form-group clearfix">
	                        <button id="refreshButton" type="button" class="btn btn-primary pull-left">Atualizar certificados</button>
    	                    <button id="signButton" type="button" class="btn btn-success pull-right">Assinar o arquivo</button>
                        </div>
                    </form>
				</div>
            </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
    <?php
	/*
    <div class="sign-in col-md-12 col-sm-12 text-center">
        <a href="{{url('/')}}" class="btn btn-black btn-lg">Voltar para a tela inicial</a>
    </div>
	*/
	?>
	<script type="text/javascript" src="{{asset('lacuna/js/signature-form.js')}}?v=<?=time();?>"></script>
    <script>
    $(document).ready(function () {
        // Once the page is ready, we call the init() function on the javascript code (see signature-form.js)
        signatureForm.init({
            token: '<?= $token ?>',                     // token acquired from REST PKI
            form: $('#signForm'),                       // the form that should be submitted when the operation is complete
            certificateSelect: $('#certificateSelect'), // the select element (combo box) to list the certificates
            refreshButton: $('#refreshButton'),         // the "refresh" button
            signButton: $('#signButton')                // the button that initiates the operation
        });
    });
</script>
@endsection