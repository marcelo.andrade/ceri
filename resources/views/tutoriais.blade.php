<?php
    $layout = ($acesso == "interno" ) ? 'layout.comum' : 'layout.home';

    $html_token = '<div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-acesso-token">Acesso via Token</a></h3>
                                <p>Acesso a Central Eletrônica de Registro de Imóveis - CERI, pode ser acessada via token veja como é feito.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-acesso-token">VER TUTORIAL</a></p>
                            </div>';
?>

@extends($layout)

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.tutorial.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        <div class="box-start col-md-12">
            <div class="">
                <h2>Tutoriais</h2>
                <p>Saiba mais sobre a Central Eletrônica de Registro de Imóveis do Estado De Mato Grosso do Sul</p><br />

                <div class="row box-tutorial">
                    @if($acesso == "externo" )
                    <div class="row clearfix">
                            {!! $html_token !!}
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqRjRTUm5qNGhrMzg" target="_blank">Manual do Usuário</a></h3>
                                <p>Confira o manual completo de utilização da Central Eletrônica de Registro de Imóveis - CERI</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqRjRTUm5qNGhrMzg" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-cadastro-cartorio">Cadastro de Cartório</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para cadastro do seu Cartório dentro da CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-cadastro-cartorio">VER TUTORIAL</a></p>
                            </div>

                    </div>
                    @endif
                    @if( $tp_usuario == 2 || $tp_usuario == 10)
                        <div class="row clearfix">
                            <br/><br/>
                            {!! $html_token !!}
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqTmM2X0xYWUNqVHc" target="_blank">Manual do Cartório</a></h3>
                                <p>Confira o manual com as funcionalidades específicas para o Cartório utilizar a Central Eletrônica de Registro de Imóveis - CERI</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqTmM2X0xYWUNqVHc" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-notificacao-alienacao-fiduciaria-1">Notificações de Alienação Fiduciária</a></h3>
                                <p>Primeiros passos nas Notificações de Alienação Fiduciária na Central Eletrônica de Registro de Imóveis - CERI. (Parte 1)</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-notificacao-alienacao-fiduciaria-1">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-notificacao-alienacao-fiduciaria-2">Notificações de Alienação Fiduciária</a></h3>
                                <p>Primeiros passos nas Notificações de Alienação Fiduciária na Central Eletrônica de Registro de Imóveis - CERI. (Parte 2)</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-notificacao-alienacao-fiduciaria-2">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-criar-oficio-cartorio">Criar Ofício Cartório</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para Criar Ofício Cartório na Central Eletrônica de Registro de Imóveis - CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-criar-oficio-cartorio">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-responder-oficio">Responder Ofício</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para Respoder Ofício na Central Eletrônica de Registro de Imóveis - CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-responder-oficio">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-responder-penhora">Responder Penhora</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para Respoder Penhora na Central Eletrônica de Registro de Imóveis - CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-responder-penhora">VER TUTORIAL</a></p>
                            </div>
                        </div>
                    @endif

                    @if( $tp_usuario == 4 || $tp_usuario == 7)
                        <div class="row clearfix">
                            <br/><br/>
                            {!! $html_token !!}
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqWWVkQ1VkREpyaU0" target="_blank">Manual do Judiciário</a></h3>
                                <p>Confira o manual com as funcionalidades específicas para o Judiciário utilizar a Central Eletrônica de Registro de Imóveis - CERI</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqWWVkQ1VkREpyaU0" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-incluir-penhora">Incluir Penhora</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para Incluir Penhora na Central Eletrônica de Registro de Imóveis - CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-incluir-penhora">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-criar-oficio-judiciario">Criar Penhora</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para Criar Ofício na Central Eletrônica de Registro de Imóveis - CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-criar-oficio-judiciario">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-youtube-play"></i><br/><br/>
                                <h3><a href="javascript:void(0);" data-toggle="modal" data-target="#tutotial-responder-oficio-judiciario">Responder Ofício</a></h3>
                                <p>Acompanhe através desta vídeo aula o procedimento para Responder Ofício na Central Eletrônica de Registro de Imóveis - CERI.</p>
                                <p><a class="btn btn-primary" data-toggle="modal" data-target="#tutotial-responder-oficio-judiciario">VER TUTORIAL</a></p>
                            </div>
                        </div>
                    @endif

                    @if( $tp_usuario == 8)
                        <div class="row clearfix">
                            <br/><br/>
                            {!! $html_token !!}
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqYTZXbVEwWGZHYjg" target="_blank">Manual da Caixa Econômica Federal</a></h3>
                                <p>Confira o manual com as funcionalidades específicas para a Caixa Econômica Federal utilizar a Central Eletrônica de Registro de Imóveis - CERI</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqYTZXbVEwWGZHYjg" target="_blank">VER TUTORIAL</a></p>
                            </div>
                        </div>
                    @endif


                    @if( $tp_usuario == 3)
                        <div class="row clearfix">
                            <br/><br/>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqYnJYLWl2Sm9CcFk" target="_blank">Canal de Contato</a></h3>
                                <p>Para dar maior comodidade aos seus usuários, a Central disponibiliza um canal de comunicação através de um formulário de “Contato”.</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqYnJYLWl2Sm9CcFk" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqUWp3c2hESlRxeXM" target="_blank">Compra de Crédito</a></h3>
                                <p>Ao clicar em “Comprar créditos” o usuário poderá adquirir seus créditos com pagamento no cartão de créditos ou débito em conta.</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqUWp3c2hESlRxeXM" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqTG1yMWxfWDlQMGM" target="_blank">Emissão de Certidões</a></h3>
                                <p>Na página de emissão de Certidões, o usuário poderá solicitar a emissão de uma Certidão clicando no botão “Nova certidão”.</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqTG1yMWxfWDlQMGM" target="_blank">VER TUTORIAL</a></p>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <br/><br/>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqN2N6VXUzX2dLWXc" target="_blank">Emissão de Relatórios</a></h3>
                                <p>O usuário (cliente) poderá emitir relatórios com suas “Movimentações financeiras”. A CERI disponibilizará um filtro para que o usuário possa escolher os parâmetros da pesquisa.</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqN2N6VXUzX2dLWXc" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqMU9ISnRjUnptMEk" target="_blank">Pesquisas eletrônicas.</a></h3>
                                <p>Na página de Pesquisa Eletrônica, o usuário poderá solicitar uma nova pesquisa clicando no botão “Nova pesquisa”. A Central exibirá uma página para que seja informado os parâmetros da pesquisa.</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqMU9ISnRjUnptMEk" target="_blank">VER TUTORIAL</a></p>
                            </div>
                            <div class="col-sm-4" align="center">
                                <i class="fa fa-file-text"></i><br/><br/>
                                <h3><a href="https://drive.google.com/open?id=0Bx-0kITATZjqUjZEcE9iUFJNUUk" target="_blank">Tela Inicial.</a></h3>
                                <p>Ao se autenticar na Central o usuário acessará a página inicial. Nessa página o usuário terá a opção de solicitar pesquisas eletrônicas, emissão de certidões matrículas eletrônicas, emissão de relatórios e compra de créditos.</p>
                                <p><a class="btn btn-primary" href="https://drive.google.com/open?id=0Bx-0kITATZjqUjZEcE9iUFJNUUk" target="_blank">VER TUTORIAL</a></p>
                            </div>
                        </div>

                    @endif


                </div>
            </div>
        </div>
    </div>


    <div id="tutotial-cadastro-cartorio" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cadastro de Cartório<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-cadastro-cartorio" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-acesso-token" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Acesso via token<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-acesso-token" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-notificacao-alienacao-fiduciaria-1" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Notificação de Alienação Fiduciária<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-notificacao-alienacao-fiduciaria-1" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-notificacao-alienacao-fiduciaria-2" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Notificação de Alienação Fiduciária<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-notificacao-alienacao-fiduciaria-2" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-criar-oficio-cartorio" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Criar Ofício Cartório<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-criar-oficio-cartorio" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-responder-oficio" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Responder Ofício<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-responder-oficio" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-responder-penhora" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Responder Penhora<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-responder-penhora" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-incluir-penhora" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Incluir Penhora<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-incluir-penhora" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-responder-oficio-judiciario" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Responder Ofício<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-responder-oficio-judiciario" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="tutotial-criar-oficio-judiciario" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Responder Ofício<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div id="conteudo-criar-oficio-judiciario" style="display: none"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection