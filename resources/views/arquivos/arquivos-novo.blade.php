<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<input type="hidden" name="token" value="{{$request->token}}" />
<input type="hidden" name="id_tipo_arquivo_grupo_produto" value="{{$request->id_tipo_arquivo_grupo_produto}}" />
<input type="hidden" name="id_flex" value="{{$request->id_flex}}" />
<input type="hidden" name="protocolo" value="{{$request->protocolo}}" />
<input type="hidden" name="index_arquivos" value="{{$request->index_arquivos}}" />
<div class="fieldset-group">
    <div class="progresso-upload progress" style="display:none">
    	<div class="progress-bar" style="width: 0%;">0%</div>
    </div>
    <label class="arquivo-upload" for="arquivo-upload">
        <span>
            <figure>
                <i class="fa fa-upload"></i>
            </figure>
            <h4>Selecionar arquivo</h4>
        </span>
        <input type="file" name="no_arquivo" id="arquivo-upload" data-idtipoarquivo="{{$request->id_tipo_arquivo_grupo_produto}}" data-idflex="{{$request->id_flex}}">
    </label>
</div>
<div class="msg-arquivo fieldset-group" style="display:none">
    <div class="alert alert-warning single">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem"></div>
    </div>
</div>