<div class="fieldset-group">
    <div class="panel table-rounded">
        <table id="certificado-detalhes" class="table table-striped table-bordered small">
            <tr>
                <td>Nome:</td>
                <td>{{$arquivo_grupo_produto->usuario_certificado->no_comum}}</td>
            </tr>
            @if($arquivo_grupo_produto->usuario_certificado->no_email)
                <tr>
                    <td>E-mail:</td>
                    <td>{{$arquivo_grupo_produto->usuario_certificado->no_email}}</td>
                </tr>
            @endif
            <tr>
                <td>CPF/CNPJ:</td>
                <td>{{$arquivo_grupo_produto->usuario_certificado->nu_cpf_cnpj}}</td>
            </tr>
            <tr>
                <td>Tipo Certificado:</td>
                <td>{{$arquivo_grupo_produto->usuario_certificado->tp_certificado}}</td>
            </tr>
            <tr>
                <td>Autoridade Raiz:</td>
                <td>{{$arquivo_grupo_produto->usuario_certificado->no_autoridade_raiz}}</td>
            </tr>
            <tr>
                <td>Autoridade Certificadora:</td>
                <td>{{$arquivo_grupo_produto->usuario_certificado->no_autoridade_certificadora}}</td>
            </tr>
            <tr>
                <td>Válido de:</td>
                <td>{{Carbon\Carbon::parse($arquivo_grupo_produto->usuario_certificado->dt_validade_ini)->format('d/m/Y')}}</td>
            </tr>
            <tr>
                <td>Valido até:</td>
                <td>{{Carbon\Carbon::parse($arquivo_grupo_produto->usuario_certificado->dt_validade_fim)->format('d/m/Y')}}</td>
            </tr>
        </table>
    </div>
</div>