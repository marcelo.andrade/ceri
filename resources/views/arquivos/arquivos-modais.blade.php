<div id="novo-arquivo" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar arquivo</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"><form name="form-novo-arquivo" method="post" action="" class="clearfix"></form></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="enviar-arquivo btn btn-success">Enviar arquivo</button>
            </div>
        </div>
    </div>
</div>
<div id="visualizar-arquivo" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Arquivo - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <a id="arquivo-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="visualizar-certificado" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Certificado do arquivo - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="nova-assinatura" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assinar arquivo(s)</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="assinar-arquivos btn btn-info" id="refreshButton">Atualizar certificados</button>
                <button type="button" class="assinar-arquivos btn btn-success" id="signButton">Assinar arquivos</button>
            </div>
        </div>
    </div>
</div>