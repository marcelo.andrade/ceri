@extends('layout.inicio')

@section('scripts')
@endsection

@section('content')
    <figure><img src="{{asset('images/logo06.png')}}" alt="CERI" class="center-block"/></figure>
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
        <div class="box-start col-md-4 col-sm-6">
            <div class="default-login">
                @if (session('status')=='3000')
                    <div class="single alert alert-success">
                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                        <div class="menssagem">
                            A confirmação foi enviada novamente com sucesso.<br /><br />
                            <a href="{{URL::to('/')}}" class="btn btn-black">Acessar o sistema</a>
                        </div>
                    </div>
                @else
                    <h2>Reenviar confirmação</h2>
                    <p>Digite seu e-mail para receber novamente.</p>
                    <form name="form-reenviar-confirmacao" method="post" action="reenviar-confirmacao">
    					{{csrf_field()}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                                <div class="menssagem">
    	                            @foreach ($errors->all() as $error)
                                        <?php
                                        switch ($error) {
                                            case '3001':
                                                echo 'O e-mail digitado não foi encontrado.';
                                                break;
                                            case '3002':
                                                echo 'O e-mail digitado já foi confirmado. <br /><br >';
                                                echo '<a href="'.URL::to('/').'" class="btn btn-black">Acessar o sistema</a>';
                                                break;
                                            default:
                                                echo $error;
                                                break;
                                        }
                                        ?>
                                        <br />
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="input-group">
    						<span class="input-group-addon" id="addon-mail"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="usuario" class="form-control" placeholder="E-mail" value="{{old('usuario')}}" />
                        </div>
                        <input type="submit" class="access btn btn-primary btn-block" value="Reenviar" />
                    </form>
                @endif
            </div>
        </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
    @if (!session('status'))
        <div class="sign-in col-md-12 col-sm-12 text-center">
            <a href="{{url('/')}}" class="btn btn-black btn-lg">Voltar</a>
        </div>
    @endif
@endsection