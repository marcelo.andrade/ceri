@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.exportar-fase-caixa.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
<div class="container">

    @if(!in_array($class->id_tipo_pessoa,array(9,13)))
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Arquivo de Fases <span class="small">/ Exportar arquivos</span></h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Arquivos</legend>
                            <div id="arquivo" class="col-md-12">
                                <button type="button" class="btn btn-success gerar-arquivo" >Gerar arquivo</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading gradient01">
            <h4>Arquivos de fase exportados <span class="small">/ Exportar arquivos</span></h4>
        </div>
        <div id="filtro-certidao" class="panel-body">
            <div class="panel table-rounded">
                <table id="pedidos-pendentes" class="table table-striped table-bordered small">
                    <thead>
                    <tr class="gradient01">
                        <th>Protocolo</th>
                        <th>Data da importação</th>
                        <th>Nome do arquivo</th>
                        <th>Registros processados</th>
                        <th width="12%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($todas_arquivo_xml)>0)
                        @foreach ($todas_arquivo_xml as $arquivo)
                            <tr class="">
                                <td>{{$arquivo->protocolo}}</td>
                                <td>{{$arquivo->dt_arquivo}}</td>
                                <td>{{$arquivo->no_arquivo}}</td>
                                <td>{{$arquivo->nu_registro_processados}}</td>
                                <td class="options">
                                    @if($arquivo->in_assinatura_digital=='S')
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-assinado="0" data-target="#download-arquivo" data-idarquivo="{{$arquivo->id_alienacao_arquivo_xml}}" data-noarquivo="{{$arquivo->no_arquivo}}">Download</button>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-primary dropdown-toggle " data-toggle="dropdown" >
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"  data-toggle="modal" data-target="#visualizar-assinatura" data-idarquivo="{{$arquivo->id_alienacao_arquivo_xml}}" data-noarquivo="{{$arquivo->no_arquivo}}">Visualizar assinatura</a></li>
                                                    <li><a href="#"  data-toggle="modal" data-assinado="1" data-target="#download-arquivo" data-idarquivo="{{$arquivo->id_alienacao_arquivo_xml}}" data-noarquivo="{{$arquivo->no_arquivo}}">Arquivo assinado</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    @else
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#assinar-xml" data-idarquivo="{{$arquivo->id_alienacao_arquivo_xml}}" data-noarquivo="{{$arquivo->no_arquivo}}" {{(in_array($class->id_tipo_pessoa,array(9,13))?'disabled="disabled"':'')}}>Assinar arquivo</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                <div class="single alert alert-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    <div class="mensagem">
                                        Nenhum registro foi importado.
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="assinar-xml" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assinar XML</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-info" id="refreshButton">Atualizar certificados</button>
                <button type="button" class="btn btn-success" id="signButton">Assinar xml</button>
            </div>
        </div>
    </div>
</div>
<div id="visualizar-assinatura" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assinatura do arquivo - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="download-arquivo" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Arquivo - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
@endsection