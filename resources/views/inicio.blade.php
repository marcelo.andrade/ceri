@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.inicio.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<?php
	if (array_intersect(array(2,8,9,13),$class->modulos)) {
	    $alienacao_regras = $alienacao->regras_filtros()['regras'];
		$alienacao_titulos = $alienacao->regras_filtros()['titulos'];

		$alienacoes_total  = $alienacao->total_alerta($alienacao_regras,'total');
	}
	?>
	<div class="container">
    	<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading gradient01">
                        <h4>Visão geral</h4>
                    </div>
                    <div class="panel-body">
                    	<div class="col-md-12">
                        	Olá <b>{{Auth::User()->no_usuario}}</b>, seja bem vindo!
                       	</div>
                        <br /><br />
                        @if (array_intersect(array(2,3,4,5,6,7,11,13),$class->modulos))
	                        <div class="col-md-3">
		                        <a href="{{URL::to('/servicos/pesquisa-eletronica')}}" class="notificacao-inicial text-center">
		                        	@if(isset($notificacoes[1]))
		                        		<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$notificacoes[1]->total}} {{($notificacoes[1]->total>1?'notificações':'notificação')}}">{{$notificacoes[1]->total}}</span>
		                        	@else
		                        		<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
		                        	@endif
		                        	<i class="fa fa-search" aria-hidden="true"></i>
		                        	<h3>Pesquisa eletrônica</h3>
		                        </a>
		                    </div>
		                @endif
		                @if (array_intersect(array(2,3,4,5,6,7,11,13),$class->modulos))
	                        <div class="col-md-3">
		                        <a href="{{URL::to('/servicos/certidao')}}" class="notificacao-inicial text-center">
		                        	@if(isset($notificacoes[2]))
		                        		<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$notificacoes[2]->total}} {{($notificacoes[2]->total>1?'notificações':'notificação')}}">{{$notificacoes[2]->total}}</span>
		                        	@else
		                        		<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
		                        	@endif
		                        	<i class="fa fa-file-text-o" aria-hidden="true"></i>
		                        	<h3>Certidão</h3>
		                        </a>
		                    </div>
	                    @endif
	                    @if (array_intersect(array(2,4,6,7,11),$class->modulos))
		                    <div class="col-md-3">
		                        <a href="{{URL::to('/servicos/certidao-matricula')}}" class="notificacao-inicial text-center">
		                        	@if(isset($notificacoes[6]))
		                        		<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$notificacoes[6]->total}} {{($notificacoes[6]->total>1?'notificações':'notificação')}}">{{$notificacoes[6]->total}}</span>
		                        	@else
		                        		<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
		                        	@endif
		                        	<i class="fa fa-certificate" aria-hidden="true"></i>
		                        	<h3>Matrícula digital</h3>
		                        </a>
		                    </div>
	                    @endif
	                    @if (array_intersect(array(2,4,5,6,7,11),$class->modulos))
		                    <div class="col-md-3">
		                        <a href="{{URL::to('/servicos/protocolo')}}" class="notificacao-inicial text-center">
		                        	@if(isset($notificacoes[5]))
		                        		<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$notificacoes[5]->total}} {{($notificacoes[5]->total>1?'notificações':'notificação')}}">{{$notificacoes[5]->total}}</span>
		                        	@else
		                        		<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
		                        	@endif
		                        	<i class="fa fa-envelope-o" aria-hidden="true"></i>
		                        	<h3>Protocolo eletrônico</h3>
		                        </a>
		                    </div>
	                    @endif
	                    @if (array_intersect(array(2,4,11),$class->modulos))
		                    <div class="col-md-3">
		                        <a href="{{URL::to('/servicos/penhora')}}" class="notificacao-inicial text-center">
		                        	@if(isset($notificacoes[3]))
		                        		<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$notificacoes[3]->total}} {{($notificacoes[3]->total>1?'notificações':'notificação')}}">{{$notificacoes[3]->total}}</span>
		                        	@else
		                        		<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
		                        	@endif
		                        	<i class="fa fa-gavel" aria-hidden="true"></i>
		                        	<h3>Penhora eletrônica de imóveis</h3>
		                        </a>
		                    </div>
	                    @endif
	                    @if (array_intersect(array(2,4,5,6,7,11,13),$class->modulos))
		                    <div class="col-md-3">
		                        <a href="{{URL::to('/servicos/oficio')}}" class="notificacao-inicial text-center">
		                        	@if(isset($notificacoes[9]))
		                        		<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$notificacoes[9]->total}} {{($notificacoes[9]->total>1?'notificações':'notificação')}}">{{$notificacoes[9]->total}}</span>
		                        	@else
		                        		<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
		                        	@endif
		                        	<i class="fa fa-envelope" aria-hidden="true"></i>
		                        	<h3>Ofício eletrônico <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="O alerta contempla (Ofícios Criados, Encaminhados, Respondidos e Inclusão de observações) e pode estar tanto nos Documentos Recebibos ou Enviados"></i></h3>
		                        </a>
		                    </div>
	                    @endif
						@if (array_intersect(array(2),$class->modulos))
							<div class="col-md-3">
								<a href="{{URL::to('/servicos/alienacao-pagamento')}}" class="notificacao-inicial text-center">
									@if($total_pagamentos_pendentes>0)
										<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$total_pagamentos_pendentes}} {{($total_pagamentos_pendentes>1?'notificações':'notificação')}}">{{$total_pagamentos_pendentes}}</span>
									@else
										<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
									@endif
									<i class="fa fa-bell-o" aria-hidden="true"></i>
									<h3>Notificações de pagamento <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="O alerta contempla o envio do comprovante de pagamento feito pela Anoreg"></i></h3>
								</a>
							</div>
						@endif
						@if (array_intersect(array(2,8,9,13),$class->modulos))
							<div class="col-md-3">
								<a href="#" data-toggle="modal" data-target="#notificacoes-alienacao" class="notificacao-inicial text-center">
									@if($alienacoes_total>0)
										<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$alienacoes_total}} {{($alienacoes_total>1?'notificações':'notificação')}}">{{$alienacoes_total}}</span>
									@else
										<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
									@endif
									<i class="fa fa-bell-o" aria-hidden="true"></i>
									<h3>Notificações de alienação fiduciária <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Clique aqui para acessar as notificações detalhadas"></i></h3>
								</a>
							</div>
						@endif
						@if (array_intersect(array(2,8,13),$class->modulos))
							<div class="col-md-3">
								<a href="{{URL::to('/servicos/alienacao/filtrar/observacoes')}}" class="notificacao-inicial text-center">
									@if($total_observacoes>0)
										<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$total_observacoes}} {{($total_observacoes>1?'notificações':'notificação')}}">{{$total_observacoes}}</span>
									@else
										<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
									@endif
									<i class="fa fa-bell-o" aria-hidden="true"></i>
									<h3>Observações <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Clique aqui para acessar as notificações que contem observações"></i></h3>
								</a>
							</div>
						@endif
						@if (array_intersect(array(2,9),$class->modulos))
							<div class="col-md-3">
								<a href="{{URL::to('/servicos/produto-pagamento-emolumento')}}" class="notificacao-inicial text-center">
									@if($total_pagamentos_servicos_pendentes>0)
										<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$total_pagamentos_servicos_pendentes}} {{($total_pagamentos_servicos_pendentes>1?'pagamentos':'pagamento')}}">{{$total_pagamentos_servicos_pendentes}}</span>
									@else
										<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
									@endif
									<i class="fa fa-bell-o" aria-hidden="true"></i>
									<h3>Notificações de pagamentos de serviços <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Clique aqui para acessar as notificações de pagamentos de serviços pendentes de aprovação"></i></h3>
								</a>
							</div>
						@endif
						@if (array_intersect(array(9),$class->modulos))
							<div class="col-md-3">
								<a href="{{URL::to('/servicos/alienacao-pagamento-emolumento')}}" class="notificacao-inicial text-center">
									@if($total_pagamentos_pendentes>0)
										<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="{{$total_pagamentos_pendentes}} {{($total_pagamentos_pendentes>1?'pagamentos':'pagamento')}}">{{$total_pagamentos_pendentes}}</span>
									@else
										<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>
									@endif
									<i class="fa fa-bell-o" aria-hidden="true"></i>
										<h3>Notificações de pagamento <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="O alerta contempla o envio do comprovante de pagamento feito pela Anoreg"></i></h3>
								</a>
							</div>
						@endif
                    </div>
                </div>
            </div>
		</div>
    </div>
    @if (array_intersect(array(2,8,9,13),$class->modulos))
    	<div id="notificacoes-alienacao" class="modal fade" tabindex="-1" role="dialog">
	        <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	                <div class="modal-header gradient01 clearfix">
	                    <h4 class="modal-title pull-left">Notificações de alienação fiduciária</h4>
	                </div>
	                <div class="modal-body clearfix">
	            		<?php
	            		foreach ($alienacao_regras as $key => $grupo) {
	            			$total = $alienacao->total_alerta($grupo,'grupo');
							echo '<div class="col-md-3">';
								echo '<a href="#" data-toggle="modal" data-target="#notificacoes-alienacao-detalhes" data-grupo="'.$key.'" data-titulogrupo="'.$alienacao_titulos[$key]['grupo'].'" class="notificacao-inicial text-center">';
									if($total>0) {
										echo '<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="'.$total.' '.($total>1?'notificações':'notificação').'">'.$total.'</span>';
									} else {
										echo '<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>';
									}
									echo '<i class="fa fa-bell-o" aria-hidden="true"></i>';
									echo '<h4>'.$alienacao_titulos[$key]['grupo'].'</h4>';
								echo '</a>';
							echo '</div>';
						}
						?>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div id="notificacoes-alienacao-detalhes" class="modal fade" tabindex="-1" role="dialog">
	        <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	                <div class="modal-header gradient01 clearfix">
	                    <h4 class="modal-title pull-left"></h4>
	                </div>
	                <div class="modal-body clearfix">
	                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    	<div class="body"></div>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
	                </div>
	            </div>
	        </div>
	    </div>
	@endif
@endsection