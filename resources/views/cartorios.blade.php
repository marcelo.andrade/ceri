@extends('layout.home')

@section('scripts')
@endsection

@section('content')
    <div class="container">
        <h2 class="titulo">
            CARTÓRIOS
            <p class="info">Saiba quem são os cartórios parceiros da CERI</p>
        </h2>

        <table class="table table-striped table-bordered small">
            <thead>
                <tr class="gradient01">
                    <th class="col-md-4">NOME</th>
                    <th>CIDADE</th>
                    <th class="col-md-3">TITULAR</th>
                    <th class="col-md-2">TELEFONE</th>
                    <th>EMAIL</th>
                    <th class="col-md-3">ENDEREÇO</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="http://www.anoregms.org.br/cartorios/cartorio-de-protesto-registro-de-imoveis-e-registro-de-titulos-e-documentos-e-de-pessoa-juridica-24/">SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</a></td>
                    <td>Água Clara</td>
                    <td>ALEXANDRE REZENDE PELLEGRINI</td>
                    <td>(67) 3239-2938</td>
                    <td>arpellegrini@terra.com.br</td>
                    <td>RUA MUNIR TOMÉ, 354 - SÃO JUDAS TADEU</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E DE REGISTRO CIVIL DAS PESSOAS NATURAIS E DE INTERDIÇÕES E TUTELAS</td>
                    <td>Amambaí</td>
                    <td>RAFAEL CABRAL DA COSTA</td>
                    <td>-</td>
                    <td>oficioregistral.amambai@gmail.com</td>
                    <td>RUA DA REPÚBLICA, 3236 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Anastácio</td>
                    <td>LEDA MARIA NOGUEIRA MENDES</td>
                    <td>(67) 3245-2798</td>
                    <td>srianastacio@uol.com.br</td>
                    <td>AV. DA INTEGRAÇÃO, 802</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Anaurilândia</td>
                    <td>JORGE MARCIO MIRANDA LOUREIRO GOMES</td>
                    <td>(67) 3445-1398</td>
                    <td>jmmlg@terra.com.br</td>
                    <td>RUA DOS FUNDADORES, 1048 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Angélica</td>
                    <td>(substituto Jane) ELENA MARIA DE MATOS BARRADAS FELIPPI</td>
                    <td>(67) 3446-1258</td>
                    <td>riangelica@brturbo.com.br</td>
                    <td>AV. 13 DE MAIO, 413, J. FLORES</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Aparecida do Taboado</td>
                    <td>(substituto Janes) MARCIO DE VASCONCELOS MARTINS</td>
                    <td>(67) 3565-1386</td>
                    <td>cartoriomartins.ms@hotmail.com</td>
                    <td>RUA PRESIDENTE DUTRA, 3965</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS</td>
                    <td>Aquidauana</td>
                    <td>REGINA LÚCIA TEIXEIRA CABRAL</td>
                    <td>(67) 3241-2229</td>
                    <td>rlcabral@brturbo.com.br</td>
                    <td>PRAÇA NOSSA SENHORA IMACULADA CONCEIÇÃO, 291</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Bandeirantes</td>
                    <td>ARISTIDES BORGES DE ESQUIVEL</td>
                    <td>(67) 3261-1239</td>
                    <td>ribande@gmail.com ou ribande@uol.com.br</td>
                    <td>RUA PRESIDENTE GETULIO VARGAS, 753</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS ETABELIONATO DE PROTESTO</td>
                    <td>Bataguassu</td>
                    <td>(substituta Roseli) ZÉLIA OLIVEIRA ALVES (interino)</td>
                    <td>(67) 3541-1297</td>
                    <td>cartoriobtgms@bol.com.br</td>
                    <td>AV. RIBAS DO RIO PARDO, 277 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Bataiporã</td>
                    <td>MARCO AURELIO RIBEIRO RAFAEL</td>
                    <td>(67) 3443-2321</td>
                    <td>maurelio99@gmail.com</td>
                    <td>AV. BRASIL, 574</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Bela Vista</td>
                    <td>(Carlos Alberto Mundies - Subistituto) (indicou funcionario Wilian) JOSÉ AVELINO E SILVA</td>
                    <td>(67) 3439-1464</td>
                    <td>cartorio1oficiobv@uol.com.br</td>
                    <td>RUA CORONEL CAMISÃO, 650 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Bonito</td>
                    <td>HÉLIO GONÇALVES DE SENA MADUREIRA</td>
                    <td>(67) 3255-1630</td>
                    <td>cartoriosena@bonitonline.com.br</td>
                    <td>RUA SANTANA DO PARAISO, 688 - CENTRO</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Brasilândia</td>
                    <td>AIMÉE APARECIDA DE SOUZA FERREIRA</td>
                    <td>(67) 3546-1473</td>
                    <td>cartbras@gmail.com</td>
                    <td>RUA VICENTE FERNANDEZ, 1147 - JD.MÃO AMIGA</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Caarapó</td>
                    <td>HELENA DIAS PEREIRA</td>
                    <td>(67) 3453-1081</td>
                    <td>pereira-helena@bol.com.br</td>
                    <td>AV. SETE DE SETEMBRO, 498 - CENTRO - CX POSTAL 67</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E DAS PESSOAS NATURAIS E DE INTERDIÇÕES E TUTELAS</td>
                    <td>Camapuã</td>
                    <td>VALDEVIR ROBERTO ZANARDI</td>
                    <td>(67) 3286-1447</td>
                    <td>ricamapua@uol.com.br</td>
                    <td>RUA PEDRO CELESTINO, 584 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS DA 2ª CIRCUNSCRIÇÃO</td>
                    <td>Campo Grande</td>
                    <td>JUAN PABLO CORREA GOSSWEILER</td>
                    <td>(67) 3306-3260</td>
                    <td>administracao@2ricampogrande.com.br</td>
                    <td>AV. MATO GROSSO, 785 - LOJA 01, 02, 03 E 04</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS DA 1ª CIRCUNSCRIÇÃO</td>
                    <td>Campo Grande</td>
                    <td>-</td>
                    <td>(67) 3321-1828</td>
                    <td>cartorio1oficioregistro@uol.com.br</td>
                    <td>RUA BARÃO DO RIO BRANCO, 1079 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS DA 3ª CIRCUNSCRIÇÃO</td>
                    <td>Campo Grande</td>
                    <td>JOSÉ PAULO BALTAZAR JÚNIOR</td>
                    <td>(67) 3349-0197</td>
                    <td>intimacoes@3ricampogrande.com.br</td>
                    <td>AV. MINISTRO JOÃO ARINOS, 453 - B. CHáCARA CACHOEIRA</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Cassilândia</td>
                    <td>EDIO AMIN</td>
                    <td>(67) 3596-1015</td>
                    <td>edioamin@terra.com.br</td>
                    <td>RUA AMIN JOSE, 417 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Chapadão do Sul</td>
                    <td>PAULA PRADO</td>
                    <td>(67) 3562-7221</td>
                    <td>imoveis@cartoriochapadao.com.br</td>
                    <td>RUA NOVE, 750 - CENTRO</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO DE REGISTRO DE IMÓVEIS DA 1ª CIRCUNSCRIÇÃO</td>
                    <td>Corumbá</td>
                    <td>(gisele substituta) JOSÉ FLAVIO ANDRÉ BOLINI</td>
                    <td>(67) 3232-3800</td>
                    <td>sri.registro@uol.com.br</td>
                    <td>RUA FIRMO DE MATOS, 802- CENTRO</td>
                </tr>
                <tr>
                    <td>5ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS DA 2ª CIRCUNSCRIÇÃO</td>
                    <td>Corumbá</td>
                    <td>FATIMA REGINA DE LIMA MARTINS</td>
                    <td>(67) 3231-5391</td>
                    <td>corumba5oficio@hotmail.com.br</td>
                    <td>RUA 7 DE SETEMBRO, 179 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO REGISTRAL DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Costa Rica</td>
                    <td>VALDEMIR ALVES</td>
                    <td>(67) 3247-1533</td>
                    <td>-</td>
                    <td>RUA HIPOLITO PEREIRA RAMOS, 263 - CENTRO</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td>Coxim</td>
                    <td>JULIO LIMA DE ALMEIDA</td>
                    <td>(67) 3291-8540</td>
                    <td>segundooficiocoxim@hotmail.com</td>
                    <td>RUA FILINTO MULLER, 280 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Deodápolis</td>
                    <td>EREMILTON ALVES SANTANA</td>
                    <td>(67) 3448-1314</td>
                    <td>ere_cartorio@hotmail.com</td>
                    <td>RUA JOSÉ CRISPINIANO DA ROCHA, 642 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO DE TÍTULOS</td>
                    <td>Dois Irmãos do Buriti</td>
                    <td>ANTONIA KATIUSCIA SOARES DO CARMO(Substituta Francisca autorizou thiago a fazer o registro)</td>
                    <td>(67) 3243-1922</td>
                    <td>cartorio2irmaos@yahoo.com.br</td>
                    <td>AV. REGINALDO LEMES DA SILVA, 328 - CENTRO</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS</td>
                    <td>Dourados</td>
                    <td>ALCEU SOARES AGUIAR</td>
                    <td>67-34169247</td>
                    <td>douradoscart1@terra.com.br</td>
                    <td>RUA JOÃO ROSA GOES, 605 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Eldorado</td>
                    <td>(Substituto Alexandre) MARILDA SANTOS DE AVILA</td>
                    <td>067-34731232</td>
                    <td>segundo.oficio.eldorado@hotmail.com</td>
                    <td>RUA SÃO PAULO, 1229 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS</td>
                    <td>Fátima do Sul</td>
                    <td>(O substituto Jose Pereira Indicou o funcionario Willian para fazer o cadastro) CYNTHIA VILANOVA CARVALHO</td>
                    <td>(67) 3467-1597 (67)34672628</td>
                    <td>primeirooficiofs@gmail.com</td>
                    <td>R: MARECHAL RONDON, 1174</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Glória de Dourados</td>
                    <td>ANIZ RASSLAN</td>
                    <td>(67) 3466-1679</td>
                    <td>cartorio1oficio.gd@hotmail.com</td>
                    <td>AV. PRESIDENTE VARGAS, 1549 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Iguatemi</td>
                    <td>PERLA LILIAN DELGADO</td>
                    <td>(67) 3471-1547</td>
                    <td>administracao@servicoregistraliguatemi.com.br</td>
                    <td>RUA WALOSZEK KONRAD, 925 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Inocência</td>
                    <td>(Titular indicou Augusto para realizar o cadastro) MARIZA ALCANTARA DOS SANTOS CARDOSO</td>
                    <td>(67) 3574-1169</td>
                    <td>cartorioinocencia@gmail.com</td>
                    <td>RUA BENEVENUTO GARCIA DIAS, 107 - JARDIM B0M JESUS</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td>Itaporã</td>
                    <td>(Substituto Alessandro) ADALBERTO LUIZ REICHERT</td>
                    <td>(67) 3451-1802</td>
                    <td>cartorioreichert@gmail.com</td>
                    <td>RUA FRANCISCO LEAL DE QUEIROZ, 610 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Itaquiraí</td>
                    <td>(Marco aurelio Substituto) KEYLA CRISTINA ALVES DE SOUZA DE DAVID</td>
                    <td>(67) 3476-2050</td>
                    <td>cartoriosouzadedavid@hotmail.com</td>
                    <td>RUA FRANCISCA MACHADO, 464, CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Ivinhema</td>
                    <td>RICARDO FABRíCIO SEGANFREDO</td>
                    <td>(67) 3442-5516</td>
                    <td>cartorioivinhema@hotmail.com</td>
                    <td>RUA JOÃO FERREIRA BORGES, 270, BAIRRO PIRAVEVê</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Jardim</td>
                    <td>(Substituta Helenice Das Costa Silva) GIL MESSIAS FLEMING</td>
                    <td>(67) 3251-1383</td>
                    <td>jardim1oficio@hotmail.com</td>
                    <td>RUA 14 DE MAIO, 503, CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS E CIVIL DAS PESSOAS NATURAIS E DE INTERDIÇÕES E TUTELAS</td>
                    <td>Maracaju</td>
                    <td>DANIELLY LEITE BARBIERI</td>
                    <td>-</td>
                    <td>registro.maracaju@gmail.com</td>
                    <td>AV. SANTA MARIA, 321 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Miranda</td>
                    <td>MAURíCIO MOREIRA</td>
                    <td>(67) 3242-2689</td>
                    <td>servicoregistralmoreira@hotmail.com</td>
                    <td>RUA BENJAMIN CONSTANT, 571</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Mundo Novo</td>
                    <td>EDILSON NOBRE DE AZEVEDO (Ana substituto)</td>
                    <td>(67) 3474-1582</td>
                    <td>cartoriomn@hotmail.com</td>
                    <td>AV. JK, 1180 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTO E CIVIL DAS PESSOAS JURÍDICAS</td>
                    <td>Naviraí</td>
                    <td>( Gisele Dimel Gomes substituta )ELMA APARECIDA DE SOUZA BOGDAN</td>
                    <td>(67) 3461-1364</td>
                    <td>elmabogdan@hotmail.com</td>
                    <td>RUA DOS JARDINS, 179 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTO E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Nioaque</td>
                    <td>WALDIR VARGAS</td>
                    <td>(67) 3236-1297</td>
                    <td>oficioregistroimoveis@gmail.com</td>
                    <td>AV. XV DE NOVEMBRO, 799 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO DE TÍTULOS</td>
                    <td>Nova Alvorada do Sul</td>
                    <td>TAE SANG LEE</td>
                    <td>(67) 3456-1224</td>
                    <td>taesanglee@gmail.com</td>
                    <td>RUA TARCíLIO BARBOSA, 459, JD. ELDORADO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E CIVIL DAS PESSOAS NATURAIS E DE INTERDIÇÕES E TUTELAS</td>
                    <td>Nova Andradina</td>
                    <td>MARCELO ARTUR MIRANDA CHADA</td>
                    <td>(67) 3441-1305</td>
                    <td>arturchada@hotmail.com</td>
                    <td>RUA WALTER HUBACHER, 1578 - CENTRO</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS</td>
                    <td>Paranaíba</td>
                    <td>(Rogerio Substituto indicou Funcionário (Diemerson) DULCE MARIA RODRIGUES DE MELLO</td>
                    <td>(67) 3668-1633</td>
                    <td>cripbams@terra.com.br</td>
                    <td>RUA VISCONDE DE TAUNAY, 990 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Pedro Gomes</td>
                    <td>ALAN LANZARINI</td>
                    <td>(67) 3230-1316</td>
                    <td>cartorio1pg@gmail.com</td>
                    <td>RUA MANOEL ALVES DE MORAES JUNIOR, Nª 25 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS</td>
                    <td>Ponta Porã</td>
                    <td>NICHOLAS SALLES FERNANDES SILVA TORRES</td>
                    <td>(67) 3431-1255</td>
                    <td>-</td>
                    <td>RUA DOM PEDRO II, 335 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E CIVIL DAS PESSOAS NATURAIS E DE INTERDIÇÕES E TUTELAS</td>
                    <td>Porto Murtinho</td>
                    <td>CARMEN LEÓN (interina)</td>
                    <td>(67) 3287-1393</td>
                    <td>cartorio448@brturbo.com.br</td>
                    <td>RUA DR. CORRêA, 448 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Ribas do Rio Pardo</td>
                    <td>LUCIA HIGA</td>
                    <td>(67) 3238-1100</td>
                    <td>cartorioderibas@gmail.com</td>
                    <td>RUA HORáCIO LEMOS, 111 - CENTRO</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td>Rio Brilhante</td>
                    <td>RICARDO LUIZ DE LIMA TRINDADE</td>
                    <td>(67) 3452-6520</td>
                    <td>primeirooficioriobrilhante@gmail.com</td>
                    <td>RUA DR. JÚLIO SIQUEIRA MAIA, 964, CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Rio Negro</td>
                    <td>AURÉLIO ORTIZ PANIáGUA</td>
                    <td>(67) 3278-1029</td>
                    <td>registrodeimoveisrn@hotmail.com</td>
                    <td>AV. BRASIL, 1005 - CENTRO</td>
                </tr>
                <tr>
                    <td>1ª SERVIÇO NOTARIAL E DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Rio Verde de Mato Grosso</td>
                    <td>(indicou funcionario Adão ) RENATO COSTA ALVES</td>
                    <td>(67) 3292-1351</td>
                    <td>rcostalves@uol.com.br</td>
                    <td>RUA PORFíRIO GONÇALVES,770 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>São Gabriel do Oeste</td>
                    <td>(Substituto Jovenil ) NAURELINA COLMAN SATORRE</td>
                    <td>(67) 3295-1207</td>
                    <td>risgo@terra.com.br</td>
                    <td>RUA BAHIA, 1259 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Sete Quedas</td>
                    <td>(Substituto Edson) DANIEL DE SOUZA</td>
                    <td>(67) 3479-1469</td>
                    <td>cartoriosetequedas@hotmail.com</td>
                    <td>RUA OSVALDO CRUZ, 135 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO</td>
                    <td>Sidrolândia</td>
                    <td>EDVALDO SILVA DE ARRUDA</td>
                    <td>(67) 3272-1787</td>
                    <td>cri@cartoriosidro.com.br</td>
                    <td>RUA SANTA CATARINA, 705 - CENTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E TABELIONATO DE PROTESTO DE TÍTULOS</td>
                    <td>Sonora</td>
                    <td>JÚLIO CÉSAR MATHEUS RUIZ (interino)</td>
                    <td>67) 3254-1453</td>
                    <td>registro1sonora@gmail.com</td>
                    <td>RUA DA ALEGRIA, 124 - CENTTRO</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS, DE TÍTULOS E DOCUMENTOS E CIVIL DAS PESSOAS JURÍDICAS E DAS PESSOAS NATURAIS E DE INTERDIÇÕES E TUTELAS</td>
                    <td>Terenos</td>
                    <td>NAYMI SALLES FERNANDES SILVA TORRES</td>
                    <td>(67) 3246-7982</td>
                    <td>registroterenos@gmail.com</td>
                    <td>RUA DR. FERNANDO CORRêA DA COSTA, 90-B</td>
                </tr>
                <tr>
                    <td>SERVIÇO DE REGISTRO DE IMÓVEIS</td>
                    <td>Três Lagoas</td>
                    <td>(Miriam Classice autorizou a Sabrina) MIRIAM REIS COSTA</td>
                    <td>(67) 3521-2247</td>
                    <td>reiscostaimoveis@gmail.com</td>
                    <td>AV. ANTONIO TRAJANO, 439 - CENTRO</td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection