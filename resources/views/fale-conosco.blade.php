@extends('layout.home')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.fale-conosco.js')}}"></script>
@endsection

@section('content')
	<div class="wrapper row">
        <div class="col-md-2 hidden-xs hidden-sm"></div>
        <div class="box-start col-md-8 col-sm-12 fix-height">
            <div class="default-login">
                <h2>Fale conosco</h2>
                <form class="clearfix" name="form-contato" method="post">
                    <div class="carregando" style="display: none">
                        <div class="text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    </div>
                    <div class="erros alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove "></i>
                        <div class="menssagem"></div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-4">
                            <label>Nome completo</label>
                            <input class="form-control" name="nome" type="text">
                        </div>
                        <div class="col-md-4">
                            <label>E-mail</label>
                            <input class="form-control" name="email" type="text">
                        </div>
                        <div class="col-md-4">
                            <label>Telefone de contato</label>
                            <input class="form-control" name="telefone" type="text" data-mask="(99) 99999999?9">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <label>Área</label>
                            <select class="form-control" name="area" id="area">
                                <option value="0">Selecione</option>
                                <option value="suporte">Suporte</option>
                                <option value="financeiro">Financeiro</option>
                                <option value="reclamacao">Reclamações</option>
                                <option value="sugestao">Sugestões</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <label>Assunto</label>
                            <input class="form-control" value="" name="titulo" id="titulo" type="text">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <label>Descrição</label>
                            <textarea rows="10" class="form-control" name="descricao" id="descricao"></textarea>
                        </div>
                    </div>

                    <!-- CAPTCHA -->
                    {!! NoCaptcha::display(['render' => 'onload', 'data-callback' => 'verifyCallbackContato']) !!}

                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary" value="Enviar solicitação" />
                        </div>
                    </div>
                </form>
			</div>
        </div>
        <div class="col-md-2 hidden-xs hidden-sm"></div>
    </div>
@endsection