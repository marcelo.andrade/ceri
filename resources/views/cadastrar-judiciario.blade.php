@extends('layout.inicio')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.cadastrar.judiciario.js')}}"></script>
@endsection

@section('content')
	<?php
	/*
    <figure><img src="{{asset('images/logo06.png')}}" alt="seri" class="center-block"/></figure>
    <h2 class="text-center">Cadastre-se gratuitamente!</h2>
    <p class="text-center">Cadastre-se e faça parte da Central Eletrônica de Registro de Imóveis como <b>judiciário</b>.</p>
	*/
	?>
    <div class="wrapper row">
        <div class="col-md-3 hidden-xs hidden-sm"></div>
        <div id="register" class="box-start col-md-6 col-sm-12 col-xs-12">
            <div class="default-register clearfix">
				@if (session('status'))
                	@if (session('status')=='SUCESSO')
	                	<div class="alert single alert-success">
    	                	<i class="icon glyphicon glyphicon-ok pull-left"></i>
        	                <div class="menssagem">
                            	<b>Parabéns!</b><br /><br />
                                Seu cadastro foi completado com sucesso. Seu cadastro será liberado após análise interna.<br /><br />
                                <a href="{{url('/')}}" class="btn btn-black">Acessar o sistema</a>
                            </div>
            	        </div>
                    @endif
                @else
                    <h3>Passo <span>1</span> de 5</h3>
                    <p class="step-1">Digite os dados de acesso do usuário principal.</p>
                    <p class="step-2" style="display:none">Digite os dados do juiz.</p>
                    <p class="step-3" style="display:none">Digite os dados da vara.</p>
                    <p class="step-4" style="display:none">Digite o endereço da vara.</p>
                    <p class="step-5" style="display:none">Leia os termos de uso para concluir o cadastro.</p>
                    <form name="form-cadastrar" method="post" action="judiciario">
                        {{csrf_field()}}
                        <input type="hidden" name="hidden_captcha_judiciario">
                        <div class="erros alert alert-danger" style="display:none">
                            <i class="icon glyphicon glyphicon-remove pull-left"></i>
                            <div class="menssagem"></div>
                        </div>
                        <div class="step-1">
                            <div class="form-group">
                            	<label>E-mail</label>
                                <input type="text" name="email_usuario" class="form-control" />
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                	<label>Senha</label>
                                    <input type="password" name="senha_usuario" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                	<label>Confirme a senha</label>
                                    <input type="password" name="senha_usuario2" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group" id="captcha_judiciario">
                                {!! NoCaptcha::display(['render' => 'onload', 'data-callback' => 'verifyCallbackJudiciario']) !!}
                            </div>

                        </div>
						<div class="step-2" style="display:none">
                            <div class="pessoa-fisica">
	                            <input type="hidden" name="tp_pessoa" value="F" />
                                <div class="form-group">
                                	<label>Nome completo</label>
                                    <input type="text" name="no_pessoa" class="form-control" />
                                </div>
                                <div class="form-group row clearfix">
                                    <div class="col-md-4">
                                    	<label>CPF</label>
                                        <input type="text" name="nu_cpf_cnpj" class="form-control" data-mask="999.999.999-99" />
                                    </div>
                                    <div class="col-md-4">
                                    	<label>Gênero</label>
                                        <select name="tp_sexo" class="form-control">
                                            <option value="0">Selecione o gênero</option>
                                            <option value="F">Feminino</option>
                                            <option value="M">Masculino</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                    	<label>Data de nascimento</label>
                                        <input type="text" name="dt_nascimento" class="form-control" data-mask="99/99/9999" />
                                    </div>
                                </div>
                                <div class="form-group row clearfix">
                                    <div class="col-md-5">
                                    	<label>Tipo de telefone</label>
                                        <select name="id_tipo_telefone" class="form-control">
                                            <option value="0">Tipo de telefone</option>
                                            <option value="1">Residencial</option>
                                            <option value="2">Comercial</option>
                                            <option value="3">Celular</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                    	<label>DDD</label>
                                        <input type="text" name="nu_ddd" class="form-control" data-mask="99" />
                                    </div>
                                    <div class="col-md-5">
                                    	<label>Número do telefone</label>
                                        <input type="text" name="nu_telefone" class="form-control" data-mask="99999999?9" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-3" style="display:none">
                            <div class="pessoa-juridica">
                                <div class="form-group row clearfix">
                                    <div class="col-md-6">
                                    	<label>Nome da vara</label>
                                        <input type="text" name="no_vara" class="form-control" />
                                    </div>
                                    <div class="col-md-6">
                                    	<label>Comarca</label>
                                        <select name="id_comarca" class="form-control">
                                            <option value="0">Selecione a comarca</option>
                                            @if(count($todasComarcas)>0)
                                                @foreach($todasComarcas as $comarca)
                                                    <option value="{{$comarca->id_comarca}}">{{$comarca->no_comarca}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row clearfix">
                                    <div class="col-md-12">
                                    	<label>Tipo da vara</label>
                                        <select name="id_vara_tipo" class="form-control">
                                            <option value="0">Selecione o tipo da vara</option>
                                            @if(count($todosVaraTipos)>0)
                                                @foreach($todosVaraTipos as $vara_tipo)
                                                    <option value="{{$vara_tipo->id_vara_tipo}}">{{$vara_tipo->no_vara_tipo}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step-4" style="display:none">
                            <input type="hidden" name="in_digitar_endereco" value="S">
                            <div class="form-group">
                            	<label>CEP</label>
                                <input type="text" name="nu_cep" class="form-control" data-mask="99999-999" />
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-10">
                                	<label>Endereço</label>
                                    <input type="text" name="no_endereco" class="form-control" />
                                </div>
                                <div class="col-md-2">
                                	<label>Número</label>
                                    <input type="text" name="nu_endereco" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                	<label>Bairro</label>
                                    <input type="text" name="no_bairro" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                	<label>Complemento</label>
                                    <input type="text" name="no_complemento" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                	<label>Estado</label>
                                    <select name="id_estado" class="form-control">
                                        <option value="0">Selecione o estado</option>
                                        @if(count($todosEstados)>0)
                                            @foreach($todosEstados as $estado)
                                                <option value="{{$estado->id_estado}}">{{$estado->no_estado}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-6">
                                	<label>Cidade</label>
                                    <select name="id_cidade" class="form-control" disabled>
                                        <option value="0">Selecione uma cidade</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="step-5" style="display:none">
                            <div class="well">
                            	<div class="overflow">
                                	@include('termos-uso')
								</div>
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" name="termos" id="termos" value="S">
                                <label for="termos">
                                    Li e aceito os Termos de Uso
                                </label>
                            </div>
                        </div>
                        <div class="botoes">
                        	<input type="button" class="back btn btn-primary pull-left" style="display:none" value="Voltar" />
                        	<input type="button" class="continue btn btn-success pull-right" value="Continuar" />
                        </div>
                    </form>
            	@endif
            </div>
        </div>
        <div class="col-md-3 hidden-xs hidden-sm"></div>
    </div>
    <?php
	/*
    @if (!session('status'))
	    <div class="sign-in col-md-12 col-sm-12 text-center">
    	    <a href="{{url('/cadastrar')}}" class="btn btn-black btn-lg">Voltar</a>
	    </div>
    @endif
	*/
	?>
@endsection