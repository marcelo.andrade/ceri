@extends('layout.erros')

@section('content')
    <figure><img src="{{asset('images/logo06.png')}}" alt="CERI" class="center-block"/></figure>
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
        <div class="fix-height-group">
            <div class="box-start col-md-4 col-sm-6 fix-height">
                <div class="default-login">
                    <h2>Erro 401</h2>
                    <br />
                    <div class="alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem">
                            Você não tem autorização para acessar esta página. <br /><br />
                            <a href="{{URL::to('/')}}" class="btn btn-black btn-sm">Voltar para a página inicial</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
@endsection