<?php
$pessoa_ativa = Session::get('pessoa_ativa');
$modulos = $pessoa_ativa->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
?>

@extends('layout.comum')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Compras realizadas</h4>
            </div>
            <div class="panel-body">

                <?php
                /*
                @if(!in_array(9,$modulos))
                    <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#nova-compra">
                        Comprar créditos
                    </button>
                @endif
                */
                ?>

                @include('creditos.creditos-historico')
            </div>
        </div>
    </div>

@endsection