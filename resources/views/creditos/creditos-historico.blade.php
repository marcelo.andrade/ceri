<div class="panel table-rounded">
    <table class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th>Protocolo compra</th>

                @if(in_array(9,$modulos))
                <th>Usuário</th>
                @endif

                <th>Data da compra</th>
                <th>Valor</th>
                <th>Status</th>
                <th>Protocolo pedido</th>
                <th>Data pedido</th>
                <th>Produto</th>
            </tr>
        </thead>
        <tbody>
        @if (count($todos_credito)>0)
            @foreach ($todos_credito as $compra)
                <tr>
                    <td>{{$compra->protocolo_compra}}</td>

                    @if(in_array(9,$modulos))
                    <td>{{$compra->usuario_credito->no_usuario}}</td>
                    @endif

                    <td>{{$compra->dt_compra}}</td>
                    <td>{{$compra->va_credito}}</td>
                    <td>{{$compra->situcao_credito->no_situacao_credito}}</td>
                    <td>{{$compra->movimentacao_financeira ? $compra->movimentacao_financeira->pedido->protocolo_pedido : '' }}</td>
                    <td>{{$compra->movimentacao_financeira ? Carbon\Carbon::parse($compra->movimentacao_financeira->pedido->dt_pedido)->format('d/m/Y H:i:s') : '' }}</td>
                    <td>{{$compra->movimentacao_financeira ? $compra->movimentacao_financeira->pedido->produto->no_produto : '' }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhuma compra de crédito foi encontrada.
                        </div>
                    </div>
                </td>
            </tr>
        @endif


        </tbody>
    </table>
</div>
<div align="center">
    {{$todos_credito->fragment('credito-pag')->render()}}
</div>