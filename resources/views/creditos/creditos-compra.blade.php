    <ul class="metodos-pagamento nav nav-tabs">
        <li role="presentation" class="active"><a href="#cartao-credito" class="credito" aria-controls="cartao-credito" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-credit-card"></i> Cartão de crédito</a></li>
        <li role="presentation" class=""><a href="#cartao-credito" class="debito" aria-controls="cartao-credito" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-credit-card"></i> Cartão de débito</a></li>
        <!--<li role="presentation" class=""><a href="#boleto" class="boleto"  aria-controls="boleto" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-barcode"></i> Boleto bancário</a></li>
        <li role="presentation"><a href="#transferencia" aria-controls="transferencia" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-usd"></i> Transferência bancária</a></li>
        -->
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active clearfix" id="cartao-credito">
            <form name="form-cartao-credito" id="form-cartao-credito" method="post" action="" class="clearfix" >
                <input type="hidden" name="id_bandeira" id="id_bandeira" value="" />
                <input type="hidden" name="hidden_captcha_pagamento" id="hidden_captcha_pagamento" value="" />
                <input type="hidden" name="v_id_pedido" id="v_id_pedido" value="" />
                <div class="erros-cartao alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
                <div class="col-md-12">
                    <div class="fieldset-group" style="display:none">
                        <fieldset>
                            <div class="col-md-12 hidden">
                                <legend>Tipo de cartão</legend>
                                <div class="radio radio-inline">
                                    <input type="radio" class="tipoRadio" id="tpcartao" value="1" name="tpcartao" checked="checked">
                                    <label class="" for="matricula">Crédito</label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" class="tipoRadio" id="tpdebito" value="2" name="tpcartao">
                                    <label class="" for="matricula">Débito</label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" class="tipoRadio" id="tpboleto" value="3" name="tpcartao">
                                    <label class="" for="matricula">Boleto</label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="fieldset-group">
                        <fieldset>
                            <legend>Dados da compra</legend>
                            <div class="col-md-12">
                                <label class="small">Valor da compra</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" name="vl_compra" id="vl_compra" class="form-control real" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                                </div>

                            </div>
                        </fieldset>
                    </div>
                    <div class="fieldset-group clearfix">
                        <fieldset class="clearfix">
                            <legend>Dados do cartão / titular</legend>
                            <div class="form-group clearfix">
                                <div class="col-md-12">
                                    <!--<ul class="tipos-cartao list-unstyled">
                                        <li><img src="{{asset('images/icone-visa-01.png')}}" alt="VISA" /></li>
                                        <li><img src="{{asset('images/icone-mastercard-01.png')}}" alt="Master Card" /></li>
                                        <li><img src="{{asset('images/icone-americanexpress-01.png')}}" alt="American Express" /></li>
                                    </ul>-->
                                    <label class="small">Número do cartão</label>
                                    <input type="text" name="nu_cartao_credito" id="nu_cartao_credito" class="form-control" />
                                    <div id="bandeira"></div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-12">
                                    <label class="small">Titular do cartão <small>(Como escrito no cartão)</small></label>
                                    <input type="text" name="titular_cartao" id="titular_cartao" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-6">
                                    <label class="small">Validade</label><small> - mm/aaaa</small>
                                    <input type="text" name="validade_cartao" id="validade_cartao" class="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <label class="small">Cód. de segurança</label>
                                    <input type="text" name="nu_seguranca" id="nu_seguranca" class="form-control" maxlength="4" onkeypress='return SomenteNumero(event)' />
                                </div>
                            </div>
                        </fieldset>

                        <div class="bandeiras_debito fieldset-group clearfix" style="display: none">
                            <fieldset>
                                <legend>Bancos aceitos: </legend>
                                <img src="{{asset('images/bancos_debito.png')}}" alt="Débito"/>
                            </fieldset>

                        </div>
                        <div class="bandeiras_credito fieldset-group clearfix">
                            <fieldset>
                                <legend>Bandeiras: </legend>
                                <img src="{{asset('images/formas_pag_cartao_credito.png')}}" alt="Crédito"/>
                            </fieldset>
                        </div>

                        <!-- CAPTCHA -->
                        <div class="form-group col-md-12 col-md-offset-3" id="captcha_pagamento">
                            {!! NoCaptcha::display(['render' => 'onload', 'data-callback' => 'verifyCallbackPagamento']) !!}
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <div role="tabpanel" class="tab-pane fade" id="boleto">
            <fieldset>
                <legend>Dados da compra</legend>
                <div class="col-md-6">
                    <label class="small">Valor da compra</label>
                    <div class="input-group input-group-sm">
                        <input type="text" name="vl_compra_bol" class="form-control real" />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Endereço de cobrança</legend>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="small">CEP</label>
                            <input type="text" name="nu_cep" class="form-control" data-mask="99999-999" />
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-8">
                                <label class="small">Endereço</label>
                                <input type="text" name="no_endereco" class="form-control" />
                            </div>
                            <div class="col-md-4">
                                <label class="small">Número</label>
                                <input type="text" name="nu_endereco" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="small">Bairro</label>
                            <input type="text" name="no_bairro" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="small">Complemento</label>
                            <input type="text" name="no_complemento" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="small">Estado</label>
                            <select name="id_estado" class="form-control">
                                <option value="0">Selecione</option>
                                @if(count($todos_estados)>0)
                                    @foreach($todos_estados as $estado)
                                        <option value="{{$estado->id_estado}}">{{$estado->no_estado}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="small">Cidade</label>
                            <select name="id_cidade" class="form-control" disabled>
                                <option value="0">Selecione</option>
                            </select>
                        </div>
                </div>
            </fieldset>






        </div>
        <div role="tabpanel" class="tab-pane fade" id="transferencia">
            <fieldset>
                <legend>Dados da compra</legend>
                <div class="col-md-12">
                    <label class="small">Valor da compra</label>
                    <div class="input-group input-group-sm">
                        <input type="text" name="validade_cartao" class="form-control" />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                    </div>
                    <a href="#" class="small">Saiba como decidir um valor</a>
                </div>
            </fieldset>
        </div>
    </div>
<!--</form>-->

{!! NoCaptcha::renderJs() !!}