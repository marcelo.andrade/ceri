@extends('layout.cartao')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.credito.cartao.debito.js?v=3.0.0')}}"></script>
    <script type="text/javascript">
        setTimeout(function(){ atualizaSaldo(); }, 2000);
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading gradient01">
                        <h4>Erro na sua transação</h4>
                    </div>
                    <div class="panel-body">
                        <div style="" class="erro-cartao alert alert-danger">
                            <i class="icon glyphicon glyphicon-remove pull-left"></i>
                            <div class="menssagem">
                                {{ $arrayErro['erro'] }}
                            </div>
                        </div>

                        <input type="button" class="btn btn-lg btn-danger" name="btnfechar-janela" id="btnfechar-janela" value="Fechar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection