@extends('layout.comum')

@section('content')
<div class="container">
    <div class="box-start col-md-12">
        <div class="">
            <h2>Indicadores</h2>
        </div>
        <div id="container-highcharts" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#container-highcharts').highcharts(
        {!! json_encode($highcharts) !!}
    )
});
</script>
@endsection