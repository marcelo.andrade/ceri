@extends('layout.relatorio')
@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.controle.acesso.js')}}?v=<?=time();?>"></script>
@endsection
@section('content')
    <div class="content">
        <div class="text-right">
            <button class="btn btn-success" onclick="print()">
                <i class="glyphicon glyphicon-print"></i> Imprimir relatório
            </button>
        </div>
    </div>
    <br />
    <table id="relatorio" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th>Nº</th>
            <th>Serveintias</th>
            <th>Cidade</th>
            <th colspan="2">Acesso</th>
            <th>Dia</th>
            <th>Mês</th>
        </tr>
        </thead>
        <tbody>
        @if (count($tabela_controle_acesso)>0)
            @foreach ($tabela_controle_acesso as $relatorio)
                <tr>
                    <td>{{$relatorio->nu_ordem}}</td>
                    <td>{{$relatorio->no_serventia}}</td>
                    <td>{{$relatorio->cidade_serventia}}</td>
                    <td>
                        @if ($relatorio->total_acesso >= 1)
                            <li class="text-success glyphicon glyphicon-ok"></li>
                        @else
                            <li class="text-danger glyphicon glyphicon-remove"></li>
                        @endif
                    </td>
                    <td>
                        @if ($relatorio->total_acesso >= 3)
                            <li class="text-success glyphicon glyphicon-ok"></li>
                        @else
                            <li class="text-danger glyphicon glyphicon-remove"></li>
                        @endif
                    </td>
                    <td>{{$relatorio->vi_dia}}</td>
                    <td>{{$relatorio->vs_mes}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum registro foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
@stop