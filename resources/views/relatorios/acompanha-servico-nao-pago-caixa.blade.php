@extends('layout.relatorio')
@section('content')
    <table id="relatorio" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th>Nº</th>
            <th>Protocolo</th>
            <th>Data do pedido</th>
            <th>Pedido</th>
            <th>Valor Aprovado</th>
            <th>Status</th>
            <th>Fase</th>
            <th>Serventia</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $vl_aprovado = 0;
        $qt_registros = 1;
        ?>
        @if (count($movimentacao_financeira)>0)
            @foreach ($movimentacao_financeira as $relatorio)
                @if ($relatorio->tipo != '#SUBTOTAL#')
                    <tr>
                        <td>{{$qt_registros}}</td>
                        <td>{{$relatorio->protocolo_compra}}</td>
                        <td>{{formatar_data_hora($relatorio->data)}}</td>
                        <td>{{$relatorio->pedido}}</td>
                        <td>{{formatar_valor($relatorio->valor)}}</td>
                        <td>{{$relatorio->status}}</td>
                        <td>{{$relatorio->tipo}}</td>
                        <td>{{$relatorio->no_usuario}}</td>
                    </tr>
                    <?php
                    $qt_registros++;
                    ?>
                @else
                    <tr> 
                        <th colspan="3" style="border-left:none;border-right:none"></th> 
						<th style="border-left:none;border-right:none">Total por Serventia</th>
                        <th style="border-left:none;border-right:none">{{formatar_valor($relatorio->valor)}}</th>
                        <th style="border-left:none;border-right:none">Total de Registros: {{$relatorio->status}}</th>
                        <th colspan="2" style="border-left:none;border-right:none"></th>
                    </tr>
                    <?php
                    $vl_aprovado += $relatorio->valor;
                    ?>
                @endif
            @endforeach
        @else
            <tr>
                <td colspan="8">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">Nenhum registro foi encontrado.</div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>

    <table id="relatorio" class="table table-striped table-bordered small tamanho_300">
        <thead>
        <tr align="center">
            <th align="center">
                <div align="center">TOTAL</div>
            </th>
        </tr>
        <tr class="gradient01">
            <th>Valor Aprovado</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{formatar_valor($vl_aprovado)}}</td>
        </tr>
        </tbody>
    </table>
@stop