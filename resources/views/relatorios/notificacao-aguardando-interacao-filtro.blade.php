@extends('layout.comum')
@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.notificacao.aguardando-interacao.js')}}?v=<?=time();?>"></script>
@endsection
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>{{$titulo_relatorio}}</h4>
            </div>
            <div id="filtro-relatorio" class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <form name="form-relatorio-aguardando-notificacao-interacao" id="form-relatorio-aguardando-notificacao-interacao" method="post" action="notificacao-aguardando-interacao" target="_blank">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <fieldset>
                                        <legend>Alçada</legend>
                                        <div class="col-md-12">
                                            <select name="id_alcada" class="form-control pull-left">
                                                <option value="0">Selecione uma alçada</option>
                                                @if(count($alcadas)>0)
                                                    @foreach ($alcadas as $alcada)
                                                        <option value="{{$alcada->id_alcada}}" @if($request->id_alcada==$alcada->id_alcada) selected @endif>{{$alcada->no_alcada}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-4">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="col-md-12">
                                            <select name="id_situacao" class="form-control pull-left">
                                                <option value="0">Todos os status</option>
                                                @if(count($situacoes)>0)
                                                    @foreach ($situacoes as $situacao)
                                                        <option value="{{$situacao->id_situacao_pedido_grupo_produto}}" @if($request->situacao_pendentes==$situacao->id_situacao_pedido_grupo_produto) selected @endif>{{$situacao->no_situacao_pedido_grupo_produto}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-12">
                                    <div class="col-md-4">
                                        <fieldset>
                                            <legend>Cidade</legend>
                                            <div class="col-md-12">
                                                <select name="id_cidade" class="form-control pull-left">
                                                    <option value="0">Selecione uma cidade</option>
                                                    @if(count($cidades)>0)
                                                        @foreach ($cidades as $cidade)
                                                            <option value="{{$cidade->id_cidade}}" @if($request->cidade_pendentes==$cidade->id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset>
                                            <legend>Serventia</legend>
                                            <div class="col-md-12">
                                                <select name="id_serventia" class="form-control pull-left" disabled="">
                                                    <option value="0">Todas as serventias</option>
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>
                            </div>
                        </div>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Fase</legend>
                                    <div class="col-md-12">
                                        <select name="id_fase" class="form-control pull-left filtro_fase">
                                            <option value="0">Todas as fases</option>
                                            @if(count($fases)>0)
                                                @foreach ($fases as $fase)
                                                    <option value="{{$fase->id_fase_grupo_produto}}" @if($request->fase==$fase->id_fase_grupo_produto) selected @endif>{{$fase->no_fase}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Etapa</legend>
                                    <div class="col-md-12">
                                        <select name="id_etapa" class="form-control pull-left filtro_etapa" disabled>
                                            <option value="0">Selecione uma fase</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Ação</legend>
                                    <div class="col-md-12">
                                        <select name="id_acao" class="form-control pull-left filtro_acao" disabled>
                                            <option value="0">Selecione uma etapa</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <br>
                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros" />
                            <input type="button" name="relatorioPesquisar" class="btn btn-success" value="Filtrar pesquisas" />
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>


@endsection