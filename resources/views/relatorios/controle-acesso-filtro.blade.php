@extends('layout.comum')
@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.controle.acesso.js')}}?v=<?=time();?>"></script>
@endsection
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>{{$titulo_relatorio}}</h4>
            </div>
            <fieldset id="filtro-relatorio" class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <form name="form-relatorio-controle-acesso" id="form-relatorio-controle-acesso" method="post" action="controle-acesso" target="_blank">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="fieldset-group clearfix">
                                <div class="col-md-4">
                                    <fieldset class="clearfix">
                                        <legend>Serventias</legend>
                                        <div class="fieldset-group clearfix">
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <input type="radio" name="serventias" id="id_serventias_T" value="T"
                                                           checked/>
                                                    <label for="id_serventias_T">Todas</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="serventias" id="id_serventias_P"
                                                           value="P"/>
                                                    <label for="id_serventias_P">Possuem pedidos</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend>Período</legend>
                                        <div class="col-md-12">
                                            <div class="periodo input-group input-daterange">
                                                <input type="text" class="form-control pull-left" name="dt_inicio"
                                                       rel="periodo" value="{{$request->dt_inicio}}" readonly/>
                                                <span class="input-group-addon small pull-left">até</span>
                                                <input type="text" class="form-control pull-left" name="dt_fim"
                                                       rel="periodo" value="{{$request->dt_fim}}" readonly/>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros"/>
                            <input type="button" name="relatorioPesquisar" class="btn btn-success"
                                   value="Filtrar pesquisas"/>
                        </div>
                    </fieldset>
                </form>
            </fieldset>
        </div>
    </div>
@endsection