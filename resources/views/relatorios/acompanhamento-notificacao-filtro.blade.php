@extends('layout.comum')
@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.acompanhamento.notificacao.js')}}?v=<?=time();?>"></script>
@endsection
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>{{$titulo_relatorio}}</h4>
            </div>
            <div id="filtro-relatorio" class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <form name="form-relatorio-movimentacao-notificacao" id="form-relatorio-movimentacao-notificacao" method="post" action="acompanhamento-notificacao" target="_blank">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-8">
                                <fieldset>
                                    <legend>Serventia</legend>
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend>Cidade</legend>
                                            <div class="col-md-12">
                                                <select name="id_cidade" class="form-control pull-left">
                                                    <option value="0">Selecione uma cidade</option>
                                                    @if(count($cidades)>0)
                                                        @foreach ($cidades as $cidade)
                                                            <option value="{{$cidade->id_cidade}}" @if($request->cidade_pendentes==$cidade->id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend>Serventia</legend>
                                            <div class="col-md-12">
                                                <select name="id_serventia" class="form-control pull-left" disabled="">
                                                    <option value="0">Todas as serventias</option>
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>
                                </fieldset>
                            </div>
                        </div>


                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros" />
                            <input type="button" name="relatorioPesquisar" class="btn btn-success" value="Filtrar pesquisas" />
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>


@endsection