@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.movimentacao.financeira.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>{{$titulo_relatorio}}</h4>
            </div>
            <div id="filtro-relatorio" class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <form name="form-relatorio-movimentacao" id="form-relatorio-movimentacao" method="post" action="movimentacao-financeira" class="clearfix" target="_new">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Período</legend>
                                    <div class="col-md-12">
                                        <div class="periodo input-group input-daterange">
                                            <input type="text" class="data form-control pull-left" name="dt_inicio" rel="periodo" value="{{$request->dt_inicio}}" readonly/>
                                            <span class="input-group-addon small pull-left">até</span>
                                            <input type="text" class="data form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" readonly/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="fieldset-group clearfix">
                                <div class="col-md-2">
                                    <fieldset>
                                        <legend>Saída</legend>
                                        <div class="col-md-12">
                                            <div class="periodo input-group input-daterange">
                                                <select id="tp_saida" name="tp_saida" class="form-control">
                                                    <option value="HTML">HTML</option>
                                                    <option value="PDF">PDF</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>


                        </div>


                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros" />
                            <input type="button" name="relatorioPesquisar" class="btn btn-success" value="Filtrar pesquisas" />
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>


@endsection