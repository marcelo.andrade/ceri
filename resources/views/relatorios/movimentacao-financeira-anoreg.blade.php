@extends('layout.relatorio')
@section('content')
    <div class="content">
        <div class="text-right">
            <form name="salvar-relatorio-excel" action="salvar-relatorio-excel" method="GET" target="_blank">
                <input type="hidden" name="dt_inicio" value="{{$request->dt_inicio}}"/>
                <input type="hidden" name="dt_fim" value="{{$request->dt_fim}}"/>
                <button class="btn btn-success">
                    <i class="glyphicon glyphicon-save-file"></i> Salvar relatório
                </button>
            </form>
        </div>
    </div>
    <br/>
    <table id="relatorio" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th>Nº</th>
            <th>SR</th>
            <th>Agência</th>
            <th>Nº S.S.</th>
            <th>Nº Protocolo</th>
            <th>Seq. Serviço</th>
            <th>Contrato</th>
            <th>Nome Mutuário</th>
            <th>Valor</th>
            <th>TD</th>
            <th>Descrição</th>
            <th>Status</th>
            <th>Andamento atual</th>
        </tr>
        </thead>
        <tbody>
        @if (count($tabela_acomp_pagamento_caixa)>0)
            @foreach ($tabela_acomp_pagamento_caixa as $relatorio)
                <tr>
                    <td>{{$relatorio->nu_ordem}}</td>
                    <td>{{$relatorio->sr}}</td>
                    <td>{{$relatorio->agencia}}</td>
                    <td>{{$relatorio->nss}}</td>
                    <td>{{$relatorio->protocolo_pedido}}</td>
                    <td>{{$relatorio->seq_servico}}</td>
                    <td>{{$relatorio->numero_contrato}}</td>
                    <td>{{$relatorio->no_devedor}}</td>
                    <td>{{$relatorio->va_tx_servico_em_aberto}}</td>
                    <td>{{$relatorio->td}}</td>
                    <td>{{$relatorio->descricao}}</td>
                    <td>{{$model_relatorio->pedido($relatorio->protocolo_pedido)->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    <td>{{$model_relatorio->andamento_atual($model_relatorio->pedido($relatorio->protocolo_pedido)->id_pedido, 'P','T','D')}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="11">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum registro foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
@stop
<script type="text/javascript" src="{{asset('js/jquery.min.js?v=1.12.4')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.movimentacao.financenira.anoreg.js')}}?v=<?=time();?>"></script>