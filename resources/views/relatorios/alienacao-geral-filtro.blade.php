@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.relatorios.alienacao.geral.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Lista de notificações geral <small>/ Relatórios</small></h4>
            </div>
            <div id="filtro-relatorio" class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <form name="form-filtro" id="form-relatorio-alienacao-geral" method="post" action="{{URL::to('relatorios/alienacao-geral/salvar')}}" target="_blank">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-3">
                                <fieldset>
                                    <legend>Período</legend>
                                    <div class="col-md-12">
                                        <div class="periodo input-group input-daterange">
                                            <input type="text" class="form-control pull-left" name="dt_ini" rel="periodo" value="{{$request->dt_ini}}" readonly/>
                                            <span class="input-group-addon small pull-left">até</span>
                                            <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" readonly/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <legend>Protocolo</legend>
                                    <div class="col-md-12">
                                        <div class="protocolo">
                                            <input type="text" name="protocolo" class="form-control numero_protocolo" value="{{$request->protocolo}}" />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <legend>Nº do contrato</legend>
                                    <div class="col-md-12">
                                        <input type="text" name="contrato" class="form-control" value="{{$request->contrato}}" />
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <legend>CPF</legend>
                                    <div class="col-md-12">
                                        <input type="text" name="cpf" class="form-control cpf" value="{{$request->cpf}}" />
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-5">
                                <fieldset>
                                    <legend>Serventia</legend>
                                    <div class="col-md-5">
                                        <fieldset>
                                            <legend>Cidade</legend>
                                            <div class="col-md-12">
                                                <select name="cidade" class="form-control pull-left">
                                                    <option value="0">Selecione uma cidade</option>
                                                    @if(count($cidades)>0)
                                                        @foreach ($cidades as $cidade)
                                                            <option value="{{$cidade->id_cidade}}" @if($request->cidade==$cidade->id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-7">
                                        <fieldset>
                                            <legend>Serventia</legend>
                                            <div class="col-md-12">
                                                <select name="serventia" class="form-control pull-left" @if(count($serventias)<=0) disabled="" @endif>
                                                    <option value="0">Todas as serventias</option>
                                                    @if(count($serventias)>0)
                                                        @foreach ($serventias as $serventia)
                                                            <option value="{{$serventia->id_serventia}}" @if($request->serventia==$serventia->id_serventia) selected @endif>{{$serventia->no_serventia}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="fieldset-group clearfix">
                                <div class="col-md-7">
                                    <fieldset>
                                        <legend>Outros</legend>
                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend>Grupo de alerta</legend>
                                                <div class="col-md-12">
                                                    <select name="grupo[]" class="grupo form-control selectpicker pull-left" multiple data-live-search="true" data-actions-box="true" data-select-all-text="Selecionar todos" data-deselect-all-text="Deselecionar todos" data-count-selected-text="{0} grupo(s) selecionado(s)" data-selected-text-format="count>4" title="Selecione um grupo">
                                                        @if(count($alienacao_regras)>0)
                                                            @foreach ($alienacao_regras as $key => $grupo) {
                                                                <option value="{{$key}}" @if(count($request->grupo)>0) @if(in_array($key,$request->grupo)) selected @endif @endif>{{$alienacao_titulos[$key]['grupo']}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend>Alerta</legend>
                                                <div class="col-md-12">
                                                    <select name="filtro[]" class="filtro form-control selectpicker pull-left" multiple data-live-search="true" data-actions-box="true" data-select-all-text="Selecionar todos" data-deselect-all-text="Deselecionar todos" data-count-selected-text="{0} filtros(s) selecionado(s)" data-selected-text-format="count>4" title="Selecione um alerta" @if(count($request->grupo)<=0) disabled="" @endif>
                                                        @if(count($request->grupo)>0)
                                                            @foreach($request->grupo as $grupo)
                                                                <optgroup label="{{$alienacao_titulos[$grupo]['grupo']}}">
                                                                    @if(isset($alienacao_regras[$grupo]))
                                                                        @if(count($alienacao_regras[$grupo])>0)
                                                                            @foreach ($alienacao_regras[$grupo] as $key => $alerta) {
                                                                                <option value="{{$grupo.'_'.$key}}" @if(count($request->filtro)>0) @if(in_array($grupo.'_'.$key,$request->filtro)) selected @endif @endif>{{$alienacao_titulos[$grupo][$key]}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                </optgroup>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset-group clearfix">
                            <div class="buttons col-md-12 text-right">
                                <input type="submit" class="btn btn-success" value="Salvar em excel" />
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>


@endsection