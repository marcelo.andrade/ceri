@extends('layout.relatorio')
@section('content')
<table id="relatorio" class="table table-striped table-bordered small">
    <thead>
    <tr class="gradient01">
        <th>Nº</th>
        <th>Protocolo</th>
        <th>Data do pedido</th>
        <th>Pedido</th>
        <th>Valor movimento</th>
        <th>Valor</th>
        <th>Status</th>
        <th>Tipo</th>
        <th>Usuário</th>
    </tr>
    </thead>
    <tbody>
	<?php
        $vl_debito      = 0;
        $vl_credito     = 0;
        $qt_registros   = 1;
    ?>
    @if (count($movimentacao_financeira)>0)
        @foreach ($movimentacao_financeira as $relatorio)
            <tr>
                <td>{{$qt_registros}}</td>
                <td>{{$relatorio->protocolo_compra}}</td>
                <td>{{ formatar_data_hora($relatorio->data)}}</td>
                <td>{{$relatorio->pedido}}</td>
                <td>{{ formatar_valor($relatorio->va_movimento)}}</td>
                <td>{{ formatar_valor($relatorio->valor)}}</td>
                <td>{{$relatorio->status}}</td>
                <td>{{$relatorio->tipo}}</td>
                <td>{{$relatorio->no_usuario}}</td>
            </tr>
            <?php
                if ($relatorio->tipo == 'Crédito'){
                    $vl_credito +=  $relatorio->valor;
                }else{
                    $vl_debito  +=  $relatorio->valor;
                }
                $qt_registros++;
            ?>
        @endforeach
    @else
        <tr>
            <td colspan="9">
                <div class="single alert alert-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    <div class="mensagem">
                        Nenhum registro foi encontrado.
                    </div>
                </div>
            </td>
        </tr>
    @endif
    </tbody>
</table>

<table id="relatorio" class="table table-striped table-bordered small tamanho_300">
    <thead>
    <tr align="center">
        <th colspan="2" align="center"><div align="center">TOTAL</div></th>
    </tr>
    <tr class="gradient01">
        <th>CRÉDITO</th>
        <th>DÉBITO</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{formatar_valor($vl_credito)}}</td>
        <td>{{formatar_valor($vl_debito)}}</td>
    </tr>
    </tbody>
</table>

@stop