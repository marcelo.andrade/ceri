@extends('layout.relatorio')
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>{{$titulo_relatorio}}</h4>
            </div>
            <div id="filtro-relatorio" class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <table id="relatorio" class="table table-striped table-bordered small">
                    <thead>
                    <tr class="gradient01">
                        <th>Nº</th>
                        <th>Protocolo</th>
                        <th>Contrato</th>
                        <th>Data importação</th>
                        <th>Serv. ANOREG(Em aberto)</th>
                        <th>Serventia</th>
                        <th>Cidade</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@if (count($tabela_acesso_financeiro)>0)
                        @foreach ($tabela_acesso_financeiro as $relatorio)
                            <tr>
                                <td>{{ $relatorio->dt_movimento}}</td>
                                <td>{{ $relatorio->va_movimento}}</td>
                            </tr>
                        @endforeach
                    @else--}}
                        <tr>
                            <td colspan="7">
                                <div class="single alert alert-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    <div class="mensagem">
                                        Nenhum registro foi encontrado.
                                    </div>
                                </div>
                            </td>
                        </tr>
                    {{--@endif--}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop