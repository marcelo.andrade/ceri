@extends('layout.relatorio')
@section('content')
<table id="relatorio" class="table table-striped table-bordered small">
    <thead>
    <tr class="gradient01">
        <th>Produto</th>
        <th>Emolumento</th>
        <th>FDP (6%)</th>
        <th>FMP(10%)</th>
        <th>FPGE(4%)</th>
        <th>Funjecc(10%)</th>
        <th>ISSQN(5%)</th>
        <th>Taxa Administração</th>
        <th>Taxa Bancária</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @if (count($tabela_preco)>0)
        @foreach ($tabela_preco as $relatorio)
            <tr>
                <td>{{$relatorio->no_produto_item}}</td>
                <td>{{$relatorio->emulumento}}</td>
                <td>{{ formatar_valor($relatorio->va_fdp)}}</td>
                <td>{{ formatar_valor($relatorio->va_fmp)}}</td>
                <td>{{ formatar_valor($relatorio->va_fpge)}}</td>
                <td>{{ formatar_valor($relatorio->va_funjecc)}}</td>
                <td>{{ formatar_valor($relatorio->va_issqn)}}</td>
                <td>{{ formatar_valor($relatorio->va_administracao)}}</td>
                <td>{{ formatar_valor($relatorio->va_bancaria)}}</td>
                <td>{{ formatar_valor($relatorio->va_total)}}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7">
                <div class="single alert alert-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    <div class="mensagem">
                        Nenhum registro foi encontrado.
                    </div>
                </div>
            </td>
        </tr>
    @endif
    </tbody>
</table>
@stop