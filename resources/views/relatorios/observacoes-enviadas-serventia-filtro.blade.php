@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.rel.observacoes.enviadas.serventia.js')}}?v=<?=time();?>"></script>
@stop

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>{{$titulo_relatorio}}<small> / Relatórios</small></h4>
            </div>
            <div class="panel-body">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem" id="menssagem"></div>
                </div>
                <form name="form-filtro-observacao" id="form-filtro-observacao" method="post" action="observacoes-enviadas-serventia" class="clearfix" target="_new">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Observações</legend>
                                    <div class="fieldset-group clearfix">
                                        <div class="radio">
                                            <input type="radio" name="in_leitura" id="in_leitura_T" value="T" checked/>
                                            <label for="in_leitura_T">Todas</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="in_leitura" id="in_leitura_S" value="S"/>
                                            <label for="in_leitura_S">Lidas</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="in_leitura" id="in_leitura_N" value="N"/>
                                            <label for="in_leitura_N">Não lidas</label>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-8">
                                <div id="serventias">
                                    <fieldset>
                                        <legend>Serventia</legend>
                                        <div class="col-md-4">
                                            <label class="small">Cidade</label>
                                            <div class="periodo input-group input-daterange">
                                                <select id="id_cidade" name="cidade" class="form-control pull-left">
                                                    <option value="0">Selecione uma cidade</option>
                                                    @if (count($cidades)>0)
                                                        @foreach($cidades as $cidade)
                                                            <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <label class="small">Serventia</label>
                                            <div class="periodo input-group input-daterange">
                                                <select id="id_serventia" name="serventia" class="form-control pull-left" disabled>
                                                    <option value="0">Todas as serventias</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Período</legend>
                                    <div class="col-md-12">
                                        <div class="periodo input-group input-daterange">
                                            <input type="text" class="form-control pull-left" name="dt_inicio" rel="periodo" value="{{$request->dt_inicio}}" readonly/>
                                            <span class="input-group-addon small pull-left">até</span>
                                            <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" readonly/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset>
                                    <legend>Saída</legend>
                                    <div class="col-md-12">
                                        <div class="periodo input-group input-daterange">
                                            <select id="tp_saida" name="tp_saida" class="form-control">
                                                <option value="HTML">HTML</option>
                                                <option value="PDF">PDF</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros"/>
                            <input type="button" name="pesquisar" id="pesquisar" class="btn btn-success" value="Filtrar observações"/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@stop