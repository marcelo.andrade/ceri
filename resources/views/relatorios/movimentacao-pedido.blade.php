@extends('layout.relatorio')
@section('content')
<table id="relatorio" class="table table-striped table-bordered small">
    <thead>
    <tr class="gradient01">
        <th>Protocolo</th>
        <th>Data do pedido</th>
        {{--<th>Origem pedido</th>--}}
        {{--<th>Destino pedido</th>--}}
        <th>Status</th>
        <th>Produto</th>
        <th>Usuário</th>
        <th>Tipo de usuário</th>
        <th>Valor pedido</th>
        <th>Valor Emulumento</th>
        <th>Valor FDP</th>
        <th>Valor FMP</th>
        <th>Valor FPGE</th>
        <th>Valor Funjec</th>
        <th>Valor Issqn</th>
        {{--<th>Valor Tx Bancária</th>--}}
        <th>Valor Adm</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $va_pedido          = 0;
        $va_emulumento      = 0;
        $va_fdp             = 0;
        $va_fmp             = 0;
        $va_fpge            = 0;
        $va_funjecc         = 0;
        $va_issqn           = 0;
       /* $va_bancaria        = 0;*/
        $va_administracao   = 0;
        $qt_registros       = 0;
    ?>
    @if (count($movimentacao_pedido)>0)
        @foreach ($movimentacao_pedido as $relatorio)

            <tr>
                <td>{{$relatorio->protocolo_pedido}}</td>
                <td>{{formatar_data_hora($relatorio->dt_pedido)}}</td>
                {{--<td>{{$relatorio->no_pedido_origem}}</td>--}}
                {{--<td>{{$relatorio->no_pedido_destino}}</td>--}}
                <td>{{$relatorio->no_situacao_pedido_grupo_produto}}</td>
                <td>{{$relatorio->no_produto}}</td>
                <td>{{$relatorio->no_usuario}}</td>
                <td>{{$relatorio->no_tipo_usuario}}</td>
                <td>{{formatar_valor($relatorio->va_pedido)}}</td>
                <td>{{formatar_valor($relatorio->va_emulumento)}}</td>
                <td>{{formatar_valor($relatorio->va_fdp)}}</td>
                <td>{{formatar_valor($relatorio->va_fmp)}}</td>
                <td>{{formatar_valor($relatorio->va_fpge)}}</td>
                <td>{{formatar_valor($relatorio->va_funjecc)}}</td>
                <td>{{formatar_valor($relatorio->va_issqn)}}</td>
                {{--<td>{{formatar_valor($relatorio->va_bancaria)}}</td>--}}
                <td>{{formatar_valor($relatorio->va_administracao)}}</td>
            </tr>
            <?php
                $va_pedido        += $relatorio->va_pedido;
                $va_emulumento    += $relatorio->va_emulumento;
                $va_fdp           += $relatorio->va_fdp;
                $va_fmp           += $relatorio->va_fmp;
                $va_fpge          += $relatorio->va_fpge;
                $va_funjecc       += $relatorio->va_funjecc;
                $va_issqn         += $relatorio->va_issqn;
                /*$va_bancaria      += $relatorio->va_bancaria;*/
                $va_administracao += $relatorio->va_administracao;
            ?>
            <?php $qt_registros++ ?>
        @endforeach
            <tr>
                <th colspan="6">TOTAL:</th>
                <th>{{formatar_valor($va_pedido)}}</th>
                <th>{{formatar_valor($va_emulumento)}}</th>
                <th>{{formatar_valor($va_fdp)}}</th>
                <th>{{formatar_valor($va_fmp)}}</th>
                <th>{{formatar_valor($va_fpge)}}</th>
                <th>{{formatar_valor($va_funjecc)}}</th>
                <th>{{formatar_valor($va_issqn)}}</th>
               {{-- <th>{{ formatar_valor($va_bancaria)}}</th>--}}
                <th>{{ formatar_valor($va_administracao)}}</th>
            </tr>
    @else
        <tr>
            <td colspan="17">
                <div class="single alert alert-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    <div class="mensagem">
                        Nenhum registro foi encontrado.
                    </div>
                </div>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<h4>Total de registros: {{$qt_registros}}</h4>
@stop
