@extends('layout.relatorio')
@section('content')
    <div class="content">
        <div class="text-right">
            <form name="salvar-relatorio-excel-acomp-notificacao" action="salvar-relatorio-excel-acomp-notificacao" method="GET" target="_blank">
                <input type="hidden" name="id_serventia" value="{{$request->id_serventia}}"/>
                <input type="hidden" name="id_cidade" value="{{$request->id_cidade}}"/>
                <button class="btn btn-success">
                    <i class="glyphicon glyphicon-save-file"></i> Salvar relatório
                </button>
            </form>
        </div>
    </div>
    <br/>

    <table id="relatorio" class="table table-striped table-bordered small">
        <tbody>
        <tr class="gradient01">
            <th colspan="2" rowspan="2">Protocolo</th>
            <th rowspan="2">Serventia</th>
            <th rowspan="2">Cidade</th>
            <th rowspan="2">Alcada</th>
            <th rowspan="2">Fase / Etapa / Ação (próxima) </th>
            <th colspan="2">Aguardando interação</th>
        </tr>
        <tr class="gradient01">
            <th>Dias corridos</th>
            <th>Dias úteis</th>
        </tr>
        @if (count($acompanhamento_notificacao)>0)
            @foreach ($acompanhamento_notificacao as $relatorio)
                <tr>
                    <td>{{$relatorio->nu_ordem}}</td>
                    <td>{{$relatorio->protocolo_pedido}}</td>
                    <td>{{$relatorio->no_serventia}}</td>
                    <td>{{$relatorio->cidade_serventia}}</td>
                    <td>{{$relatorio->no_alcada}}</td>
                    <td>{{$relatorio->fase_etapa_acao_futuro}}</td>
                    <td>{{$relatorio->dia_corrido}}</td>
                    <td>{{$relatorio->dia_util}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum registro foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>




@stop