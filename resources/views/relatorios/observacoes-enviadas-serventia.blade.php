@extends('layout.relatorio')

@section('content')
    <table id="relatorio" class="table table-striped table-bordered small table-responsive">
        <thead>
        <tr class="gradient01">
            <th>Protocolo</th>
            <th>Contrato</th>
            <th>Data do cadastro</th>
            <th>Usuário origem</th>
            <th>Serventia</th>
            <th>Cidade</th>
            <th>Lida</th>
            <th>Usuário leitura</th>
            <th>Observação</th>
        </tr>
        </thead>
        <tbody>
        @if (count($todas_observacoes_enviadas)>0)
            @forelse($todas_observacoes_enviadas as $observacoes_enviadas)
                <tr>
                    <td>{{$observacoes_enviadas->protocolo_pedido}}</td>
                    <td>{{$observacoes_enviadas->numero_contrato}}</td>
                    <td>{{formatar_data($observacoes_enviadas->dt_cadastro)}}</td>
                    <td>{{$observacoes_enviadas->no_usuario_cad}}</td>
                    <td>{{$observacoes_enviadas->no_serventia}}</td>
                    <td>{{$observacoes_enviadas->no_cidade}}</td>
                    <td>{{$observacoes_enviadas->in_leitura == 'S' ? 'Sim' : 'Não'}}</td>
                    <td>{{$observacoes_enviadas->in_leitura == 'N' ? '' : $observacoes_enviadas->no_usuario_leitura}}</td>
                    <td>{{$observacoes_enviadas->de_observacao}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhum registro foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
        @endif
        </tbody>
    </table>
@stop