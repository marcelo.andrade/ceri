@extends('layout.relatorio')
@section('content')
    <div class="content">
        <div class="text-right">
            <a href="salvar-dados-cadastrais" target="_blank" class="btn btn-success">
                <i class="glyphicon glyphicon-save-file"></i> Salvar relatório
            </a>
        </div>
    </div>
    <br />
    <table id="relatorio" class="table table-striped table-bordered small">
        <thead>
        <tr>
            <th align="right" valign="middle" colspan="6">Serventia</th>
            <th colspan="4">Expediente</th>
            <th colspan="5">Endereços</th>
            <th colspan="8">Dados Bancários</th>
            <th colspan="4">CNS</th>
        <tr>
        <tr class="gradient01">
            {{--serventia--}}
            <th>Nº</th>
            <th>Nome</th>
            <th>Cidade</th>
            <th>Tipo de serventia</th>
            <th>Oficial</th>
            <th>Substituto</th>
            {{--expendiente--}}
            <th>Início expediente</th>
            <th>Início Almoço</th>
            <th>Término Almoço</th>
            <th>Término expediente</th>
            {{--endereç os--}}
            <th>Endereço</th>
            <th>Complemento</th>
            <th>Bairro</th>
            <th>Cidade</th>
            <th>UF</th>
            {{--dados bancarios--}}
            <th>Nome Banco</th>
            <th>Cód. Banco</th>
            <th>Cód. Agência</th>
            <th>DV Agência</th>
            <th>Nª Conta</th>
            <th>DV Conta</th>
            <th>Tipo</th>
            <th>Variação</th>
            <th>Tipo Pessoa</th>
            <th>CPF/CNPJ</th>
            {{--CNS --}}
            <th>Código</th>
            <th>DV</th>


        </tr>
        </thead>
        <tbody>
        <?php
        $count = 1;
        ?>
        @if (count($serventias)>0)
            @foreach ($serventias as $relatorio)
                <tr>
                    <td>{{$count}}</td>

                    <td>{{$relatorio->no_serventia}}</td>
                    <td>{{$relatorio->no_cidade}}</td>
                    <td>{{$relatorio->no_tipo_serventia}}</td>
                    <td>{{$relatorio->no_oficial}}</td>
                    <td>{{$relatorio->no_substituto}}</td>

                    <td>{{$relatorio->hora_inicio_expediente}}</td>
                    <td>{{$relatorio->hora_inicio_almoco}}</td>
                    <td>{{$relatorio->hora_termino_almoco}}</td>
                    <td>{{$relatorio->hora_termino_expediente}}</td>

                    <td>{{$relatorio->no_endereco}}</td>
                    <td>{{$relatorio->no_complemento}}</td>
                    <td>{{$relatorio->no_bairro}}</td>
                    <td>{{$relatorio->no_cidade}}</td>
                    <td>{{$relatorio->uf}}</td>

                    <td>{{$relatorio->no_banco}}</td>
                    <td>{{$relatorio->codigo_banco}}</td>
                    <td>{{$relatorio->nu_agencia}}</td>
                    <td>{{$relatorio->nu_dv_agencia}}</td>
                    <td>{{$relatorio->nu_conta}}</td>
                    <td>{{$relatorio->nu_dv_conta}}</td>
                    <td>{{$relatorio->tipo_conta}}</td>
                    <td>{{$relatorio->nu_variacao}}</td>
                    <td>{{$relatorio->in_tipo_pessoa_conta}}</td>
                    <td>{{$relatorio->nu_cpf_cnpj_conta}}</td>

                    <td>{{$relatorio->codigo_cns}}</td>
                    <td>{{$relatorio->dv_codigo_cns}}</td>
                </tr>
                <?php
                $count++;
                ?>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum registro foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
    </table>
@stop

