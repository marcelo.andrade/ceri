@extends('layout.inicio')

@section('scripts')
@endsection

@section('content')
	<?php
	/*
    <figure><img src="{{asset('images/logo06.png')}}" alt="CERI" class="center-block"/></figure>
	*/
	?>
    <div class="wrapper row">
        <div class="col-md-2 hidden-xs hidden-sm"></div>
        <div class="fix-height-group">
            <div class="box-start col-md-4 col-sm-6 fix-height">
                <div class="default-login">
                    <h2>Acessar</h2>
                    <p>Acesse utilizando seu cadastro.</p>
                    <form name="form-login" method="post" action="acessar">
						{{csrf_field()}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                                <div class="menssagem">
		                            @foreach ($errors->all() as $error)
		    	                        <?php
		    	                        switch ($error) {
		    	                        	case '1001':
		    	                        		echo 'Os dados digitados são inválidos.';
		    	                        		break;
		    	                        	case '1002':
		    	                        		echo 'O seu e-mail ainda não foi confirmado. <br /><br />';
		    	                        		echo '<a href="'.URL::to('/cadastrar/reenviar-confirmacao').'" class="btn btn-black">Reenviar confirmação</a>';
		    	                        		break;
											case '1003':
		    	                        		echo 'O seu cadastro está em análise. Por favor, aguarde.';
		    	                        		break;
											case '1004':
		    	                        		echo 'O seu cadastro foi desabilitado.';
		    	                        		break;
                                            case '1005':
                                                echo 'O seu usuário não possui vínculos, entre em contato com o administrador.';
                                                break;
		    	                        	default:
		    	                        		echo $error;
		    	                        		break;
		    	                        }
		    	                        ?>
		    	                        <br />
        		                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="form-group input-group">
							<span class="input-group-addon" id="addon-mail"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="usuario" class="form-control" placeholder="E-mail" value="{{old('usuario')}}" />
                        </div>
                        <div class="form-group input-group">
							<span class="input-group-addon" id="addon-password"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="senha_usuario" class="form-control" placeholder="Senha" />
                        </div>
                        <a href="acessar/lembrar-senha" class="forgot-password">
                            <i class="glyphicon glyphicon-lock"></i>
                            Esqueceu sua senha?
                        </a>
                        <input type="submit" class="access btn btn-primary btn-block" value="Acessar" />
                        <div class="checkbox">
                            <input type="checkbox" id="lembrar_usuario" name="lembrar_usuario" value="1" @if(old('lembrar_usuario')=='1') checked="checked" @endif>
                            <label for="lembrar_usuario">
                                Permanecer conectado
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-start col-md-4 col-sm-6 fix-height">
                <div class="certificate-login">
                    <h2>Acessar com e-CPF</h2>
                    <p>Acesse utilizando seu e-CPF.</p>
                    <figure class="center-block">
                        <img src="{{asset('images/e-cpf01.png')}}" class="img-responsive" />
                    </figure>
                    <a href="{{URL::to('/acessar/certificado')}}" class="access btn btn-primary btn-block">Acessar com e-CPF</a>
                </div>
            </div>
        </div>
        <div class="col-md-2 hidden-xs hidden-sm"></div>
    </div>
    <?php
	/*
    <div class="sign-up col-md-12 col-sm-12 text-center">
        <a href="{{url('/cadastrar')}}" class="btn btn-black btn-lg">Ainda não tenho cadastro, quero começar!</a><br /><br />
        <a href="{{url('/validar')}}" class="btn btn-primary btn-lg">Validar certidão</a>
    </div>
	*/
	?>
@endsection