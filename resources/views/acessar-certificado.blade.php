@extends('layout.home')

@section('scripts')
	<script type="text/javascript" src="{{asset('lacuna/js/jquery.blockUI.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('lacuna/js/lacuna-web-pki-2.5.0.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
            <div class="box-start col-md-4 col-sm-6">
                <div class="default-login">
                    <h2>Acessar com certificado</h2>
                    <p>Clique em selecionar um certificado abaixo.</p>
                    <form id="authForm" action="certificado/autenticar" method="POST">
	                    {{csrf_field()}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                                <div class="menssagem">
		                            @foreach ($errors->all() as $error)
		    	                        <?php
		    	                        switch ($error) {
		    	                        	case '6001':
		    	                        		echo 'O CPF do certificado autenticado não foi encontrado em nosso banco de dados.';
		    	                        		break;
		    	                        	case '6002':
		    	                        		echo 'Um erro ocorreu com seu certificado, por favor, confira se o mesmo ainda é válido.';
		    	                        		break;
											case '6003':
		    	                        		echo 'O CNPJ do certificado autenticado não foi encontrado em nosso banco de dados.';
		    	                        		break;
		    	                        	default:
		    	                        		echo $error;
		    	                        		break;
		    	                        }
		    	                        ?>
		    	                        <br />
        		                    @endforeach
                                </div>
                            </div>
                        @endif
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                        <div class="form-group">
                            <label for="certificateSelect">Selecione um certificado</label>
                            <select id="certificateSelect" class="form-control"></select>
                        </div>
                        <br />
 						<div class="form-group clearfix">
	                        <button id="refreshButton" type="button" class="btn btn-primary pull-left">Atualizar certificados</button>
    	                    <button id="signInButton" type="button" class="btn btn-success pull-right">Acessar</button>
                        </div>
                    </form>
				</div>
            </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
    <script>

		var pki = new LacunaWebPKI();
	
		// -------------------------------------------------------------------------------------------------
		// Function called once the page is loaded
		// -------------------------------------------------------------------------------------------------
		function init() {
	
			// Wireup of button clicks
			$('#signInButton').click(signIn);
			$('#refreshButton').click(refresh);
	
			// Block the UI while we get things ready
			$.blockUI({message:'<h1>Carregando certificados.</h1>',css:{padding:20}});
	
			// Call the init() method on the LacunaWebPKI object, passing a callback for when
			// the component is ready to be used and another to be called when an error occurs
			// on any of the subsequent operations. For more information, see:
			// https://webpki.lacunasoftware.com/#/Documentation#coding-the-first-lines
			// http://webpki.lacunasoftware.com/Help/classes/LacunaWebPKI.html#method_init
			pki.init({
				ready: loadCertificates, // as soon as the component is ready we'll load the certificates
				defaultError: onWebPkiError
			});
		}
	
		// -------------------------------------------------------------------------------------------------
		// Function called when the user clicks the "Refresh" button
		// -------------------------------------------------------------------------------------------------
		function refresh() {
			// Block the UI while we load the certificates
			$.blockUI({message:'<h1>Carregando certificados.</h1>',css:{padding:20}});
			// Invoke the loading of the certificates
			loadCertificates();
		}
	
		// -------------------------------------------------------------------------------------------------
		// Function that loads the certificates, either on startup or when the user
		// clicks the "Refresh" button. At this point, the UI is already blocked.
		// -------------------------------------------------------------------------------------------------
		function loadCertificates() {
	
			// Call the listCertificates() method to list the user's certificates
			pki.listCertificates({
	
				// specify that expired certificates should be ignored
				filter: pki.filters.isWithinValidity,
	
				// in order to list only certificates within validity period and having a CPF (ICP-Brasil), use this instead:
				//filter: pki.filters.all(pki.filters.hasPkiBrazilCpf, pki.filters.isWithinValidity),
	
				// id of the select to be populated with the certificates
				selectId: 'certificateSelect',
	
				// function that will be called to get the text that should be displayed for each option
				selectOptionFormatter: function (cert) {
					return cert.subjectName + ' (issued by ' + cert.issuerName + ')';
				}
	
			}).success(function () {
	
				// once the certificates have been listed, unblock the UI
				$.unblockUI();
	
			});
		}
	
		// -------------------------------------------------------------------------------------------------
		// Function called when the user clicks the "Sign In" button
		// -------------------------------------------------------------------------------------------------
		function signIn() {
	
			// Block the UI while we process the authentication
			$.blockUI({message:'<h1>Autenticando certificado.</h1>',css:{padding:20}});
	
			// Get the thumbprint of the selected certificate
			var selectedCertThumbprint = $('#certificateSelect').val();
	
			// Call signWithRestPki() on the Web PKI component passing the token received from REST PKI and the certificate
			// selected by the user. Although we're making an authentication, at the lower level we're actually signing
			// a cryptographic nonce (a random number generated by the REST PKI service), hence the name of the method.
			pki.signWithRestPki({
				token: '<?php echo $token; ?>',
				thumbprint: selectedCertThumbprint,
			}).success(function () {
				// Once the operation is completed, we submit the form
				$('#authForm').submit();
			});
		}
	
		// -------------------------------------------------------------------------------------------------
		// Function called if an error occurs on the Web PKI component
		// -------------------------------------------------------------------------------------------------
		function onWebPkiError(message, error, origin) {
			// Unblock the UI
			$.unblockUI();
			// Log the error to the browser console (for debugging purposes)
			if (console) {
				console.log('An error has occurred on the signature browser component: ' + message, error);
			}
			// Show the message to the user. You might want to substitute the alert below with a more user-friendly UI
			// component to show the error.
			alert(message);
		}
	
		// Schedule the init function to be called once the page is loaded
		$(document).ready(init);
	
	</script>
@endsection