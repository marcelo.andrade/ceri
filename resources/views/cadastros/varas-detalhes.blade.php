<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados do vara</legend>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label>Comarca</label>
                    <select class="form-control" name="id_comarca" disabled>
                        <option value="{{$vara->comarca->id_comarca}}">{{$vara->comarca->no_comarca}}</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Tipo de vara</label>
                    <select class="form-control" name="id_vara_tipo" disabled>
                        @if($vara->id_vara_tipo>0)
                            <option>{{$vara->vara_tipo->no_vara_tipo}}</option>
                        @else
                            <option>Não informado</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <label>Sigla</label>
                    <input type="text" class="form-control" name="abv_vara" value="{{$vara->abv_vara}}" disabled>
                </div>
                <div class="col-md-8">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="no_vara" value="{{$vara->no_vara}}" disabled>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label>E-mail geral da vara</label>
                    <input type="text" class="form-control" name="no_email_pessoa" value="{{$vara->pessoa->no_email_pessoa}}" disabled>
                </div>
            </div>
        </fieldset>
    </div>
</div>