@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.cadastros.vara.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Varas</h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                @if(!in_array($class->id_tipo_pessoa,array(9,13)))
                    <button data-target="#nova-vara" data-toggle="modal" class="new btn btn-success" type="button">
                        Nova vara
                    </button>
                @endif
                @include('cadastros.varas-historico')
            </div>
        </div>
    </div>
    <div id="nova-vara" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nova vara</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-nova-vara" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="enviar-vara btn btn-success" name="enviar-vara">Salvar vara</button>
                </div>
            </div>
        </div>
    </div>
    <div id="alterar-vara" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Alterar vara - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-alterar-vara" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="salvar-vara btn btn-success" name="enviar-vara">Salvar vara</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-vara" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes da vara - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection