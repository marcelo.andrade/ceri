@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.cadastros.cargo.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Cargos</h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                @if(!in_array($class->id_tipo_pessoa,array(9,13)))
                    <button data-target="#novo-cargo" data-toggle="modal" class="new btn btn-success" type="button">
                        Novo cargo
                    </button>
                @endif
                @include('cadastros.cargos-historico')
            </div>
        </div>
    </div>
    <div id="novo-cargo" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo cargo</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-cargo" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="enviar-cargo btn btn-success" name="enviar-cargo">Salvar cargo</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-cargo" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do cargo - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection