<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados do vara</legend>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label>Comarca</label>
                    <select class="form-control" name="id_comarca">
                        <option value="0">Selecione</option>
                        @if(count($comarcas)>0)
                            @foreach($comarcas as $comarca)
                                <option value="{{$comarca->id_comarca}}">{{$comarca->no_comarca}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Tipo de vara</label>
                    <select class="form-control" name="id_vara_tipo">
                        <option value="0">Selecione</option>
                        @if(count($vara_tipos)>0)
                            @foreach($vara_tipos as $vara_tipo)
                                <option value="{{$vara_tipo->id_vara_tipo}}">{{$vara_tipo->no_vara_tipo}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <label>Sigla</label>
                    <input type="text" class="form-control" name="abv_vara">
                </div>
                <div class="col-md-8">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="no_vara">
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label>E-mail geral da vara</label>
                    <input type="text" class="form-control" name="no_email_pessoa">
                </div>
            </div>
        </fieldset>
    </div>
</div>