<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados do cargo</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label>Tipo de usuário</label>
                    <select class="form-control" name="id_tipo_usuario" disabled>
                        <option value="{{$cargo->tipo_usuario->id_tipo_usuario}}">{{$cargo->tipo_usuario->no_tipo_usuario}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-3">
                    <label>Código</label>
                    <input type="text" class="form-control" name="codigo_cargo" value="{{$cargo->codigo_cargo}}" disabled>
                </div>
                <div class="col-md-3">
                    <label>Sigla</label>
                    <input type="text" class="form-control" name="abv_cargo" value="{{$cargo->abv_cargo}}" disabled>
                </div>
                <div class="col-md-6">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="no_cargo" value="{{$cargo->no_cargo}}" disabled>
                </div>
            </div>
        </fieldset>
    </div>
</div>