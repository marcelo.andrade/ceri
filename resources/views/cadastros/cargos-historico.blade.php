<div class="panel table-rounded">
    <table id="cargos" class="table table-striped table-bordered table-fixed small">
        <thead>
        <tr class="gradient01">
            <th width="15%">Código</th>
            <th width="35%">Nome</th>
            <th width="20%">Tipo usuário</th>
            <th width="15%">Sigla</th>
            <th width="15%">Ações</th>
        </tr>
        </thead>
        <tbody>
            @if (count($cargos)>0)
                @foreach ($cargos as $cargo)
                    <tr>
                        <td>{{$cargo->codigo_cargo}}</td>
                        <td>{{$cargo->no_cargo}}</td>
                        <td>{{$cargo->tipo_usuario->no_tipo_usuario}}</td>
                        <td>{{$cargo->abv_cargo}}</td>
                        <td class="options">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-cargo" data-idcargo="{{$cargo->id_cargo}}" data-nocargo="{{$cargo->no_cargo}}">Detalhes</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhum cargo foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>