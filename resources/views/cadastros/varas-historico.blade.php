<div class="panel table-rounded">
	<table id="varas" class="table table-striped table-bordered table-fixed small">
		<thead>
		<tr class="gradient01">
			<th width="20%">Comarca</th>
			<th width="50%">Nome</th>
			<th width="20%">Tipo</th>
			<th width="15%">Ações</th>
		</tr>
		</thead>
		<tbody>
			@if (count($varas)>0)
				@foreach ($varas as $vara)
					<tr>
						<td>{{$vara->comarca->no_comarca}}</td>
						<td>{{$vara->no_vara}}</td>
						<td>
							@if($vara->id_vara_tipo>0)
								{{$vara->vara_tipo->no_vara_tipo}}
							@else
								Não informado
							@endif
						</td>
						<td class="options">
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-vara" data-idvara="{{$vara->id_vara}}" data-novara="{{$vara->no_vara}}">Detalhes</button>
								@if(!in_array($class->id_tipo_pessoa,array(9,13)))
									<div class="btn-group" role="group">
		                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                                    <span class="caret"></span>
		                                </button>
		                                <ul class="dropdown-menu">
											<li><a href="#" data-toggle="modal" data-target="#alterar-vara"  data-idvara="{{$vara->id_vara}}" data-novara="{{$vara->no_vara}}">Alterar</a></li>
											<li><a href="#" class="remover-vara" data-idvara="{{$vara->id_vara}}" data-novara="{{$vara->no_vara}}">Remover</a></li>
		                                </ul>
		                            </div>
		                        @endif
	                        </div>
						</td>
					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="4">
						<div class="single alert alert-danger">
							<i class="glyphicon glyphicon-remove"></i>
							<div class="mensagem">
								Nenhuma vara foi encontrada.
							</div>
						</div>
					</td>
				</tr>
			@endif
		</tbody>
	</table>
</div>