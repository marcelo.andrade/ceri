<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados do cargo</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label>Tipo de usuário</label>
                    <select class="form-control" name="id_tipo_usuario">
                        <option value="0">Selecione</option>
                        @if(count($tipos_usuario)>0)
                            @foreach($tipos_usuario as $tipo_usuario)
                                <option value="{{$tipo_usuario->id_tipo_usuario}}">{{$tipo_usuario->no_tipo_usuario}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-3">
                    <label>Código</label>
                    <input type="text" class="form-control" name="codigo_cargo">
                </div>
                <div class="col-md-3">
                    <label>Sigla</label>
                    <input type="text" class="form-control" name="abv_cargo">
                </div>
                <div class="col-md-6">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="no_cargo">
                </div>
            </div>
        </fieldset>
    </div>
</div>