<div class="clearfix">
	<?php
	$alienacao_regras = $alienacao->regras_filtros($request->grupo)['regras'];
	$alienacao_titulos = $alienacao->regras_filtros($request->grupo)['titulos'];
	$alienacao_alcadas = $alienacao->regras_filtros($request->grupo)['alcadas'];

	if (count($alienacao_regras)>0) {
		foreach ($alienacao_regras as $key => $regra) {
			$total = $alienacao->total_alerta($regra,'unica');
			switch ($alienacao_alcadas[$key]) {
				case 2:
					$classe = 'cartorio';
					break;
				case 8:
					$classe = 'cef';
					break;
				default:
					$classe = '';
					break;
			}
			echo '<div class="col-md-3">';
				echo '<a href="'.URL::to('/servicos/alienacao/filtrar/'.$request->grupo.'/'.$request->grupo.'_'.$key).'" class="notificacao-inicial text-center '.$classe.'">';
					if($total>0) {
						echo '<span class="badge ativa" data-toggle="tooltip" data-placement="bottom" title="'.$total.' '.($total>1?'notificações':'notificação').'">'.$total.'</span>';
					} else {
						echo '<span class="badge" data-toggle="tooltip" data-placement="bottom" title="Nenhuma notificação encontrada">0</span>';
					}
					echo '<i class="fa fa-bell-o" aria-hidden="true"></i>';
					echo '<h4>'.$alienacao_titulos[$key].'</h4>';
				echo '</a>';
			echo '</div>';
		}
	}
	?>
</div>
<div class="col-md-12 form-group">
	<div class="alert single alert-notificacao-cef" role="alert"><b>Alertas com essa cor são de alçada da Caixa Econômica federal</b></div>
</div>
<div class="col-md-12 form-group">
	<div class="alert single alert-notificacao-cartorio" role="alert"><b>Alertas com essa cor são de alçada do Cartório</b></div>
</div>