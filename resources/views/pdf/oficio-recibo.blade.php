@extends('layout.pdf')

@section('content')
	<?php
	$destinatario = $documento->pedido->pedido_pessoa_atual->pessoa;
	?>
	<h3 align="center">RECIBO - OFÍCIO ELETRÔNICO</h3>
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<?php
        switch ($documento->id_tipo_usuario_dest) {
            case 2:
        ?>
        		<tr>
                	<th width="30%">Cidade destinatária:</th>
					<td width="70%">{{$destinatario->enderecos[0]->cidade->no_cidade}}</td>
				</tr>
				<tr>
                	<th>Serventia destinatária:</th>
					<td>{{$destinatario->no_pessoa}}</td>
				</tr>
        <?php
                break;
            case 4:
        ?>
                <tr>
                	<th width="30%">Comarca destinatária:</th>
					<td width="70%">{{$destinatario->vara->comarca->no_comarca}}</td>
				</tr>
				<tr>
                	<th>Vara destinatária:</th>
					<td>{{$destinatario->no_pessoa}}</td>
				</tr>
        <?php
                break;
	    	case 7:
        ?>
                <tr>
                    <th width="30%">Unidade gestora destinatária:</th>
                    <td width="70%">{{$destinatario->no_pessoa}}</td>
                </tr>
        <?php
                break;
            case 11:
        ?>
                <tr>
                	<th width="30%">Unidade judiciária destinatária:</th>
					<td width="70%">{{$destinatario->unidade_judiciaria_divisao->unidade_judiciaria->no_unidade_judiciaria}}</td>
				</tr>
				<tr>
                	<th>Subunidade destinatária:</th>
					<td>{{$destinatario->no_pessoa}}</td>
				</tr>
        <?php
                break;
        }
        ?>
		<tr>
			<th>Protocolo:</th>
			<td>{{$documento->numero_protocolo}}</td>
		</tr>
		<tr>
			<th>Data do cadastro:</th>
			<td>{{Carbon\Carbon::parse($documento->dt_criacao)->format('d/m/Y H:i:s')}}</td>
		</tr>
		<tr>
			<th>Número do processo:</th>
			<td>{{$documento->numero_processo}}</td>
		</tr>
		<tr>
			<th>Número do documento:</th>
			<td>{{$documento->numero_documento}}</td>
		</tr>
		<tr>
			<th>Tipo de documento:</th>
			<td>{{$documento->tipo_documento->no_tipo_documento}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td width="30%">
				<b>Usuário emissor:</b><br />
				{{Auth::User()->no_usuario}}
			</td>
			<td width="70%">
				<b>Data e hora geração do relatório:</b><br />
				{{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}
			</td>
		</tr>
	</table>
@endsection
