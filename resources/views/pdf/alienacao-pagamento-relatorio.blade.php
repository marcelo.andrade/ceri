@extends('layout.pdf')

@section('content')
    <div class="panel table-rounded">
        <h3 align="center">{{$titulo}}</h3>
        <h4>DATA DO PAGAMENTO: {{\Carbon\Carbon::parse($alienacao_pagamento->dt_pagamento)->format('d/m/Y')}}</h4>
        <table class="table" border="0" cellpadding="2" cellspacing="0" width="100%"style="margin-bottom: 20px">
            <thead>
            <tr>
                <th width="15%">Protocolo/Protocolo Int.</th>
                <th width="10%">Data</th>
                <th width="10%">Contrato</th>
                <th width="25%">Devedores</th>
                <th width="15%">Valor Pago</th>
                <th width="20%">Status</th>
            </tr>
            </thead>
            <tbody>
            @if (count($alienacao_pagamento->alienacoes)>0)
                @foreach ($alienacao_pagamento->alienacoes as $alienacao)
                    <tr>
                        <td style="text-align: center">{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}} / {{$alienacao->prenotacao()->de_texto_curto_acao or ''}}</td>
                        <td>{{Carbon\Carbon::parse($alienacao->alienacao_pedido->pedido->dt_pedido)->format('d/m/Y')}}</td>
                        <td>{{$alienacao->numero_contrato}}</td>
                        <td>
                            @if(count($alienacao->alienacao_devedor)>0)
                                @foreach($alienacao->alienacao_devedor as $devedor)
                                    <span class="label label-primary">{{$devedor->no_devedor}}</span>
                                @endforeach
                            @endif
                        </td>
                        <td>{{formatar_valor($alienacao->valor_pago($alienacao_pagamento->id_alienacao_pagamento))}}</td>
                        <td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhuma notificação foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        <h4>VALOR DO PAGAMENTO: {{formatar_valor($alienacao_pagamento->va_pagamento)}}</h4>
    </div>
@endsection