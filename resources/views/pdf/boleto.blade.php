@extends('layout.boleto.pdf')

@section('content')
    <div id="container">
        <div id="logo_empresa">
            <img src="images/logo_empresa.png" height="70"/>
        </div>
        <div id="instr_header">
            @if ($dadosboleto->cpf_cnpj)
                <h1 style="font-size:14px">{{$dadosboleto['identificacao']}}<br/>{{$dadosboleto['cpf_cnpj']}}</h1>
            @else
                <h1 style="font-size:14px">{{$dadosboleto['identificacao']}}</h1>
            @endif
            <address>{{$dadosboleto['endereco']}}<br/></address>
            <address>{{$dadosboleto['cidade_uf']}}</address>
        </div>
        <div id="">
            <div id="instr_content">
                <p>
                    O pagamento deste boleto também poderá ser efetuado
                    nos terminais de Auto-Atendimento BB.
                </p>
                <h2>Instruções</h2>
                <ol>
                    <li>
                        Imprima em impressora jato de tinta (ink jet) ou laser, em
                        qualidade normal ou alta. Não use modo econômico.<br/>
                        <p class="notice">Por favor, configure margens esquerda e direita para 17mm.</p>
                    </li>
                    <li>
                        Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens
                        m&iacute;nimas &agrave; esquerda e &agrave; direita do
                        formulário.
                    </li>
                    <li>
                        Corte na linha indicada. Não rasure, risque, fure ou dobre
                        a região onde se encontra o código de barras
                    </li>
                </ol>
            </div>    <!-- id="instr_content" -->
        </div>    <!-- id="instructions" -->
        <div id="boleto">
            <div class="cut">
                <p>Corte na linha pontilhada</p>
            </div>

            <table cellspacing="0" cellpadding="0" width="666" border="0">
                <tbody>
                <tr>
                    <td class="ct" width="666">
                        <div align="right">
                            <b class="cp" style="font-size: 10px">Recibo do Sacado</b>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <table class="header" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width=150>
                        <img src="images/logobb.jpg">
                    </td>
                    <td width=50>
                        <div class="field_cod_banco" style="border-left:solid 2px navy;border-right:solid 2px navy">
                            &nbsp;{{$dadosboleto['codigo_banco_com_dv']}}&nbsp;</div>
                    </td>
                    <td class="linha_digitavel">{{$dadosboleto['linha_digitavel']}}</td>
                </tr>
                </tbody>
            </table>

            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="cedente">Cedente</td>
                    <td class="ag_cod_cedente">Agência / Código do Cedente</td>
                    <td class="especie">Espécie</td>
                    <td class="qtd">Quantidade</td>
                    <td class="nosso_numero">Nosso número</td>
                </tr>
                <tr class="campos">
                    <td class="cedente">{{$dadosboleto['cedente']}}&nbsp;</td>
                    <td class="ag_cod_cedente">{{$dadosboleto['agencia_codigo']}}&nbsp;</td>
                    <td class="especie">{{$dadosboleto['especie']}}&nbsp;</td>
                    <td class="qtd">{{$dadosboleto['quantidade']}}&nbsp;</td>
                    <td class="nosso_numero">{{$dadosboleto['nosso_numero']}}&nbsp;</td>
                </tr>
                </tbody>
            </table>

            <table class="line" cellspacing="0" cellPadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="num_doc">Número do documento</td>
                    <td class="contrato">Contrato</td>
                    <td class="cpf_cei_cnpj">CPF/CEI/CNPJ</td>
                    <td class="vencmento">Vencimento</td>
                    <td class="valor_doc">Valor documento</td>
                </tr>
                <tr class="campos">
                    <td class="num_doc">{{$dadosboleto['numero_documento']}}</td>
                    <td class="contrato">{{$dadosboleto['contrato']}}</td>
                    <td class="cpf_cei_cnpj">{{$dadosboleto['cpf_cnpj']}}</td>
                    <td class="vencimento">{{$dadosboleto['data_vencimento']}}</td>
                    <td class="valor_doc">{{$dadosboleto['valor_boleto']}}</td>
                </tr>
                </tbody>
            </table>

            <table class="line" cellspacing="0" cellPadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="desconto">(-) Desconto / Abatimento</td>
                    <td class="outras_deducoes">(-) Outras deduções</td>
                    <td class="mora_multa">(+) Mora / Multa</td>
                    <td class="outros_acrescimos">(+) Outros acréscimos</td>
                    <td class="valor_cobrado">(=) Valor cobrado</td>
                </tr>
                <tr class="campos">
                    <td class="desconto">&nbsp;</td>
                    <td class="outras_deducoes">&nbsp;</td>
                    <td class="mora_multa">&nbsp;</td>
                    <td class="outros_acrescimos">&nbsp;</td>
                    <td class="valor_cobrado">&nbsp;</td>
                </tr>
                </tbody>
            </table>

            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="sacado">Sacado</td>
                </tr>
                <tr class="campos">
                    <td class="sacado">{{$dadosboleto['sacado']}}</td>
                </tr>
                </tbody>
            </table>

            <div class="footer">
                <p>Autenticação mecânica</p>
            </div>

            <div class="cut">
                <p>Corte na linha pontilhada</p>
            </div>

            <table class="header" border=0 cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="150"><img src="imagens/logobb.jpg"></td>
                    <td width="50">
                        <div class="field_cod_banco" style="border-left:solid 2px navy;border-right:solid 2px navy">
                            &nbsp;{{$dadosboleto['codigo_banco_com_dv']}}&nbsp;</div>
                    </td>
                    <td class="linha_digitavel">{{$dadosboleto['linha_digitavel']}}</td>
                </tr>
                </tbody>
            </table>

            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="local_pagto">Local de pagamento</td>
                    <td class="vencimento2">Vencimento</td>
                </tr>
                <tr class="campos">
                    <td class="local_pagto">QUALQUER BANCO ATÉ O VENCIMENTO</td>
                    <td class="vencimento2">{{$dadosboleto['data_vencimento']}}</td>
                </tr>
                </tbody>
            </table>

            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="cedente2">Cedente</td>
                    <td class="ag_cod_cedente2">Agência/Código cedente</td>
                </tr>
                <tr class="campos">
                    <td class="cedente2">{{$dadosboleto['cedente']}}</td>
                    <td class="ag_cod_cedente2">{{$dadosboleto['agencia_codigo']}}</td>
                </tr>
                </tbody>
            </table>
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="data_doc">Data do documento</td>
                    <td class="num_doc2">No. documento</td>
                    <td class="especie_doc">Espécie doc.</td>
                    <td class="aceite">Aceite</td>
                    <td class="data_process">Data process.</td>
                    <td class="nosso_numero2">Nosso número</td>
                </tr>
                <tr class="campos">
                    <td class="data_doc">{{$dadosboleto['data_documento']}}</td>
                    <td class="num_doc2">{{$dadosboleto['numero_documento']}}</td>
                    <td class="especie_doc">{{$dadosboleto['especie_doc']}}</td>
                    <td class="aceite">{{$dadosboleto['aceite']}}</td>
                    <td class="data_process">{{$dadosboleto['data_processamento']}}</td>
                    <td class="nosso_numero2">{{$dadosboleto['nosso_numero']}}</td>
                </tr>
                </tbody>
            </table>
            <table class="line" cellspacing="0" cellPadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="reservado">Uso do banco</td>
                    <td class="carteira">Carteira</td>
                    <td class="especie2">Espécie</td>
                    <td class="qtd2">Quantidade</td>
                    <td class="xvalor">x Valor</td>
                    <td class="valor_doc2">(=) Valor documento</td>
                </tr>
                <tr class="campos">
                    <td class="reservado">&nbsp;</td>
                    <td class="carteira">{{$dadosboleto['carteira']}}
                        @if ($dadosboleto['variacao_carteira'])
                            {{$dadosboleto['variacao_carteira']}}
                        @else
                            &nbsp;
                        @endif
                    </td>
                    <td class="especie2">{{$dadosboleto['especie']}}</td>
                    <td class="qtd2">{{$dadosboleto['quantidade']}}</td>
                    <td class="xvalor">{{$dadosboleto['valor_unitario']}}</td>
                    <td class="valor_doc2">{{$dadosboleto['valor_boleto']}}</td>
                </tr>
                </tbody>
            </table>
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="last_line" rowspan="6">
                        <table class="line" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr class="titulos">
                                <td class="instrucoes">
                                    Instruções (Texto de responsabilidade do cedente)
                                </td>
                            </tr>
                            <tr class="campos">
                                <td class="instrucoes" rowspan="5">
                                    <p>{{$dadosboleto['demonstrativo1']}}</p>
                                    <p>{{$dadosboleto['demonstrativo2']}}</p>
                                    <p>{{$dadosboleto['demonstrativo3']}}</p>
                                    <p>{{$dadosboleto['instrucoes1']}}</p>
                                    <p>{{$dadosboleto['instrucoes2']}}</p>
                                    <p>{{$dadosboleto['instrucoes3']}}</p>
                                    <p>{{$dadosboleto['instrucoes4']}}</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="line" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr class="titulos">
                                <td class="desconto2">(-) Desconto / Abatimento</td>
                            </tr>
                            <tr class="campos">
                                <td class="desconto2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="line" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr class="titulos">
                                <td class="outras_deducoes2">(-) Outras deduções</td>
                            </tr>
                            <tr class="campos">
                                <td class="outras_deducoes2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="line" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr class="titulos">
                                <td class="mora_multa2">(+) Mora / Multa</td>
                            </tr>
                            <tr class="campos">
                                <td class="mora_multa2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="line" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr class="titulos">
                                <td class="outros_acrescimos2">(+) Outros Acréscimos</td>
                            </tr>
                            <tr class="campos">
                                <td class="outros_acrescimos2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="last_line">
                        <table class="line" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr class="titulos">
                                <td class="valor_cobrado2">(=) Valor cobrado</td>
                            </tr>
                            <tr class="campos">
                                <td class="valor_cobrado2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="line" cellspacing="0" cellPadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="sacado2">Sacado</td>
                </tr>
                <tr class="campos">
                    <td class="sacado2">
                        <p>{{$dadosboleto['sacado']}}</p>
                        <p>{{$dadosboleto['endereco1']}}</p>
                        <p>{{$dadosboleto['endereco2']}}

                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            @if (strlen($dado["cnpj_sacado"]) > 11)
                                CNPJ: {{DataValidator::criarMasckCNPJ($dado['cnpj_sacado'])}}
                            @else
                                CPF: {{DataValidator::criarMasckCPF($dado['cnpj_sacado'])}}
                            @endif
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="line" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="titulos">
                    <td class="sacador_avalista" colspan="2">Sacador/Avalista</td>
                </tr>
                <tr class="campos">
                    <td class="sacador_avalista">&nbsp;</td>
                    <td class="cod_baixa">Cód. baixa</td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" width="666" border="0">
                <tbody>
                <tr>
                    <td width="666" align="right">
                        <font style="font-size: 10px;">Autenticação mecânica - Ficha de Compensação</font>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="barcode">
                <p>{{fbarcode($dadosboleto['codigo_barras'])}}</p>
            </div>
            <div class="cut">
                <p>Corte na linha pontilhada</p>
            </div>
        </div>
    </div>
@endsection	