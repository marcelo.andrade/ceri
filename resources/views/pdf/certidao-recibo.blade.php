@extends('layout.pdf')

@section('content')
    <?php
    $valor_total = 0;
    ?>
    <h3 align="center">RECIBO - CERTIDÃO</h3>
    <table class="table" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <th>Cidade destinatária:</th>
            <td>{{$pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</td>
        </tr>
        <tr>
            <th>Serventia destinatária:</th>
            <td>{{$pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
        </tr>
        <tr>
            <th>Tipo de certidão:</th>
            <td>{{$pedido->produto->no_produto}}</td>
        </tr>
        <tr>
            <th>Protocolo:</th>
            <td>{{$pedido->protocolo_pedido}}</td>
        </tr>
        @if($pedido->pessoa_origem->id_tipo_pessoa == 4)
            @if(count($pedido->certidao->processo)>0)
            <tr>
                <th>Número do processo</th>
                <td>{{$pedido->certidao->processo->numero_processo}}</td>
            </tr>
            @endif
            <tr>
                <th>Tipo de custas</th>
                <td>{{$pedido->certidao->tipo_custa->no_tipo_custa}}</td>
            </tr>
        @endif
        <tr>
            <th>Data do pedido:</th>
            <td>{{\Carbon\Carbon::parse($pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
        </tr>
        @if($class->id_tipo_pessoa == 2)
            <tr>
                <th>Solicitante:</th>
                <td>{{$pedido->usuario->no_usuario}} - {{$pedido->pessoa_origem->no_pessoa}}</td>
            </tr>
        @endif
        <tr>
            <th>Tipo de pesquisa:</th>
            <td>
                @php
                    switch($pedido->certidao->id_tipo_certidao_chave_pesquisa) {
                    case '1':
                        echo 'CPF';
                        break;
                    case '2':
                        echo 'CNPJ';
                        break;
                    case '3':
                        echo 'Nome';
                        break;
                    case '4':
                        echo 'Matrícula';
                        break;
                    }
                @endphp
            </td>
        </tr>
        <tr>
            <th>Chave de pesquisa:</th>
            <td>{{$pedido->certidao->de_chave_certidao}}</td>
        </tr>
        @if($pedido->certidao->id_tipo_certidao_chave_pesquisa)
            @php
                switch($pedido->certidao->id_tipo_certidao_chave_pesquisa) {
                case '1':
                    echo '<tr>
                            <th>Titular do CPF:</th>
                            <td>' . $pedido->certidao->de_chave_complementar . '</td>
                         </tr>';
                    break;
                    case '2':
                    echo '<tr>
                            <th>Razão social do CNPJ:</th>
                            <td>' . $pedido->certidao->de_chave_complementar . '</td>
                         </tr>';
                    break;
                }
            @endphp
        @endif
        <tr>
            <th>Status:</th>
            <td>{{$pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
        </tr>
    </table>
    <br>
    @if(in_array($pedido->pessoa_origem->id_tipo_pessoa,[2,3]))
        @if(count($pedido->produto_itens[0]->produto_item->lista_preco_pedido($pedido->id_pedido, 'L'))>0)
            <h4 align="center">Composição do valor da certidão</h4>
            <table class="table table-striped table-fixed table-condensed single">
                <thead>
                <tr>
                    <th width="60%">Descrição</th>
                    <th class="text-right" width="40%">Valor</th>
                </tr>
                </thead>
                <tbody>
                @if($class->id_tipo_pessoa == 2)
                    @foreach($pedido->produto_itens[0]->produto_item->lista_preco_pedido($pedido->id_pedido, 'L') as $produto_item)
                        @if(!in_array($produto_item->descricao,[" Total","Taxa de Administração"]))
                            <tr>
                                <td>{{$produto_item->descricao}}</td>
                                <td>{{formatar_valor($produto_item->va_preco)}}</td>
                            </tr>
                            @php $valor_total = $valor_total + $produto_item->va_preco; @endphp
                        @endif
                    @endforeach
                    <tr>
                        <td><strong>Total</strong></td>
                        <td><strong>{{formatar_valor($valor_total)}}</strong></td>
                    </tr>
                @else
                    @foreach($pedido->produto_itens[0]->produto_item->lista_preco_pedido($pedido->id_pedido, 'L') as $produto_item)
                        <tr>
                            @if($produto_item->descricao == " Total")
                                <td><strong>{{$produto_item->descricao}}</strong></td>
                                <td><strong>{{formatar_valor($produto_item->va_preco)}}</strong></td>
                            @else
                                <td>{{$produto_item->descricao}}</td>
                                <td>{{formatar_valor($produto_item->va_preco)}}</td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        @endif
    @endif
    <br/>
    @if($pedido->produto_itens[0]->produto_item->pedido_desconto_origem($pedido->id_pedido,'IN_MOVIMENTO_DESCONTO')->descricao == "S")
        <p>O desconto aplicado no valor de <b>{{formatar_valor($pedido->produto_itens[0]->produto_item->pedido_desconto_origem($pedido->id_pedido,'VA_MOVIMENTO_DESCONTO')->descricao)}} ({{valor_extenso($pedido->produto_itens[0]->produto_item->pedido_desconto_origem($pedido->id_pedido,'VA_MOVIMENTO_DESCONTO')->descricao)}})</b> é referente à <b>Pesquisa Eletrônica</b> do protocolo <b>{{$pedido->produto_itens[0]->produto_item->pedido_desconto_origem($pedido->id_pedido,'PROTOCOLO_PEDIDO_DESCONTO')->descricao}}</b>.</p>
    @endif
@endsection
