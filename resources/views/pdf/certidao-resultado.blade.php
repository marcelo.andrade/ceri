@extends('layout.pdf')

@section('content')
    <div class="container" align="center">
        <div class="row">
            <h3>Resultado da Certidão</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <p>Protocolo: <b>{{$pedido->protocolo_pedido}}</b></p>
            <p align="left">Tipo de certidão: <b>{{$pedido->produto->abv_produto}}</b></p>
            <p>
                Pesquisa:
                <b>
                    <?php
                        switch ($pedido->certidao->id_tipo_certidao_chave_pesquisa){
                            case 1:
                                echo "CPF";
                                break;
                            case 2:
                                echo "CNPJ";
                                break;
                            case 3:
                                echo "Nome";
                                break;
                            case 4:
                                echo "Matrícula";
                                break;
                        }
                    ?>
                </b>
            </p>
            <p>Chave de pesquisa:<b> {{$pedido->certidao->de_chave_certidao}}</b></p>
            <?php
            switch ($pedido->pedido_resultado->in_positivo) {
                case 'S':
            ?>
                    <p>Resultado da certidão: <b>POSITIVO</b></p>
                    <p>Observação: <b>{{$pedido->pedido_resultado->de_resultado}}</b></p>
            <?php
                    break;
                case 'N':
            ?>
                    <p>Resultado da certidão: <b>NEGATIVO</b></p>
                    <p>Observação: <b>Não foram localizados registros com a chave <font color="#FF0000">{{$pedido->certidao->de_chave_certidao}}</font> pesquisada nesta serventia.</b></p>
            <?php
                    break;
            }
            ?>
        </div>
    </div>
@endsection