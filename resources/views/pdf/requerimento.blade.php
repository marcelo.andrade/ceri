<div style="margin-left: 3cm; margin-right: 2cm; margin-top: 3cm;">
    <?php
    switch($opcao) :
    case 'pesquisa':
    ?>

    @if($nova_pesquisa->id_tipo_custa == 2)
        <div align="center">
            <img style="margin-bottom: 30px;" src="{{asset('images/logo06.png')}}" height="60px" />
        </div>
    @else
        <p style="padding-bottom: 30px; text-align: center"><b>{{$nova_pesquisa->pedido->pessoa_origem->no_pessoa}}</b></p>
    @endif

    <p style="padding-bottom: 30px">Ao Senhor(a) responsável pelo 
        {{$nova_pesquisa->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}
        da Cidade de
        {{$nova_pesquisa->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}
    </p>
    <p style="padding-bottom: 30px">Assunto: Solicitação de Pesquisa</p>
    <p style="padding-bottom: 30px; text-align: justify">Solicito a Vossa Senhoria, nos termos

        @if($nova_pesquisa->id_tipo_custa == 2)
            do provimento
        @else
            da Lei 30003/2005 Art. 16 e provimento
        @endif

        146/206/CGJ - MS

        @if($nova_pesquisa->id_tipo_custa != 2)
            Art. 20 § 1º, 2º, 3º, 4º e 5º
        @endif
        , que sejam encaminhados os dados solicitados no protocolo CERI nº <b>{{$nova_pesquisa->pedido->protocolo_pedido}}</b>

        @if($nova_pesquisa->id_tipo_custa != 2)
            , referente ao número de processo <b>{{$nova_pesquisa->processo->numero_processo}}</b>, deste orgão
        @endif
        .
    </p>
    <p style="padding-bottom: 30px">
        <b>Pesquisa:</b> {{$nova_pesquisa->chave_pesquisa_pesquisa->no_chave_pesquisa_pesquisa}}<br/>
        <b>{{$nova_pesquisa->chave_pesquisa_pesquisa->no_chave_pesquisa_pesquisa}}:</b> {{$nova_pesquisa->de_chave_pesquisa}}<br/>
        <?php
        switch ($nova_pesquisa->id_chave_pesquisa_pesquisa) {
            case '2':
                echo "<b>Titular do CPF:</b> $nova_pesquisa->de_chave_complementar<br/>";
                break;
            case '3':
                echo "<b>Razão social do CNPJ:</b> $nova_pesquisa->de_chave_complementar<br/>";
                break;
        }
        ?>

        @if($nova_pesquisa->id_tipo_custa != 2)
            <b>Número do processo:</b> {{$nova_pesquisa->processo->numero_processo}}<br/>
        @endif

        <b>Tipo Pesquisa:</b>  {{$nova_pesquisa->tipo_pesquisa->no_tipo_pesquisa}}<br/>
        <b>Tipo Custa:</b>  {{$nova_pesquisa->tipo_custa->no_tipo_custa}}
    </p>
    <p style="padding-bottom: 30px">Atenciosamente,</p>
    <p style="text-align: center">{{$nova_pesquisa->pedido->usuario->no_usuario}}</p>

        @if($nova_pesquisa->id_tipo_custa != 2)
            <p style="text-align: center">{{$nova_pesquisa->pedido->pessoa_origem->no_pessoa}}</p>
        @endif

    <?php
    break;
    case 'certidao':
    ?>
        @if($certidao->id_tipo_custa == 2)
            <div align="center">
                <img style="margin-bottom: 30px;" src="{{asset('images/logo06.png')}}" height="60px" />
            </div>
        @else
            <p style="padding-bottom: 30px; text-align: center"><b>{{$certidao->pedido->pessoa_origem->no_pessoa}}</b></p>
        @endif

    <p style="padding-bottom: 30px">Ao Senhor(a) responsável pelo
        {{$certidao->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}
        da Cidade de
        {{$certidao->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}
    </p>
    <p style="padding-bottom: 30px">Assunto: Solicitação de Certidão</p>
    <p style="padding-bottom: 30px; text-align: justify">Solicito a Vossa Senhoria, nos termos

        @if($certidao->id_tipo_custa == 2)
            do provimento
        @else
            da Lei 30003/2005 Art. 16 e provimento
        @endif

        146/206/CGJ - MS

        @if($certidao->id_tipo_custa != 2)
            Art. 20 § 1º, 2º, 3º, 4º e 5º
        @endif
        , que sejam encaminhados os dados solicitados no protocolo CERI nº <b>{{$certidao->pedido->protocolo_pedido}}</b>

        @if($certidao->id_tipo_custa != 2)
            , referente ao número de processo <b>{{$certidao->processo->numero_processo}}</b>, deste orgão
        @endif
        .
    </p>
    <p style="padding-bottom: 30px">
        <b>Certidão tipo:</b> {{$certidao->pedido->produto->abv_produto}}<br/>
        <b>Pesquisa:</b> {{$certidao->tipo_certidao_chave_pesquisa->no_chave_pesquisa}}<br/>
        <b>{{$certidao->tipo_certidao_chave_pesquisa->no_chave_pesquisa}}</b> {{$certidao->pedido->certidao->de_chave_certidao}}<br/>

        @if($certidao->id_tipo_custa != 2)
            <b>Número do processo:</b> {{$certidao->processo->numero_processo}}<br/>
        @endif

        <b>Tipo de Custa:</b> {{$certidao->tipo_custa->no_tipo_custa}}
    </p>
    <p style="padding-bottom: 30px">Atenciosamente,</p>
    <p style="text-align: center">{{$certidao->pedido->usuario->no_usuario}}</p>

    @if(in_array($certidao->id_tipo_custa, [1,3]))
        <p style="text-align: center">{{$certidao->pedido->pessoa_origem->no_pessoa}}</p>
    @endif

    <?php
    break;
    endswitch;
    ?>

</div>

