@extends('layout.pdf-pesquisa-resultado')

@section('content')
    <div class="box">
        <div class="container" align="center">
            <div class="row">
                <h3 style="padding:15px">Resultado da Pesquisa</h3>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <p>Protocolo: <b>{{$pedido->protocolo_pedido}}</b></p>
                <p align="left">Tipo de pesquisa: <b>{{$pedido->produto->abv_produto}}</b></p>
                <p>
                    Pesquisa:
                    <b>
                        <?php
                            switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa){
                                case 1:
                                    echo "Matrícula";
                                    break;
                                case 2:
                                    echo "CPF";
                                    break;
                                case 3:
                                    echo "CNPJ";
                                    break;
                                case 4:
                                    echo "Nome";
                                    break;
                            }
                        ?>
                    </b>
                </p>
                <p>Chave de pesquisa:<b> {{$pedido->pesquisa->de_chave_pesquisa}}</b></p>
                <?php
                switch ($pedido->pedido_resultado->in_positivo) {
                    case 'S':
                ?>
                        <p>Resultado da pesquisa: <b>POSITIVO</b></p>
                        <p>Observação: <b>{{$pedido->pedido_resultado->de_resultado}}</b></p>
                <?php
                        break;
                    case 'N':
                ?>
                        <p>Resultado da pesquisa: <b>NEGATIVO</b></p>
                        <p>Observação: <b>Não foram localizados registros com a chave <font color="#FF0000">{{$pedido->pesquisa->de_chave_pesquisa}}</font> pesquisada nesta serventia.</b></p>
                <?php
                        break;
                }
                ?>
            </div>
        </div>
    </div>
@endsection