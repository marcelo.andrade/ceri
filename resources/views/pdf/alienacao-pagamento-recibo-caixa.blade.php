@extends('layout.pdf')

@section('content')

<div align="center">
    <h3>RECIBO PAGAMENTO</h3>
    <p>{{$alienacao_valor_repasse_lote->protocolo_pedido}}</p>
</div>

<table width="100%" class="table-impressao">
    <tr>
        <th colspan="2">Produto: {{$nome_projeto}}</th>
    </tr>
    <tr>
        <th width="250">Data do pagamento:</th>
        <td>{{formatar_data($alienacao_valor_repasse_lote->dt_repasse_lote)}}</td>
    </tr>
    <tr>
        <th>Valor pago:</th>
        <td>{{formatar_valor($alienacao_valor_repasse_lote->va_repasse_lote)}}</td>
    </tr>
    <tr>
        <th>Quantidade (notificações):</th>
        <td>{{$alienacao_valor_repasse_lote->nu_quantidade_lote}}</td>
    </tr>
    <tr>
        <th>Número do lote:</th>
        <td>{{str_pad($alienacao_valor_repasse_lote->numero_lote, 5, "0", STR_PAD_LEFT)}}</td>
    </tr>
    <tr>
        <th>Usuário:</th>
        <td>{{$alienacao_valor_repasse_lote->usuario_cad->no_usuario}}</td>
    </tr>

</table>



<br><br>
<b>Data e hora da geração do documento:</b> {{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}

@endsection
