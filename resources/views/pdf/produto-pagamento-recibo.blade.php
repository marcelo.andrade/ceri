@extends('layout.pdf')

@section('content')

<div align="center">
    <h3>RECIBO DE PAGAMENTO</h3>
    <p>{{$pagamentos_repasse_lote->protocolo_pedido}}</p>
</div>

<table width="100%" class="table-impressao">
    <tr>
        <th>DETALHES DO LOTE DE PAGAMENTO</th>
    </tr>
    <tr>
        <th>Data do repasse: {{formatar_data($pagamentos_repasse_lote->dt_lote)}}</th>
    </tr>
    <tr>
        <th>Data do comprovante: {{formatar_data($pagamentos_repasse_lote->dt_comprovante_pagamento)}}</th>
    </tr>
    <tr>
        <th>Quantidade: {{str_pad($pagamentos_repasse_lote->nu_quantidade_lote, 5, "0", STR_PAD_LEFT)}}</th>
    </tr>
    <tr>
        <th>Valor pedido: <span class="real">{{formatar_valor($pagamentos_repasse_lote->va_parcela_pedido)}}</span></th>
    </tr>
    <tr>
        <th>Valor repasse: <span class="real">{{formatar_valor($pagamentos_repasse_lote->va_repasse_lote)}}</span></th>
    </tr>
</table>

<br><br>
<b>Data e hora da geração do documento:</b> {{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}

@endsection
