@extends('layout.pdf')

@section('content')
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Cidade:</b> {{$pedido->pedido_serventia->serventia->pessoa->enderecos[0]->cidade->no_cidade}}</td>
		</tr>
		<tr>
			<td><b>Cartório:</b> {{$pedido->pedido_serventia->serventia->no_serventia}}</td>
		</tr>
		<tr>
			<td><b>Tipo de certidão:</b> {{$pedido->produto->no_produto}}</td>
		</tr>
		<tr>
			<td><b>Data do pedido:</b> {{$pedido->dt_pedido}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Tipo de pesquisa:</b> {{$pedido->certidao->tipo_certidao_chave_pesquisa->no_chave_pesquisa}}</td>
		</tr>
		<tr>
			<td><b>Chave pesquisada:</b> {{$pedido->certidao->de_chave_certidao}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Resultado</b></td>
		</tr>
		<tr>
			<td class="resultado">{{$de_resultado}}</td>
		</tr>
	</table>
@endsection