@extends('layout.pdf')

@section('content')
    <?php
    $valor_total = 0;
    ?>
    <h3 align="center">RECIBO - PESQUISA ELETRÔNICA</h3>
    <table class="table" border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <th>Cidade destinatária:</th>
            <td>{{$pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</td>
        </tr>
        <tr>
            <th>Serventia destinatária:</th>
            <td>{{$pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
        </tr>
        <tr>
            <th>Tipo:</th>
            <td>{{$pedido->pesquisa->tipo_pesquisa->no_tipo_pesquisa}}</td>
        </tr>
        <tr>
            <th>Protocolo:</th>
            <td>{{$pedido->protocolo_pedido}}</td>
        </tr>
        @if($pedido->pessoa_origem->id_tipo_pessoa == 4)
            @if(count($pedido->pesquisa->processo)>0)
            <tr>
                <th>Número do processo</th>
                <td>{{$pedido->pesquisa->processo->numero_processo}}</td>
            </tr>
            @endif
            <tr>
                <th>Tipo de custas</th>
                <td>{{$pedido->pesquisa->tipo_custa->no_tipo_custa}}</td>
            </tr>
        @endif
        <tr>
            <th>Data do pedido</th>
            <td>{{\Carbon\Carbon::parse($pedido->pesquisa->dt_cadastro)->format('d/m/Y H:i')}}</td>
        </tr>
        @if($class->id_tipo_pessoa == 2)
            <tr>
                <th>Solicitante:</th>
                <td>{{$pedido->usuario->no_usuario}} - {{$pedido->pessoa_origem->no_pessoa}}</td>
            </tr>
        @endif
        <tr>
            <th>Chave de pesquisa:</th>
            <td>{{$pedido->pesquisa->de_chave_pesquisa}}</td>
        </tr>
        <tr>
            <th>Status:</th>
            <td>{{$pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
        </tr>
    </table>
    <br/>
    <br/>

    @if(in_array($pedido->pessoa_origem->id_tipo_pessoa,[2,3]))
        @if(count($pedido->produto_itens[0]->produto_item->lista_preco($pedido->pedido_pessoa_atual->pessoa->id_pessoa))>0)
            <h4 align="center">Composição do valor da pesquisa</h4>
            <table class="table table-striped table-fixed table-condensed single">
                <thead>
                <tr>
                    <th width="60%">Descrição</th>
                    <th class="text-right" width="40%">Valor</th>
                </tr>
                </thead>
                <tbody>
                @if($class->id_tipo_pessoa == 2)
                    @foreach(array_slice($pedido->produto_itens[0]->produto_item->lista_preco($pedido->pedido_pessoa_atual->pessoa->id_pessoa), 0,6) as $produto_item)
                        <tr>
                            <td>{{$produto_item->descricao}}</td>
                            <td>{{formatar_valor($produto_item->va_preco)}}</td>
                        </tr>
                        @php $valor_total += $produto_item->va_preco; @endphp
                    @endforeach
                    <tr>
                        <td><strong>Total</strong></td>
                        <td><strong>{{formatar_valor($valor_total)}}</strong></td>
                    </tr>
                @else
                    @foreach($pedido->produto_itens[0]->produto_item->lista_preco($pedido->pedido_pessoa_atual->pessoa->id_pessoa) as $produto_item)
                        <tr>
                            @if ($produto_item->descricao == 'Total')
                                <td><strong>{{$produto_item->descricao}}</strong></td>
                                <td><strong>{{formatar_valor($produto_item->va_preco)}}</strong></td>
                            @else
                                <td>{{$produto_item->descricao}}</td>
                                <td>{{formatar_valor($produto_item->va_preco)}}</td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        @endif
    @endif
@endsection
