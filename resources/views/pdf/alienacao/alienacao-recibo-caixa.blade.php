@extends('layout.pdf-alienacao-recibo')
@section('content')
    <div align="center">
        <h3>{{$titulo}}</h3>
        <p>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</p>
    </div>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Destinatário</th>
        </tr>
        <tr>
            <td width="100">Cidade</td>
            <td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</td>
        </tr>
        <tr>
            <td>Cartório</td>
            <td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}</td>
        </tr>

    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Credor</th>
        </tr>
        <tr>
            <td width="100">Credor</td>
            <td>{{$alienacao->credor_alienacao->credor->no_credor}}</td>
        </tr>
        <tr>
            <td width="100">Agência</td>
            <td>{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}</td>
        </tr>
    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="3">Dados da alienação</th>
        </tr>
        <tr>
            <td>Número do contrato</td>
            <td>Valor da dívida</td>
            <td>Código legado</td>
        </tr>
        <tr>
            <td>{{$alienacao->numero_contrato}}</td>
            <td>{{ formatar_valor($alienacao->va_divida) }}</td>
            <td>{{$alienacao->id_legado}}</td>
        </tr>
    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Dados do Imóvel</th>
        </tr>
        <tr>
            <td width="100">Matrícula</td>
            <td>{{$alienacao->matricula_imovel}}</td>
        </tr>
    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="4">Projeção</th>
        </tr>
        <tr>
            <td>Valor (1 a 15 dias)</td>
            <td>Valor (16 a 30 dias)</td>
            <td>Valor (31 a 45 dias)</td>
            <td>Valor (46 a 60 dias)</td>
        </tr>
        <tr>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_01)}}</td>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_02)}}</td>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_03)}}</td>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_04)}}</td>
        </tr>
    </table>
    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Devedores</th>
        </tr>
        @if(count($alienacao->alienacao_devedor)>0)
            @foreach($alienacao->alienacao_devedor as $devedor)
                <tr>
                    <td width="100">{{$devedor->nu_cpf_cnpj}}</td>
                    <td>{{$devedor->no_devedor}}</td>
                </tr>
                </tr>
            @endforeach
        @endif
    </table>
    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Endereço de cobrança</th>
        </tr>
        @if(count($alienacao->endereco_alienacao)>0)
            <?php
            $i = 0;
            ?>
            @foreach($alienacao->endereco_alienacao as $endereco)
                <tr id="{{$endereco->id_endereco_alienacao}}">
                    <td>
                        {{$endereco->no_endereco}}
                    </td>
                </tr>
                <?php
                $i++;
                ?>
            @endforeach
        @endif
    </table>
    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Protocolo (Prenotação)</th>
        </tr>
        <tr>
            <td width="200">Número do protocolo</td>
            <td width="200">Data do protocolo</td>
        </tr>
        <tr>
            <td>{{$alienacao->protocolo()["de_texto_curto_acao"]}}</td>
            <td>{{formatar_data($alienacao->protocolo()["dt_acao"]) }}</td>
        </tr>
    </table>
    <div class="fieldset-group">
        <div class="col-md-12">
            <div class="panel table-rounded">
                <h3 style="margin-top: 80px; margin-bottom: 10px">Histórico de Intimação</h3>
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                    <tr class="gradient01">
                        <th>Fase</th>
                        <th>Etapa</th>
                        <th>Ação</th>
                        <th>Resultado</th>
                        <th>Data</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($alienacao->alienacao_pedido->andamento_alienacao as $andamento)
                        <tr>
                            <td>{{$andamento->fase_grupo_produto->no_fase}}</td>
                            <td>{{$andamento->etapa_fase->no_etapa}}</td>
                            <td>{{$andamento->acao_etapa->no_acao}}</td>
                            <td>
                                @if($andamento->resultado_acao)
                                    <label class="label label-success">{{$andamento->resultado_acao->no_resultado}}</label>
                                @else
                                    <label class="label label-warning">Aguardando</label>
                                @endif
                            </td>
                            <td>{{Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{--<br><br>--}}
    {{--<b>Data e hora da geração do documento:</b> {{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}--}}
@endsection
