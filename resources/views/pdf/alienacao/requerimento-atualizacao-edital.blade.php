@extends('layout.pdf-alienacao-requerimento-cancelamento')

@section('content')

    <div class="box">
        <br/>
        <h3 align="center">Campo Grande MS{{local_e_data()}}</h3><br/>
        <p>A sua Senhoria o(a) Senhor(a)<br/>
        Oficial de Registro de Imóveis,</p>

        <p>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</p>

        <p>Assunto: <b>Atualização de valores do Edital</b><br/>
        Presado Senhores,</p>

        <p class="indent" align="justify">
        <ol>
            <li>Solicitamos nova emissão do Edital do processo de intimação em nome do(s) mutuário(s) abaixo relacionado(s),
                devido à purga de débito, junto à agência da CAIXA.
            </li><br /><br />
            <dl>
                @foreach($alienacao->alienacao_devedor as $devedor)
                    <dd>- {{$devedor->no_devedor}}</dd>
                @endforeach
                    <dd>- <b>Protocolo:</b> {{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</dd>
            </dl>
        </ol>
        </p>

        <p align="center">Atenciosamente,<br/>
        Superintendência da CAIXA Econômica Federal - Campo Grande, MS.</p>
    </div>
@endsection