@extends('layout.pdf-alienacao-certidao')

@section('content')
    <br />
    <table class="table">
        <tr>
            <td>
                <br />
                <h1 align="center">C E R T I D Ã O</h1>
                <br /><br /><br /><br />
                <p class="indent" align="justify">
                    <b>C E R T I F I C O</b>, que na data
                    <?php
                    $data = $alienacao->fim_decurso();
                    ?>
                    {{$data->format('d/m/Y')}}
                    transcorreu o prazo previsto no artigo 26 da Lei 9.514/97, sem que
                     @if(count($alienacao->alienacao_devedor))
                        <?php
                        $i=1;
                        ?>
                        @foreach($alienacao->alienacao_devedor as $devedor)
                             <b>{{(trim($devedor->no_devedor))}}</b>, <b>CPF: {{$devedor->nu_cpf_cnpj}}</b>{{($i == count($alienacao->alienacao_devedor) -1) ? ' e ' : ', '}}

                            <?php
                            $i++;
                            ?>
                        @endforeach
                    @endif
                     ou alguém por ele(a) tenha comparecido neste Serviço Registral, a fim de atender à intimação referente ao Processo de Notificação nº @if ($alienacao->numero_carta_intimacao() == ""){{$alienacao->id_alienacao}}@else{{$alienacao->numero_carta_intimacao()}}@endif,
                    <?php
                    switch ($alienacao->tipo_notificacao()['tipo']) {
                        case 'notificacao':
                            if($alienacao->tipo_encaminhamento()['tipo'] == 'rtd')
                                echo 'feita através do Cartório de Títulos e Documentos desta Comarca no dia '.\Carbon\Carbon::parse($alienacao->tipo_notificacao()['notificacao']->dt_resultado)->format('d/m/Y');
                            else{
                                echo 'feita através desta Serventia no dia '.\Carbon\Carbon::parse($alienacao->tipo_notificacao()['notificacao']->dt_resultado)->format('d/m/Y');
                            }
                            break;
                        case 'edital':
                            echo 'feita através de Edital publicado no dia '.\Carbon\Carbon::parse($alienacao->tipo_notificacao()['edital']->dt_acao)->format('d/m/Y');
                            break;
                        case 'ar':
                            echo 'feita através de Aviso de Recebimento nos termos do Art. 26§ 3º da Lei 9.514/97, no dia '.\Carbon\Carbon::parse($alienacao->tipo_notificacao()['ar']->dt_acao)->format('d/m/Y');
                            break;
                    }
                    ?>, para a liquidação dos débitos em atraso com o pagamento das prestações relativas ao contrato garantido por alienação fiduciária nº {{$alienacao->numero_contrato}}, registrado na{{$alienacao->registro_imovel}} matrícula nº {{$alienacao->matricula_imovel}} do Livro 02 desta Serventia, tendo como credor do(a) credor(a) <b>CAIXA ECONÔMICA FEDERAL</b>, {{$alienacao->credor_alienacao->credor->no_credor}}, CNPJ: {{$alienacao->credor_alienacao->credor->nu_cpf_cnpj}}. <b>Dou fé</b>. Protocolo nº {{$alienacao->protocolo()->de_texto_curto_acao}}. Emolumentos: R$ 29,00, Funjecc: R$ 2,90, Funadep: R$ 1,74, Funde-PGE: R$ 1,16, ISSQN: R$ 3,45, FEADMP-MS: R$ 2,90. {{$campos['de_texto_curto_acao'] ? 'Selo Dgital n° ' . $campos['de_texto_curto_acao'] .'. Consulte em www.tjms.jus.br' : ''}}
                </p>
                <?php
                /* Notificacao
                <p class="indent" align="justify">
                    <b>C E R T I F I C O</b>, que decorreu o prazo previsto no artigo 26 da Lei 9.514/97, sem que
                     @if(count($alienacao->alienacao_devedor))
                        <?php
                        $i=1;
                        ?>
                        @foreach($alienacao->alienacao_devedor as $devedor)
                            {{(($i>1 and $i<count($alienacao->alienacao_devedor))?', ':' e ')}}<b>{{$devedor->alienacao_devedor->no_devedor}}</b>, CPF: {{$devedor->alienacao_devedor->nu_cpf_cnpj}}
                            <?php
                            $i++;
                            ?>
                        @endforeach
                    @endif
                    , ou alguém por ele(a) tenha comparecido neste Serviço Registral, a fim de atender À intimação referente ao Processo de Notificação nº 999, feita através do Cartório de Títulos e Documentos desta Comarca, no dia 99.99.9999. para a liquidação dos débitos em atraso com o pagamento das prestações relativas ao contrato garantido por alienação fiduciária nº {{$alienacao->numero_contrato}}, registrado sob R-{{$alienacao->registro_imovel}} da matrícula nº {{$alienacao->matricula_imovel}} do Livro 02 desta Serventia, tendo como credor do(a) credor(a) <b>CAIXA ECONÔMICA FEDERAL</b>, {{$alienacao->credor_alienacao->credor->no_credor}}, CNPJ: {{$alienacao->credor_alienacao->credor->nu_cpf_cnpj}}. <b>Dou fé</b>. Protocolo nº {{$alienacao->protocolo()->de_texto_curto_resultado}}. Emolumentos: R$ 99,99, Funjecc: R$ 00,00, Funadep: R$ 11,11, Funde-PGE: R$ 22,22, ISSQN: R$ 33,33, FEADMP-MS: R$ 44,44.
                </p>
                */
                ?>
                <br /><br /><br /><br />
                <p align="center">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->uf}}{{local_e_data()}}.</p>
                <br /><br />
                <p align="center">
                    ___________________________________________<br />
                    {{$alienacao->serventia->no_oficial}}<br />
                    <b>Oficial Registrador</b>
                </p>
            </td>
        </tr>
    </table>
    <div id="footer">{{$footer}}</div>
@endsection