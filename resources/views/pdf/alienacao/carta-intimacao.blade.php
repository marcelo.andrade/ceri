<?php
$endereco = (count($alienacao->endereco_imovel) > 0 ) ? $alienacao->endereco_imovel->no_endereco : ' (sem endereço ativo)';
?>

@extends('layout.pdf-alienacao-carta')

@section('content')

    @if($campos['gerar_capa'])
        @include('pdf.alienacao.capa-de-processo')
    @endif

    <div class="box">
    	<br />
        <h3 align="center">CARTA DE INTIMAÇÃO nº {{($campos['de_texto_curto_acao']?$campos['de_texto_curto_acao']:$alienacao->id_alienacao)}}</h3><br />
        <p class="indent line-break">Prezado(a) Senhor(a):</p>
        <p class="indent" align="justify">Na qualidade de Oficial do {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}} de {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}, estado de {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->no_estado}}, segundo as atribuições a mim conferidas pelo §1º e §3º do art. 26 da Lei nº 9.514/97 e atendendo ao requerimento do(a) credor(a) <b>CAIXA ECONÔMICA FEDERAL</b>, {{$alienacao->credor_alienacao->credor->no_credor}}, {{$alienacao->credor_alienacao->credor->cidade->estado->uf}}, ingressado nesta Serventia com número de protocolo/andamento interno nº {{$alienacao->protocolo()->de_texto_curto_acao}} em {{\Carbon\Carbon::parse($alienacao->protocolo()->dt_acao)->format('d/m/Y')}}, venho pela presente proceder a esta <b>INTIMAÇÃO</b> para que Vossa(s) Senhoria(s):
            @if(count($alienacao->alienacao_devedor))
                @foreach($alienacao->alienacao_devedor as $devedor)
                    {{$devedor->no_devedor}} - CPF: {{$devedor->nu_cpf_cnpj}},
                @endforeach
            @endif
            residente(s) nesta cidade, satisfaça(m) as prestações vencidas e não pagas

            originárias do Contrato de financiamento com garantia fiduciária nº {{$alienacao->numero_contrato}}, registrado{{$alienacao->registro_imovel}} na matrícula nº {{$alienacao->matricula_imovel}} do livro 02 deste Serviço Registral, referente ao imóvel situado na {{$endereco}}.
        </p>
        <p class="indent" align="justify">Fica desde já ciente quem receber a presente notificação que, em havendo mais de um comprador e havendo previsão contratual na qual haja a constituição recíproca de poderes (procuração) para o recebimento de notificações, o recebimento desta implicará automaticamente o recebimento pelo outro adquirente outorgante.</p>
          
        <p class="indent" align="justify">Informo ainda que o valor do débito indicado pelo(a) credor(a) fiduciário(a), posicionado em {{formatar_data($alienacao->dt_encaminhamento())}}, é de <b>{{formatar_valor($alienacao->va_divida)}}</b> ({{valor_extenso($alienacao->va_divida)}}), resultantes das prestações vencidas, juros convencionados, as penalidades e os demais encargos contratuais e legais, sujeitos à atualização monetária, e as despesas de cobrança até a data do efetivo pagamento, somando-se, também, os encargos que se vencerem no prazo da presente intimação, tudo como prevê o art. 26, §1º da Lei 9.514/97.</p>
        <p class="indent" align="justify">Assim, solicito o comparecimento de Vossa(s) Senhoria(s) na sede deste Cartório de Registro de Imóveis, localizado na {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->no_endereco}} nº {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->nu_endereco}}, {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->no_bairro}}, {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->uf}} - CEP: {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->nu_cep}}, entre o horário de expediente, das {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_inicio_expediente}} às {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_inicio_almoco}} e {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_termino_almoco}} às {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_termino_expediente}}, ou na agência da Caixa Econômica Federal, {{$alienacao->credor_alienacao->credor->agencia->no_agencia}}{{($alienacao->credor_alienacao->credor->agencia->no_endereco!=''?', '.$alienacao->credor_alienacao->credor->agencia->no_endereco:'')}}{{($alienacao->credor_alienacao->credor->agencia->no_bairro!=''?', '.$alienacao->credor_alienacao->credor->agencia->no_bairro:'')}}{{($alienacao->credor_alienacao->credor->agencia->id_cidade>0?', '.$alienacao->credor_alienacao->credor->agencia->cidade->no_cidade:'')}}{{($alienacao->credor_alienacao->credor->agencia->nu_cep!=''?', '.$alienacao->credor_alienacao->credor->agencia->nu_cep:'')}}, para efetuar o pagamento do débito acima indicado, <b>no prazo improrrogável de 15 (quinze) dias contatos do recebimento desta, ficando ciente de que o não cumprimento da referida obrigação no prazo ora estipulado, permitirá ao credor(a) exercer o direito de consolidar a propriedade plena em seu nome, nos termos do art. 26, §7º da Lei 9.514/97.</b></p>
        <p class="indent" align="justify">Caso já tenha sido efetuado o pagamento do débito acima referido, peço a gentileza de desconsiderar esta intimação.</p>
        <p class="indent" align="justify">Na existência de cláusula, no seu contrato de financiamento, que prevê a outorga recíproca de procuração,
            na ausência de qualquer um dos participantes, o fiduciante localizado é responsável solidariamente pelos ausentes. </p>
        <br />
        <p class="indent line-break">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}} {{($campos['dt_acao']?$campos['dt_acao']:'(Não informado)')}}.</p>
        <p class="indent line-break">Atenciosamente,</p>
        <p class="indent">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_oficial}}</p>
        <p class="indent line-break">Oficial Registrador</p>
        <p>Senhores:<br />

        @if(count($alienacao->alienacao_devedor))
            @foreach($alienacao->alienacao_devedor as $devedor)
                <b>{{$devedor->no_devedor}}</b><br />
            @endforeach
        @endif
        @if(count($alienacao->endereco_alienacao))
            @foreach($alienacao->endereco_alienacao as $endereco)
               {{$endereco->no_endereco}};<br />
            @endforeach
        @endif
        <br />
        CIENTES(S) em: _____/_______________/_______, conforme assinaturas(s)
        </p>       
        <p>Assinatura(s): _____________________________________________________________________________</p>
	</div>
    @if($campos['gerar_nota'])
        @if(count($alienacao->endereco_alienacao)>0)
            @foreach($alienacao->endereco_alienacao as $endereco)
                @include('pdf.alienacao.nota-de-diligencia')
            @endforeach
        @endif
    @endif
@endsection