@extends('layout.pdf-alienacao-edital')

@section('content')
    <br />
    <table class="table">
        <tr>
            <td>
                <h3 class="align line-break">E D I T A L</h3>
                <p class="align line-break" align="justify"><b>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_oficial or ''}}</b>, Oficial do {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}.</p>
                <p class="align2 indent" align="justify">
                	<b>FAZ SABER</b> que atendendo solicitação feita pela <b>CAIXA ECONÔMICA FEDERAL</b>, com sede em Brasília-DF, na qualidade de credora fiduciante, nos termos do Instrumento Particular de n° {{$alienacao->numero_contrato}} amparado pela Lei 9.514/97, datada em 20/11/1997, referente ao imóvel situado na {{$alienacao->endereco_imovel->no_endereco}}{{($alienacao->endereco_imovel->nu_endereco!=''?' Nº '.$alienacao->endereco_imovel->nu_endereco:'')}}{{($alienacao->endereco_imovel->no_complemento!=''?', '.$alienacao->endereco_imovel->no_complemento:'')}}{{($alienacao->endereco_imovel->no_bairro!=''?', '.$alienacao->endereco_imovel->no_bairro:'')}}{{--{{$alienacao->endereco_imovel->cidade->no_cidade}}--}} - {{--{{$alienacao->endereco_imovel->cidade->estado->uf}}--}} objeto da matrícula n.º {{$alienacao->matricula_imovel}}, deste Cartório de Registro de Imóveis, fica intimado a comparecer a este Serviço Registral, situado à {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->no_endereco}}, {{($alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->nu_endereco!=''?$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->nu_endereco.', ':'')}}{{($alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_bairro!=''?$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_bairro.' - ':'')}}{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->uf}}, o devedor fiduciante
                    @if(count($alienacao->alienacao_devedor))
                        @foreach($alienacao->alienacao_devedor as $devedor)
                            <b>{{$devedor->no_devedor}}, CPF: {{$devedor->nu_cpf_cnpj}}</b>
                        @endforeach
                    @endif
                    @if ($alienacao->vencimentos()['dt_vencimento_periodo_1'])
                        {{--, ou alguém por ele, a fim de efetuar o pagamento das prestações em atraso do referido imóvel, vencidas desde {{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_1'])->format('d/m/Y')}} até {{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_4'])->format('d/m/Y')}} e as que vencerem até a data do efetivo pagamento.--}}
                        , ou alguém por ele, a fim de efetuar o pagamento, da(s) prestação(ões) vencida(s), posicionadas na data {{(isset($campos['arquivo_novo']) and $campos['arquivo_novo'] === "S") ? \Carbon\Carbon::parse($alienacao->alienacao_pedido->andamento_alienacao[0]->dt_resultado)->format('d/m/Y') : \Carbon\Carbon::parse($alienacao->protocolo()->dt_acao)->format('d/m/Y')}} no valor de <b>{{(isset($campos['arquivo_novo']) and $campos['arquivo_novo'] === "S") ? formatar_valor($alienacao->alienacao_pedido->andamento_alienacao[0]->va_valor_resultado) : formatar_valor($alienacao->va_divida)}}</b> ({{(isset($campos['arquivo_novo']) and $campos['arquivo_novo'] === "S") ? valor_extenso($alienacao->alienacao_pedido->andamento_alienacao[0]->va_valor_resultado) : valor_extenso($alienacao->va_divida)}}) bem como as que se vencerem até a data do pagamento, os juros convencionais, as penalidades e os demais encargos contratuais, os encargos legais, inclusive tributos, as contribuições condominiais imputáveis ao imóvel, além das despesas de cobrança e de intimação (art. 26, § 1º da Lei 9514/97).
                    @else
                        .
                    @endif

                </p>
                <p class="align2 indent line-break" align="justify">
                	Em decorrência da publicação do presente Edital, o citado devedor fiduciante será considerado intimado e terá prazo de 15 dias para satisfazer seu débito, sob as penas da Lei, tendo em vista que o mesmo está em lugar incerto e não sabido.
                </p>
                <p class="align line-break">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}{{ local_e_data()}}.</p>
                <br />
                <p class="align2 line-break" align="center">
                	<b>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_oficial or ''}}</b><br />
                    {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}<br />
                    {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->uf}}
                </p>
            </td>
        </tr>
    </table>
@endsection