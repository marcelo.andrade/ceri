{{----------------------PROCESSSO DE NOTIFICACAO---------------------}}
<div class="box">
    <table style="width: 100%">
        <tr>
            <td>PROTOCOLO Nº{{$alienacao->protocolo()->de_texto_curto_acao}}</td>
            <td align="right">LIVRO Nº 1</td>
        </tr>
    </table>
    <div style="width: 100%; height: 150px"></div>
    <h4 align="center"><u>PROCESSO DE NOTIFICAÇÃO Nº {{($campos['de_texto_curto_acao']?$campos['de_texto_curto_acao']:$alienacao->id_alienacao)}}</u></h4>
    <div style="width: 100%; height: 100px"></div>
    <table style="width: 100%">
        <tr>
            <td>CAIXA ECONÔMICA FEDERAL</td>
            <td align="right">Requerente</td>
        </tr>
    </table>
    <table style="width: 100%; height: 450px">
        <tr>
            <td><?php $i = 1; ?>
                @foreach($alienacao->alienacao_devedor as $devedor)
                    {{strtoupper(trim($devedor->no_devedor))}}{{($i == count($alienacao->alienacao_devedor) -1) ? ' e ' : '. '}}
                    <?php $i++; ?>
                @endforeach
            </td>
            <td align="right">Requerido(s)</td>
        </tr>
    </table>
    <p style="letter-spacing: 4px"><u>AUTUAÇÃO</u></p>
    <p class="indent" align="justify">
        Aos {{\Carbon\Carbon::parse($alienacao->protocolo()->dt_acao)->format('d/m/Y')}}, nessa Cidade
        de {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}},
        no estado
        de {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->no_estado}},
        em meu cartório, autuo a petição e documentos que seguem e
        para constar eu,____________________________________________________, oficial, a subscrevi.
    </p>
</div>
{{----------------------CERTIDÃO---------------------}}
@include('layout.pdf-alienacao-carta')
<div class="box">
    <div style="margin-left: 10em">
        <p style= "letter-spacing: 5px; padding-bottom: 10px"><u>CERTIDÃO</u></p>
        <table style="width: 60%; padding-bottom: 10px">
            <tr style="text-align: justify">
                <td>
                    CERTIFICO, que nesta data, registrei o
                    Requerimento e demais documentos sob o nº {{$alienacao->protocolo()->de_texto_curto_acao}}
                    no livro 01.<br />Dou fé.
                </td>
            </tr>
        </table>
        <p style="padding-bottom: 15px">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}/
            {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->uf}},
            {{\Carbon\Carbon::parse($alienacao->protocolo()->dt_acao)->format('d/m/Y')}}.
        </p>
        <p style="padding-bottom: 15px">O OFICIAL</p>
        <p style="letter-spacing: 5px; padding-bottom: 10px"><u>CERTIDÃO</u></p>
        <table style="width: 60%; padding-bottom: 10px">
            <tr style="text-align: justify">
                <td>
                    CERTIFICO, e dou fé, que nesta data, foi feito o deposito de
                    {{formatar_valor($alienacao->valor_emolumento()->va_total)}}
                    para despesas de intimação.
                </td>
            </tr>
        </table>
        <p style="padding-bottom: 15px">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}/
            {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->uf}},
            {{\Carbon\Carbon::parse($alienacao->protocolo()->dt_acao)->format('d/m/Y')}}.
        </p>
        <p style="padding-bottom: 15px">O OFICIAL</p>
        <p style="letter-spacing: 5px; padding-bottom: 10px"><u>CERTIDÃO</u></p>
        <table style="width: 60%; padding-bottom: 10px">
            <tr style="text-align: justify">
                <td>
                    CERTIFICO, e dou fé, que nesta data expedi a Intimação para sua efetivação.
                </td>
            </tr>
        </table>
        <p style="padding-bottom: 15px">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}/
            {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->uf}},
            {{\Carbon\Carbon::now()->format('d/m/Y')}}.
        </p>
        <p style="padding-bottom: 300px">O OFICIAL</p>
    </div>
</div>
@include('layout.pdf-alienacao-carta')