<?php
$endereco = (count($alienacao->endereco_imovel) > 0 ) ? $alienacao->endereco_imovel->no_endereco : ' (sem endereço ativo)';
?>
@extends('layout.pdf-requerimento-consolidacao')

@section('content')
    <div algin="right">
        <p>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}{{local_e_data()}}</p>
    </div>
    <div class="container">
        <div class="row">
            <p align="right"> {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}{{local_e_data()}}</p>
            <p align="left">
                <b>À</b><br/><b>Sua Senhoria o(a) Senhor(a)</b><br/>
                <b>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_oficial}}</b><br/>
                {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}} <br/>
                {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->no_endereco}}
            </p>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <p>Aussunto: Consolidação de Propriedade</p>
                <p align="left">Prezados Senhores,</p>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <p>Aussunto: Consolidação de Propriedade</p>
                <p align="left">Prezados Senhores,</p>
                <p class="indent" align="justify">1<span style="margin-left: 40px"> </span>
                    A CAIXA ECONÔMICA FEDERAL - CEF – Instituição financeira sob a forma de empresa pública,
                    universal, vinculada ao Ministério da Fazenda, criada pelo Decreto-Lei nº 759,de 12.08.1969,
                    alterado pelo Decreto-Lei nº 1259 de 19.02.1973, regendo-se pelo Estatuto vigente nesta data,
                    com sede no Setor Bancário Sul, Quadra 4, lotes 3/4, em Brasília-DF, CNPJ/MF nº 00.360.305/0001-04,
                    na qualidade de credora fiduciária da dívida relativa ao contrato de financiamento imobiliário com Alienação
                    Fiduciária em garantia nº
                   {{-- <b>{{$alienacao->numero_contrato}}</b>, em nome de
                    @foreach($alienacao->alienacao_devedor as $devedor)
                        <b>{{strtoupper($devedor->alienacao_devedor->no_devedor)}},</b>
                    @endforeach--}}
                    <b>{{$alienacao->numero_contrato}}</b>, em nome de
                    <?php $i = 1; ?>
                    @foreach($alienacao->alienacao_devedor as $devedor)
                        <b>{{strtoupper(trim($devedor->no_devedor))}}</b>{{($i == count($alienacao->alienacao_devedor) -1) ? ' e ' : ', '}}
                        <?php $i++; ?>
                    @endforeach
                    , na matrícula nº
                    <b>{{$alienacao->matricula_imovel}}</b>
                    , desse Registro de Imóveis, vem,
                    por seu representante legal ao final identificado, requerer, em seu favor, nos termos de Art. 26, § 7º da Lei 9.514/97,
                    a averbação da CONSOLIDAÇÃO DA PROPRIEDADE à margem da referida matrícula,
                    relativa ao imóvel situado na
                    <b>{{$endereco}}</b>.
                </p>
                <p class="indent" align="justify">2<span style="margin-left: 40px"> </span>
                    Requer também, que se procedam todas as averbações necessárias.
                </p>
                <p align="center">Atenciosamente,</p>
            </div>
        </div>
    </div>
@endsection