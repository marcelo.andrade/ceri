@extends('layout.pdf-alienacao-carta')

@section('content')
    <br />
    <h3 align="center">ORÇAMENTO</h3><br />
    <p>
    	Requerente:<br />
        Natureza do Título:
	</p>
    <br /><br /><br /><br /><br /><br />
    <?php
	$totais['Emolumentos Serventia'] = 0;
	$totais['Funjecc'] = 0;
	$totais['FDP'] = 0;
	$totais['FPGE'] = 0;
	$totais['ISSQN'] = 0;
	$totais['FMP'] = 0;
	?>
    <table class="table2" cellpadding="0" cellspacing="0">
        <tr>
            <th>Qtd.</th>
            <th>Discriminação da Custa</th>
            <th>Base Calc.</th>
            <th>Emol.</th>
            <th>Funjecc</th>
            <th>Funadep</th>
            <th>Funde-PGE</th>
            <th>ISSQN</th>
            <th>FEADMP</th>
            <th>Total</th>
        </tr>
        @if(count($produtos)>0)
        	@foreach($produtos as $produto)
            	<?php
				$total_linha = 0;
				?>
                <tr>
                    <td width="4%">{{$produto['qtd']}}</td>
                    <td width="25%">{{$produto['no_produto_item']}}</td>
                    <td width="9%">
                    	@if(isset($produto['valores']['Base de Cálculo']))
                            {{number_format($produto['valores']['Base de Cálculo'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['Base de Cálculo'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                    <td width="10%">
                    	@if(isset($produto['valores']['Emolumentos Serventia']))
                        	{{number_format($produto['valores']['Emolumentos Serventia'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['Emolumentos Serventia'];
							$totais['Emolumentos Serventia'] += $produto['valores']['Emolumentos Serventia'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                    <td width="8%">
                    	@if(isset($produto['valores']['Funjecc']))
                        	{{number_format($produto['valores']['Funjecc'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['Funjecc'];
							$totais['Funjecc'] += $produto['valores']['Funjecc'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                    <td width="9%">
                    	@if(isset($produto['valores']['FDP']))
                        	{{number_format($produto['valores']['FDP'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['FDP'];
							$totais['FDP'] += $produto['valores']['FDP'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                    <td width="11%">
                    	@if(isset($produto['valores']['FPGE']))
                        	{{number_format($produto['valores']['FPGE'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['FPGE'];
							$totais['FPGE'] += $produto['valores']['FPGE'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                    <td width="7%">
                    	@if(isset($produto['valores']['ISSQN']))
                        	{{number_format($produto['valores']['ISSQN'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['ISSQN'];
							$totais['ISSQN'] += $produto['valores']['ISSQN'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                   <td width="8%">
                    	@if(isset($produto['valores']['FMP']))
                        	{{number_format($produto['valores']['FMP'],2,',','.')}}
                            <?php
							$total_linha += $produto['valores']['FMP'];
							$totais['FMP'] += $produto['valores']['FMP'];
							?>
                        @else
                        	0,00
                        @endif
                    </td>
                    <td width="7%">{{number_format($total_linha,2,',','.')}}</td>
                </tr>
			@endforeach
        @endif
    </table>
    <br /><br />
    <table class="table3" cellpadding="0" cellspacing="0">
    	<tr>
			<th>Emolumentos</th>
            <th>Funjecc</th>
            <th>Funadep</th>
            <th>Funde-PGE</th>
            <th>ISSQN</th>
            <th>Total</th>
		</tr>
		<tr>
            <td>
            	@if(isset($totais['Emolumentos Serventia']))
                   	R$ {{number_format($totais['Emolumentos Serventia'],2,',','.')}}
                @else
                	0,00
                @endif
            </td>
            <td>
            	@if(isset($totais['Funjecc']))
                   	R$ {{number_format($totais['Funjecc'],2,',','.')}}
                @else
                	0,00
                @endif
            </td>
            <td>
            	@if(isset($totais['FDP']))
                   	R$ {{number_format($totais['FDP'],2,',','.')}}
                @else
                	0,00
                @endif
            </td>
            <td>
            	@if(isset($totais['FPGE']))
                   	R$ {{number_format($totais['FPGE'],2,',','.')}}
                @else
                	0,00
                @endif
            </td>
            <td>
            	@if(isset($totais['ISSQN']))
                   	R$ {{number_format($totais['ISSQN'],2,',','.')}}
                @else
                	0,00
                @endif
            </td>
            <td>R$ {{number_format(array_sum($totais),2,',','.')}}</td>
		</tr>
    </table>
    <br /><br />
    <p align="center">{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}<?php echo local_e_data(''); ?>.</p>
    <br /><br />
    <p align="center">
        ___________________________________________<br />
        {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_oficial}}<br />
        <b>Oficial Registrador</b>
    </p>
@endsection