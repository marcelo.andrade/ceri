<?php
$valor_total = 0;
$qtd = 0;
$id = [];
$valor = [];
$total = [];

foreach ($alienacao_valor_repasses as $alienacao) {
    $valor[$alienacao->id_serventia] = isset($valor[$alienacao->id_serventia]) ? $valor[$alienacao->id_serventia] : 0;
    $valor[$alienacao->id_serventia] += $alienacao->valor;

    $total[$alienacao->id_serventia] = isset($total[$alienacao->id_serventia]) ? $total[$alienacao->id_serventia] : 0;
    $total[$alienacao->id_serventia] += $alienacao->total;
}
?>
@extends('layout.pdf')
@section('content')
    <div class="container">
        <h3 align="center">RECIBO - PAGAMENTO EMOLUMENTO </h3>
        <p style="text-align: right">{{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}</p>
        <table class="table">
            <thead>
            <tr>
                <th style="text-align:left">Serventia</th>
                <th style="text-align:left">Cidade</th>
                <th style="text-align:right">Qtd</th>
                <th style="text-align:right">Valor Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($alienacao_valor_repasses as $alienacao)
                @if(!in_array($alienacao->id_serventia, $id, true))
                    <tr>
                        <td>
                            <b>{{$alienacao->no_serventia}}</b><br/>
                            <b>Código do banco:</b> {{$alienacao->codigo_banco}}
                            <b>Nome do banco:</b> {{$alienacao->no_banco}}
                            <b>Agência/DV:</b> {{$alienacao->nu_agencia}}/{{$alienacao->nu_dv_agencia}}
                            <b>Conta/DV:</b> {{$alienacao->nu_conta}}/{{$alienacao->nu_dv_conta}}
                            <b>Tipo de conta:</b> @if(isset($alienacao->tipo_conta))  {{$alienacao->tipo_conta == 'C' ? 'Corrente':'Poupança'}} @endif
                            <b>Tipo Pessoa:</b> {{$alienacao->in_tipo_pessoa_conta}}
                            <b>CPF/CNPJ:</b> {{cpf_cnpj($alienacao->nu_cpf_cnpj_conta)}}
                        </td>
                        <td>{{$alienacao->no_cidade}}</td>
                        <td style="text-align: right">{{$total[$alienacao->id_serventia]}}</td>
                        <td style="text-align: right">{{formatar_valor($valor[$alienacao->id_serventia])}}</td>
                    </tr>
                @endif
                <?php
                array_push($id, $alienacao->id_serventia);
                $valor_total += $alienacao->valor;
                $qtd += $alienacao->total;
                ?>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th style="text-align: left" colspan="2">TOTAL GERAL:</th>
                    <th style="text-align: right">{{$qtd}}</th>
                    <th style="text-align: right">{{formatar_valor($valor_total)}}</th>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection