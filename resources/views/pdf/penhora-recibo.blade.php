@extends('layout.pdf')

@section('content')
	<h3 align="center">RECIBO - PENHORA ELETRÔNICA DE IMÓVEIS </h3>
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<th width="30%">Protocolo:</th>
			<td width="70%">{{$pedido->protocolo_pedido}}</td>
		</tr>
		<tr>
			<th>Data do cadastro:</th>
			<td>{{$pedido->dt_pedido}}</td>
		</tr>
		<tr>
			<th>Número do processo:</th>
			<td>{{$pedido->penhora->numero_processo}}</td>
		</tr>
		<tr>
			<th>Tipo:</th>
			<td>{{$pedido->penhora->tipo_penhora->no_tipo_penhora}}</td>
		</tr>
		<tr>
			<th>Serventia:</th>
			<td>{{$pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
		</tr>
		<tr>
			<th>Status:</th>
			<td>{{$pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td  width="30%">
				<b>Usuário emissor:</b><br />
				{{Auth::User()->no_usuario}}
			</td>
			<td width="70%">
				<b>Data e hora geração do relatório:</b><br />
				{{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}
			</td>
		</tr>
	</table>
@endsection