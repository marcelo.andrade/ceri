@extends('layout.pdf')

@section('content')
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Cidade:</b> {{$novo_pedido->pedido_serventia->serventia->pessoa->enderecos[0]->cidade->no_cidade}}</td>
		</tr>
		<tr>
			<td><b>Cartório:</b> {{$novo_pedido->pedido_serventia->serventia->no_serventia}}</td>
		</tr>
        @if(isset($novo_pedido->pesquisa->tipo_pesquisa))
            <tr>
                <td><b>Tipo do pedido:</b> {{$novo_pedido->pesquisa->tipo_pesquisa->no_tipo_pesquisa}}</td>
            </tr>
		@endif
		<tr>
			<td><b>Período:</b> {{$novo_pedido->produto_itens[0]->produto_item->abv_produto_item}}</td>
		</tr>
		@if($nu_selo[0]->f_retorna_selo>0)
		<tr>
			<td><b>Selo utilizado:</b> {{$nu_selo[0]->f_retorna_selo}}</td>
		</tr>
		@endif
		<tr>
			<td><b>Data do pedido:</b> {{$novo_pedido->dt_pedido}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Tipo de pesquisa:</b> {{$novo_pedido->pesquisa->chave_pesquisa_pesquisa->no_chave_pesquisa_pesquisa}}</td>
		</tr>
		<tr>
			<td><b>Chave pesquisada:</b> {{$novo_pedido->pesquisa->de_chave_pesquisa}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Matrículas encontradas neste cartório:</b></td>
		</tr>
		<tr>
			<td class="resultado">
				@if(count($busca_matriculas)>0)
					<ul>
					@foreach ($busca_matriculas as $matricula)
						<li>{{$matricula->codigo_matricula}}</li>
					@endforeach
					</ul>
				@else
					Nenhuma matrícula encontrada.
				@endif
			</td>
		</tr>
	</table>
@endsection	