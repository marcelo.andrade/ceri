@extends('layout.pdf')

@section('content')
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Cidade:</b> {{$pedido->pedido_serventia->serventia->pessoa->enderecos[0]->cidade->no_cidade}}</td>
		</tr>
		<tr>
			<td><b>Cartório:</b> {{$pedido->pedido_serventia->serventia->no_serventia}}</td>
		</tr>
        @if($pedido->pesquisa->id_tipo_pesquisa>0)
            <tr>
                <td><b>Tipo do pedido:</b> {{$pedido->pesquisa->tipo_pesquisa->no_tipo_pesquisa}}</td>
            </tr>
        @endif
		<tr>
			<td><b>Período:</b> {{$pedido->produto_itens[0]->produto_item->abv_produto_item}}</td>
		</tr>
		@if($nu_selo[0]->f_retorna_selo>0)
			<tr>
				<td><b>Selo utilizado:</b> {{$nu_selo[0]->f_retorna_selo}}</td>
			</tr>
		@endif
		<tr>
			<td><b>Data do pedido:</b> {{$pedido->dt_pedido}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Tipo de pesquisa:</b> {{$pedido->pesquisa->chave_pesquisa_pesquisa->no_chave_pesquisa_pesquisa}}</td>
		</tr>
		<tr>
			<td><b>Chave pesquisada:</b> {{$pedido->pesquisa->de_chave_pesquisa}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Resultado</b></td>
		</tr>
		<tr>
			<td class="resultado">{{$de_resultado}}</td>
		</tr>
	</table>
@endsection