@extends('layout.pdf')

@section('content')
	<div class="marca"><img src="{{asset('images/marca_certidao.png')}}" alt="" width="100%" /></div>
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Cidade:</b> {{$pedido_selecionado->pedido_serventia->serventia->pessoa->enderecos[0]->cidade->no_cidade}}</td>
		</tr>
		<tr>
			<td><b>Cartório:</b> {{$pedido_selecionado->pedido_serventia->serventia->no_serventia}}</td>
		</tr>
		<tr>
			<td><b>Tipo de certidão:</b> {{$pedido_selecionado->produto->no_produto}}</td>
		</tr>
		@if($pedido_selecionado->certidao->id_tipo_certidao>0)
			<tr>
				<td><b>Tipo do pedido:</b> {{$pedido_selecionado->certidao->tipo_certidao->no_tipo_certidao}}</td>
			</tr>
		@endif
        @if($nu_selo[0]->f_retorna_selo>0)
			<tr>
				<td><b>Selo utilizado:</b> {{$nu_selo[0]->f_retorna_selo}}</td>
			</tr>
		@endif
		@if($pedido_selecionado->certidao->id_periodo>0)
			<tr>
				<td><b>Período:</b> {{$pedido_selecionado->certidao->periodo->no_periodo}}</td>
			</tr>
		@endif
		<tr>
			<td><b>Data do pedido:</b> {{$pedido_selecionado->dt_pedido}}</td>
		</tr>
	</table>
	<br />
	<table class="table" border="0" cellpadding="2" cellspacing="0" widtd="100%">
		<tr>
			<td><b>Tipo de pesquisa:</b> {{$pedido_selecionado->certidao->tipo_certidao_chave_pesquisa->no_chave_pesquisa}}</td>
		</tr>
		<tr>
			<td><b>Chave pesquisada:</b> {{$pedido_selecionado->certidao->de_chave_certidao}}</td>
		</tr>
	</table>
	<div id="footer">{{$footer}}</div>
	<div class="page-break"></div>
	<?php
	if (count($imagens)>0) {
		foreach ($imagens as $imagem) {
			echo '<div class="marca"><img src="'.asset('images/marca_certidao.png').'" alt="" width="100%" /></div>';
			echo '<div id="footer">'.$footer.'</div>';
			echo $imagem;
		}
	}
	?>
    @if($pedido_selecionado->pedido_resultado->id_serventia_certificado>0)
	    <div class="page-break"></div>
        <div class="certificado alert alert-success clearfix">
            <i class="icon glyphicon glyphicon-ok pull-left"></i>
            <div class="menssagem pull-left">
                O documento foi assinado digitalmente por:<br />
                <b>{{$pedido_selecionado->pedido_resultado->serventia_certificado->no_comum}}</b><br />
                @if($nu_selo[0]->f_retorna_selo>0)
                <b>Selo utilizado:</b> {{$nu_selo[0]->f_retorna_selo}}<br />
                @endif
                <b>Data do documento:</b> {{Carbon\Carbon::parse($pedido_selecionado->pedido_resultado->dt_cadastro)->format('d/m/Y')}}
            </div>
        </div>
    @endif
@endsection	