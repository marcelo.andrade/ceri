@extends('layout.pdf')

@section('content')

<h3 align="center">{{$titulo}}</h3>

<p style="text-align: right">{{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}</p>

<table class="table">
    <thead>
        <tr>
            <th width="55%">Serventia</th>
            <th width="25%">Cidade</th>
            <th width="8%">Qtd</th>
            <th width="12%">Valor total</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $qtd = 0;
    $valor_total = 0;
    ?>
    @foreach($pagamento_repasse_lote as $pagamento)
        <tr>
            <td>
                <b>{{$pagamento->no_serventia}}</b><br />
                <b>Código do banco: </b>{{$pagamento->codigo_banco}}
                <b>Nome do banco: </b>{{$pagamento->no_banco}}
                <b>Agência/DV: </b>{{$pagamento->nu_agencia}}{{$pagamento->nu_agencia_dv}}
                <b>Conta/DV: </b>{{$pagamento->nu_conta}}/{{$pagamento->nu_dv_conta}}
                <b>Tipo de conta: </b>{{$pagamento->tipo_conta}}
                <b>Tipo de pessoa: </b>{{$pagamento->in_tipo_pessoa_conta}}
                <b>CPF/CNPJ: </b>{{cpf_cnpj($pagamento->nu_cpf_cnpj_conta)}}
            </td>
            <td>{{$pagamento->no_cidade}}</td>
            <td>{{$pagamento->nu_quantidade}}</td>
            <td>{{formatar_valor($pagamento->va_repasse)}}</td>
            <?php
            $qtd += $pagamento->nu_quantidade;
            $valor_total += $pagamento->va_repasse;
            ?>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr style="font-weight: bold">
            <td colspan="2">TOTAL GERAL:</td>
            <td>{{$qtd}}</td>
            <td>{{formatar_valor($valor_total)}}</td>
        </tr>
    </tfoot>
</table>

@stop
