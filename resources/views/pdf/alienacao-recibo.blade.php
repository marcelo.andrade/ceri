@extends('layout.pdf-alienacao-recibo')

@section('content')

    <div class="container">
        <div class="row">
            <p align="right">
                    {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}
                    @if($alienacao->alienacao_pedido->andamento_alienacao[0]->dt_cadastro == NULL)
                        -
                    @else
                        {{Carbon\Carbon::parse($alienacao->alienacao_pedido->andamento_alienacao[0]->dt_cadastro)->format('d/m/Y')}}
                    @endif
            </p>
            <p align="left">
                <b>À</b><br/><b>Sua Senhoria o(a) Senhor(a)</b><br/>
                <b>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_oficial}}</b><br/>
                {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}<br/>
                {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->no_endereco}}
            </p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <p class="indent" align="justify">1<span style="margin-left: 40px">Solicitamos que, nos termos do § 1º do Art. 26 da Lei nº 9.514/97, seja(m) providenciada(s) a(s) INTIMAÇÃO(ÕES) do(s)
                destinatário(s)
                <?php $i = 1; ?>
                @foreach($alienacao->alienacao_devedor as $devedor)
                    <b>{{strtoupper(trim($devedor->no_devedor))}} - CPF: {{$devedor->nu_cpf_cnpj}}</b>{{($i == count($alienacao->alienacao_devedor) -1) ? ' e ' : ', '}}
                    <?php $i++; ?>
                @endforeach
                referente ao Contrato Habitacional nº
                <b>{{strtoupper(trim($alienacao->numero_contrato))}}</b>, registrado sob a matrícula nº
                <b>{{strtoupper(trim($alienacao->matricula_imovel))}}</b>, neste Cartório, referente ao imóvel
                situado no(a)
                <b>{{strtoupper(trim($alienacao->endereco_imovel->no_endereco))}}</b>.
                <b>{{strtoupper(trim($alienacao->endereco_imovel->nu_cep ? ' ,CEP:' . $alienacao->endereco_imovel->nu_cep : '' ))}}</b>
                </span>
            </p>
            <p class="indent" align="justify" >2<span style="margin-left: 40px">Solicitamos que, na existência de cláusula contratual que prevê a outorga recíproca de procuração, na ausência
                de qualquer um dos coobrigados, o fiduciante localizado deverá assinar pelo(s) ausente(s).</span>
            </p>
            <p class="indent" align="justify">3<span style="margin-left: 40px">Quando, por duas vezes, o intimando for procurado e não localizado em seu domicílio ou residência, havendo
                suspeita motivada de ocultação, solicitamos, por ato contínuo, efetuar a intimação de qualquer pessoa da família,
                vizinho, ou funcionário da portaria nos condomínios edilícios com controle de acesso, que no dia útil imediato,
                retornará ao imóvel, a fim de efetuar a intimação, na hora que designar, conforme <b>§3°-A e §3º-B do Art. 26</b> incluídos
                        na <b>Lei 9.514/97</b> pela <b>Lei 13.465/17</b>, sendo devidamente identificada na certidão definitiva de intimação.</span>
            </p>
            <p class="indent" align="justify">4<span style="margin-left: 40px">Por fim, solicitamos que se houver quaisquer outro(s) processo(s) para intimação do(s) mesmo(s) devedor(es),
                para o mesmo contrato e no(s) mesmo(s) endereço(s), que seja(m) devidamente finalizado(s), sem necessidade de
                comunicar à CAIXA, devendo ser dado andamento no mais recente.</span>
            </p>
        </div>
    </div>

    <div align="center">
        <h3>RECIBO / PROTOCOLO</h3>
        <p>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</p>
    </div>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Destinatário</th>
        </tr>
        <tr>
            <td width="100">Cidade</td>
            <td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</td>
        </tr>
        <tr>
            <td>Cartório</td>
            <td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}</td>
        </tr>

    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Credor</th>
        </tr>
        <tr>
            <td  width="100">Credor</td>
            <td>{{$alienacao->credor_alienacao->credor->no_credor}}</td>
        </tr>
        <tr>
            <td  width="100">Agência</td>
            <td>{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}</td>
        </tr>
    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="3">Dados da alienação</th>
        </tr>
        <tr>
            <td>Número do contrato</td>
            <td>Valor da dívida</td>
            <td>Código legado</td>
        </tr>
        <tr>
            <td>{{$alienacao->numero_contrato}}</td>
            <td>{{ formatar_valor($alienacao->va_divida) }}</td>
            <td>{{$alienacao->id_legado}}</td>
        </tr>
    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Dados do Imóvel</th>
        </tr>
        <tr>
            <td  width="100">Matrícula</td>
            <td>{{$alienacao->matricula_imovel}}</td>
        </tr>
    </table>

    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="4">Projeção</th>
        </tr>
        <tr>
            <td>Valor (1 a 15 dias)</td>
            <td>Valor (16 a 30 dias)</td>
            <td>Valor (31 a 45 dias)</td>
            <td>Valor (46 a 60 dias)</td>
        </tr>
        <tr>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_01)}}</td>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_02)}}</td>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_03)}}</td>
            <td>{{formatar_valor($alienacao->alienacao_projecao->va_periodo_04)}}</td>
        </tr>
    </table>
    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Devedores</th>
        </tr>
        @if(count($alienacao->alienacao_devedor)>0)
            @foreach($alienacao->alienacao_devedor as $devedor)
                <tr>
                    <td  width="100">{{$devedor->nu_cpf_cnpj}}</td>
                    <td>{{$devedor->no_devedor}}</td>
                </tr>
                </tr>
            @endforeach
        @endif
    </table>
    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Endereço de cobrança</th>
        </tr>
        @if(count($alienacao->endereco_alienacao)>0)
            <?php
            $i=0;
            ?>
            @foreach($alienacao->endereco_alienacao as $endereco)
                <tr id="{{$endereco->id_endereco_alienacao}}">
                    <td>
                        {{$endereco->no_endereco}}
                    </td>
                </tr>
                <?php
                $i++;
                ?>
            @endforeach
        @endif
    </table>
    <table width="100%" class="table-impressao">
        <tr>
            <th colspan="2">Protocolo (Prenotação)</th>
        </tr>
        <tr>
            <td width="200">Número do protocolo</td>
            <td width="200">Data do protocolo</td>
        </tr>
        <tr>
            <td>{{$alienacao->protocolo()["de_texto_curto_acao"]}}</td>
            <td>{{formatar_data($alienacao->protocolo()["dt_acao"]) }}</td>
        </tr>
    </table>
    <br><br>
    <b>Data e hora da geração do documento:</b> {{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}

@endsection
