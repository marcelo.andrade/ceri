@extends('layout.inicio')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.acessar.js?v=3.1.0')}}"></script>
@endsection

@section('content')
    <figure><img src="{{asset('images/logo06.png')}}" alt="CERI" class="center-block"/></figure>
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
        <div class="box-start col-md-4 col-sm-6">
            <div class="default-login">
                @if (session('status')=='5000')
                    <div class="single alert alert-success">
                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                        <div class="menssagem">
                            A nova senha foi salva com sucesso.<br /><br />
                            <a href="{{URL::to('/')}}" class="btn btn-black">Acessar o sistema</a>
                        </div>
                    </div>
                @else
                    <h2>Recuperar senha</h2>
                    <p>Digite uma nova senha para continuar.</p>
                    <form name="form-recuperar-senha" method="post" action="../recuperar-senha">
    					{{csrf_field()}}
                        <input type="hidden" name="recuperar_token" value="{{$recuperar_token}}" />
                        <div class="erros alert alert-danger" @if (count($errors)==0) style="display:none" @endif>
                        	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                            <div class="menssagem">
	                            @foreach ($errors->all() as $error)
                                    <?php
                                    switch ($error) {
                                        case '5001':
                                            echo 'O token de recuperação é inválido.';
                                            break;
                                        case '5002':
                                            echo 'A senha digitada já foi utilizada, digite uma nova senha.';
                                            break;
                                        default:
                                            echo $error;
                                            break;
                                    }
                                    ?>
                                    <br />
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group input-group">
    						<span class="input-group-addon" id="addon-mail"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="usuario" class="form-control" placeholder="E-mail" value="{{$usuario->email_usuario}}" disabled />
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon" id="addon-password"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="senha_usuario" class="form-control" placeholder="Senha" />
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon" id="addon-password"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="senha_usuario2" class="form-control" placeholder="Confirme a senha" />
                        </div>
                        <input type="submit" class="access btn btn-primary btn-block" value="Salvar" />
                    </form>
                @endif
            </div>
        </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
    @if (!session('status'))
        <div class="sign-in col-md-12 col-sm-12 text-center">
            <a href="{{url('/')}}" class="btn btn-black btn-lg">Voltar</a>
        </div>
    @endif
@endsection