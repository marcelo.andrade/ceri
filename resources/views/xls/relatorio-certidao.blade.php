@extends('layout.xls')

@section('content')
    <table>
        <td colspan="5"><h2>Certidão</h2></td>
        <td colspan="2" align="center" valign="middle">Data de emissão: {{\Carbon\Carbon::now()->format('d/m/Y')}}
    </table>
    <table class="table table-striped table-bordered small">
        <thead>
        <tr>
            <th>Protocolo</th>
            <th>Data do pedido</th>
            <th>Tipo da certidão</th>
            <th>Solicitante</th>
            <th>Serventia</th>
            <th>Status</th>
            <th>Período de disponibilização</th>
        </tr>
        </thead>
        <tbody>
        @if(count($todas_certidoes)>0)
            @foreach($todas_certidoes as $certidao)
                <tr>
                    <td>{{$certidao->pedido->protocolo_pedido}}</td>
                    <td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                    <td>{{$certidao->pedido->produto->abv_produto}}</td>
                    <td>{{$certidao->pedido->usuario->no_usuario}} - {{$certidao->pedido->pessoa_origem->no_pessoa}}</td>
                    <td>{{$certidao->pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
                    <td>{{$certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    <td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($certidao->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                    @endforeach
                    @endif
                </tr>
        </tbody>
    </table>
@endsection