<?php $total = 0 ?>
@extends('layout.xls')
@section('content')
  <table>
      <tr>
          <td><img src="images/logo06.png" width="120"/></td>
      </tr>
  </table>

    <table>
        <tr>
            <th colspan="2">Protocolo</th>
            <th>Serventia</th>
            <th>Cidade</th>
            <th>Alcada</th>
            <th>Fase / Etapa / Ação (próxima) </th>
            <th>Aguardando interação - Dias corridos</th>
            <th>Aguardando interação - Dias úteis</th>
        </tr>
        @if (count($acompanhamento_notificacao)>0)
            @foreach ($acompanhamento_notificacao as $relatorio)
                <tr>
                    <td>{{$relatorio->nu_ordem}}</td>
                    <td>{{$relatorio->protocolo_pedido}}</td>
                    <td>{{$relatorio->no_serventia}}</td>
                    <td>{{$relatorio->cidade_serventia}}</td>
                    <td>{{$relatorio->no_alcada}}</td>
                    <td>{{$relatorio->fase_etapa_acao_futuro}}</td>
                    <td>{{$relatorio->dia_corrido}}</td>
                    <td>{{$relatorio->dia_util}}</td>
                </tr>
            @endforeach
        @endif
    </table>
    <br />
  <table>
      <tr>
          <td colspan="6">
              Data e hora geração do relatório: {{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}
          </td>
      </tr>
      <tr>
          <td colspan="6">
              Usuário emissor: {{Auth::User()->no_usuario}}
          </td>
      </tr>
  </table>

@endsection