@extends('layout.xls')

@section('content')
	<table>
		<td colspan="6"><h2>Notificações de alienação fiduciária / Serviços</h2></td>
		<td colspan="2" align="right" valign="middle">Data de emissão: {{\Carbon\Carbon::now()->format('d/m/Y')}}
	</table>
	<table>
	    <thead>
		    <tr>
                <th>Protocolo</th>
                <th>Protocolo Interno</th>
                <th>Contrato</th>
                <th>Mutuário</th>
                <th>Serventia</th>
                <th>Cidade</th>
                <th>Alçada</th>
                <th>Etapa atual</th>
                <th>Dias corridos</th>
                <th>Dias úteis</th>
				<th>Data última movimentação</th>
		    </tr>
	    </thead>
	    <tbody>
			@if(count($alienacoes)>0)
	            @foreach($alienacoes as $alienacao)
	            	<tr>
	            		<td>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
						<td>
							@if($alienacao->protocolo())
								{{$alienacao->protocolo()->de_texto_curto_acao}}
							@endif
						</td>
	                    <td>{{$alienacao->numero_contrato}}</td>
						<td>{{$alienacao->alienacao_devedor[0]->no_devedor}}</td>
						<td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}</td>
						<td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</td>
						<td>
                        	<?php
                        	switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
                        		case 55: case 57: case 67:
                        			echo 'Serventia';
                        			break;
                        		default:
                        			echo 'Caixa';
                        			break;
                        	}
                        	?>
                        </td>
						<td>{{$alienacao->etapa_atual()}}</td>
	                    <td>{{$alienacao->tempo_ultimo_andamento()}}</td>
	                    <td>{{$alienacao->tempo_ultimo_andamento(true)}}</td>
						<td>{{Carbon\Carbon::parse($alienacao->alienacao_pedido->alienacao_andamento_situacao->dt_cadastro)->format('d/m/Y')}}</td>
					</tr>
	            @endforeach
	        @endif
	    </tbody>
	</table>
@endsection