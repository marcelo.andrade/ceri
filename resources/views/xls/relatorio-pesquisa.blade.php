@extends('layout.xls')

@section('content')
    <table>
        <td colspan="4"><h2>Pesquisa Eletrônica</h2></td>
        <td colspan="2" align="center" valign="middle">Data de emissão: {{\Carbon\Carbon::now()->format('d/m/Y')}}
    </table>
    <table class="table table-striped table-bordered small">
        <thead>
        <tr>
            <th>Protocolo</th>
            <th>Data do pedido</th>
            <th>Item pesquisado</th>
            <th>Solicitante</th>
            <th>Status</th>
            <th>Período de disponibilização</th>
        </tr>
        </thead>
        <tbody>
        @if(count($todas_pesquisas)>0)
            @foreach($todas_pesquisas as $pesquisa)
                <tr>
                    <td>{{$pesquisa->pedido->protocolo_pedido}}</td>
                    <td>{{\Carbon\Carbon::parse($pesquisa->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                    <td>{{$pesquisa->de_chave_pesquisa}}</td>
                    <td>{{$pesquisa->pedido->usuario->no_usuario}} - {{$pesquisa->pedido->pessoa_origem->no_pessoa}}</td>
                    <td>{{$pesquisa->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    <td>{{\Carbon\Carbon::parse($pesquisa->pedido->dt_pedido)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($pesquisa->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                    @endforeach
                    @endif
                </tr>
        </tbody>
    </table>
@endsection