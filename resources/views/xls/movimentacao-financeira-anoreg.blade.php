<?php $total = 0 ?>
@extends('layout.xls')
@section('content')
    <table>
        <tr>
            <th colspan="3" align="center" valign="middle"><img src="images/caixa-logo.png" width="150"/></th>
        </tr>
        <tr>
            <th colspan="2" align="right" valign="middle">Data de emissão:</th>
            <th align="right" valign="middle">{{\Carbon\Carbon::now()->format('d/m/Y')}}</th>
        </tr>
    </table>
    <table>
        <thead>
        <tr>
            <th align="right" valign="middle" style="width: 20px">Nº</th>
            <th align="right" valign="middle">SR</th>
            <th>Agência</th>
            <th>Nº S.S.</th>
            <th>Nº Protocolo</th>
            <th align="right" valign="middle">Seq. Serviço</th>
            <th>Contrato</th>
            <th>Nome Mutuário</th>
            <th>Valor</th>
            <th>TD</th>
            <th>Descrição</th>
            <th>Status</th>
            <th>Andamento atual</th>
        </tr>
        </thead>
        <tbody>
        @if(count($tabela_acomp_pagamento_caixa)>0)
            @foreach ($tabela_acomp_pagamento_caixa as $relatorio)
                <tr>
                    <td align="right" valign="middle">{{$relatorio->nu_ordem}}</td>
                    <td align="right" valign="middle">{{$relatorio->sr}}</td>
                    <td>{{$relatorio->agencia}}</td>
                    <td>{{$relatorio->nss}}</td>
                    <td>{{$relatorio->protocolo_pedido}}</td>
                    <td align="right" valign="middle">{{$relatorio->seq_servico}}</td>
                    <td>{{$relatorio->numero_contrato}}</td>
                    <td>{{$relatorio->no_devedor}}</td>
                    <td align="right" valign="middle">{{formatar_valor(str_replace(',','.',$relatorio->va_tx_servico_em_aberto))}}</td>
                    <td align="right" valign="middle">{{$relatorio->td}}</td>
                    <td>{{$relatorio->descricao}}</td>
                    <td>{{$model_relatorio->pedido($relatorio->protocolo_pedido)->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    <td>{{$model_relatorio->andamento_atual($model_relatorio->pedido($relatorio->protocolo_pedido)->id_pedido, 'P','T','D')}}</td>
                </tr>
                <?php $total += $relatorio->va_tx_servico_em_aberto ?>
            @endforeach
        @endif
        </tbody>
        <tfoot>
        <tr>
            <th colspan="8">TOTAL GERAL</th>
            <th align="right">{{formatar_valor($total)}}</th>
            {{--<th>=SUM(I5:I{{(count($tabela_acomp_pagamento_caixa)+4)}})</th>--}}
        </tr>
        </tfoot>
    </table>
@endsection