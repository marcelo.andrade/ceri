@php $total = 0 @endphp
@extends('layout.xls')
@section('content')
	<table>
		<td colspan="7"><h2>Notificação de alienação fiduciária / Devolução de produto (Diligência não efetuada) / Devoluções efetuadas</h2></td>
		<td colspan="2" align="right" valign="middle">Data de emissão: {{\Carbon\Carbon::now()->format('d/m/Y')}}
	</table>
	<table>
	    <thead>
		<tr class="gradient01">
			<th>Protocolo</th>
			<th>Produto</th>
			@if($class->id_tipo_pessoa == 9)
				<th>Serventia</th>
			@endif
			<th>Data da devolução</th>
			<th>Valor da devolução</th>
			<th>Justificativa</th>
		</tr>
		</thead>
		<tbody>
		@forelse($alienacoes_valores as $alienacao_valor)
			<tr>
				<td>
					<input type="hidden" name="ids_alienacoes[]" class="alienacao" value="{{ $alienacao_valor->alienacao->id_alienacao }}" />
					{{ $alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido }}
				</td>
				<td>{{ $alienacao_valor->no_produto_item }}</td>
				@if($class->id_tipo_pessoa == 9)
					<td>{{ $alienacao_valor->alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa }}</td>
				@endif
				<td>{{ \Carbon\Carbon::parse($alienacao_valor->dt_repasse_devolucao)->format('d/m/Y') }}</td>
				<td><span class="real">{{ formatar_valor($alienacao_valor->va_repasse_devolucao) }}</span></td>
				<td>{{ $alienacao_valor->de_repasse_devolucao }}</td>
			</tr>
            @php $total += str_replace(',','.',$alienacao_valor->va_repasse_devolucao) @endphp
		@empty
			<tr>
				<td colspan="5">
					<div class="single alert alert-danger">
						<i class="glyphicon glyphicon-remove"></i>
						<div class="mensagem">
							Nenhuma notificação foi encontrada.
						</div>
					</div>
				</td>
			</tr>
		@endforelse
	    </tbody>
		</tfoot>
		<tr>
			<th colspan="{{ $class->id_tipo_pessoa == 9 ? '4':'3' }}">TOTAL GERAL</th>
			<th>{{ formatar_valor($total) }}</th>
		</tr>
		</tfoot>
	</table>
@endsection