<?php $total = 0 ?>
@extends('layout.xls')
@section('content')
    <table id="relatorio" class="table table-striped table-bordered small">
        <thead>
        <tr>
            <td></td>
            <td colspan="2"><img src="images/caixa-logo.png" width="120"/></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="4">Data Entrega: {{\Carbon\Carbon::now()->format('d/m/Y')}}</td>
        </tr>
        <tr class="gradient01">
            <th></th>
            <th>SR</th>
            <th>Agência</th>
            <th>Nº S.S</th>
            <th>NºProtocolo</th>
            <th>Seq. Serviço</th>
            <th>Contrato</th>
            <th>Nome Mutuário</th>
            <th>Valor emolumento aprovado</th>
            <th>TD</th>
            <th>Descrição</th>
            <th>Status</th>
            <th>Andamento atual</th>
        </tr>
        </thead>
        <tbody>
        @if (count($tabela)>0)
            @foreach ($tabela as $relatorio)
                <tr>
                    <td></td>
                    <td style="width: 15px" align="left">{{$relatorio->sr}}</td>
                    <td>{{$relatorio->agencia}}</td>
                    <td>{{$relatorio->nss}}</td>
                    <td>{{$relatorio->protocolo_pedido}}</td>
                    <td>{{$relatorio->seq_servico}}</td>
                    <td align="left">{{$relatorio->numero_contrato}}</td>
                    <td align="left">{{$relatorio->no_devedor}}</td>
                    <td align="right">{{formatar_valor(str_replace(',','.',$relatorio->va_tx_servico_em_aberto))}}</td>
                    <td>{{$relatorio->td}}</td>
                    <td>{{$relatorio->descricao}}</td>
                    <td>{{$model_relatorio->pedido($relatorio->protocolo_pedido)->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    <td>{{$model_relatorio->andamento_atual($model_relatorio->pedido($relatorio->protocolo_pedido)->id_pedido, 'P','T','D')}}</td>
                </tr>
                <?php $total += str_replace(',','.',$relatorio->va_tx_servico_em_aberto) ?>
            @endforeach
        @else
            <tr>
                <td colspan="24">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum registro foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
        </tfoot>
        <tr>
            <th></th>
            <th colspan="7">TOTAL GERAL</th>
            <th align="right">{{formatar_valor($total)}}</th>
            {{--<th>=SUM(H4:H{{(count($tabela)+3)}})</th>--}}
        </tr>
        </tfoot>
    </table>
@stop

