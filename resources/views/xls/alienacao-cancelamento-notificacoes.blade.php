@extends('layout.xls')

@section('content')
	<table>
		<td colspan="3"><h1>Notificações / Cancelamento de contratos</h1></td>
		<td colspan="3" align="right" valign="middle">Data de emissão: {{\Carbon\Carbon::now()->format('d/m/Y')}}
	</table>
	<table>
	    <thead>
		    <tr>
		        <th>Contrato</th>
				<th>Data do pedido</th>
		        {{--<th>Prenotação</th>--}}
		        <th>Protocolo (CERI)</th>
		        <th>Devedor(es)</th>
		        <th>Status</th>
		        <th>Valor</th>
		    </tr>
	    </thead>
	    <tbody>
			@if(count($alienacoes)>0)
	            @foreach($alienacoes as $alienacao)
	            	<tr>
	                    <td valign="middle">{{$alienacao->numero_contrato}}</td>
						<td>{{\Carbon\Carbon::parse($alienacao->alienacao_pedido->pedido->dt_pedido)->format('d/m/Y')}}</td>
						{{--<td valign="middle">{{$alienacao->prenotacao()['de_texto_curto_acao']}}</td>--}}
	                    <td valign="middle">{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
	                    <td class="wrap-text" valign="middle">
	                        @if(count($alienacao->alienacao_devedor)>0)
	                        	<?php
	                        	$i=1;
	                        	?>
	                            @foreach($alienacao->alienacao_devedor as $devedor)
	                            	{{$devedor->no_devedor}}
	                                @if($i<count($alienacao->alienacao_devedor))
	                                	<br />
	                                @endif
	                                <?php
	                                $i++;
	                                ?>
	                            @endforeach
	                        @endif
	                    </td>
	                    <td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
	                    <td valign="middle">{{$alienacao->alienacao_pedido->pedido->va_pedido}}</td>
					</tr>
	            @endforeach
	        @endif
	    </tbody>
	    <tfoot>
	    	<tr>
	    		<th colspan="5"></th>
	   			<th>=SUM(F3:F{{(count($alienacoes)+2)}})</th>
	   		</tr>
	    </tfoot>
	</table>
@endsection