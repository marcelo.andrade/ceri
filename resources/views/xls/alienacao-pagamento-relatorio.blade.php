@extends('layout.xls')

@section('content')
    <div>
        <h3>RELATÓRIO DE PAGAMENTO</h3>
        <h4>DATA DO PAGAMENTO {{\Carbon\Carbon::parse($alienacao_pagamento->dt_pagamento)->format('d/m/Y')}}</h4>
        <table>
            <thead>
            <tr style="text-align: left">
                <th width="50">Protocolo/Protocolo Int.</th>
                <th width="15">Data</th>
                <th width="20">Contrato</th>
                <th width="25">Devedores</th>
                <th width="15">Valor Pago</th>
                <th width="15">Status</th>
            </tr>
            </thead>
            <tbody>
            @if (count($alienacao_pagamento->alienacoes)>0)
                @foreach ($alienacao_pagamento->alienacoes as $alienacao)
                    <tr>
                        <td>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}} / {{$alienacao->prenotacao()->de_texto_curto_acao or ''}}</td>
                        <td>{{Carbon\Carbon::parse($alienacao->alienacao_pedido->pedido->dt_pedido)->format('d/m/Y')}}</td>
                        <td>{{$alienacao->numero_contrato}}</td>
                        <td>
                            @if(count($alienacao->alienacao_devedor)>0)
                                @foreach($alienacao->alienacao_devedor as $devedor)
                                    {{$devedor->no_devedor}}
                                @endforeach
                            @endif
                        </td>
                        <td>{{formatar_valor($alienacao->valor_pago($alienacao_pagamento->id_alienacao_pagamento))}}</td>
                        <td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7">
                        <div>
                            Nenhuma notificação foi encontrada.
                        </div>
                    </td>
                </tr>
            @endif
            </tbody>
            <tfoot>
            <tr></tr>
            <tr>
                <th colspan="4" align="left">VALOR PAGO</th>
                <th>{{formatar_valor($alienacao_pagamento->va_pagamento)}}</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

