@extends('layout.home')

@section('scripts')
@endsection

@section('content')
	<div id="index-slide" class="carousel slide carousel-fade" data-ride="carousel" data-interval="4000">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background-image:url('{{asset('slides/slide01.jpg')}}');">
                <div class="carousel-caption">
                	<div class="box" style="width:300px">
                		<h2 style="padding-bottom:10px;margin-bottom:10px">SERVIÇOS<br />DE CARTÓRIO<br />ONLINE</h2>
                		<p>Conheça a Central Eletrônica de Registro de Imóveis - CERI, canal oficial de solicitações homologado pelo Conselho Nacional de Justiça - CNJ e Justiça do Estado do Mato Grosso do Sul</p>
                	</div>
                </div>
            </div>
        </div>
    </div>
    <section id="index-servicos" class="conteudo">
    	<div class="container">
	    	<h3 class="titulo text-center">
	    		SEJA BEM-VINDO À CENTRAL ELETRÔNICA DE REGISTRO DE IMÓVEIS
	    		<p class="info">Selecione um serviço abaixo.</p>
	    	</h3>
	    	<ul class="servicos list-unstyled" id="servicos">
	    		<li class="col-md-3">
	    			<a href="javascript:void(0);" data-toggle="modal" data-target="#pesquisa-eletronica" class="box text-center">
	    				<i class="fa fa-search"></i>
	    				<h4>PESQUISA ELETRÔNICA</h4>
	    				<p>Pesquise por imóveis em uma base centralizada.</p>
	    				<button class="btn btn-primary">Saiba mais</button>
	    			</a>
	    		</li>
	    		<li class="col-md-3">
					<a href="javascript:void(0);" data-toggle="modal" data-target="#certidao-eletronica" class="box text-center">
	    				<i class="fa fa-file-text-o"></i>
	    				<h4>CERTIDÃO ELETRÔNICA</h4>
	    				<p>Emita as certidões dos seus imóveis sem sair de casa.</p>
	    				<button class="btn btn-primary">Saiba mais</button>
	    			</a>
	    		</li>
	    		<li class="col-md-3">
					<a href="javascript:void(0);" data-toggle="modal" data-target="#matricula-digital" class="box text-center">
	    				<i class="fa fa-file-image-o"></i>
	    				<h4>MATRÍCULA DIGITAL *</h4>
	    				<p>Visualize a matrícula imobiliária do seu imóvel de forma rápida.</p>
	    				<button class="btn btn-primary">Saiba mais</button>
	    			</a>
	    		</li>
	    		<li class="col-md-3">
					<a href="javascript:void(0);" data-toggle="modal" data-target="#protocolo-eletronico" class="box text-center">
	    				<i class="fa fa-laptop"></i>
	    				<h4>PROTOCOLO ELETRÔNICO</h4>
	    				<p>Envie documentos eletrônicos de forma digital.</p>
	    				<button class="btn btn-primary">Saiba mais</button>
	    			</a>
	    		</li>
	    	</ul>
	    </div>
    </section>
    <section id="index-acessar" class="conteudo">
	    <div class="container">
	    	<div class="destaque col-md-5">
	    		<h2>
	    			<div>Sua certidão com <b>rapidez</b>, <b>segurança</b> e <b>economia</b></div>
	    		</h2>
	    		<div class="laptop">
	    			<img src="{{asset('images/home/laptop01.png')}}" class="img-responsive" />
	    		</div>
	    	</div>
	    	<div class="col-md-7">
	    		<h3 class="titulo">SERVIÇOS CARTORIAIS DE IMÓVEIS JÁ PODEM SER FEITOS PELA INTERNET</h3>
	    		<p>Antes, para solicitar algum serviço relacionado à Registro de Imóveis, era preciso se dirigir a um Cartório e gastar muito tempo e dinheiro.</p>
	    		<p>Com a Central Eletrônica de Registro de Imóveis – CERI, você poderá solicitar serviços de qualquer lugar com acesso a internet, o que permite uma rápida consulta e maior eficiência na entrega dos serviços cartoriais.</p>
	    		<p>Acesse agora e não perca mais tempo!</p>

	    		<div class="usuario">
		    		<div class="col-md-8">
	                    <h5>ACESSE UTILIZANDO SEU CADASTRO</h5>
						<form name="form-login2" method="post" action="acessar">
							{{csrf_field()}}
							<div class="form-group input-group">
								<span class="input-group-addon" id="addon-email"><i class="glyphicon glyphicon-user"></i></span>
								<input type="text" name="usuario" class="form-control" placeholder="E-mail" value="{{old('usuario')}}" />
							</div>
							<div class="form-group input-group">
								<span class="input-group-addon" id="addon-senha"><i class="glyphicon glyphicon-lock"></i></span>
								<input type="password" name="senha_usuario" class="form-control" placeholder="Senha" />
							</div>
							<a href="{{url('/acessar/lembrar-senha')}}" class="lembrar-senha">Esqueceu sua senha?</a>

                                                        <div class="form-group" id="captcha_login2">
                                                            {!! NoCaptcha::display(['render' => 'onload', 'data-callback' => 'verifyCallbackLogin2']) !!}
                                                        </div>

							<div class="botoes clearfix">
								<input type="button" class="cadastrar btn btn-primary" value="NÃO TENHO CADASTRO">
                                                                <input type="button" class="acessar btn btn-primary" value="ACESSAR" id="btn_login2">
							</div>
						</form>
	                </div>
					<div class="col-md-4 text-center certificado">
						<img src="{{asset('images/home/e-cpf01.png')}}" height="100" />
						<a href="{{url('/acessar/certificado')}}" class="acessar-certificado btn btn-primary">ACESSAR COM CERTIFICADO</a>
					</div>
	            </div>
	    	</div>
	    </div>
    </section>
@endsection