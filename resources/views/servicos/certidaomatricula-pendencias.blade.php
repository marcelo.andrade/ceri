<div id="pedidos-pendentes" class="fieldset-group col-md-12 clearfix" @if (count($certidoes_pendentes)<=0) style="display:none;" @endif>
    <fieldset>
        <legend>Pedidos pendentes (Não enviados ao cartório)</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="pedidos-pendentes" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Protocolo</th>
                            <th>Data do pedido</th>
                            <th>Cartório</th>
                            <th>Validade</th>
                            <th>Valor</th>
                            <th>Ações</th>                    
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($certidoes_pendentes)>0)
                        	@foreach ($certidoes_pendentes as $certidao_pendente)
                                <tr>
                                    <td>{{$certidao_pendente->pedido->protocolo_pedido}}</td>
                                    <td>{{$certidao_pendente->pedido->dt_pedido}}</td>
                                    <td>{{$certidao_pendente->pedido->serventias[0]->no_serventia}}</td>
                                    <td>{{Carbon\Carbon::parse($certidao_pendente->pedido->dt_cadastro)->addDays(30)->format('d/m/Y')}}</td>
                                    <td>
                                        @if(isset($certidao_pendente->pedido->va_pedido))
                                            <span class="real">{{$certidao_pendente->pedido->va_pedido}}</span>
                                        @else
                                            ?
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu black">
                                                <li><a href="#" class="detalhes-certidao" data-toggle="modal" data-target="#detalhes-certidao" data-idpedido="{{$certidao_pendente->pedido->id_pedido}}" data-protocolo="{{$certidao_pendente->pedido->protocolo_pedido}}">Ver detalhes</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="enviar-pedido" data-idpedido="{{$certidao_pendente->pedido->id_pedido}}">Enviar pedido</a></li>
                                                <li><a href="#" class="cancelar-pedido" data-idpedido="{{$certidao_pendente->pedido->id_pedido}}" data-protocolo="{{$certidao_pendente->pedido->protocolo_pedido}}">Cancelar pedido</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                        	@endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
@if (count($certidoes_a_confirmar)>0)
    <div class="fieldset-group col-md-12 clearfix">
        <fieldset>
            <legend>Pedidos à confirmar (Já respondidos pelo cartório)</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="pedidos-confirmar" class="table table-striped table-bordered small">
                        <thead>
                            <tr class="gradient01">
                                <th>Protocolo</th>
                                <th>Data do pedido</th>
                                <th>Cartório</th>
                                <th>Validade</th>
                                <th>Valor</th>
                                <th>Ações</th>                    
                            </tr>
                        </thead>
                        <tbody>
							@foreach ($certidoes_a_confirmar as $certidao_a_confirmar)
                                <tr>
                                    <td>{{$certidao_a_confirmar->pedido->protocolo_pedido}}</td>
                                    <td>{{$certidao_a_confirmar->pedido->dt_pedido}}</td>
                                    <td>{{$certidao_a_confirmar->pedido->serventias[0]->no_serventia}}</td>
                                    <td>{{Carbon\Carbon::parse($certidao_a_confirmar->pedido->dt_cadastro)->addDays(30)->format('d/m/Y')}}</td>
                                    <td>
                                        @if(isset($certidao_a_confirmar->pedido->pedido_valor))
                                            <span class="real">{{$certidao_a_confirmar->pedido->pedido_valor->va_pedido}}</span>
                                        @else
                                            ?
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu black">
                                                <li><a href="#" class="detalhes-certidao" data-toggle="modal" data-target="#detalhes-certidao" data-idpedido="{{$certidao_a_confirmar->pedido->id_pedido}}" data-protocolo="{{$certidao_a_confirmar->pedido->protocolo_pedido}}">Ver detalhes</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="confirmar-pedido" data-idpedido="{{$certidao_a_confirmar->pedido->id_pedido}}">Confirmar pedido</a></li>
                                                <li><a href="#" class="cancelar-pedido" data-idpedido="{{$certidao_a_confirmar->pedido->id_pedido}}" data-protocolo="{{$certidao_a_confirmar->pedido->protocolo_pedido}}">Cancelar pedido</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                        	@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </div>
@endif