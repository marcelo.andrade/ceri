@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.certidaomatricula.cartorio.js?v=3.2.0')}}"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Matrícula digital <span class="small">/ Serviços</span></h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                @if(env('APP_ENV') != 'local')
                    <div class="erros alert alert-info single">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">
                            Prezado (a),<br />
                            De acordo com PROVIMENTO N° 146, de 18 de novembro de 2016, Capítulo I, art. 11º § 3º, o Módulo <b>"Visualização de Matrícula on-line"</b> da CERI-MS, será implantado e disponibilizado para consulta até o dia 1º de janeiro de 2018.<br /><br />
                            Assim que disponível, o (a) senhor (a) será comunicado (a) através de e-mail  a disponibilidade deste módulo.
                        </div>
                    </div>
                @else
                    <form name="form-filtro" method="post" action="certidao-matricula#pedidos" class="clearfix">
                        {{csrf_field()}}
                        @include('servicos.certidaomatricula-filtro')
                    </form>
                    @include('servicos.cartorio.certidaomatricula-historico')
                @endif
            </div>
        </div>
    </div>
    <div id="nova-resposta" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                	<h4 class="modal-title">Responder matrícula digital - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                	<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="nova-resposta" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Responder certidão - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="resultado-certidao" class="sub-modal total-height modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Resultado matrícula digital - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    {{--<button id="resultado-certificado-digital" type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#certificado-detalhes" style="display:none"><i class="glyphicon glyphicon-check"></i> O documento foi assinado digitalmente</button>--}}
                    <a id="resultado-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do documento</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="certificado-detalhes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Dados do certificado<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection