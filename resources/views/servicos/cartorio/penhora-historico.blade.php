<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th>Protocolo</th>
				<th>Número do Processo</th>
                <th>Data do pedido</th>
                <th>Solicitante</th>
                <th>Status</th>
                <th>Tipo</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
			@if (count($todas_penhoras)>0)
                @foreach ($todas_penhoras as $penhora)
                    <tr id="{{$penhora->pedido->id_pedido}}" @if(count($penhora->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
                        <td>
                            {{$penhora->pedido->protocolo_pedido}}
                            @if(count($penhora->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                        </td>
                        <td>{{$penhora->numero_processo}}</td>
                        <td>{{$penhora->pedido->dt_pedido}}</td>
                        <td>{{$penhora->pedido->pessoa_origem->no_pessoa}}</td>
                        <td>{{$penhora->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                        <td>{{$penhora->tipo_penhora->no_tipo_penhora}}</td>
                        <td class="options">
                            <a href="#nova-resposta" class="btn btn-primary" data-toggle="modal" data-target="#nova-resposta" data-idpedido="{{$penhora->pedido->id_pedido}}" data-protocolo="{{$penhora->pedido->protocolo_pedido}}">
                                @if ($penhora->pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_FINALIZADO)
                                    Detalhes
                                @else
                                    Responder
                                @endif
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhuma penhora foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>