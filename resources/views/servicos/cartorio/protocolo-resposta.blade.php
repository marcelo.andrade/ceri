<div class="fieldset-group clearfix">
    <div class="col-md-4">
        <select name="id_cidade" class="form-control pull-left" disabled>
            <option>{{$pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
        </select>
    </div>
    <div class="col-md-5">
        <select name="id_serventia" class="form-control pull-left" disabled>
            <option>{{$pedido->pedido_pessoa->no_serventia}}</option>
        </select>
    </div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Arquivo do protocolo</legend>
        <div class="col-md-12">
            @if(count($pedido->protocolo->arquivos)>0)
                @foreach($pedido->protocolo->arquivos as $arquivo)
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#arquivo-protocolo" data-idarquivo="{{$arquivo->id_protocolo_arquivo}}" data-protocolo="{{$pedido->protocolo_pedido}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</a>
                @endforeach
            @endif
        </div>
    </fieldset>
</div>
<?php
if ($pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_PROTOCOLADO) {
	$disabled_resposta = 'disabled';
} else {
	$disabled_resposta = '';
}
?>
<div class="fieldset-group clearfix">
    <form name="form-nova-resposta" method="post" action="" class="clearfix" enctype="multipart/form-data">
        <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
        <div class="erros-resposta alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset class="col-md-12">
                <legend>Dados do protocolo</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Natureza formal</label>
                        <select name="id_protocolo_natureza" class="form-control" {{$disabled_resposta}}>
                            <option value="0">Selecione</option>
                            @if(count($naturezas)>0)
                                @foreach ($naturezas as $natureza)
                                    <option value="{{$natureza->id_protocolo_natureza}}" {{($natureza->id_protocolo_natureza==$pedido->protocolo->id_protocolo_natureza?'selected="selected"':'')}}>{{$natureza->no_protocolo_natureza}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Assunto</label>
                        <input type="text" name="no_assunto" class="form-control" value="{{$pedido->protocolo->no_assunto}}" {{$disabled_resposta}} />
                    </div>
                </div>
                <div class="fieldset-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Nome do apresentante</label>
                        <input type="text" name="no_apresentante" class="form-control" value="{{$pedido->protocolo->no_apresentante}}" {{$disabled_resposta}} />
                    </div>
                    <div class="col-md-2">
                        <label class="small">Tipo de documento</label>
                        <div class="radios col-md-12">
                            <div class="radio radio-inline">
                                <input type="radio" name="tp_pessoa_apresentante" id="tp_pessoa_apresentante_F" value="F" @if($pedido->protocolo->tp_pessoa_apresentante=='F') checked @endif {{$disabled_resposta}}>
                                <label for="tp_pessoa_apresentante_F" class="small">
                                    CPF
                                </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" name="tp_pessoa_apresentante" id="tp_pessoa_apresentante_J" value="J" @if($pedido->protocolo->tp_pessoa_apresentante=='J') checked @endif {{$disabled_resposta}}>
                                <label for="tp_pessoa_apresentante_J" class="small">
                                    CNPJ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="small">Documento do apresentante <span></span></label>
                        <input type="text" name="nu_cpf_cnpj_apresentante" class="form-control" value="{{$pedido->protocolo->nu_cpf_cnpj_apresentante}}" disabled />
                    </div>
                </div>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Conteúdo do protocolo</label>
                        <textarea name="de_conteudo" class="form-control" rows="10" {{$disabled_resposta}}>{{$pedido->protocolo->de_conteudo}}</textarea>
                    </div>
                </div>
            </fieldset>
        </div>
        @if($disabled_resposta=='')
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Imóveis vinculados</legend>
                    <div class="erros-imoveis alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                        <div class="menssagem"></div>
                    </div>
                    <div class="col-md-5">
                        <label class="small">Matrícula do imóvel</label>
                        <div class="input-group">
                            <input type="text" name="matricula_bem_imovel" class="form-control" />
                            <div class="input-group-btn">
                                <button type="button" class="incluir-imovel btn btn-default btn-inline" >
                                    Incluir
                                </button>
                                <button type="button" class="btn btn-success btn-inline" data-toggle="modal" data-target="#novo-imovel">
                                    Novo imóvel
                                </button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        @endif
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Imóveis</legend>
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table id="imoveis-protocolo" class="table table-striped table-bordered small">
                            <thead>
                                <tr class="gradient01">
                                    <th width="10%">Matrícula</th>
                                    <th width="80%">Proprietários</th>
                                    <th width="10%">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@if(count($pedido->protocolo->bem_imoveis)>0)
                                	@foreach($pedido->protocolo->bem_imoveis as $bem_imovel)
                                        <tr>
                                            <td>{{$bem_imovel->matricula_bem_imovel}}</td>
                                            <td>
                                            	@if(count($bem_imovel->proprietarios)>0)
                                                	@foreach($bem_imovel->proprietarios as $proprietario)
                                                    	<span class="label label-primary">{{$proprietario->proprietario_bem_imovel->no_proprietario_bem_imovel}} ({{$proprietario->proprietario_bem_imovel->nu_cpf_cnpj}})</span> 
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                            	<div class="btn-group">
                                                    <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                                                    Ações <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu black">
                                                        <li><a href="#" data-toggle="modal" data-target="#detalhes-imovel" data-idbemimovel="{{$bem_imovel->id_bem_imovel}}" data-matriculabemimovel="{{$bem_imovel->matricula_bem_imovel}}">Ver detalhes</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                	@endforeach
                            	@endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
        @if($disabled_resposta=='')
            <div class="botoes fieldset-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar resposta" />
                </div>
            </div>
		@endif
    </form>
</div>
<div class="fieldset-group clearfix">
    <form name="form-nova-observacao" method="post" action="" class="clearfix">
        <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
        <fieldset class="col-md-12">
            <legend>Inserir observação</legend>
            <div class="erros-observacao alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    <label class="small">Observação</label>
                    <textarea name="de_observacao" class="form-control" rows="3"></textarea>
                </div>
            </div>
            <div class="botoes fieldset-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar observação" />
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico de observações</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-observacoes" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($pedido->protocolo->observacoes)>0)
                            @foreach($pedido->protocolo->observacoes as $observacao)
                                <tr>
                                    <td>{{$observacao->usuario_cad->no_usuario}}</td>
                                    <td>{{Carbon\Carbon::parse($observacao->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                    <td>{{$observacao->de_observacao}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>