<div class="fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Pesquisa</legend>
        <div class="fieldset-group clearfix">
            <div class="col-md-5">
                <div class="form-group">
                    <div class="radio radio-inline">
                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="matricula" value="4" disabled @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==4) checked @endif>
                        <label for="matricula" class="small">
                            Matrícula
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="cpf" value="1" disabled @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==1) checked @endif>
                        <label for="cpf" class="small">
                            CPF
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="cnpj" value="2" disabled @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==2) checked @endif>
                        <label for="cnpj" class="small">
                            CNPJ
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="nome" value="3" disabled @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==3) checked @endif>
                        <label for="nome" class="small">
                            Nome
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="small">
                        Documento identificador 
                        <span>
                            (<?php
                                switch ($pedido->certidao->id_tipo_certidao_chave_pesquisa) {
                                    case 1:
                                        echo 'CPF';
                                        break;
                                    case 2:
                                        echo 'CNPJ';
                                        break;
                                    case 3:
                                        echo 'Nome';
                                        break;
                                    case 4:
                                        echo 'Matrícula';
                                        break;
                                }
                            ?>)
                        </span>
                    </label>
                    <input type="text" name="de_chave_certidao" class="form-control" value="{{$pedido->certidao->de_chave_certidao}}" disabled />
                </div>
            </div>
            <div class="col-md-5">
                <fieldset>
                    <legend>Tipo</legend>
                    <div class="col-md-12">
						@if(count($tipos)>0)
						    @foreach ($tipos as $tipo)
						        <div class="radio">
						            <input type="radio" name="id_tipo_certidao" id="id_tipo_certidao{{$tipo->id_tipo_certidao}}" value="{{$tipo->id_tipo_certidao}}" disabled @if ($pedido->certidao->id_tipo_certidao==$tipo->id_tipo_certidao) checked @endif>
						            <label for="id_tipo_certidao{{$tipo->id_tipo_certidao}}" class="small">
						                {{$tipo->no_tipo_certidao}}
						            </label>
						        </div>
						    @endforeach
						@endif
                    </div>
                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset>
                    <legend>Pedido</legend>
                    <div class="col-md-12">
                        <div class="radio">
                            <input type="radio" id="tipo1" disabled>
                            <label for="tipo1" class="small">
                                Emissão
                            </label>
                        </div>
                        <div class="radio">
                            <input type="radio" id="tipo2" checked disabled>
                            <label for="tipo2" class="small">
                                Precificação
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="fieldset-group fix-height-group clearfix">
            @if($pedido->certidao->periodo_certidao)
                <div class="col-md-6 fix-height">
                    <fieldset>
                        <legend>Período da pesquisa</legend>
                        <div class="col-md-6">
                            <label class="small">Período</label>
                            <input type="text" name="periodo_certidao" class="form-control" value="{{$pedido->certidao->periodo_certidao->no_periodo_certidao}}" disabled />
                        </div>
                        <div class="col-md-6">
                            <label class="small">Data do cadastro</label>
                            <input type="text" name="dt_cadastro" class="form-control" value="{{Carbon\Carbon::parse($pedido->certidao->dt_cadastro)->format('d/m/Y H:i')}}" disabled />
                        </div>
                    </fieldset>
                </div>
            @endif
            <div class="col-md-6 fix-height">
                <fieldset>
                    <legend>Protocolo</legend>
                    <div class="col-md-6">
                        <label class="small">Protocolo</label>
                        <input type="text" name="protocolo_certidao" class="form-control" value="{{$pedido->protocolo_pedido}}" disabled />
                    </div>
                    <div class="col-md-6">
                        <label class="small">Data do protocolo</label>
                        <input type="text" name="dt_protocolo" class="form-control" value="{{$pedido->dt_pedido}}" disabled />
                    </div>
                </fieldset>
            </div>
        </div>
        @if ($pedido->id_alcada==2)
            <?php
            if ($pedido->certidao->processo) {
                $numero_processo = $pedido->certidao->processo->numero_processo;
            } else {
                $numero_processo = '';
            }
            ?>
            <div class="judiciario fieldset-group clearfix">
                <div class="col-md-12 fix-height">
                    <fieldset>
                        <legend>Judiciário</legend>
                        <div class="col-md-4">
                            <div class="checkbox">
                                <input type="checkbox" name="in_isenta" id="in_isenta" @if ($pedido->certidao->in_isenta=='S') checked @endif disabled>
                                <label for="in_isenta" class="small">
                                    Isenta de emolumentos
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="checkbox">
                                <input type="checkbox" name="in_penhora" id="in_penhora" @if ($pedido->certidao->in_penhora=='S') checked @endif disabled>
                                <label for="in_penhora" class="small">
                                    Penhora
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="small">Número do processo</label>
                            <input type="text" name="processo" class="form-control" value="{{$numero_processo}}" disabled />
                        </div>
                    </fieldset>
                </div>
            </div>
		@endif
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Andamento interno</legend>
        <form name="form-novo-andamento" method="post" action="" class="clearfix">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
            <div class="erros-andamento alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-6">
                    <fieldset>
                        <legend>Opções</legend>
                        <div class="col-md-12">
                            @if(count($tipos_resposta)>0)
                                @foreach ($tipos_resposta as $tipo_resposta)
                                    <div class="radio">
                                        <input type="radio" name="id_tipo_resposta" id="tipo_resposta_{{$tipo_resposta->id_tipo_resposta}}" value="{{$tipo_resposta->id_tipo_resposta}}" data-nome="Não encontrado">
                                        <label for="tipo_resposta_{{$tipo_resposta->id_tipo_resposta}}" class="small">
                                            {{$tipo_resposta->no_tipo_resposta}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <legend>Observações</legend>
                        <div class="form-group col-md-12">
                            <label class="small">Observação</label>
                            <textarea name="de_resposta" class="form-control" rows="4" maxlength="200"></textarea>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar andamento" />
                </div>
            </div>
        </form>
    </fieldset>
</div>
<?php
$nu_quantidade = 0;
$va_pedido = 0;
$in_positivo_precif = '';
$in_positivo_result = '';
$de_resultado = '';
$no_arquivo = '';
$no_arquivo_p7s = '';
$no_local_arquivo = '';

switch ($pedido->id_situacao_pedido_grupo_produto) {
	case $class::ID_SITUACAO_ANALISE:
		$disabledResultado = 'disabled';
		$disabledPrecificacao = '';
		break;
	case $class::ID_SITUACAO_APROVACAO:
		$disabledResultado = 'disabled';
		$disabledPrecificacao = 'disabled';
        $nu_quantidade = $pedido->pedido_valor->nu_quantidade;
        $va_pedido = $pedido->pedido_valor->va_pedido;
        $in_positivo_precif = $pedido->pedido_valor->in_positivo;
		break;
	case $class::ID_SITUACAO_ENCAMINHADO:
		$disabledResultado = '';
		$disabledPrecificacao = 'disabled';
		break;
    case $class::ID_SITUACAO_FINALIZADO: case $class::ID_SITUACAO_CORRECAO:
        $disabledResultado = 'disabled';
        $disabledPrecificacao = 'disabled';
        $in_positivo_result = $pedido->pedido_resultado->in_positivo;
        $de_resultado = $pedido->pedido_resultado->de_resultado;
        $no_arquivo = $pedido->pedido_resultado->no_arquivo;
        $no_arquivo_p7s = $pedido->pedido_resultado->no_arquivo_p7s;
        $no_local_arquivo = $pedido->pedido_resultado->no_local_arquivo;
        break;
	default:
		$disabledResultado = 'disabled';
		$disabledPrecificacao = 'disabled';
		break;
}
?>
@if($pedido->id_situacao_pedido_grupo_produto!=$class::ID_SITUACAO_ANALISE)
    <div class="resultado fieldset-group clearfix">
        <fieldset class="col-md-12">
            <legend>Resultado da certidão</legend>
            @if($pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_CORRECAO)
                <form name="form-corrigir-resultado" method="post" action="" class="clearfix" enctype="multipart/form-data">
                    <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
                    <input type="hidden" name="id_certidao" value="{{$pedido->certidao->id_certidao}}" />
                    <div class="erros-resultado alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                        <div class="menssagem"></div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-12">
                            <div class="alert alert-warning single">
                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                <div class="menssagem">
                                    Uma das páginas do resultado automático encontra-se corrompida, por favor, envie um novo PDF para finalizar o pedido. <br /><br />
                                    <a href="#" class="btn btn-black" data-toggle="modal" data-target="#resultado-certidao" data-idpedido="{{$pedido->id_pedido}}" data-protocolo="{{$pedido->protocolo_pedido}}">{{$no_arquivo}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Envie um novo arquivo</legend>
                                <div class="col-md-12">
                                    <div class="arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="button input-group-addon btn-file">
                                            <span class="fileinput-new">Selecionar</span>
                                            <span class="fileinput-exists">Alterar</span>
                                            <input type="file" name="no_arquivo">
                                        </span>
                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-12">
                            <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                            <input type="submit" class="btn btn-success pull-right" value="Salvar correção" />
                        </div>
                    </div>
                </form>
            @else
                <form name="form-novo-resultado" method="post" action="" class="clearfix" enctype="multipart/form-data">
                    <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
                    <input type="hidden" name="id_certidao" value="{{$pedido->certidao->id_certidao}}" />
                    <div class="erros-resultado alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                        <div class="menssagem"></div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-3">
                            <fieldset>
                                <legend>Tipo</legend>
                                <div class="col-md-12">
                                    <div class="radio">
                                        <input type="radio" name="in_positivo" id="in_positivo_result_S" value="S" @if($in_positivo_result=='S') checked @endif {{$disabledResultado}}>
                                        <label for="in_positivo_result_S" class="small">
                                            Positivo
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="in_positivo" id="in_positivo_result_N" value="N" @if($in_positivo_result=='N') checked @endif {{$disabledResultado}}>
                                        <label for="in_positivo_result_N" class="small">
                                            Negativo
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="resultado-mensagem col-md-9" @if(isset($pedido->pedido_resultado)) style="display:none" @endif>
                            <fieldset>
                                <legend>Resultado</legend>
                                <div class="col-md-12">
                                    <div class="alert alert-warning single">
                                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                        <div class="menssagem">Selecione um tipo de resultado</div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="resultado-positivo col-md-9" @if(!isset($pedido->pedido_resultado)) style="display:none" @else @if($pedido->pedido_resultado->in_positivo=='N') style="display:none" @endif @endif>
                            <div class="fieldset-group col-md-12">
                                <fieldset>
                                    <legend>
                                        <div class="radio">
                                            <input type="radio" name="tipo_resultado_envio" id="digitar" value="1" @if ($de_resultado!='') checked @endif {{$disabledResultado}}>
                                            <label for="digitar">
                                               <b>Digitar resultado</b>
                                            </label>
                                        </div>                           
                                    </legend>
                                    <div class="col-md-12">
                                        <label class="small">Observação</label>
                                        <textarea name="de_resultado" class="form-control" rows="3" {{$disabledResultado}}>{{$de_resultado}}</textarea>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="fieldset-group col-md-12">
                                <fieldset>
                                    <legend>
                                        <div class="radio">
                                            <input type="radio" name="tipo_resultado_envio" id="upload" value="2" @if ($no_arquivo!='') checked @endif {{$disabledResultado}}>
                                            <label for="upload">
                                               <b>Upload do resultado</b>
                                            </label>
                                        </div>
                                    </legend>
                                    <div class="col-md-12">
                                        <label class="small">Arquivo do resultado</label>
                                    	@if ($no_arquivo=='')
                                            <div class="arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="button input-group-addon btn-file">
                                                    <span class="fileinput-new">Selecionar</span>
                                                    <span class="fileinput-exists">Alterar</span>
                                                    <input type="file" name="no_arquivo" {{$disabledResultado}}>
                                                </span>
                                                <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                                            </div>
                                            @if (Auth::User()->usuario_serventia->serventia->serventia_certificado)
                                                @if (Auth::User()->usuario_serventia->serventia->serventia_certificado->dt_validade_fim>Carbon\Carbon::now())
                                                    <div class="alert alert-success small single">
                                                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                                                        <div class="menssagem">O arquivo será automaticamente assinado com seu certificado digital.</div>
                                                    </div>
                                                @else
                                                    <div class="alert alert-warning small single">
                                                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                                        <div class="menssagem">O arquivo não será assinado, pois o seu certificado está vencido.</div>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="alert alert-danger small single">
                                                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                                                    <div class="menssagem">O arquivo não será assinado, pois você não definiu um certificado digital.</div>
                                                </div>
    										@endif
                                        @else
                                            <div class="link">
                                                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#resultado-certidao" data-idpedido="{{$pedido->id_pedido}}" data-protocolo="{{$pedido->protocolo_pedido}}">{{$no_arquivo}}</a>
                                            </div>
                                            @if ($no_arquivo_p7s!='')
                                                <div class="alert alert-success small single">
                                                    <i class="icon glyphicon glyphicon-ok pull-left"></i>
                                                    <div class="menssagem">O arquivo foi assinado digitalmente.</div>
                                                </div>
    										@else
                                                <div class="alert alert-danger small single">
                                                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                                                    <div class="menssagem">O arquivo não foi assinado digitalmente.</div>
                                                </div>
    										@endif
                                        @endif
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="resultado-negativo col-md-9" @if(!isset($pedido->pedido_resultado)) style="display:none" @else @if($pedido->pedido_resultado->in_positivo=='S') style="display:none" @endif @endif>
                            <fieldset>
                                <legend>Resultado</legend>
                                <div class="col-md-12">
                                    <div class="well" style="margin-bottom:0">Não foram localizados registros com a chave <label class="label label-danger label-sm">{{$pedido->certidao->de_chave_certidao}}</label> pesquisada nesta serventia.</div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    @if(!$disabledResultado)
                        <div class="fieldset-group clearfix">
                            <div class="col-md-12">
                                <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                                <input type="submit" class="btn btn-success pull-right" value="Salvar resultado" />
                            </div>
                        </div>
                    @endif
                </form>
            @endif
        </fieldset>
    </div>
@endif
@if($pedido->produto_itens[0]->produto_item->in_precificacao=='S' and $pedido->certidao->in_isenta=='N')
    <div class="fieldset-group clearfix">
        <fieldset class="col-md-12">
            <legend>Resultado da precificação</legend>
            <form name="form-nova-precificacao" method="post" action="" class="clearfix">
                <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
                <div class="erros-precificacao alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
                <div class="fieldset-group clearfix">
                    <div class="col-md-3">
                        <fieldset>
                            <legend>Tipo</legend>
                            <div class="col-md-12">
                                <div class="radio">
                                    <input type="radio" name="in_positivo" id="in_positivo_precif_S" value="S" @if($in_positivo_precif=='S') checked @endif {{$disabledPrecificacao}}>
                                    <label for="in_positivo_precif_S" class="small">
                                        Positivo
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" name="in_positivo" id="in_positivo_precif_N" value="N" @if($in_positivo_precif=='N') checked @endif {{$disabledPrecificacao}}>
                                    <label for="in_positivo_precif_N" class="small">
                                        Negativo
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-9">
                        <fieldset>
                            <legend>Dados da precificação</legend>
                            <div class="col-md-6">
                                <label class="small">Quantidade de folhas</label>
                                <input type="text" name="nu_quantidade" class="numero100 form-control" value="{{($nu_quantidade>0?$nu_quantidade:'')}}" {{$disabledPrecificacao}} />
                            </div>
                            <div class="col-md-6">
                                <label class="small">Valor da certidão</label>
                                <input type="text" name="va_pedido" class="real form-control" value="{{($va_pedido>0?$va_pedido:'')}}" {{$disabledPrecificacao}} />
                            </div>
    					</fieldset>
                    </div>
                </div>
                @if(!$disabledPrecificacao)
                    <div class="fieldset-group clearfix">
                        <div class="col-md-12">
                            <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                            <input type="submit" class="btn btn-success pull-right" value="Salvar precificação" />
                        </div>
                    </div>
                @endif
            </form>
        </fieldset>
    </div>
@endif
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico da certidão</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($pedido->historico as $historico)
                        	<tr>
                                <td>{{$historico->usuario_cad->no_usuario}}</td>
                                <td>{{Carbon\Carbon::parse($historico->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>{{$historico->de_observacao}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>