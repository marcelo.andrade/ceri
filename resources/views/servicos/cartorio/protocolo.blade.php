@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.protocolo.cartorio.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.bemimovel.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Protocolo eletrônico <span class="small">/ Serviços</span></h4>
            </div>
            <div id="filtro-pesquisa" class="panel-body">
            	<div class="erros alert alert-info single">
                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                    <div class="menssagem">
						Prezado (a),<br />
						De acordo com PROVIMENTO N° 146, de 18 de novembro de 2016, Capítulo I, art. 11º § 3º, o Módulo <b>"Protocolo Eletrônico de Títulos"</b> da CERI-MS, será implantado e disponibilizado para consulta até o dia 1º de janeiro de 2018.<br /><br />
						Assim que disponível, o (a) senhor (a) será comunicado (a) através de e-mail  a disponibilidade deste módulo.
                    </div>
                </div>
            	<?php
				/*
                <form name="form-filtro" method="post" action="protocolo#pedidos" class="clearfix">
                    {{csrf_field()}}
                    <?php //@include('servicos.pesquisa-filtro') ?>
                </form>
                @include('servicos.cartorio.protocolo-historico')
				*/
				?>
            </div>
        </div>
    </div>
    <?php
	/*
    <div id="nova-resposta" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                	<h4 class="modal-title">Responder protocolo - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                	<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="arquivo-protocolo" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Arquivo do protocolo - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <a id="arquivo-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo imóvel</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="incluir-imovel btn btn-success">Cadastrar imóvel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes imóvel - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

	*/
	?>
@endsection