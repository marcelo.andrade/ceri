<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th>Protocolo</th>
				<th>Matrícula</th>
                <th>Data do pedido</th>
                <th>Status</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
			@if (count($todas_certidoes)>0)
                @foreach ($todas_certidoes as $certidao)
                    <tr {{@marcar_linha_vermelha(6,$certidao->pedido->id_pedido, $notificacao)}}>
                        <td>{{$certidao->pedido->protocolo_pedido}} {{@mostrar_sino(6,$certidao->pedido->id_pedido, $notificacao)}}</td>
                        <td>{{$certidao->de_chave_certidao}}</td>
                        <td>{{$certidao->pedido->dt_pedido}}</td>
                		<td>{{$certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                   		<td class="options">
                            <?php
                            switch ($certidao->pedido->id_situacao_pedido_grupo_produto) {
                                case $this::ID_SITUACAO_FINALIZADO:
                                    echo '<a href="#nova-resposta" class="btn btn-success" data-toggle="modal" data-target="#nova-resposta" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Ver detalhes</a>';
                                    break;
                                case $this::ID_SITUACAO_CORRECAO:
                                    echo '<a href="#nova-resposta" class="btn btn-primary" data-toggle="modal" data-target="#nova-resposta" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Corrigir arquivo</a>';
                                    break;
                                default:
                                    echo '<a href="#nova-resposta" class="btn btn-primary" data-toggle="modal" data-target="#nova-resposta" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Responder</a>';
                                    break;
                            }
                            ?>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhuma matrícula foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>