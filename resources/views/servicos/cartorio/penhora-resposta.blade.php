<?php
if ($pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto=='Finalizado') {
	$desativar_prenotacao = 'disabled';
	$desativar_custa = 'disabled';
	$desativar_averbacao = 'disabled';
	$desativar_exigencia = 'disabled';
	$desativar_penhora = 'disabled';
} else {
	$desativar_prenotacao = '';
	$desativar_custa = '';
	$desativar_averbacao = '';
	$desativar_exigencia = '';
    $desativar_penhora = '';
    $desativar_averbacao_documento = '';
}

$codigo_prenotacao              = '';
$dt_prenotacao                  = '';
$dt_vencimento_prenotacao       = '';
$va_custa                       = '';
$de_custa                       = '';
$disabled                       = ( count($pedido->penhora->bens_imoveis)           > 0 )   ? 'disabled' : '';
$disabled_parte_processo        = ( count($pedido->penhora->processo_parte_penhora) > 0 )   ? 'disabled' : '';
$dt_penhora                     = ( $pedido->penhora->dt_penhora                    != "" ) ? formatar_data($pedido->penhora->dt_penhora) : '';
$documentoPenhora               = $pedido->penhora->documentos_penhora;

if($pedido->penhora->penhora_prenotacao) {
	$desativar_prenotacao       = 'disabled';
	$codigo_prenotacao          = $pedido->penhora->penhora_prenotacao->codigo_prenotacao;
	$dt_prenotacao              = Carbon\Carbon::parse($pedido->penhora->penhora_prenotacao->dt_prenotacao)->format('d/m/Y');
	$dt_vencimento_prenotacao   = Carbon\Carbon::parse($pedido->penhora->penhora_prenotacao->dt_vencimento_prenotacao)->format('d/m/Y');
}
if($pedido->penhora->penhora_custa) {
	$desativar_custa            = 'disabled';
	$va_custa                   = $pedido->penhora->penhora_custa->va_custa;
    $de_custa                   = $pedido->penhora->penhora_custa->de_custa;
}
if($pedido->penhora->penhora_averbacao) {
    $desativar_averbacao        = 'disabled';
    $de_resposta_averbacao      = $pedido->penhora->penhora_averbacao->de_resposta;
    $no_documento               = $pedido->penhora->penhora_averbacao->no_documento;
}else{
    $de_resposta_averbacao      = '';
    $no_documento               = '';
}

if ($pedido->penhora->penhora_averbacao['no_documento'])
{
    $desativar_averbacao_documento  = 'disabled';
    $id_penhora_averbacao           = $pedido->penhora->penhora_averbacao->id_penhora_averbacao;
    $no_arquivo                     = $pedido->penhora->penhora_averbacao->no_documento;
}else{
    $no_arquivo                    = '';
    $id_penhora_averbacao          = '';
    $desativar_averbacao_documento = '';

}

$id_tipo_penhora  = @$pedido->penhora->tipo_penhora->id_tipo_penhora;
$id_tipo_custa    = @$pedido->penhora->id_tipo_custa;
$documentoPenhora = @$pedido->penhora->documentos_penhora;

switch ($id_tipo_custa)
{
    case 1:
        $mensagem_alerta = "";
        $style           = 'style="display: none"';
    break;
    case 2:
        //$mensagem_alerta = "O exequente deverá ser intimado a recolher as custas diretamente no registro de imóveis para o registro da penhora.   Registro de Penhora = R$ 210,60 e Certidão = ".formatar_valor( $pedido->penhora->va_emitir_certidao );
        $mensagem_alerta = "";
        $style           = 'style="display: none"';
    break;
    case 3:
        /*if ( $pedido->penhora->no_documento_isencao == '' )
        {
            $mensagem_alerta = "A parte interessada deverá ser intimada para recolher a custa junto à serventia.   Registro de Penhora = R$ 210,60 e Certidão = ".formatar_valor( $pedido->penhora->va_emitir_certidao );
            $style           = 'style="display: block"';
        }else{
            $mensagem_alerta = "Documento de isenção de emolumentos anexado logo abaixo.";
        $style           = 'style="display: block"';
        }*/
        $mensagem_alerta = "";
        $style           = 'style="display: none"';
    break;
}
?>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Processo</legend>
            <div class="col-md-4">
                <label class="small">Número do processo</label>
                <div class="">
                    <input type="text" name="numero_processo" class="form-control" value="{{$pedido->penhora->numero_processo}}" disabled />
                </div>
            </div>
            <div class="col-md-2">
                <label class="small">Data do pedido</label>
                <input type="text" name="dt_pedido" class="form-control" value="{{$pedido->dt_pedido}}" disabled />
            </div>
            <div class="col-md-6">
                <label class="small">Solicitante</label>
                <input type="text" name="dt_pedido" class="form-control" value="{{$pedido->penhora->usuario_cad->pessoa->no_pessoa}}" disabled />
            </div>

        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Penhora</legend>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control pull-left" disabled="disabled">
                        <option value="{{$pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->id_cidade}}">{{$pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                    </select>
                </div>
                <div id="id_serventia" class="col-md-6">
                    <label class="small">Serventia</label>
                    <select name="id_serventia" class="form-control pull-left" disabled>
                        <option value="{{$pedido->pedido_pessoa->id_serventia}}">{{$pedido->pedido_pessoa->no_serventia}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Protocolo</label>
                    <input type="text" name="protocolo_pedido" class="form-control" value="{{$pedido->protocolo_pedido}}" disabled />
                </div>
                <div class="col-md-3">
                    <label class="small">Valor da causa</label>
                    <input type="text" value="{{$pedido->penhora->va_penhora}}" name="va_penhora" class="real form-control"  disabled >
                </div>
                <div class="col-md-3">
                    <label class="small">Data da penhora</label>
                    <input type="text" value="{{($pedido->penhora->dt_penhora?Carbon\Carbon::parse($pedido->penhora->dt_penhora)->format('d/m/Y'):'')}}" name="dt_penhora" class="data form-control"  disabled >
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Imóveis</legend>
                        <div class="col-md-12">
                            @foreach($pedido->penhora->bens_imoveis as $imoveis)
                                <div class="btn-group espacamento-button" >
                                    <button class="btn btn-primary" type="button">Matrícula: {{$imoveis->bem_imovel->matricula_bem_imovel}}</button>
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <fieldset>
                        <legend>Tipo de custas</legend>
                        <div class="col-md-12">
                            @if(count($tipo_custa)>0)
                                @foreach($tipo_custa as $tipo_custa)
                                    <div class="radio">
                                        <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($pedido->penhora->id_tipo_custa == $tipo_custa->id_tipo_custa) checked @endif disabled>
                                        <label for="id_tipo_penhora_{{$tipo_custa->id_tipo_custa}}" class="small">
                                            {{$tipo_custa->no_tipo_custa}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset>
                        <legend>Tipo</legend>
                        @if(count($tipos)>0)
                            <div class="col-md-12">
                                @foreach($tipos as $tipo)
                                    <div class="radio">
                                        <input type="radio" name="id_tipo_penhora" id="id_tipo_penhora_{{$tipo->id_tipo_penhora}}" value="{{$tipo->id_tipo_penhora}}"  @if($pedido->penhora->id_tipo_penhora == $tipo->id_tipo_penhora) checked @endif disabled>
                                        <label for="id_tipo_penhora_{{$tipo->id_tipo_penhora}}" class="small">
                                            {{$tipo->no_tipo_penhora}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset>
                        <legend>Emitir certidão?</legend>
                        <div class="form-group">
                            <div class="radio radio-inline">
                                <input name="in_emitir_certidao" id="in_emitir_certidao_sim" value="S" data-tipo="S" type="radio" @if($pedido->penhora->in_emitir_certidao == 'S') checked @endif disabled>
                                <label for="in_emitir_certidao_sim" class="small">
                                    Sim
                                </label>
                            </div>
                            <div class="radio">
                                <input name="in_emitir_certidao" id="in_emitir_certidao_nao" value="N" data-tipo="N" type="radio" @if($pedido->penhora->in_emitir_certidao == 'N') checked @endif  disabled>
                                <label for="in_emitir_certidao_nao" class="small">
                                    Não
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            @if(count($arquivos_isencao)>0)
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Documento de isenção de emolumentos</legend>
                            <div class="col-md-12">
                                @foreach($arquivos_isencao as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                        @if($arquivo->in_ass_digital=='S')
                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </fieldset>
                    </div>
                </div>
            @endif
            @if(count($arquivos)>0)
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Documento anexado a penhora</legend>
                            <div class="col-md-12">
                                @foreach($arquivos as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                        @if($arquivo->in_ass_digital=='S')
                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </fieldset>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <form name="form-resposta-averbacao" method="post" action="" class="clearfix" enctype="multipart/form-data">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
            <input type="hidden" name="penhora_token" value="{{$penhora_token}}" />
            <fieldset>
                <legend>Responder com averbação</legend>
                <div class="form-group col-md-12">
	                <div class="erros-averbacao alert alert-danger single" style="display:none">
	                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
	                    <div class="menssagem"></div>
	                </div>
	            </div>
                @if(!$desativar_averbacao)
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Arquivos</legend>
                                <div id="arquivos-averbacao" class="col-md-12">
                                    <button type="button" class="adicionar-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="13" data-token="{{$penhora_token}}" data-limite="0">Adicionar arquivo</button>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                @else
                    <div class="form-group clearfix">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Arquivos</legend>
                                <div class="col-md-12">
                                    @if(count($pedido->penhora->penhora_averbacao->arquivos_grupo)>0)
                                        @foreach($pedido->penhora->penhora_averbacao->arquivos_grupo as $arquivo)
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                @if($arquivo->in_ass_digital=='S')
                                                    <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                @endif
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-warning single">
                                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                            <div class="menssagem">Não há arquivos para serem listados.</div>
                                        </div>
                                    @endif
                                </div>
                            </fieldset>
                        </div>
                    </div>
                @endif
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Resposta</label>
                        <textarea name="de_resposta" class="form-control text-justify" rows="3" maxlength="500" {{$desativar_averbacao}}>{{$de_resposta_averbacao}}</textarea>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Averbações</legend>
                            <div class="col-md-12">
                                <div class="panel table-rounded">
                                    <table id="historico-respostas" class="table table-striped table-bordered small">
                                        <thead>
	                                        <tr class="gradient01">
	                                            <th>Matrícula</th>
	                                            <th width="30%">Certidão</th>
	                                        </tr>
                                        </thead>
                                        <tbody>
	                                        @foreach($pedido->penhora->bens_imoveis as $bem_imovel)
	                                            <tr>
	                                                <td>{{$bem_imovel->bem_imovel->matricula_bem_imovel}}</td>
	                                                <td>
	                                                    @if ($bem_imovel->no_arquivo)
	                                                        <a href="#" data-toggle="modal" data-target="#certidao-imovel" class="btn btn-success" data-idpenhorabemimovel="{{$bem_imovel->id_penhora_bem_imovel}}" data-matriculabemimovel="{{$bem_imovel->bem_imovel->matricula_bem_imovel}}">{{$bem_imovel->no_arquivo}}</a>
	                                                    @else
	                                                    	@if(!$desativar_averbacao)
		                                                        @if ($pedido->penhora->in_emitir_certidao == 'S')
		                                                            <div id="arquivo-imovel-{{$bem_imovel->id_penhora_bem_imovel}}" class="col-md-12">
		                                                                <button type="button" class="adicionar-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="12" data-token="{{$penhora_token}}" data-limite="1" data-idflex="{{$bem_imovel->id_penhora_bem_imovel}}">Adicionar arquivo</button>
		                                                            </div>
		                                                        @else
                                                                    <div class="alert alert-danger single">
                                                                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                                                                        <div class="menssagem">Não emitir certidão.</div>
                                                                    </div>
		                                                        @endif
		                                                    @else
		                                                    	@if(count($bem_imovel->arquivos_grupo)>0)
		                                                    		@foreach($bem_imovel->arquivos_grupo as $arquivo)
							                                            <div class="btn-group">
							                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
							                                                @if($arquivo->in_ass_digital=='S')
							                                                    <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
							                                                @endif
							                                            </div>
							                                        @endforeach
		                                                    	@else
	                                                                <div class="alert alert-warning single">
	                                                                    <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
	                                                                    <div class="menssagem">Certidão não enviada.</div>
	                                                                </div>
	                                                            @endif
		                                                    @endif
	                                                    @endif
	                                                </td>
	                                            </tr>
	                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                @if(!$desativar_averbacao)
                    <div class="botoes clearfix">
                        <div class="col-md-12">
                            <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                            <input type="submit" class="btn btn-success pull-right" value="Salvar averbação" />
                        </div>
                    </div>
                @endif
            </fieldset>
        </form>
    </div>
</div>
<hr />
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Prenotação</legend>
            <form name="form-prenotacao" method="post" action="" class="clearfix" enctype="multipart/form-data">
                <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
                <div class="form-group col-md-12">
	                <div class="erros-prenotacao alert alert-danger single" style="display:none">
	                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
	                    <div class="menssagem"></div>
	                </div>
	            </div>
                <div class="form-group clearfix">
                    <div class="col-md-4">
                        <label class="small">Número</label>
                        <input type="text" name="codigo_prenotacao" class="form-control" value="{{$codigo_prenotacao}}" {{$desativar_prenotacao}} />
                    </div>
                    <div class="col-md-4">
                        <label class="small">Data de cadastro</label>
                        <input type="text" name="dt_prenotacao" class="data form-control" value="{{$dt_prenotacao}}" {{$desativar_prenotacao}} />
                    </div>
                    <div class="col-md-4">
                        <label class="small">Data de vencimento</label>
                        <input type="text" name="dt_vencimento_prenotacao" class="data form-control" value="{{$dt_vencimento_prenotacao}}" disabled />
                    </div>
                </div>
                @if(!$desativar_prenotacao)
                    <div class="botoes clearfix">
                        <div class="col-md-12">
                            <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                            <input type="submit" class="btn btn-success pull-right" value="Salvar prenotação" />
                        </div>
                    </div>
                @endif
            </form>
        </fieldset>
    </div>
</div>
@if($pedido->penhora->in_isenta=='N')
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Custas</legend>
                <form name="form-custa" method="post" action="" class="clearfix" enctype="multipart/form-data">
                    <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
                    <div class="form-group col-md-12">
	                    <div class="erros-custa alert alert-danger single" style="display:none">
	                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
	                        <div class="menssagem"></div>
	                    </div>
	                </div>
                    <div class="form-group clearfix">
                        <div class="col-md-8">
                            <label class="small">Descrição</label>
                            <input type="text" name="de_custa" class="form-control text-justify" rows="3" maxlength="500" value="{{$de_custa}}" {{$desativar_custa}} />
                        </div>
                        <div class="col-md-4">
                            <label class="small">Valor</label>
                            <input type="text" name="va_custa" class="real form-control" value="{{$va_custa}}" {{$desativar_custa}} />
                        </div>
                    </div>
                    @if(!$desativar_custa)
                        <div class="botoes clearfix">
                            <div class="col-md-12">
                                <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                                <input type="submit" class="btn btn-success pull-right" value="Salvar custa" />
                            </div>
                        </div>
                    @endif
                </form>
            </fieldset>
        </div>
    </div>
@endif
<hr />
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <form name="form-resposta-exigencia" method="post" action="" class="clearfix" enctype="multipart/form-data">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
			<input type="hidden" name="penhora_token" value="{{$penhora_token}}" />
            <input type="hidden" name="tipo_resultado_envio" value="3" />
            <fieldset>
                <legend>Responder com Pendências</legend>
                <div class="form-group col-md-12">
	                <div class="erros-exigencia alert alert-danger single" style="display:none">
	                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
	                    <div class="menssagem"></div>
	                </div>
	            </div>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Arquivos</legend>
                            <div id="arquivos-pendencias" class="col-md-12" data-local="resposta">
                                <button type="button" class="adicionar-arquivo btn btn-success {{$desativar_exigencia}}" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="11" data-token="{{$penhora_token}}" data-limite="0" {{$desativar_exigencia}}>Adicionar arquivo</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Resposta</label>
                        <textarea name="de_resposta" class="form-control text-justify" rows="3" maxlength="500" {{$desativar_exigencia}}></textarea>
                    </div>
                </div>
                @if(!$desativar_exigencia)
                    <div class="botoes clearfix">
                        <div class="col-md-12">
                            <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                            <input type="submit" class="btn btn-success pull-right" value="Salvar pendências" />
                        </div>
                    </div>
                @endif
            </fieldset>
        </form>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Histórico de Pendências</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="historico-pedido" class="table table-striped table-bordered small">
                        <thead>
                            <tr class="gradient01">
                                <th class="col-md-3">Usuário</th>
                                <th class="col-md-2">Data</th>
                                <th class="col-md-5">Resposta</th>
                                <th class="col-md-2">Arquivo</th>
                            </tr>
                        </thead>
                        <tbody>
    						@foreach($pedido->penhora->penhora_exigencia as $exigencia)
                                <tr>
                                    <td>{{$exigencia->usuario_cad->no_usuario}}</td>
                                    <td>{{Carbon\Carbon::parse($exigencia->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                    <td>{{$exigencia->de_resposta}}</td>
                                    <td>
                                        @if ($exigencia->de_resultado!='')
                                            {{$exigencia->de_resultado}}
                                        @elseif(count($exigencia->arquivos_grupo)>0)
                                            @foreach($exigencia->arquivos_grupo as $arquivo)
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                    @if($arquivo->in_ass_digital=='S')
                                                        <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                    @endif
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="alert alert-warning single">
                                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                                <div class="menssagem">Arquivos não enviados.</div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div id="assinaturas-arquivos" class="fieldset-group clearfix">
	<div class="col-md-12">
        <div class="alert alert-warning single">
            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
            <div class="menssagem clearfix">
            	<span>Nenhum arquivo foi inserido.</span><br />
                <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$penhora_token}}">Assinar arquivo(s)</a>
			</div>
        </div>
	</div>
</div>