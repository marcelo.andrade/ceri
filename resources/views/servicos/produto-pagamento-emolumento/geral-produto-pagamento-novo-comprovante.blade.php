<input type="hidden" name="id_pagamento_repasse_pessoa_lote" value="{{$id_pagamento_repasse_pessoa_lote}}" />
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Detalhes do pagamento</legend>
            <div class="col-md-5">
                <div class="col-md-12">
                    <label class="small">Serventia</label>
                    <input type="text" class="form-control" value="{{$pagamentos_repasse_pessoa_lote->pessoa->serventia->no_serventia}}" disabled="disabled" />
                </div>
            </div>
            <div class="col-md-7 clearfix">
                <div class="col-md-3">
                    <label class="small">Data de cadastro</label>
                    <input type="text" class="form-control" value="{{formatar_data($pagamentos_repasse_pessoa_lote->dt_cadastro)}}" disabled="disabled" />
                </div>
                <div class="col-md-3">
                    <label class="small">Valor total pedido</label>
                    <input type="text" class="form-control real" value="{{$pagamentos_repasse_pessoa_lote->va_parcela_pedido}}" disabled="disabled" />
                </div>
                <div class="col-md-3">
                    <label class="small">Valor total repasse</label>
                    <input type="text" class="form-control real" value="{{$pagamentos_repasse_pessoa_lote->va_repasse}}" disabled="disabled" />
                </div>
                <div class="col-md-2">
                    <label class="small">Quantidade</label>
                    <input type="text" class="form-control" value="{{$pagamentos_repasse_pessoa_lote->nu_quantidade}}" disabled="disabled" />
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <table id="pedidos-pendentes" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="10%">Protocolo</th>
            <th width="10%">Cidade</th>
            <th width="10%">Serventia</th>
            <th width="10%">Data do repasse</th>
            <th width="20%">Valor pedido</th>
            <th width="20%">Valor do repasse</th>
            <th width="20%">Situação</th>
            <th width="10%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if (count($pagamentos_repasse_pessoa_lote->pagamento_repasse_parcela)>0)
            @foreach ($pagamentos_repasse_pessoa_lote->pagamento_repasse_parcela as $pagamento)
                <tr>
                    <td>{{$pagamento->movimentacao_parcela_repasse->pedido->protocolo_pedido}}</td>
                    <td>{{$pagamento->movimentacao_parcela_repasse->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</td>
                    <td>{{$pagamento->movimentacao_parcela_repasse->pedido->pedido_pessoa_atual->pessoa->serventia->no_serventia}}</td>
                    <td>{{formatar_data($pagamento->movimentacao_parcela_repasse->dt_repasse)}}</td>
                    <td><span class="real">{{$pagamento->movimentacao_parcela_repasse->va_parcela_pedido}}</span></td>
                    <td><span class="real">{{$pagamento->movimentacao_parcela_repasse->va_repasse}}</span></td>
                    <td>{{$pagamento->movimentacao_parcela_repasse->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="@if ($pagamento->movimentacao_parcela_repasse->pedido->situacao_pedido_grupo_produto->id_grupo_produto == 1 ) #detalhes-pesquisa @else #detalhes-certidao @endif" data-protocolo="{{$pagamento->movimentacao_parcela_repasse->pedido->protocolo_pedido}}" data-idpedido="{{$pagamento->movimentacao_parcela_repasse->pedido->id_pedido}}" data-idgrupoproduto="{{$pagamento->movimentacao_parcela_repasse->pedido->situacao_pedido_grupo_produto->id_grupo_produto}}">Detalhes</button>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum pagamento pendente foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Comprovante de pagamento</legend>
        <div class="erros alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-6">
				<label class="small">Data do pagamento</label>
				<input type="text" name="dt_pagamento" class="form-control data" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}" />
			</div>
            <div class="col-md-6">
				<label class="small">Valor pago</label>
				<input type="text" name="va_pago" class="form-control real" value="{{$pagamentos_repasse_pessoa_lote->va_repasse}}" readonly="readonly" />
			</div>
		</div>
        <div class="form-group clearfix">
            <div class="col-md-12">
				<label class="small">Arquivo do comprovante</label>
                <div class="arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput">
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="button input-group-addon btn-file">
                        <span class="fileinput-new">Adicionar arquivo</span>
                        <span class="fileinput-exists">Alterar</span>
                        <input type="file" name="no_arquivo_comprovante">
                    </span>
                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
			</div>
		</div>
	</fieldset>
</div>