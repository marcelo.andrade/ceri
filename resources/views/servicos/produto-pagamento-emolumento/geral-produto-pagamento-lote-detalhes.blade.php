<input type="hidden" name="id_pagamento_repasse_lote" value="{{$request->id_pagamento_repasse_lote}}">

<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Serviços</legend>
            <div class="fieldset-group clearfix">
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <div class="panel table-rounded">
                            <table id="lote-pagametos-efetuados-detalhes"
                                   class="table table-striped table-bordered small">
                                <thead>
                                    <tr class="gradient01">
                                        <th>Protocolo</th>
                                        <th>Data do pedido</th>
                                        <th>Produto</th>
                                        <th>Cidade</th>
                                        <th>Serventia</th>
                                        <th width="10%">Total repasse</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $qtd = 0;
                                $va_repasse = 0;
                                ?>
                                @if(count($lote_pagamentos_efetuados) > 0)
                                    @foreach($lote_pagamentos_efetuados as $lote_pagamento)
                                        <tr>
                                            <td>{{$lote_pagamento->protocolo_pedido}}</td>
                                            <td>{{formatar_data($lote_pagamento->dt_pedido)}}</td>
                                            <td>{{$lote_pagamento->no_produto_item}}</td>
                                            <td>{{$lote_pagamento->no_cidade}}</td>
                                            <td>{{$lote_pagamento->no_serventia}}</td>
                                            <td>{{formatar_valor($lote_pagamento->va_repasse)}}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="@if ($lote_pagamento->id_grupo_produto == 1 ) #detalhes-pesquisa @else #detalhes-certidao @endif" data-protocolo="{{$lote_pagamento->protocolo_pedido}}" data-idpedido="{{$lote_pagamento->id_pedido}}" data-idgrupoproduto="{{$lote_pagamento->id_grupo_produto}}">Detalhes</button>
                                            </td>
                                        </tr>
                                        <?php
                                        $qtd++;
                                        $va_repasse += $lote_pagamento->va_repasse;
                                        ?>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">
                                            <div class="single alert alert-danger">
                                                <i class="glyphicon glyphicon-remove"></i>
                                                <div class="mensagem">
                                                    Nenhum registro foi encontrado.
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        @if(count($lote_pagamentos_efetuados) > 0)
                            <div class="form-group clearfix">
                                <div class="col-md-12">
                                    <div id="serventias-total-geral">
                                        <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                                            <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                                            <div class="pull-right totais">
                                                <label class="quantidade label label-default"><b>Quantidade selecionada:</b>
                                                    <span>{{$qtd}}</span>
                                                </label>&nbsp;
                                                <label class="valor-total label label-success"><b>Valor do pedido:</b>
                                                    <span class="real">{{$va_parcela_pedido}}</span>
                                                </label>&nbsp;
                                                <label class="valor-total label label-info"><b>Taxa bancária (Operadora):</b>
                                                    <span class="real">{{$lote_pagamentos_efetuados[0]->va_taxa_bancaria_operadora}}</span>
                                                </label>&nbsp;
                                                <label class="valor-total label label-primary"><b>Valor do repasse:</b>
                                                    <span class="real">{{formatar_valor($va_repasse)}}</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
