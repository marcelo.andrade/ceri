<?php $vl_total_repasse = 0; ?>
<div class="panel table-rounded">
    <table id="pedidos-receber" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="10%">Protocolo</th>
            <th width="10%">Produto</th>
            <th width="10%">Cidade</th>
            <th width="20%">Serventia</th>
            <th width="10%">Valor pedido</th>
            <th width="10%">Valor do repasse</th>
        </tr>
        </thead>
        <tbody>
        @if (count($pagamentos_receber)>0)
            @foreach ($pagamentos_receber as $pagamento)
                <tr id="{{$pagamento->id_movimentacao_parcela_repasse}}">
                    <td>{{$pagamento->protocolo_pedido}}  <br>/  {{ formatar_data($pagamento->dt_pedido)}}</td>
                    <td>{{$pagamento->no_produto}}</td>
                    <td>{{$pagamento->no_cidade}}</td>
                    <td>{{$pagamento->no_serventia}}</td>
                    <td><span class="real">{{$pagamento->va_total_pedido}}</span></td>
                    <td><span class="real">{{$pagamento->va_total_repasse}}</span></td>
                </tr>
                <?php $vl_total_repasse +=  $pagamento->va_total_repasse; ?>
            @endforeach
        @else
            <tr>
                <td colspan="10">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum pagamento à receber foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
    <div class="form-group clearfix">
        <div class="col-md-12">
            <div id="serventias-total-geral">
                <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                    <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                    <div class="pull-right totais">
                        <label class="valor-total label label-primary"><b>Valor total:</b>
                            <span class="real">{{$vl_total_repasse}}</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>