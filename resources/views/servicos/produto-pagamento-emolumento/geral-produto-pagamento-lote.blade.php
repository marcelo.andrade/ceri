<div class="panel table-rounded">
    <table id="lote-pagamentos-efetuados" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="15%">Protocolo</th>
            <th width="10%">Data do lote</th>
            <th width="10%">Valor</th>
            <th width="10%">Quantidade</th>
            <th width="10%">Lote</th>
            <th width="30%">Usuário</th>
            <th width="15%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if(count($lote_pagamentos_efetuados) > 0)
            @foreach($lote_pagamentos_efetuados as $lote_pagamento)
                <tr>
                    <td>{{$lote_pagamento->protocolo_pedido}}</td>
                    <td>{{formatar_data($lote_pagamento->dt_lote)}}</td>
                    <td>{{formatar_valor($lote_pagamento->va_repasse_lote)}}</td>
                    <td>{{$lote_pagamento->nu_quantidade_lote}}</td>
                    <td>{{$lote_pagamento->numero_lote}}</td>
                    <td>{{$lote_pagamento->no_usuario}}</td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-lote-pagamentos" data-idpagamentorepasselote="{{$lote_pagamento->id_pagamento_repasse_lote}}" data-protocolo="{{$lote_pagamento->protocolo_pedido}}">Detalhes</button>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#imprimir-recibo-lote-pagamentos" data-idpagamentorepasselote="{{$lote_pagamento->id_pagamento_repasse_lote}}" data-protocolo="{{$lote_pagamento->protocolo_pedido}}">Emitir recibo</a></li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum registro foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($lote_pagamentos_efetuados)}}</b> de <b>{{$lote_pagamentos_efetuados->total()}}</b>.
</div>
<div align="center">
    {{$lote_pagamentos_efetuados->fragment('lote-pagamentos-efetuados')->render()}}
</div>
