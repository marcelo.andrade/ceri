<form name="form-pesquisa-pagamento" id="form-pesquisa-pagamento">
    <input type="hidden" name="acao" id="acao" value="pesquisar">
    <fieldset class="clearfix">
        <legend>Filtro</legend>
        <div class="fieldset-group clearfix">
            <div class="col-md-4">
                <fieldset>
                    <legend>Período</legend>
                    <div class="col-md-12">
                        <div class="periodo input-group input-daterange">
                            <input type="text" class="form-control pull-left" name="dt_inicio" rel="periodo" value="{{$request->dt_inicio}}" />
                            <span class="input-group-addon small pull-left">até</span>
                            <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" />
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset>
                    <legend>Protocolo</legend>
                    <div class="col-md-12">
                        <div class="protocolo_pedido">
                            <input type="text" name="protocolo_pedido" class="form-control" value="{{$request->protocolo_pedido}}" />
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset>
                    <legend>Produtos</legend>
                    <div class="col-md-12">
                        <select name="id_grupo_produto" class="form-control pull-left">
                            <option value="0">Todos os produtos</option>
                            @if(count($grupo_produtos)>0)
                                @foreach ($grupo_produtos as $produto)
                                    <option value="{{$produto->id_grupo_produto}}" @if($request->id_grupo_produto==$produto->id_grupo_produto) selected @endif>{{$produto->no_grupo_produto}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </fieldset>
            </div>
        </div>

        </div>
        <div class="fieldset-group clearfix">
            <div class="col-md-12">
                <fieldset>
                    <legend>Serventia</legend>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Cidade</legend>
                            <div class="col-md-12">
                                <select name="id_cidade" class="form-control pull-left cidade">
                                    <option value="0">Selecione uma cidade</option>
                                    @if(count($cidades)>0)
                                        @foreach ($cidades as $cidade)
                                            <option value="{{$cidade->id_cidade}}" @if($request->cidade==$cidade->id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Serventia</legend>
                            <div class="col-md-12">
                                <select name="id_serventia" class="form-control pull-left serventia" disabled="">
                                    <option value="0">Todas as serventias</option>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                </fieldset>
            </div>

        </div>

        <div class="fieldset-group clearfix">
            <div class="buttons col-md-12 text-right">
                <input type="reset" name="limpar-filtrar-pagamentos" id="limpar-filtrar-pagamentos" class="btn btn-primary limpar-formulario" value="Limpar filtros" />
                <input type="button" name="filtrar-pagamentos" id="filtrar-pagamentos" class="btn btn-success filtrar-pagamentos" value="Filtrar pagamentos" />
            </div>
        </div>
    </fieldset>
</form>

<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>

<div id="resultadoPesquisaPagamento">
    @include('servicos.produto-pagamento-emolumento.geral-produto-pagamento-historico') <!-- lista de pagamento -->
</div>
