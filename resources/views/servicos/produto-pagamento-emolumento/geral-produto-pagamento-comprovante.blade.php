@if(in_array($alienacao_pagamento->no_extensao_comprovante,array('pdf','jpg','png','bmp','gif')))
	@if(strtolower($alienacao_pagamento->no_extensao_comprovante)=='pdf')
        <object data="{{URL::to('/arquivos/alienacao-pagamento/'.$alienacao_pagamento->id_alienacao_pagamento.'/'.$alienacao_pagamento->no_arquivo_comprovante)}}" type="application/pdf" class="resultado-pdf">
            <p>Seu navegador não tem um plugin pra PDF</p>
        </object>
	@else
    	<img src="{{URL::to('/arquivos/alienacao-pagamento/'.$alienacao_pagamento->id_alienacao_pagamento.'/'.$alienacao_pagamento->no_arquivo_comprovante)}}" class="img-responsive" />
    @endif
@else
    <div class="alert alert-warning single">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem">
            O arquivo não pode ser pré-visualizado, por favor, faça o download.
        </div>
    </div>
@endif