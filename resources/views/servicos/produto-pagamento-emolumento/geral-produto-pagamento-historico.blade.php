<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Serviços</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <div class="panel-heading gradient01">
                        <div class="checkbox">
                            <input type="checkbox" name="selecionar_tudo" id="selecionar_tudo" value="S" @if (count($movimentacoes_parcela_repasse)<=0) disabled @endif>
                            <label for="selecionar_tudo" class="small">
                                Selecionar todas
                            </label>
                        </div>
                    </div>
                    <table id="notificacoes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="3%"></th>
                            <th width="10%">Protocolo / Data</th>
                            <th width="15%">Produto</th>
                            <th width="10%">Cidade</th>
                            <th width="15%">Serventia</th>
                            <th width="5%">Status</th>
                            <th width="8%">Total pedido</th>
                            <th width="8%">Total repasse</th>
                            <th width="13%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serventias = array();
                        ?>
                        @if (count($movimentacoes_parcela_repasse)>0)
                            @foreach ($movimentacoes_parcela_repasse as $movimentacao_parcela_repasse)
                                <?php
                                if (!in_array($movimentacao_parcela_repasse->id_serventia,$serventias)) {
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['no_serventia']        = $movimentacao_parcela_repasse->no_serventia;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['no_cidade']           = $movimentacao_parcela_repasse->no_cidade;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['codigo_banco']        = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->banco->codigo_banco;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['no_banco']            = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->banco->no_banco;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['nu_variacao']	        = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->nu_variacao;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['nu_agencia']	        = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->nu_agencia;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['nu_dv_agencia']	    = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->nu_dv_agencia;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['nu_conta']		    = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->nu_conta;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['nu_dv_conta']	        = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->nu_dv_conta;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['tipo_conta']	        = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->tipo_conta;
                                    $serventias[$movimentacao_parcela_repasse->id_serventia]['nu_cpf_cnpj_conta']   = $movimentacao_parcela_repasse->serventia->pessoa->banco_pessoa->nu_cpf_cnpj_conta;
                                }
                                ?>
                                <tr id="{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}">
                                    <td>
                                        <div class="checkbox">
                                            <input type="checkbox" name="id_movimentacao_parcela_repasse[{{$movimentacao_parcela_repasse->id_pessoa}}][{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}][{{$movimentacao_parcela_repasse->va_total_repasse}}][{{$movimentacao_parcela_repasse->va_total_pedido}}][]" class="id_movimentacao_parcela_repasse" id="id_movimentacao_parcela_repasse_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" data-idserventia="{{$movimentacao_parcela_repasse->id_serventia}}" value="{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" >
                                            <label for="id_movimentacao_parcela_repasse_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" class="small"></label>
                                        </div>
                                    </td>
                                    <td>{{$movimentacao_parcela_repasse->protocolo_pedido}}  <br>/  {{ formatar_data($movimentacao_parcela_repasse->dt_pedido)}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_produto}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_cidade}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_serventia}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_situacao_pedido_grupo_produto}}</td>
                                    <td>
                                        <span class="real">{{$movimentacao_parcela_repasse->va_total_pedido}}</span>
                                        <input type="hidden" id="valor_pedido_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" value="{{$movimentacao_parcela_repasse->va_total_repasse}}" />
                                    </td>
                                    <td><span class="real">{{$movimentacao_parcela_repasse->va_total_repasse}}</span></td>
                                    <td class="options">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="@if ($movimentacao_parcela_repasse->id_grupo_produto == 1 ) #detalhes-pesquisa @else #detalhes-certidao @endif" data-protocolo="{{$movimentacao_parcela_repasse->protocolo_pedido}}" data-idpedido="{{$movimentacao_parcela_repasse->id_pedido}}" data-idgrupoproduto="{{$movimentacao_parcela_repasse->id_grupo_produto}}">Detalhes</button>
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">
                                    <div class="single alert alert-danger">
                                        <i class="glyphicon glyphicon-remove"></i>
                                        <div class="mensagem">
                                            Nenhuma produto foi encontrado.
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                @if (count($serventias)>0)
                    <div id="serventias-totais">
                        @foreach ($serventias as $key => $serventia)
                            <div id="serventia_{{$key}}" class="well well-sm gradient01 clearfix">
                                <span class="pull-left nome col-md-8">
                                    {{$serventia['no_serventia']}} - ( {{$serventia['no_cidade']}} ) <br />
                                    @if($serventia['codigo_banco'] != "")
                                        <label class="label label-default">Código Banco:</label> {{$serventia['codigo_banco']}}
                                        <label class="label label-default">Nome do Banco:</label> {{$serventia['no_banco']}}
                                        <label class="label label-default">Agência/DV:</label> {{$serventia['nu_agencia']}}/{{$serventia['nu_dv_agencia']}}
                                        <label class="label label-default">Conta/DV:</label> {{$serventia['nu_conta']}}/{{$serventia['nu_dv_conta']}}
                                        <label class="label label-default">Tipo de conta:</label> @if ( $serventia['tipo_conta'] == 'C' ) Corrente @else Poupança @endif
                                        <label class="label label-default">CPF/CNPJ:</label> {{cpf_cnpj($serventia['nu_cpf_cnpj_conta'])}}
                                    @endif
                                </span>
                                <div class="pull-right totais">
                                    <label class="quantidade label label-default"><b>Quantidade selecionada:</b> <span>0</span></label>&nbsp;<label class="valor-total label label-primary"><b>Valor total:</b> <span class="real">0</span></label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                    <div id="serventias-total-geral">
                            <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                                <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                                <div class="pull-right totais">
                                    <label class="quantidade label label-default"><b>Quantidade selecionada:</b>
                                        <span>0</span>
                                    </label>&nbsp;
                                    <label class="valor-total label label-primary"><b>Valor total:</b>
                                        <span class="real">0</span>
                                    </label>
                                </div>
                            </div>
                    </div>
            </div>
        </div>
    </fieldset>
</div>