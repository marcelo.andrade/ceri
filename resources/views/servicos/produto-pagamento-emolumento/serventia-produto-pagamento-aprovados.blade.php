<div class="panel table-rounded">
    <table id="pedidos-pendentes" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="10%">Protocolo</th>
            <th width="5%">Cidade</th>
            <th width="5%">Serventia</th>
            <th width="5%">Data do repasse</th>
            <th width="5%">Data do comprovante repasse</th>
            <th width="5%">Quantidade</th>
            <th width="10%">Valor pedido</th>
            <th width="10%">Valor do repasse</th>
            <th width="15%">Situação</th>
            <th width="15%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if (count($pagamentos_aprovados)>0)
            @foreach ($pagamentos_aprovados as $pagamento)
                <tr>
                    <td>{{$pagamento->protocolo_pedido}}</td>
                    <td>{{$pagamento->pessoa->enderecos[0]->cidade->no_cidade}}</td>
                    <td>{{$pagamento->pessoa->serventia->no_serventia}}</td>
                    <td>{{formatar_data($pagamento->dt_repasse)}}</td>
                    <td>{{formatar_data($pagamento->dt_comprovante_repasse)}}</td>
                    <td>{{str_pad($pagamento->nu_quantidade, 5, "0", STR_PAD_LEFT)}}</td>
                    <td><span class="real">{{$pagamento->va_parcela_pedido}}</span></td>
                    <td><span class="real">{{$pagamento->va_repasse}}</span></td>
                    <td>{{$pagamento->situacao_pagamento_repasse->no_situacao_pagamento_repasse}}</td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhe-historico-pagamento" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idpagamentorepassepessoalote="{{$pagamento->id_pagamento_repasse_pessoa_lote}}">Detalhes</button>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum pagamento efetuado foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>