@if ($class->id_tipo_pessoa == $class::ID_TIPO_PESSOA_ANOREG )
    <fieldset class="clearfix">
    <legend>Filtro</legend>
        <div class="fieldset-group clearfix">
            <div class="col-md-12">
                <fieldset>
                    <legend>Serventia</legend>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Cidade</legend>
                            <div class="col-md-12">
                                <select name="cidade_pagamentos" class="form-control pull-left">
                                    <option value="0">Selecione uma cidade</option>
                                    @if(count($cidades) > 0)
                                        @foreach ($cidades as $cidade)
                                            <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Serventia</legend>
                            <div class="col-md-12">
                                <select name="serventia_pagamentos" class="form-control pull-left" disabled="">
                                    <option value="0">Todas as serventias</option>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                </fieldset>
            </div>
        </div>
    <div class="fieldset-group clearfix">
        <div class="fieldset-group clearfix">
            <div class="col-md-6">
                <fieldset>
                    <legend>Data de repasse</legend>
                    <div class="col-md-12">
                        <div class="periodo input-group input-daterange">
                            <input type="text" class="form-control pull-left" name="dt_ini_pagamentos" rel="periodo" value="{{$request->dt_ini_pagamentos}}" readonly/>
                            <span class="input-group-addon small pull-left">até</span>
                            <input type="text" class="form-control pull-left" name="dt_fim_pagamentos" rel="periodo" value="{{$request->dt_fim_pagamentos}}" readonly/>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
        <div class="fieldset-group clearfix">
            <div class="buttons col-md-12 text-right">
                <input type="reset" class="btn btn-primary" value="Limpar filtros"/>
                <input type="submit" class="btn btn-success" value="Filtrar pagamentos"/>
            </div>
        </div>
    </fieldset>
@endif
