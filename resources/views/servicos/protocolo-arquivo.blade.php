@if(in_array($protocolo_arquivo->no_extensao,array('pdf','jpg','png','bmp','gif')))
	@if($protocolo_arquivo->no_extensao=='pdf')
        <object data="{{URL::to('/arquivos/protocolo/'.$protocolo_arquivo->id_protocolo_arquivo.'/'.$protocolo_arquivo->no_arquivo)}}" type="application/pdf" class="resultado-pdf">
            <p>Seu navegador não tem um plugin pra PDF</p>
        </object>
	@else
    	<img src="{{URL::to('/arquivos/protocolo/'.$protocolo_arquivo->id_protocolo_arquivo.'/'.$protocolo_arquivo->no_arquivo)}}" class="img-responsive" />
    @endif
@else
    <div class="alert alert-warning single">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem">
            O arquivo não pode ser pré-visualizado, por favor, faça o download.
        </div>
    </div>
@endif