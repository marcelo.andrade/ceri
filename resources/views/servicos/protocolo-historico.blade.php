<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th>Protocolo</th>
                <th>Data do cadastro</th>
                <th>Natureza formal</th>
                <th>Apresentante</th>
                <th>Status</th>
                <th>Ações</th>                    
            </tr>
        </thead>
        <tbody>
        	@if (count($todos_protocolos)>0)
                @foreach ($todos_protocolos as $protocolo)
                    <tr {{@marcar_linha_vermelha(5,$protocolo->pedido->id_pedido, $notificacao)}}>
                        <td>{{$protocolo->pedido->protocolo_pedido}} {{@mostrar_sino(5,$protocolo->pedido->id_pedido, $notificacao)}}</td>
                        <td>{{$protocolo->pedido->dt_pedido}}</td>
                        <td>
                            @if ($protocolo->pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_ANALISE)
                                Aguardando
                            @else
                                {{$protocolo->protocolo_natureza->no_protocolo_natureza}}
                            @endif
                        </td>
                        <td>
                            @if ($protocolo->pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_ANALISE)
                                Aguardando
                            @else
                                {{$protocolo->no_apresentante}}
                            @endif
                        </td>
                    	<td>{{$protocolo->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                   		<td class="options">
                            <div class="btn-group">
                                <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Ações <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu black">
                                    <li><a href="#" data-toggle="modal" data-target="#detalhes-protocolo" data-idpedido="{{$protocolo->pedido->id_pedido}}" data-protocolo="{{$protocolo->pedido->protocolo_pedido}}">Ver detalhes</a></li>
                                </ul>
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhum protocolo foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>