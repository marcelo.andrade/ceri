<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados do credor</legend>
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label class="small">Documento identificador <span>(CNPJ)</span></label>
                <input type="text" name="nu_cpf_cnpj" class="form-control" disabled="disabled" value="{{$credor->nu_cpf_cnpj}}" />
            </div>
            <div class="form-group col-md-6">
                <label class="small">Nome do credor</label>
                <input type="text" name="no_credor" class="form-control" disabled="disabled" value="{{$credor->no_credor}}" />
            </div>
            <div class="form-group col-md-12">
                <label class="small">Agência</label>
                <select name="id_agencia" class="form-control pull-left" disabled>
                    <option value="{{$credor->agencia->codigo_agencia}}">{{$credor->agencia->codigo_agencia}}</option>
                </select>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereço do credor</legend>
        <div class="col-md-12">
            <div class="form-group clearfix">
                <div class="col-md-10">
                    <label class="small">Endereço</label>
                    <input type="text" name="no_endereco" class="form-control" disabled="disabled" value="{{$credor->no_endereco}}" />
                </div>

               {{-- <div class="col-md-2">
                    <label class="small">Número</label>
                    <input type="text" name="nu_endereco" class="form-control" disabled="disabled" value="{{$credor->nu_endereco}}" />
                </div>--}}
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Complemento</label>
                    <input type="text" name="no_complemento" class="form-control" disabled="disabled" value="{{$credor->no_complemento}}" />
                </div>
                <div class="col-md-6">
                    <label class="small">Bairro</label>
                    <input type="text" name="no_bairro" class="form-control" disabled="disabled" value="{{$credor->no_bairro}}" />
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control" disabled="disabled">
                        <option>{{$credor->cidade->no_cidade}}</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">CEP</label>
                    <input type="text" name="nu_cep" class="form-control"  disabled="disabled" value="{{$credor->nu_cep}}" />
                </div>
            </div>
        </div>
    </fieldset>
</div>