<fieldset class="clearfix">
    <legend>Filtro</legend>
    <div class="fieldset-group clearfix">
        <div class="col-md-4">
            <fieldset>
                <legend>Período</legend>
                <div class="col-md-12">
                    <div class="periodo input-group input-daterange">
                        <input type="text" class="form-control pull-left" name="dt_inicio" rel="periodo" value="{{$request->dt_inicio}}" />
                        <span class="input-group-addon small pull-left">até</span>
                        <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset>
                <legend>Status</legend>
                <div class="col-md-12">
                    <select name="id_situacao_pedido_grupo_produto" class="form-control pull-left">
                        <option value="0">Todos os status</option>
                        @if(count($situacoes)>0)
                            @foreach ($situacoes as $situacao)
                                <option value="{{$situacao->id_situacao_pedido_grupo_produto}}" @if($request->id_situacao_pedido_grupo_produto==$situacao->id_situacao_pedido_grupo_produto) selected @endif>{{$situacao->no_situacao_pedido_grupo_produto}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset>
                <legend>Protocolo</legend>
                <div class="col-md-12">
                    <div class="protocolo">
                        <input type="text" name="protocolo_pedido" id="protocolo_pedido" class="form-control" value="{{$request->protocolo_pedido}}" />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="col-md-4">
            <fieldset>
                <legend>Pesquisa por <span></span></legend>
                <div class="col-md-4">
                    @if(count($tipos_chave_filtro)>0)
                        <select name="id_tipo_certidao_chave_pesquisa" class="form-control">
                            <option value="0">Tipo de chave</option>
                            @foreach ($tipos_chave_filtro as $tipo_chave)
                                <option value="{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" {{($request->id_tipo_certidao_chave_pesquisa==$tipo_chave->id_tipo_certidao_chave_pesquisa?'selected':'')}}>{{$tipo_chave->no_chave_pesquisa}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                <div class="col-md-8">
                    <input type="text" {{$request->de_chave_pesquisa!="" ? '' : 'disabled="disabled"' }} class="form-control" name="de_chave_pesquisa" value="{{$request->de_chave_pesquisa}}">
                </div>
            </fieldset>
        </div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="buttons col-md-12 text-right">
            <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar filtros" />
            <input type="submit" class="btn btn-success" value="Filtrar matrículas" />
        </div>
    </div>
</fieldset>