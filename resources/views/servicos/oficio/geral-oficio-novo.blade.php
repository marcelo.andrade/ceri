@if ( in_array( $class->id_tipo_pessoa, array(4,6,7,11)) )
<div class="erros alert alert-danger" style="">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem">
        Ordens de Indisponibilidades devem ser cadastradas exclusivamente na Central Nacional de Indisponibilidades de Bens – CNIB ( https://www.indisponibilidade.org.br ), conforme previsto no art. 5º do Provimento nº 39/2014 do CNJ (que proíbe o envio de Ofícios genéricos aos Cartórios).
    </div>
</div>
@endif

<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<input type="hidden" name="oficio_token" id="oficio_token" value="{{$oficio_token}}" />
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Ofício</legend>
            <div class="col-md-5">
                <label class="small">Número documento</label>
                <input type="text" name="numero_documento" id="numero_documento" class="form-control" maxlength="45"/>
            </div>
           <div class="col-md-2">
                <label class="small">Tipo de processo</label>
                <select name="tp_processo" class="form-control">
                    <option value="J">Judicial</option>
                    <option value="A">Administrativo</option>
                </select>
            </div>
            <div class="col-md-5">
                <label class="small">Número processo</label>
                <input type="text" name="numero_processo" class="numero_processo form-control" maxlength="45" />
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Destinatário</legend>
            <div class="col-md-3">
                <label class="small">Tipo de destinário</label>
                <select name="id_tipo_usuario" class="form-control pull-left" data-ultimo="0">
                    <option value="0">Selecione</option>
                    <?php
                    switch ($class->id_tipo_pessoa) {
                        case 2:
                            echo '<option value="4">Judiciário</option>';
                            echo '<option value="2">Serventia</option>';
                            echo '<option value="7">Unidade gestora</option>';
                            echo '<option value="11">Unidade judiciária</option>';
                            break;
                        case 4: case 7: case 11: case 6:
                            echo '<option value="2">Serventia</option>';
                            break;
                    }
                    ?>
                </select>
            </div>
            <div id="campos-serventia" class="campos-destinatario col-md-6 row" style="display: none">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma cidade</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">Serventia</label>
                    <select name="id_serventia" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($serventias)>0)
                            @foreach ($serventias as $serventia)
                                <option value="{{$serventia->id_serventia}}">{{$serventia->no_serventia}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="campos-judiciario" class="campos-destinatario col-md-6 row" style="display: none">
                <div class="col-md-6">
                    <label class="small">Comarca</label>
                    <select name="id_comarca" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma comarca</option>
                        @if(count($comarcas)>0)
                            @foreach ($comarcas as $comarca)
                                <option value="{{$comarca->id_comarca}}">{{$comarca->no_comarca}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">Vara</label>
                    <select name="id_vara" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($varas)>0)
                            @foreach ($varas as $vara)
                                <option value="{{$vara->id_vara}}">{{$vara->no_vara}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="campos-unidade-gestora" class="campos-destinatario col-md-4 row" style="display: none">
                <div class="col-md-12">
                    <label class="small">Unidade gestora</label>
                    <select name="id_unidade_gestora" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma unidade</option>
                        @if(count($unidades_gestoras)>0)
                            @foreach ($unidades_gestoras as $unidade_gestora)
                                <option value="{{$unidade_gestora->id_unidade_gestora}}">{{$unidade_gestora->no_unidade_gestora}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="campos-unidade-judiciaria" class="campos-destinatario col-md-6 row" style="display: none">
                <div class="col-md-6">
                    <label class="small">Unidade judiciária</label>
                    <select name="id_unidade_judiciaria" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma unidade</option>
                        @if(count($unidades_judiciarias)>0)
                            @foreach ($unidades_judiciarias as $unidade_judiciaria)
                                <option value="{{$unidade_judiciaria->id_unidade_judiciaria}}">{{$unidade_judiciaria->no_unidade_judiciaria}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">Subunidade</label>
                    <select name="id_unidade_judiciaria_divisao" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($unidade_judiciaria_divisoes)>0)
                            @foreach ($unidade_judiciaria_divisoes as $unidade_judiciaria_divisao)
                                <option value="{{$unidade_judiciaria_divisao->id_unidade_judiciaria_divisao}}">{{$unidade_judiciaria_divisao->no_unidade_judiciaria_divisao}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <label>&nbsp;</label>
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button class="incluir-destinatario disabled btn btn-default btn-inline" type="button" disabled>Incluir</button>
                    </div>
                    <div class="btn-group">
                        <button class="incluir-todos-destinatarios disabled btn btn-primary btn-inline" type="button" disabled>Incluir todos</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Destinatários escolhidos</legend>
            <div id="destinatarios-selecionados" class="btn-list col-md-12 clearfix"></div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-6">
        <fieldset>
            <legend>Tipo de custas</legend>
            <div class="col-md-12">
                @if(count($tipo_custas)>0)
                    @foreach($tipo_custas as $tipo_custa)
                        <div class="radio">
                            <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}"
                                   value="{{$tipo_custa->id_tipo_custa}}">
                            <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                {{$tipo_custa->no_tipo_custa}}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <div id="msg-isencao" class="col-md-12 clearfix" style="margin-top:10px;display:none">
            <div class="alert alert-danger single">
                Para que o processamento do Ofício se dê com AJG será necessário anexar o despacho de deferimento ou escolha outra opção para dar continuidade à solicitação.
            </div>
        </div>
        <div id="documento-isencao" class="clearfix" style="display:none">
            <fieldset>
                <legend>Documento de isenção de emolumentos</legend>
                <div id="arquivos-isencao" class="col-md-12">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="16" data-token="{{$oficio_token}}" data-limite="1">Adicionar arquivo</button>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Documento</legend>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Classificação</label>
                        <select name="id_classificacao_documento" class="form-control pull-left">
                            <option value="0">Selecione</option>
                            @if(count($classificacoes)>0)
                                @foreach ($classificacoes as $classificacao)
                                    <option value="{{$classificacao->id_classificacao_documento}}">{{$classificacao->no_classificacao_documento}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Tipo do documento</label>
                        <select name="id_tipo_documento" class="form-control pull-left" disabled>
                            <option value="0">Selecione</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Documento (descritivo)</label>
                        <textarea name="no_documento" class="form-control" rows="3" maxlength="250"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="alert alert-info small single">
                            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                            <div class="menssagem">Você digitou <span class="total">0</span> caractere(s) dos 250 possíveis. <span class="limite"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Arquivos</legend>
            <div id="arquivos-gerais" class="col-md-12">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="15" data-token="{{$oficio_token}}" data-limite="0">Adicionar arquivo</button>
            </div>
        </fieldset>
    </div>
</div>
<div id="assinaturas-arquivos" class="fieldset-group clearfix">
    <div class="col-md-12">
        <div class="alert alert-warning single">
            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
            <div class="menssagem clearfix">
                <span>Nenhum arquivo foi inserido.</span><br />
                <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$oficio_token}}">Assinar arquivo(s)</a>
            </div>
        </div>
    </div>
</div>