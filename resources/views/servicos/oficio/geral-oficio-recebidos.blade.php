@extends('servicos.oficio.geral-oficio')

@section('sub-content')
    <form name="form-filtro" method="post" action="" class="clearfix">
        {{csrf_field()}}
        <fieldset class="clearfix">
            <legend>Filtro</legend>
            <div class="fieldset-group clearfix">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Número documento</label>
                        <input type="text" name="numero_documento" id="numero_documento" class="form-control"
                               value="{{$request->numero_documento}}"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Número processo</label>
                        <input type="text" name="numero_processo" id="numero_processo" class="form-control"
                               value="{{$request->numero_processo}}"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Número protocolo</label>
                        <input type="text" name="protocolo_pedido" id="protocolo_pedido" class="form-control"
                               value="{{$request->protocolo_pedido}}"/>
                    </div>
                </div>
            </div>
            <div class="buttons col-md-12 text-right">
                <input type="reset" value="Limpar filtros" class="btn btn-primary">
                <input type="submit" value="Filtrar ofícios" class="btn btn-success">
            </div>
        </fieldset>
    </form>
    <div class="panel table-rounded">
        <table id="oficios-recebidos" class="table table-striped table-bordered table-fixed small">
            <thead>
            <tr class="gradient01">
                <th width="20%">Protocolo</th>
                <th width="12%">Data</th>
                <th width="15%">Tipo</th>
                <th width="21%">Número documento / <br/>Número processo</th>
                <th width="29%">Of. Origem / Enviado Por:</th>
                <th width="10%">Custas</th>
                <th width="12%">Situação</th>
                <th width="19%">Ações</th>
            </tr>
            </thead>
            <tbody>
            @if (count($documentos)>0)
                @foreach ($documentos as $documento)
                    <tr id="{{$documento->id_documento}}"
                        @if(count($documento->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
                        <td>
                            {{$documento->pedido->protocolo_pedido}}
                            @if(count($documento->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                            @if($documento->id_documento_anterior>0)
                                <br/><strong>[Re] Of. {{$documento->documento_anterior->numero_protocolo}}</strong>
                            @endif
                        </td>
                        <td class="col-md-1">{{Carbon\Carbon::parse($documento->dt_criacao)->format('d/m/Y H:i')}}</td>
                        <td>
                            @if($documento->tipo_documento)
                                {{$documento->tipo_documento->no_tipo_documento}}
                            @else
                                -
                            @endif
                        </td>
                        <td class="col-md-3">
                            @if($documento->numero_documento)
                                {{$documento->numero_documento}}
                            @else
                                -
                            @endif
                            /<br/>
                            @if($documento->numero_processo)
                                {{$documento->numero_processo}}
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            {{--Recebimento do nome do Usuario--}}
                            @if($documento->id_documento_anterior>0)
                                {{$documento->documento_anterior->usuario->no_usuario}} <br>
                                ({{$documento->documento_anterior->usuario_pessoa[0]->pessoa->no_pessoa}})
                            @else
                                {{$documento->usuario->no_usuario}} <br>
                                ({{$documento->usuario_pessoa[0]->pessoa->no_pessoa}})
                            @endif
                        </td>
                        <td class="col-md-2">{{$documento->va_custa or '0,00'}}</td>
                        <td class="situacao">
                            @if($documento->documento_enviado->in_respondido =='S')
                                Respondido
                            @elseif ($documento->documento_enviado->in_leitura =='S')
                                Lida
                            @else
                                Não Lida
                            @endif
                        </td>
                        <td class="options">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#detalhes-oficio" data-iddocumento="{{$documento->id_documento}}"
                                        data-protocolo="{{$documento->numero_protocolo}}" data-origem="R">Detalhes
                                </button>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-toggle="modal" data-target="#observacoes-oficio"
                                               data-iddocumento="{{$documento->id_documento}}"
                                               data-protocolo="{{$documento->numero_protocolo}}" data-origem="R">Observações</a>
                                        </li>
                                        @if(!in_array($class->id_tipo_pessoa,array(9,13)))
                                            <li><a href="#" data-toggle="modal" data-target="#responder-oficio"
                                                   data-iddocumento="{{$documento->id_documento}}"
                                                   data-protocolo="{{$documento->numero_protocolo}}" data-origem="R">Responder</a>
                                            </li>
                                            <li><a href="#" data-toggle="modal" data-target="#encaminhar-oficio"
                                                   data-iddocumento="{{$documento->id_documento}}"
                                                   data-protocolo="{{$documento->numero_protocolo}}" data-origem="R">Encaminhar</a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhum oficio eletrônico recebido.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        Exibindo <b>{{count($documentos)}}</b> de
        <b>{{$documentos->total()}}</b> {{($documentos->total()>1?'ofícios':'ofício')}}.
    </div>
    <div align="center">
        {{$documentos->fragment('oficios-recebidos')->render()}}
    </div>
    <br>
@endsection