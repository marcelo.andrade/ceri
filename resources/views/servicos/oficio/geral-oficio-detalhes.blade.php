<?php
$arquivos = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',15)->get();
$arquivos_isencao = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',16)->get();

$destinatario = $documento->pedido->pedido_pessoa_atual->pessoa;
$remetente = $documento->pedido->pessoa_origem;
?>
@if ($documento->in_respondido=='S')
    <div class="col-md-12">
        <div align="center" class="alert alert-warning">
            <h4><strong>OFÍCIO DE RESPOSTA</strong>: {{$documento->numero_protocolo }}</h4>
        </div>
    </div>
@endif
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
@if($class->id_pessoa<>$documento->pedido->id_pessoa_origem or in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Remetente</legend>
                <?php
                switch ($remetente->id_tipo_pessoa) {
                    case 2:
                ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Cidade</label>
                                <input type="text" name="no_cidade" class="form-control" value="{{$remetente->enderecos[0]->cidade->no_cidade}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Serventia</label>
                                <input type="text" name="no_serventia" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Usuário</label>
                                <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                    case 4:
                ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Comarca</label>
                                <input type="text" name="no_comarca" class="form-control" value="{{$remetente->vara->comarca->no_comarca}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Vara</label>
                                <input type="text" name="no_vara" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Usuário</label>
                                <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                    case 7:
                ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Unidade gestora</label>
                                <input type="text" name="no_unidade_gestora" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Usuário</label>
                                <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                    case 11:
                ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Unidade judiciária</label>
                                <input type="text" name="no_unidade_judiciaria" class="form-control" value="{{$remetente->unidade_judiciaria_divisao->unidade_judiciaria->no_unidade_judiciaria}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Subunidade</label>
                                <input type="text" name="no_unidade_judiciaria_divisao" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="small">Usuário</label>
                                <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                }
                ?>
        </fieldset>
    </div>
</div>
@endif
@if($class->id_pessoa==$documento->pedido->id_pessoa_origem or in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Destinatário</legend>
                <?php
                switch ($destinatario->id_tipo_pessoa) {
                    case 2:
                ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Cidade</label>
                                <input type="text" name="no_cidade" class="form-control" value="{{$destinatario->enderecos[0]->cidade->no_cidade}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Serventia</label>
                                <input type="text" name="no_serventia" class="form-control" value="{{$destinatario->no_pessoa}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                    case 4:
                ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Comarca</label>
                                <input type="text" name="no_comarca" class="form-control" value="{{$destinatario->vara->comarca->no_comarca}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Vara</label>
                                <input type="text" name="no_vara" class="form-control" value="{{$destinatario->no_pessoa}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                     case 7:
                ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Unidade gestora</label>
                                <input type="text" name="no_unidade_gestora" class="form-control" value="{{$destinatario->no_pessoa}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                    case 11:
                ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Unidade judiciária</label>
                                <input type="text" name="no_unidade_judiciaria" class="form-control" value="{{$destinatario->unidade_judiciaria_divisao->unidade_judiciaria->no_unidade_judiciaria}}" disabled />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small">Subunidade</label>
                                <input type="text" name="no_unidade_judiciaria_divisao" class="form-control" value="{{$destinatario->no_pessoa}}" disabled />
                            </div>
                        </div>
                <?php
                        break;
                }
                ?>
            </fieldset>
        </div>
    </div>
@endif
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Ofício</legend>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Número documento</label>
                    <input type="text" name="numero_documento" id="numero_documento" value="{{$documento->numero_documento}}" class="form-control" disabled />
                </div>
                <div class="col-md-6">
                    <label class="small">Número processo</label>
                    <input type="text" name="numero_processo" id="numero_processo" value="{{$documento->numero_processo}}" class="form-control" disabled />
                </div>
            </div>
            @if($documento->in_encaminhado=='S' or $documento->in_respondido=='S')
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <div class="alert alert-info single">
                            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                            <div class="menssagem">
                                @if($documento->in_encaminhado=='S')
                                    Este ofício é um <b>encaminhamento</b> do ofício protocolo nº <b>{{$documento->documento_anterior->numero_protocolo}}</b>.
                                @elseif($documento->in_respondido=='S')
                                    Este ofício é uma <b>resposta</b> do ofício protocolo nº <b>{{$documento->documento_anterior->numero_protocolo}}</b>.
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-6">
        <fieldset>
            <legend>Tipo de custas</legend>
            <div class="col-md-12">
                @if(count($tipo_custas)>0)
                    @foreach($tipo_custas as $tipo_custa)
                        <div class="radio">
                            <input type="radio"  name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($documento->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif disabled>
                            <label for="id_tipo_penhora_{{$tipo_custa->id_tipo_custa}}" class="small">
                                {{$tipo_custa->no_tipo_custa}}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </fieldset>
    </div>
    @if(count($arquivos_isencao)>0)
        <div class="col-md-6">
            <fieldset>
                <legend>Documento de isenção de emolumentos</legend>
                <div class="col-md-12">
                    @foreach($arquivos_isencao as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                            @if($arquivo->in_ass_digital=='S')
                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                            @endif
                        </div>
                    @endforeach
                </div>
            </fieldset>
        </div>
    @endif
</div>
@if(count($arquivos)>0)
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Documentos anexados ao ofício</legend>
                <div class="col-md-12">
                    @foreach($arquivos as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                            @if($arquivo->in_ass_digital=='S')
                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                            @endif
                        </div>
                    @endforeach
                </div>
            </fieldset>
        </div>
    </div>
@endif
@if ($documento->documento_prenotacao)
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Prenotação</legend>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="small">Número</label>
                        <input type="text" name="codigo_prenotacao" value="{{$documento->documento_prenotacao->codigo_prenotacao}}" class="form-control"  disabled />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Data de cadastro</label>
                        <input type="text" name="dt_prenotacao" value="{{Carbon\Carbon::parse($documento->documento_prenotacao->dt_prenotacao)->format('d/m/Y')}}" class="form-control"  disabled />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endif
@if ($documento->va_custa>0)
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Custa</legend>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="small">Descricao</label>
                        <input type="text" name="de_custa" value="{{$documento->de_custa}}" class="form-control"  disabled />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Valor</label>
                        <input type="text" name="va_custa" value="{{$documento->va_custa}}" class="form-control"  disabled />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endif
<div class="fieldset-group clearfix">
	<div class="col-md-12">
        <fieldset>
            <legend>Documento</legend>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Classificação</label>
                        <select name="id_classificacao_documento" class="form-control pull-left" disabled>
                            @if($documento->classificacao_documento)
                                <option value="">{{$documento->classificacao_documento->no_classificacao_documento}}</option>
                            @else
                                <option value="">Selecione</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Tipo do documento</label>
                        <select name="id_tipo_documento" class="form-control pull-left" disabled>
                            @if($documento->tipo_documento)
                                <option value="">{{$documento->tipo_documento->no_tipo_documento}}</option>
                            @else
                                <option value="">Selecione</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            @if($documento->in_respondido=='S')
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        @if($documento->in_reposta_positivo=='S')
                            <div class="erros alert alert-success single">
                                <i class="icon glyphicon glyphicon-ok pull-left"></i>
                                <div class="menssagem">A resposta foi classificada como <b>Positiva</b></div>
                            </div>
                        @elseif($documento->in_resposta_positivo=='N')
                            <div class="erros alert alert-warning single">
                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                <div class="menssagem">A resposta foi classificada como <b>Negativa</b></div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Documento (descritivo)</label>
                    <textarea name="no_documento" class="form-control" rows="3" disabled>{{$documento->no_documento}}</textarea>
                </div>
            </div>
        </fieldset>
	</div>
</div>
@if ($documento->in_respondido=='S')
    <?php
    $arquivos_resposta = $documento->documento_anterior->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',15)->get();
    $arquivos_isencao_resposta = $documento->documento_anterior->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',16)->get();

    $destinatario_anterior = $documento->documento_anterior->pedido->pedido_pessoa_atual->pessoa;
    $remetente_anterior = $documento->documento_anterior->pedido->pessoa_origem;
    ?>
    <div class="clearfix">
        <div class="col-md-12">
            <hr />
            <div class="alert alert-info">
                <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                <div class="menssagem">Para visualizar os dados do ofício de origem basta clicar no número do ofício abaixo.</div>
            </div>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                <span class="pull-right glyphicon glyphicon-chevron-down"></span>
                                <strong>OFÍCIO DE ORIGEM:</strong> {{$documento->documento_anterior->numero_protocolo}}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="fieldset-group clearfix">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Remetente</legend>
                                        <?php
                                        switch ($remetente_anterior->id_tipo_pessoa) {
                                            case 2:
                                        ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Cidade</label>
                                                        <input type="text" name="no_cidade" class="form-control" value="{{$remetente_anterior->enderecos[0]->cidade->no_cidade}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Serventia</label>
                                                        <input type="text" name="no_serventia" class="form-control" value="{{$remetente_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Usuário</label>
                                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                            case 4:
                                        ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Comarca</label>
                                                        <input type="text" name="no_comarca" class="form-control" value="{{$remetente_anterior->vara->comarca->no_comarca}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Vara</label>
                                                        <input type="text" name="no_vara" class="form-control" value="{{$remetente_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Usuário</label>
                                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                            case 7:
                                        ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Unidade gestora</label>
                                                        <input type="text" name="no_unidade_gestora" class="form-control" value="{{$remetente_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Usuário</label>
                                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                            case 11:
                                        ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Unidade judiciária</label>
                                                        <input type="text" name="no_unidade_judiciaria" class="form-control" value="{{$remetente_anterior->unidade_judiciaria_divisao->unidade_judiciaria->no_unidade_judiciaria}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Subunidade</label>
                                                        <input type="text" name="no_unidade_judiciaria_divisao" class="form-control" value="{{$remetente_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="small">Usuário</label>
                                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                        }
                                        ?>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="fieldset-group clearfix">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Destinatário</legend>
                                        <?php
                                        switch ($destinatario_anterior->id_tipo_pessoa) {
                                            case 2:
                                        ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Cidade</label>
                                                        <input type="text" name="no_cidade" class="form-control" value="{{$destinatario_anterior->enderecos[0]->cidade->no_cidade}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Serventia</label>
                                                        <input type="text" name="no_serventia" class="form-control" value="{{$destinatario_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                            case 4:
                                        ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Comarca</label>
                                                        <input type="text" name="no_comarca" class="form-control" value="{{$destinatario_anterior->vara->comarca->no_comarca}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Vara</label>
                                                        <input type="text" name="no_vara" class="form-control" value="{{$destinatario_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                             case 7:
                                        ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Unidade gestora</label>
                                                        <input type="text" name="no_unidade_gestora" class="form-control" value="{{$destinatario_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                            case 11:
                                        ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Unidade judiciária</label>
                                                        <input type="text" name="no_unidade_judiciaria" class="form-control" value="{{$destinatario_anterior->unidade_judiciaria_divisao->unidade_judiciaria->no_unidade_judiciaria}}" disabled />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small">Subunidade</label>
                                                        <input type="text" name="no_unidade_judiciaria_divisao" class="form-control" value="{{$destinatario_anterior->no_pessoa}}" disabled />
                                                    </div>
                                                </div>
                                        <?php
                                                break;
                                        }
                                        ?>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="fieldset-group clearfix">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Ofício</legend>
                                        <div class="form-group clearfix">
                                            <div class="col-md-6">
                                                <label class="small">Número documento</label>
                                                <input type="text" name="numero_documento_resposta" value="{{$documento->documento_anterior->numero_documento}}" class="form-control" disabled />
                                            </div>
                                            <div class="col-md-6">
                                                <label class="small">Número processo</label>
                                                <input type="text" name="numero_processo_resposta" value="{{$documento->documento_anterior->numero_processo}}" class="form-control" disabled />
                                            </div>
                                        </div>
                                        @if($documento->documento_anterior->in_encaminhado=='S' or $documento->documento_anterior->in_respondido=='S')
                                            <div class="form-group clearfix">
                                                <div class="col-md-12">
                                                    <div class="alert alert-info single">
                                                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                                                        <div class="menssagem">
                                                            @if($documento->documento_anterior->in_encaminhado=='S')
                                                                Este ofício é um <b>encaminhamento</b> do ofício protocolo nº <b>{{$documento->documento_anterior->documento_anterior->numero_protocolo}}</b>.
                                                            @elseif($documento->documento_anterior->in_respondido=='S')
                                                                Este ofício é uma <b>resposta</b> do ofício protocolo nº <b>{{$documento->documento_anterior->documento_anterior->numero_protocolo}}</b>.
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </fieldset>
                                </div>
                            </div>
                            <div class="fieldset-group clearfix">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend>Tipo de custas</legend>
                                        <div class="col-md-12">
                                            @if(count($tipo_custas)>0)
                                                @foreach($tipo_custas as $tipo_custa)
                                                    <div class="radio">
                                                        <input type="radio" name="id_tipo_custa_resposta" id="id_tipo_custa_resposta_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($documento->documento_anterior->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif disabled>
                                                        <label for="id_tipo_custa_resposta_{{$tipo_custa->id_tipo_custa}}" class="small">
                                                            {{$tipo_custa->no_tipo_custa}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </fieldset>
                                </div>
                                @if(count($arquivos_isencao_resposta)>0)
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend>Documento de isenção de emolumentos</legend>
                                            <div class="col-md-12">
                                                @foreach($arquivos_isencao_resposta as $arquivo)
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                        @if($arquivo->in_ass_digital=='S')
                                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        </fieldset>
                                    </div>
                                @endif
                            </div>
                            @if(count($arquivos_resposta)>0)
                                <div class="fieldset-group clearfix">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend>Documentos anexados ao ofício</legend>
                                            <div class="col-md-12">
                                                @foreach($arquivos_resposta as $arquivo)
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                        @if($arquivo->in_ass_digital=='S')
                                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            @endif
                            @if ($documento->documento_anterior->documento_prenotacao)
                                <div class="fieldset-group clearfix">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend>Prenotação</legend>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="small">Número</label>
                                                    <input type="text" name="codigo_prenotacao_resposta" value="{{$documento->documento_anterior->documento_prenotacao->codigo_prenotacao}}" class="form-control"  disabled />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="small">Data de cadastro</label>
                                                    <input type="text" name="dt_prenotacao_resposta" value="{{Carbon\Carbon::parse($documento->documento_anterior->documento_prenotacao->dt_prenotacao)->format('d/m/Y')}}" class="form-control"  disabled />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            @endif
                            @if ($documento->documento_anterior->va_custa>0)
                                <div class="fieldset-group clearfix">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend>Custa</legend>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="small">Descricao</label>
                                                    <input type="text" name="de_custa_resposta" value="{{$documento->documento_anterior->de_custa}}" class="form-control"  disabled />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="small">Valor</label>
                                                    <input type="text" name="va_custa_resposta" value="{{$documento->documento_anterior->va_custa}}" class="form-control"  disabled />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            @endif
                            <div class="fieldset-group clearfix">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Documento</legend>
                                        <div class="form-group clearfix">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="small">Classificação</label>
                                                    <select name="id_classificacao_documento_resposta" class="form-control pull-left" disabled>
                                                        <option value="">{{$documento->documento_anterior->classificacao_documento->no_classificacao_documento}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="small">Tipo do documento</label>
                                                    <select name="id_tipo_documento_resposta" class="form-control pull-left" disabled>
                                                        <option value="">{{$documento->documento_anterior->tipo_documento->no_tipo_documento}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @if($documento->documento_anterior->in_respondido=='S')
                                            <div class="form-group clearfix">
                                                <div class="col-md-12">
                                                    @if($documento->documento_anterior->in_reposta_positivo=='S')
                                                        <div class="erros alert alert-success single">
                                                            <i class="icon glyphicon glyphicon-ok pull-left"></i>
                                                            <div class="menssagem">A resposta foi classificada como <b>Positiva</b></div>
                                                        </div>
                                                    @elseif($documento->documento_anterior->in_resposta_positivo=='N')
                                                        <div class="erros alert alert-warning single">
                                                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                                            <div class="menssagem">A resposta foi classificada como <b>Negativa</b></div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                        <div class="form-group clearfix">
                                            <div class="col-md-12">
                                                <label class="small">Documento (descritivo)</label>
                                                <textarea name="no_documento_resposta" class="form-control" rows="3" disabled>{{$documento->documento_anterior->no_documento}}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if($origem!='H')
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Histórico de ofícios</legend>
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table id="historico-observacoes" class="table table-striped table-bordered small">
                            <thead>
                            <tr class="gradient01">
                                <th width="17%">Número do processo</th>
                                <th width="53%">Descritivo</th>
                                <th width="10%">Encaminhado</th>
                                <th width="10%">Respondido</th>
                                <th width="10%">Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if ($documento->id_documento_anterior>0)
                                    @while($documento->id_documento_anterior>0)
                                        <tr>
                                            <td>{{$documento->documento_anterior->numero_protocolo}}</td>
                                            <td>
                                                @if($documento->documento_anterior->in_respondido == 'S')
                                                    {{$documento->documento_anterior->de_resposta}}
                                                @else
                                                    {{$documento->documento_anterior->no_documento}}
                                                @endif
                                            </td>
                                            <td>{{($documento->documento_anterior->in_encaminhado=='S'?'Sim':'Não')}}</td>
                                            <td>{{($documento->documento_anterior->in_respondido=='S'?'Sim':'Não')}}</td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-oficio-historico" data-iddocumento="{{$documento->documento_anterior->id_documento}}" data-protocolo="{{$documento->documento_anterior->numero_protocolo}}" data-origem="H">Detalhes</button>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $documento = $documento->documento_anterior;
                                        ?>
                                    @endwhile
                                @else
                                    <tr>
                                        <td colspan="5">
                                            <div class="single alert alert-danger">
                                                <i class="glyphicon glyphicon-remove"></i>
                                                <div class="mensagem">
                                                    Não há histórico a ser exibido.
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endif