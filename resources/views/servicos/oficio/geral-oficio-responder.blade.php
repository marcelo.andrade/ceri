<?php
$desativar_prenotacao = '';
$codigo_prenotacao = '';
$dt_prenotacao = '';
if ($documento->documento_prenotacao) {
    $desativar_prenotacao = 'disabled';
    $codigo_prenotacao = $documento->documento_prenotacao->codigo_prenotacao;
    $dt_prenotacao = Carbon\Carbon::parse($documento->documento_prenotacao->dt_prenotacao)->format('d/m/Y');
}

$desativar_custas = "";
$de_custa = "";
$va_custa = "";
if ($documento->va_custa>0){
    $desativar_custas = "disabled";
    $de_custa = $documento->de_custa;
    $va_custa = $documento->va_custa;
}

$remetente = $documento->pedido->pessoa_origem;
?>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <span class="pull-right glyphicon glyphicon-chevron-up"></span>
                    <strong>OFÍCIO DE ORIGEM</strong>: {{$documento->numero_protocolo}}<br/>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Remetente</legend>
                        <?php
                        switch ($remetente->id_tipo_pessoa) {
                            case 2:
                        ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Cidade</label>
                                        <input type="text" name="no_cidade" class="form-control" value="{{$remetente->enderecos[0]->cidade->no_cidade}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Serventia</label>
                                        <input type="text" name="no_serventia" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Usuário</label>
                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                    </div>
                                </div>
                        <?php
                                break;
                            case 4:
                        ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Comarca</label>
                                        <input type="text" name="no_comarca" class="form-control" value="{{$remetente->vara->comarca->no_comarca}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Vara</label>
                                        <input type="text" name="no_vara" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Usuário</label>
                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                    </div>
                                </div>
                        <?php
                                break;
                            case 7:
                        ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="small">Unidade gestora</label>
                                        <input type="text" name="no_unidade_gestora" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="small">Usuário</label>
                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                    </div>
                                </div>
                        <?php
                                break;
                            case 11:
                        ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Unidade judiciária</label>
                                        <input type="text" name="no_unidade_judiciaria" class="form-control" value="{{$remetente->unidade_judiciaria_divisao->unidade_judiciaria->no_unidade_judiciaria}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Subunidade</label>
                                        <input type="text" name="no_unidade_judiciaria_divisao" class="form-control" value="{{$remetente->no_pessoa}}" disabled />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Usuário</label>
                                        <input type="text" name="no_usuario" class="form-control" value="{{$documento->usuario->no_usuario}}" disabled />
                                    </div>
                                </div>
                        <?php
                                break;
                        }
                        ?>
                    </fieldset>
                </div>
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Ofício</legend>
                        <div class="form-group clearfix">
                            <div class="col-md-6">
                                <label class="small">Número documento</label>
                                <input type="text" name="numero_documento" id="numero_documento" value="{{$documento->numero_documento}}" class="form-control" disabled />
                            </div>
                            <div class="col-md-6">
                                <label class="small">Número processo</label>
                                <input type="text" name="numero_processo" id="numero_processo" value="{{$documento->numero_processo}}" class="form-control" disabled />
                            </div>
                        </div>
                        @if($documento->in_encaminhado=='S' or $documento->in_respondido=='S')
                            <div class="form-group clearfix">
                                <div class="col-md-12">
                                    <div class="alert alert-info single">
                                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                                        <div class="menssagem">
                                            @if($documento->in_encaminhado=='S')
                                                Este ofício é um <b>encaminhamento</b> do ofício protocolo nº <b>{{$documento->documento_anterior->numero_protocolo}}</b>.
                                            @elseif($documento->in_respondido=='S')
                                                Este ofício é uma <b>resposta</b> do ofício protocolo nº <b>{{$documento->documento_anterior->numero_protocolo}}</b>.
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </fieldset>
                </div>
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Documento</legend>
                        <div class="col-md-12">
                            <label class="small">Documento (descritivo)</label>
                            <textarea name="no_documento" class="form-control" rows="3" disabled>{{$documento->no_documento}}</textarea>
                        </div>
                    </fieldset>
                </div>
                <div class="fieldset-group clearfix row">
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Tipo de custas</legend>
                            <div class="col-md-12">
                                @if(count($tipo_custas)>0)
                                    @foreach($tipo_custas as $tipo_custa)
                                        <div class="radio">
                                            <input type="radio"  name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($documento->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif disabled>
                                            <label for="id_tipo_penhora_{{$tipo_custa->id_tipo_custa}}" class="small">
                                                {{$tipo_custa->no_tipo_custa}}
                                            </label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </fieldset>
                    </div>
                    @if(count($arquivos_isencao)>0)
                        <div class="col-md-6">
                            <fieldset>
                                <legend>Documento de isenção de emolumentos</legend>
                                <div class="col-md-12">
                                    @foreach($arquivos_isencao as $arquivo)
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                            @if($arquivo->in_ass_digital=='S')
                                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </fieldset>
                        </div>
                    @endif
                </div>
                @if(count($arquivos)>0)
                    <div class="fieldset-group clearfix">
                        <fieldset>
                            <legend>Documentos anexados ao ofício</legend>
                            <div class="col-md-12">
                                @foreach($arquivos as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                        @if($arquivo->in_ass_digital=='S')
                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </fieldset>
                    </div>
                @endif

                @if (Auth::User()->id_tipo_usuario==$class::TP_USUARIO_SERVENTIA)
                    <div class="fieldset-group clearfix">
                        <fieldset>
                            <legend>Prenotação</legend>
                            <form name="form-inserir-prenotacao" method="post" action="" class="clearfix" enctype="multipart/form-data">
                                <input type="hidden" name="id_documento" value="{{$documento->id_documento}}" />
                                <div class="col-md-12">
                                    <div class="erros-prenotacao alert alert-danger" style="display:none">
                                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                                        <div class="menssagem"></div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="col-md-6">
                                        <label class="small">Número</label>
                                        <input type="text" name="codigo_prenotacao" class="form-control" value="{{$codigo_prenotacao}}" {{$desativar_prenotacao}} maxlength="20" />
                                    </div>
                                    <div class="col-md-6">
                                        <label class="small">Data de cadastro</label>
                                        <input type="text" name="dt_prenotacao" class="data form-control" value="{{$dt_prenotacao}}" {{$desativar_prenotacao}} maxlength="10" />
                                    </div>
                                </div>
                                @if(!$desativar_prenotacao)
                                    <div class="botoes form-group clearfix">
                                        <div class="col-md-12">
                                            <input type="reset"  class="btn btn-primary pull-left" value="Cancelar" />
                                            <input type="submit" class="btn btn-success pull-right" value="Salvar prenotação" />
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </fieldset>
                    </div>
                    <div class="fieldset-group clearfix row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Custas</legend>
                                <form name="form-inserir-custas" id="form-inserir-custas" method="post" action="" class="clearfix">
                                    <input type="hidden" name="id_documento" id="id_documento" value="{{$documento->id_documento}}">
                                    <div class="col-md-12">
                                        <div class="erros-custas alert alert-danger" style="display:none">
                                            <i class="icon glyphicon glyphicon-remove pull-left"></i>
                                            <div class="menssagem"></div>
                                        </div>
                                    </div>
                                    <div class="form-group clearfix">
                                        <div class="col-md-8">
                                            <label class="small">Descrição</label>
                                            <input type="text" name="de_custa" value="{{$de_custa}}" class="form-control" {{$desativar_custas}} maxlength="70" />
                                        </div>
                                        <div class="col-md-4">
                                            <label class="small">Valor</label>
                                            <input type="text" name="va_custa" value="{{$va_custa}}" class="real form-control" {{$desativar_custas}} maxlength="10" />
                                        </div>
                                    </div>
                                    @if(!$desativar_custas)
                                        <div class="botoes form-group clearfix">
                                            <div class="col-md-12">
                                                <input type="reset"  class="btn btn-primary pull-left" value="Cancelar" />
                                                <input type="submit" class="btn btn-success pull-right" value="Salvar custas" />
                                            </div>
                                        </div>
                                    @endif
                                </form>
                            </fieldset>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <span class="pull-right glyphicon glyphicon-chevron-up"></span>
                    <strong>RESPONDER PARA:</strong><br />
                    {{$documento->pedido->pessoa_origem->no_pessoa}} / {{$documento->usuario->no_usuario}}
                    <?php
                    /*switch ($documento->usuario->id_tipo_usuario) {
                        case 2:
                            echo $documento->usuario->usuario_serventia->serventia->pessoa->enderecos[0]->cidade->no_cidade.' / '.$documento->usuario->usuario_serventia->serventia->no_serventia.' / '.$documento->usuario->no_usuario;
                            break;
                        case 4:
                            echo $documento->usuario->usuario_vara->vara->comarca->no_comarca.' / '.$documento->usuario->usuario_vara->vara->no_vara.' / '.$documento->usuario->no_usuario;
                            break;
                    }*/
                    ?>
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse in">
            <div class="panel-body">
                <form name="form-responder" method="post" action="" class="clearfix" enctype="multipart/form-data">
                    <input type="hidden" name="id_documento" value="{{$documento->id_documento}}" />
                    <input type="hidden" name="oficio_token" id="oficio_token" value="{{$oficio_token}}" />
                    <div class="erros-responder alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                        <div class="menssagem"></div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <fieldset>
                            <legend>Documento</legend>
                            <div class="form-group clearfix">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Classificação</label>
                                        <select name="id_classificacao_documento" class="form-control pull-left" >
                                            <option value="0">Selecione</option>
                                            @if(count($classificacoes)>0)
                                                @foreach ($classificacoes as $classificacao)
                                                    <option value="{{$classificacao->id_classificacao_documento}}" {{($documento->id_classificacao_documento==$classificacao->id_classificacao_documento?'selected':'')}}>{{$classificacao->no_classificacao_documento}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="small">Tipo do documento</label>
                                        <select name="id_tipo_documento" class="form-control pull-left" >
                                            <option value="0">Selecione um tipo</option>
                                            @if(count($tipos_documento)>0)
                                                @foreach ($tipos_documento as $tipo_documento)
                                                    <option value="{{$tipo_documento->id_tipo_documento}}" {{($documento->id_tipo_documento==$tipo_documento->id_tipo_documento?'selected':'')}}>{{$tipo_documento->no_tipo_documento}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-4">
                                    <fieldset>
                                        <legend>Tipo de resposta</legend>
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <input name="in_reposta_positivo" id="in_reposta_positivo_S" value="S" type="radio">
                                                <label for="in_reposta_positivo_S" class="small">
                                                    Positiva
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <input name="in_reposta_positivo" id="in_reposta_positivo_N" value="N" type="radio">
                                                <label for="in_reposta_positivo_N" class="small">
                                                    Negativa
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-8">
                                    <fieldset>
                                        <legend>Resposta (descritivo)</legend>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea name="de_resposta" class="form-control" rows="3" disabled maxlength="250"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-info small single">
                                                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                                                    <div class="menssagem">Você digitou <span class="total">0</span> caractere(s) dos 250 possíveis. <span class="limite"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="fieldset-group clearfix">
                        <fieldset>
                            <legend>Arquivos</legend>
                            <div id="arquivos-gerais" class="col-md-12">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="15" data-token="{{$oficio_token}}" data-limite="0">Adicionar arquivo</button>
                            </div>
                        </fieldset>
                    </div>
                    <div id="assinaturas-arquivos" class="fieldset-group clearfix">
                        <div class="alert alert-warning single">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem clearfix">
                                <span>Nenhum arquivo foi inserido.</span><br />
                                <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$oficio_token}}">Assinar arquivo(s)</a>
                            </div>
                        </div>
                    </div>
                    <div class="botoes fieldset-group clearfix">
                        <input type="reset"  class="btn btn-primary pull-left" value="Cancelar" />
                        <input type="submit" class="btn btn-success pull-right" value="Responder ofício" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>