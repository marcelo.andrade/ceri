@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.oficio.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.arquivos.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="oficio panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Ofício eletrônico <span class="small">/ Serviços</span></h4>
            </div>
            <div class="panel-body no-padding">
                <nav class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        @if(!in_array($class->id_tipo_pessoa,array(9,13)))
                            <li @if(strstr(Route::current()->getPath(),'novo')) class="active" @endif><a href="#" class="novo" data-toggle="modal" data-target="#novo-oficio"><i class="glyphicon glyphicon-file"></i> Novo ofício</a></li>
                        @endif
                    	<li @if(strstr(Route::current()->getPath(),'documentos')) class="active" @endif>
                            <a href="javascript:void(0);"><i class="glyphicon glyphicon-menu-down"></i> Documentos</a>
                            <ul class="nav nav-pills nav-stacked">
                                @if(in_array($class->id_tipo_pessoa,array(9,13)))
                                    <li @if(strstr(Route::current()->getPath(),'recebidos')) class="active" @endif><a href="recebidos">Todos</a></li>
                                @else
                                    <li @if(strstr(Route::current()->getPath(),'recebidos')) class="active" @endif><a href="recebidos">Recebidos</a></li>
                                    <li @if(strstr(Route::current()->getPath(),'enviados')) class="active" @endif><a href="enviados">Enviados</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </nav>
                <div class="content col-md-10">
                    <div class="carregando flutuante text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                	@yield('sub-content')
                </div>
            </div>
        </div>
    </div>
    <div id="novo-oficio" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Novo ofício</h4> 
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-oficio" method="post" action="" class="clearfix" enctype="multipart/form-data"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="enviar-oficio btn btn-success">Enviar ofício</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-oficio" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Detalhes do ofício - <span></span></h4> 
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-oficio-historico" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Detalhes do ofício (Histórico) - <span></span></h4> 
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="observacoes-oficio" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Observações do ofício - <span></span></h4> 
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="encaminhar-oficio" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Encaminhar ofício - <span></span></h4> 
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-encaminhar" method="post" action="" class="clearfix" enctype="multipart/form-data"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="encaminhar-oficio btn btn-success">Encaminhar ofício</button>
                </div>
            </div>
        </div>
    </div>
    <div id="responder-oficio" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Responder ofício - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="recibo-oficio" class="total-height modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recibo do ofício <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <a id="resultado-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do documento</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    @include('arquivos.arquivos-modais')
@endsection