<?php
    if ( $documento->va_custa != "R$ 0,00" )
    {
        $disabled   = "disabled";
        $de_custa   = $documento->de_custa;
        $va_custa   = $documento->va_custa;
        $class_real = '';
    }else{
        $disabled   = "";
        $de_custa   = "";
        $va_custa   = "";
        $class_real = 'real';
    }
?>

<form name="form-inserir-custas" id="form-inserir-custas" method="post" action="" class="clearfix">
    <input type="hidden" name="id_documento" id="id_documento" value="{{ $documento->id_documento or '' }}">
    <div class="erros alert alert-danger" style="display:none">
        <i class="icon glyphicon glyphicon-remove pull-left"></i>
        <div class="menssagem"></div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Custa</legend>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="small">Descrição</label>
                        <input type="text" name="de_custa" value="{{$de_custa}}" class="form-control"  {{$disabled}} />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Valor</label>
                        <input type="text" name="va_custa" value="{{$va_custa}}" class="{{$class_real}} form-control"  {{$disabled}} />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</form>
