@extends('servicos.oficio')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.configuracoes.dadosacesso.js?v=3.0.0')}}"></script>
@endsection

@section('sub-content')
<div class="panel table-rounded">
    <table id="protocolos-recebidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th>Protocolo</th>
                <th>Remetente</th>
                <th>Data</th>
                <th>Tipo</th>
                <th>Tamanho total</th>
                <th>Ações</th>                    
            </tr>
        </thead>
        <tbody>
            @if (1)
                <?php //@foreach ($todas_certidoes as $certidao) ?>
                    <tr>
                        <td>99345.67990.12349</td>
                        <td>Cartório 001</td>
                        <td>21/02/2016</td>
                        <td>Ofício</td>
                        <td>11kb</td>
                        <td class="options">
                            <div class="btn-group">
                                <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Ações <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu black">
                                    <li><a href="#" data-toggle="modal">Visualizar</a></li>
                                    <li><a href="#" data-toggle="modal">Excluir</a></li>
                                    <li><a href="#" data-toggle="modal">Encaminhar</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php //@endforeach ?>
            @else
                <tr>
                    <td colspan="7">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhuma certidão foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection