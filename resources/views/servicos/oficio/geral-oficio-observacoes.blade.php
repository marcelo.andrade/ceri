@if(!in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group clearfix">
        <form name="form-observacao" method="post" action="" class="clearfix" enctype="multipart/form-data">
    	    <input type="hidden" name="id_documento" value="{{$documento->id_documento}}" />
    	    <input type="hidden" name="aba_acao" value="" />
            <fieldset class="col-md-12">
                <legend>Nova observação</legend>
                <div class="col-md-12">
                    <div class="erros alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                        <div class="menssagem"></div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Observação</label>
                        <textarea name="de_observacao" class="form-control"></textarea>
                    </div>
                </div>
                <div class="botoes form-group clearfix">
                    <div class="col-md-12">
                        <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                        <input type="submit" class="btn btn-success pull-right" value="Salvar observação" />
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
@endif
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico de observações</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-observacoes" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@if(count($documento->observacoes)>0)
                            @foreach($documento->observacoes as $observacao)
                                <tr>
                                    <td>{{$observacao->usuario->no_usuario}}</td>
                                    <td>{{Carbon\Carbon::parse($observacao->dt_observacao)->format('d/m/Y H:i')}}</td>
                                    <td>{{$observacao->de_observacao}}</td>
                                </tr>
                            @endforeach
						@endif
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>