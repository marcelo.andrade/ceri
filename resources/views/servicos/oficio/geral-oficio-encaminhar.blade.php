<?php
$encaminhado_checked = ( $documento['in_encaminhado'] == 'S') ? 'checked' : '';
$respondido_checked  = ( $documento['in_respondido']  == 'S') ? 'checked' : '';
?>
<input type="hidden" name="id_documento" value="{{$documento->id_documento}}" />
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Destinatário</legend>
            <div class="col-md-2">
                <label class="small">Tipo de destinário</label>
                <select name="id_tipo_usuario" class="form-control pull-left" data-ultimo="0">
                    <option value="0">Selecione</option>
                    @if (Auth::User()->id_tipo_usuario!=4)
                        <option value="4">Judiciário</option>
                    @endif
                    <option value="2">Serventia</option>
                    <option value="7">Unidade gestora</option>
                    <option value="11">Unidade judiciária</option>
                </select>
            </div>
            <div id="campos-serventia" class="col-md-7 row" style="display: none">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma cidade</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">Serventia</label>
                    <select name="id_serventia" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($serventias)>0)
                            @foreach ($serventias as $serventia)
                                <option value="{{$serventia->id_serventia}}">{{$serventia->no_serventia}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="campos-judiciario" class="col-md-7 row" style="display: none">
                <div class="col-md-6">
                    <label class="small">Comarca</label>
                    <select name="id_comarca" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma comarca</option>
                        @if(count($comarcas)>0)
                            @foreach ($comarcas as $comarca)
                                <option value="{{$comarca->id_comarca}}">{{$comarca->no_comarca}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">Vara</label>
                    <select name="id_vara" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($varas)>0)
                            @foreach ($varas as $vara)
                                <option value="{{$vara->id_vara}}">{{$vara->no_vara}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="campos-unidade-gestora" class="col-md-4 row" style="display: none">
                <div class="col-md-12">
                    <label class="small">Unidade</label>
                    <select name="id_unidade_gestora" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma unidade</option>
                        @if(count($unidades_gestoras)>0)
                            @foreach ($unidades_gestoras as $unidade_gestora)
                                <option value="{{$unidade_gestora->id_unidade_gestora}}">{{$unidade_gestora->no_unidade_gestora}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="campos-unidade-judiciaria" class="campos-destinatario col-md-6 row" style="display: none">
                <div class="col-md-6">
                    <label class="small">Unidade judiciária</label>
                    <select name="id_unidade_judiciaria" class="form-control pull-left" disabled>
                        <option value="0">Selecione uma unidade</option>
                        @if(count($unidades_judiciarias)>0)
                            @foreach ($unidades_judiciarias as $unidade_judiciaria)
                                <option value="{{$unidade_judiciaria->id_unidade_judiciaria}}">{{$unidade_judiciaria->no_unidade_judiciaria}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">Subunidade</label>
                    <select name="id_unidade_judiciaria_divisao" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($unidade_judiciaria_divisoes)>0)
                            @foreach ($unidade_judiciaria_divisoes as $unidade_judiciaria_divisao)
                                <option value="{{$unidade_judiciaria_divisao->id_unidade_judiciaria_divisao}}">{{$unidade_judiciaria_divisao->no_unidade_judiciaria_divisao}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <label>&nbsp;</label>
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button class="incluir-destinatario disabled btn btn-default btn-inline" type="button" disabled>Incluir</button>
                    </div>
                    <div class="btn-group">
                        <button class="incluir-todos-destinatarios disabled btn btn-primary btn-inline" type="button" disabled>Incluir todos</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Destinatários escolhidos</legend>
            <div id="destinatarios-selecionados" class="btn-list col-md-12 clearfix"></div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Ofício</legend>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Número documento</label>
                    <input type="text" name="numero_documento" id="numero_documento" value="{{$documento->numero_documento}}" class="form-control" disabled />
                </div>
                <div class="col-md-6">
                    <label class="small">Número processo</label>
                    <input type="text" name="numero_processo" id="numero_processo" value="{{$documento->numero_processo}}" class="form-control" disabled />
                </div>
            </div>
            @if($documento->in_encaminhado=='S' or $documento->in_respondido=='S')
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <div class="alert alert-info single">
                            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                            <div class="menssagem">
                                @if($documento->in_encaminhado=='S')
                                    Este ofício é um <b>encaminhamento</b> do ofício protocolo nº <b>{{$documento->documento_anterior->numero_protocolo}}</b>.
                                @elseif($documento->in_respondido=='S')
                                    Este ofício é uma <b>resposta</b> do ofício protocolo nº <b>{{$documento->documento_anterior->numero_protocolo}}</b>.
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-6">
        <fieldset>
            <legend>Tipo de custas</legend>
            <div class="col-md-12">
                @if(count($tipo_custas)>0)
                    @foreach($tipo_custas as $tipo_custa)
                        <div class="radio">
                            <input type="radio"  name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($documento->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif disabled>
                            <label for="id_tipo_penhora_{{$tipo_custa->id_tipo_custa}}" class="small">
                                {{$tipo_custa->no_tipo_custa}}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </fieldset>
    </div>
    @if(count($arquivos_isencao)>0)
        <div class="col-md-6">
            <fieldset>
                <legend>Documento de isenção de emolumentos</legend>
                <div class="col-md-12">
                    @foreach($arquivos_isencao as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                            @if($arquivo->in_ass_digital=='S')
                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                            @endif
                        </div>
                    @endforeach
                </div>
            </fieldset>
        </div>
    @endif
</div>
@if(count($arquivos)>0)
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Documentos anexados ao ofício</legend>
                <div class="col-md-12">
                    @foreach($arquivos as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                            @if($arquivo->in_ass_digital=='S')
                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                            @endif
                        </div>
                    @endforeach
                </div>
            </fieldset>
        </div>
    </div>
@endif
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Documento</legend>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Classificação</label>
                        <select name="id_classificacao_documento" class="form-control pull-left" disabled>
                            @if($documento->classificacao_documento)
                                <option value="">{{$documento->classificacao_documento->no_classificacao_documento}}</option>
                            @else
                                <option value="">Selecione</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="small">Tipo do documento</label>
                        <select name="id_tipo_documento" class="form-control pull-left" disabled>
                            @if($documento->tipo_documento)
                                <option value="">{{$documento->tipo_documento->no_tipo_documento}}</option>
                            @else
                                <option value="">Selecione</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            @if($documento->in_respondido=='S')
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        @if($documento->in_reposta_positivo=='S')
                            <div class="erros alert alert-success single">
                                <i class="icon glyphicon glyphicon-ok pull-left"></i>
                                <div class="menssagem">A resposta foi classificada como <b>Positiva</b></div>
                            </div>
                        @elseif($documento->in_resposta_positivo=='N')
                            <div class="erros alert alert-warning single">
                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                <div class="menssagem">A resposta foi classificada como <b>Negativa</b></div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Documento (descritivo)</label>
                    <textarea name="no_documento" class="form-control" rows="3" disabled>{{$documento->no_documento}}</textarea>
                </div>
            </div>
        </fieldset>
    </div>
</div>