<form name="form-novo-processo" method="post" action="" class="clearfix">
    <div class="erros alert alert-danger" style="display:none">
        <i class="icon glyphicon glyphicon-remove pull-left"></i>
        <div class="menssagem"></div>
    </div>
	<div class="fieldset-group dadosprocesso clearfix">
        <fieldset>
            <legend>Processo</legend>
            <div class="col-md-12">
                <label class="small">Número do processo</label>
                <input type="text" name="numero_processo" class="form-control" maxlength="45" value="" />
            </div>
            <div class="col-md-12">
                <label class="small">Natureza do processo</label>
                <select name="id_natureza_acao" class="form-control">
                    <option value="0">Selecione</option>
                    @if (count($acoes)>0)
                    	@foreach ($acoes as $acao)
                        	<option value="{{$acao->id_natureza_acao}}">{{$acao->no_natureza_acao}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </fieldset>
	</div>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Cadastro das partes</legend>
            <div class="col-md-12">
                <div class="erros-partes alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
				<div class="form-group">
                    <div class="radio radio-inline">
                        <input type="radio" name="tp_pessoa" id="tp_pessoa_f" value="F">
                        <label for="tp_pessoa_f" class="small">
                            CPF
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="tp_pessoa" id="tp_pessoa_j" value="J">
                        <label for="tp_pessoa_j" class="small">
                            CNPJ
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="small">Documento identificador <span></span></label>
                    <input type="text" name="nu_cpf_cnpj" class="form-control" disabled="disabled" />
                </div>
                <div class="form-group">
                    <label class="small">Nome da parte</label>
	                <input type="text" name="no_parte" class="form-control" placeholder="Nome da parte" />
                </div>
                <button type="button" class="cadastrar-parte btn btn-info pull-left" data-tipo="1">Cadastrar como Autor</button>
                <button type="button" class="cadastrar-parte btn btn-info pull-right" data-tipo="2">Cadastrar como Réu</button>
            </div>
        </fieldset>
	</div>
    <div class="fieldset-group">
        <fieldset>
            <legend>Partes do processo</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="partes-processo" class="table table-striped table-bordered small">
                        <thead>
                            <tr class="gradient01">
                                <th>CPF / CNPJ</th>
                                <th>Nome</th>
                                <th>Tipo</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
			</div>
		</fieldset>
    </div>
</form>