@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.certidaomatricula.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.processo.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Matrícula digital <span class="small">/ Serviços</span></h4>
            </div>
            <div class="panel-body">
                @if(env('APP_ENV') != 'local')
                    <div class="erros alert alert-info single">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">
                            Prezado (a),<br />
                            De acordo com PROVIMENTO N° 146, de 18 de novembro de 2016, Capítulo I, art. 11º § 3º, o Módulo <b>"Visualização de Matrícula on-line"</b> da CERI-MS, será implantado e disponibilizado para consulta até o dia 1º de janeiro de 2018.<br /><br />
                            Assim que disponível, o (a) senhor (a) será comunicado (a) através de e-mail  a disponibilidade deste módulo.
                        </div>
                    </div>
                @else
                    <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#nova-certidao">
                        Nova matrícula digital
                    </button>
                    @include('servicos.certidaomatricula-pendencias')
                @endif
            </div>
        </div>
        @if(env('APP_ENV') != 'local')
            <div class="panel panel-default">
                <div class="panel-heading gradient01">
                    <h4>Matrículas digitais confirmadas <span class="small">/ Matrícula digital / Serviços</span></h4>
                </div>
                <div id="filtro-certidao" class="panel-body">
                    <form name="form-filtro" method="post" action="certidao-matricula#pedidos" class="clearfix">
                        {{csrf_field()}}
                        @include('servicos.certidaomatricula-filtro')
                    </form>
                    @include('servicos.certidaomatricula-historico')
                </div>
            </div>
        @endif
    </div>
    <div id="nova-certidao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Nova matrícula digital</h4>
                    <label class="div_saldo_atual saldo label label-info pull-left">Saldo atual: {{ formatar_valor( Auth::User()->saldo_usuario() ) }}</label>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-nova-certidao" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="enviar-pedidos btn btn-success">Enviar todas as matrículas</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-certidao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes matrícula digital - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="resultado-certidao" class="total-height modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Resultado matrícula digital - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    {{--<button id="resultado-certificado-digital" type="button" class="btn btn-success pull-left" data-toggle="modal" data-target="#certificado-detalhes" style="display:none"><i class="glyphicon glyphicon-check"></i> O documento foi assinado digitalmente</button>--}}
                    <a id="resultado-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do documento</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="certificado-detalhes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Dados do certificado<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-processo" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo processo</h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="incluir-processo btn btn-success">Incluir processo</button>
                </div>
            </div>
        </div>
    </div>
@endsection