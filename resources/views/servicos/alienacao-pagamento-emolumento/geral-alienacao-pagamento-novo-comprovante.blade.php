<input type="hidden" name="id_alienacao_pagamento" value="{{$alienacao_pagamento->id_alienacao_pagamento}}" />
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Detalhes do pagamento</legend>
        <div class="form-group clearfix">
        	<div class="col-md-6">
				<label class="small">Serventia</label>
				<input type="text" class="form-control" value="{{$alienacao_pagamento->serventia->no_serventia}}" disabled="disabled" />
			</div>
            <div class="col-md-2">
				<label class="small">Data de cadastro</label>
				<input type="text" class="form-control" value="{{\Carbon\Carbon::parse($alienacao_pagamento->dt_cadastro)->format('d/m/Y H:i')}}" disabled="disabled" />
			</div>
            <div class="col-md-2">
				<label class="small">Valor total</label>
				<input type="text" class="form-control real" value="{{$alienacao_pagamento->va_pagamento}}" disabled="disabled" />
			</div>
            <div class="col-md-2">
				<label class="small">Quantidade</label>
				<input type="text" class="form-control" value="{{count($alienacao_pagamento->alienacoes)}}" disabled="disabled" />
			</div>
		</div>
	</fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Notificações de alienação fidunciárias</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="notificacoes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="15%">Protocolo</th>
                            <th width="10%">Data</th>
                            <th width="10%">Contrato</th>
                            <th width="25%">Devedores</th>
                            <th width="15%">Status</th>
                            {{--<th width="10%">Custo</th>--}}
                            <th width="10%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if (count($alienacao_pagamento->alienacoes)>0)
                                @foreach ($alienacao_pagamento->alienacoes as $alienacao)
                                    <tr id="{{$alienacao->id_alienacao}}">
                                        <td>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
                                        <td>{{formatar_data_hora($alienacao->alienacao_pedido->pedido->dt_pedido)}}</td>
                                        <td>{{$alienacao->numero_contrato}}</td>
                                        <td>
                                            @if(count($alienacao->alienacao_devedor)>0)
                                                @foreach($alienacao->alienacao_devedor as $devedor)
                                                    <span class="label label-primary">{{$devedor->no_devedor}} ({{$devedor->nu_cpf_cnpj}})</span>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                                        {{--<td>-</td>--}}
                                        <td class="options">
                                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}">Ver detalhes</a>
                                        </td>                        
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">
                                        <div class="single alert alert-danger">
                                            <i class="glyphicon glyphicon-remove"></i>
                                            <div class="mensagem">
                                                Nenhuma notificação foi encontrada.
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Comprovante de pagamento</legend>
        <div class="erros alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-6">
				<label class="small">Data do pagamento</label>
				<input type="text" name="dt_pagamento" class="form-control data" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}" />
			</div>
            <div class="col-md-6">
				<label class="small">Valor pago</label>
				<input type="text" name="va_pago" class="form-control real" value="{{$alienacao_pagamento->va_pagamento}}" readonly="readonly" />
			</div>
		</div>
        <div class="form-group clearfix">
            <div class="col-md-12">
				<label class="small">Arquivo do comprovante</label>
                <div class="arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput">
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="button input-group-addon btn-file">
                        <span class="fileinput-new">Adicionar arquivo</span>
                        <span class="fileinput-exists">Alterar</span>
                        <input type="file" name="no_arquivo_comprovante">
                    </span>
                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
			</div>
		</div>
	</fieldset>
</div>