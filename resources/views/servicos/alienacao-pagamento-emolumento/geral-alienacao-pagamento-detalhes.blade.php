<input type="hidden" name="id_alienacao_pagamento" value="{{$alienacao_pagamento->id_alienacao_pagamento}}" />
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Detalhes do pagamento</legend>
        <div class="form-group clearfix">
        	<div class="col-md-6">
				<label class="small">Serventia</label>
				<input type="text" class="form-control" value="{{$alienacao_pagamento->serventia->no_serventia}}" disabled="disabled" />
			</div>
            <div class="col-md-2">
				<label class="small">Data de cadastro</label>
				<input type="text" class="form-control" value="{{\Carbon\Carbon::parse($alienacao_pagamento->dt_cadastro)->format('d/m/Y H:i')}}" disabled="disabled" />
			</div>
            <div class="col-md-2">
				<label class="small">Valor total</label>
				<input type="text" class="form-control real" value="{{$alienacao_pagamento->va_pagamento}}" disabled="disabled" />
			</div>
            <div class="col-md-2">
				<label class="small">Quantidade</label>
				<input type="text" class="form-control" value="{{count($alienacao_pagamento->alienacoes)}}" disabled="disabled" />
			</div>
		</div>
	</fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Notificações de alienação fidunciárias</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="notificacoes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="15%">Protocolo/Protocolo Int.</th>
                            {{--<th width="15%">Protocolo Int.</th>--}}
                            <th width="10%">Data</th>
                            <th width="10%">Contrato</th>
                            <th width="25%">Devedores</th>
                            <th width="15%">Valor Pago</th>
                            <th width="15%">Status</th>
                           {{-- <th width="10%">Custo</th>--}}
                            <th width="10%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if (count($alienacao_pagamento->alienacoes)>0)
                                @foreach ($alienacao_pagamento->alienacoes as $alienacao)
                                    <tr id="{{$alienacao->id_alienacao}}">
                                        <td>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}} / {{$alienacao->prenotacao()->de_texto_curto_acao or ''}}</td>
                                        {{--<td></td>--}}
                                        <td>{{formatar_data_hora($alienacao->alienacao_pedido->pedido->dt_pedido)}}</td>
                                        <td>{{$alienacao->numero_contrato}}</td>
                                        <td>
                                            @if(count($alienacao->alienacao_devedor)>0)
                                                @foreach($alienacao->alienacao_devedor as $devedor)
                                                    <span class="label label-primary">{{$devedor->no_devedor}}{{--({{$devedor->alienacao_devedor->nu_cpf_cnpj}})--}}</span>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td><?php echo formatar_valor($alienacao->valor_pago($id_alienacao_pagamento)); ?> </td>
                                        <td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                                        {{--<td><span class="real">{{$alienacao->valor_nao_pago()}}</span></td>--}}
                                        <td class="options">
                                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}">Ver detalhes</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">
                                        <div class="single alert alert-danger">
                                            <i class="glyphicon glyphicon-remove"></i>
                                            <div class="mensagem">
                                                Nenhuma notificação foi encontrada.
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
@if($alienacao_pagamento->dt_pagamento)
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Comprovante de pagamento</legend>
            <div class="erros alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Data do pagamento</label>
                    <input type="text" name="dt_pagamento" class="form-control data" value="{{\Carbon\Carbon::parse($alienacao_pagamento->dt_pagamento)->format('d/m/Y')}}" disabled="disabled" />
                </div>
                <div class="col-md-6">
                    <label class="small">Valor pago</label>
                    <input type="text" name="va_pago" class="form-control real" value="{{$alienacao_pagamento->va_pago}}" disabled="disabled" />
                </div>
            </div>
            <div class="form-group clearfix">
                <form name="form-arquivo">
                    <div id="arquivos" class="col-md-12">
                        <label class="small">Arquivo do comprovante</label><br />
                            @if($alienacao_pagamento->no_arquivo_comprovante != '')
                            <div class="btn-group" id="comprovante">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#arquivo-comprovante"
                                   data-idpagamento="{{$alienacao_pagamento->id_alienacao_pagamento}}"
                                   data-noarquivo="{{$alienacao_pagamento->no_arquivo_comprovante}}"
                                   data-noextensao="{{$alienacao_pagamento->no_extensao_comprovante}}">
                                    {{$alienacao_pagamento->no_arquivo_comprovante}}
                                </a>
                                <button type="button" class="remover-comprovante btn btn-danger " data-linha="0" data-token="{{$alienacao_pagamento_token}}" data-idpagamento="{{$alienacao_pagamento->id_alienacao_pagamento}}"><i class="fa fa-times"></i></button>
                            </div>
                            @endif
                    </div>
                    <button type="button" id="adicionar-comprovante" class="idPagamento btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="1" data-token="{{$alienacao_pagamento_token}}" data-idpagamento="{{$alienacao_pagamento->id_alienacao_pagamento}}" data-limite="0" style="display: {{$alienacao_pagamento->no_arquivo_comprovante!=''?'none':'block'}}">Adicionar arquivo</button>
                </form>
            </div>
        </fieldset>
    </div>
@endif