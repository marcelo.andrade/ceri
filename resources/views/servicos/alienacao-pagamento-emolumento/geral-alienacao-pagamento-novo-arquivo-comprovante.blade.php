<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<input type="hidden" name="token" id="token" value="{{$request->token}}" />
<input type="hidden" name="id_alienacao_pagamento" id="id_alienacao_pagamento" value=""  />
<div class="fieldset-group">
    <div class="progresso-upload progress" style="display:none">
        <div class="progress-bar" style="width: 0%;">0%</div>
    </div>
    <label class="arquivo-upload" for="arquivo-upload">
        <span>
            <figure>
                <i class="fa fa-upload"></i>
            </figure>
            <h4>Selecionar arquivo</h4>
        </span>
        <input type="file" name="no_arquivo" id="arquivo-upload" data-idpagamento="{{$request->id_alienacao_pagamento}}">
    </label>
</div>
<div class="msg-arquivo fieldset-group" style="display:none">
    <div class="alert alert-warning single">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem"></div>
    </div>
</div>