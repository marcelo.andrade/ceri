<div class="panel table-rounded">
    <table id="pedidos-recebidos" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="15%">Protocolo</th>
            <th width="15%">Data do pagamento</th>
            <th width="15%">Valor pago</th>
            <th width="10%">Quantidade</th>
            <th width="15%">Lote</th>
            <th width="17%">Usuário</th>
            <th width="13%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if (count($todos_pagamentos_recebidos)>0)
            @foreach ($todos_pagamentos_recebidos as $pagamento)
                <tr>
                    <td>{{ $pagamento->protocolo_pedido }}</td>
                    <td>{{ formatar_data($pagamento->dt_repasse_lote) }}</td>
                    <td><span class="real">{{$pagamento->va_repasse_lote}}</span></td>
                    <td>{{ $pagamento->nu_quantidade_lote}}</td>
                    <td>{{str_pad($pagamento->numero_lote, 5, "0", STR_PAD_LEFT)}}</td>
                    <td>{{$pagamento->no_usuario}}</td>

                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhe-historico-pagamento-caixa" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idalienacaovalorrepasselote="{{$pagamento->id_alienacao_valor_repasse_lote}}">Detalhes</button>
                            {{-- inserido menu dropdown--}}
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#imprimir-recibo-pagamento" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idalienacaovalorrepasselote="{{$pagamento->id_alienacao_valor_repasse_lote}}">Imprimir recibo</a></li>
                                </ul>
                            </div>
                            {{-- fim menu dorpdown --}}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum pagamento pendente foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todos_pagamentos_recebidos)}}</b> de <b>{{$total_pagamentos_recebidos}}</b> {{(count($todos_pagamentos_recebidos)>1?'pagamentos':'pagamento')}}.
</div>
<div align="center">
    {{$todos_pagamentos_recebidos->fragment('pedidos-recebidos')->render()}}
</div>