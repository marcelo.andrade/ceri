<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Notificações de alienação fidunciárias</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <div class="panel-heading gradient01">
                        <div class="checkbox">
                            <input type="checkbox" name="selecionar_tudo" id="selecionar_tudo" value="S" @if (count($alienacoes)<=0) disabled @endif>
                            <label for="selecionar_tudo" class="small">
                                Selecionar todas
                            </label>
                        </div>
                    </div>
                    <table id="notificacoes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="3%"></th>
                            <th width="10%">Protocolo / Data</th>
                            <th width="15%">Serventia</th>
                            <th width="10%">Contrato</th>
                            <th width="10%">Devedores</th>
                            <th width="20%">Fase / Etapa / Ação</th>
                            <th width="10%">Custo</th>
                            <th width="30%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serventias = array();
                        ?>
                        @if (count($alienacoes)>0)
                            @foreach ($alienacoes as $alienacao)
                                <?php
                                if (!in_array($alienacao->id_serventia,$serventias)) {
                                    $serventias[$alienacao->id_serventia]['no_serventia']       = $alienacao->no_serventia;
                                    $serventias[$alienacao->id_serventia]['no_cidade']          = $alienacao->no_cidade;
                                    $serventias[$alienacao->id_serventia]['codigo_banco']       = $alienacao->codigo_banco;
                                    $serventias[$alienacao->id_serventia]['no_banco']           = $alienacao->no_banco;
                                    $serventias[$alienacao->id_serventia]['nu_agencia']	        = $alienacao->nu_agencia;
                                    $serventias[$alienacao->id_serventia]['nu_dv_agencia']	    = $alienacao->nu_dv_agencia;
                                    $serventias[$alienacao->id_serventia]['nu_conta']		    = $alienacao->nu_conta;
                                    $serventias[$alienacao->id_serventia]['nu_dv_conta']	    = $alienacao->nu_dv_conta;
                                    $serventias[$alienacao->id_serventia]['tipo_conta']	        = $alienacao->tipo_conta;
                                    $serventias[$alienacao->id_serventia]['nu_variacao']	    = $alienacao->nu_variacao;
                                    $serventias[$alienacao->id_serventia]['nu_cpf_cnpj_conta']	= $alienacao->nu_cpf_cnpj_conta;
                                }
                                ?>
                                <tr id="{{$alienacao->id_alienacao}}">
                                    <td>
                                        <div class="checkbox">
                                            <input type="checkbox" name="id_alienacao[{{$alienacao->id_serventia}}][]" class="id_alienacao" id="id_alienacao_{{$alienacao->id_alienacao}}" data-idserventia="{{$alienacao->id_serventia}}" value="{{$alienacao->id_alienacao}}" >
                                            {{--<input type="checkbox" name="id_alienacao[{{$alienacao->id_serventia}}][{{$alienacao->id_alienacao_valor}}][{{$alienacao->va_total}}][]" class="id_alienacao" id="id_alienacao_{{$alienacao->id_alienacao}}" data-idserventia="{{$alienacao->id_serventia}}" value="{{$alienacao->id_alienacao}}" >--}}
                                            <label for="id_alienacao_{{$alienacao->id_alienacao}}" class="small"></label>
                                            <input type="hidden" name="id_alienacao_valor[{{$alienacao->id_serventia}}][]" class="id_alienacao_valor" id="id_alienacao_valor_{{$alienacao->id_alienacao}}" data-idserventia="{{$alienacao->id_serventia}}" value="{{$alienacao->id_alienacao_valor}}">
                                        </div>
                                    </td>
                                    <td>{{$alienacao->protocolo_pedido}} <br>/  {{ formatar_data($alienacao->dt_pedido)}}</td>
                                    <td>{{$alienacao->no_serventia}}</td>
                                    <td>{{$alienacao->numero_contrato}}<br/></td>
                                    <td>
                                        {{$alienacao->no_devedores}}
                                        {{--@if(count($alienacao->alienacao_devedor)>0)
                                            @foreach($alienacao->alienacao_devedor as $devedor)
                                               --}}{{-- <span class="label label-primary">{{$devedor->alienacao_devedor->no_devedor}} ({{$devedor->alienacao_devedor->nu_cpf_cnpj}})</span><br/>--}}{{--
                                               <span class="label label-primary">{{$devedor->alienacao_devedor->no_devedor}}</span><br/>
                                            @endforeach
                                        @endif--}}
                                    </td>
                                    <td>
                                        {{$alienacao->no_fase}} /<br />
                                        {{$alienacao->no_etapa}} /<br />
                                        {{$alienacao->no_acao}}
                                    </td>
                                    {{--<td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>--}}
                                    <td>
                                        <span class="real">{{$alienacao->va_total}}</span>
                                        {{--<input type="hidden" id="valor_pedido_{{$alienacao->id_alienacao}}" value="{{$alienacao->valor_nao_pago()}}" />--}}
                                        <input type="hidden" id="valor_pedido_{{$alienacao->id_alienacao}}" value="{{$alienacao->va_total}}" />
                                    </td>
                                    <td class="options">
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->protocolo_pedido}}">Detalhes</button>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" data-toggle="modal" data-target="#detalhes-custas" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->protocolo_pedido}}">Detalhes das custas</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <div class="single alert alert-danger">
                                        <i class="glyphicon glyphicon-remove"></i>
                                        <div class="mensagem">
                                            Nenhuma notificação foi encontrada.
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        /*
        <div class="form-group clearfix totais text-right">
            <div class="col-md-12">
                <label class="quantidade label label-default"><b>Quantidade selecionada:</b> <span>0</span></label>&nbsp;<label class="valor-total label label-primary"><b>Valor total:</b> <span class="real">0</span></label>
            </div>
        </div>
        */
        ?>
        <div class="form-group clearfix">
            <div class="col-md-12">
                @if (count($serventias)>0)
                    <div id="serventias-totais">
                        @foreach ($serventias as $key => $serventia)
                            <div id="serventia_{{$key}}" class="well well-sm gradient01 clearfix">
                                <span class="pull-left nome col-md-8">
                                    {{$serventia['no_serventia']}} - ( {{$serventia['no_cidade']}} ) <br />
                                    @if($serventia['codigo_banco'] != "")
                                    <label class="label label-default">Código Banco:</label> {{$serventia['codigo_banco']}}
                                    <label class="label label-default">Nome do Banco:</label> {{$serventia['no_banco']}}
                                    <label class="label label-default">Agência/DV:</label> {{$serventia['nu_agencia']}}/{{$serventia['nu_dv_agencia']}}
                                    <label class="label label-default">Conta/DV:</label> {{$serventia['nu_conta']}}/{{$serventia['nu_dv_conta']}}
                                    <label class="label label-default">Tipo de conta:</label> @if ( $serventia['tipo_conta'] == 'C' ) Corrente @else 'Poupança' @endif
                                    <label class="label label-default">CPF/CNPJ:</label> {{cpf_cnpj($serventia['nu_cpf_cnpj_conta'])}}
                                    @endif
                                </span>
                                <div class="pull-right totais">
                                    <label class="quantidade label label-default"><b>Quantidade selecionada:</b> <span>0</span></label>&nbsp;<label class="valor-total label label-primary"><b>Valor total:</b> <span class="real">0</span></label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                    <div id="serventias-total-geral">
                            <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                                <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                                <div class="pull-right totais">
                                    <label class="quantidade label label-default"><b>Quantidade selecionada:</b>
                                        <span>0</span>
                                    </label>&nbsp;
                                    <label class="valor-total label label-primary"><b>Valor total:</b>
                                        <span class="real">0</span>
                                    </label>
                                </div>
                            </div>
                    </div>
            </div>
        </div>
    </fieldset>
</div>