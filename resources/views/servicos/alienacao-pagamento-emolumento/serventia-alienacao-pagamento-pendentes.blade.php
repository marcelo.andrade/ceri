<div class="panel table-rounded">
    <table id="pedidos-pendentes" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
	            <th width="15%">Data de cadastro</th>
                <th width="15%">Data do pagamento</th>
                <th width="10%">Quantidade</th>
                <th width="15%">Valor total</th>
                <th width="15%">Valor pago</th>
                <th width="17%">Situação</th>
                <th width="13%">Ações</th>                    
            </tr>
        </thead>
        <tbody>
        	@if (count($pagamentos_pendentes)>0)
                @foreach ($pagamentos_pendentes as $pagamento)
                    <tr @if(count($pagamento->alienacoes[0]->alienacao_pedido->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
                        <td>
                            {{\Carbon\Carbon::parse($pagamento->dt_cadastro)->format('d/m/Y H:i')}}
                            @if(count($pagamento->alienacoes[0]->alienacao_pedido->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                        </td>
                        <td>{{($pagamento->dt_pagamento?\Carbon\Carbon::parse($pagamento->dt_pagamento)->format('d/m/Y H:i'):'-')}}</td>
                        <td>{{count($pagamento->alienacoes)}}</td>
                        <td><span class="real">{{$pagamento->va_pagamento}}</span></td>
                        <td>{!!($pagamento->va_pago?'<span class="real">'.$pagamento->va_pago.'</span>':'-')!!}</td>
                        <td>{{$pagamento->situacao->no_situacao_alienacao_pagamento}}</td>
                   		<td class="options">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-pagamento" data-idpagamento="{{$pagamento->id_alienacao_pagamento}}">Detalhes</button>
                                <?php
								if ($pagamento->id_situacao_alienacao_pagamento==$this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF) {
									echo '<div class="btn-group" role="group">';
										echo '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
										echo '<span class="caret"></span>';
									echo '</button>';
									echo '<ul class="dropdown-menu">';
										echo '<li><a href="#" class="aprovar-pagamento" data-idpagamento="'.$pagamento->id_alienacao_pagamento.'">Aprovar pagamento</a></li>';
										//echo '<li><a href="#" class="reprovar-pagamento" data-idpagamento="'.$pagamento->id_alienacao_pagamento.'">Reprovar pagamento</a></li>';
									echo '</ul>';
								}
								?>
                                </div>
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhum pagamento pendente foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>