<?php $vl_total = 0; ?>
<div class="panel table-rounded">
    <table id="pedidos-receber" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="10%">Protocolo / Data</th>
            <th width="20%">Serventia</th>
            <th width="10%">Contrato</th>
            <th width="10%">Devedores</th>
            <th width="15%">Fase / Etapa / Ação</th>
            <th width="10%">Custo</th>
            <th width="11%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if (count($pagamentos_receber)>0)
            @foreach($pagamentos_receber as $pagamento)
                <tr>
                    <td>{{$pagamento->protocolo_pedido}} <br>/ {{ formatar_data($pagamento->dt_pedido)}}</td>
                    <td>{{$pagamento->no_serventia}}</td>
                    <td>{{$pagamento->numero_contrato}}<br/></td>
                    <td>
                        {{$pagamento->no_devedores}}
                    </td>
                    <td>
                        {{$pagamento->no_fase}} /<br/>
                        {{$pagamento->no_etapa}} /<br/>
                        {{$pagamento->no_acao}}
                    </td>
                    <td>
                        <span class="real">{{$pagamento->va_total}}</span>
                        <input type="hidden" id="valor_pedido{{$pagamento->id_alienacao}}" value="{{$pagamento->va_total}}"/>
                    </td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$pagamento->id_alienacao}}" data-protocolo="{{$pagamento->alienacao_pedido->pedido->protocolo_pedido}}">Detalhes</button>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#detalhes-custas" data-idalienacao="{{$pagamento->id_alienacao}}" data-protocolo="{{$pagamento->alienacao_pedido->pedido->protocolo_pedido}}">Detalhes das custas</a></li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php $vl_total += $pagamento->va_total; ?>
            @endforeach
        @endif
        </tbody>
    </table>
    <div class="form-group clearfix">
        <div class="col-md-12">
            <div id="serventias-total-geral">
                <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                    <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                    <div class="pull-right totais">
                        <label class="valor-total label label-primary"><b>Valor total:</b>
                            <span class="real">{{$vl_total}}</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>