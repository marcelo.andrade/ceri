<div class="panel table-rounded">
    <table id="pedidos-efetuados" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th width="10%">Data de cadastro</th>
                <th width="10%">Data do pagamento</th>
                <th width="10%">Cidade</th>
                <th width="29%">Cartórios</th>
                <th width="1%">Quantidade</th>
                <th width="9%">Valor total</th>
                <th width="9%">Valor pago</th>
                <th width="10%">Situação</th>
                <th width="12%">Ações</th>
            </tr>
        </thead>
        <tbody>
        	@if (count($todos_pagamentos)>0)
                @foreach ($todos_pagamentos as $pagamento)
                    <?php
                    $no_cidade = ''; $no_cartorio = '';
                    ?>
                    <tr>
                        <td>{{\Carbon\Carbon::parse($pagamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                        <td>{{($pagamento->dt_pagamento?\Carbon\Carbon::parse($pagamento->dt_pagamento)->format('d/m/Y'):'-')}}</td>
                        <td>
                            @if (count($pagamento->alienacoes)>0)
                                @foreach ($pagamento->alienacoes as $alienacao )
                                    @if($no_cidade != $alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade)
                                        {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}
                                    @endif
                                    <?php $no_cidade = $alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade; ?>
                                @endforeach
                            @else
                                -
                            @endif
                        </td><td>
                            @if (count($pagamento->alienacoes)>0)
                                @foreach ($pagamento->alienacoes as $alienacao )
                                    @if($no_cartorio != $alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa)
                                        {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}
                                    @endif
                                    <?php $no_cartorio = $alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa; ?>
                                @endforeach
                            @else
                                -
                            @endif
                        </td>
                        <td>{{count($pagamento->alienacoes)}}</td>
                        <td><span class="real">{{$pagamento->va_pagamento}}</span></td>
                        <td>{!!($pagamento->va_pago?'<span class="real">'.$pagamento->va_pago.'</span>':'-')!!}</td>
                        <td>{{$pagamento->situacao->no_situacao_alienacao_pagamento}}</td>
                   		<td class="options">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-pagamento" data-idpagamento="{{$pagamento->id_alienacao_pagamento}}">Detalhes</button>
							</div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhum pagamento foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todos_pagamentos)}}</b> de <b>{{$total_pagamentos}}</b> {{(count($todos_pagamentos)>1?'pagamentos':'pagamento')}}.
</div>
<div align="center">
     {{$todos_pagamentos->fragment('pagamentos')->render()}}
</div>