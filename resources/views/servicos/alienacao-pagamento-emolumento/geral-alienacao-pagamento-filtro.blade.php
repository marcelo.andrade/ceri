<fieldset class="clearfix">
    <legend>Filtro</legend>
    @if ($class->id_tipo_pessoa == $class::ID_TIPO_PESSOA_ANOREG )
        <div class="fieldset-group clearfix">
            <div class="col-md-12">
                <fieldset>
                    <legend>Serventia</legend>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Cidade</legend>
                            <div class="col-md-12">
                                <select name="cidade_pagamentos" class="form-control pull-left">
                                    <option value="0">Selecione uma cidade</option>
                                    @if(count($cidades)>0)
                                        @foreach ($cidades as $cidade)
                                            <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Serventia</legend>
                            <div class="col-md-12">
                                <select name="serventia_pagamentos" class="form-control pull-left" disabled="">
                                    <option value="0">Todas as serventias</option>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                </fieldset>
            </div>
        </div>
    @endif
    <div class="fieldset-group clearfix">
    @if($class->id_tipo_pessoa == $class::ID_TIPO_PESSOA_CAIXA)
    <div class="col-md-4">
        <fieldset>
            <legend>Protocolo</legend>
            <div class="col-md-12">
                <div class="protocolo">
                    <input type="text" name="protocolo" class="form-control" value="{{$request->protocolo}}"/>
                </div>
            </div>
        </fieldset>
    </div>
    @endif
        <div class="fieldset-group clearfix">
            <div class="col-md-6">
                <fieldset>
                    <legend>Data de pagamento</legend>
                    <div class="col-md-12">
                        <div class="periodo input-group input-daterange">
                            <input type="text" class="form-control pull-left" name="dt_ini_pagamentos" rel="periodo"
                                   value="{{$request->dt_ini_pagamentos}}"/>
                            <span class="input-group-addon small pull-left">até</span>
                            <input type="text" class="form-control pull-left" name="dt_fim_pagamentos" rel="periodo"
                                   value="{{$request->dt_fim_pagamentos}}"/>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="buttons col-md-12 text-right">
            <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar filtros"/>
            <input type="submit" class="btn btn-success" value="{{$class->id_tipo_pessoa == $class::ID_TIPO_PESSOA_ANOREG ?'Filtrar notificações':'Filtrar Pagamentos'}}"/>
        </div>
    </div>
</fieldset>