<div class="panel table-rounded">
    <table id="pedidos-pendentes" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th width="15%">Data de cadastro</th>
                <th width="15%">Data do pagamento</th>
                <th width="10%">Quantidade</th>
                <th width="15%">Valor total</th>
                <th width="15%">Valor pago</th>
                <th width="20%">Situação</th>
                <th width="15%">Ações</th>
            </tr>
        </thead>
        <tbody>
        	@if (count($pagamentos)>0)
                @foreach ($pagamentos as $pagamento)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($pagamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                        <td>{{($pagamento->dt_pagamento?\Carbon\Carbon::parse($pagamento->dt_pagamento)->format('d/m/Y'):'-')}}</td>
                        <td>{{count($pagamento->alienacoes)}}</td>
                        <td><span class="real">{{$pagamento->va_pagamento}}</span></td>
                        <td>{!!($pagamento->va_pago?'<span class="real">'.$pagamento->va_pago.'</span>':'-')!!}</td>
                        <td>{{$pagamento->situacao->no_situacao_alienacao_pagamento}}</td>
                   		<td class="options">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-pagamento" data-idpagamento="{{$pagamento->id_alienacao_pagamento}}">Detalhes</button>
							</div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhum pagamento foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>