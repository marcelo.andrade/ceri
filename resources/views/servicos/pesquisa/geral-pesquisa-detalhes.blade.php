@if(in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Remetente</legend>
            <div class="col-md-6">
                <label class="small">Cidade</label>
                <select name="id_cidade_rem" class="form-control pull-left" disabled>
                    <option>
                    	@if(count($pedido->pessoa_origem->enderecos))
                    		{{$pedido->pessoa_origem->enderecos[0]->cidade->no_cidade}}
                    	@else
                    		Não informado
                    	@endif
                   	</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label class="small">Pessoa</label>
                <div class="">
                    <select name="id_pessoa_rem" class="form-control pull-left" disabled>
                        <option>{{$pedido->pessoa_origem->no_pessoa}}</option>
                    </select>
                </div>
            </div>
        </fieldset>
    </div>
@endif
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Destinatário</legend>
        <div class="col-md-4">
            <select name="id_cidade" class="form-control pull-left" disabled>
                <option>{{$pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</option>
            </select>
        </div>
        <div class="col-md-8">
            <select name="id_serventia" class="form-control pull-left" disabled>
                <option>{{$pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</option>
            </select>
        </div>
    </fieldset>
</div>
<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <fieldset class="fieldset-group">
            <legend>Pesquisa</legend>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    @if(count($tipos_chave)>0)
                        <div class="form-group">
                            @foreach ($tipos_chave as $tipo_chave)
                                <div class="radio radio-inline">
                                    <input type="radio" name="id_chave_pesquisa_pesquisa" id="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" value="{{$tipo_chave->id_chave_pesquisa_pesquisa}}" disabled @if ($pedido->pesquisa->id_chave_pesquisa_pesquisa==$tipo_chave->id_chave_pesquisa_pesquisa) checked @endif>
                                    <label for="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" class="small">
                                        {{$tipo_chave->no_chave_pesquisa_pesquisa}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="small">
                            Chave de pesquisa 
                            <span>
                                <?php
                                switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa) {
                                    case '1':
                                        echo '(Matrícula)';
                                        break;
                                    case '2':
                                        echo '(CPF)';
                                        break;
                                    case '3':
                                        echo '(CNPJ)';
                                        break;
                                    case '4':
                                        echo '(Nome)';
                                        break;
                                }
                                ?>
                            </span>
                        </label>
                        <input type="text" name="de_chave_pesquisa" class="form-control" value="{{$pedido->pesquisa->de_chave_pesquisa}}" disabled="disabled" />
                    </div>
                    <?php
                    switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa) {
                        case '2':
                            echo '<div class="form-group">';
                                echo '<label class="small">Titular do CPF</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->pesquisa->de_chave_complementar.'" disabled="disabled" />';
                            echo '</div>';
                            break;
                        case '3':
                            echo '<div class="form-group">';
                                echo '<label class="small">Razão social do CNPJ</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->pesquisa->de_chave_complementar.'" disabled="disabled" />';
                            echo '</div>';
                            break;
                    }
                    ?>
                    <div class="form-group">
                        <label class="small">Observação<span></span></label>
                        <textarea name="ds_observacao" id="ds_observacao" class="form-control" readonly>{{$pedido->pesquisa->ds_observacao}}</textarea>
                    </div>
                </div>
            </div>
        </fieldset>
        @if(count($tipos)>0)
            <fieldset class="fieldset-group">
                <legend>Tipo</legend>
                <div class="col-md-12">
                    @foreach ($tipos as $tipo)
                        <div class="radio">
                            <input type="radio" name="id_tipo_pesquisa" id="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" value="{{$tipo->id_tipo_pesquisa}}" disabled @if ($pedido->pesquisa->id_tipo_pesquisa==$tipo->id_tipo_pesquisa) checked @endif>
                            <label for="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" class="small">
                                {{$tipo->no_tipo_pesquisa}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </fieldset>
        @endif
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" disabled @if($pedido->pesquisa->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif>
                                <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            @if($pedido->pesquisa->id_processo>0)
                <div id="numero-processo" class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Número do processo</legend>
                        <div class="col-md-12">
                            <input type="text" name="numero_processo" class="form-control" value="{{$pedido->pesquisa->processo->numero_processo}}" disabled />
                        </div>
                    </fieldset>
                </div>
            @endif
            @if(count($arquivos_isencao)>0)
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Documento de isenção de emolumentos</legend>
                        <div class="col-md-12">
                            @foreach($arquivos_isencao as $arquivo)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            @endif
        @endif
    </div>
</div>
@if(in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group">
        <fieldset>
            <legend>Histórico da pesquisa</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="historico-pedido" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pedido->historico as $historico)
                            <tr>
                                <td>{{$historico->usuario_cad->no_usuario}}<br/>({{$historico->usuario_pessoa[0]->pessoa->no_pessoa}})</td>
                                <td>{{Carbon\Carbon::parse($historico->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>{{$historico->de_observacao}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </div>
@endif