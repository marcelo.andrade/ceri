<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <fieldset class="fieldset-group">
            <legend>Pesquisa</legend>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    @if(count($tipos_chave)>0)
                        <div class="form-group">
                            @foreach ($tipos_chave as $tipo_chave)
                                <div class="radio radio-inline">
                                    <input type="radio" name="id_chave_pesquisa_pesquisa" id="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" value="{{$tipo_chave->id_chave_pesquisa_pesquisa}}" disabled @if ($pedido->pesquisa->id_chave_pesquisa_pesquisa==$tipo_chave->id_chave_pesquisa_pesquisa) checked @endif>
                                    <label for="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" class="small">
                                        {{$tipo_chave->no_chave_pesquisa_pesquisa}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="small">
                            Chave de pesquisa 
                            <span>
                                <?php
                                $chave_pesquisa= 'a chave';
                                switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa) {
                                    case '1':
                                        echo '(Matrícula)';
                                        break;
                                    case '2':
                                        $chave_pesquisa='o CPF';
                                        echo '(CPF)';
                                        break;
                                    case '3':
                                        $chave_pesquisa='o CNPJ';
                                        echo '(CNPJ)';
                                        break;
                                    case '4':
                                        $chave_pesquisa='o nome';
                                        echo '(Nome)';
                                        break;
                                }
                                ?>
                            </span>
                        </label>
                        <input type="text" name="de_chave_pesquisa" class="form-control" value="{{$pedido->pesquisa->de_chave_pesquisa}}" disabled="disabled" />
                    </div>
                    <?php
                    switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa) {
                        case '2':
                            echo '<div class="form-group">';
                                echo '<label class="small">Titular do CPF</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->pesquisa->de_chave_complementar.'" disabled="disabled" />';
                            echo '</div>';
                            break;
                        case '3':
                            echo '<div class="form-group">';
                                echo '<label class="small">Razão social do CNPJ</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->pesquisa->de_chave_complementar.'" disabled="disabled" />';
                            echo '</div>';
                            break;
                    }
                    ?>
                </div>
            </div>
        </fieldset>
        @if(count($tipos)>0)
            <fieldset class="fieldset-group">
                <legend>Tipo</legend>
                <div class="col-md-12">
                    @foreach ($tipos as $tipo)
                        <div class="radio">
                            <input type="radio" name="id_tipo_pesquisa" id="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" value="{{$tipo->id_tipo_pesquisa}}" disabled @if ($pedido->pesquisa->id_tipo_pesquisa==$tipo->id_tipo_pesquisa) checked @endif>
                            <label for="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" class="small">
                                {{$tipo->no_tipo_pesquisa}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </fieldset>
        @endif
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" disabled @if($pedido->pesquisa->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif>
                                <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            @if($pedido->pesquisa->id_tipo_custa==1)
                <div id="numero-processo" class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Número do processo</legend>
                        <div class="col-md-12">
                            <input type="text" name="numero_processo" class="form-control" value="{{$pedido->pesquisa->processo->numero_processo}}" disabled />
                        </div>
                    </fieldset>
                </div>
            @endif
            @if($pedido->pesquisa->id_tipo_custa==3)
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Documento de isenção de emolumentos</legend>
                        <div class="col-md-12">
                            @if(count($arquivos_isencao)>0)
                                @foreach($arquivos_isencao as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                    </div>
                                @endforeach
                            @else
                                <div class="alert alert-warning single">
                                    <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                    <div class="menssagem">
                                        Nenhum arquivo foi inserido.
                                    </div>
                                </div>
                            @endif
                        </div>
                    </fieldset>
                </div>
            @endif
        @endif
    </div>
</div>
@if (count($arquivos_requerimento)>0)
    <div class="resultado fieldset-group clearfix">
        <fieldset class="col-md-12">
            <legend>Requerimento</legend>
            @foreach($arquivos_requerimento as $arquivo)
                <div id="arquivos-resultado" class="col-md-12">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                </div>
            @endforeach
        </fieldset>
    </div>
@endif
<div class="fieldset-group clearfix">
    @if($pedido->pedido_resultado->in_positivo=='S')
        <div class="alert alert-success single">
            <i class="icon glyphicon glyphicon-ok pull-left"></i>
            <div class="menssagem">
            	@if(count($arquivos_resultado)>0)
                    @if(count($arquivos_resultado)>1)
                	   {{count($arquivos_resultado)}} arquivos foram enviados como resultado para esta pesquisa eletrônica. <br /><br />
                    @else
                        1 arquivo foi enviado como resultado para esta pesquisa eletrônica. <br /><br />
                    @endif
                    @foreach($arquivos_resultado as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                        </div>
                    @endforeach
                @else
                    <?php
                    switch ($pedido->pesquisa->id_tipo_pesquisa) {
                        case '1':
                            echo 'Foram localizadas e abaixo relacionadas as seguintes matrículas de imóveis de atual propriedade do pesquisado nesta serventia. Maiores informações poderão ser obtidas via certidão de inteiro teor emitida pela central.';
                            break;
                        case '3':
                            echo 'Foram localizadas e abaixo relacionadas as seguintes matrículas com registros (atuais e passados) em nome do pesquisado nesta serventia. Maiores informações poderão ser obtidas via certidão de inteiro teor emitida pela central.';
                            break;
                    }
                    ?>	            	
				@endif
			</div>
        </div>
	@else
        <div class="alert alert-danger single">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem">Não foram localizados registros com {{$chave_pesquisa}} <label class="label label-danger label-sm">{{$pedido->pesquisa->de_chave_pesquisa}}</label> pesquisada nesta serventia.</div>
        </div>
    @endif
</div>
@if($pedido->pedido_resultado->in_positivo=='S')
    @if(count($pedido->pedido_resultado->matriculas)>0)
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Matrículas</legend>
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table id="historico-observacoes" class="table table-striped table-bordered small">
                            <thead>
                                <tr class="gradient01">
                                    <th width="10%">Matrícula</th>
                                    <th width="10%">Transcrição</th>
                                    <th width="80%">Proprietários</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pedido->pedido_resultado->matriculas as $matricula)
                                    <tr>
                                        <td>
                                            @if($matricula->id_matricula>0 && $matricula->in_transcricao == 'N')
                                                {{$matricula->matricula->codigo_matricula}}
                                            @elseif($matricula->in_transcricao == 'N')
                                                {{$matricula->codigo_matricula}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($matricula->id_matricula>0 && $matricula->in_transcricao == 'S')
                                                {{$matricula->matricula->codigo_matricula}}
                                            @elseif($matricula->in_transcricao == 'S')
                                                {{$matricula->codigo_matricula}}
                                            @endif
                                        </td>
                                        <td>
                                        	@if($matricula->id_matricula>0)
                                                @foreach($matricula->matricula->proprietarios as $proprietario)
                                                    <span class="label label-primary">{{$proprietario->no_proprietario}} ({{$proprietario->nu_cpf_cnpj}})</span>
                                                @endforeach
                                            @else
                                            	Não informado
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    @endif
@endif
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico da pesquisa</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pedido->historico as $historico)
                            <tr>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-solicitante" data-idusuario="{{$historico->usuario_cad->id_usuario}}" data-nousuario="{{$historico->usuario_cad->no_usuario}}">{{$historico->usuario_cad->no_usuario}}<br/>({{$historico->usuario_pessoa[0]->pessoa->no_pessoa}})</a>
                                </td>
                                <td>{{Carbon\Carbon::parse($historico->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>{{$historico->de_observacao}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>