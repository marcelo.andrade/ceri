<input type="hidden" name="va_pedido" value="0">
<input type="hidden" name="pesquisa_token" value="{{$pesquisa_token}}" />
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Inserir serventia</legend>
        <div class="col-md-3">
            <label class="small">Cidade</label>
            <select name="id_cidade" class="form-control pull-left">
                <option value="0">Selecione</option>
                @if(count($cidades)>0)
                	@foreach ($cidades as $cidade)
                    	<option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div id="id_serventia" class="form-group col-md-6">
            <label class="small">Cartório</label>
            <div class="">
                <select name="id_serventia" class="form-control pull-left" disabled>
                    <option value="0">Selecione</option>
                    @if(count($serventias)>0)
                        @foreach ($serventias as $serventia)
                            <option value="{{$serventia->id_serventia}}">{{$serventia->no_serventia}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <label>&nbsp;</label>
            <div class="btn-group btn-group-justified">
                <div class="btn-group">
                    <button class="incluir-serventia disabled btn btn-default btn-inline" type="button" disabled>Incluir</button>
                </div>
                <div class="btn-group">
                    <button class="incluir-todas-serventias btn btn-primary btn-inline" type="button">Incluir todas</button>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Cartórios selecionados</legend>
        <div class="fieldset-group clearfix">
            <div class="col-md-12 btn-list clearfix" id="serventias-selecionadas">
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <fieldset class="fieldset-group">
            <legend>Pesquisa</legend>
            <div class="col-md-12">
                @if(count($tipos_chave)>0)
                    <div class="form-group">
                        @foreach ($tipos_chave as $tipo_chave)
                            <div class="radio radio-inline">
                                <input type="radio" name="id_chave_pesquisa_pesquisa" id="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" value="{{$tipo_chave->id_chave_pesquisa_pesquisa}}" data-tipo="{{$tipo_chave->tp_chave_pesquisa_pesquisa}}">
                                <label for="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" class="small">
                                    {{$tipo_chave->no_chave_pesquisa_pesquisa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                @endif
                <div class="form-group">
                    <label class="small">Chave de pesquisa <span></span></label>
                    <input type="text" name="de_chave_pesquisa" class="form-control" disabled="disabled" />
                </div>
                <div id="chave-complementar" class="form-group" style="display:none">
                    <label class="small"></label>
                    <input type="text" name="de_chave_complementar" class="form-control somente-texto" maxlength="150" disabled="disabled" />
                </div>
                <div class="form-group">
                    <label class="small">Observação <span></span></label>
                    <textarea name="ds_observacao" id="ds_observacao" class="form-control" maxlength="250"></textarea>
                </div>
                <div class="form-group">
                    <div class="alert alert-info small single">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">Você digitou <span class="total">0</span> caractere(s) dos 250 possíveis. <span class="limite"></span></div>
                    </div>
                </div>
                <div id="msg-homonimia" class="alert alert-warning single" style="display: none"><b>Atenção:</b> Resultado não exclui casos de homonímia.</div>
            </div>
        </fieldset>
        @if(count($tipos)>0)
            <fieldset class="fieldset-group">
                <legend>Tipo</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        @foreach ($tipos as $tipo)
                            <div class="radio">
                                <input type="radio" name="id_tipo_pesquisa" id="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" value="{{$tipo->id_tipo_pesquisa}}">
                                <label for="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" class="small">
                                    {{$tipo->no_tipo_pesquisa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div id="msg-tipo" class="alert alert-info single" style="display: none"><b>Atenção:</b> <span></span></div>
                </div>
            </fieldset>
        @endif
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}">
                                <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            <div id="numero-processo" class="fieldset-group clearfix" style="display:none">
                <fieldset>
                    <legend>Número do processo</legend>
                    <div class="col-md-12">
                        <input type="text" name="numero_processo" class="form-control" maxlength="50" />
                    </div>
                </fieldset>
            </div>
            <div id="msg-isencao" class="fieldset-group clearfix" style="display:none">
                <div class="alert alert-danger single">
                    Para que o processamento da Pesquisa se dê com AJG será necessário anexar o despacho de deferimento ou escolha outra opção para dar continuidade à solicitação.
                </div>
            </div>
            <div id="documento-isencao" class="fieldset-group clearfix" style="display:none">
                <fieldset>
                    <legend>Documento de isenção de emolumentos</legend>
                    <div id="arquivos-isencao" class="col-md-12">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="18" data-token="{{$pesquisa_token}}" data-limite="1">Adicionar arquivo</button>
                    </div>
                </fieldset>
            </div>
        @endif
    </div>
</div>
<div class="fieldset-group row clearfix">
    <div class="col-md-6" id="pesquisa-valores" style="display:none">
        <fieldset>
            <legend>Composição do valor para a nova pesquisa (por serventia)</legend>
            <div class="tabela col-md-12">
                <table class="table table-striped table-fixed table-condensed single">
                    <thead>
                        <th width="60%">Descrição</th>
                        <th width="40%" class="text-right">Valor</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
</div>
<div id="msg-ciencia" class="fieldset-group clearfix" style="display:none">
    <div  class="alert alert-success single">
        <div class="checkbox">
            <input type="checkbox" name="in_ciencia_pesquisa" id="in_ciencia_pesquisa" value="1">
            <label for="in_ciencia_pesquisa" class="small text-justify"></label>
        </div>
    </div>
</div>