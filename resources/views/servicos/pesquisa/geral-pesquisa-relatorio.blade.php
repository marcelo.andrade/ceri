<form name="form-salvar-relatorio" action="{{URL::to('servicos/pesquisa-eletronica/relatorio/salvar')}}" method="post" target="_blank">
    {{csrf_field()}}
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Pesquisa eletrônica</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table class="table table-striped table-bordered small">
                            <thead>
                            <tr class="gradient01">
                                <th width="10%">Protocolo</th>
                                <th width="15%">Data do pedido</th>
                                <th width="15%">Item pesquisado</th>
                                <th width="35%">Solicitante</th>
                                <th width="15%">Status</th>
                                <th width="10%">Período de disponibilização</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($todas_pesquisas)>0)
                                @foreach($todas_pesquisas as $pesquisa)
                                    <tr>
                                        <td><input type="hidden" name="id_pesquisa[]" class="id_pesquisa" value="{{$pesquisa->pedido->id_pedido}}"/>{{$pesquisa->pedido->protocolo_pedido}}</td>
                                        <td>{{\Carbon\Carbon::parse($pesquisa->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                                        <td>{{$pesquisa->de_chave_pesquisa}}</td>
                                        <td>{{$pesquisa->pedido->usuario->no_usuario}} - {{$pesquisa->pedido->pessoa_origem->no_pessoa}}</td>
                                        <td>{{$pesquisa->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                                        <td>{{\Carbon\Carbon::parse($pesquisa->pedido->dt_pedido)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($pesquisa->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                                        @endforeach
                                        @endif
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</form>