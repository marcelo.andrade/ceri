<div class="fieldset-group clearfix">
    <form name="form-observacao" method="post" action="" class="clearfix" enctype="multipart/form-data">
        <input type="hidden" name="id_pesquisa" value="{{$pesquisa->id_pesquisa}}" />
        <input type="hidden" name="observacao_token" value="{{$observacao_token}}" />
        <fieldset class="col-md-12">
            <legend>Nova observação</legend>
            <div class="col-md-12">
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
            </div>
            <?php
            switch ($class->id_tipo_pessoa) {
                case 2:
                $no_pessoa_dest = $pesquisa->pedido->pessoa_origem->no_pessoa;
                break;
                default:
                    $no_pessoa_dest = $pesquisa->pedido->pedido_pessoa_atual->pessoa->no_pessoa;
                    break;
            }
            ?>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Para</label>
                    <input name="no_pessoa_dest" class="form-control" value="{{$no_pessoa_dest}}" disabled />
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Observação</label>
                    <textarea name="de_observacao" class="form-control"></textarea>
                </div>
            </div>
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Arquivos</legend>
                    <div id="arquivos-observacao" class="col-md-12">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="25" data-token="{{$observacao_token}}" data-limite="0">Adicionar arquivo</button>
                    </div>
                </fieldset>
            </div>
            <div id="assinaturas-arquivos" class="fieldset-group clearfix">
                <div class="col-md-12">
                    <div class="alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem clearfix">
                            <span>Nenhum arquivo foi inserido.</span><br />
                            <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$observacao_token}}">Assinar arquivo(s)</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="botoes form-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" data-token="{{$observacao_token}}" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar observação" />
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico de observações</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-observacoes" class="table table-bordered small">
                    <thead>
                    <tr class="gradient01">
                        <th width="35%">Usuário</th>
                        <th width="10%">Data</th>
                        <th width="30%">Observação</th>
                        <th width="22%">Arquivo</th>
                        <th width="3%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($pesquisa->observacoes)>0)
                        @foreach($pesquisa->observacoes as $observacao)
                            <tr {{($observacao->in_leitura == 'N' and $observacao->id_pessoa_dest == $pessoa_ativa->id_pessoa)?'class=alert-info':''}}>
                                <td>{{$observacao->pessoa->no_pessoa}} ({{$observacao->usuario_cad->no_usuario}})</td>
                                <td>{{Carbon\Carbon::parse($observacao->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td class="text-wrap">{{$observacao->de_observacao}}</td>
                                <td>
                                    @if(count($observacao->pesquisa_observacao_arquivo_grupo) > 0)
                                        @foreach($observacao->pesquisa_observacao_arquivo_grupo as $arquivo)
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn_observacao" data-toggle="modal" data-target="#visualizar-arquivo" title="{{$arquivo->no_arquivo}}" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                @if($arquivo->in_ass_digital=='S')
                                                    <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                @endif
                                            </div>
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    <div class="checkbox checkbox-primary">
                                        <input id="observacao-{{$observacao->id_pesquisa_observacao}}" class="styled" type="checkbox" value="{{$observacao->id_pesquisa_observacao}}" {{$observacao->in_leitura == 'S'?'checked':''}} {{$observacao->id_pessoa_dest != $pessoa_ativa->id_pessoa?'disabled':''}}/>
                                        <label for="observacao-{{$observacao->id_pesquisa_observacao}}"></label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
