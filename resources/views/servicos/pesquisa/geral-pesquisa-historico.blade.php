<?php
$query_string = http_build_query($request->except(['_token','ord']));
?>
@if($class->id_tipo_pessoa == 9)
    @if (count($todas_pesquisas)>0)
        <div id="alert" class="alert alert-info">
            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
            <div class="menssagem clearfix" style="overflow:initial;">
                <span class="total">0 pesquisas selecionadas</span>
                <div class="pull-right">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target="#relatorio-pesquisa" aria-haspopup="true" aria-expanded="false" disabled>
                            Gerar relatório das selecionadas
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                @if($class->id_tipo_pessoa == 9)
                    <th width="3%">
                        <div class="checkbox checkbox-primary">
                            <input id="selecionar-todas" class="styled" type="checkbox">
                            <label for="selecionar-todas"></label>
                        </div>
                    </th>
                @endif
                <th width="10%">Protocolo</th>
                        <th width="12%">
                            <?php
                                switch ($request->ord) {
                                    case 'asc':
                                        $ord = 'desc';
                                        $icone = 'fa fa-chevron-up';
                                        break;
                                    case 'desc': default:
                                    $ord = 'asc';
                                    $icone = 'fa fa-chevron-down';
                                    break;
                                }
                            ?>
                            <a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=data&ord={{$ord}}">
                                <span class="pull-left">Data do pedido</span>
                                <i class="pull-right {{$icone}}"></i>
                            </a>
                        </th>
                <th width="13%">Item pesquisado</th>
                <th width="25%">Solicitante</th>
                <th width="10%">Status</th>
                <th width="14%">Período de disponibilização</th>
                <th width="13%">Ações</th>
            </tr>
        </thead>
        <tbody>
        	@if (count($todas_pesquisas)>0)
                @foreach ($todas_pesquisas as $pesquisa)
                    <tr id="{{$pesquisa->pedido->id_pedido}}" @if(count($pesquisa->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
					@if($class->id_tipo_pessoa == 9)
                        <td>
                            <div class="checkbox checkbox-primary">
                                <input id="pesquisa-{{$pesquisa->pedido->id_pedido}}"  class="pesquisa" class="styled" type="checkbox" value="{{$pesquisa->pedido->id_pedido}}">
                                <label for="pesquisa-{{$pesquisa->pedido->id_pedido}}"></label>
                            </div>
                        </td>
                    @endif
                        <td>
                            {{$pesquisa->pedido->protocolo_pedido}}
                            @if(count($pesquisa->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                        </td>
                        <td>{{\Carbon\Carbon::parse($pesquisa->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                        <td>{{$pesquisa->de_chave_pesquisa}}</td>
                        <td>{{$pesquisa->pedido->usuario->no_usuario}} - {{$pesquisa->pedido->pessoa_origem->no_pessoa}}</td>
                		<td>{{$pesquisa->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    	<td>{{\Carbon\Carbon::parse($pesquisa->pedido->dt_pedido)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($pesquisa->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                   		<td class="options">
                            <div class="btn-group" role="group">
                                <?php
                                switch ($pesquisa->pedido->id_situacao_pedido_grupo_produto) {
                                    case $class::ID_SITUACAO_FINALIZADO:
                                        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#resultado-pesquisa" data-idpedido="'.$pesquisa->pedido->id_pedido.'" data-protocolo="'.$pesquisa->pedido->protocolo_pedido.'">Resultado</button>';
                                        break;
                                    default:
                                        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-pesquisa" data-idpedido="'.$pesquisa->pedido->id_pedido.'" data-protocolo="'.$pesquisa->pedido->protocolo_pedido.'">Detalhes</button>';
                                        break;
                                }
                                ?>
                                @if ($pesquisa->observacoes_nao_lidas()>0)
                                    <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idpesquisa="{{$pesquisa->id_pesquisa}}" data-protocolo="{{$pesquisa->pedido->protocolo_pedido}}" title="{{$pesquisa->observacoes_nao_lidas()}} {{($pesquisa->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                        {{$pesquisa->observacoes_nao_lidas()}}
                                    </a>
                                @endif
                                @if(in_array($class->id_tipo_pessoa,[3,4]))
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#recibo-pesquisa" data-idpedido="{{$pesquisa->pedido->id_pedido}}" data-protocolo="{{$pesquisa->pedido->protocolo_pedido}}">Visualizar recibo</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#observacoes" data-idpesquisa="{{$pesquisa->id_pesquisa}}" data-protocolo="{{$pesquisa->pedido->protocolo_pedido}}">Observações</a></li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhuma pesquisa foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todas_pesquisas)}}</b> de <b>{{$todas_pesquisas->total()}}</b> {{($todas_pesquisas->total()>1?'pesquisas':'pesquisa')}}.
</div>
<div align="center">
    {{$todas_pesquisas->fragment('pedidos')->render()}}
</div>