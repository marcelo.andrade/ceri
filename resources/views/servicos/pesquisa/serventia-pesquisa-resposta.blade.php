<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <fieldset class="fieldset-group">
            <legend>Pesquisa</legend>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    @if(count($tipos_chave)>0)
                        <div class="form-group">
                            @foreach ($tipos_chave as $tipo_chave)
                                <div class="radio radio-inline">
                                    <input type="radio" name="id_chave_pesquisa_pesquisa" id="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" value="{{$tipo_chave->id_chave_pesquisa_pesquisa}}" disabled @if ($pedido->pesquisa->id_chave_pesquisa_pesquisa==$tipo_chave->id_chave_pesquisa_pesquisa) checked @endif>
                                    <label for="id_chave_pesquisa_pesquisa{{$tipo_chave->id_chave_pesquisa_pesquisa}}" class="small">
                                        {{$tipo_chave->no_chave_pesquisa_pesquisa}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="small">
                            Chave de pesquisa 
                            <span>
                                <?php
                                $chave_pesquisa = '';
                                switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa) {
                                    case 1:
                                        $chave_pesquisa = 'a matrícula';
                                        break;
                                    case 2:
                                        $chave_pesquisa = 'o CPF';
                                        break;
                                    case 3:
                                        $chave_pesquisa = 'o CNPJ';
                                        break;
                                    case 4:
                                        $chave_pesquisa = 'o nome';
                                        break;
                                }
                                ?>
                            </span>
                        </label>
                        <input type="text" name="de_chave_pesquisa" class="form-control" value="{{$pedido->pesquisa->de_chave_pesquisa}}" disabled="disabled" />
                    </div>
                    <?php
                    switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa) {
                        case '2':
                            echo '<div class="form-group">';
                                echo '<label class="small">Titular do CPF</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->pesquisa->de_chave_complementar.'" disabled="disabled" />';
                            echo '</div>';
                            break;
                        case '3':
                            echo '<div class="form-group">';
                                echo '<label class="small">Razão social do CNPJ</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->pesquisa->de_chave_complementar.'" disabled="disabled" />';
                            echo '</div>';
                            break;
                    }
                    ?>
                        <div class="form-group">
                            <label class="small">Observação<span></span></label>
                            <textarea name="ds_observacao" id="ds_observacao" class="form-control" readonly>{{$pedido->pesquisa->ds_observacao}}</textarea>
                        </div>
                </div>

            </div>
        </fieldset>
        @if(count($tipos)>0)
            <fieldset class="fieldset-group">
                <legend>Tipo</legend>
                <div class="col-md-12">
                    @foreach ($tipos as $tipo)
                        <div class="radio">
                            <input type="radio" name="id_tipo_pesquisa" id="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" value="{{$tipo->id_tipo_pesquisa}}" disabled @if ($pedido->pesquisa->id_tipo_pesquisa==$tipo->id_tipo_pesquisa) checked @endif>
                            <label for="id_tipo_pesquisa{{$tipo->id_tipo_pesquisa}}" class="small">
                                {{$tipo->no_tipo_pesquisa}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </fieldset>
        @endif
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" disabled @if($pedido->pesquisa->id_tipo_custa==$tipo_custa->id_tipo_custa) checked @endif>
                                <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            @if($pedido->pesquisa->id_tipo_custa==1)
                <div id="numero-processo" class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Número do processo</legend>
                        <div class="col-md-12">
                            <input type="text" name="numero_processo" class="form-control" value="{{$pedido->pesquisa->processo->numero_processo}}" disabled />
                        </div>
                    </fieldset>
                </div>
            @endif
            @if($pedido->pesquisa->id_tipo_custa==3)
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Documento de isenção de emolumentos</legend>
                        <div class="col-md-12">
                            @if(count($arquivos_isencao)>0)
                                @foreach($arquivos_isencao as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                    </div>
                                @endforeach
                            @else
                                <div class="alert alert-warning single">
                                    <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                    <div class="menssagem">
                                        Nenhum arquivo foi inserido.
                                    </div>
                                </div>
                            @endif
                        </div>
                    </fieldset>
                </div>
            @endif
        @endif
    </div>
</div>
<div class="fieldset-group row clearfix">
    <div class="col-md-12">
        <div class="alert alert-warning single">
            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
            <div class="menssagem">
                <b>
                    <?php
                    switch ($pedido->pesquisa->id_tipo_pesquisa) {
                        case '1':
                            echo 'A pesquisa ora requerida deverá informar tão somente as matrículas dos imóveis de atual propriedade do pesquisado.';
                            break;
                        case '3':
                            echo 'A pesquisa ora requerida deverá informar todas as matriculas com registros do pesquisado (atuais e passados).';
                            break;
                    }
                    ?>
                </b>
            </div>
        </div>
    </div>
</div>
@if (count($arquivos_requerimento)>0)
    <div class="resultado fieldset-group clearfix">
        <fieldset class="col-md-12">
            <legend>Requerimento</legend>
            @foreach($arquivos_requerimento as $arquivo)
                <div id="arquivos-resultado" class="col-md-12">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                </div>
            @endforeach
        </fieldset>
    </div>
@endif
<div class="resultado fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Resultado da pesquisa</legend>
        <form name="form-novo-resultado" method="post" action="" class="clearfix" enctype="multipart/form-data">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
            <input type="hidden" name="pesquisa_token" value="{{$pesquisa_token}}" />
            <div class="erros-resultado alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-3">
                    <fieldset>
                        <legend>Tipo</legend>
                        <div class="col-md-12">
                            <div class="radio">
                                <input type="radio" name="in_positivo" id="in_positivo_S" value="S">
                                <label for="in_positivo_S" class="small">
                                    Positivo
                                </label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="in_positivo" id="in_positivo_N" value="N">
                                <label for="in_positivo_N" class="small">
                                    Negativo
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="resultado-mensagem col-md-9">
                    <fieldset>
                        <legend>Resultado</legend>
                        <div class="col-md-12">
                            <div class="alert alert-warning single">
                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                <div class="menssagem">Selecione um tipo de resultado</div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="resultado-positivo col-md-9" style="display:none">
                    <div class="fieldset-group">
                        <fieldset>
                            <legend>
                                <div class="radio">
                                    <input type="radio" name="tipo_resultado_envio" id="digitar" value="1">
                                    <label for="digitar">
                                       <b>Digitar matrículas e/ou transcrições</b>
                                    </label>
                                </div>
                            </legend>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="erros-matriculas alert alert-danger single" style="display:none">
                                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                                        <div class="menssagem"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small">Código da matrícula</label>
                                    <div class="input-group">
                                        <input type="text" name="codigo_matricula" class="form-control" maxlength="10" disabled />
                                        <div class="input-group-btn">
                                            <button type="button" class="incluir-matricula btn btn-default btn-inline disabled" disabled >
                                                Incluir
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small">Código da transcrição</label>
                                    <div class="input-group">
                                        <input type="text" name="codigo_transcricao" class="form-control" maxlength="10" disabled />
                                        <div class="input-group-btn">
                                            <button type="button" class="incluir-transcricao btn btn-default btn-inline disabled" disabled >
                                                Incluir
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="well" style="margin-bottom:0">
                                        <?php
                                        switch ($pedido->pesquisa->id_tipo_pesquisa){
                                            case '1':
                                                switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa){
                                                    case '2': case '3':
                                                        echo ' Foram localizadas e abaixo relacionadas as seguintes matrículas e/ou transcrições de atual propriedade em nome do pesquisado nesta serventia.<span id="matriculas"></span><span id="transcricoes"></span> Maiores informações poderão ser obtidas via certidão de inteiro teor emitida pela central.';
                                                        break;
                                                    case '4':
                                                        echo 'Foram localizados as seguintes matrículas e/ou transcrições de atual propriedade em nome de <span class="label label-danger"> '.$pedido->pesquisa->de_chave_pesquisa.'</span>, <span id="matriculas"></span><span id="transcricoes"></span> contudo sem vinculação a CPF (homônimo). Maiores informações deverão ser obtidas diretamente no cartório. ';
                                                        break;
                                                }
                                                break;
                                            case '3':
                                                switch ($pedido->pesquisa->id_chave_pesquisa_pesquisa){
                                                    case '2': case '3':
                                                        echo 'Foram localizadas e abaixo relacionadas as seguintes matrículas e/ou transcrições com registros (atuais e passados) em nome do pesquisado nesta serventia.<span id="matriculas"></span><span id="transcricoes"></span> Maiores informações poderão ser obtidas via certidão de inteiro teor emitida pela central.';
                                                        break;
                                                    case '4':
                                                        echo 'Foram localizados as seguintes matrículas e/ou transcrições com registros (atuais e passados) em nome de <span class= "label label-danger">'.$pedido->pesquisa->de_chave_pesquisa.'</span><span id="matriculas"></span><span id="transcricoes"></span>, contudo sem vinculação a CPF (homônimo). Maiores informações deverão ser obtidas diretamente no cartório.';
                                                        break;
                                                }
                                                break;
                                        }
                                        ?>
                                    </div>
                                </div>
							</div>
                        </fieldset>
                    </div>
                    <div class="fieldset-group">
                        <fieldset>
                            <legend>
                                <div class="radio">
                                    <input type="radio" name="tipo_resultado_envio" id="upload" value="2">
                                    <label for="upload">
                                       <b>Upload do resultado</b>
                                    </label>
                                </div>
                            </legend>
                            <div class="col-md-12">
                                <div id="arquivos-resultado">
                                    <button type="button" class="btn btn-success disabled" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="19" data-token="{{$pesquisa_token}}" data-limite="0" disabled>Adicionar arquivo</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="resultado-negativo col-md-9" style="display:none">
                    <fieldset>
                        <legend>Resultado</legend>
                        <div class="col-md-12">
                            <div class="well" style="margin-bottom:0">Não foram localizados registros com {{$chave_pesquisa}} e a chave de pesquisa <label class="label label-danger label-sm">{{$pedido->pesquisa->de_chave_pesquisa}}</label> pesquisada nesta serventia.</div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar resultado" />
                </div>
            </div>
        </form>
    </fieldset>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico da pesquisa</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($pedido->historico as $historico)
                            <tr>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-solicitante" data-idusuario="{{$historico->usuario_cad->id_usuario}}" data-nousuario="{{$historico->usuario_cad->no_usuario}}">{{$historico->usuario_cad->no_usuario}}<br/>({{$historico->usuario_pessoa[0]->pessoa->no_pessoa}})</a>
                                </td>
                                <td>{{Carbon\Carbon::parse($historico->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>{{$historico->de_observacao}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>