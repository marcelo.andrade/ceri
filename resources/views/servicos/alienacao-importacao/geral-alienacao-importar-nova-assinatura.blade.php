<?php
if ($request->session()->has('arquivos_'.$request->token)) {
	$arquivos = $request->session()->get('arquivos_'.$request->token);
	$arquivosIds = array();
	$arquivosNomes = array();
	foreach($arquivos as $key => $arquivo) {
		$arquivosIds[] = $key;
		$arquivosNomes[] = $arquivo['no_arquivo'];
	}
?>
	<script type="text/javascript" src="{{asset('lacuna/js/jquery.blockUI.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('lacuna/js/lacuna-web-pki-2.5.0.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('lacuna/js/batch-signature-form-importar-xml.js')}}?v=<?=time();?>"></script>
	<form id="signForm" method="POST">
		<div class="fieldset-group clearfix">
	    	<fieldset>
	            <legend>Arquivos</legend>
	            <div class="col-md-12">
					<div id="docList"></div>
				</div>
			</fieldset>
		</div>
		<div class="fieldset-group clearfix">
	    	<fieldset>
	            <legend>Certificados</legend>
	            <div class="col-md-12">
					<label for="certificateSelect">Selecione um certificado</label>
					<select id="certificateSelect" class="form-control"></select>
				</div>
			</fieldset>
		</div>
		{{--<button id="signButton" type="button" class="btn btn-primary">Assinar arquivos</button>
		<button id="refreshButton" type="button" class="btn btn-default">Atualizar certificados</button>--}}
	</form>
	<script>
	    $(document).ready(function () {
	        batchSignatureForm.init({
	            documentsIds: <?=json_encode($arquivosIds);?>,
	            documentsNames: <?=json_encode($arquivosNomes);?>,
	            arquivosToken: '<?=$request->token;?>',
	            certificateSelect: $('#certificateSelect'),
	            refreshButton: $('#refreshButton'),
	            signButton: $('#signButton'),
	        });
	    });
	</script>
<?php
}
?>