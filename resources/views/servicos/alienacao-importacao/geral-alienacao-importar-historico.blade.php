@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao-importar.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
<div class="container">
    @if(!in_array($class->id_tipo_pessoa,array(9,13)))
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Importar arquivo de Notificação de alienação fiduciária <span class="small">/ Serviços</span></h4>
            </div>
            <div id="filtro-certidao" class="panel-body">

                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Arquivos</legend>
                            <div id="arquivos-gerais" class="col-md-12">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="15" data-token="{{$token}}" data-limite="0">Importar notificação</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="assinaturas-arquivos" class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="alert alert-warning single">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem clearfix">
                                <span>Nenhum arquivo foi inserido.</span><br />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif

    @if (count($todas_arquivo_xml)>0)
        <div id="alert" class="alert alert-info">
            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
            <div class="menssagem clearfix" style="overflow:initial;">
                <span class="total">0 lotes selecionadas</span>
                <div class="pull-right">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target="#relatorio-alienacao" class="gerar-relatorio" disabled>Gerar relatório das selecionadas <span></span></a></a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading gradient01">
            <h4>Notificação de alienação fiduciária importadas <span class="small">/ Serviços</span></h4>
        </div>
        <div id="filtro-certidao" class="panel-body">
            <div class="panel table-rounded">
                <table id="pedidos-pendentes" class="table table-striped table-bordered small">
                    <thead>
                    <tr class="gradient01">
                        <th width="3%">
                            <div class="checkbox checkbox-primary">
                                <input id="selecionar-todas" class="styled" type="checkbox">
                                <label for="selecionar-todas"></label>
                            </div>
                        </th>
                        <th>Protocolo</th>
                        <th>Data da importação</th>
                        <th>Nome do arquivo</th>
                        <th>Registros processados</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($todas_arquivo_xml)>0)
                        @foreach ($todas_arquivo_xml as $arquivo)
                            <tr class="">
                                <td>
                                    <div class="checkbox checkbox-primary">
                                        <input id="alienacao-{{$arquivo->id_alienacao_arquivo_xml}}" class="alienacao" class="styled" type="checkbox" value="{{$arquivo->id_alienacao_arquivo_xml}}">
                                        <label for="alienacao-{{$arquivo->id_alienacao_arquivo_xml}}"></label>
                                    </div>
                                </td>
                                <td>{{$arquivo->protocolo}}</td>
                                <td>{{$arquivo->dt_arquivo}}</td>
                                <td>{{$arquivo->no_arquivo}}</td>
                                <td>{{$arquivo->nu_registro_processados}}</td>
                                <td class="options">
                                    @if($arquivo->in_assinatura_digital=='S')
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#visualizar-assinatura" data-idarquivo="{{$arquivo->id_alienacao_arquivo_xml}}" data-noarquivo="{{$arquivo->no_arquivo}}">Visualizar assinatura</button>
                                    @else
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#assinar-xml" data-idarquivo="{{$arquivo->id_alienacao_arquivo_xml}}" data-noarquivo="{{$arquivo->no_arquivo}}" {{(in_array($class->id_tipo_pessoa,array(9,13))?'disabled="disabled"':'')}}>Assinar arquivo</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                <div class="single alert alert-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    <div class="mensagem">
                                        Nenhum registro foi importado.
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="novo-arquivo" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar arquivo</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"><form name="form-novo-arquivo" method="post" action="" class="clearfix"></form></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="enviar-arquivo btn btn-success">Enviar arquivo</button>
            </div>
        </div>
    </div>
</div>
<div id="assinar-xml" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assinar XML</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-info" id="refreshButton">Atualizar certificados</button>
                <button type="button" class="btn btn-success" id="signButton">Assinar xml</button>
            </div>
        </div>
    </div>
</div>
<div id="visualizar-assinatura" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assinatura do arquivo - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="relatorio-alienacao" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Relatório de lotes - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="salvar-relatorio btn btn-success pull-right">Salvar relatório</button>
                    </div>
                </div>
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                <button type="button" class="salvar-relatorio btn btn-success">Salvar relatório</button>
            </div>
        </div>
    </div>
</div>
@endsection