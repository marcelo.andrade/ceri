<div id="filtro-certidao" class="panel-body">
    <div class="panel table-rounded">
        <table id="pedidos-pendentes" class="table table-striped table-bordered small">
            <thead>
            <tr class="gradient01">
                <th>Protocolo</th>
                <th>Data da cadastro</th>
                <th>Id legado</th>
                <th>Número contrato / Dv</th>
                <th>Cód. serventia cliente</th>
                <th>Matrícula imóvel</th>
                <th>Motivo baixa</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @if (count($todas_baixa_arquivo_xml)>0)
                @foreach ($todas_baixa_arquivo_xml as $baixa_arquivo_xml)
                    <tr class="">
                        <td>{{$baixa_arquivo_xml->alienacao_arquivo_xml->protocolo}}</td>
                        <td>{{$baixa_arquivo_xml->dt_cadastro}}</td>
                        <td>{{$baixa_arquivo_xml->id_legado}}</td>
                        <td>{{$baixa_arquivo_xml->nu_contrato}} / {{$baixa_arquivo_xml->nu_dv_contrato}}</td>
                        <td>{{$baixa_arquivo_xml->codigo_serventia_cliente}}</td>
                        <td>{{$baixa_arquivo_xml->matricula_imovel}}</td>
                        <td>{{$baixa_arquivo_xml->id_motivo_baixa}}</td>
                        <td>{{$baixa_arquivo_xml->alienacao_arquivo_xml_registro_status->no_registro_status}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhum registro foi encontrado.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>