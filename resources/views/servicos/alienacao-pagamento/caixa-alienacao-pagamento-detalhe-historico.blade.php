<div class="fieldset-group clearfix">
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="notificacoes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th>Protocolo / Data</th>
                            <th>Serventia</th>
                            <th>Contrato</th>
                            <th>Devedores</th>
                            <th width="10%">Custo</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $i=0;
                            $valorTotal = 0;
                            //dd($alienacoes);
                        ?>
                        @foreach ($alienacoes as $alienacao)
                            <tr>
                            <td>
                                {{ $alienacao->protocolo_pedido}} <br>/
                                {{ formatar_data($alienacao->dt_pedido)}}
                            </td>
                            <td>{{$alienacao->no_serventia}}</td>
                            <td>{{$alienacao->numero_contrato}}</td>
                            <td>
                                {{$alienacao->no_devedores}}
                               {{-- @if(count($alienacao->aliencao_valor->alienacao->alienacao_devedor)>0)
                                    @foreach($alienacao->aliencao_valor->alienacao->alienacao_devedor as $devedor)
                                        <span class="label label-primary">{{$devedor->alienacao_devedor->no_devedor}}</span><br/>
                                    @endforeach
                                @endif--}}
                            </td>
                                <td> <span class="real">{{$alienacao->va_repasse}}</span></td>
                            <td>
                                <a href="#" class="btn btn-success"  data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{ $alienacao->id_alienacao}}" data-protocolo="{{ $alienacao->protocolo_pedido}}">Ver detalhes</a>
                            </td>
                            </tr>
                            <?php
                            $i++;
                            $valorTotal += $alienacao->va_repasse;
                            $no_arquivo_comprovante = $alienacao->no_arquivo_comprovante;
                            $id_alienacao_valor_repasse_lote = $alienacao->id_alienacao_valor_repasse_lote;
                            ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div id="serventias-total-geral">
                    <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                        <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                        <div class="pull-right totais">
                            <label class="quantidade label label-default"><b>Quantidade selecionada:</b>
                                <span>{{$i}}</span>
                            </label>&nbsp;
                            <label class="valor-total label label-primary"><b>Valor total:</b>
                                <span class="real">{{$valorTotal}}</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@if($no_arquivo_comprovante!="")
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Comprovante de pagamento</legend>
            <div class="erros alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Data do pagamento</label>
                    <input type="text" name="dt_repasse_lote" class="form-control data" disabled="disabled" value="{{formatar_data($alienacoes[0]->dt_repasse_lote)}}" />
                </div>
                <div class="col-md-6">
                    <label class="small">Valor pago</label>
                    <input type="text" name="va_pago" class="form-control real" value="{{$alienacoes[0]->va_repasse_lote}}" disabled="disabled" />
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Arquivo do comprovante</label><br />
                        {{--<a href="#" class="btn btn-success" data-toggle="modal" data-target="#arquivo-comprovante-caixa" data-idalienacaovalorrepasselote="{{$alienacoes->id_alienacao_valor_repasse_lote}}" data-noarquivo="{{$alienacoes->no_arquivo_comprovante}}" data-noextensao="{{$alienacoes->no_extensao_comprovante}}">{{$alienacoes->no_arquivo_comprovante}}</a>--}}
                        <a href="{{URL::to('/arquivos/download/comprovante-pagamento-caixa/'.$id_alienacao_valor_repasse_lote)}}" class="btn btn-success" target="_blank">{{$no_arquivo_comprovante}}</a>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
@endif