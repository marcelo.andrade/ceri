<input type="hidden" name="id_alienacao_valor_repasse_lote" id="id_alienacao_valor_repasse_lote" value="{{$alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote}}" />
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Comprovante de pagamento</legend>
        <div class="erros alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-6">
				<label class="small">Data do pagamento</label>
				<input type="text" name="dt_repasse_lote" class="form-control data" disabled="disabled" value="{{formatar_data($alienacao_valor_repasse_lote->dt_repasse_lote)}}" />
			</div>
            <div class="col-md-6">
				<label class="small">Valor pago</label>
				<input type="text" name="va_pago" class="form-control real" value="{{$alienacao_valor_repasse_lote->va_repasse_lote}}" disabled="disabled" />
			</div>
		</div>
        <div class="form-group clearfix">
            <div class="col-md-12">
				<label class="small">Arquivo do comprovante</label>
                <div class="arquivo fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput">
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="button input-group-addon btn-file">
                        <span class="fileinput-new">Adicionar arquivo</span>
                        <span class="fileinput-exists">Alterar</span>
                        <input type="file" name="no_arquivo_comprovante">
                    </span>
                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
			</div>
		</div>
	</fieldset>
</div>