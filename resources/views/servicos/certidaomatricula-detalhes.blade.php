<div class="fieldset-group clearfix">
    <div class="col-md-4">
        <select name="id_cidade" class="form-control pull-left" disabled>
            <option>{{$pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
        </select>
    </div>
    <div class="col-md-5">
        <select name="id_serventia" class="form-control pull-left" disabled>
            <option>{{$pedido->pedido_pessoa->no_serventia}}</option>
        </select>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>Pesquisa</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="radio radio-inline">
                                <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="matricula" value="4" @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==4) checked @endif disabled>
                                <label for="matricula" class="small">
                                    Matrícula
                                </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="cpf" value="1" @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==1) checked @endif disabled>
                                <label for="cpf" class="small">
                                    CPF
                                </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="cnpj" value="2" @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==2) checked @endif disabled>
                                <label for="cnpj" class="small">
                                    CNPJ
                                </label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="nome" value="3" @if ($pedido->certidao->id_tipo_certidao_chave_pesquisa==3) checked @endif disabled>
                                <label for="nome" class="small">
                                    Nome
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="de_chave_certidao" class="form-control" value="{{$pedido->certidao->de_chave_certidao}}" disabled="disabled" />
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        @if(Auth::User()->id_tipo_usuario==4 || Auth::User()->id_tipo_usuario==7)
            <?php
            if ($pedido->certidao->processo) {
                $numero_processo = $pedido->certidao->processo->numero_processo;
            } else {
                $numero_processo = '';
            }
            ?>
            <div class="col-md-6 fix-height">
                <fieldset>
                    <legend>Judiciário</legend>
                    <div class="form-group col-md-6">
                        <div class="checkbox">
                            <input type="checkbox" name="in_isenta" id="in_isenta" @if ($pedido->certidao->in_isenta=='S') checked @endif disabled>
                            <label for="in_isenta" class="small">
                                Isenta de emolumentos
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" name="processo" class="form-control" value="{{$numero_processo}}" disabled />
                    </div>
                </fieldset>
            </div>
        @endif
    </div>
</div>