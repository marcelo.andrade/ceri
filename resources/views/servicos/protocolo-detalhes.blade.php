<form name="form-detalhes" method="post" action="" class="clearfix">
	<div class="fieldset-group clearfix">
        <div class="col-md-4">
            <select name="id_cidade" class="form-control pull-left" disabled>
                <option>{{$pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
            </select>
        </div>
        <div class="col-md-5">
            <select name="id_serventia" class="form-control pull-left" disabled>
                <option>{{$pedido->pedido_pessoa->no_serventia}}</option>
            </select>
        </div>
	</div>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Arquivos do protocolo</legend>
            <div class="col-md-12">
                @if(count($pedido->protocolo->arquivos)>0)
                    @foreach($pedido->protocolo->arquivos as $arquivo)
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#arquivo-protocolo" data-idarquivo="{{$arquivo->id_protocolo_arquivo}}" data-protocolo="{{$pedido->protocolo_pedido}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</a>
                    @endforeach
                @endif
            </div>
        </fieldset>
    </div>
    @if ($pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_PROTOCOLADO)
        <div class="fieldset-group clearfix">
            <div class="fieldset-group clearfix">
                <fieldset class="col-md-12">
                    <legend>Dados do protocolo</legend>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-6">
                            <label class="small">Natureza formal</label>
                            <select name="id_protocolo_natureza" class="form-control" disabled>
								<option>{{$pedido->protocolo->protocolo_natureza->no_protocolo_natureza}}</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="small">Assunto</label>
                            <input type="text" name="no_assunto" class="form-control" value="{{$pedido->protocolo->no_assunto}}" disabled />
                        </div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-6">
                            <label class="small">Nome do apresentante</label>
                            <input type="text" name="no_apresentante" class="form-control" value="{{$pedido->protocolo->no_apresentante}}" disabled />
                        </div>
                        <div class="col-md-2">
                            <label class="small">Tipo de documento</label>
                            <div class="radios col-md-12">
                                <div class="radio radio-inline">
                                    <input type="radio" name="tp_pessoa_apresentante" id="tp_pessoa_apresentante_F" value="F" @if($pedido->protocolo->tp_pessoa_apresentante=='F') checked @endif disabled>
                                    <label for="tp_pessoa_apresentante_F" class="small">
                                        CPF
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" name="tp_pessoa_apresentante" id="tp_pessoa_apresentante_J" value="J" @if($pedido->protocolo->tp_pessoa_apresentante=='J') checked @endif disabled>
                                    <label for="tp_pessoa_apresentante_J" class="small">
                                        CNPJ
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="small">Documento do apresentante <span></span></label>
                            <input type="text" name="nu_cpf_cnpj_apresentante" class="form-control" value="{{$pedido->protocolo->nu_cpf_cnpj_apresentante}}" disabled />
                        </div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-12">
                            <label class="small">Conteúdo do protocolo</label>
                            <textarea name="de_conteudo" class="form-control" rows="10" disabled>{{$pedido->protocolo->de_conteudo}}</textarea>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Imóveis</legend>
                    <div class="col-md-12">
                        <div class="panel table-rounded">
                            <table id="imoveis-protocolo" class="table table-striped table-bordered small">
                                <thead>
                                    <tr class="gradient01">
                                        <th width="10%">Matrícula</th>
                                        <th width="80%">Proprietários</th>
                                        <th width="10%">Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($pedido->protocolo->bem_imoveis)>0)
                                        @foreach($pedido->protocolo->bem_imoveis as $bem_imovel)
                                            <tr>
                                                <td>{{$bem_imovel->matricula_bem_imovel}}</td>
                                                <td>
                                                    @if(count($bem_imovel->proprietarios)>0)
                                                        @foreach($bem_imovel->proprietarios as $proprietario)
                                                            <span class="label label-primary">{{$proprietario->proprietario_bem_imovel->no_proprietario_bem_imovel}} ({{$proprietario->proprietario_bem_imovel->nu_cpf_cnpj}})</span> 
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Ações <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu black">
                                                            <li><a href="#" data-toggle="modal" data-target="#detalhes-imovel" data-idbemimovel="{{$bem_imovel->id_bem_imovel}}" data-matriculabemimovel="{{$bem_imovel->matricula_bem_imovel}}">Ver detalhes</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
	@endif
   	@if(count($pedido->protocolo->observacoes)>0)
        <div class="fieldset-group">
            <fieldset>
                <legend>Histórico de observações</legend>
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table id="historico-observacoes" class="table table-striped table-bordered small">
                            <thead>
                                <tr class="gradient01">
                                    <th>Usuário</th>
                                    <th>Data</th>
                                    <th>Observação</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($pedido->protocolo->observacoes)>0)
                                    @foreach($pedido->protocolo->observacoes as $observacao)
                                        <tr>
                                            <td>{{$observacao->usuario_cad->no_usuario}}</td>
                                            <td>{{Carbon\Carbon::parse($observacao->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                            <td>{{$observacao->de_observacao}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
	@endif
</form>