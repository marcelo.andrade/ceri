@if($penhora_bem_imovel->id_serventia_certificado>0)
    <div class="certificado alert alert-success clearfix">
        <i class="icon glyphicon glyphicon-ok pull-left"></i>
        <div class="menssagem pull-left">
            O documento foi assinado digitalmente por:<br />
            <b>{{$penhora_bem_imovel->serventia_certificado->no_comum}}</b><br />
            <b>Data do documento:</b> {{Carbon\Carbon::parse($penhora_bem_imovel->dt_cadastro)->format('d/m/Y')}}
        </div>
        <button id="resultado-certificado-digital" type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#certificado-detalhes" data-idpenhorabemimovel="{{$penhora_bem_imovel->id_penhora_bem_imovel}}"><i class="glyphicon glyphicon-check"></i> Ver certificado</button>
    </div>
@endif
<object data="{{URL::to('/arquivos/penhora/'.$penhora_bem_imovel->id_penhora_bem_imovel.'/'.$penhora_bem_imovel->no_arquivo)}}" type="application/pdf" class="resultado-pdf @if($penhora_bem_imovel->id_serventia_certificado>0) assinado @endif">
    <p>Seu navegador não tem um plugin pra PDF</p>
</object>