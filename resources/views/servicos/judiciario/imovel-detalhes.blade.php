<form name="form-novo-imovel" method="post" action="" class="clearfix">
    <div class="erros alert alert-danger" style="display:none">
        <i class="icon glyphicon glyphicon-remove pull-left"></i>
        <div class="menssagem"></div>
    </div>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Dados do imóvel</legend>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <label class="small">Matrícula</label>
                    <input type="text" name="matricula_bem_imovel" class="form-control" value="{{$bem_imovel->matricula_bem_imovel}}" disabled>
                </div>
                <div class="col-md-4">
                    <label class="small">Cidade do imóvel</label>
                    <select name="bemimovel_id_cidade" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}" @if($bem_imovel->id_cidade==$cidade->id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="small">Tipo do imóvel</label>
                    <select name="id_tipo_zona_imovel" class="form-control pull-left" disabled>
                        <option value="0">Selecione</option>
                        @if(count($tipos_imovel)>0)
                            @foreach ($tipos_imovel as $tipo)
                                <option value="{{$tipo->id_tipo_zona_imovel}}" @if($bem_imovel->id_tipo_zona_imovel==$tipo->id_tipo_zona_imovel) selected @endif>{{$tipo->no_tipo_zona_imovel}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-7">
                    <label class="small">Endereço</label>
                    <input type="text" name="no_endereco" class="form-control" value="{{$bem_imovel->no_endereco}}" disabled>
                </div>
                <div class="col-md-5">
                    <label class="small">Bairro</label>
                    <input type="text" name="no_bairro" class="form-control" value="{{$bem_imovel->no_bairro}}" disabled>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Descrição do imóvel</label>
                    <textarea name="de_bem_imovel" class="form-control" rows="3" disabled>{{$bem_imovel->de_bem_imovel}}</textarea>
                </div>
            </div>
        </fieldset>
	</div>
    <div class="fieldset-group">
        <fieldset>
            <legend>Proprietários do imóvel</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="proprietarios-imovel" class="table table-striped table-bordered small">
                        <thead>
                            <tr class="gradient01">
                                <th>CPF / CNPJ</th>
                                <th>Nome</th>
                                <th>Passivo de penhora</th>
                        	</tr>
                        </thead>
                        <tbody>
                        	@foreach ($bem_imovel->proprietarios as $proprietario)
                            	<tr>
                                	<td>{{$proprietario->proprietario_bem_imovel->nu_cpf_cnpj}}</td>
                                	<td>{{$proprietario->proprietario_bem_imovel->no_proprietario_bem_imovel}}</td>
                                	<td>{{($proprietario->in_passivo_penhora=='S'?'Sim':'Não')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
			</div>
		</fieldset>
    </div>
</form>