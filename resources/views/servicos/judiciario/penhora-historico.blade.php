<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered table-fixed small">
        <thead>
            <tr class="gradient01">
                <th width="14%">Protocolo</th>
                <th width="12%">Data do cadastro</th>
                <th width="18%">Número do processo</th>
                <th width="8%">Status</th>
                <th width="14%">Tipo</th>
                <th width="22%">Serventia</th>
                <th width="12%">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if(count($todas_penhoras)>0)
                @foreach ($todas_penhoras as $penhora)
                    <tr id="{{$penhora->pedido->id_pedido}}" @if(count($penhora->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
                        <td>
                            {{$penhora->pedido->protocolo_pedido}}
                            @if(count($penhora->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                        </td>
                        <td>{{$penhora->pedido->dt_pedido}}</td>
                        <td>{{$penhora->numero_processo}}</td>
                        <td>{{$penhora->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                        <td>{{$penhora->tipo_penhora->no_tipo_penhora}}</td>
                        <td>{{$penhora->pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
                        <td class="options">
                            <div class="btn-group" role="group">
                                @if ($penhora->pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_FINALIZADO)
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#resultado-penhora" data-idpedido="{{$penhora->pedido->id_pedido}}" data-protocolo="{{$penhora->pedido->protocolo_pedido}}">Resultado</button>
                                @else
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-penhora" data-idpedido="{{$penhora->pedido->id_pedido}}" data-protocolo="{{$penhora->pedido->protocolo_pedido}}">Detalhes</button>
                                @endif
                                @if(!in_array($class->id_tipo_pessoa,array(9,13)) or count($penhora->penhora_exigencia)>0)
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            @if(!in_array($class->id_tipo_pessoa,array(9,13)))
                                                <li>
                                                    <a href="#" data-toggle="modal" data-target="#recibo-penhora" data-idpedido="{{$penhora->pedido->id_pedido}}" data-protocolo="{{$penhora->pedido->protocolo_pedido}}">
                                                        @if ($penhora->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',14)->count()>0)
                                                            Visualizar recibo
                                                        @else
                                                            Emitir recibo
                                                        @endif
                                                    </a>
                                                </li>
                                            @endif
                                            @if (count($penhora->penhora_exigencia)>0)
                                                <li><a href="#" data-toggle="modal" data-target="#exigencias-penhora" data-idpedido="{{$penhora->pedido->id_pedido}}" data-protocolo="{{$penhora->pedido->protocolo_pedido}}">Ver exigências</a></li>
                                            @endif
                                            @if ($penhora->pedido->id_situacao_pedido_grupo_produto==$class::ID_SITUACAO_FINALIZADO)
                                                <li><a href="#" data-toggle="modal" data-target="#detalhes-penhora" data-idpedido="{{$penhora->pedido->id_pedido}}" data-protocolo="{{$penhora->pedido->protocolo_pedido}}">Detalhes</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7">
                        <div class="single alert alert-danger">
                            <i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
                                Nenhuma penhora foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>