@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.penhora.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.arquivos.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.processo.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.penhora.bemimovel.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Penhora eletrônica de imóveis <span class="small">/ Serviços</span></h4>
            </div>
            <div class="panel-body">
                @if(!in_array($class->id_tipo_pessoa,array(9,13)))
                    <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#nova-penhora">
                        Nova penhora
                    </button>
                @endif
                <form name="form-filtro" method="post" action="" class="clearfix">
                    {{csrf_field()}}
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Tipo de penhora</legend>
                                    <div class="col-md-12">
                                        <select name="id_tipo_penhora" class="form-control pull-left">
                                            <option value="0">Todos os tipos</option>
                                            @if(count($tipos)>0)
                                                @foreach ($tipos as $tipo)
                                                    <option value="{{$tipo->id_tipo_penhora}}" @if($request->id_tipo_penhora==$tipo->id_tipo_penhora) selected @endif>{{$tipo->no_tipo_penhora}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Período</legend>
                                    <div class="col-md-12">
                                        <div class="periodo input-group input-daterange">
                                            <input type="text" class="form-control pull-left" name="dt_inicio" rel="periodo" value="{{$request->dt_inicio}}" />
                                            <span class="input-group-addon small pull-left">até</span>
                                            <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Status</legend>
                                    <div class="col-md-12">
                                        <select name="id_situacao_pedido_grupo_produto" class="form-control pull-left">
                                            <option value="0">Todos os status</option>
                                            @if(count($situacoes)>0)
                                                @foreach ($situacoes as $situacao)
                                                    <option value="{{$situacao->id_situacao_pedido_grupo_produto}}" @if($request->id_situacao_pedido_grupo_produto==$situacao->id_situacao_pedido_grupo_produto) selected @endif>{{$situacao->no_situacao_pedido_grupo_produto}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <legend>Protocolo</legend>
                                <div class="col-md-12">
                                    <div class="protocolo">
                                        <input type="text" name="protocolo_pedido" id="protocolo_pedido" class="form-control" value="{{$request->protocolo_pedido}}" />
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <legend>Número do processo</legend>
                                <div class="col-md-12">
                                    <div class="protocolo">
                                        <input type="text" value="" class="numero_processo form-control" id="numero_processo" name="numero_processo">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros" />
                            <input type="submit" class="btn btn-success" value="Filtrar pesquisas" />
                        </div>
                    </fieldset>
                </form>
                @include('servicos.judiciario.penhora-historico')
            </div>
        </div>
    </div>
    <div id="nova-penhora" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nova penhora</h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-nova-penhora" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="submit incluir-penhora btn btn-success">Enviar penhora</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-penhora" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes penhora - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="exigencias-penhora" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Pendência da penhora - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="resultado-penhora" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Resultado penhora - <span></span></h4>
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="recibo-penhora" class="total-height modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recibo penhora eletrônica de imóveis  - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <a id="resultado-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do documento</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    @include('arquivos.arquivos-modais')
@endsection