<?php
if ($pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto=='Finalizado') {
	$desativar_exigencia = 'disabled';
} else {
	$desativar_exigencia = '';
}
?>
@if(!in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group clearfix">
        <form name="form-resposta-exigencia" method="post" action="" class="clearfix" enctype="multipart/form-data">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
            <input type="hidden" name="penhora_token" value="{{$penhora_token}}" />
            <fieldset class="col-md-12">
                <legend>Responder a exigência</legend>
                <div class="erros-exigencia alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Resposta</label>
                        <textarea name="de_resposta" class="form-control" {{$desativar_exigencia}}></textarea>
                    </div>
                </div>
                <div class="fieldset-group clearfix">
                    <div class="col-md-6">
                        <fieldset>
                            <legend>
                                <div class="radio">
                                    <input type="radio" name="tipo_resultado_envio" id="upload" value="2" {{$desativar_exigencia}}>
                                    <label for="upload">
                                       <b>Upload do resultado</b>
                                    </label>
                                </div>
                            </legend>
                            <div class="col-md-12">
                                <div id="arquivos-pendencias" class="col-md-12" data-local="exigencias">
                                    <button type="button" class="btn btn-success disabled" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="11" data-token="{{$penhora_token}}" data-limite="0" disabled>Adicionar arquivo</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>
                                <div class="radio">
                                    <input type="radio" name="tipo_resultado_envio" id="digitar" value="1" {{$desativar_exigencia}}>
                                    <label for="digitar">
                                       <b>Digitar resultado</b>
                                    </label>
                                </div>                           
                            </legend>
                            <div class="col-md-12">
                                <textarea name="de_resultado" class="form-control" placeholder="Digite uma observação" rows="3" disabled></textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>
                @if(!$desativar_exigencia)
                    <div class="botoes fieldset-group clearfix">
                        <div class="col-md-12">
                            <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                            <input type="submit" class="btn btn-success pull-right" value="Salvar resposta" />
                        </div>
                    </div>
                @endif
            </fieldset>
        </form>
    </div>
@endif
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico de exigências</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Resposta</th>
                            <th>Resultado</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($pedido->penhora->penhora_exigencia as $exigencia)
                            <tr>
                                <td>{{$exigencia->usuario_cad->no_usuario}}</td>
                                <td>{{Carbon\Carbon::parse($exigencia->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>{{$exigencia->de_resposta}}</td>
                                <td>
                                    @if ($exigencia->de_resultado!='')
                                        {{$exigencia->de_resultado}}
                                    @elseif(count($exigencia->arquivos_grupo)>0)
                                        @foreach($exigencia->arquivos_grupo as $arquivo)
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                @if($arquivo->in_ass_digital=='S')
                                                    <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                @endif
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-warning single">
                                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                            <div class="menssagem">Arquivos não enviados.</div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
@if(!in_array($class->id_tipo_pessoa,array(9,13)))
    <div id="assinaturas-arquivos" class="fieldset-group clearfix">
        <div class="col-md-12">
            <div class="alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem clearfix">
                    <span>Nenhum arquivo foi inserido.</span><br />
                    <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$penhora_token}}">Assinar arquivo(s)</a>
                </div>
            </div>
        </div>
    </div>
@endif