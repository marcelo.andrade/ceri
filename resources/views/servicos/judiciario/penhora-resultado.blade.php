<?php
if ($pedido->penhora->penhora_averbacao['no_documento'])
{
    $desativar_averbacao_documento  = 'disabled';
    $id_penhora_averbacao           = $pedido->penhora->penhora_averbacao->id_penhora_averbacao;
    $no_arquivo                     = $pedido->penhora->penhora_averbacao->no_documento;
}else{
    $no_arquivo                    = '';
    $id_penhora_averbacao          = '';
    $desativar_averbacao_documento = '';

}
?>
<div class="fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Penhora</legend>
        <div class="form-group clearfix">
            <div class="col-md-4">
                <label class="small">Número do processo</label>
                <input type="text" name="numero_processo" class="form-control" value="{{$pedido->penhora->numero_processo}}" disabled />
            </div>
            {{--<div class="col-md-4">
                <label class="small">Natureza do processo</label>
                <input type="text" name="natureza_processo" class="form-control" value="{{$pedido->penhora->processo->natureza_acao->no_natureza_acao}}" disabled />
            </div>--}}
            <div class="col-md-4">
                <label class="small"></label>
                <div class="checkbox">
                    <input type="checkbox" name="in_isenta" id="in_isenta" @if ($pedido->penhora->in_isenta=='S') checked @endif disabled>
                    <label for="in_isenta" class="small">
                        Isenta de emolumentos
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <label class="small">Protocolo</label>
                <input type="text" name="protocolo_pedido" class="form-control" value="{{$pedido->protocolo_pedido}}" disabled />
            </div>
            <div class="col-md-3">
                <label class="small">Data do pedido</label>
                <input type="text" name="dt_pedido" class="form-control" value="{{$pedido->dt_pedido}}" disabled />
            </div>
            <div class="col-md-5">
                <label class="small">Solicitante</label>
                <input type="text" name="dt_pedido" class="form-control" value="{{$pedido->penhora->usuario_cad->pessoa->no_pessoa}}" disabled />
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix row">
	@if($pedido->penhora->penhora_prenotacao)
        <div class="col-md-8">
            <fieldset>
                <legend>Prenotação</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-4">
                        <label class="small">Código</label>
                        <input type="text" name="codigo_prenotacao" class="form-control" value="{{$pedido->penhora->penhora_prenotacao->codigo_prenotacao}}" disabled />
                    </div>
                    <div class="col-md-4">
                        <label class="small">Data de cadastro</label>
                        <input type="text" name="dt_prenotacao" class="data form-control" value="{{Carbon\Carbon::parse($pedido->penhora->penhora_prenotacao->dt_prenotacao)->format('d/m/Y')}}" disabled />
                    </div>
                    <div class="col-md-4">
                        <label class="small">Data de vencimento</label>
                        <input type="text" name="dt_vencimento_prenotacao" class="data form-control" value="{{Carbon\Carbon::parse($pedido->penhora->penhora_prenotacao->dt_vencimento_prenotacao)->format('d/m/Y')}}" disabled />
                    </div>
                </div>
            </fieldset>
        </div>
    @endif
    @if($pedido->penhora->penhora_custa)
        <div class="col-md-4">
            <fieldset>
                <legend>Custa</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Valor</label>
                        <input type="text" name="va_custa" class="real form-control" value="{{$pedido->penhora->penhora_custa->va_custa}}" disabled />
                    </div>
                </div>
            </fieldset>
        </div>
	@endif
</div>
<div class="fieldset-group clearfix">
	@if($pedido->penhora->penhora_averbacao)
        <div class="fieldset-group clearfix">
            <fieldset class="col-md-12">
                <legend>Resposta com averbação</legend>
                <div class="form-group clearfix">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Arquivos</legend>
                                <div class="col-md-12">
                                    @if(count($pedido->penhora->penhora_averbacao->arquivos_grupo)>0)
                                        @foreach($pedido->penhora->penhora_averbacao->arquivos_grupo as $arquivo)
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                @if($arquivo->in_ass_digital=='S')
                                                    <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                @endif
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-warning single">
                                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                            <div class="menssagem">Não há arquivos para serem listados.</div>
                                        </div>
                                    @endif
                                </div>
                            </fieldset>
                        </div>
                    </div>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">Resposta</label>
                        <textarea name="de_resposta_averbacao" class="form-control" disabled>{{$pedido->penhora->penhora_averbacao->de_resposta}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Averbações</legend>
                            <div class="col-md-12">
                                <div class="panel table-rounded">
                                    <table id="historico-respostas" class="table table-striped table-bordered small">
                                        <thead>
                                            <tr class="gradient01">
                                                <th>Matrícula</th>
                                                <th width="30%">Certidão</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($pedido->penhora->bens_imoveis as $bem_imovel)
                                                <tr>
                                                    <td>{{$bem_imovel->bem_imovel->matricula_bem_imovel}}</td>
                                                    <td>
                                                        @if(count($bem_imovel->arquivos_grupo)>0)
                                                            @foreach($bem_imovel->arquivos_grupo as $arquivo)
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                                    @if($arquivo->in_ass_digital=='S')
                                                                        <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="alert alert-warning single">
                                                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                                                <div class="menssagem">Certidão não enviada.</div>
                                                            </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </fieldset>
        </div>
    @endif
</div>
