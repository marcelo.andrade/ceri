<form name="form-novo-imovel" method="post" action="" class="clearfix">
    <div class="erros alert alert-danger" style="display:none">
        <i class="icon glyphicon glyphicon-remove pull-left"></i>
        <div class="menssagem"></div>
    </div>
    <?php
    /*
	<div class="fieldset-group clearfix">
        <div class="col-md-5">
            <label class="small">Cidade</label>
            <select name="id_cidade" class="form-control pull-left">
                <option value="0">Selecione</option>
                @if(count($cidades)>0)
                	@foreach ($cidades as $cidade)
                    	<option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div id="id_serventia" class="col-md-7">
            <label class="small">Cartório</label>
            <select name="id_serventia" class="form-control pull-left" disabled>
                <option value="0">Selecione</option>
            </select>
        </div>
	</div>
	*/
	?>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Dados do imóvel</legend>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <label class="small">Matrícula</label>
                    <input type="text" name="matricula_bem_imovel" class="form-control">
                </div>
                <div class="col-md-4">
                    <label class="small">Cidade do imóvel</label>
                    <select name="bemimovel_id_cidade" class="form-control pull-left">
                        <option value="0">Selecione</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="small">Tipo do imóvel</label>
                    <select name="id_tipo_zona_imovel" class="form-control pull-left">
                        <option value="0">Selecione</option>
                        @if(count($tipos_imovel)>0)
                            @foreach ($tipos_imovel as $tipo)
                                <option value="{{$tipo->id_tipo_zona_imovel}}">{{$tipo->no_tipo_zona_imovel}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-7">
                    <label class="small">Endereço (Opcional)</label>
                    <input type="text" name="no_endereco" class="form-control">
                </div>
                <div class="col-md-5">
                    <label class="small">Bairro (Opcional)</label>
                    <input type="text" name="no_bairro" class="form-control">
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Descrição do imóvel (Opcional)</label>
                    <textarea name="de_bem_imovel" class="form-control" rows="3"></textarea>
                </div>
            </div>
        </fieldset>
	</div>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Cadastro de proprietário</legend>
            <div class="col-md-12">
                <div class="erros-partes alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
				<div class="form-group">
                    <div class="radio radio-inline">
                        <input type="radio" name="tp_pessoa" id="tp_pessoa_f" value="F">
                        <label for="tp_pessoa_f" class="small">
                            CPF
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="tp_pessoa" id="tp_pessoa_j" value="J">
                        <label for="tp_pessoa_j" class="small">
                            CNPJ
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="small">Documento identificador <span></span></label>
                    <input type="text" name="nu_cpf_cnpj" class="form-control" disabled="disabled" />
                </div>
                <div class="form-group">
                    <label class="small">Nome</label>
	                <input type="text" name="no_proprietario_bem_imovel" class="form-control" />
                </div>
                <div class="form-group">
                    <div class="checkbox radio-inline">
                        <input type="checkbox" name="in_passivo_penhora" id="in_passivo_penhora" value="S">
                        <label for="in_passivo_penhora" class="small">
                            Passivo de penhora
                        </label>
                    </div>
                </div>
                <div class="form-group">
	                <button type="button" class="cancelar-proprietario btn btn-primary pull-left">Cancelar</button>
    	            <button type="button" class="incluir-proprietario btn btn-success pull-right">Incluir proprietário</button>
                </div>
            </div>
        </fieldset>
	</div>
    <div class="fieldset-group">
        <fieldset>
            <legend>Proprietários do imóvel</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="proprietarios-imovel" class="table table-striped table-bordered small">
                        <thead>
                            <tr class="gradient01">
                                <th>CPF / CNPJ</th>
                                <th>Nome</th>
                                <th>Passivo de penhora</th>
                        	</tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
			</div>
		</fieldset>
    </div>
</form>