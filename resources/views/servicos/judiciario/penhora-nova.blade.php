<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<input type="hidden" name="id_processo" id="id_processo" value="0" />
<input type="hidden" name="penhora_token" id="penhora_token" value="{{$penhora_token}}" />
<input type="hidden" name="va_emitir_certidao" id="va_emitir_certidao" value="0">
<div class="fieldset-group clearfix">
    <div class="col-md-5">
        <label class="small">Cidade</label>
        <select name="id_cidade" class="form-control pull-left">
            <option value="0">Selecione</option>
            @if(count($cidades)>0)
                @foreach ($cidades as $cidade)
                    <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-4">
        <label class="small">Serventia</label>
        <select name="id_serventia_select" class="form-control pull-left" disabled>
            <option value="0">Selecione</option>
            @if(count($serventias)>0)
                @foreach ($serventias as $serventia)
                    <option value="{{$serventia->id_serventia}}">{{$serventia->no_serventia}}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-3">
        <label>&nbsp;</label>
        <div class="btn-group btn-group-justified">
            <div class="btn-group">
                <button class="incluir-serventia disabled btn btn-default btn-inline" type="button" disabled="disabled">Incluir</button>
            </div>
            <div class="btn-group">
                <button class="incluir-todas-serventias btn btn-primary btn-inline" type="button" >Incluir todas</button>
            </div>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Serventias selecionadas</legend>
            <div class="btn-list col-md-12" id="serventias-selecionadas"></div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-4">
        <fieldset>
            <legend>Tipo</legend>
            @if(count($tipos)>0)
                <div class="col-md-12">
                    @foreach($tipos as $tipo)
                        <div class="radio">
                            <input type="radio" name="id_tipo_penhora" id="id_tipo_penhora_{{$tipo->id_tipo_penhora}}" value="{{$tipo->id_tipo_penhora}}">
                            <label for="id_tipo_penhora_{{$tipo->id_tipo_penhora}}" class="small">
                                {{$tipo->no_tipo_penhora}}
                            </label>
                        </div>
                    @endforeach
                </div>
            @endif
        </fieldset>
    </div>
    <div class="col-md-3">
        <fieldset>
            <legend>Emitir certidão?</legend>
            <div class="col-md-12">
                <div class="radio radio-inline">
                    <input name="in_emitir_certidao" id="in_emitir_certidao_sim" value="S" data-tipo="S" type="radio">
                    <label for="in_emitir_certidao_sim" class="small">
                        Sim
                    </label>
                </div>
                <div class="radio">
                    <input name="in_emitir_certidao" id="in_emitir_certidao_nao" value="N" data-tipo="N" type="radio">
                    <label for="in_emitir_certidao_nao" class="small">
                        Não
                    </label>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-5">
        <fieldset>
            <legend>Dados da penhora</legend>
            <div class="col-md-6">
                <label class="small">Valor da causa</label>
                <input type="text" value="" name="va_penhora" class="real form-control">
            </div>
            <div class="col-md-6">
                <label class="small">Data da penhora</label>
                <input type="text" value="" name="dt_penhora" class="data form-control">
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
	<div class="col-md-6">
        <fieldset>
            <legend>Tipo de custas</legend>
            <div class="col-md-12">
                @if(count($tipo_custa)>0)
                    @foreach($tipo_custa as $tipo_custa)
                        <div class="radio">
                            <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}">
                            <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                {{$tipo_custa->no_tipo_custa}}
                            </label>
                        </div>
                    @endforeach
                @endif
			</div>
        </fieldset>
	</div>
    <div class="col-md-6">
        <fieldset>
            <legend>Processo</legend>
            <div class="col-md-12">
	            <label class="small">Número do processo</label>
    	        <input type="text" name="numero_processo" class="numero_processo form-control" maxlength="45" />
			</div>
        </fieldset>
    </div>
</div>
<div id="msg-isencao" class="fieldset-group clearfix" style="display:none">
    <div class="col-md-12">
    	<div class="alert alert-danger single">
    		Para que o processamento da Penhora se dê com AJG será necessário anexar o despacho de deferimento ou escolha outra opção para dar continuidade à solicitação.
    	</div>
    </div>
</div>
<div id="documento-isencao" class="fieldset-group clearfix" style="display:none">
    <fieldset>
        <legend>Documento de isenção de emolumentos</legend>
        <div id="arquivos-isencao" class="col-md-12">
            <button type="button" class="adicionar-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="10" data-token="{{$penhora_token}}" data-limite="1">Adicionar arquivo</button>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <div class="erros-imovel alert alert-danger single" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
	<div class="col-md-12">
        <fieldset>
            <legend>Imóveis</legend>
            <div class="col-md-6">
                <label class="small">Serventia</label>
                <select name="id_serventia_imoveis" class="form-control pull-left" disabled="">
                    <option value="0">Selecione</option>
                </select>
            </div>
            <div class="col-md-6">
                <label class="small">Matrícula do imóvel</label>
                <div class="input-group">
                    <input type="text" name="matricula_imovel" class="form-control" maxlength="20">
                    <div class="input-group-btn">
                        <button class="incluir-imovel btn btn-primary btn-inline" type="button">
                            Adicionar
                        </button>
                    </div>
                </div>
            </div>
        </fieldset>
	</div>
</div>
<div id="imoveis-cadastrados" class="fieldset-group clearfix" style="display: none">
	<div class="col-md-12">
        <fieldset>
            <legend>Imóveis cadastrados</legend>
            <div class="imoveis btn-list col-md-12"></div>
        </fieldset>
	</div>
</div>
<div class="fieldset-group clearfix">
	<div class="col-md-12">
        <fieldset>
            <legend>Arquivos</legend>
            <div id="arquivos-gerais" class="col-md-12">
            	<button type="button" class="adicionar-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="9" data-token="{{$penhora_token}}" data-limite="0">Adicionar arquivo</button>
            </div>
        </fieldset>
	</div>
</div>
<div id="assinaturas-arquivos" class="fieldset-group clearfix">
	<div class="col-md-12">
        <div class="alert alert-warning single">
            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
            <div class="menssagem clearfix">
            	<span>Nenhum arquivo foi inserido.</span><br />
                <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$penhora_token}}">Assinar arquivo(s)</a>
			</div>
        </div>
	</div>
</div>