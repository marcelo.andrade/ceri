<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Processo</legend>
            <div class="col-md-4">
                <label class="small">Número do processo</label>
                <div class="">
                    <input type="text" name="numero_processo" class="form-control" value="{{$pedido->penhora->numero_processo}}" disabled />
                </div>
            </div>
            <div class="col-md-2">
                <label class="small">Data do pedido</label>
                <input type="text" name="dt_pedido" class="form-control" value="{{$pedido->dt_pedido}}" disabled />
            </div>
            <div class="col-md-6">
                <label class="small">Solicitante</label>
                <input type="text" name="dt_pedido" class="form-control" value="{{$pedido->penhora->usuario_cad->pessoa->no_pessoa}}" disabled />
            </div>

        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Penhora</legend>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control pull-left" disabled="disabled">
                        <option value="{{$pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->id_cidade}}">{{$pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                    </select>
                </div>
                <div id="id_serventia" class="col-md-6">
                    <label class="small">Serventia</label>
                    <select name="id_serventia" class="form-control pull-left" disabled>
                        <option value="{{$pedido->pedido_pessoa_atual->pessoa->serventia->id_serventia}}">{{$pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Protocolo</label>
                    <input type="text" name="protocolo_pedido" class="form-control" value="{{$pedido->protocolo_pedido}}" disabled />
                </div>
                <div class="col-md-3">
                    <label class="small">Valor da causa</label>
                    <input type="text" value="{{$pedido->penhora->va_penhora}}" name="va_penhora" class="real form-control"  disabled >
                </div>
                <div class="col-md-3">
                    <label class="small">Data da penhora</label>
                    <input type="text" value="{{($pedido->penhora->dt_penhora?Carbon\Carbon::parse($pedido->penhora->dt_penhora)->format('d/m/Y'):'')}}" name="dt_penhora" class="data form-control"  disabled >
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Imóveis</legend>
                        <div class="col-md-12">
                            @foreach($pedido->penhora->bens_imoveis as $imoveis)
                                <div class="btn-group espacamento-button" >
                                    <button class="btn btn-primary" type="button">Matrícula: {{$imoveis->bem_imovel->matricula_bem_imovel}}</button>
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <fieldset>
                        <legend>Tipo de custas</legend>
                        <div class="col-md-12">
                            @if(count($tipo_custa)>0)
                                @foreach($tipo_custa as $tipo_custa)
                                    <div class="radio">
                                        <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($pedido->penhora->id_tipo_custa == $tipo_custa->id_tipo_custa) checked @endif disabled>
                                        <label for="id_tipo_penhora_{{$tipo_custa->id_tipo_custa}}" class="small">
                                            {{$tipo_custa->no_tipo_custa}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset>
                        <legend>Tipo</legend>
                        @if(count($tipos)>0)
                            <div class="col-md-12">
                                @foreach($tipos as $tipo)
                                    <div class="radio">
                                        <input type="radio" name="id_tipo_penhora" id="id_tipo_penhora_{{$tipo->id_tipo_penhora}}" value="{{$tipo->id_tipo_penhora}}"  @if($pedido->penhora->id_tipo_penhora == $tipo->id_tipo_penhora) checked @endif disabled>
                                        <label for="id_tipo_penhora_{{$tipo->id_tipo_penhora}}" class="small">
                                            {{$tipo->no_tipo_penhora}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset>
                        <legend>Emitir certidão?</legend>
                        <div class="form-group">
                            <div class="radio radio-inline">
                                <input name="in_emitir_certidao" id="in_emitir_certidao_sim" value="S" data-tipo="S" type="radio" @if($pedido->penhora->in_emitir_certidao == 'S') checked @endif disabled>
                                <label for="in_emitir_certidao_sim" class="small">
                                    Sim
                                </label>
                            </div>
                            <div class="radio">
                                <input name="in_emitir_certidao" id="in_emitir_certidao_nao" value="N" data-tipo="N" type="radio" @if($pedido->penhora->in_emitir_certidao == 'N') checked @endif  disabled>
                                <label for="in_emitir_certidao_nao" class="small">
                                    Não
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            @if(count($arquivos_isencao)>0)
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Documento de isenção de emolumentos</legend>
                            <div class="col-md-12">
                                @foreach($arquivos_isencao as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                        @if($arquivo->in_ass_digital=='S')
                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </fieldset>
                    </div>
                </div>
            @endif
            @if(count($arquivos)>0)
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Documento anexado a penhora</legend>
                            <div class="col-md-12">
                                @foreach($arquivos as $arquivo)
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                        @if($arquivo->in_ass_digital=='S')
                                            <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </fieldset>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Prenotação</legend>
            @if(count($pedido->penhora->penhora_prenotacao)>0)
                <div class="col-md-4">
                    <label class="small">Número</label>
                    <input type="text" name="codigo_prenotacao" class="form-control" value="{{$pedido->penhora->penhora_prenotacao->codigo_prenotacao}}" disabled />
                </div>
                <div class="col-md-4">
                    <label class="small">Data de cadastro</label>
                    <input type="text" name="dt_prenotacao" class="data form-control" value="{{Carbon\Carbon::parse($pedido->penhora->penhora_prenotacao->dt_prenotacao)->format('d/m/Y')}}" disabled />
                </div>
                <div class="col-md-4">
                    <label class="small">Data de vencimento</label>
                    <input type="text" name="dt_vencimento_prenotacao" class="data form-control" value="{{Carbon\Carbon::parse($pedido->penhora->penhora_prenotacao->dt_vencimento_prenotacao)->format('d/m/Y')}}" disabled />
                </div>
            @else
                <div class="col-md-12">
                    <div class="alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem">A prenotação ainda não foi inserida.</div>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Custas</legend>
            @if(count($pedido->penhora->penhora_custa)>0)
                <div class="col-md-8">
                    <label class="small">Descrição</label>
                    <input name="va_custa" class="form-control" value="{{$pedido->penhora->penhora_custa->de_custa}}" disabled type="text">
                </div>
                <div class="col-md-4">
                    <label class="small">Valor</label>
                    <input name="va_custa" class="real form-control" value="{{$pedido->penhora->penhora_custa->va_custa}}" disabled type="text">
                </div>
            @else
                <div class="col-md-12">
                    <div class="alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem">As custas ainda não foram inseridas.</div>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>