<input type="hidden" name="va_pedido" id="va_pedido" value="">
<input type="hidden" name="saldo_atual" id="saldo_atual" value="{{ Auth::User()->saldo_usuario() }}" />
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
	<div class="row">
        <div class="col-md-4">
            <label class="small">Cidade</label>
            <select name="id_cidade" class="form-control pull-left">
                <option value="0">Selecione</option>
                @if(count($cidades)>0)
                    @foreach ($cidades as $cidade)
                        <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div id="id_serventia" class="col-md-5">
            <label class="small">Cartório</label>
            <select name="id_serventia" id="serventia" class="form-control pull-left" disabled>
                <option value="0">Selecione</option>
            </select>
        </div>
	</div>
</div>
<div class="fieldset-group clearfix">
	<div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>Pesquisa</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            @if(count($tipos_chave)>0)
                                @foreach ($tipos_chave as $tipo_chave)
                                    <div class="radio radio-inline">
                                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" value="{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" data-tipo="{{$tipo_chave->tp_chave_pesquisa}}">
                                        <label for="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" class="small">
                                            {{$tipo_chave->no_chave_pesquisa}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="small">Chave de pesquisa <span></span></label>
                            <input type="text" name="de_chave_certidao" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        @if(Auth::User()->id_tipo_usuario==4 || Auth::User()->id_tipo_usuario==7)
            <div class="col-md-6 fix-height">
                <fieldset>
                    <legend>Judiciário</legend>
                    <div class="form-group col-md-6">
                        <div class="checkbox">
                            <input type="checkbox" name="in_isenta" id="in_isenta" value="S">
                            <label for="in_isenta" class="small">
                                Isenta de emolumentos
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="small">Número do processo</label>
                        <div class="input-group">
                            <input type="text" name="numero_processo" class="form-control" />
                            <div class="button input-group-addon" data-toggle="modal" data-target="#novo-processo">Novo processo</div>
                        </div>
                    </div>
                </fieldset>
            </div>
        @endif
        <div class="col-md-12" id="valConsulta" style="display: none">
            <div class="col-md-6">
                <fieldset>
                    <legend>Composição do valor para a nova certidão</legend>
                    <div class="col-md-10"></div>
                    <div class="col-md-2"><strong>R$</strong></div>
                    <div id="precoConsulta"></div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
    <input type="submit" class="btn btn-success pull-right" value="Incluir matrícula digital" />
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Pedidos pendentes (Não enviados ao cartório)</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="novos-pedidos" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Protocolo</th>
                            <th>Data do pedido</th>
                            <th>Cartório</th>
                            <th>Validade</th>
                            <th>Valor</th>
                            <th>Ações</th> 
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
