<?php
$query_string = http_build_query($request->except(['_token','ordby','ord']));
?>
@if (count($todas_alienacoes)>0)
    <div id="alert" class="alert alert-info">
        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
        <div class="menssagem clearfix" style="overflow:initial;">
            <span class="total">0 notificações selecionadas</span>
            <div class="pull-right">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target="#recibo-notificacoes" class="gerar-recibo" disabled>Gerar recibo das selecionadas <span></span></a></a>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th width="3%">
                    <div class="checkbox checkbox-primary">
                        <input id="selecionar-todas" class="styled" type="checkbox">
                        <label for="selecionar-todas"></label>
                    </div>
                </th>
                <th width="13%">Protocolo</th>
                <th width="11%">
                	<?php
                	if ($request->ordby=='data') {
	                	switch ($request->ord) {
							case 'asc':
								$ord = 'desc';
								$icone = 'fa fa-chevron-up';
								break;
							case 'desc': default:
								$ord = 'asc';
								$icone = 'fa fa-chevron-down';
								break;
						}
					} else {
						$ord = 'desc';
						$icone = 'fa fa-chevron-down';
					}
					?>
                	<a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=data&ord={{$ord}}">
                		<span class="pull-left">Data do pedido</span> 
                		<i class="pull-right {{$icone}}"></i>
                	</a>
                </th>
                <th width="8%">Contrato</th>
                <th width="14%">Status</th>
                <th width="20%">Etapa Atual</th>
                <th width="17%">
                    <?php
                    if ($request->ordby=='ultima-mov') {
                        switch ($request->ord) {
                            case 'asc':
                                $ord = 'desc';
                                $icone = 'fa fa-chevron-up';
                                break;
                            case 'desc': default:
                                $ord = 'asc';
                                $icone = 'fa fa-chevron-down';
                                break;
                        }
                    } else {
                        $ord = 'desc';
                        $icone = 'fa fa-chevron-down';
                    }
                    ?>
                    <a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=ultima-mov&ord={{($request->ordby=='ultima-mov'?$ord:'desc')}}">
                        <span class="pull-left">Data última movimentação</span>
                        <i class="pull-right {{$icone}}"></i>
                    </a>
                </th>

            </tr>
        </thead>
        <tbody>
        	@if (count($todas_alienacoes)>0)
                @foreach ($todas_alienacoes as $alienacao)
                    <tr>
                        <td>
                            <div class="checkbox checkbox-primary">
                                <input id="alienacao-{{$alienacao->id_alienacao}}" class="alienacao" class="styled" type="checkbox" value="{{$alienacao->id_alienacao}}">
                                <label for="alienacao-{{$alienacao->id_alienacao}}"></label>
                            </div>
                        </td>
                        <td>
                            {{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}
                        </td>
                        <td>{{\Carbon\Carbon::parse($alienacao->alienacao_pedido->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                        <td>{{$alienacao->numero_contrato}}</td>
                		<td>
                            @if ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto == $this::ID_SITUACAO_CANCELADO)
                                @if ($alienacao->checar_cancelamento_pagamento() == NULL)
                                    Processo Finalizado (Cancelado)
                                @else
                                    {{$alienacao->no_situacao_pedido_grupo_produto}}
                                @endif
                            @else
                                {{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}
                            @endif
                        </td>
                        <td>{{$alienacao->etapa_atual()}}</td>
                        <td>{{Carbon\Carbon::parse($alienacao->alienacao_pedido->alienacao_andamento_situacao->dt_cadastro)->format('d/m/Y')}}</td>

                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="8">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhuma notificação foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todas_alienacoes)}}</b> de <b>{{$todas_alienacoes_total}}</b> {{(count($todas_alienacoes)>1?'notificações':'notificação')}}.
</div>
<div align="center">
    <?php
    echo $todas_alienacoes->fragment('pedidos')->appends(['dt_ini' => $request->dt_ini,
                                                          'dt_fim' => $request->dt_fim,
                                                          'situacao' => $request->situacao,
                                                          'protocolo' => $request->protocolo,
                                                          'contrato' => $request->contrato,
                                                          'total_paginacao' => $request->total_paginacao,
                                                          'grupo' => $request->grupo,
                                                          'filtro' => $request->filtro])->render();
    ?>
</div>