@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.recibo.lote.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.arquivos.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
	<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
            	<h4>Recibo de Notificações em lote <span class="small">/ Relatórios</span></h4>
            </div>
            <div class="panel-body">
                <form name="form-filtro" id="form-filtro" method="post" action="{{URL::to('servicos/recibo')}}#pedidos" class="clearfix">
                    {{csrf_field()}}
                    @include('servicos.recibo.serventia-recibo-filtro')
                </form>
                @include('servicos.recibo.serventia-recibo-historico')
            </div>
        </div>
    </div>
    <div id="recibo-notificacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recibo da notificação em lote<span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    @include('arquivos.arquivos-modais')
@endsection