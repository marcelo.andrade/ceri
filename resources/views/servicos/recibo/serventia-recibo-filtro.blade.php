<fieldset class="clearfix">
    <legend>Filtro</legend>
    <div class="fieldset-group clearfix">
        <div class="col-md-3">
            @php
                if ($request->parar_pesquisa == ''  )
                {
                    $parar_pesquisa = 'N';
                }else{
                    $parar_pesquisa = $request->parar_pesquisa;
                }

            @endphp
            <input type="hidden" id="parar_pesquisa" name="parar_pesquisa" value="{{$parar_pesquisa}}">
            <fieldset>
                <legend>Período</legend>
                <div class="col-md-12">
                    <div class="periodo input-group input-daterange">
                        <input type="text" class="form-control pull-left " name="dt_ini" rel="periodo" value="{{$request->dt_ini}}" readonly/>
                        <span class="input-group-addon small pull-left">até</span>
                        <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo" value="{{$request->dt_fim}}" readonly/>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Protocolo</legend>
                <div class="col-md-12">
                    <div class="protocolo">
                        <input type="text" name="protocolo" class="form-control numero_protocolo" value="{{$request->protocolo}}" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Nº do contrato</legend>
                <div class="col-md-12">
                    <input type="text" name="contrato" class="form-control" value="{{$request->contrato}}" />
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Protocolo interno</legend>
                <div class="col-md-12">
                    <div class="protocolo_interno">
                        <input type="text" name="protocolo_interno" class="form-control" value="{{$request->protocolo_interno}}" />
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="col-md-3">
            <fieldset>
                <legend>Grupo de alerta</legend>
                <div class="col-md-12">
                    <select name="grupo[]" class="grupo form-control selectpicker pull-left" multiple data-live-search="true" data-actions-box="true" data-select-all-text="Selecionar todos" data-deselect-all-text="Deselecionar todos" data-count-selected-text="{0} grupo(s) selecionado(s)" data-selected-text-format="count>4" title="Selecione um grupo">
                        @if(count($alienacao_regras)>0)
                            @foreach ($alienacao_regras as $key => $grupo) {
                                <option value="{{$key}}" @if(count($request->grupo)>0) @if(in_array($key,$request->grupo)) selected @endif @endif>{{$alienacao_titulos[$key]['grupo']}}</option>
                            @endforeach
                        @endif
                        <option value="observacoes" @if(count($request->grupo)>0) @if(in_array('observacoes',$request->grupo)) selected @endif @endif>Observações</option>
                    </select>
                </div>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset>
                <legend>Alerta</legend>
                <div class="col-md-12">
                    <select name="filtro[]" class="filtro form-control selectpicker pull-left" multiple data-live-search="true" data-actions-box="true" data-select-all-text="Selecionar todos" data-deselect-all-text="Deselecionar todos" data-count-selected-text="{0} filtros(s) selecionado(s)" data-selected-text-format="count>4" title="Selecione um alerta" @if(count($request->grupo)<=0) disabled="" @endif>
                        @if(count($request->grupo)>0)
                            @foreach($request->grupo as $grupo)
                                @if(!in_array('observacoes',$request->grupo))
                                    <optgroup label="{{$alienacao_titulos[$grupo]['grupo']}}">
                                        @if(isset($alienacao_regras[$grupo]))
                                            @if(count($alienacao_regras[$grupo])>0)
                                                @foreach ($alienacao_regras[$grupo] as $key => $alerta) {
                                                    <?php
                                                    switch ($alienacao_alcadas[$grupo][$key]) {
                                                        case 2:
                                                            $classe = 'cartorio';
                                                            break;
                                                        case 8:
                                                            $classe = 'cef';
                                                            break;
                                                        default:
                                                            $classe = '';
                                                            break;
                                                    }
                                                    ?>
                                                    <option value="{{$grupo.'_'.$key}}" class="{{$classe}}" @if(count($request->filtro)>0) @if(in_array($grupo.'_'.$key,$request->filtro)) selected @endif @endif>{{$alienacao_titulos[$grupo][$key]}}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </optgroup>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </fieldset>
        </div>
        <div class="col-md-2">
            <fieldset>
                <legend>Total por página</legend>
                <div class="col-md-12">
                    <select name="total_paginacao" class="form-control pull-left">
                        <option value="10" @if($request->total_paginacao==10) selected="selected" @endif>10</option>
                        <option value="50" @if($request->total_paginacao==50) selected="selected" @endif>50</option>
                        <option value="100" @if($request->total_paginacao==100) selected="selected" @endif>100</option>
                        <option value="500" @if($request->total_paginacao==500) selected="selected" @endif>500</option>
                        <option value="-1" @if($request->total_paginacao==-1) selected="selected" @endif>Todas</option>
                    </select>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Data última movimentação</legend>
                <div class="col-md-12">
                        <input type="text" class="data form-control pull-left" name="dt_ult_mov" value="{{$request->dt_ult_mov}}" readonly/>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="buttons col-md-12 text-right">
            <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar filtros" />
            <input type="submit" class="btn btn-success" value="Filtrar notificações" name="btnFiltrarNotificacoes" id="btnFiltrarNotificacoes"/>
        </div>
    </div>
</fieldset>