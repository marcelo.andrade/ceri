<div class="erros-credor alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados do credor</legend>
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label class="small">Documento identificador <span>(CNPJ)</span></label>
                <input type="text" name="nu_cpf_cnpj" class="form-control" data-mask="99.999.999/9999-99" />
            </div>
            <div class="form-group col-md-6">
                <label class="small">Nome do credor</label>
                <input type="text" name="no_credor" class="form-control" />
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group col-md-12">
                <label class="small">Agência</label>
                <select name="id_agencia" class="form-control pull-left">
                    <option value="0">Selecione</option>
                    @if(count($agencias)>0)
                        @foreach ($agencias as $agencia)
                            <option value="{{$agencia->id_agencia}}" >{{$agencia->codigo_agencia}} - {{$agencia->no_agencia}}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>

    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereço do credor</legend>
        <div class="col-md-12">
            <div class="form-group clearfix">
                <div class="col-md-10">
                    <label class="small">Endereço</label>
                    <input type="text" name="no_endereco" class="form-control" />
                </div>
                <div class="col-md-2">
                    <label class="small">Número</label>
                    <input type="text" name="nu_endereco" class="form-control" />
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Complemento</label>
                    <input type="text" name="no_complemento" class="form-control" />
                </div>
                <div class="col-md-6">
                    <label class="small">Bairro</label>
                    <input type="text" name="no_bairro" class="form-control" />
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control">
                        <option value="0">Selecione</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">CEP</label>
                    <input type="text" name="nu_cep" class="form-control" data-mask="99999-999" />
                </div>
            </div>
        </div>
    </fieldset>
</div>