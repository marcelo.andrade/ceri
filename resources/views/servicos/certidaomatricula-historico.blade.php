<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th>Protocolo</th>
                <th>Data do pedido</th>
                <th>Cartório</th>
                <th>Status</th>
                <th>Período de disponibilização</th>
                <th>Ações</th>                    
            </tr>
        </thead>
        <tbody>
        	@if (count($todas_certidoes)>0)
                @foreach ($todas_certidoes as $certidao)
                    <tr {{@marcar_linha_vermelha(6,$certidao->pedido->id_pedido, $notificacao)}}>
                        <td>{{$certidao->pedido->protocolo_pedido}} {{@mostrar_sino(6,$certidao->pedido->id_pedido, $notificacao)}}</td>
                        <td>{{$certidao->pedido->dt_pedido}}</td>
                        <td>{{$certidao->pedido->serventias[0]->no_serventia}}</td>
                		<td>{{$certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                        <td>{{ explode(" ", $certidao->pedido->dt_pedido)[0] }}  a {{\Carbon\Carbon::parse($certidao->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                   		<td class="options">
                        	<div class="btn-group">
                                <button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Ações <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu black">
                                    <li><a href="#" data-toggle="modal" data-target="#detalhes-certidao" data-idpedido="{{$certidao->pedido->id_pedido}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}">Ver detalhes</a></li>
                                    @if ($certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto=='Finalizado')
                                        <li><a href="#" data-toggle="modal" data-target="#resultado-certidao" data-idpedido="{{$certidao->pedido->id_pedido}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}">Ver resultado</a></li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhuma matrícula digital foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>