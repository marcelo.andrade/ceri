@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.produto.pagamento.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Pagamentos pendentes (Prestação de Serviço Anoreg/MS) <span class="small"> / Serviços</span></h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#novo-pagamento">
                    Novo pagamento
                </button>
                @include('servicos.produto-pagamento.geral-produto-pagamento-pendentes')
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Pagamentos efetuados (Prestação de Serviço Anoreg/MS) <span class="small"> / Serviços</span></h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                @include('servicos.produto-pagamento.geral-produto-pagamento-efetuados')
          </div>
        </div>
    </div>
    <div id="novo-pagamento" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Novo pagamento</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}"/></div>
                    <div class="form">
                        <form name="form-novo-pagamento" method="post" action="" class="clearfix"></form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="enviar-pagamento btn btn-success">Enviar pagamento</button>
                </div>
            </div>
        </div>
    </div>
<div id="detalhes-pagamento" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
 <div class="modal-content">
     <div class="modal-header gradient01">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Detalhes do pagamento</h4>
     </div>
     <div class="modal-body">
         <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
         <div class="form"></div>
     </div>
     <div class="modal-footer">
         <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
     </div>
 </div>
</div>
</div>
<div id="novo-comprovante" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01 clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title pull-left">Enviar comprovante de pagamento</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"><form name="form-novo-comprovante" method="post" action="" class="clearfix"></form></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                <button type="button" class="enviar-comprovante btn btn-success">Enviar comprovante</button>
            </div>
        </div>
    </div>
</div>
<div id="detalhes-certidao" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detalhes certidão - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="detalhes-pesquisa" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detalhes da pesquisa - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="detalhe-historico-pagamento" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
 <div class="modal-content">
     <div class="modal-header gradient01">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Detalhes do pagamento - <span></span></h4>
     </div>
     <div class="modal-body">
         <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
         <div class="form"></div>
     </div>
     <div class="modal-footer">
         <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
     </div>
 </div>
</div>
</div>
<div id="arquivo-comprovante" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog modal-lg">
 <div class="modal-content">
     <div class="modal-header gradient01">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Comprovante de pagamento - <span></span></h4>
     </div>
     <div class="modal-body">
         <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
         <div class="form"></div>
     </div>
     <div class="modal-footer">
         <a id="arquivo-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
         <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
     </div>
 </div>
</div>
</div>
<div id="imprimir-recibo-pagamento" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Imprimir recibo de pagamento: <span></span></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

@include('arquivos.arquivos-modais')
@endsection