<div class="panel table-rounded">
    <table id="pedidos-efetuados" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="10%">Protocolo</th>
            <th width="5%">Data do repasse</th>
            <th width="5%">Data do comprovante repasse</th>
            <th width="5%">Quantidade</th>
            <th width="10%">Valor pedido</th>
            <th width="10%">Valor do repasse</th>
            <th width="15%">Situação</th>
            <th width="15%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if (count($pagamentos_efetuados)>0)
            @foreach ($pagamentos_efetuados as $pagamento)
                <tr>
                    <td>{{$pagamento->protocolo_pedido}}</td>
                    <td>{{formatar_data($pagamento->dt_lote)}}</td>
                    <td>{{formatar_data($pagamento->dt_comprovante_pagamento)}}</td>
                    <td>{{str_pad($pagamento->nu_quantidade_lote, 5, "0", STR_PAD_LEFT)}}</td>
                    <td><span class="real">{{$pagamento->va_parcela_pedido}}</span></td>
                    <td><span class="real">{{$pagamento->va_repasse_lote}}</span></td>
                    <td>{{$pagamento->situacao_pagamento_repasse->no_situacao_pagamento_repasse}}</td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhe-historico-pagamento" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idpagamentorepasselote="{{$pagamento->id_pagamento_repasse_lote}}">Detalhes</button>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#imprimir-recibo-pagamento" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idpagamentorepasselote="{{$pagamento->id_pagamento_repasse_lote}}">Emitir recibo</a></li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum pagamento efetuado foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>