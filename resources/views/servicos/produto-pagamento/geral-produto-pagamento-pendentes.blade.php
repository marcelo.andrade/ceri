<div class="panel table-rounded">
    <table id="pedidos-pendentes" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="10%">Protocolo</th>
            <th width="5%">Data do repasse</th>
            <th width="5%">Data do comprovante repasse</th>
            <th width="5%">Quantidade</th>
            <th width="10%">Valor pedido</th>
            <th width="10%">Valor repasse (Emolumento)</th>
            <th width="10%">Valor do repasse (Serviço)</th>
            <th width="10%">Taxa bancária</th>
            <th width="10%">Taxa bancária (Operadora)</th>
            <th width="11%">Situação</th>
            <th width="14%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @if (count($pagamentos_pendentes)>0)
            @foreach ($pagamentos_pendentes as $pagamento)
                <tr>
                    <td>{{$pagamento->protocolo_pedido}}</td>
                    <td>{{formatar_data($pagamento->dt_lote)}}</td>
                    <td>{{formatar_data($pagamento->dt_comprovante_repasse)}}</td>
                    <td>{{str_pad($pagamento->nu_quantidade_lote, 5, "0", STR_PAD_LEFT)}}</td>
                    <td><span class="real">{{$pagamento->va_parcela_pedido}}</span></td>
                    <td><span class="real">{{$pagamento->va_repasse_emolumento}}</span></td>
                    <td><span class="real">{{$pagamento->va_repasse_lote}}</span></td>
                    <td><span class="real">{{$pagamento->va_taxa_bancaria}}</span></td>
                    <td><span class="real">{{$pagamento->va_taxa_bancaria_operadora}}</span></td>
                    <td>{{$pagamento->no_situacao_pagamento_repasse}}</td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhe-historico-pagamento" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idpagamentorepasselote="{{$pagamento->id_pagamento_repasse_lote}}">Detalhes</button>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @if ($pagamento->id_situacao_pagamento_repasse == $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP)
                                        <li><a href="#" data-toggle="modal" data-target="#novo-comprovante" data-idpagamentorepasselote="{{$pagamento->id_pagamento_repasse_lote}}">Enviar comprovante</a></li>
                                    @endif
                                    <li><a href="#" data-toggle="modal" data-target="#imprimir-recibo-pagamento" data-protocolo="{{$pagamento->protocolo_pedido}}" data-idpagamentorepasselote="{{$pagamento->id_pagamento_repasse_lote}}">Emitir recibo</a></li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum pagamento pendente foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>