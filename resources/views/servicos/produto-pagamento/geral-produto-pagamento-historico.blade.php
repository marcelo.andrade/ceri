<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Serviços</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <div class="panel-heading gradient01">
                        <div class="checkbox">
                            <input type="checkbox" name="selecionar_tudo" id="selecionar_tudo" value="S" @if (count($movimentacoes_parcela_repasse)<=0) disabled @endif>
                            <label for="selecionar_tudo" class="small">
                                Selecionar todas
                            </label>
                        </div>
                    </div>
                    <table id="notificacoes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="3%"></th>
                            <th width="10%">Protocolo / Data</th>
                            <th width="15%">Produto</th>
                            <th width="10%">Cidade</th>
                            <th width="15%">Serventia</th>
                            <th width="5%">Status</th>
                            <th width="8%">Total pedido</th>
                            <th width="8%">Total repasse</th>
                            <th width="13%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $serventias = array();
                        ?>
                        @if (count($movimentacoes_parcela_repasse)>0)
                            @foreach ($movimentacoes_parcela_repasse as $movimentacao_parcela_repasse)
                                <?php
                                if (!in_array($movimentacao_parcela_repasse->id_serventia,$serventias)) {
                                    $serventias[$movimentacao_parcela_repasse->id_serventia] = $movimentacao_parcela_repasse->no_serventia;
                                }
                                ?>
                                <tr id="{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}">
                                    <td>
                                        <div class="checkbox">
                                            <input type="checkbox" name="id_movimentacao_parcela_repasse[{{$movimentacao_parcela_repasse->id_pessoa}}][{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}][{{$movimentacao_parcela_repasse->va_total_repasse}}][{{$movimentacao_parcela_repasse->va_total_pedido}}][]" class="id_movimentacao_parcela_repasse" id="id_movimentacao_parcela_repasse_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" data-idserventia="{{$movimentacao_parcela_repasse->id_serventia}}" data-idmovimentacaoparcelarepasse="{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" value="{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" >
                                            <label for="id_movimentacao_parcela_repasse_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" class="small"></label>
                                        </div>
                                        <input type="hidden" id="valor_taxa_calculada_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" value="{{$movimentacao_parcela_repasse->va_taxa_calculada}}">
                                    </td>
                                    <td>{{$movimentacao_parcela_repasse->protocolo_pedido}}  <br>/  {{ formatar_data($movimentacao_parcela_repasse->dt_pedido)}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_produto}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_cidade}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_serventia}}</td>
                                    <td>{{$movimentacao_parcela_repasse->no_situacao_pedido_grupo_produto}}</td>
                                    <td>
                                        <span class="real">{{$movimentacao_parcela_repasse->va_total_pedido}}</span>
                                        <input type="hidden" id="valor_pedido_{{$movimentacao_parcela_repasse->id_movimentacao_parcela_repasse}}" value="{{$movimentacao_parcela_repasse->va_total_repasse}}" />
                                    </td>
                                    <td><span class="real">{{$movimentacao_parcela_repasse->va_total_repasse}}</span></td>
                                    <td class="options">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="@if ($movimentacao_parcela_repasse->id_grupo_produto == 1 ) #detalhes-pesquisa @else #detalhes-certidao @endif" data-protocolo="{{$movimentacao_parcela_repasse->protocolo_pedido}}" data-idpedido="{{$movimentacao_parcela_repasse->id_pedido}}" data-idgrupoproduto="{{$movimentacao_parcela_repasse->id_grupo_produto}}">Detalhes</button>
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">
                                    <div class="single alert alert-danger">
                                        <i class="glyphicon glyphicon-remove"></i>
                                        <div class="mensagem">
                                            Nenhuma produto foi encontrado.
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                @if (count($serventias)>0)
                    <div id="serventias-totais">
                        @foreach ($serventias as $key => $serventia)
                            <div id="serventia_{{$key}}" class="well well-sm gradient01 clearfix">
                                <span class="pull-left nome">{{$serventia}}</span>
                                <div class="pull-right totais">
                                    <label class="quantidade label label-default"><b>Quantidade selecionada:</b> <span>0</span></label>&nbsp;<label class="valor-total label label-primary"><b>Valor total:</b> <span class="real">0</span></label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                    <div id="serventias-total-geral">
                            <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                                <span class="pull-left nome"><b>TOTAL GERAL:</b></span>
                                <div class="pull-right totais">
                                    <label class="quantidade label label-default"><b>Quantidade selecionada:</b>
                                        <span>0</span>
                                    </label>&nbsp;
                                    <label class="valor-total label label-primary"><b>Valor total:</b>
                                        <span class="real">0</span>
                                    </label>
                                </div>
                            </div>
                    </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="serventias-total-geral">
                <div id="serventia-geral" class="well well-sm gradient01 clearfix">
                    <span class="pull-left nome"><b>TAXA BANCÁRIA:</b></span>
                    <div class="pull-right totais">
                        <label class="quantidade label label-default"><b>Quantidade selecionada:</b>
                            <span>0</span>
                        </label>&nbsp;
                        <label class="valor-taxa-bancaria label label-primary"><b>Valor total:</b>
                            <span class="real">0</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
</div>