    <div class="erros alert alert-danger" style="display:none">
        <i class="icon glyphicon glyphicon-remove pull-left"></i>
        <div class="menssagem"></div>
    </div>
	<div class="fieldset-group clearfix">
        <div class="col-md-4">
            <label class="small">Cidade</label>
            <select name="id_cidade" class="form-control pull-left">
                <option value="0">Selecione</option>
                @if(count($cidades)>0)
                	@foreach ($cidades as $cidade)
                    	<option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div id="id_serventia" class="form-group col-md-5">
            <label class="small">Cartório</label>
            <select name="id_serventia" class="form-control pull-left" disabled>
                <option value="0">Selecione</option>
            </select>
        </div>
	</div>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Arquivos do protocolo</legend>
            <div id="arquivos" class="col-md-12">
                <div class="novo_arquivo fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-success btn-file"><span class="fileinput-new">Adicionar arquivo</span><input type="file" name="no_arquivo[]"></span>
                </div>
            </div>
        </fieldset>
	</div>