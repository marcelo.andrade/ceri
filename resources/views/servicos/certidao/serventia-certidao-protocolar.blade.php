<form name="form-protocolar">
    <div class="erros-protocolo alert alert-danger" style="display:none">
        <i class="icon glyphicon glyphicon-remove pull-left"></i>
        <div class="menssagem"></div>
    </div>
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Protocolar</legend>
            <div class="form-group clearfix">
                <input type="hidden" name="id_pedido" class="form-control" value="{{$pedido->id_pedido}}"/>
                <div class="col-md-6">
                    <label class="small">Protocolo Interno</label>
                    <input type="text" name="nu_protocolo_legado" class="form-control" value="{{$pedido->nu_protocolo_legado}}" maxlength="25">
                </div>
                <div class="col-md-6">
                    <label class="small">Data</label>
                    <input type="text" name="dt_protocolo_legado" class="data form-control" value="{{Carbon\Carbon::parse($pedido->dt_protocolo_legado)->format('d/m/Y')}}" >
                </div>
            </div>
        </fieldset>
    </div>
</form>