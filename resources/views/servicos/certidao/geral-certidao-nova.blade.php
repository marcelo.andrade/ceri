<input type="hidden" name="saldo_atual" value="{{Auth::User()->saldo_usuario()}}" />
<input type="hidden" name="certidao_token" value="{{$certidao_token}}" />
<input type="hidden" name="id_pedido_resultado_matricula" value="{{(isset($pedido_resultado_matricula)?$pedido_resultado_matricula->id_pedido_resultado_matricula:0)}}" />
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Inserir serventia</legend>
        <div class="col-md-6">
            <label class="small">Cidade</label>
            <select name="id_cidade" class="form-control pull-left" {{(isset($pedido_resultado_matricula)?'disabled':'')}}>
                @if(isset($pedido_resultado_matricula))
                    <option value="{{$pedido_resultado_matricula->pedido_resultado->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->id_cidade}}">{{$pedido_resultado_matricula->pedido_resultado->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                @else
                    <option value="0">Selecione</option>
                    @if(count($cidades)>0)
                        @foreach ($cidades as $cidade)
                            <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                        @endforeach
                    @endif
                @endif
            </select>
        </div>
        <div id="id_serventia" class="form-group col-md-6">
            <label class="small">Cartório</label>
            <select name="id_serventia" class="form-control pull-left" disabled>
                @if(isset($pedido_resultado_matricula))
                    <option value="{{$pedido_resultado_matricula->pedido_resultado->pedido->pedido_pessoa_atual->pessoa->serventia->id_serventia}}">{{$pedido_resultado_matricula->pedido_resultado->pedido->pedido_pessoa_atual->pessoa->serventia->no_serventia}}</option>
                @else
                    <option value="0">Selecione</option>
                @endif
            </select>
        </div>
    </fieldset>
</div>
<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Tipo de certidão</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Tipo de certidão</label>
                        <select name="id_produto" class="form-control pull-left">
                            <option value="0">Selecione</option>
                            @if(count($produtos)>0)
                                @foreach ($produtos as $produto)
                                    <?php
                                    if(isset($pedido_resultado_matricula) and in_array($produto->codigo_produto,array(2007,2008))) {
                                        $disabled_produto = 'disabled';
                                    } else {
                                        $disabled_produto = '';
                                    }
                                    ?>
                                    <option value="{{$produto->id_produto}}" data-precificacao="{{$produto->produto_itens[0]->in_precificacao}}" data-totalsub="{{count($produto->produto_itens)}}" data-codigo="{{$produto->codigo_produto}}" {{$disabled_produto}}>{{$produto->abv_produto}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div id="id_produto_item" class="form-group" style="display:none">
                        <label class="small">Item da certidão</label>
                        <select name="id_produto_item" class="form-control pull-left">
                            <option value="0">Selecione</option>
                        </select>
                    </div>
                    <br/><br/>
                    <div id="alert-pesquisa" class="form-group clearfix" style="display:none">
                        <div class="alert alert-info small single">
                            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                            <div class="menssagem">Atenção! É recomendável que o usuário faça uma pesquisa eletrônica antes do pedido da certidão para constatar se existe ou não imóveis no nome do pesquisado. Somente após a pesquisa será possível o usuário saber se o tipo de certidão será "Positiva de Bens"(Quando é encontrada matrícula no nome do pesquisado)  ou "Negativa de Bens"(Quando não é encontrada matrícula no nome do pesquisado).</div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Pesquisa</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        @if(count($tipos_chave)>0)
                            @foreach ($tipos_chave as $tipo_chave)
                                <div class="radio radio-inline">
                                    @if(isset($pedido_resultado_matricula))
                                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" value="{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" data-tipo="{{$tipo_chave->tp_chave_pesquisa}}" {{($tipo_chave->tp_chave_pesquisa=='1'?'disabled':'checked')}}>
                                    @else
                                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" value="{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" data-tipo="{{$tipo_chave->tp_chave_pesquisa}}">
                                    @endif
                                    <label for="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" class="small">
                                        @if(isset($pedido_resultado_matricula) && $tipo_chave->id_tipo_certidao_chave_pesquisa == 4 && $pedido_resultado_matricula->in_transcricao == 'S')
                                            {{'Transcrição'}}
                                        @else
                                            {{$tipo_chave->no_chave_pesquisa}}
                                        @endif
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="small">Chave de pesquisa <span></span></label>
                        @if(isset($pedido_resultado_matricula))
                            <input type="text" name="de_chave_certidao" class="form-control" readonly value="{{($pedido_resultado_matricula->id_matricula>0?$pedido_resultado_matricula->matricula->codigo_matricula:$pedido_resultado_matricula->codigo_matricula)}}" />
                        @else
                            <input type="text" name="de_chave_certidao" class="form-control" disabled="disabled" />
                        @endif
                    </div>
                    <div id="chave-complementar" class="form-group" style="display:none">
                        <label class="small"></label>
                        <input type="text" name="de_chave_complementar" maxlength="150" class="form-control somente-texto" autocomplete="off" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <label class="small">Observação <span></span></label>
                            <textarea name="ds_observacao" id="ds_observacao" class="form-control" maxlength="250"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="alert alert-info small single">
                            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                            <div class="menssagem">Você digitou <span class="total">0</span> caractere(s) dos 250 possíveis. <span class="limite"></span></div>
                        </div>
                    </div>

                    <div id="msg-homonimia" class="alert alert-success single" style="display: none"><b>Atenção:</b> Resultado não exclui casos de homonímia.</div>
                </div>
            </fieldset>
        </div>
        
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_certidao_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}">
                                <label for="id_tipo_custa_certidao_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            <div id="numero-processo" class="fieldset-group clearfix" style="display:none">
                <fieldset>
                    <legend>Número do processo</legend>
                    <div class="col-md-12">
                        <input type="text" name="numero_processo" class="form-control" maxlength="50"/>
                    </div>
                </fieldset>
            </div>
            <div id="msg-isencao" class="fieldset-group clearfix" style="display:none">
                <div class="alert alert-danger single">
                    Para que o processamento da Certidão se dê com AJG será necessário anexar o despacho de deferimento ou escolha outra opção para dar continuidade à solicitação.
                </div>
            </div>
            <div id="documento-isencao" class="fieldset-group clearfix" style="display:none">
                <fieldset>
                    <legend>Documento de isenção de emolumentos</legend>
                    <div id="arquivos-isencao" class="col-md-12">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="20" data-token="{{$certidao_token}}" data-limite="1">Adicionar arquivo</button>
                    </div>
                </fieldset>
            </div>
        @endif
        <div id="certidao-valores" class="fieldset-group clearfix" style="display:none">
            <fieldset>
                <legend>Composição do valor para a nova certidão (por serventia)</legend>
                <div class="tabela col-md-12">
                    <table class="table table-striped table-fixed table-condensed single">
                        <thead>
                            <th width="60%">Descrição</th>
                            <th width="40%" class="text-right">Valor</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div id="assinaturas-arquivos" class="fieldset-group clearfix" style="display: none">
    <div class="col-md-12">
        <div class="alert alert-warning single">
            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
            <div class="menssagem clearfix">
                <span>Nenhum arquivo foi inserido.</span><br />
                <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$certidao_token}}">Assinar arquivo(s)</a>
            </div>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
	<input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
	<input type="submit" class="btn btn-success pull-right" value="Incluir certidão" />
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Pedidos pendentes (Não enviados à serventia)</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="novos-pedidos" class="table table-striped table-bordered table-fixed small">
                    <thead>
                        <tr class="gradient01">
                            <th width="16%">Protocolo</th>
                            <th width="15%">Data do pedido</th>
                            <th width="13%">Tipo da certidão</th>
                            <th width="20%">Serventia</th>
                            <th width="10%">Validade</th>
                            <th width="10%">Valor</th>
                            <th width="16%">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
		</div>
	</fieldset>
</div>
<div id="msg-ciencia" class="fieldset-group clearfix">
    <div class="alert alert-success single">
        <div class="checkbox">
            <input type="checkbox" name="in_ciencia_certidao_nova" id="in_ciencia_certidao_nova" value="1">
            <label for="in_ciencia_certidao_nova" class="small text-justify">Tenha certeza que o cartório e matrícula/CPF selecionados sejam os corretos. Em pedidos realizados com dados equivocados não haverá estorno dos valores pagos. Na dúvida, realize primeiramente a pesquisa.<br>
                Os pedidos de certidões não enviados ao cartório até às 24h serão cancelados automaticamente.</label>
        </div>
    </div>
</div>
@if(isset($pedido_resultado_matricula))
	<script>
        id_pedido_resultado_matricula = '{{$pedido_resultado_matricula->id_pedido_resultado_matricula}}';
	</script>
@endif