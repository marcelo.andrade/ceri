<?php
$query_string = http_build_query($request->except(['_token','ord']));
?>
@if($class->id_tipo_pessoa == 9)
    @if (count($todas_certidoes)>0)
        <div id="alert" class="alert alert-info">
            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
            <div class="menssagem clearfix" style="overflow:initial;">
                <span class="total">0 certidões selecionadas</span>
                <div class="pull-right">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target="#relatorio-certidao" aria-haspopup="true" aria-expanded="false" disabled>
                            Gerar relatório das selecionadas
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        </div> 
    @endif
@endif
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered table-fixed small">
        <thead>
            <tr class="gradient01">
                @if($class->id_tipo_pessoa == 9)
                    <th width="3%">
                        <div class="checkbox checkbox-primary">
                            <input id="selecionar-todas" class="styled" type="checkbox">
                            <label for="selecionar-todas"></label>
                        </div>
                    </th>
                @endif
                <th width="14%">Protocolo</th>
                    <th width="12%">
                            <?php
                                switch ($request->ord) {
                                    case 'asc':
                                        $ord = 'desc';
                                        $icone = 'fa fa-chevron-up';
                                        break;
                                    case 'desc': default:
                                    $ord = 'asc';
                                    $icone = 'fa fa-chevron-down';
                                    break;
                                }
                            ?>
                            <a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=data&ord={{$ord}}">
                                <span class="pull-left">Data do pedido</span>
                                <i class="pull-right {{$icone}}"></i>
                            </a>
                        </th>
                <th width="10%">Tipo da certidão</th>
                <th width="15%">Solicitante</th>
                <th width="21%">Serventia</th>
                <th width="9%">Status</th>
                <th width="10%">Período de disponibilização</th>
                <th width="13%">Ações</th>
            </tr>
        </thead>
        <tbody>
        	@if (count($todas_certidoes)>0)
                @foreach ($todas_certidoes as $certidao)
                    <tr id="{{$certidao->pedido->id_pedido}}" @if(count($certidao->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
                        @if($class->id_tipo_pessoa == 9)
                            <td>
                                <div class="checkbox checkbox-primary">
                                    <input id="certidao-{{$certidao->pedido->id_pedido}}" class="certidao styled" type="checkbox" value="{{$certidao->pedido->id_pedido}}">
                                    <label for="certidao-{{$certidao->pedido->id_pedido}}"></label>
                                </div>
                            </td>
                        @endif
                        <td>
                            {{$certidao->pedido->protocolo_pedido}}
                            @if(count($certidao->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                        </td>
                        <td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                        <td>{{$certidao->pedido->produto->abv_produto}}</td>
                        <td>{{$certidao->pedido->usuario->no_usuario}} - {{$certidao->pedido->pessoa_origem->no_pessoa}}</td>
                        <td>{{$certidao->pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
                		<td>{{$certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                    	<td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($certidao->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                   		<td class="options">
                            <div class="btn-group" role="group">
                                <?php
                                switch ($certidao->pedido->id_situacao_pedido_grupo_produto) {
                                    case $class::ID_SITUACAO_FINALIZADO:
                                        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#resultado-certidao" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Resultado</button>';
                                        break;
                                    default:
                                        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-certidao" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Detalhes</button>';
                                        break;
                                }
                                ?>
                                @if ($certidao->observacoes_nao_lidas()>0)
                                    <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idcertidao="{{$certidao->id_certidao}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}" title="{{$certidao->observacoes_nao_lidas()}} {{($certidao->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                        {{$certidao->observacoes_nao_lidas()}}
                                    </a>
                                @endif
                                @if(in_array($class->id_tipo_pessoa,[3,4]))
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#recibo-certidao" data-idpedido="{{$certidao->pedido->id_pedido}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}">Visualizar recibo</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#observacoes" data-idcertidao="{{$certidao->id_certidao}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}">Observações</a></li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="8">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhuma certidão foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todas_certidoes)}}</b> de <b>{{$todas_certidoes->total()}}</b> {{($todas_certidoes->total()>1?'certidões':'certidão')}}.
</div>
<div align="center">
    {{$todas_certidoes->fragment('pedidos')->render()}}
</div>
