<?php
$query_string = http_build_query($request->except(['_token','ord']));
?>
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered table-fixed small">
        <thead>
            <tr class="gradient01">
                <th width="16%">Protocolo</th>
				<th width="11%">Matrícula</th>
                <th width="12%">
                    <?php
                    switch ($request->ord) {
                        case 'asc':
                            $ord = 'desc';
                            $icone = 'fa fa-chevron-up';
                            break;
                        case 'desc': default:
                        $ord = 'asc';
                        $icone = 'fa fa-chevron-down';
                        break;
                    }
                    ?>
                    <a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=data&ord={{$ord}}">
                        <span class="pull-left">Data do pedido</span>
                        <i class="pull-right {{$icone}}"></i>
                    </a>
                </th>
                <th width="27%">Solicitante</th>
                <th width="13%">Tipo de certidão</th>
                <th width="10%">Status</th>
                <th width="13%">Ações</th>
            </tr>
        </thead>
        <tbody>
			@if (count($todas_certidoes)>0)
                @foreach ($todas_certidoes as $certidao)
                    <tr id="{{$certidao->pedido->id_pedido}}" @if(count($certidao->pedido->pedido_notificacao)>0) class="linha-notificacao-danger" @endif>
                        <td>
                            {{$certidao->pedido->protocolo_pedido}}
                            @if(count($certidao->pedido->pedido_notificacao)>0)
                                <i class="sino-alerta glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>
                            @endif
                        </td>
                        <td>{{$certidao->de_chave_certidao}}</td>
                        <td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                        <td>{{$certidao->pedido->usuario->no_usuario}} - {{$certidao->pedido->pessoa_origem->no_pessoa}}</td>
                        <td>{{$certidao->pedido->produto->abv_produto}}</td>
                		<td>{{$certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                   		<td class="options">
                            <div class="btn-group" role="group">
                                <?php
                                switch ($certidao->pedido->id_situacao_pedido_grupo_produto) {
                                    case $class::ID_SITUACAO_FINALIZADO:
                                        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#resultado-certidao" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Resultado</button>';
                                        break;
                                    default:
                                        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nova-resposta" data-idpedido="'.$certidao->pedido->id_pedido.'" data-protocolo="'.$certidao->pedido->protocolo_pedido.'">Responder</button>';
                                        break;
                                }
                                ?>
                                @if ($certidao->observacoes_nao_lidas()>0)
                                    <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idcertidao="{{$certidao->id_certidao}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}" title="{{$certidao->observacoes_nao_lidas()}} {{($certidao->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                        {{$certidao->observacoes_nao_lidas()}}
                                    </a>
                                @endif
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-toggle="modal" data-target="#recibo-certidao" data-idpedido="{{$certidao->pedido->id_pedido}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}">Visualizar recibo</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#observacoes" data-idcertidao="{{$certidao->id_certidao}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}">Observações</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#protocolar" data-idpedido="{{$certidao->pedido->id_pedido}}" data-protocolo="{{$certidao->pedido->protocolo_pedido}}" data-nuprotocololegado="{{$certidao->pedido->nu_protocolo_legado}}">Protocolar</a></li>
                                    </ul>
                                 </div>
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="7">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
		                    	Nenhuma certidão foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
			@endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todas_certidoes)}}</b> de <b>{{$todas_certidoes->total()}}</b> {{($todas_certidoes->total()>1?'certidões':'certidão')}}.
</div>
<div align="center">
    {{$todas_certidoes->fragment('pedidos')->render()}}
</div>