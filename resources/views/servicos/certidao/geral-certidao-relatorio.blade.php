<form name="form-salvar-relatorio" action="{{URL::to('servicos/certidao/relatorio/salvar')}}" method="post" target="_blank">
    {{csrf_field()}}
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Certidão</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <div class="panel table-rounded">
                        <table class="table table-striped table-bordered small">
                            <thead>
                            <tr class="gradient01">
                                <th width="10%">Protocolo</th>
                                <th width="10%">Data do pedido</th>
                                <th width="10%">Tipo da certidão</th>
                                <th width="20%">Solicitante</th>
                                <th width="30%">Serventia</th>
                                <th width="10%">Status</th>
                                <th width="10%">Período de disponibilização</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($todas_certidoes)>0)
                                @foreach($todas_certidoes as $certidao)
                                    <tr>
                                        <td><input type="hidden" name="id_certidao[]" class="id_certidao" value="{{$certidao->pedido->id_pedido}}"/>{{$certidao->pedido->protocolo_pedido}}</td>
                                        <td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                                        <td>{{$certidao->pedido->produto->abv_produto}}</td>
                                        <td>{{$certidao->pedido->usuario->no_usuario}} - {{$certidao->pedido->pessoa_origem->no_pessoa}}</td>
                                        <td>{{$certidao->pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</td>
                                        <td>{{$certidao->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                                        <td>{{\Carbon\Carbon::parse($certidao->pedido->dt_pedido)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($certidao->dt_indisponibilizacao)->format('d/m/Y')}}</td>
                                        @endforeach
                                        @endif
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</form>