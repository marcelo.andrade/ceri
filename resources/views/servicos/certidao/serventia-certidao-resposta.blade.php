<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Tipo de certidão</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Tipo de certidão</label>
                        <select name="id_produto" class="form-control pull-left" disabled>
                            <option>{{$pedido->produto->abv_produto}}</option>
                        </select>
                    </div>
                    @if(count($pedido->produto->produto_itens)>1)
                        <div id="id_produto_item" class="form-group">
                            <label class="small">Item da certidão</label>
                            <select name="id_produto_item" class="form-control pull-left" disabled>
                                <option>{{$pedido->produto_itens[0]->abv_produto_item}}</option>
                            </select>
                        </div>
                    @endif
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Pesquisa</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            @if(count($tipos_chave)>0)
                                @foreach ($tipos_chave as $tipo_chave)
                                    <div class="radio radio-inline">
                                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" value="{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" @if($tipo_chave->id_tipo_certidao_chave_pesquisa==$pedido->certidao->id_tipo_certidao_chave_pesquisa) checked="checked" @endif disabled>
                                        <label for="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" class="small">
                                            {{$tipo_chave->no_chave_pesquisa}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="small">Chave de pesquisa <span></span></label>
                            <input type="text" name="de_chave_certidao" class="form-control" disabled="disabled" value="{{$pedido->certidao->de_chave_certidao}}" />
                        </div>
                        @php
                            switch ($pedido->certidao->id_tipo_certidao_chave_pesquisa) {
                                case '1':
                                    echo '<div class="form-group">';
                                    echo '<label class="small">Titular do CPF</label>';
                                    echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->certidao->de_chave_complementar.'" disabled="disabled" />';
                                    echo '</div>';
                                    break;
                                case '2':
                                    echo '<div class="form-group">';
                                    echo '<label class="small">Razão social do CNPJ</label>';
                                    echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->certidao->de_chave_complementar.'" disabled="disabled" />';
                                    echo '</div>';
                                    break;
                            }
                        @endphp
                        <div class="form-group">
                            <label class="small">Observação<span></span></label>
                            <textarea name="ds_observacao" id="ds_observacao" class="form-control" readonly>{{$pedido->certidao->ds_observacao}}</textarea>
                        </div>
                        @if($pedido->certidao->id_tipo_certidao_chave_pesquisa==3)
                            <div id="msg-homonimia" class="alert alert-success single"><b>Atenção:</b> Resultado não exclui casos de homonímia.</div>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($tipo_custa->id_tipo_custa==$pedido->certidao->id_tipo_custa) checked="checked" @endif disabled>
                                <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            @if($pedido->certidao->id_processo>0)
                <fieldset>
                    <legend>Número do processo</legend>
                    <div class="col-md-12">
                        <input type="text" name="numero_processo" class="form-control" disabled="disabled" value="{{$pedido->certidao->processo->numero_processo}}" />
                    </div>
                </fieldset>
	        @endif
            @if(count($arquivos_isencao)>0)
                <div id="documento-isencao" class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Documento de isenção de emolumentos</legend>
                        <div id="arquivos-isencao" class="col-md-12">
                            @foreach($arquivos_isencao as $arquivo)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                    @if($arquivo->in_ass_digital=='S')
                                        <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            @endif
        @endif
    </div>
</div>
@if (count($arquivos_requerimento)>0)
    <div class="resultado fieldset-group clearfix">
        <fieldset class="col-md-12">
            <legend>Requerimento</legend>
            @foreach($arquivos_requerimento as $arquivo)
                <div id="arquivos-resultado" class="col-md-12">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                </div>
            @endforeach
        </fieldset>
    </div>
@endif
<div class="fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Andamento interno</legend>
        <form name="form-novo-andamento" method="post" action="" class="clearfix">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
            <div class="erros-andamento alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-6">
                    <fieldset>
                        <legend>Opções</legend>
                        <div class="col-md-12">
                            @if(count($tipos_resposta)>0)
                                @foreach ($tipos_resposta as $tipo_resposta)
                                    <div class="radio">
                                        <input type="radio" name="id_tipo_resposta" id="tipo_resposta_{{$tipo_resposta->id_tipo_resposta}}" value="{{$tipo_resposta->id_tipo_resposta}}" data-nome="Não encontrado">
                                        <label for="tipo_resposta_{{$tipo_resposta->id_tipo_resposta}}" class="small">
                                            {{$tipo_resposta->no_tipo_resposta}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <legend>Observações</legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="small">Observação</label>
                                <textarea name="de_resposta" class="form-control" rows="4" maxlength="250"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="alert alert-info small single">
                                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                                    <div class="menssagem">Você digitou <span class="total">0</span> caractere(s) dos 250 possíveis. <span class="limite"></span></div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar andamento" />
                </div>
            </div>
        </form>
    </fieldset>
</div>
<div class="resultado fieldset-group clearfix">
    <fieldset class="col-md-12">
        <legend>Resultado da certidão</legend>
        <form name="form-novo-resultado" method="post" action="" class="clearfix" enctype="multipart/form-data">
            <input type="hidden" name="id_pedido" value="{{$pedido->id_pedido}}" />
            <input type="hidden" name="certidao_token" value="{{$certidao_token}}" />
            <div class="erros-resultado alert alert-danger" style="display:none">
                <i class="icon glyphicon glyphicon-remove pull-left"></i>
                <div class="menssagem"></div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-3">
                    <fieldset>
                        <legend>Tipo</legend>
                        <div class="col-md-12">
                            <div class="radio">
                                <input type="radio" name="in_positivo" id="in_positivo_result_S" value="S">
                                <label for="in_positivo_result_S" class="small">
                                    Positivo
                                </label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="in_positivo" id="in_positivo_result_N" value="N">
                                <label for="in_positivo_result_N" class="small">
                                    Negativo
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="resultado-mensagem col-md-9">
                    <fieldset>
                        <legend>Resultado</legend>
                        <div class="col-md-12">
                            <div class="alert alert-warning single">
                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                <div class="menssagem">Selecione um tipo de resultado</div>
                            </div>
                        </div>
                    </fieldset>
                </div>
        		<div class="resultado-positivo col-md-9" style="display:none">
                    <div class="fieldset-group">
                        <fieldset>
                            <legend>
                                <div class="radio">
                                    <input type="radio" name="tipo_resultado_envio" id="digitar" value="1">
                                    <label for="digitar">
                                       <b>Digitar resultado</b>
                                    </label>
                                </div>                           
                            </legend>
                            <div class="col-md-12">
                                <label class="small">Observação</label>
                                <textarea name="de_resultado" class="form-control" rows="3" disabled></textarea>
                            </div>
                        </fieldset>
                    </div>
                    <div class="fieldset-group col-md-12">
                        <fieldset>
                            <legend>
                                <div class="radio">
                                    <input type="radio" name="tipo_resultado_envio" id="upload" value="2">
                                    <label for="upload">
                                       <b>Upload do resultado</b>
                                    </label>
                                </div>
                            </legend>
                            <div class="col-md-12">
                                <div id="arquivos-resultado" class="col-md-12">
                                    <button type="button" class="btn btn-success disabled" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="21" data-token="{{$certidao_token}}" data-limite="0" data-protocolo="{{$pedido->protocolo_pedido}}" disabled>Adicionar arquivo</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="resultado-negativo col-md-9" style="display:none">
                    <fieldset>
                        <legend>Resultado</legend>
                        <div class="col-md-12">
                            <div class="well" style="margin-bottom:0">Não foram localizados registros com a chave <label class="label label-danger label-sm">{{$pedido->certidao->de_chave_certidao}}</label> pesquisada nesta serventia.</div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div id="assinaturas-arquivos" class="fieldset-group clearfix">
                <div class="col-md-12">
                    <div class="alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem clearfix">
                            <span>Nenhum arquivo foi inserido.</span><br />
                            <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$certidao_token}}">Assinar arquivo(s)</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fieldset-group clearfix">
                <div class="col-md-12">
                    <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                    <input type="submit" class="btn btn-success pull-right" value="Salvar resultado" />
                </div>
            </div>
        </form>
    </fieldset>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico da certidão</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($pedido->historico as $historico)
                        	<tr>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-solicitante" data-idusuario="{{$historico->usuario_cad->id_usuario}}" data-nousuario="{{$historico->usuario_cad->no_usuario}}">{{$historico->usuario_cad->no_usuario}}<br/>({{$historico->usuario_pessoa[0]->pessoa->no_pessoa}})</a>
                                </td>
                                <td>{{Carbon\Carbon::parse($historico->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td class="text-wrap">{{$historico->de_observacao}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>