<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Detalhes do solicitante</legend>
        <div class="form-group clearfix">
            <div class="col-md-6">
                <label class="small">Email</label>
                <input type="text" name="matricula_imovel" class="form-control" value="{{$usuario->email_usuario}}" disabled="disabled">
            </div>
            <div class="col-md-6">
                <label class="small">Telefone</label>
                <input type="text" name="matricula_imovel" class="form-control" value="({{trim($usuario->pessoa->pessoa_telefone[0]->telefone->nu_ddd)}}) {{trim($usuario->pessoa->pessoa_telefone[0]->telefone->nu_telefone)}}" disabled="disabled">
            </div>
        </div>
    </fieldset>
</div>