@if(in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Remetente</legend>
            <div class="col-md-6">
                <label class="small">Cidade</label>
                <select name="id_cidade_rem" class="form-control pull-left" disabled>
                    <option>
                        @if(count($pedido->pessoa_origem->enderecos))
                            {{$pedido->pessoa_origem->enderecos[0]->cidade->no_cidade}}
                        @else
                            Não informado
                        @endif
                    </option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label class="small">Pessoa</label>
                <div class="">
                    <select name="id_pessoa_rem" class="form-control pull-left" disabled>
                        <option>{{$pedido->pessoa_origem->no_pessoa}}</option>
                    </select>
                </div>
            </div>
        </fieldset>
    </div>
@endif
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Destinatário</legend>
        <div class="col-md-6">
            <label class="small">Cidade</label>
            <select name="id_cidade" class="form-control pull-left" disabled>
                <option>{{$pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->no_cidade}}</option>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label class="small">Serventia</label>
            <div class="">
                <select name="id_serventia" class="form-control pull-left" disabled>
                    <option>{{$pedido->pedido_pessoa_atual->pessoa->no_pessoa}}</option>
                </select>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group row clearfix">
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Tipo de certidão</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Tipo de certidão</label>
                        <select name="id_produto" class="form-control pull-left" disabled>
                            <option>{{$pedido->produto->abv_produto}}</option>
                        </select>
                    </div>
                    @if(count($pedido->produto->produto_itens)>1)
                        <div id="id_produto_item" class="form-group">
                            <label class="small">Item da certidão</label>
                            <select name="id_produto_item" class="form-control pull-left" disabled>
                                <option>{{$pedido->produto_itens[0]->abv_produto_item}}</option>
                            </select>
                        </div>
                    @endif
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Pesquisa</legend>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            @if(count($tipos_chave)>0)
                                @foreach ($tipos_chave as $tipo_chave)
                                    <div class="radio radio-inline">
                                        <input type="radio" name="id_tipo_certidao_chave_pesquisa" id="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" value="{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" @if($tipo_chave->id_tipo_certidao_chave_pesquisa==$pedido->certidao->id_tipo_certidao_chave_pesquisa) checked="checked" @endif disabled>
                                        <label for="id_tipo_certidao_chave_pesquisa{{$tipo_chave->id_tipo_certidao_chave_pesquisa}}" class="small">
                                            {{$tipo_chave->no_chave_pesquisa}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="small">Chave de pesquisa <span></span></label>
                            <input type="text" name="de_chave_certidao" class="form-control" disabled="disabled" value="{{$pedido->certidao->de_chave_certidao}}" />
                        </div>
                        @php
                        switch ($pedido->certidao->id_tipo_certidao_chave_pesquisa) {
                            case '1':
                                echo '<div class="form-group">';
                                echo '<label class="small">Titular do CPF</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->certidao->de_chave_complementar.'" disabled="disabled" />';
                                echo '</div>';
                                break;
                            case '2':
                                echo '<div class="form-group">';
                                echo '<label class="small">Razão social do CNPJ</label>';
                                echo '<input type="text" name="de_chave_complementar" class="form-control" value="'.$pedido->certidao->de_chave_complementar.'" disabled="disabled" />';
                                echo '</div>';
                                break;
                        }
                        @endphp
                        <div class="form-group">
                            <label class="small">Observação<span></span></label>
                            <textarea name="ds_observacao" id="ds_observacao" class="form-control" readonly>{{$pedido->certidao->ds_observacao}}</textarea>
                        </div>
                        @if($pedido->certidao->id_tipo_certidao_chave_pesquisa==3)
                            <div id="msg-homonimia" class="alert alert-success single"><b>Atenção:</b> Resultado não exclui casos de homonímia.</div>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="col-md-6">
        @if(count($tipos_custa)>0)
            <div class="fieldset-group clearfix">
                <fieldset>
                    <legend>Tipo de custas</legend>
                    <div class="col-md-12">
                        @foreach($tipos_custa as $tipo_custa)
                            <div class="radio">
                                <input type="radio" name="id_tipo_custa" id="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" value="{{$tipo_custa->id_tipo_custa}}" @if($tipo_custa->id_tipo_custa==$pedido->certidao->id_tipo_custa) checked="checked" @endif disabled>
                                <label for="id_tipo_custa_{{$tipo_custa->id_tipo_custa}}" class="small">
                                    {{$tipo_custa->no_tipo_custa}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </fieldset>
            </div>
            @if($pedido->certidao->id_processo>0)
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Número do processo</legend>
                        <div class="col-md-12">
                            <input type="text" name="numero_processo" class="form-control" disabled="disabled" value="{{$pedido->certidao->processo->numero_processo}}" />
                        </div>
                    </fieldset>
                </div>
            @endif
            @if(count($arquivos_isencao)>0)
                <div id="documento-isencao" class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Documento de isenção de emolumentos</legend>
                        <div id="arquivos-isencao" class="col-md-12">
                            @foreach($arquivos_isencao as $arquivo)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                    @if($arquivo->in_ass_digital=='S')
                                        <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            @endif
        @endif
    </div>
</div>
@if(in_array($class->id_tipo_pessoa,array(9,13)))
    <div class="fieldset-group">
        <fieldset>
            <legend>Histórico da certidão</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="historico-pedido" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pedido->historico as $historico)
                            <tr>
                                <td>{{$historico->usuario_cad->no_usuario}}<br/>({{$historico->usuario_pessoa[0]->pessoa->no_pessoa}})</td>
                                <td>{{Carbon\Carbon::parse($historico->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>{{$historico->de_observacao}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </div>
@endif