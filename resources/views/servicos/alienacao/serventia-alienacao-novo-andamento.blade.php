<?php
$in_decurso = false;
$exibe_resultado = false;
if ($alienacao->tempo_decurso()) {
    $in_decurso = true;
    $dt_acao = $alienacao->tempo_decurso()['dt_acao'];
    $dt_diferenca = $alienacao->tempo_decurso()['dias'];
}


switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
	case $class::ID_SITUACAO_FINALIZADO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					A notificação foi finalizada. Para mais informações, confira o histórico de andamentos abaixo.
				</div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_AGUARDANDOINTERACAO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					Aguardando interação do usuário.
				</div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_AGUARDANDOPAGAMENTO:
    case $class::ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
                    @if ( $alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto ==  $class::ID_SITUACAO_CANCELADO )
                        @if ( $alienacao->checar_cancelamento_pagamento() == NULL )
                            Processo Finalizado (Cancelado)
                        @else
                            Aguardando pagamento.
                        @endif
                    @else
                        Aguardando pagamento.
                    @endif
				</div>
			</div>
		</div>
<?php
        break;
    case $class::ID_SITUACAO_CANCELADO:
?>
        <div class="col-md-12">
            <div class="status-atual alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem">
                    Processo Finalizado (Cancelado)
                </div>
            </div>
        </div>
<?php
		break;
	case $class::ID_SITUACAO_DEVOLVIDO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					A notificação foi devolvida ao usuário. Aguardando alterações.
				</div>
			</div>
		</div>
<?php
        break;
    case $class::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO:
?>
        <div class="col-md-12">
            <div class="status-atual alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem">
                    Aguardando aprovação do orçamento.
                </div>
            </div>
        </div>
<?php
        break;
    case $class::ID_SITUACAO_AGUARDANDO_AUTORIZACAO_DECURSO_PRAZO:
?>
        <div class="col-md-12">
            <div class="status-atual alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem">
                    Aguardando aprovação certidão decurso de prazo.
                </div>
            </div>
        </div>
<?php
		break;
	case $class::ID_SITUACAO_EMPROCESSAMENTO:
?>
		@if($in_decurso)

            <div class="col-md-12">
                <div class="status-atual alert alert-info single">
                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                    <div class="menssagem">
                        A notificação está em decurso de prazo até o dia <b>{{$dt_acao->format('d/m/Y')}}</b>, {{($dt_diferenca>1?'restam '.$dt_diferenca.' dias úteis.':'resta  '.$dt_diferenca.' dia útil.')}}
                    </div>
                </div>
            </div>
            <br /><br /><br />
            <div class="col-md-12 clearfix">
                <div class="status-atual alert alert-danger single">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem">
                        {{--<b>Atenção!</b> Caso o devedor compareça no cartório para o pagamento da dívida, entre em contato com a CEF para obtenção do boleto atualizado.--}}

                        <b>Atenção!</b>  Caso o devedor compareça no cartório para o pagamento da dívida, entre em contato com a CEF nestes telefones para emissão do boleto atualizado.
                        <br><br>
                        <b>GIGADCT - GI Gestão  da Adimplência Curitiba/PR</b>
                        <table>
                            <tr>
                                <td><b>Telefone(s):</b></td>
                                <td>(041) 3888-9347</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>(041) 3888-9369</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        @else
            @if($alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->id_pessoa==$class->id_pessoa)
                <form name="form-novo-andamento" method="post" action="">
                    <input type="hidden" name="id_alienacao" value="{{$alienacao->id_alienacao}}" />
                    <input type="hidden" name="andamento_token" value="{{$andamento_token}}" />
                    <div class="erros-andamento alert alert-danger" style="display:none">
                        <i class="icon glyphicon glyphicon-remove pull-left"></i>
                        <div class="menssagem"></div>
                    </div>
                    <div class="fieldset-group clearfix">
                        @if($alienacao->alienacao_pedido->andamento_alienacao[0]->id_resultado_acao>0)
                            <?php
                            $nova_fase = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao->nova_fase;
                            $nova_etapa = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao->nova_etapa;
                            $nova_acao = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao->nova_acao;
                            ?>
                            @if($nova_acao->in_resultado_direto=='N')
                                <input type="hidden" name="in_inserir_resultado" value="N">
                                <div class="form-group col-md-12 clearfix">
                                    <div class="status-atual alert alert-warning single">
                                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                        <div class="menssagem">
                                            <span style="font-size: 13.5px">O próximo andamento será a fase <b>"{{$nova_fase->no_fase}}"</b>, etapa <b>"{{$nova_etapa->no_etapa}}"</b> e ação <b>"{{$nova_acao->no_acao}}"</b>.</span>
                                            @if($nova_acao->tp_alerta_acao>0)
                                                <button type="button" data-toggle="modal" data-target="#info-andamento-acao" data-fase="{{$nova_fase->no_fase}}" data-etapa="{{$nova_etapa->no_etapa}}" data-acao="{{$nova_acao->no_acao}}"  data-dealertaacao="{{$nova_acao->de_alerta_acao}}" class="btn btn-sm-6 btn-primary"><i class="glyphicon glyphicon-question-sign"></i> Infor.</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if($nova_acao->tp_texto_curto_acao>0 or $nova_acao->tp_data_acao>0 or $nova_acao->tp_valor_acao>0)
                                    <div class="form-group clearfix">
                                        @if($nova_acao->tp_texto_curto_acao>0)
                                            <div class="col-md-4">
                                                <label class="small">{{$nova_acao->lb_texto_curto_acao}}</label>
                                                <input type="text" name="de_texto_curto_acao" class="{{($nova_acao->co_acao==2?'numero':'')}} form-control {{($nova_acao->tp_texto_curto_acao>1?'obrigatorio':'')}}" rows="4" title="{{mb_strtolower($nova_acao->lb_texto_curto_acao,'UTF-8')}}" />
                                            </div>
                                        @endif
                                        @if($nova_acao->tp_valor_acao>0)
                                            <div class="col-md-4">
                                                <label class="small">{{$nova_acao->lb_valor_acao}}</label>
                                                <input type="text" name="va_valor_acao" class="real form-control {{($nova_acao->tp_valor_acao>1?'obrigatorio':'')}}" title="{{mb_strtolower($nova_acao->lb_valor_acao,'UTF-8')}}" />
                                            </div>
                                        @endif
                                        @if($nova_acao->tp_data_acao>0)
                                            <div class="col-md-4">
                                                <label class="small">{{$nova_acao->lb_data_acao}}</label>
                                                <input type="text" name="dt_acao" class="data form-control {{($nova_acao->tp_data_acao>1?'obrigatorio':'')}}" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}" title="{{mb_strtolower($nova_acao->lb_data_acao,'UTF-8')}}" />
                                            </div>
                                        @endif
                                    </div>
                                @endif
                                @if($nova_acao->tp_texto_longo_acao>0)
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <label class="small">{{$nova_acao->lb_texto_longo_acao}}</label>
                                            <textarea name="de_texto_longo_acao" class="form-control {{($nova_acao->tp_texto_longo_acao>1?'obrigatorio':'')}}" rows="4" title="{{mb_strtolower($nova_acao->lb_texto_longo_acao,'UTF-8')}}"></textarea>
                                        </div>
                                    </div>
                                @endif
                                @if($nova_acao->tp_upload_acao>0)
                                    <div class="col-md-12">
                                        <div class="form-group clearfix">
                                            <fieldset>
                                                <legend class="small">{{$nova_acao->lb_upload_acao}}</legend>
                                                <div class="col-md-12">
                                                    <div id="arquivos-andamento" class="arquivos {{($nova_acao->tp_upload_acao>1?'obrigatorio':'')}} btn-list" title="{{mb_strtolower($nova_acao->lb_upload_acao,'UTF-8')}}">
                                                        <?php
                                                        if($alienacao->alienacao_pedido->andamento_alienacao[0]->id_resultado_acao>0) {
                                                            switch ($nova_acao->co_acao) {
                                                                case 6:
                                                                case 33:
                                                                case 85:
                                                                    echo '<div class="form-group">
                                                                            <button type="button" class="gerar-arquivo btn btn-info" data-toggle="modal" data-target="#gerar-arquivo-previsualizar" data-token="'.$andamento_token.'" data-idalienacao="'.$alienacao->id_alienacao.'" data-tipo="carta-intimacao" data-token="{{$andamento_token}}" data-idtipoarquivo="22" data-limite="'.$nova_acao->lim_upload_acao.'">Gerar carta de intimação</button>
                                                                            <div class="btn-group">
                                                                                <div class="checkbox">
                                                                                    <input type="checkbox" name="gerar_capa" id="gerar_capa" value="S" >
                                                                                    <label for="gerar_capa" class="small"><b>Incluir capa de processo</b></label>
                                                                                </div>
                                                                                <div class="checkbox">
                                                                                    <input type="checkbox" name="gerar_nota" id="gerar_nota" value="S" >
                                                                                    <label for="gerar_nota" class="small"><b>Incluir nota de diligência</b></label>
                                                                                </div>
                                                                            </div>
                                                                      </div>';
                                                                    break;
                                                                case 15:
                                                                case 41:
                                                                case 80:
                                                                    echo '<button type="button" class="gerar-arquivo btn btn-info" data-toggle="modal" data-target="#gerar-arquivo-previsualizar" data-token="'.$andamento_token.'" data-idalienacao="'.$alienacao->id_alienacao.'" data-tipo="edital-intimacao" data-token="{{$andamento_token}}" data-idtipoarquivo="22" data-limite="'.$nova_acao->lim_upload_acao.'">Gerar edital de intimação</button>';
                                                                    break;
                                                                case 24:
                                                                    echo '<button type="button" class="gerar-arquivo btn btn-info" data-toggle="modal" data-target="#gerar-arquivo-previsualizar" data-token="'.$andamento_token.'" data-idalienacao="'.$alienacao->id_alienacao.'" data-tipo="certidao-decurso" data-token="{{$andamento_token}}" data-idtipoarquivo="22" data-limite="'.$nova_acao->lim_upload_acao.'">Gerar certidão de decurso de prazo</button>';
                                                                    break;
                                                                case 29:
                                                                    echo '<button type="button" class="gerar-arquivo btn btn-info" data-toggle="modal" data-target="#gerar-arquivo-previsualizar" data-token="'.$andamento_token.'" data-idalienacao="'.$alienacao->id_alienacao.'" data-tipo="requerimento-consolidacao" data-token="{{$andamento_token}}" data-idtipoarquivo="22" data-limite="'.$nova_acao->lim_upload_acao.'">Gerar requerimento de consolidação</button>';
                                                                    break;
                                                                case 94:
                                                                    echo '<button type="button" class="gerar-arquivo btn btn-info" data-toggle="modal" data-target="#gerar-arquivo-previsualizar" data-token="'.$andamento_token.'" data-idalienacao="'.$alienacao->id_alienacao.'" data-tipo="edital-intimacao" data-token="{{$andamento_token}}" data-idtipoarquivo="22" data-limite="'.$nova_acao->lim_upload_acao.'" data-arquivonovo="S">Gerar novo edital de intimação</button>';
                                                                    break;
                                                            }
                                                        }
                                                        ?>
                                                        <button type="button" class="novo-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="22" data-token="{{$andamento_token}}" data-limite="{{$nova_acao->lim_upload_acao}}">Adicionar arquivo</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                @endif
                                @if($nova_acao->de_observacao != "")
                                    <?php
                                    switch ($nova_acao->tp_observacao) {
                                        case "A" :
                                            $tp_alerta = 'alert-warning';
                                            $ico_alerta = '<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>';
                                            break;
                                        case "S" :
                                            $tp_alerta = 'alert-success';
                                            $ico_alerta = '<i class="icon glyphicon glyphicon-ok pull-left"></i>';
                                            break;
                                        case "E" :
                                            $tp_alerta = 'alert-danger';
                                            $ico_alerta = '<i class="icon glyphicon glyphicon-remove pull-left"></i>';
                                            break;
                                        case "I" :
                                            $tp_alerta = 'alert-info';
                                            $ico_alerta = '<i class="icon glyphicon glyphicon-info-sign pull-left"></i>';
                                            break;
                                    }
                                    ?>
                                    <div class="form-group clearfix">
                                        <div class="col-md-12">
                                            <div class="alert {{$tp_alerta}} single">
                                                {!!$ico_alerta!!}
                                                <div class="menssagem">{{$nova_acao->de_observacao}}</div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($nova_acao->tp_upload_acao>0)
                                    <div id="assinaturas-arquivos" class="form-group clearfix">
                                        <div class="col-md-12">
                                            <div class="alert alert-warning single">
                                                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                                <div class="menssagem clearfix">
                                                    <span>Nenhum arquivo foi inserido.</span><br />
                                                    <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$andamento_token}}">Assinar arquivo(s)</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <input type="hidden" name="in_inserir_resultado" value="S">
                                <div class="form-group col-md-12 clearfix">
                                    <div class="status-atual alert alert-info single">
                                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                                        <div class="menssagem">
                                            Você está na fase <b>"{{$nova_fase->no_fase}}"</b>, etapa <b>"{{$nova_etapa->no_etapa}}"</b> e ação <b>"{{$nova_acao->no_acao}}"</b>. Selecione um resultado para a ação
                                            @if($nova_acao->tp_alerta_acao>0)
                                            <button type="button" data-toggle="modal" data-target="#info-andamento-acao" data-fase="{{$nova_fase->no_fase}}" data-etapa="{{$nova_etapa->no_etapa}}" data-acao="{{$nova_acao->no_acao}}"  data-dealertaacao="{{$nova_acao->de_alerta_acao}}" class="btn btn-sm-6 btn-primary"><i class="glyphicon glyphicon-question-sign"></i> Sobre</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="col-md-4">
                                        <label class="small">Resultado</label>
                                        <select name="id_resultado_acao" class="form-control">
                                            <option value="0">Selecione</option>
                                            @if(count($nova_acao->resultado_acao)>0)
                                                @foreach($nova_acao->resultado_acao as $resultado)
                                                    <option value="{{$resultado->id_resultado_acao}}">{{$resultado->no_resultado}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div id="resultado-acao">
                                    <div class="carregando text-center" style="display:none"><img src="{{asset('images/loading01.gif')}}" /></div>
                                    <div class="form"></div>
                                </div>                        
                            @endif
                        @else
                            <input type="hidden" name="in_inserir_resultado" value="S">
                            <div class="form-group col-md-12 clearfix">
                                <div class="status-atual alert alert-info single">
                                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                                    <div class="menssagem">
                                        Você está na fase <b>"{{$alienacao->alienacao_pedido->andamento_alienacao[0]->fase_grupo_produto->no_fase}}"</b>, etapa <b>"{{$alienacao->alienacao_pedido->andamento_alienacao[0]->etapa_fase->no_etapa}}"</b> e ação <b>"{{$alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->no_acao}}"</b>. Selecione um resultado para a ação abaixo.
                                        @if($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->tp_alerta_acao>0)
                                            <button type="button" data-toggle="modal" data-target="#info-andamento-acao" data-fase="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->fase_grupo_produto->no_fase}}" data-etapa="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->etapa_fase->no_etapa}}" data-acao="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->no_acao}}"  data-dealertaacao="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->de_alerta_acao}}" class="btn btn-sm-6 btn-primary"><i class="glyphicon glyphicon-question-sign"></i> Sobre</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-4">
                                    <label class="small">Resultado</label>
                                    <select name="id_resultado_acao" class="form-control">
                                        <option value="0">Selecione</option>
                                        @if(count($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->resultado_acao)>0)
                                            @foreach($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->resultado_acao as $resultado)
                                                <option value="{{$resultado->id_resultado_acao}}">{{$resultado->no_resultado}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div id="resultado-acao">
                                <div class="carregando text-center" style="display:none"><img src="{{asset('images/loading01.gif')}}" /></div>
                                <div class="form"></div>
                            </div>
                        @endif
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="col-md-12">
                            {{--<input type="reset" class="btn btn-primary pull-left" value="Cancelar" />--}}
                            <div class="pull-right">
                                <input type="submit" class="btn btn-success" value="{{($alienacao->alienacao_pedido->andamento_alienacao[0]->id_resultado_acao>0?'Iniciar andamento':'Salvar resultado')}}" /> 
                            </div>
                        </div>
                    </div>
                </form>
            @else
                <div class="col-md-12">
                    <div class="status-atual alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem">
                            A notificação está sob responsabilidade da serventia "{{$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->no_pessoa}}". Acompanhe os andamentos no histórico abaixo.
                        </div>
                    </div>
                </div>
            @endif
		@endif
<?php
		break;
	}
?>