<form name="form-salvar-relatorio" action="{{URL::to('servicos/alienacao/relatorio/salvar')}}" method="post" target="_blank">
	{{csrf_field()}}
	<div class="fieldset-group clearfix">
	    <fieldset>
	        <legend>Notificações</legend>
	        <div class="form-group clearfix">
	            <div class="col-md-12">
	                <div class="panel table-rounded">
	                    <table id="parcelas" class="table table-striped table-bordered small">
	                        <thead>
		                        <tr class="gradient01">
		                            <th width="10%">Protocolo</th>
		                            <th width="22%">Protocolo Interno</th>
		                            <th width="8%">Contrato</th>
		                            <th width="15%">Serventia</th>
		                            <th width="10%">Cidade</th>
		                            <th width="5%">Alçada</th>
		                            <th width="15%">Etapa atual</th>
		                            <th width="5%">Dias corridos</th>
		                            <th width="5%">Dias úteis</th>
		                            <th width="5%">Data última movimentação</th>
		                        </tr>
	                        </thead>
	                        <tbody>
								@if(count($alienacoes)>0)
	                                @foreach($alienacoes as $alienacao)
	                                	<tr>
	                                        <td><input type="hidden" name="id_alienacao[]" class="id_alienacao" value="{{$alienacao->id_alienacao}}" />{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
												<td class="text-wrap">
													@if($alienacao->protocolo())
													{{$alienacao->protocolo()->de_texto_curto_acao}}
													@endif
												</td>
						                    <td>{{$alienacao->numero_contrato}}</td>
											<td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}</td>
											<td>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</td>
	                                        <td>
	                                        	<?php
	                                        	switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
	                                        		case 55: case 57: case 67:
	                                        			echo 'Serventia';
	                                        			break;
	                                        		default:
	                                        			echo 'Caixa';
	                                        			break;
	                                        	}
	                                        	?>
	                                        </td>
	                                        <td>{{$alienacao->etapa_atual()}}</td>
	                                        <td>{{$alienacao->tempo_ultimo_andamento()}}</td>
	                                        <td>{{$alienacao->tempo_ultimo_andamento(true)}}</td>
											<td>{{Carbon\Carbon::parse($alienacao->alienacao_pedido->alienacao_andamento_situacao->dt_cadastro)->format('d/m/Y')}}</td>
										</td>
	                                @endforeach
	                            @endif
								</tr>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </fieldset>
	</div>
</form>