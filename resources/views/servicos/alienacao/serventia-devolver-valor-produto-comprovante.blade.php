<div class="fieldset-group clearfix">
    <form name="form-alienacao-devolver-valor" method="post" action="" class="clearfix" enctype="multipart/form-data">
        <input type="hidden" name="id_alienacao_valor_repasse" value="{{$alienacao_valor->id_alienacao_valor_repasse}}"/>
        <input type="hidden" name="alienacao_valor_token" value="{{$alienacao_valor_token}}"/>
        <div class="fieldset-group clearfix">
            <div class="col-md-12">
                <fieldset>
                    <legend>Dados bancários (Devolução)</legend>
                    <div class="col-md-12 clearfix">
                        <div class="col-md-12">
                            <label class="small">Associação dos Notários e Registradores MS (ANOREG)</label>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Código do banco</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->codigo_banco}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-9">
                            <label class="small">Nome banco</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->no_banco}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Número da agência</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->nu_agencia}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Dígito da agência</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->nu_dv_agencia}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Número da conta</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->nu_conta}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Dígito da conta</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->nu_dv_conta}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Tipo de conta</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->tipo_conta}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Variação</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->nu_variacao}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Tipo de pessoa</label>
                            <input type="text" class="form-control" value="{{$banco_pessoa->in_tipo_pessoa_conta}}"
                                   disabled="disabled"/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">CNPJ</label>
                            <input type="text" class="form-control"
                                   value="{{cpf_cnpj($banco_pessoa->nu_cpf_cnpj_conta)}}" disabled="disabled"/>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="fieldset-group clearfix">
            <div class="col-md-12">
                <fieldset>
                    <legend>Custas da notificação</legend>
                    <table id="pedidos-pendentes" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="15%">Data cadastro</th>
                            <th width="15%">Descrição</th>
                            <th width="20%">Data pagamento</th>
                            <th width="10%">Quantidade</th>
                            <th width="20%">Valor</th>
                            <th width="20%">Valor total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{formatar_data($alienacao_valor->dt_cadastro)}}</td>
                            <td>{{$alienacao_valor->no_produto_item}}</td>
                            <td>{{formatar_data($alienacao_valor->dt_repasse)}}</td>
                            <td>{{$alienacao_valor->nu_quantidade}}</td>
                            <td><span class="real">{{$alienacao_valor->va_valor}}</span></td>
                            <td><span class="real">{{$alienacao_valor->va_total}}</span></td>
                        </tr>
                        </tbody>
                    </table>
                </fieldset>
            </div>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Comprovante de pagamento</legend>
                <div class="erros alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Data do pagamento</label>
                        <input type="text" name="dt_pagamento" class="form-control data"
                               value="{{\Carbon\Carbon::now()->format('d/m/Y')}}"/>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Valor pago</label>
                        <input type="text" name="va_pago" class="form-control real"
                               value="{{$alienacao_valor->va_total}}" readonly="readonly"/>
                    </div>
                    <div class="col-md-12">
                        <label class="small">Justificativa</label>
                        <textarea name="de_repasse_devolucao" class="form-control" maxlength="150"></textarea>
                    </div>
                </div>
                <div class="fieldset-group clearfix">
                    <fieldset>
                        <legend>Arquivo comprovante</legend>
                        <div id="arquivos-alienacao-devolver-valor" class="col-md-12">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#novo-arquivo" data-idtipoarquivo="26"
                                    data-token="{{$alienacao_valor_token}}" data-limite="1">Adicionar arquivo
                            </button>
                        </div>
                    </fieldset>
                </div>
                <div id="assinaturas-arquivos" class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="alert alert-warning single">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem clearfix">
                                <span>Nenhum arquivo foi inserido.</span><br/>
                                <a href="#" class="btn btn-warning disabled" data-toggle="modal"
                                   data-target="#nova-assinatura" data-token="{{$alienacao_valor_token}}">Assinar
                                    arquivo(s)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
</div>