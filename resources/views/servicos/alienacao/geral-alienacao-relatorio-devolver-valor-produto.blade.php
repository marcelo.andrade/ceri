<form name="form-salvar-relatorio-devolver-valor-produto" action="{{URL::to('servicos/alienacao/devolucao-valor/relatorio-devolver-valor-produto/salvar')}}" method="post" target="_blank">
	{{csrf_field()}}
	<div class="fieldset-group clearfix">
	    <fieldset>
	        <legend>Notificações</legend>
	        <div class="form-group clearfix">
	            <div class="col-md-12">
	                <div class="panel table-rounded">
	                    <table id="parcelas" class="table table-striped table-bordered small">
	                        <thead>
		                        <tr class="gradient01">
		                            <th>Protocolo</th>
		                            <th>Produto</th>
									@if($class->id_tipo_pessoa == 9)
		                            <th>Serventia</th>
									@endif
		                            <th>Data da devolução</th>
		                            <th>Valor da devolução</th>
		                            <th>Justificativa</th>
		                        </tr>
	                        </thead>
	                        <tbody>
							@forelse($alienacoes_valores_devolvidas as $alienacao_valor)
								<tr>
									<td>
                                        <input type="hidden" name="ids_alienacoes[]" class="alienacao" value="{{$alienacao_valor->alienacao->id_alienacao}}" />
                                        {{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}
                                    </td>
									<td>{{$alienacao_valor->no_produto_item}}</td>
									@if($class->id_tipo_pessoa == 9)
									<td>{{$alienacao_valor->alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}</td>
									@endif
									<td>{{\Carbon\Carbon::parse($alienacao_valor->dt_repasse_devolucao)->format('d/m/Y')}}</td>
									<td><span class="real">{{$alienacao_valor->va_repasse_devolucao}}</span></td>
									<td>{{$alienacao_valor->de_repasse_devolucao}}</td>
								</tr>
							@empty
								<tr>
									<td colspan="5">
										<div class="single alert alert-danger">
											<i class="glyphicon glyphicon-remove"></i>
											<div class="mensagem">
												Nenhuma notificação foi encontrada.
											</div>
										</div>
									</td>
								</tr>
							@endforelse
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </fieldset>
	</div>
</form>