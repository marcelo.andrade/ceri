<input type="hidden" name="alienacao_token" value="{{$alienacao_token}}">
<input type="hidden" name="id_alienacao" value="{{$request->id_alienacao}}">
<div class="erros-endereco alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereço do devedor</legend>
        <div class="col-md-12">
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Endereço</label>
                    <textarea rows="3" name="no_endereco" class="form-control text-uppercase" maxlength="120" placeholder="informe o endereço completo contendo o complemento, bairro, cidade, estado e cep."></textarea>
                </div>
                {{--<div class="col-md-2">
                    <label class="small">Número</label>
                    <input type="text" name="nu_numero" class="form-control" />
                </div>--}}
            </div>
            {{--<div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Complemento</label>
                    <input type="text" name="no_complemento" class="form-control" />
                </div>
                <div class="col-md-6">
                    <label class="small">Bairro</label>
                    <input type="text" name="no_bairro" class="form-control" />
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control">
                        <option value="0">Selecione</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">CEP</label>
                    <input type="text" name="nu_cep" class="form-control" data-mask="99999-999" />
                </div>
            </div>--}}
            <div class="form-group">
                <div class="col-md-12">
                    <button type="button" class="cancelar-endereco btn btn-info pull-left">Cancelar</button>
                    <button type="button" class="inserir-endereco btn btn-info pull-right">Inserir endereço</button>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Endereços</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="novo-enderecos" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th width="85%">Endereço</th>
                            <th width="15%">Ação</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>