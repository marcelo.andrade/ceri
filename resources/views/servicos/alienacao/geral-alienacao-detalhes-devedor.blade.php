<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados do devedor</legend>
        <div class="col-md-12">
            <input type="hidden" name="id_alienacao" class="form-control" value="{{$alienacao->id_alienacao}}" />
            <input type="hidden" name="id_alienacao_devedor" class="form-control" value="{{$devedor->id_alienacao_devedor}}" />
            <div class="form-group">
                <label class="small">Documento identificador <span>{{strlen(limpar_mascara($devedor->nu_cpf_cnpj))==11?'(CPF)':'(CNPJ)'}}</span></label>
                <input type="text" name="nu_cpf_cnpj" class="form-control" disabled="disabled" value="{{$devedor->nu_cpf_cnpj}}" />
            </div>
            <div class="form-group">
                <label class="small">Nome do devedor</label>
                <input type="text" name="no_devedor" class="form-control" maxlength="60" value="{{$devedor->no_devedor}}" />
            </div>
        </div>
    </fieldset>
</div>