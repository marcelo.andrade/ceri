<?php
$pode_alterar = false;
$in_atualizar_alienacao_valor = 0;

if ($request->in_pagamento!='S')
{
	switch ( $alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto )
    {
        case   $class::ID_SITUACAO_AGUARDANDOINTERACAO :
            if ($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->id_acao_etapa==20)
            {
                $pode_alterar = true;
                $in_atualizar_alienacao_valor = 0;
            }
        break;
        case   $class::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO :
            if ($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->id_acao_etapa==9)
            {
                $pode_alterar = true;
                $in_atualizar_alienacao_valor = 1;
            }
        break;
    }
}
?>
<div class="row">
    <div class="pull-right">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#visualizar-arquivo-caixa" data-idalienacao="{{$alienacao->id_alienacao}}" data-tipo="recibo-caixa" class="btn btn-primary" id="btnImprimir">
            <span class="glyphicon glyphicon-print"></span> Imprimir Recibo Notificação
        </a>
    </div>
</div>
<input type="hidden" name="alienacao_token" value="{{$alienacao_token}}">
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>Destinatário</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">Cidade</label>
                        <select name="id_cidade" class="form-control pull-left" disabled="disabled">
                            <option>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                        </select>
                    </div>
                    <div id="id_serventia" class="col-md-12">
                        <label class="small">Cartório</label>
                        <select name="id_serventia" class="form-control pull-left" disabled="disabled">
                            <option>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Credor</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">Credor</label>
                        <select name="id_credor_alienacao" class="form-control pull-left" disabled>
                            <option>{{$alienacao->credor_alienacao->credor->no_credor}}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="small">Agência</label>
                        <select name="id_agencia" class="form-control pull-left" disabled>
                            <option value="{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}">{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}</option>
                        </select>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-8">
            <fieldset>
                <legend>Dados da alienação</legend>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="small">Número do contrato</label>
                        <input type="text" name="numero_contrato" class="form-control " value="{{$alienacao->numero_contrato}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor da dívida</label>
                        <input type="text" name="va_divida" class="form-control real" value="{{$alienacao->va_divida}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do leilão</label>
                        <input type="text" name="va_leilao" class="form-control real" value="{{$alienacao->va_leilao}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Código legado</label>
                        <input type="text" name="id_legado" class="form-control " value="{{$alienacao->id_legado}}" disabled />
                    </div>
                </div>

            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset>
                <legend>Dados do imóvel</legend>
                <div id="matricula_imovel" class="col-md-12">
                    <label class="small">Matrícula do imóvel</label>
                    <div class="input-group">
                        <input type="text" name="matricula_imovel" class="form-control disabled" readonly value="{{$alienacao->matricula_imovel}}" />
                        <div class="input-group-btn">
                            <button type="button" class="detalhes-imovel btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-imovel" data-idalienacao="{{$alienacao->id_alienacao}}" data-matriculaimovel="{{$alienacao->matricula_imovel}}">Detalhes do imóvel</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Projeção</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="small">Valor do período (1 a 15 dias)</label>
                        <input type="text" name="va_periodo_01" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_01}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do período (16 a 30 dias)</label>
                        <input type="text" name="va_periodo_02" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_02}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do período (31 a 45 dias)</label>
                        <input type="text" name="va_periodo_03" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_03}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do período (46 a 60 dias)</label>
                        <input type="text" name="va_periodo_04" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_04}}" disabled/>
                    </div>
                </div>
            </div>
            @if($alienacao->dt_encaminhamento())
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="small">Vencimento 1º período</label>
                            <input type="text" name="va_vencimento_periodo_01" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_1'])->format('d/m/Y')}}" disabled />
                        </div>
                        <div class="col-md-3">
                            <label class="small">Vencimento 2º período</label>
                            <input type="text" name="va_vencimento_periodo_02" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_2'])->format('d/m/Y')}}" disabled/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Vencimento 3º período</label>
                            <input type="text" name="va_vencimento_periodo_03" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_3'])->format('d/m/Y')}}" disabled/>
                        </div>
                        <div class="col-md-3">
                            <label class="small">Vencimento 4º período</label>
                            <input type="text" name="va_vencimento_periodo_04" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_4'])->format('d/m/Y')}}" disabled/>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Devedores</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="devedores" class="table table-striped table-bordered small">
                        <thead>
                            <tr class="gradient01">
                                <th width="20%">CPF/CNPJ</th>
                                <th width="70%">Nome</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($alienacao->alienacao_devedor)>0)
                                @foreach($alienacao->alienacao_devedor as $devedor)
                                	<tr>
                                        <td>{{$devedor->nu_cpf_cnpj}}</td>
                                        <td>{{$devedor->no_devedor}}</td>
									</tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereços de cobrança</legend>
        @if($pode_alterar)
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <button type="button" class="novo-endereco btn btn-success pull-left" data-toggle="modal" data-target="#novo-endereco" data-alienacaotoken="{{$alienacao_token}}" data-idalienacao="{{$alienacao->id_alienacao}}">Novo endereço</button>
                </div>
            </div>
		@endif
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="enderecos" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="90%">Endereço</th>
                            @if($pode_alterar || ($class->id_tipo_pessoa==8 and in_array($alienacao->verifica_acao_etapa(),[1,81])))
	                            <th width="10%">Ações</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($alienacao->endereco_alienacao)>0)
                            	<?php  $i=0; ?>
                                @foreach($alienacao->endereco_alienacao as $endereco)
                                    <?php  $disabled  = ($i == 0 ) ? 'disabled' : ''; ?>
                                	<tr id="{{$endereco->id_endereco_alienacao}}">
                                        <td>
                                            <?php //{{$endereco->no_endereco}}{{($endereco->nu_numero?' Nº '.$endereco->nu_numero:'')}}{{($endereco->no_complemento?', '.$endereco->no_complemento:'')}}{{($endereco->no_bairro?' - '.$endereco->no_bairro:'')}}. {{$endereco->cidade->no_cidade}}{{($endereco->nu_cep?' - '.$endereco->nu_cep:'')}} ?>
                                            {{$endereco->no_endereco}}
                                            <span style="font-size: 10px;float: right; display: none;"></span>
                                        </td>
                                        @if($pode_alterar)
                                        <td class="options">
                                            <a href="#" class="remover-endereco btn btn-black btn-sm {{$disabled}}" data-in_atualiza_alienacao_valor="{{$in_atualizar_alienacao_valor}}" data-linha="{{$endereco->id_endereco_alienacao}}" data-idalienacao="{{$alienacao->id_alienacao}}">Remover</a>
                                        </td>
                                        @elseif($class->id_tipo_pessoa==8 && in_array($alienacao->verifica_acao_etapa(),[1,81]))
                                        <td>
                                            <a href="#" class="{{count($alienacao->endereco_alienacao)==1?'disabled':'remover-endereco-duplicado'}} btn btn-black btn-sm  pull-right" data-linha="{{$endereco->id_endereco_alienacao}}" data-idalienacao="{{$alienacao->id_alienacao}}">Remover</a>
                                        </td>
                                        @endif
									</tr>
                                    <?php
									$i++;
									?>
                                @endforeach
                                @if(count($alienacao->endereco_alienacao_excluido)>0)
                                    @foreach($alienacao->endereco_alienacao_excluido as $endereco_excluido)
                                    <tr id="{{$endereco_excluido->id_endereco_alienacao}}" style="color:red">
                                        <td>
                                            {{$endereco_excluido->no_endereco}}
                                            <span style="font-size: 10px; float: right">Excluído por {{$endereco_excluido->usuario_excluido->no_usuario}} <br> em {{\Carbon\Carbon::parse($endereco_excluido->dt_excluido)->format('d/m/Y H:i')}}</span>
                                        </td>
                                        @if($class->id_tipo_pessoa==8 && in_array($alienacao->verifica_acao_etapa(),[1,81]))
                                        <td>
                                            <a href="#" class="disabled btn btn-danger btn-sm  pull-right" data-linha="{{$endereco_excluido->id_endereco_alienacao}}" data-idalienacao="{{$alienacao->id_alienacao}}">Excluído</a>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                @endif
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
@if(count($alienacao->arquivos_grupo)>0)
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Arquivos</legend>
            <div class="col-md-12">
                <div id="arquivos" class="col-md-12">
                    @foreach($alienacao->arquivos_grupo as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                            @if($arquivo->in_ass_digital=='S')
                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </fieldset>
    </div>
@endif
@if(array_intersect(array(8),$class->modulos))
    @if ($request->in_pagamento!='S')
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Andamento</legend>
                <div id="novo-andamento">
                    <div class="carregando text-center" style="display:none"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
            </fieldset>
            <script>
                $(document).ready(function(e) {
                    carrega_andamento(<?=$alienacao->id_alienacao;?>);
                });
            </script>
        </div>
    @endif
@endif
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico de intimação</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Fase</th>
                            <th>Etapa</th>
                            <th>Ação</th>
                            <th>Resultado</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($alienacao->alienacao_pedido->andamento_alienacao as $andamento)
                        	<tr>
                                <td>{{$andamento->fase_grupo_produto->no_fase}}</td>
                                <td>{{$andamento->etapa_fase->no_etapa}}</td>
                                <td>{{$andamento->acao_etapa->no_acao}}</td>
                                <td>
                                	@if($andamento->resultado_acao)
                                    	<label class="label label-success">{{$andamento->resultado_acao->no_resultado}}</label>
                                    @else
	                                	<label class="label label-warning">Aguardando</label>
                                    @endif
                                </td>
                                <td>{{Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>
                                	<a href="#" class="detalhes-andamento btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-andamento" data-idandamento="{{$andamento->id_andamento_alienacao}}" data-nofase="{{$andamento->fase_grupo_produto->no_fase}}" data-noetapa="{{$andamento->etapa_fase->no_etapa}}" data-noacao="{{$andamento->acao_etapa->no_acao}}">Ver detalhes</a></li>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
@if(count($alienacao->alienacao_pedido->andamento_alienacao_excluido) > 0)
    <div class="fieldset-group">
        <fieldset>
            <legend>Histórico de remoções de interações</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="historico-pedido" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th>Fase</th>
                            <th>Etapa</th>
                            <th>Ação</th>
                            <th>Resultado</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alienacao->alienacao_pedido->andamento_alienacao_excluido as $andamento)
                            <tr>
                                <td>{{$andamento->fase_grupo_produto->no_fase}}</td>
                                <td>{{$andamento->etapa_fase->no_etapa}}</td>
                                <td>{{$andamento->acao_etapa->no_acao}}</td>
                                <td>
                                    @if($andamento->resultado_acao)
                                        <label class="label label-success">{{$andamento->resultado_acao->no_resultado}}</label>
                                    @else
                                        <label class="label label-warning">Aguardando</label>
                                    @endif
                                </td>
                                <td>{{Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="#" class="detalhes-andamento btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-andamento-excluido" data-idandamento="{{$andamento->id_andamento_alienacao_excluido}}" data-nofase="{{$andamento->fase_grupo_produto->no_fase}}" data-noetapa="{{$andamento->etapa_fase->no_etapa}}" data-noacao="{{$andamento->acao_etapa->no_acao}}">Ver detalhes</a></li>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </div>
@endif