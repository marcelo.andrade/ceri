<?php
$matricula_imovel = '';
$registro_imovel = '';
$no_endereco = '';
$nu_numero = '';
$no_complemento = '';
$no_bairro = '';
$id_cidade = '';
$nu_cep = '';
if($request->session()->has('imovel_'.$alienacao_token)) {
    $imovel = $request->session()->get('imovel_'.$alienacao_token);
    $no_endereco = $imovel['no_endereco'];
    $matricula_imovel = $imovel['matricula_imovel'];
    /*$registro_imovel = $imovel['registro_imovel'];
    $nu_numero = $imovel['nu_numero'];
    $no_complemento = $imovel['no_complemento'];
    $no_bairro = $imovel['no_bairro'];
    $id_cidade = $imovel['id_cidade'];
    $nu_cep = $imovel['nu_cep'];*/
}
?>
<input type="hidden" name="alienacao_token" value="{{$alienacao_token}}">
<div class="erros-imovel alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados do imóvel</legend>
        <div class="form-group clearfix">
            <div class="col-md-6">
                <label class="small">Matrícula do imóvel</label>
                <input type="text" name="matricula_imovel" class="form-control" value="{{$matricula_imovel}}">
            </div>
            {{--<div class="col-md-6">
                <label class="small">Registro do imóvel</label>
                <input type="text" name="registro_imovel" class="form-control" value="{{$registro_imovel}}">
            </div>--}}
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereço do imóvel</legend>
        <div class="col-md-12">
            <div class="form-group clearfix">
                <label class="small">Endereço</label>
                <textarea class="form-control text-uppercase" name="no_endereco">{{$no_endereco}}</textarea>
                {{--<div class="col-md-2">
                    <label class="small">Número</label>
                    <input type="text" name="nu_numero" class="form-control" value="{{$nu_numero}}" />
                </div>--}}
            </div>
            {{--<div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Complemento</label>
                    <input type="text" name="no_complemento" class="form-control" value="{{$no_complemento}}" />
                </div>
                <div class="col-md-6">
                    <label class="small">Bairro</label>
                    <input type="text" name="no_bairro" class="form-control" value="{{$no_bairro}}" />
                </div>
            </div>--}}
            {{--<div class="form-group clearfix">
                <div class="col-md-6">
                    <label class="small">Cidade</label>
                    <select name="id_cidade" class="form-control">
                        <option value="0">Selecione</option>
                        @if(count($cidades)>0)
                            @foreach ($cidades as $cidade)
                                <option value="{{$cidade->id_cidade}}" @if($id_cidade==$cidade->id_cidade)selected="selected"@endif>{{$cidade->no_cidade}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="small">CEP</label>
                    <input type="text" name="nu_cep" class="form-control" data-mask="99999-999" value="{{$nu_cep}}" />
                </div>
            </div>--}}
        </div>
    </fieldset>
</div>