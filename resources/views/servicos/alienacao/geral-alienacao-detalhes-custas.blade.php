<?php
    $legenda = '';
?>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Custas da notificação</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    @if ( $class->id_tipo_pessoa == $class::ID_TIPO_PESSOA_ANOREG )
                        <table id="parcelas" class="table table-striped table-bordered small">
                            <thead>
                            <tr class="gradient01">
                                <th width="10%">Data cadastro</th>
                                <th width="30%">Descrição</th>
                                <th width="10%">Data pagamento</th>
                                <th width="10%">Data repasse</th>
                                <th width="10%">Quantidade</th>
                                <th width="20%">Valor</th>
                                <th width="20%">Valor Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(count($alienacao_valor)>0)
                                    @foreach($alienacao_valor as $custa)
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($custa->dt_cadastro)->format('d/m/Y')}}</td>
                                            <td>{{$custa->produto_item->no_produto_item}}</td>
                                            <td>
                                                @if ($custa->dt_pagamento == NULL)
                                                    -
                                                @else
                                                    {{\Carbon\Carbon::parse($custa->dt_pagamento)->format('d/m/Y') }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($custa->dt_repasse == NULL)
                                                    -
                                                @else
                                                    {{\Carbon\Carbon::parse($custa->dt_repasse)->format('d/m/Y') }}
                                                @endif
                                            </td>
                                            <td>{{$custa->nu_quantidade}}</td>
                                            <td class="real">{{$custa->va_valor}}</td>
                                            <td class="real">{{$custa->va_total}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    @else
                        <table id="parcelas" class="table table-striped table-bordered small">
                            <thead>
                            <tr class="gradient01">
                                <th width="10%">Data cadastro</th>
                                <th width="30%">Descrição</th>
                                <th width="10%">Data pagamento</th>
                                <th width="10%">Quantidade</th>
                                <th width="20%">Valor</th>
                                <th width="20%">Valor Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($alienacao_valor)>0)
                                @foreach($alienacao_valor as $custa)
                                    @if ($custa->produto_item->no_produto_item != 'Prestação de Serviços ANOREG/MS')
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($custa->dt_cadastro)->format('d/m/Y')}}</td>
                                            <td>{{$custa->produto_item->no_produto_item}}</td>
                                            <td>
                                                @if ($custa->dt_pagamento == NULL)
                                                    -
                                                @else
                                                    {{\Carbon\Carbon::parse($custa->dt_pagamento)->format('d/m/Y') }}
                                                @endif
                                            </td>
                                            <td>{{$custa->nu_quantidade}}</td>
                                            <td class="real">{{$custa->va_valor}}</td>
                                            <td class="real">{{$custa->va_total}}</td>
                                        </tr>
                                    @endif
                                    <?php
                                        $legenda = $custa->legenda;
                                    ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
            @if ( $class->id_tipo_pessoa == $class::ID_TIPO_PESSOA_CAIXA )
                <span><b>{{$legenda}}</b></span>
            @endif
        </div>
    </fieldset>
</div>