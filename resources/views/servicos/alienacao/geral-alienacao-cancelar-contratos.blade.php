@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.cancelamento.js')}}?v=<?=time();?>"></script>

    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.imovel.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.devedor.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.endereco.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.credoralienacao.js')}}?v=<?=time();?>"></script>

    <script type="text/javascript" src="{{asset('js/jquery.funcoes.arquivos.js')}}?v=<?=time();?>"></script>

    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.endereco.andamento.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.observacoes.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.detalhes.andamento.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">

        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Cancelamento e Pesquisa em Lote<span class="small">/ Notificação de alienação fiduciária / Serviços</span></h4>
            </div>
            <div class="panel-body">
                <form name="form-filtro" id="form-filtro-cancelamento" method="post" action="{{URL::to('servicos/alienacao/cancelar-contratos')}}" class="clearfix">
                    {{csrf_field()}}
                    <div class="fieldset-group clearfix">
                        <div class="col-md-6">
                            <fieldset>
                                <legend>Contratos / Protocolos</legend>
                                <div class="col-md-12">
                                    <div class="">
                                        <textarea name="contratos" id="contratos" class="form-control" rows="12">{{$request->contratos}}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <legend>Observações</legend>
                                <ol>
                                    <li>O sistema ira buscar somente notificações que ainda não foram <b>canceladas</b> ou <b>finalizadas.</b></li>
                                    <li><b>Atenção:</b> uma vez o contrato cancelado não poderá mais interagir com o mesmo.</li>
                                    <li>Caso a notificação esteja como <b>aguardando pagamento</b> o sistema irá gerar o requerimento de cancelamento mais continuará com o mesmo status.</li>
                                    <li>
                                        Os números de contratos deveram ficar um embaixo do outro como no exemplo abaixo.
                                        <ul>
                                            <li>123</li>
                                            <li>456</li>
                                            <li>789</li>
                                        </ul>
                                    </li>
                                </ol>
                            </fieldset>
                        </div>
                    </div>
                    <div class="fieldset-group clearfix">
                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar filtros" />
                            <input type="submit" class="btn btn-success" value="Filtrar notificações" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if (count($alienacoes)>0)
            <div id="alert" class="alert alert-info">
                <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                <div class="menssagem clearfix" style="overflow:initial;">
                    <span class="total">0 notificações selecionadas</span>
                    <div class="pull-right">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                                Ações com selecionadas
                            </button>
                            <button type="button" class="btn btn-primary dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                @if(array_intersect(array(8),$modulos))
                                    <li><a href="#" data-toggle="modal" data-target="#cancelamento-notificacoes" disabled>Cancelar contrato das selecionadas<span></span></a></li>
                                @endif
                                <li><a href="#" data-toggle="modal" data-target="#relatorio-notificacoes-pesquisa" class="gerar-relatorio" disabled>Gerar relatório das selecionadas <span></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="panel table-rounded">
            <table id="pedidos-cancelamento" class="table table-striped table-bordered small">
                <thead>
                <tr class="gradient01">
                    <th width="3%">
                        <div class="checkbox checkbox-primary">
                            <input id="selecionar-todas" class="styled" type="checkbox">
                            <label for="selecionar-todas"></label>
                        </div>
                    </th>
                    <th width="12%">Protocolo (CERI)</th>
                    <th width="12%">Contrato</th>
                    <th width="25%">Devedor(es)</th>
                    <th width="7%">Valor</th>
                    <th width="12%">Status</th>
                    <th width="10%">Ações</th>
                </tr>
                </thead>
                <tbody>
                    @if(count($alienacoes)>0)
                        @foreach($alienacoes as $alienacao)
                            <?php
                            $lst_opcoes['aprovar-orcamento'] = '<li><a href="#" class="aprovar-orcamento" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Aprovar orçamento</a></li>';
                            $lst_opcoes['aprovar-decurso'] = '<li><a href="#" class="aprovar-decurso-prazo" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Aprovar certidão decurso de prazo</a></li>';
                            $lst_opcoes['encaminhar'] = '<li><a href="#" class="encaminhar-notificacao" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Encaminhar</a></li>';
                            $lst_opcoes['alterar'] =  '<li><a href="#" data-toggle="modal" data-target="#nova-alienacao" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Alterar</a></li>';
                            $lst_opcoes['cancelar'] = '<li><a href="#" class="cancelar-notificacao" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Cancelar notificação</a></li>';
                            $lst_opcoes['detalhes-custas'] = '<li><a href="#" data-toggle="modal" data-target="#detalhes-custas" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Detalhes das custas</a></li>';
                            $lst_opcoes['observacoes'] = '<li><a href="#" data-toggle="modal" data-target="#observacoes" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Observacoes</a></li>';

                            switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
                                case $this::ID_SITUACAO_CADASTRADO:
                                    $cls_check = 'pendente';
                                    $btn_detalhes = 'Detalhes';
                                    $opcoes = array('encaminhar','cancelar');
                                    break;
                                case $this::ID_SITUACAO_FINALIZADO:
                                    $cls_check = 'finalizada';
                                    $btn_detalhes = 'Resultado';
                                    $opcoes = array('detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_CANCELADO:
                                    $cls_check = 'finalizada';
                                    $btn_detalhes = 'Detalhes';
                                    $opcoes = array('detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_AGUARDANDOINTERACAO:
                                    $cls_check = 'encaminhada';
                                    $btn_detalhes = 'Interagir';
                                    $opcoes = array('cancelar','detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_EMPROCESSAMENTO:
                                    $cls_check = 'encaminhada';
                                    $btn_detalhes = 'Detalhes';
                                    $opcoes = array('cancelar','detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_DEVOLVIDO:
                                    $cls_check = 'pendente';
                                    $btn_detalhes = 'Corrigir';
                                    $opcoes = array('alterar','encaminhar','cancelar','detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO:
                                    $cls_check = 'orcamento';
                                    $btn_detalhes = 'Detalhes';
                                    $opcoes = array('aprovar-orcamento','cancelar','detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_AGUARDANDO_AUTORIZACAO_DECURSO_PRAZO:
                                    $cls_check = 'decurso';
                                    $btn_detalhes = 'Detalhes';
                                    $opcoes = array('aprovar-decurso','cancelar','detalhes-custas','observacoes');
                                    break;
                                case $this::ID_SITUACAO_AGUARDANDOPAGAMENTO: case $this::ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ:
                                $cls_check = 'pagamento';
                                $btn_detalhes = 'Detalhes';
                                $opcoes = array('detalhes-custas','observacoes');
                                break;
                                default:
                                    $cls_check = 'encaminhada';
                                    $btn_detalhes = 'Detalhes';
                                    $opcoes = array();
                                    break;
                            }
                            if(array_intersect(array(8),$modulos)) {
                                $lst_opcoes = array_intersect_key($lst_opcoes,array_flip($opcoes));
                            } else {
                                $btn_detalhes = 'Detalhes';
                                $opcoes = array('detalhes-custas','observacoes');
                                $lst_opcoes = array_intersect_key($lst_opcoes,array_flip($opcoes));
                            }
                            ?>
                            <tr>
                                <td>
                                    <div class="checkbox checkbox-primary">
                                        <input type="hidden" name="id_alienacao[]" class="id_alienacao" value="{{$alienacao->id_alienacao}}" />
                                        <input id="alienacao-{{$alienacao->id_alienacao}}" class="alienacao" class="styled" type="checkbox" value="{{$alienacao->id_alienacao}}">
                                        <label for="alienacao-{{$alienacao->id_alienacao}}"></label>
                                    </div>
                                </td>
                                <td>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
                                <td>{{$alienacao->numero_contrato}}</td>
                                <td>
                                    @if(count($alienacao->alienacao_devedor)>0)
                                        @foreach($alienacao->alienacao_devedor as $devedor)
                                            <span class="label label-primary">{{$devedor->no_devedor}} ({{$devedor->nu_cpf_cnpj}})</span>
                                        @endforeach
                                    @endif
                                </td>
                                <td><span class="real">{{$alienacao->alienacao_valor_total()}}</span></td>
                                <td>{{$alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}</td>
                                <td class="options">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}">{{$btn_detalhes}}</button>
                                        @if ($alienacao->observacoes_nao_lidas()>0)
                                            <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}" title="{{$alienacao->observacoes_nao_lidas()}} {{($alienacao->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                                {{$alienacao->observacoes_nao_lidas()}}
                                            </a>
                                        @endif
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-primary dropdown-toggle {{(count($lst_opcoes)<=0?'disabled':'')}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" {{(count($lst_opcoes)<=0?'disabled':'')}}>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                {!!implode($lst_opcoes)!!}
                                            </ul>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">
                                <div class="single alert alert-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    <div class="mensagem">
                                        Nenhuma notificação foi encontrada.
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    <div id="cancelamento-notificacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cancelamento de notificacoes</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="salvar-relatorio btn btn-success">Salvar relatório em excel</button>
                    <button type="button" class="salvar-cancelamento btn btn-success">Salvar cancelamentos</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-alienacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="arquivo-alienacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Arquivo da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form carregando"></div>
                </div>
                <div class="modal-footer">
                    <a id="arquivo-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="previsualizar" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Orçamento da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form carregando"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="aceitar-orcamento btn btn-success pull-right">Aceitar orçamento</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-credor" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo credor</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-credor" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="incluir-credor btn btn-success">Incluir credor</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo imóvel</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-imovel" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-imovel btn btn-success">Cadastrar imóvel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do imóvel - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-devedor" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo devedor</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-devedor" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-devedores btn btn-success">Incluir devedores</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-devedor" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do devedor - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-endereco" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo endereço</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-endereco" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-enderecos btn btn-success">Incluir endereços</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-endereco" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do endereço</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="nova-parcela" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nova parcela</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-nova-parcela" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-parcelas btn btn-success">Incluir parcelas</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-andamento" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes andamento da alienação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-custas" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes das custas - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="relatorio-notificacoes-pesquisa" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Relatório de notificações</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="salvar-relatorio-pesquisa btn btn-success pull-right">Salvar relatório</button>
                        </div>
                    </div>
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="salvar-relatorio-pesquisa btn btn-success">Salvar relatório</button>
                </div>
            </div>
        </div>
    </div>
    <div id="visualizar-arquivo-caixa" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recibo Alienação Caixa<span></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-endereco-andamento" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo endereço</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-endereco" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-enderecos btn btn-success">Incluir endereços</button>
                </div>
            </div>
        </div>
    </div>
    <div id="observacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Observações da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection