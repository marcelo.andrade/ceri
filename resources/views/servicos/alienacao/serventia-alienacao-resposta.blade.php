<div class="row">
    <div class="pull-right">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#visualizar-arquivo" data-idalienacao="{{$alienacao->id_alienacao}}" class="btn btn-primary" id="btnImprimir">
            <span class="glyphicon glyphicon-print"></span> Imprimir Recibo Notificação
        </a>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>Destinatário</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">Cidade</label>
                        <select name="id_cidade" class="form-control pull-left" disabled="disabled">
                            <option>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                        </select>
                    </div>
                    <div id="id_serventia" class="col-md-12">
                        <label class="small">Cartório</label>
                        <select name="id_serventia" class="form-control pull-left" disabled="disabled">
                            <option>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Credor</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">Credor</label>
                        <select name="id_credor_alienacao" class="form-control pull-left" disabled>
                            <option>{{$alienacao->credor_alienacao->credor->no_credor}}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="small">Agência</label>
                        <select name="id_agencia" class="form-control pull-left" disabled>
                            <option value="{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}">{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}</option>
                        </select>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-8">
            <fieldset>
                <legend>Dados da alienação</legend>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="small">Número do contrato</label>
                        <input type="text" name="numero_contrato" class="form-control " value="{{$alienacao->numero_contrato or ''}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor da dívida</label>
                        <input type="text" name="va_divida" class="form-control real" value="{{$alienacao->va_divida or ''}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do leilão</label>
                        <input type="text" name="va_leilao" class="form-control real" value="{{$alienacao->va_leilao or ''}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Código legado</label>
                        <input type="text" name="id_legado" class="form-control " value="{{$alienacao->id_legado or ''}}" disabled />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset>
                <legend>Dados do imóvel</legend>
                <div id="matricula_imovel" class="col-md-12">
                    <label class="small">Matrícula do imóvel</label>
                    <div class="input-group">
                        <input type="text" name="matricula_imovel" class="form-control disabled" readonly value="{{$alienacao->matricula_imovel}}" />
                        <div class="input-group-btn">
                            <button type="button" class="detalhes-imovel btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-imovel" data-idalienacao="{{$alienacao->id_alienacao}}" data-matriculaimovel="{{$alienacao->matricula_imovel}}">Detalhes do imóvel</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Projeção</legend>
        <div class="clearfix">
            @if($alienacao->alienacao_projecao)
                <div class="form-group col-md-12">
                    <div class="col-md-3">
                        <label class="small">Valor do período (1 a 15 dias)</label>
                        <input type="text" name="va_periodo_01" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_01}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do período (16 a 30 dias)</label>
                        <input type="text" name="va_periodo_02" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_02}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do período (31 a 45 dias)</label>
                        <input type="text" name="va_periodo_03" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_03}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do período (46 a 60 dias)</label>
                        <input type="text" name="va_periodo_04" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_04}}" disabled/>
                    </div>
                </div>
            @endif
            @if($alienacao->dt_encaminhamento())
                <div class="form-group col-md-12">
                    <div class="col-md-3">
                        <label class="small">Vencimento 1º período</label>
                        <input type="text" name="va_vencimento_periodo_01" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_1'])->format('d/m/Y')}}" disabled />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Vencimento 2º período</label>
                        <input type="text" name="va_vencimento_periodo_02" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_2'])->format('d/m/Y')}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Vencimento 3º período</label>
                        <input type="text" name="va_vencimento_periodo_03" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_3'])->format('d/m/Y')}}" disabled/>
                    </div>
                    <div class="col-md-3">
                        <label class="small">Vencimento 4º período</label>
                        <input type="text" name="va_vencimento_periodo_04" class="form-control" value="{{Carbon\Carbon::parse($alienacao->vencimentos()['dt_vencimento_periodo_4'])->format('d/m/Y')}}" disabled/>
                    </div>
                </div>
            @endif
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Devedores</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="devedores" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="20%">CPF/CNPJ</th>
                            <th width="70%">Nome</th>
                            <th>Ação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($alienacao->alienacao_devedor)>0)
                            @foreach($alienacao->alienacao_devedor as $devedor)
                                <tr>
                                    <td>{{$devedor->nu_cpf_cnpj}}</td>
                                    <td>{{$devedor->no_devedor}}</td>
                                    <td>
                                        @if($devedor->in_registro_ativo=='S')
                                            <a href="#" class="detalhes-credor btn btn-primary" data-toggle="modal" data-target="#detalhes-devedor" data-idalienacao="{{$alienacao->id_alienacao}}" data-iddevedor="{{$devedor->id_alienacao_devedor}}" data-nucpfcnpj="{{$devedor->nu_cpf_cnpj}}">Alterar</a>
                                        @else
                                            <a href="#" class="detalhes-credor btn btn-primary disabled" disabled="disabled">Alterar</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereços de cobrança</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="enderecos" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="90%">Endereço</th>
                            <th>Ação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($alienacao->endereco_alienacao)>0)
                            <?php
                            $i=0;
                            ?>
                            @foreach($alienacao->endereco_alienacao as $endereco)
                                <tr id="{{$endereco->id_endereco_alienacao}}">
                                    <td>
                                        {{$endereco->no_endereco}}
                                    </td>
                                    <td>
                                        @if($endereco->in_imovel=='S')
                                            <a href="#" class="detalhes-imovel btn btn-primary" data-toggle="modal" data-target="#detalhes-imovel" data-idalienacao="{{$endereco->id_alienacao}}" data-matriculaimovel="{{$alienacao->matricula_imovel}}">Alterar</a>
                                        @else
                                            <a href="#" class="detalhes-imovel btn btn-primary disabled" disabled>Alterar</a>
                                        @endif
                                    </td>
                                </tr>
                                <?php
                                $i++;
                                ?>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Arquivos</legend>
        <div class="col-md-12">
            @if(count($alienacao->alienacao_arquivo)>0)
                @foreach($alienacao->alienacao_arquivo as $arquivo)
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#arquivo-alienacao" data-idarquivo="{{$arquivo->arquivo_grupo_produto->id_arquivo_grupo_produto}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}" data-noarquivo="{{$arquivo->arquivo_grupo_produto->no_arquivo}}" data-noextensao="{{$arquivo->arquivo_grupo_produto->no_extensao}}">{{$arquivo->arquivo_grupo_produto->no_arquivo}}</a>
                @endforeach
            @endif
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Andamento</legend>
        <div id="novo-andamento">
            <div class="carregando text-center" style="display:none"><img src="{{asset('images/loading01.gif')}}" /></div>
            <div class="form"></div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Histórico de intimação</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="historico-pedido" class="table table-striped table-bordered small">
                    <thead>
                    <tr class="gradient01">
                        <th>Fase</th>
                        <th>Etapa</th>
                        <th>Ação</th>
                        <th>Resultado</th>
                        <th>Data</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($alienacao->alienacao_pedido->andamento_alienacao as $andamento)
                        <tr id="{{$andamento->id_andamento_alienacao}}">
                            <td>{{$andamento->fase_grupo_produto->no_fase}}</td>
                            <td>{{$andamento->etapa_fase->no_etapa}}</td>
                            <td>{{$andamento->acao_etapa->no_acao}}</td>
                            <td class="resultado">
                                @if($andamento->resultado_acao)
                                    <label class="label label-success">{{$andamento->resultado_acao->no_resultado}}</label>
                                @else
                                    <label class="label label-warning">Aguardando</label>
                                @endif
                            </td>
                            <td>{{Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                            <td>
                                <a href="#" class="detalhes-andamento btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-andamento" data-idandamento="{{$andamento->id_andamento_alienacao}}" data-nofase="{{$andamento->fase_grupo_produto->no_fase}}" data-noetapa="{{$andamento->etapa_fase->no_etapa}}" data-noacao="{{$andamento->acao_etapa->no_acao}}">Ver detalhes</a></li>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
@if(count($alienacao->alienacao_pedido->andamento_alienacao_excluido) > 0)
    <div class="fieldset-group">
        <fieldset>
            <legend>Histórico de remoções de interações</legend>
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="historico-pedido" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th>Fase</th>
                            <th>Etapa</th>
                            <th>Ação</th>
                            <th>Resultado</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alienacao->alienacao_pedido->andamento_alienacao_excluido as $andamento)
                            <tr>
                                <td>{{$andamento->fase_grupo_produto->no_fase}}</td>
                                <td>{{$andamento->etapa_fase->no_etapa}}</td>
                                <td>{{$andamento->acao_etapa->no_acao}}</td>
                                <td>
                                    @if($andamento->resultado_acao)
                                        <label class="label label-success">{{$andamento->resultado_acao->no_resultado}}</label>
                                    @else
                                        <label class="label label-warning">Aguardando</label>
                                    @endif
                                </td>
                                <td>{{Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i')}}</td>
                                <td>
                                    <a href="#" class="detalhes-andamento btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-andamento-excluido" data-idandamento="{{$andamento->id_andamento_alienacao_excluido}}" data-nofase="{{$andamento->fase_grupo_produto->no_fase}}" data-noetapa="{{$andamento->etapa_fase->no_etapa}}" data-noacao="{{$andamento->acao_etapa->no_acao}}">Ver detalhes</a></li>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
    </div>
@endif