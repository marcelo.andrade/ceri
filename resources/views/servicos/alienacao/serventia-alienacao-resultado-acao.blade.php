<?php
switch ($resultado_acao->co_resultado) {
    case 9: case 52:
?>
        <div class="form-group clearfix">
            <div class="col-md-4">
                <label class="small">Serventia RTD</label>
                <select name="id_serventia" class="form-control">
                    <option value="0">Selecione</option>
                    @if(count($variaveis['rtds'])>0)
                        @foreach($variaveis['rtds'] as $rtd)
                            <option value="{{$rtd->id_serventia}}">{{$rtd->no_serventia}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
<?php
        break;
    case 134: case 136:
?>
        <br />
        <div class="form-group col-md-12 clearfix">
            <fieldset>
                <legend>Novos endereços de cobrança</legend>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <button type="button" class="novo-endereco btn btn-success pull-left" data-toggle="modal" data-target="#novo-endereco-andamento" data-andamentotoken="{{$andamento_token}}">Novo endereço</button>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <div class="panel table-rounded">
                            <table id="enderecos-andamento" class="table table-striped table-bordered small">
                                <thead>
                                    <tr class="gradient01">
                                        <th width="90%">Endereço</th>
                                        <th width="10%">Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
<?php
        break;
}
?>
@if($resultado_acao->de_observacao != "")
    <?php
    switch ($resultado_acao->tp_observacao) {
        case "A" :
            $tp_alerta = 'alert-warning';
            $ico_alerta = '<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>';
            break;
        case "S" :
            $tp_alerta = 'alert-success';
            $ico_alerta = '<i class="icon glyphicon glyphicon-ok pull-left"></i>';
            break;
        case "E" :
            $tp_alerta = 'alert-danger';
            $ico_alerta = '<i class="icon glyphicon glyphicon-remove pull-left"></i>';
            break;
        case "I" :
            $tp_alerta = 'alert-info';
            $ico_alerta = '<i class="icon glyphicon glyphicon-info-sign pull-left"></i>';
            break;
    }
    ?>
    <div class="form-group clearfix">
        <div class="col-md-12">
            <div class="alert {{$tp_alerta}} single">
                {!!$ico_alerta!!}
                <div class="menssagem">{{$resultado_acao->de_observacao}}</div>
            </div>
        </div>
    </div>
@endif
@if($resultado_acao->tp_texto_curto_resultado>0 or $resultado_acao->tp_data_resultado>0 or $resultado_acao->tp_valor_resultado>0)
    <div class="form-group clearfix">
        @if($resultado_acao->tp_texto_curto_resultado>0)
            <div class="col-md-4">
                <label class="small">{{$resultado_acao->lb_texto_curto_resultado}}</label>
                <input type="text" name="de_texto_curto_resultado" class="form-control {{($resultado_acao->tp_texto_curto_resultado>1?'obrigatorio':'')}}" rows="4" title="{{mb_strtolower($resultado_acao->lb_texto_curto_resultado,'UTF-8')}}" />
            </div>
        @endif
        @if($resultado_acao->tp_valor_resultado>0)
            <div class="col-md-4">
                <label class="small">{{$resultado_acao->lb_valor_resultado}}</label>
                <input type="text" name="va_valor_resultado" class="real form-control {{($resultado_acao->tp_valor_resultado>1?'obrigatorio':'')}}" value="{{isset($variaveis['edital'])?$variaveis['edital']['va_divida']:""}}" title="{{mb_strtolower($resultado_acao->lb_valor_resultado,'UTF-8')}}" />
            </div>
        @endif
        @if($resultado_acao->tp_data_resultado>0)
            <div class="col-md-4">
                <label class="small">{{$resultado_acao->lb_data_resultado}}</label>
                <input type="text" name="dt_resultado" class="data form-control {{($resultado_acao->tp_data_resultado>1?'obrigatorio':'')}}" value="{{isset($variaveis['edital'])?$variaveis['edital']['dt_acao']:\Carbon\Carbon::now()->format('d/m/Y')}}" title="{{mb_strtolower($resultado_acao->lb_data_resultado,'UTF-8')}}" />
            </div>
        @endif
    </div>
@endif
@if($resultado_acao->tp_texto_longo_resultado>0)
    <div class="form-group clearfix">
        <div class="col-md-12">
            <label class="small">{{$resultado_acao->lb_texto_longo_resultado}}</label>
            <textarea name="de_texto_longo_resultado" class="form-control {{($resultado_acao->tp_texto_longo_resultado>1?'obrigatorio':'')}}" rows="4" title="{{mb_strtolower($resultado_acao->lb_texto_longo_resultado,'UTF-8')}}"></textarea>
        </div>
    </div>
@endif
@if($resultado_acao->tp_upload_resultado>0)
    <div class="col-md-12">
        <div class="form-group clearfix">
            <fieldset>
                <legend class="small">{{$resultado_acao->lb_upload_resultado}}</legend>
                <div class="col-md-12">
                    <div id="arquivos-andamento" class="arquivos {{($resultado_acao->tp_upload_resultado>1?'obrigatorio':'')}}" title="{{mb_strtolower($resultado_acao->lb_upload_resultado,'UTF-8')}}">
                        <button type="button" class="novo-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="22" data-token="{{$andamento_token}}" data-limite="{{$resultado_acao->lim_upload_resultado}}">Adicionar arquivo</button>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div id="assinaturas-arquivos" class="form-group clearfix">
        <div class="col-md-12">
            <div class="alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem clearfix">
                    <span>Nenhum arquivo foi inserido.</span><br />
                    <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$andamento_token}}">Assinar arquivo(s)</a>
                </div>
            </div>
        </div>
    </div>
@endif