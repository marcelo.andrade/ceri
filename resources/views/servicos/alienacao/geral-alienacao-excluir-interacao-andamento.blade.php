    <form name="form-excluir-interacao-andamento" method="post" action="">
        <input type="hidden" name="id_andamento_alienacao" value="{{$andamento_alienacao->id_andamento_alienacao}}" />
        <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
        <div class="erros-andamento alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <fieldset>
            <legend>Justificativa da exclusão</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">Justificativa</label>
                    <textarea name="justificativa" class="form-control" rows="4" maxlength="500"></textarea>
                </div>
            </div>
        </fieldset>
    </form>