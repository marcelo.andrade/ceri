@if (count($todas_alienacoes)>0)
    <div id="alert" class="alert alert-info">
        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
        <div class="menssagem clearfix" style="overflow:initial;">
            <span class="total">0 notificações selecionadas</span>
            <div class="pull-right">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-primary disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                        Ações com selecionadas
                    </button>
                    <button type="button" class="btn btn-primary dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        @if(array_intersect(array(8),$modulos))
                            <li class="disabled"><a href="#" class="encaminhar-todas-notificacoes" disabled>Encaminhar selecionadas <span></span></a></li>
                            <li class="disabled"><a href="#" class="aprovar-todos-decursos-prazo" disabled>Aprovar decursos de prazo das selecionadas <span></span></a></li>
                            <li class="disabled"><a href="#" class="aprovar-todos-orcamentos" disabled>Aprovar orçamentos das selecionadas <span></span></a></li>
                            <li role="separator" class="divider"></li>
                            <li class="disabled"><a href="#" data-toggle="modal" data-target="#cancelamento-notificacoes" class="cancelar-todas" disabled>Cancelar selecionadas <span></span></a></li>
                        @endif
                        <li class="disabled"><a href="#" data-toggle="modal" data-target="#relatorio-notificacoes" class="gerar-relatorio-todas" disabled>Gerar relatório das selecionadas <span></span></a></li>
                    </ul>
                </div>
                <?php
                /*
                <button type="button" class="btn btn-success disabled" data-toggle="modal" data-target="#cancelamento-contratos" disabled>
                    Cancelar contratos das selecionadas
                </button>
                <button type="button" class="btn btn-success disabled" data-toggle="modal" data-target="#relatorio-notificacoes" disabled>
                    Gerar relatório das selecionadas
                </button>
                */
                ?>
            </div>
        </div>
    </div>
@endif
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th width="2%">
                    <div class="checkbox checkbox-primary">
                        <input id="selecionar-todas" class="styled" type="checkbox">
                        <label for="selecionar-todas"></label>
                    </div>
                </th>
                <th width="10%">Protocolo</th>
                <th width="10%">Data do pedido</th>
                <th width="10%">Contrato</th>
                <th width="26%">Devedor</th>
                <th width="10%">Etapa Atual</th>
                <th width="10%">Data última movimentação</th>
                <th width="8%">Custo</th>
                <th width="14%">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (count($todas_alienacoes)>0)
                @foreach ($todas_alienacoes as $alienacao)
                    <?php
                    $lst_opcoes['atualizar-valor-edital'] = '<li class="dropdown-submenu"><a tabindex="-1" href="javascript:void();">Atualizar valor edital</a>
                                                                <ul class="dropdown-menu">
                                                                  <li><a href="javascript:void();" class="atualizar-valor-edital" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'" data-idresultadoacao="152">Sim</a></li>
                                                                  <li><a href="javascript:void();" class="atualizar-valor-edital" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'" data-idresultadoacao="153" data-inserirresultado="S">Não</a></li>
                                                                </ul>
                                                             </li>';

                    $lst_opcoes['aprovar-orcamento'] = '<li><a href="javascript:void();" class="aprovar-orcamento" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Aprovar orçamento</a></li>';
                    $lst_opcoes['aprovar-decurso'] = '<li><a href="javascript:void();" class="aprovar-decurso-prazo" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Aprovar certidão decurso de prazo</a></li>';
                    $lst_opcoes['encaminhar'] = '<li><a href="javascript:void();" class="encaminhar-notificacao" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Encaminhar</a></li>';
                    $lst_opcoes['alterar'] =  '<li><a href="javascript:void();" data-toggle="modal" data-target="#nova-alienacao" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Alterar</a></li>';
                    $lst_opcoes['cancelar'] = '<li><a href="javascript:void();" class="cancelar-notificacao" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Cancelar notificação</a></li>';
                    $lst_opcoes['detalhes-custas'] = '<li><a href="javascript:void();" data-toggle="modal" data-target="#detalhes-custas" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Detalhes das custas</a></li>';

                    if($class->id_tipo_pessoa!=9){
                        $lst_opcoes['observacoes'] = '<li><a href="javascript:void();" data-toggle="modal" data-target="#observacoes" data-idalienacao="'.$alienacao->id_alienacao.'" data-protocolo="'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'">Observações</a></li>';
                    }
                    
                    switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
                        case $this::ID_SITUACAO_CADASTRADO:
                            $cls_check = 'pendente';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array('encaminhar','cancelar');
                            break;
                        case $this::ID_SITUACAO_FINALIZADO:
                            $cls_check = 'finalizada';
                            $btn_detalhes = 'Resultado';
                            $opcoes = array('detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_CANCELADO:
                            $cls_check = 'finalizada';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array('detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_AGUARDANDOINTERACAO:
                            $cls_check = 'encaminhada';
                            $btn_detalhes = 'Interagir';
                            $opcoes = array('cancelar','detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_EMPROCESSAMENTO:
                            $cls_check = 'encaminhada';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array('cancelar','detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_DEVOLVIDO:
                            $cls_check = 'pendente';
                            $btn_detalhes = 'Corrigir';
                            $opcoes = array('alterar','encaminhar','cancelar','detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO:
                            $cls_check = 'orcamento';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array('aprovar-orcamento','cancelar','detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_AGUARDANDO_AUTORIZACAO_DECURSO_PRAZO:
                            $cls_check = 'decurso';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array('aprovar-decurso','cancelar','detalhes-custas','observacoes');
                            break;
                        case $this::ID_SITUACAO_AGUARDANDOPAGAMENTO: case $this::ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ:
                            $cls_check = 'pagamento';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array('detalhes-custas','observacoes');
                            break;
                        default:
                            $cls_check = 'encaminhada';
                            $btn_detalhes = 'Detalhes';
                            $opcoes = array();
                            break;
                    }
                    if ($alienacao->alienacao_pedido->alienacao_andamento_atual->id_resultado_acao == 132) {
                        array_unshift($opcoes,'atualizar-valor-edital');
                    }
                    if(array_intersect(array(8),$modulos)) {
                        $lst_opcoes = array_intersect_key($lst_opcoes,array_flip($opcoes));
                    } else {
                        $btn_detalhes = 'Detalhes';
                        $opcoes = array('detalhes-custas','observacoes');
                        $lst_opcoes = array_intersect_key($lst_opcoes,array_flip($opcoes));
                    }
                    ?>
                    <tr>
                        <td>
                            <div class="checkbox checkbox-primary">
                                <input id="alienacao-{{$alienacao->id_alienacao}}" class="alienacao alienacao-{{$cls_check}}" class="styled" type="checkbox" value="{{$alienacao->id_alienacao}}">
                                <label for="alienacao-{{$alienacao->id_alienacao}}"></label>
                            </div>
                        </td>
                        <td><input type="hidden" name="id_alienacao[]" class="id_alienacao" value="{{$alienacao->id_alienacao}}" />{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
                        <td>{{\Carbon\Carbon::parse($alienacao->alienacao_pedido->pedido->dt_pedido)->format('d/m/Y')}}</td>
                        <td>{{$alienacao->numero_contrato}}</td>
                        <td >
                            @if(count($alienacao->alienacao_devedor)>0)
                                @foreach($alienacao->alienacao_devedor as $devedor)
                                    <span class="label label-primary label-wrap">{{$devedor->no_devedor}} ({{$devedor->nu_cpf_cnpj}})</span>
                                @endforeach
                            @endif
                        </td>
                		<td>{{$alienacao->etapa_atual()}}</td>
                        <td>{{Carbon\Carbon::parse($alienacao->alienacao_pedido->alienacao_andamento_situacao->dt_cadastro)->format('d/m/Y')}}</td>
                        <td><span class="real">{{$alienacao->alienacao_valor_total()}}</span></td>
                   		<td class="options">
    						<div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}">{{$btn_detalhes}}</button>
                                @if ($alienacao->observacoes_nao_lidas()>0)
                                    <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}" title="{{$alienacao->observacoes_nao_lidas()}} {{($alienacao->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                        {{$alienacao->observacoes_nao_lidas()}}
                                    </a>
                                @endif
    							<div class="btn-group" role="group">
    								<button type="button" class="btn btn-primary dropdown-toggle {{(count($lst_opcoes)<=0?'disabled':'')}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" {{(count($lst_opcoes)<=0?'disabled':'')}}>
    									<span class="caret"></span>
    								</button>
    								<ul class="dropdown-menu">
    									{!!implode($lst_opcoes)!!}
    								</ul>
                            	</div>
                                @if ($class->id_tipo_pessoa==8 && $alienacao->verifica_endereco_duplicado()==="S" && in_array($alienacao->verifica_acao_etapa(),[1,81]))
                                    <a href="#" class="badge-ed badge badge-end-duplic btn-tooltip" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao->id_alienacao}}" data-protocolo="{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}" title="Possui endereço duplicado">
                                        ED
                                    </a>
                                @endif
                            </div>
                        </td>
                	</tr>
            	@endforeach
            @else
            	<tr>
                	<td colspan="8">
                    	<div class="single alert alert-danger">
                        	<i class="glyphicon glyphicon-remove"></i>
                            <div class="mensagem">
    	                    	Nenhuma notificação foi encontrada.
                            </div>
                        </div>
                    </td>
                </tr>
    		@endif
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($todas_alienacoes)}}</b> de <b>{{$todas_alienacoes_total}}</b> {{(count($todas_alienacoes)>1?'notificações':'notificação')}}.
</div>
<div align="center">
    <?php
    echo $todas_alienacoes->fragment('pedidos')->appends(['dt_ini' => $request->dt_ini,
                                                          'dt_fim' => $request->dt_fim,
                                                          'situacao' => $request->situacao,
                                                          'protocolo' => $request->protocolo,
                                                          'contrato' => $request->contrato,
                                                          'total' => $request->total,
                                                          'grupo' => $request->grupo,
                                                          'filtro' => $request->filtro])->render();
    ?>
</div>
