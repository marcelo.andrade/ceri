<fieldset class="clearfix">
    <legend>Filtro</legend>
    <div class="fieldset-group clearfix">
        <div class="col-md-3">
            <fieldset>
                <legend>Período</legend>
                <div class="col-md-12">
                    <div class="periodo input-group input-daterange">
                        <input type="text" class="form-control pull-left" name="dt_ini" rel="periodo"
                               value="{{$request->dt_ini}}" readonly/>
                        <span class="input-group-addon small pull-left">até</span>
                        <input type="text" class="form-control pull-left" name="dt_fim" rel="periodo"
                               value="{{$request->dt_fim}}" readonly/>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Protocolo</legend>
                <div class="col-md-12">
                    <div class="protocolo">
                        <input type="text" name="protocolo_pedido_devolvidos" class="form-control"
                               value="{{$request->protocolo_pedido_devolvidos}}"/>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Total por página</legend>
                <div class="col-md-12">
                    <select name="total_paginacao" class="form-control pull-left">
                        <option value="10" @if($request->total_paginacao==10) selected="selected" @endif>10</option>
                        <option value="50" @if($request->total_paginacao==50) selected="selected" @endif>50</option>
                        <option value="100" @if($request->total_paginacao==100) selected="selected" @endif>100</option>
                        <option value="500" @if($request->total_paginacao==500) selected="selected" @endif>500</option>
                        <option value="-1" @if($request->total_paginacao==-1) selected="selected" @endif>Todas</option>
                    </select>
                </div>
            </fieldset>
        </div>
    </div>
    @if($class->id_tipo_pessoa == 9)
    <div class="fieldset-group clearfix">
        <div class="col-md-6">
            <fieldset>
                <legend>Serventia</legend>
                <div class="col-md-5">
                    <fieldset>
                        <legend>Cidade</legend>
                        <div class="col-md-12">
                            <select name="cidade" class="form-control pull-left">
                                <option value="0">Selecione uma cidade</option>
                                @if(count($cidades)>0)
                                    @foreach ($cidades as $cidade)
                                        <option value="{{$cidade->id_cidade}}"
                                                @if($request->cidade==$cidade->id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-7">
                    <fieldset>
                        <legend>Serventia</legend>
                        <div class="col-md-12">
                            <select name="serventia" class="form-control pull-left"
                                    @if(count($serventias)<=0) disabled="" @endif>
                                <option value="0">Todas as serventias</option>
                                @if(count($serventias)>0)
                                    @foreach ($serventias as $serventia)
                                        <option value="{{$serventia->id_serventia}}"
                                                @if($request->serventia==$serventia->id_serventia) selected @endif>{{$serventia->no_serventia}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </fieldset>
                </div>
            </fieldset>
        </div>
    </div>
    @endif
    <div class="fieldset-group clearfix">
        <div class="buttons col-md-12 text-right">
            <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar"/>
            <input type="submit" class="btn btn-success" value="Filtrar"/>
        </div>
    </div>
    </div>
</fieldset>