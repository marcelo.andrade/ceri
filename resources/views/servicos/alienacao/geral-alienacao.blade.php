@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.cancelamento.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.imovel.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.devedor.js')}}?v=<?=time();?>"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.endereco.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.credoralienacao.js')}}?v=<?=time();?>"></script>

    <script type="text/javascript" src="{{asset('js/jquery.funcoes.arquivos.js')}}?v=<?=time();?>"></script>

    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.endereco.andamento.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.observacoes.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.detalhes.andamento.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.remover.interacao.andamento.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading  gradient01">
                <h4>Notificação de alienação fiduciária <span class="small">/ Serviços</span></h4>
            </div>
            <div id="filtro-certidao" class="panel-body">
                @if(array_intersect(array(8),$modulos))
                    <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#nova-alienacao">
                        Nova notificação
                    </button>
                @endif
                <form name="form-filtro" id="form-filtro" method="post" action="{{URL::to('servicos/alienacao')}}" class="clearfix">
                    {{csrf_field()}}
                    @include('servicos.alienacao.geral-alienacao-filtro')
                </form>
                @include('servicos.alienacao.geral-alienacao-historico')
            </div>
        </div>
    </div>
    <div id="nova-alienacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pull-left">Nova notificação</h4> 
                </div>
                <div class="modal-body">
                	<div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-nova-alienacao" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="enviar-alienacao btn btn-success">Enviar notificação</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-alienacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="arquivo-alienacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Arquivo da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form carregando"></div>
                </div>
                <div class="modal-footer">
                    <a id="arquivo-download" target="_blank" class="btn btn-success pull-left" style="display:none"><i class="glyphicon glyphicon-floppy-save"></i> Download do arquivo</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="previsualizar" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Orçamento da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form carregando"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="aceitar-orcamento btn btn-success pull-right">Aceitar orçamento</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-credor" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo credor</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-credor" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="incluir-credor btn btn-success">Incluir credor</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo imóvel</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-imovel" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-imovel btn btn-success">Cadastrar imóvel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do imóvel - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-devedor" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo devedor</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-devedor" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-devedores btn btn-success">Incluir devedores</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-devedor" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do devedor - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-endereco" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo endereço</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-endereco" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-enderecos btn btn-success">Incluir endereços</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-endereco" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do endereço</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="nova-parcela" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nova parcela</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-nova-parcela" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-parcelas btn btn-success">Incluir parcelas</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-andamento" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes andamento da alienação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-custas" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes das custas - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="relatorio-notificacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Relatório de notificações</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="salvar-relatorio btn btn-success pull-right">Salvar relatório</button>
                        </div>
                    </div>
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="salvar-relatorio btn btn-success">Salvar relatório</button>
                </div>
            </div>
        </div>
    </div>
    <div id="cancelamento-notificacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cancelamento de notificacoes</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="salvar-relatorio btn btn-success">Salvar relatório em excel</button>
                    <button type="button" class="salvar-cancelamento btn btn-success">Salvar cancelamentos</button>
                </div>
            </div>
        </div>
    </div>
    <div id="visualizar-arquivo-caixa" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recibo Alienação Caixa<span></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="novo-endereco-andamento" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo endereço</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-novo-endereco" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="inserir-enderecos btn btn-success">Incluir endereços</button>
                </div>
            </div>
        </div>
    </div>
    <div id="observacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Observações da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-andamento-excluido" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes andamento da alienação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    @include('arquivos.arquivos-modais')
@endsection