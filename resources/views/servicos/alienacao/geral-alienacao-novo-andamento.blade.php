<?php
switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
	case $class::ID_SITUACAO_CADASTRADO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					Aguardando o encaminhamento.
				</div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_ORCAMENTOGERADO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					Aguardando o envio da notificação.
				</div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_FINALIZADO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					A notificação foi finalizada. Para mais informações, confira o histórico de andamentos abaixo.
				</div>
			</div>
		</div>
<?php
        break;
    /*
    Para permitir que a Caixa possa reprovar o orçamento
    case $class::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO:
?>
        <div class="col-md-12">
            <div class="status-atual alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem">
                   Aguardando aprovação do orçamento.
                </div>
            </div>
        </div>
<?php
        break;
    */
    case $class::ID_SITUACAO_AGUARDANDO_AUTORIZACAO_DECURSO_PRAZO:
?>
        <div class="col-md-12">
            <div class="status-atual alert alert-warning single">
                <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem">
                    Aguardando aprovação certidão decurso de prazo.
                </div>
            </div>
        </div>
<?php
		break;
	case $class::ID_SITUACAO_EMPROCESSAMENTO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					A notificação está em processamento.
				</div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_DEVOLVIDO:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
				<div class="menssagem">
					A notificação foi devolvida e está aguardando suas alterações. Para alterar, selecione a opção "Alterar" na caixa de ações.
				</div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_AGUARDANDOPAGAMENTO:
    case $class::ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ:
?>
		<div class="col-md-12">
			<div class="status-atual alert alert-warning single">
				<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                <div class="menssagem">
                    @if ( $alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto ==  $class::ID_SITUACAO_CANCELADO )
                        @if ( $alienacao->checar_cancelamento_pagamento() == NULL )
                            Processo Finalizado (Cancelado)
                        @else
                            Aguardando pagamento.
                        @endif
                    @else
                        Aguardando pagamento.
                    @endif
                </div>
			</div>
		</div>
<?php
		break;
	case $class::ID_SITUACAO_AGUARDANDOINTERACAO:
    case $class::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO:
?>
    <form name="form-novo-andamento" method="post" action="">
        <input type="hidden" name="id_alienacao" value="{{$alienacao->id_alienacao}}" />
        <input type="hidden" name="andamento_token" value="{{$andamento_token}}" />
        <div class="erros-andamento alert alert-danger" style="display:none">
            <i class="icon glyphicon glyphicon-remove pull-left"></i>
            <div class="menssagem"></div>
        </div>
        <div class="fieldset-group clearfix">
            @if($alienacao->alienacao_pedido->andamento_alienacao[0]->id_resultado_acao>0)
                <?php
                $nova_fase = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao->nova_fase;
                $nova_etapa = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao->nova_etapa;
                $nova_acao = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao->nova_acao;
                ?>
                @if($nova_acao->in_resultado_direto=='N')
                    <input type="hidden" name="in_inserir_resultado" value="N">
                    <div class="form-group col-md-12 clearfix">
                        <div class="status-atual alert alert-warning single">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem">
                                O próximo andamento será a fase <b>"{{$nova_fase->no_fase}}"</b>, etapa <b>"{{$nova_etapa->no_etapa}}"</b> e ação <b>"{{$nova_acao->no_acao}}"</b>.
                            </div>
                        </div>
                    </div>
                    @if($nova_acao->tp_texto_curto_acao>0 or $nova_acao->tp_data_acao>0)
                        <div class="form-group clearfix">
                            @if($nova_acao->tp_texto_curto_acao>0)
                                <div class="col-md-4">
                                    <label class="small">{{$nova_acao->lb_texto_curto_acao}}</label>
                                    <input type="text" name="de_texto_curto_acao" class="form-control {{($nova_acao->tp_texto_curto_acao>1?'obrigatorio':'')}}" rows="4" title="{{mb_strtolower($nova_acao->lb_texto_curto_acao,'UTF-8')}}"></textarea>
                                </div>
                            @endif
                            @if($nova_acao->tp_valor_acao>0)
                                <div class="col-md-4">
                                    <label class="small">{{$nova_acao->lb_valor_acao}}</label>
                                    <input type="text" name="va_valor_acao" class="real form-control {{($nova_acao->tp_valor_acao>1?'obrigatorio':'')}}" title="{{mb_strtolower($nova_acao->lb_valor_acao,'UTF-8')}}" />
                                </div>
                            @endif
                            @if($nova_acao->tp_data_acao>0)
                                <div class="col-md-4">
                                    <label class="small">{{$nova_acao->lb_data_acao}}</label>
                                    <input type="text" name="dt_acao" class="data form-control {{($nova_acao->tp_data_acao>1?'obrigatorio':'')}}" value="{{\Carbon\Carbon::now()->format('d/m/Y')}}" title="{{mb_strtolower($nova_acao->lb_data_acao,'UTF-8')}}" />
                                </div>
                            @endif
                        </div>
                    @endif
                    @if($nova_acao->tp_texto_longo_acao>0)
                        <div class="form-group clearfix">
                            <div class="col-md-12">
                                <label class="small">{{$nova_acao->lb_texto_longo_acao}}</label>
                                <textarea name="de_texto_longo_acao" class="form-control {{($nova_acao->tp_texto_longo_acao>1?'obrigatorio':'')}}" rows="4" title="{{mb_strtolower($nova_acao->lb_texto_longo_acao,'UTF-8')}}"></textarea>
                            </div>
                        </div>
                    @endif
                    @if($nova_acao->tp_upload_acao>0)
                        <div class="col-md-12">
                            <div class="form-group clearfix">
                                <fieldset>
                                    <legend class="small">{{$nova_acao->lb_upload_acao}}</legend>
                                    <div class="col-md-12">
                                        <div id="arquivos-andamento" class="arquivos {{($nova_acao->tp_upload_acao>1?'obrigatorio':'')}} btn-list" title="{{mb_strtolower($nova_acao->lb_upload_acao,'UTF-8')}}">
                                            <button type="button" class="novo-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="22" data-token="{{$andamento_token}}" data-limite="{{$nova_acao->lim_upload_acao}}">Adicionar arquivo</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    @endif
                    @if($nova_acao->de_observacao != "")
                        <?php
                        switch ($nova_acao->tp_observacao) {
                            case "A" :
                                $tp_alerta = 'alert-warning';
                                $ico_alerta = '<i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>';
                                break;
                            case "S" :
                                $tp_alerta = 'alert-success';
                                $ico_alerta = '<i class="icon glyphicon glyphicon-ok pull-left"></i>';
                                break;
                            case "E" :
                                $tp_alerta = 'alert-danger';
                                $ico_alerta = '<i class="icon glyphicon glyphicon-remove pull-left"></i>';
                                break;
                            case "I" :
                                $tp_alerta = 'alert-info';
                                $ico_alerta = '<i class="icon glyphicon glyphicon-info-sign pull-left"></i>';
                                break;
                        }
                        ?>
                        <div class="form-group clearfix">
                            <div class="col-md-12">
                                <div class="alert {{$tp_alerta}} single">
                                    {!!$ico_alerta!!}
                                    <div class="menssagem">{{$nova_acao->de_observacao}}</div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($nova_acao->tp_upload_acao>0)
                        <div id="assinaturas-arquivos" class="form-group clearfix">
                            <div class="col-md-12">
                                <div class="alert alert-warning single">
                                    <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                                    <div class="menssagem clearfix">
                                        <span>Nenhum arquivo foi inserido.</span><br />
                                        <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$andamento_token}}">Assinar arquivo(s)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <input type="hidden" name="in_inserir_resultado" value="S">
                    <div class="form-group col-md-12 clearfix">
                        <div class="status-atual alert alert-info single">
                            <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                            <div class="menssagem">
                                Você está na fase <b>"{{$nova_fase->no_fase}}"</b>, etapa <b>"{{$nova_etapa->no_etapa}}"</b> e ação <b>"{{$nova_acao->no_acao}}"</b>. Selecione um resultado para a ação abaixo.
                            </div>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-4">
                            <label class="small">Resultado</label>
                            <select name="id_resultado_acao" class="form-control">
                                <option value="0">Selecione</option>
                                @if(count($nova_acao->resultado_acao)>0)
                                    @foreach($nova_acao->resultado_acao as $resultado)
                                        <option value="{{$resultado->id_resultado_acao}}">{{$resultado->no_resultado}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div id="resultado-acao">
                        <div class="carregando text-center" style="display:none"><img src="{{asset('images/loading01.gif')}}" /></div>
                        <div class="form"></div>
                    </div>
                @endif
            @else
                <input type="hidden" name="in_inserir_resultado" value="S">
                <div class="form-group col-md-12 clearfix">
                    <div class="status-atual alert alert-info single">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">
                            Você está na fase <b>"{{$alienacao->alienacao_pedido->andamento_alienacao[0]->fase_grupo_produto->no_fase}}"</b>, etapa <b>"{{$alienacao->alienacao_pedido->andamento_alienacao[0]->etapa_fase->no_etapa}}"</b> e ação <b>"{{$alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->no_acao}}"</b>. Selecione um resultado para a ação abaixo.
                            @if($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->tp_alerta_acao>0)
                                <button type="button" data-toggle="modal" data-target="#info-andamento-acao" data-fase="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->fase_grupo_produto->no_fase}}" data-etapa="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->etapa_fase->no_etapa}}" data-acao="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->no_acao}}"  data-dealertaacao="{{$alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->de_alerta_acao}}" class="btn btn-sm-6 btn-primary"><i class="glyphicon glyphicon-question-sign"></i> Sobre</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-4">
                        <label class="small">Resultado</label>
                        <select name="id_resultado_acao" class="form-control">
                            <option value="0">Selecione</option>
                            @if(count($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->resultado_acao)>0)
                                @foreach($alienacao->alienacao_pedido->andamento_alienacao[0]->acao_etapa->resultado_acao as $resultado)
                                    <option value="{{$resultado->id_resultado_acao}}">{{$resultado->no_resultado}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div id="resultado-acao">
                    <div class="carregando text-center" style="display:none"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
            @endif
        </div>
        <div class="fieldset-group clearfix">
            <div class="col-md-12">
                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="{{($alienacao->alienacao_pedido->andamento_alienacao[0]->id_resultado_acao>0?'Iniciar andamento':'Salvar resultado')}}" /> 
                </div>
            </div>
        </div>
    </form>
<?php
}
?>