<input type="hidden" name="alienacao_token" value="{{$alienacao_token}}">
<input type="hidden" name="id_alienacao" value="{{$request->id_alienacao}}">
<div class="erros-parcela alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados da parcela</legend>
        <div class="col-md-12">
            <div class="form-group">
                <label class="small">Número da parcela</label>
                <input type="text" name="nu_parcela" class="numero form-control" />
            </div>
            <div class="form-group">
                <label class="small">Data de vencimento</label>
                <input type="text" name="dt_vencimento" class="data form-control" />
            </div>
            <div class="form-group">
                <label class="small">Valor da parcela</label>
                <input type="text" name="va_valor" class="real form-control" />
            </div>
            <div class="form-group">
                <button type="button" class="cancelar-parcela btn btn-info pull-left">Cancelar</button>
                <button type="button" class="inserir-parcela btn btn-info pull-right">Inserir parcela</button>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Parcelas</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="nova-parcelas" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Número da parcela</th>
                            <th>Data de vencimento</th>
                            <th>Valor</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>