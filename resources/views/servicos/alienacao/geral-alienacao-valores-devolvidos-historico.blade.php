@if (count($alienacoes_valores_devolvidas)>0)
    <div id="alert" class="alert alert-info">
        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
        <div class="menssagem clearfix" style="overflow:initial;">
            <span class="total">0 notificações selecionadas</span>
            <div class="pull-right">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-primary disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                        Ações com selecionadas
                    </button>
                    <button type="button" class="btn btn-primary dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="disabled"><a href="#" data-toggle="modal" data-target="#relatorio-valores-devolvidos" class="gerar-relatorio-todas" disabled>Gerar relatório das selecionadas <span></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
            <tr class="gradient01">
                <th width="2%">
                    <div class="checkbox checkbox-primary">
                        <input id="selecionar-todas" class="styled" type="checkbox">
                        <label for="selecionar-todas"></label>
                    </div>
                </th>
                <th>Protocolo</th>
                <th>Produto</th>
                @if($class->id_tipo_pessoa == 9)
                    <th>Serventia</th>
                @endif
                <th>Data da devolução</th>
                <th>Valor da devolução</th>
                <th>Justificativa</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
        @forelse($alienacoes_valores_devolvidas as $alienacao_valor)
            <tr>
                <td>
                    <div class="checkbox checkbox-primary">
                        <input id="alienacao-{{$alienacao_valor->alienacao->id_alienacao}}" class="alienacao" class="styled" type="checkbox" value="{{$alienacao_valor->alienacao->id_alienacao}}">
                        <label for="alienacao-{{$alienacao_valor->alienacao->id_alienacao}}"></label>
                    </div>
                </td>
                <td><input type="hidden" name="id_alienacao[]" class="id_alienacao" value="{{$alienacao_valor->alienacao->id_alienacao}}" />{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
                <td>{{$alienacao_valor->no_produto_item}}</td>
                @if($class->id_tipo_pessoa == 9)
                    <td>{{ $alienacao_valor->alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa }}</td>
                @endif
                <td>{{\Carbon\Carbon::parse($alienacao_valor->dt_repasse_devolucao)->format('d/m/Y')}}</td>
                <td><span class="real">{{$alienacao_valor->va_repasse_devolucao}}</span></td>
                <td>{{$alienacao_valor->de_repasse_devolucao}}</td>
                <td class="options">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao_valor->alienacao->id_alienacao}}" data-protocolo="{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}">detalhes</button>
                        @if ($alienacao_valor->alienacao->observacoes_nao_lidas()>0)
                            <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idalienacao="{{$alienacao_valor->alienacao->id_alienacao}}" data-protocolo="{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}" title="{{$alienacao_valor->alienacao->observacoes_nao_lidas()}} {{($alienacao_valor->alienacao->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                {{$alienacao_valor->alienacao->observacoes_nao_lidas()}}
                            </a>
                        @endif
                        @if ($class->id_tipo_pessoa==8 && $alienacao->verifica_endereco_duplicado()==="S" && in_array($alienacao_valor->alienacao->verifica_acao_etapa(),[1,81]))
                            <a href="#" class="badge-ed badge badge-end-duplic btn-tooltip" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao_valor->alienacao->id_alienacao}}" data-protocolo="{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}" title="Possui endereço duplicado">
                                ED
                            </a>
                        @endif
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="8">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhuma notificação foi encontrada.
                        </div>
                    </div>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<div class="col-md-12">
    Exibindo <b>{{count($alienacoes_valores_devolvidas)}}</b> de <b>{{$total_alienacoes_valores_devolvidos}}</b> {{(count($alienacoes_valores_devolvidas)>1?'notificações':'notificação')}}.
</div>
<div align="center">
    {{$alienacoes_valores_devolvidas->links()}}
</div>
