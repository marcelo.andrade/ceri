@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.arquivos.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.imovel.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.detalhes.andamento.js')}}?v=<?=time();?>"></script>
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.alienacao.devolucao.valor.produto.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="container">
        @if(in_array($class->id_tipo_pessoa, [2]))
        <div class="panel panel-default">
            <div class="panel-heading  gradient01">
                <h4>Notificação de alienação fiduciária <span class="small">/ Devolução de produto (Diligência não efetuada)</span></h4>
            </div>
            <div id="filtro-devolver-valor" class="panel-body">
                <form name="form-filtro" id="form-filtro" method="post" action="{{URL::to('servicos/alienacao/devolucao-valor')}}" class="clearfix">
                    {{csrf_field()}}
                    @include('servicos.alienacao.geral-alienacao-devolver-valor-filtro')
                </form>
                @include('servicos.alienacao.geral-alienacao-devolver-valor-historico')
            </div>
        </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading  gradient01">
                <h4>Notificação de alienação fiduciária <span class="small">/ Devolução de produto (Diligência não efetuada) / Devoluções efetuadas</span></h4>
            </div>
            <div id="filtro-valores-devolvidos" class="panel-body">
                <form name="form-filtro" id="form-filtro" method="post" action="{{URL::to('servicos/alienacao/devolucao-valor')}}#pedidos" class="clearfix">
                    {{csrf_field()}}
                    @include('servicos.alienacao.geral-alienacao-valores-devolvidos-filtro')
                </form>
                @include('servicos.alienacao.geral-alienacao-valores-devolvidos-historico')
            </div>
        </div>

    </div>
    <div id="detalhes-alienacao" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="previsualizar" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Orçamento da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form carregando"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="aceitar-orcamento btn btn-success pull-right">Aceitar orçamento</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-imovel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes do imóvel - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-andamento" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes andamento da alienação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="detalhes-custas" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalhes das custas - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="relatorio-valores-devolvidos" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Relatório de notificações / Devolução de produto <span></span> / Devolução efetuadas</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="salvar-relatorio btn btn-success pull-right" style="display: none">Salvar relatório</button>
                        </div>
                    </div>
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="salvar-relatorio btn btn-success">Salvar relatório</button>
                </div>
            </div>
        </div>
    </div>
    <div id="visualizar-arquivo-caixa" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Recibo Alienação Caixa<span></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="observacoes" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Observações da notificação - <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="devolver-valor-produto" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Notificação de alienação fiduciária / Devolução de produto <span></span></h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" class="enviar-comprovante btn btn-success">Enviar comprovante</button>
                </div>
            </div>
        </div>
    </div>
    @include('arquivos.arquivos-modais')
@endsection