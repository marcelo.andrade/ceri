<?php
$query_string = http_build_query($request->except(['_token','ordby','ord']));
?>
<div class="panel table-rounded">
    <table id="pedidos" class="table table-striped table-bordered small">
        <thead>
        <tr class="gradient01">
            <th width="13%">Protocolo</th>
            <th width="11%">
                <?php
                if ($request->ordby=='data') {
                    switch ($request->ord) {
                        case 'asc':
                            $ord = 'desc';
                            $icone = 'fa fa-chevron-up';
                            break;
                        case 'desc': default:
                        $ord = 'asc';
                        $icone = 'fa fa-chevron-down';
                        break;
                    }
                } else {
                    $ord = 'desc';
                    $icone = 'fa fa-chevron-down';
                }
                ?>
                <a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=data&ord={{$ord}}">
                    <span class="pull-left">Data do pedido</span>
                    <i class="pull-right {{$icone}}"></i>
                </a>
            </th>
            <th width="8%">Contrato</th>
            <th width="14%">Status</th>
            <th width="20%">Etapa Atual</th>
            <th width="17%">
                <?php
                if ($request->ordby=='ultima-mov') {
                    switch ($request->ord) {
                        case 'asc':
                            $ord = 'desc';
                            $icone = 'fa fa-chevron-up';
                            break;
                        case 'desc': default:
                        $ord = 'asc';
                        $icone = 'fa fa-chevron-down';
                        break;
                    }
                } else {
                    $ord = 'desc';
                    $icone = 'fa fa-chevron-down';
                }
                ?>
                <a href="{{$request->url()}}?{{$query_string.($query_string?'&':'')}}ordby=ultima-mov&ord={{($request->ordby=='ultima-mov'?$ord:'desc')}}">
                    <span class="pull-left">Data última movimentação</span>
                    <i class="pull-right {{$icone}}"></i>
                </a>
            </th>
            <th width="14%">Ações</th>
        </tr>
        </thead>
        <tbody>
            @forelse($alienacoes_valores as $alienacao_valor)
                <tr>
                    <td>
                        {{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}
                    </td>
                    <td>{{\Carbon\Carbon::parse($alienacao_valor->alienacao->alienacao_pedido->pedido->dt_pedido)->format('d/m/Y H:i')}}</td>
                    <td>{{$alienacao_valor->alienacao->numero_contrato}}</td>
                    <td>
                        @if ($alienacao_valor->alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto == $class::ID_SITUACAO_CANCELADO)
                            @if ($alienacao_valor->alienacao->checar_cancelamento_pagamento() == NULL)
                                Processo Finalizado (Cancelado)
                            @else
                                {{$alienacao_valor->alienacao->no_situacao_pedido_grupo_produto}}
                            @endif
                        @else
                            {{$alienacao_valor->alienacao->alienacao_pedido->pedido->situacao_pedido_grupo_produto->no_situacao_pedido_grupo_produto}}
                        @endif
                    </td>
                    <td>{{$alienacao_valor->alienacao->etapa_atual()}}</td>
                    <td>{{Carbon\Carbon::parse($alienacao_valor->alienacao->alienacao_pedido->alienacao_andamento_situacao->dt_cadastro)->format('d/m/Y')}}</td>
                    <td class="options">
                        <?php
                        /*switch ($alienacao_valor->alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
                            case $class::ID_SITUACAO_AGUARDANDOINTERACAO:*/
                                $btn_principal = 'Detalhes';
                                $btn_class = 'btn-primary';
                            /*break;
                        default:
                            $btn_principal = 'Encaminhar';
                            $btn_class = 'btn-success';
                            break;*/
                        /*}*/
                        ?>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn {{$btn_class}}" data-toggle="modal" data-target="#detalhes-alienacao" data-idalienacao="{{$alienacao_valor->alienacao->id_alienacao}}" data-protocolo="{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}">{{$btn_principal}}</button>
                            @if ($alienacao_valor->alienacao->observacoes_nao_lidas()>0)
                                <a href="#" class="badge-observacoes badge badge-notify btn-tooltip" data-toggle="modal" data-target="#observacoes" data-idalienacao="{{$alienacao_valor->alienacao->id_alienacao}}" data-protocolo="{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}" title="{{$alienacao_valor->alienacao->observacoes_nao_lidas()}} {{($alienacao_valor->alienacao->observacoes_nao_lidas()>1?'Observações não lidas':'Observação não lida')}}">
                                    {{$alienacao_valor->alienacao->observacoes_nao_lidas()}}
                                </a>
                            @endif
                            <div class="btn-group" role="group">
                                <button type="button" class="btn {{$btn_class}} dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#devolver-valor-produto" data-idalienacao="{{$alienacao_valor->alienacao->id_alienacao}}" data-protocolo="{{$alienacao_valor->alienacao->alienacao_pedido->pedido->protocolo_pedido}}">Devolução de valor</a></li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
        @empty
            <tr>
                <td colspan="8">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhuma notificação foi encontrada.
                        </div>
                    </div>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
