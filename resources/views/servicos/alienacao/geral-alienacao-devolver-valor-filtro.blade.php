<fieldset class="clearfix">
    <legend>Filtro</legend>
    <div class="fieldset-group clearfix">
        <div class="col-md-3">
            <fieldset>
                <legend>Produto</legend>
                <div class="col-md-12">
                    <div class="protocolo">
                        <select name="produto" class="form-control" disabled>
                                <option value="{{$produto_item->id_produto_item}}">{{$produto_item->no_produto_item}}</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Protocolo</legend>
                <div class="col-md-12">
                    <div class="protocolo">
                        <input type="text" name="protocolo_pedido" class="form-control"
                               value="{{$request->protocolo_pedido}}"/>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <div class="buttons col-md-12 text-right">
                <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar"/>
                <input type="submit" class="btn btn-success" value="Filtrar"/>
            </div>
        </div>
    </div>
</fieldset>