<input type="hidden" name="alienacao_token" value="{{$alienacao_token}}">
<div class="erros-devedor alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados do devedor</legend>
        <div class="col-md-12">
			<div class="form-group">
                <div class="radio radio-inline">
                    <input type="radio" name="tp_pessoa" id="tp_pessoa_f" value="F">
                    <label for="tp_pessoa_f" class="small">
                        CPF
                    </label>
                </div>
                <div class="radio radio-inline">
                    <input type="radio" name="tp_pessoa" id="tp_pessoa_j" value="J">
                    <label for="tp_pessoa_j" class="small">
                        CNPJ
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="small">Documento identificador <span></span></label>
                <input type="text" name="nu_cpf_cnpj" class="form-control" disabled="disabled" />
            </div>
            <div class="form-group">
                <label class="small">Nome do devedor</label>
                <input type="text" name="no_devedor" class="form-control" maxlength="60"/>
            </div>
            <div class="form-group">
                <button type="button" class="cancelar-devedor btn btn-info pull-left">Cancelar</button>
                <button type="button" class="inserir-devedor btn btn-info pull-right">Inserir devedor</button>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group">
    <fieldset>
        <legend>Devedores</legend>
        <div class="col-md-12">
            <div class="panel table-rounded">
                <table id="novo-devedores" class="table table-striped table-bordered small">
                    <thead>
                        <tr class="gradient01">
                            <th>Documento identificador</th>
                            <th>Nome</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>