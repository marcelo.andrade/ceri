<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Detalhes da ação</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="alert alert-info small single">
                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                    <div class="menssagem">Ação: <b>{{$andamento->acao_etapa->no_acao}}</b>, inserido em {{\Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i:s')}}</div>
                </div>
            </div>
        </div>
        @if($andamento->acao_etapa->tp_texto_curto_acao>0 or $andamento->acao_etapa->tp_data_acao>0 or $andamento->acao_etapa->tp_valor_acao>0)
            <div class="form-group clearfix">
                @if($andamento->acao_etapa->tp_texto_curto_acao>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->acao_etapa->lb_texto_curto_acao}}</label>
                        <input type="text" name="de_texto_curto_acao" class="form-control" rows="4" value="{{$andamento->de_texto_curto_acao}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->acao_etapa->tp_valor_acao>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->acao_etapa->lb_valor_acao}}</label>
                        <input type="text" name="va_valor_acao" class="real form-control" value="{{$andamento->va_valor_acao}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->acao_etapa->tp_data_acao>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->acao_etapa->lb_data_acao}}</label>
                        <input type="text" name="dt_acao" class="data form-control" value="{{\Carbon\Carbon::parse($andamento->dt_acao)->format('d/m/Y')}}" disabled="disabled" />
                    </div>
                @endif
            </div>
        @endif
        @if($andamento->acao_etapa->tp_texto_longo_acao>0)
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">{{$andamento->acao_etapa->lb_texto_longo_acao}}</label>
                    <textarea name="de_texto_longo_acao" class="form-control" rows="4" disabled="disabled">{{$andamento->de_texto_longo_acao}}</textarea>
                </div>
            </div>
        @endif
        @if(count($arquivos_acao)>0)
            <div id="documento-isencao" class="form-group col-md-12 clearfix">
                <fieldset>
                    <legend>{{$andamento->acao_etapa->lb_upload_acao}}</legend>
                    <div id="arquivos-isencao" class="btn-list col-md-12">
                            <div class="col-md-12">
                                <div class="panel table-rounded">
                                    <table id="historico-pedido" class="table table-striped table-bordered small">
                                        <thead>
                                        <tr class="gradient01">
                                            <th class="col-md-2">Data</th>
                                            <th class="col-md-5">Usuário</th>
                                            <th class="col-md-5">Arquivo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($arquivos_acao as $arquivo)
                                            <tr>
                                                <td>{{formatar_data_hora($arquivo->dt_cadastro)}}</td>
                                                <td>{{$arquivo->usuario_cad->no_usuario}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                        @if($arquivo->in_ass_digital=='S')
                                                            <button type="button" class="icone-assinatura btn btn-sm btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </fieldset>
            </div>
        @endif
    </fieldset>
</div>
@if($andamento->id_resultado_acao>0)
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Detalhes do resultado</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <div class="alert alert-info small single">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">Resultado: <b>{{$andamento->resultado_acao->no_resultado}}</b>, inserido em {{\Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i:s')}}</div>
                    </div>
                </div>
            </div>
            @if($andamento->resultado_acao->tp_texto_curto_resultado>0 or $andamento->resultado_acao->tp_data_resultado>0 or $andamento->resultado_acao->tp_valor_resultado>0)
            <div class="form-group clearfix">
                @if($andamento->resultado_acao->tp_texto_curto_resultado>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->resultado_acao->lb_texto_curto_resultado}}</label>
                        <input type="text" name="de_texto_curto_resultado" class="form-control" rows="4" value="{{$andamento->de_texto_curto_resultado}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->resultado_acao->tp_valor_resultado>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->resultado_acao->lb_valor_resultado}}</label>
                        <input type="text" name="va_valor_resultado" class="real form-control" value="{{$andamento->va_valor_resultado}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->resultado_acao->tp_data_resultado>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->resultado_acao->lb_data_resultado}}</label>
                        <input type="text" name="dt_resultado" class="data form-control" value="{{\Carbon\Carbon::parse($andamento->dt_resultado)->format('d/m/Y')}}" disabled="disabled" />
                    </div>
                @endif
            </div>
            @endif
            @if($andamento->resultado_acao->tp_texto_longo_resultado>0)
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">{{$andamento->resultado_acao->lb_texto_longo_resultado}}</label>
                        <textarea name="de_texto_longo_resultado" class="form-control" rows="4" disabled="disabled">{{$andamento->de_texto_longo_resultado}}</textarea>
                    </div>
                </div>
            @endif
            @if(count($arquivos_resultado)>0)
                <div id="documento-isencao" class="form-group col-md-12 clearfix">
                    <fieldset>
                        <legend>{{$andamento->resultado_acao->lb_upload_resultado}}</legend>
                        <div id="arquivos-isencao" class="btn-list col-md-12">
                            <div class="col-md-12">
                                <div class="panel table-rounded">
                                    <table id="historico-pedido" class="table table-striped table-bordered small">
                                        <thead>
                                        <tr class="gradient01">
                                            <th class="col-md-2">Data</th>
                                            <th class="col-md-5">Usuário</th>
                                            <th class="col-md-5">Arquivo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($arquivos_resultado as $arquivo)
                                            <tr>
                                                <td>{{formatar_data_hora($arquivo->dt_cadastro)}}</td>
                                                <td>{{$arquivo->usuario_cad->no_usuario}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                        @if($arquivo->in_ass_digital=='S')
                                                            <button type="button" class="icone-assinatura btn btn-sm btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            @endif
        </fieldset>
    </div>
@endif
@if(count($andamento->endereco_alienacao)>0)
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereços de cobrança</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="enderecos" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="90%">Novos endereços</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($andamento->endereco_alienacao as $endereco)
                            <tr>
                                <td>
                                    {{$endereco->no_endereco}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
@endif