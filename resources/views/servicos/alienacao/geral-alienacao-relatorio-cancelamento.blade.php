<form name="form-salvar-cancelamento-notificacoes" action="{{URL::to('servicos/alienacao/relatorio/cancelamento-notificacoes/salvar')}}" method="post" target="_blank">
	{{csrf_field()}}
	<div class="fieldset-group clearfix">
		<fieldset>
			<legend>Notificações para cancelamento de contrato</legend>
			<div class="form-group clearfix">
				<div class="col-md-12">
					<div class="panel table-rounded">
						<table id="pedidos" class="table table-striped table-bordered small">
							<thead>
							<tr class="gradient01">
								<th width="20%">Contrato</th>
								<th width="18%">Protocolo (CERI)</th>
								<th width="30%">Devedor(es)</th>
								<th width="12%">Valor</th>
							</tr>
							</thead>
							<tbody>
								@if(count($alienacoes)>0)
									@foreach($alienacoes as $alienacao)
										<tr>
											<td><input type="hidden" name="id_alienacao[]" class="id_alienacao" value="{{$alienacao->id_alienacao}}" />{{$alienacao->numero_contrato}}</td>
											<td>{{$alienacao->alienacao_pedido->pedido->protocolo_pedido}}</td>
											<td>
												@if(count($alienacao->alienacao_devedor)>0)
													@foreach($alienacao->alienacao_devedor as $devedor)
														<span class="label label-primary">{{$devedor->no_devedor}} ({{$devedor->nu_cpf_cnpj}})</span>
													@endforeach
												@endif
											</td>
											<td><span class="real">{{$alienacao->alienacao_valor_total()}}</span></td>
										</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</form>