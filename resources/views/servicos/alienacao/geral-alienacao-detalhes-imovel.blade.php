<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Dados do imóvel</legend>
        <div class="form-group clearfix">
            <div class="col-md-6">
                <label class="small">Matrícula do imóvel</label>
                <input type="text" name="matricula_imovel" class="form-control" value="{{$alienacao->matricula_imovel}}" disabled="disabled">
            </div>
            {{--<div class="col-md-6">
                <label class="small">Registro do imóvel</label>
                <input type="text" name="registro_imovel" class="form-control" value="{{$alienacao->registro_imovel}}" disabled="disabled">
            </div>--}}
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereço do imóvel</legend>
        <div class="col-md-12">
            <div class="form-group clearfix">
                <label class="small">Endereço</label>
                <textarea class="form-control text-uppercase" name="no_endereco" @if(!array_intersect(array(2,8),$modulos)) disabled @endif>{{$alienacao->endereco_imovel->no_endereco}}</textarea>
            </div>
        </div>
    </fieldset>
</div>