@if(in_array($arquivo_grupo_produto->no_extensao,array('pdf','jpg','png','bmp','gif')))
	@if($arquivo_grupo_produto->no_extensao=='pdf')
        <object data="{{URL::to('/arquivos/alienacao/'.$arquivo_grupo_produto->id_arquivo_grupo_produto.'/'.$arquivo_grupo_produto->no_arquivo)}}" type="application/pdf" class="resultado-pdf">
            <p>Seu navegador não tem um plugin pra PDF</p>
        </object>
	@else
    	<img src="{{URL::to('/arquivos/alienacao/'.$arquivo_grupo_produto->id_arquivo_grupo_produto.'/'.$arquivo_grupo_produto->no_arquivo)}}" class="img-responsive" />
    @endif
@else
    <div class="alert alert-warning single">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem">
            O arquivo não pode ser pré-visualizado, por favor, faça o download.
        </div>
    </div>
@endif