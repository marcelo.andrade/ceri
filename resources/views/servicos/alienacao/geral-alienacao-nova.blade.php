<?php
$id_credor_alienacao = 0;
$matricula_imovel = '';
$numero_contrato = '';
$dt_contrato = '';
if ($alienacao) {
	$id_credor_alienacao = $alienacao->credor_alienacao->credor->id_credor_alienacao;
	$matricula_imovel = $alienacao->matricula_imovel;
	$numero_contrato = $alienacao->numero_contrato;
	$dt_contrato = \Carbon\Carbon::parse($alienacao->dt_contrato)->format('d/m/Y');
}
?>
<input type="hidden" name="alienacao_token" value="{{$alienacao_token}}">
<input type="hidden" name="id_alienacao" value="{{$request->id_alienacao}}">
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>Destinatário</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">Cidade</label>
                        <select name="id_cidade" class="form-control pull-left" {{($alienacao?'disabled="disabled"':'')}}>
                        	@if(!$alienacao)
                                <option value="0">Selecione</option>
                                @if(count($cidades)>0)
                                    @foreach ($cidades as $cidade)
                                        <option value="{{$cidade->id_cidade}}" >{{$cidade->no_cidade}}</option>
                                    @endforeach
                                @endif
                            @else
	                            <option>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                            @endif
                        </select>
                    </div>
                    <div id="id_serventia" class="col-md-12">
                        <label class="small">Cartório</label>
                        <select name="id_serventia" class="form-control pull-left" disabled="disabled">
                            @if(!$alienacao)
                                <option value="0">Selecione</option>
                            @else
	                            <option>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_pessoa}}</option>
                            @endif
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Credor</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">Nome</label>
                        <select name="id_credor_alienacao" class="form-control pull-left" disabled>
                            <option value="0">Selecione</option>
                        </select>
                    </div>

                        <div class="form-group col-md-12">
                            <label class="small">Agência</label>
                            <select name="id_agencia" class="form-control pull-left" disabled>
                                @if(!$alienacao)
                                    <option value="0">Selecione</option>
                                @else
                                    <option value="{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}">{{$alienacao->credor_alienacao->credor->agencia->codigo_agencia}}</option>
                                @endif
                            </select>
                        </div>


                   {{-- <div class="col-md-4">
                        <label class="small">&nbsp;</label>
                        <button type="button" class="incluir-credor btn btn-success btn-sm btn-block" data-toggle="modal" data-target="#novo-credor">Novo credor</button>
                    </div>--}}
                </div>

            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="row">
        <div class="col-md-8">
            <fieldset>
                <legend>Dados da alienação</legend>
                <div class="form-group">
                    <div class="col-md-3">
                        <label class="small">Número do contrato</label>
                        <input type="text" name="numero_contrato" class="form-control " value="{{$numero_contrato}}" />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor da dívida</label>
                        <input type="text" name="va_divida" class="form-control real" value="{{$alienacao->va_divida or ''}}" />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Valor do leilão</label>
                        <input type="text" name="va_leilao" class="form-control real" value="{{$alienacao->va_leilao or ''}}" />
                    </div>
                    <div class="col-md-3">
                        <label class="small">Código legado</label>
                        <input type="text" name="id_legado" class="form-control " value="{{$alienacao->id_legado or ''}}" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset>
                <legend>Dados do imóvel</legend>
                <div id="matricula_imovel" class="col-md-12">
                    <label class="small">Matrícula do imóvel</label>
                    <div class="input-group">
                        <input type="text" name="matricula_imovel" class="form-control" readonly value="{{$matricula_imovel}}" />
                        <div class="input-group-btn">
                            <button type="button" class="remover-imovel btn btn-danger btn-sm" data-alienacaotoken="{{$alienacao_token}}" {{(!$alienacao?'style=display:none':'')}}>Remover</button>
                            <button type="button" class="{{($alienacao?'alterar-imovel':'inserir-imovel')}} btn btn-success btn-sm" data-toggle="modal" data-target="#novo-imovel" data-acao="N" data-alienacaotoken="{{$alienacao_token}}">{{($alienacao?'Alterar imóvel':'Inserir imóvel')}}</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

    </div>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Projeção</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">

                    <div class="form-group">
                        <div class="col-md-3">
                            <label class="small">Valor do período (1 a 15 dias)</label>
                            <input type="text" name="va_periodo_01" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_01 or ''}}" />
                        </div>
                        <div class="col-md-3">
                            <label class="small">Valor do período (16 a 30 dias)</label>
                            <input type="text" name="va_periodo_02" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_02 or ''}}" />
                        </div>
                        <div class="col-md-3">
                            <label class="small">Valor do período (31 a 45 dias)</label>
                            <input type="text" name="va_periodo_03" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_03 or ''}}" />
                        </div>
                        <div class="col-md-3">
                            <label class="small">Valor do período (46 a 60 dias)</label>
                            <input type="text" name="va_periodo_04" class="form-control real" value="{{$alienacao->alienacao_projecao->va_periodo_04 or ''}}" />
                        </div>
                    </div>

            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Devedores</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <button type="button" class="novo-devedor btn btn-success pull-left" data-toggle="modal" data-target="#novo-devedor" data-alienacaotoken="{{$alienacao_token}}">Novo devedor</button>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="devedores" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="20%">CPF/CNPJ</th>
                            <th width="70%">Nome</th>
                            <th width="10%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@if($alienacao)
                            	@if(count($alienacao->alienacao_devedor)>0)
                                	<?php
									$i=0;
									?>
                                    @foreach($alienacao->alienacao_devedor as $devedor)
                                        <tr id="{{$i}}">
                                            <td>
                                            	<input type="hidden" name="devedores_nu_cpf_cnpj[]" class="devedores_nu_cpf_cnpj" value="{{$devedor->nu_cpf_cnpj}}" />
                                            	{{$devedor->nu_cpf_cnpj}}
                                            </td>
                                            <td>{{$devedor->no_devedor}}</td>
                                            <td><a href="#" class="remover-devedor btn btn-black btn-sm" data-linha="{{$i}}" data-alienacaotoken="{{$alienacao_token}}">Remover</a></td>
                                        </tr>
                                        <?php
										$i++;
										?>
                                    @endforeach
                                @endif
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereços de cobrança</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <button type="button" class="novo-endereco btn btn-success pull-left" data-toggle="modal" data-target="#novo-endereco" data-alienacaotoken="{{$alienacao_token}}">Novo endereço</button>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="enderecos" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="90%">Endereço</th>
                            <th width="10%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($alienacao)
                            @if(count($alienacao->endereco_alienacao)>0)
                                <?php
                                $i=0;
                                ?>
                                @foreach($alienacao->endereco_alienacao as $endereco)
                                    <tr id="{{$i}}" {{($endereco->id_endereco_alienacao==$alienacao->id_endereco_imovel?'class=endereco_imovel':'')}}>
                                        <td class="endereco">
                                            {{$endereco->no_endereco}}{{($endereco->nu_numero?' Nº '.$endereco->nu_numero:'')}}{{($endereco->no_complemento?', '.$endereco->no_complemento:'')}}{{($endereco->no_bairro?' - '.$endereco->no_bairro:'')}}. {{$endereco->cidade->no_cidade or ''}}{{($endereco->nu_cep?' - '.$endereco->nu_cep:'')}}
                                        </td>
                                        <td class="options">
                                            <a href="#" class="{{($endereco->id_endereco_alienacao==$alienacao->id_endereco_imovel?'disabled':'remover-endereco')}} btn btn-black btn-sm" data-linha="{{$i}}" data-alienacaotoken="{{$alienacao_token}}">Remover</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                    ?>
                                @endforeach
                            @endif
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Arquivos</legend>
        <div id="arquivos" class="col-md-12">
            @if($alienacao)
                @if(count($alienacao->arquivos_grupo)>0)
                    @foreach($alienacao->arquivos_grupo as $arquivo)
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                            <input type="hidden" name="no_arquivo_atual[]" value="{{$arquivo->no_arquivo}}" />
                            @if($arquivo->in_ass_digital=='S')
                                <button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                            @endif
                            <button type="button" class="remover-arquivo btn btn-danger disabled" disabled><i class="fa fa-times"></i></button>
                        </div>
                    @endforeach
                @endif
            @endif
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="1" data-token="{{$alienacao_token}}" data-limite="0">Adicionar arquivo</button>
        </div>
    </fieldset>
</div>
<div id="assinaturas-arquivos" class="fieldset-group clearfix">
    <div class="col-md-12">
        <div class="alert alert-warning single">
            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
            <div class="menssagem clearfix">
                <span>Nenhum arquivo foi inserido.</span><br />
                <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$alienacao_token}}">Assinar arquivo(s)</a>
            </div>
        </div>
    </div>
</div>
@if($alienacao)
    <script>
        carrega_credores(<?=$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->id_cidade;?>,<?=$id_credor_alienacao;?>);
        total_parcelas = <?=count($alienacao->parcela_alienacao);?>;
        total_devedores = <?=count($alienacao->alienacao_devedor);?>;
        total_enderecos = <?=count($alienacao->endereco_alienacao);?>;
    </script>
@endif