<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Detalhes da ação</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="alert alert-info small single">
                    <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                    <div class="menssagem">Ação: <b>{{$andamento->acao_etapa->no_acao}}</b>, inserido em {{\Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i:s')}}</div>
                </div>
            </div>
        </div>
        @if($andamento->acao_etapa->tp_texto_curto_acao>0 or $andamento->acao_etapa->tp_data_acao>0 or $andamento->acao_etapa->tp_valor_acao>0)
            <div class="form-group clearfix">
                @if($andamento->acao_etapa->tp_texto_curto_acao>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->acao_etapa->lb_texto_curto_acao}}</label>
                        <input type="text" name="de_texto_curto_acao" class="form-control" rows="4" value="{{$andamento->de_texto_curto_acao}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->acao_etapa->tp_valor_acao>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->acao_etapa->lb_valor_acao}}</label>
                        <input type="text" name="va_valor_acao" class="real form-control" value="{{$andamento->va_valor_acao}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->acao_etapa->tp_data_acao>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->acao_etapa->lb_data_acao}}</label>
                        <input type="text" name="dt_acao" class="data form-control" value="{{\Carbon\Carbon::parse($andamento->dt_acao)->format('d/m/Y')}}" disabled="disabled" />
                    </div>
                @endif
            </div>
        @endif
        @if($andamento->acao_etapa->tp_texto_longo_acao>0)
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <label class="small">{{$andamento->acao_etapa->lb_texto_longo_acao}}</label>
                    <textarea name="de_texto_longo_acao" class="form-control" rows="4" disabled="disabled">{{$andamento->de_texto_longo_acao}}</textarea>
                </div>
            </div>
        @endif
        @if(count($arquivos_acao)>0)
            <div id="documento-isencao" class="form-group col-md-12 clearfix">
                <fieldset>
                    <legend>{{$andamento->acao_etapa->lb_upload_acao}}</legend>
                    <div id="arquivos-isencao" class="btn-list col-md-12">
                            <div class="col-md-12">
                                <div class="panel table-rounded">
                                    <table id="historico-pedido" class="table table-striped table-bordered small">
                                        <thead>
                                        <tr class="gradient01">
                                            <th class="col-md-2">Data</th>
                                            <th class="col-md-5">Usuário</th>
                                            <th class="col-md-5">Arquivo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($arquivos_acao as $arquivo)
                                            <tr>
                                                <td>{{formatar_data_hora($arquivo->dt_cadastro)}}</td>
                                                <td>{{$arquivo->usuario_cad->no_usuario}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                        @if($arquivo->in_ass_digital=='S')
                                                            <button type="button" class="icone-assinatura btn btn-sm btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </fieldset>
            </div>

        @endif
    </fieldset>
</div>
@if($andamento->id_resultado_acao>0)
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Detalhes do resultado</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <div class="alert alert-info small single">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">Resultado: <b>{{$andamento->resultado_acao->no_resultado}}</b>, inserido em {{\Carbon\Carbon::parse($andamento->dt_cadastro)->format('d/m/Y H:i:s')}}</div>
                    </div>
                </div>
            </div>
            <?php
            /*
            switch ($andamento->resultado_acao->co_resultado) {
                case 9: case 52:
            ?>
                    <div class="form-group clearfix">
                        <div class="col-md-4">
                            <label class="small">Serventia RTD</label>
                            <select name="id_serventia" class="form-control">
                                <option value="0">Selecione</option>
                                @if(count($variaveis['rtds'])>0)
                                    @foreach($variaveis['rtds'] as $rtd)
                                        <option value="{{$rtd->id_serventia}}">{{$rtd->no_serventia}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
            <?php
                    break;
            }*/
            ?>
            @if($andamento->resultado_acao->tp_texto_curto_resultado>0 or $andamento->resultado_acao->tp_data_resultado>0 or $andamento->resultado_acao->tp_valor_resultado>0)
            <div class="form-group clearfix">
                @if($andamento->resultado_acao->tp_texto_curto_resultado>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->resultado_acao->lb_texto_curto_resultado}}</label>
                        <input type="text" name="de_texto_curto_resultado" class="form-control" rows="4" value="{{$andamento->de_texto_curto_resultado}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->resultado_acao->tp_valor_resultado>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->resultado_acao->lb_valor_resultado}}</label>
                        <input type="text" name="va_valor_resultado" class="real form-control" value="{{$andamento->va_valor_resultado}}" disabled="disabled" />
                    </div>
                @endif
                @if($andamento->resultado_acao->tp_data_resultado>0)
                    <div class="col-md-4">
                        <label class="small">{{$andamento->resultado_acao->lb_data_resultado}}</label>
                        <input type="text" name="dt_resultado" class="data form-control" value="{{\Carbon\Carbon::parse($andamento->dt_resultado)->format('d/m/Y')}}" disabled="disabled" />
                    </div>
                @endif
            </div>
            @endif
            @if($andamento->resultado_acao->tp_texto_longo_resultado>0)
                <div class="form-group clearfix">
                    <div class="col-md-12">
                        <label class="small">{{$andamento->resultado_acao->lb_texto_longo_resultado}}</label>
                        <textarea name="de_texto_longo_resultado" class="form-control" rows="4" disabled="disabled">{{$andamento->de_texto_longo_resultado}}</textarea>
                    </div>
                </div>
            @endif
            @if(count($arquivos_resultado)>0)
                <div id="documento-isencao" class="form-group col-md-12 clearfix">
                    <fieldset>
                        <legend>{{$andamento->resultado_acao->lb_upload_resultado}}</legend>
                        <div id="arquivos-isencao" class="btn-list col-md-12">
                            <div class="col-md-12">
                                <div class="panel table-rounded">
                                    <table id="historico-pedido" class="table table-striped table-bordered small">
                                        <thead>
                                        <tr class="gradient01">
                                            <th class="col-md-2">Data</th>
                                            <th class="col-md-5">Usuário</th>
                                            <th class="col-md-5">Arquivo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($arquivos_resultado as $arquivo)
                                            <tr>
                                                <td>{{formatar_data_hora($arquivo->dt_cadastro)}}</td>
                                                <td>{{$arquivo->usuario_cad->no_usuario}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#visualizar-arquivo" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}" data-noextensao="{{$arquivo->no_extensao}}">{{$arquivo->no_arquivo}}</button>
                                                        @if($arquivo->in_ass_digital=='S')
                                                            <button type="button" class="icone-assinatura btn btn-sm btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="{{$arquivo->id_arquivo_grupo_produto}}" data-noarquivo="{{$arquivo->no_arquivo}}"><i class="fa fa-lock"></i></button>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            @endif
        </fieldset>
    </div>
@endif

@if(count($andamento->endereco_alienacao)>0)
<div class="fieldset-group clearfix">
    <fieldset>
        <legend>Endereços de cobrança</legend>
        <div class="form-group clearfix">
            <div class="col-md-12">
                <div class="panel table-rounded">
                    <table id="enderecos" class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th width="90%">Novos endereços</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($andamento->endereco_alienacao as $endereco)
                            <tr>
                                <td>
                                    {{$endereco->no_endereco}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </fieldset>
</div>
@endif

@if(array_intersect(array(8,2),$class->modulos))
    @if($andamento->resultado_acao->tp_upload_resultado > 0 || $andamento->acao_etapa->tp_upload_acao > 0) 
        @php
            $origem_arquivo = ($andamento->acao_etapa->tp_upload_acao>0)?'acao':'resultado';
        @endphp
        <div class="fieldset-group clearfix">
            <form name="form-novo-andamento" method="post" action="">
                <input name="id_alienacao" value="{{$andamento->alienacao_pedido->id_alienacao}}" type="hidden">
                <input name="id_andamento_alienacao" value="{{$andamento->id_andamento_alienacao}}" type="hidden">
                <input name="id_acao_etapa" value="{{$andamento->acao_etapa->id_acao_etapa}}" type="hidden">
                <input name="origem_arquivo" value="{{$origem_arquivo}}" type="hidden">
                <input name="andamento_detalhe_token" value="{{$token}}" type="hidden">

                <div class="erros-andamento-detalhe alert alert-danger" style="display:none">
                    <i class="icon glyphicon glyphicon-remove pull-left"></i>
                    <div class="menssagem"></div>
                </div>

                <div class="form-group clearfix">
                    <fieldset>
                        <legend class="small">Inserir novo arquivo</legend>
                        <div class="col-md-12">
                            <div id="arquivos-andamento" class="arquivos obrigatorio btn-list" title="novos arquivos">
                                <button type="button" class="novo-arquivo btn btn-success" data-toggle="modal" data-target="#novo-arquivo" data-idtipoarquivo="22" data-token="{{$token}}" data-limite="0" data-idflex="{{$andamento->id_andamento_alienacao}}">Adicionar arquivo</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div id="assinaturas-arquivos" class="form-group clearfix">
                    <div class="alert alert-warning single">
                        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                        <div class="menssagem clearfix">
                            <span>Nenhum arquivo foi inserido.</span><br>
                            <a href="#" class="btn btn-warning disabled" data-toggle="modal" data-target="#nova-assinatura" data-token="{{$token}}">Assinar arquivo(s)</a>
                        </div>
                    </div>
                </div>
                <div class="fieldset-group clearfix">
                    <div class="col-md-12">
                        <div class="pull-right" id="novos-arquivos-detalhes">
                            <input class="btn btn-success btn-enviar-novos-arquivos disabled" value="Salvar novos arquivos" type="submit">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif
@endif
@if(array_intersect(array(8,2),$class->modulos) and $andamento->remover_andamento_alienacao('V', Auth::User()->id_usuario, '') === 'S' and Auth::User()->in_usuario_master === 'S')
    <div class="fieldset-group clearfix">
        <fieldset>
            <legend>Cancelar interação da notificação</legend>
            <div class="form-group clearfix">
                <div class="col-md-12">
                    <button type="button" class="excluir-interacao btn btn-success" data-idandamentoalienacao="{{$andamento->id_andamento_alienacao}}" data-protocolo="{{$andamento->alienacao_pedido->pedido->protocolo_pedido}}">Remover interação</button>
                </div>
            </div>
        </fieldset>
    </div>
@endif
