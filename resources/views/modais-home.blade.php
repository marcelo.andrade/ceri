
<div id="pesquisa-eletronica" class="modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pesquisa eletrônica<span></span></h4>
            </div>
            <div class="modal-body">
                <div id="conteudo-acesso-token">
                    <p>
                        Esta ferramenta possibilita a pesquisa da ocorrência de determinado CPF ou CNPJ nas bases de dados de todos os Registros de Imóveis do Estado de Mato Grosso do Sul.
                        Para efetuar a pesquisa, o usuário deverá informar o CPF/CNPJ do proprietário do imóvel e os Cartórios onde a busca deverá ser realizada.
                    </p>
                    <p>
                        Caso não sejam localizadas ocorrências automáticas, o pedido será direcionado ao cartório para a busca com prazo de até 2 dias para a resposta.
                        O resultado da pesquisa eletrônica não vale como certidão. Em caso resultado positivo, o usuário poderá solicitar o serviço de certidão eletrônica.
                        A resposta ficará disponível na Central Eletrônica de Registro de Imóveis no menu "Pesquisa Eletrônica".
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="certidao-eletronica" class="modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Certidão eletrônica<span></span></h4>
            </div>
            <div class="modal-body">
                <div id="conteudo-acesso-token">
                    <p>
                        O módulo de pedido de Certidão Eletrônica possibilita ao usuário solicitar a emissão das seguintes certidões em qualquer cartório de registro de imóveis do Estado de Mato Grosso do Sul:
                    <ul>
                        <li>Inteiro teor (Matrícula)</li>
                        <li>Inteiro teor com ônus e ações reipersecutórias</li>
                        <li>Certidão Negativa de Bens</li>
                        <li>Certidão Positiva de Bens e Tríduo</li>
                    </ul>
                    A Certidão Eletrônica é expedida em formato eletrônico. Ela possui fé pública e validade jurídica. O documento poderá ser utilizado para lavratura de escrituras públicas, contratos de financiamento imobiliário e quaisquer outros documentos públicos e particulares em geral.
                    </p>
                    <p>
                        Caso não sejam localizadas ocorrências automáticas, o pedido será direcionado ao cartório para a emissão com prazo de até 2 dias para a resposta.
                        A resposta ficará disponível na Central Eletrônica de Registro de Imóveis no menu "Certidão Eletrônica".
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="matricula-digital" class="modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Matrícula digital<span></span></h4>
            </div>
            <div class="modal-body">
                <div id="conteudo-acesso-token">
                    <p>
                        Este módulo destina-se à visualização da imagem eletrônica do inteiro teor das matrículas imobiliárias.
                        Para a visualização de cada matrícula será devido o valor equivalente a um período de busca previsto na Tabela de custas judiciais, destinado ao oficial do Registro de Imóveis responsável pela serventia que lavrou o ato.
                        A resposta ficará disponível na Central Eletrônica de Registro de Imóveis no menu "Matrícula Digital".
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="protocolo-eletronico" class="modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Protocolo eletrônico<span></span></h4>
            </div>
            <div class="modal-body">
                <div id="conteudo-acesso-token">
                    <p>O Protocolo Eletrônico ou e-Protocolo é o módulo que permite às instituições financeiras solicitar o registro ou averbação de títulos, escrituras, contratos ou outros documentos, em qualquer cartório de registro de Imóveis do Estado de Mato Grosso do Sul.
                    </p>
                    <p>
                        Permite que a serventia realize o envio do arquivo digital, eliminando a necessidade de comparecer ao Ofício de Registro de Imóveis para abertura do processo de registro ou averbação. As instituições financeiras podem realizar o envio de documentos em arquivo de formato XML.
                        As transação ficará disponível na Central Eletrônica de Registro de Imóveis no menu "Protocolo Eletrônico".
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="cartorios-integrados" class="modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">cartorios ELETRÔNICA<span></span></h4>
            </div>
            <div class="modal-body">
                <div id="conteudo-acesso-token">
                    <p>cartorios por imóveis em uma base centralizada...</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="div_modal_error">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="material-icons"></i>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body text-center">
                <h4>Ooops!</h4>
                <p>
                @foreach ($errors->all() as $error)
                    <?php
                    switch ($error) {
                        case '1001':
                            echo 'Os dados digitados são inválidos.';
                            break;
                        case '1002':
                            echo 'O seu e-mail ainda não foi confirmado. <br /><br />';
                            echo '<a href="'.URL::to('/cadastrar/reenviar-confirmacao').'" class="btn btn-black">Reenviar confirmação</a>';
                            break;
                        case '1003':
                            echo 'O seu cadastro está em análise. Por favor, aguarde.';
                            break;
                        case '1004':
                            echo 'O seu cadastro foi desabilitado.';
                            break;
                        case '1005':
                            echo 'O seu usuário não possui vínculos, entre em contato com o administrador.';
                            break;
                        default:
                            echo $error;
                            break;
                    }
                    ?>
                    <br />
                @endforeach
                </p>
                <button class="acessar btn btn-primary" data-dismiss="modal">Tentar novamente</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="div_modal_warning">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header-warning">
                <div class="icon-box">
                    <i class="material-icons">info</i>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body text-center">
                <h4>Atenção</h4>
                <p>Após marcar a opção, a janela irá se fechar automaticamente. Aguarde...</p>
            </div>
            <div style="margin-left:25%" id="captcha_login">
                {!! NoCaptcha::display(['render' => 'onload', 'data-callback' => 'verifyCallbackLogin']) !!}
            </div>
        </div>
    </div>
</div>