@extends('layout.home')

@section('scripts')
@endsection

@section('content')
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
        <div class="box-start col-md-4 col-sm-6">
            <div class="default-login">
                @if (session('status')=='4000')
                    <div class="single alert alert-success">
                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                        <div class="menssagem">
                            A recuperação de senha foi enviada para o e-mail <b>{{old('usuario')}}</b>.<br /><br />
                            <a href="{{URL::to('/')}}" class="btn btn-black">Acessar o sistema</a>
                        </div>
                    </div>
                @else
                    <h2>Recuperar senha</h2>
                    <p>Digite seu e-mail para recuperar sua senha.</p>
                    <form name="form-lembrar-senha" method="post" action="lembrar-senha">
    					{{csrf_field()}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                                <div class="menssagem">
    	                            @foreach ($errors->all() as $error)
                                        <?php
                                        switch ($error) {
                                            case '4001':
                                                echo 'O e-mail digitado não foi encontrado.';
                                                break;
                                            case '4002':
                                                echo 'O e-mail digitado não foi confirmado. <br /><br >';
                                                echo '<a href="'.URL::to('/cadastrar/reenviar-confirmacao').'" class="btn btn-black">Reenviar confirmação</a>';
                                                break;
                                            case '4003':
                                                echo 'Erro nº 4003. Tente novamente mais tarde<br /><br >';
                                                break;
                                            default:
                                                echo $error;
                                                break;
                                        }
                                        ?>
                                        <br />
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="input-group">
    						<span class="input-group-addon" id="addon-mail"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="usuario" class="form-control" placeholder="E-mail" value="{{old('usuario')}}" />
                        </div>
                        <input type="submit" class="access btn btn-primary btn-block" value="Recuperar" />
                    </form>
                @endif
            </div>
        </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
@endsection