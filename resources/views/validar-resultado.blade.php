@if($pedido->pedido_resultado->id_serventia_certificado>0)
    <div class="certificado alert alert-success clearfix">
        <i class="icon glyphicon glyphicon-ok pull-left"></i>
        <div class="menssagem pull-left">
            O documento foi assinado digitalmente por:<br />
            <b>{{$pedido->pedido_resultado->serventia_certificado->no_comum}}</b><br />
            @if($pedido->nu_selo>0)
            <b>Selo utilizado:</b> {{$pedido->nu_selo}}<br />
            @endif
            <b>Data do documento:</b> {{Carbon\Carbon::parse($pedido->pedido_resultado->dt_cadastro)->format('d/m/Y')}}
        </div>
    </div>
@endif
<object data="{{URL::to('/validar-arquivo/'.$hash_protocolo.'/'.$pedido->pedido_resultado->no_arquivo)}}" type="application/pdf" class="resultado-pdf @if($pedido->pedido_resultado->id_serventia_certificado>0) assinado @endif">
    <p>Seu navegador não tem um plugin pra PDF</p>
</object>