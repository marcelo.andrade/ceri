<table id="usuarios-ativos" class="table table-striped table-bordered table-responsive small">
    <thead>
    <tr class="gradient01">
        @if(in_array($class->id_tipo_pessoa,array(7,9)))
            <th width="25%">Nome do usuário</th>
            <th width="30%">Vínculo(s)</th>
            <th width="15%">Comarca(s)</th>
            <th width="15%">Data de cadastro</th>
            <th width="15%">Ações</th>
        @else
            <th width="55%">Nome do usuário</th>
            <th width="15%">Comarca(s)</th>
            <th width="15%">Data de cadastro</th>
            <th width="15%">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @if (count($todos_usuarios)>0)
        @foreach ($todos_usuarios as $usuario)
            <tr>
                <td>{{$usuario->no_usuario}}</td>
                @if(in_array($class->id_tipo_pessoa,array(7,9)))
                    <td>
                        @if(count($usuario->usuario_pessoa)>0)
                            @foreach($usuario->usuario_pessoa as $pessoa)
                                <label class="label label-primary label-wrap">{{$pessoa->pessoa->no_pessoa}}</label>
                            @endforeach
                        @endif
                    </td>
                @endif
                @if (in_array($class->id_tipo_pessoa,array(1,2,4,6,7,8,9,10,13)))
                    <?php $comarcas = comarca($usuario->pessoa->nu_cpf_cnpj) ?>
                    <td>
                        @if (count($comarcas)>0)
                            @foreach($comarcas as $comarca)
                                <div class="vinculo btn-group">
                                    <button class="btn btn-sm btn-primary"
                                            type="button">{{$comarca->no_comarca}}</button>
                                </div>
                            @endforeach
                        @else
                            @if(count($usuario->pessoa->enderecos)>0)
                                @foreach($usuario->pessoa->enderecos as $endereco)
                                    <div class="vinculo btn-group">
                                        <button class="btn btn-sm btn-primary"
                                                type="button">{{$endereco->cidade->no_cidade}}</button>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </td>
                @endif
                <td>{{Carbon\Carbon::parse($usuario->dt_cadastro)->format('d/m/Y H:i:s')}}</td>
                <td class="options">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#detalhes-usuario" data-idusuario="{{$usuario->id_usuario}}"
                                data-nousuario="{{$usuario->no_usuario}}">Detalhes
                        </button>
                        @if(in_array($class->id_tipo_pessoa,array(1,2,4,6,7,8,10)) or ($class->id_tipo_pessoa==9 and in_array($request->id_tipo_pessoa_list,array(1,2,7,9,10))))
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#alterar-usuario"
                                           data-idusuario="{{$usuario->id_usuario}}"
                                           data-nousuario="{{$usuario->no_usuario}}">Alterar usuário</a></li>
                                    <li><a href="#" class="desativar-usuario"
                                           data-idusuario="{{$usuario->id_usuario}}"
                                           data-nousuario="{{$usuario->no_usuario}}">Desativar usuário</a></li>
                                    <li><a href="#" class="gerar-nova-senha"
                                           data-idusuario="{{$usuario->id_usuario}}"
                                           data-nousuario="{{$usuario->no_usuario}}">Gerar e enviar nova senha</a>
                                    </li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="@if(in_array($class->id_tipo_pessoa,array(7,9))) 5 @else 4 @endif">
                <div class="single alert alert-danger">
                    <i class="glyphicon glyphicon-remove"></i>
                    <div class="mensagem">
                        Nenhum usuário ativo foi encontrado.
                    </div>
                </div>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="col-md-12">
    Exibindo <b>{{count($todos_usuarios)}}</b> de <b>{{$todos_usuarios->total()}}</b> {{($todos_usuarios->total()>1?'pesquisas':'pesquisa')}}.
</div>
<div align="center">
    {{$todos_usuarios->fragment('usuarios-ativos')->render()}}
</div>
