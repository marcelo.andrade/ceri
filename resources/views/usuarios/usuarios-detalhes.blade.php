@if($usuario->in_confirmado=='N' or $usuario->in_aprovado=='N')
    <div class="alert alert-warning">
        <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
        <div class="menssagem">
            <b>Pendências:</b><br />
            @if ($usuario->in_confirmado=='N')
                Este usuário ainda não foi confirmado.<br />
            @endif
            @if ($usuario->in_aprovado=='N')
                Este usuário ainda não foi aprovado.
            @endif
        </div>
    </div>
@endif
<div class="fieldset-group clearfix">
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados pessoais</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Tipo de pessoa</label>
                        <select class="form-control" name="tp_pessoa" disabled>
                            <option value="F" @if($usuario->pessoa->tp_pessoa=='F') selected="selected" @endif>Pessoa física</option>
                            <option value="J" @if($usuario->pessoa->tp_pessoa=='J') selected="selected" @endif>Pessoa jurídica</option>
                        </select>
                    </div>
                    @if($usuario->pessoa->tp_pessoa=='F')
                        <div class="pessoa-fisica">
                            <div class="form-group">
                                <label class="small">Nome completo</label>
                                <input type="text" class="form-control" name="no_pessoa" value="{{$usuario->pessoa->no_pessoa}}" disabled>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-4">
                                    <label class="small">CPF</label>
                                    <input type="text" data-mask="999.999.999-99" class="form-control" name="nu_cpf_cnpj" value="{{$usuario->pessoa->nu_cpf_cnpj}}" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Gênero</label>
                                    <select class="form-control" name="tp_sexo" disabled>
                                        <option value="0">Selecione o gênero</option>
                                        <option value="F" @if($usuario->pessoa->tp_sexo=='F') selected="selected" @endif>Feminino</option>
                                        <option value="M" @if($usuario->pessoa->tp_sexo=='M') selected="selected" @endif>Masculino</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Data nascimento</label>
                                    <input type="text" data-mask="99/99/9999" class="form-control" name="dt_nascimento" value="{{Carbon\Carbon::parse($usuario->pessoa->dt_nascimento)->format('d/m/Y')}}" disabled>
                                </div>
                            </div>
                        </div>
                    @elseif($usuario->pessoa->tp_pessoa=='J')
                        <div class="pessoa-juridica">
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                    <label class="small">CNPJ</label>
                                    <input type="text" data-mask="99.999.999/9999-99" class="form-control" name="nu_cpf_cnpj" value="{{$usuario->pessoa->nu_cpf_cnpj}}" disabled>
                                </div>
                                <div class="col-md-6">
                                    <label class="small">Inscrição municipal</label>
                                    <input type="text" class="form-control" name="no_inscricao_municipal" value="{{$usuario->pessoa->no_inscricao_municipal}}" disabled>
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                    <label class="small">Razão social</label>
                                    <input type="text" class="form-control" name="no_pessoa" value="{{$usuario->pessoa->no_pessoa}}" disabled>
                                </div>
                                <div class="col-md-6">
                                    <label class="small">Nome fantasia</label>
                                    <input type="text" class="form-control" name="no_fantasia" value="{{$usuario->pessoa->no_fantasia}}" disabled>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Telefone</legend>
                @if(count($usuario->pessoa->telefones)>0)
                    <div class="form-group clearfix">
                        <div class="col-md-5">
                            <label class="small">Tipo de telefone</label>
                            <select class="form-control" name="id_tipo_telefone" disabled>
                                <option value="{{$usuario->pessoa->telefones[0]->tipo_telefone->id_tipo_telefone}}">{{$usuario->pessoa->telefones[0]->tipo_telefone->no_tipo_telefone}}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="small">DDD</label>
                            <input type="text" placeholder="DDD" data-mask="99" class="form-control" name="nu_ddd" value="{{$usuario->pessoa->telefones[0]->nu_ddd}}" disabled>
                        </div>
                        <div class="col-md-5">
                            <label class="small">Número do telefone</label>
                            <input type="text" data-mask="99999999?9" class="form-control" name="nu_telefone" value="{{$usuario->pessoa->telefones[0]->nu_telefone}}" disabled>
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="erros alert alert-warning single small">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem">Nenhum telefone foi cadastrado</div>
                        </div>
                    </div>
                @endif
            </fieldset>
        </div>
    </div>
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Endereço</legend>
                @if(count($usuario->pessoa->enderecos)>0)
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="small">CEP</label>
                            <input type="text" data-mask="99999-999" class="form-control" name="nu_cep" value="{{$usuario->pessoa->enderecos[0]->nu_cep}}" disabled>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-10">
                            <label class="small">Endereço</label>
                            <input type="text" class="form-control" name="no_endereco" value="{{$usuario->pessoa->enderecos[0]->no_endereco}}" disabled>
                        </div>
                        <div class="col-md-2">
                            <label class="small">Número</label>
                            <input type="text" class="form-control" name="nu_endereco" value="{{$usuario->pessoa->enderecos[0]->nu_endereco}}" disabled>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-6">
                            <label class="small">Bairro</label>
                            <input type="text" class="form-control" name="no_bairro" value="{{$usuario->pessoa->enderecos[0]->no_bairro}}" disabled>
                        </div>
                        <div class="col-md-6">
                            <label class="small">Complemento</label>
                            <input type="text" class="form-control" name="no_complemento" value="{{$usuario->pessoa->enderecos[0]->no_complemento}}" disabled>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-6">
                            <label class="small">Estado</label>
                            <select class="form-control" name="id_estado" disabled>
                                <option value="{{$usuario->pessoa->enderecos[0]->cidade->id_estado}}">{{$usuario->pessoa->enderecos[0]->cidade->estado->no_estado}}</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="small">Cidade</label>
                            <select class="form-control" name="id_cidade" disabled>
                                <option value="{{$usuario->pessoa->enderecos[0]->id_cidade}}">{{$usuario->pessoa->enderecos[0]->cidade->no_cidade}}</option>
                            </select>
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="erros alert alert-warning single small">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem">Nenhum endereço foi cadastrado</div>
                        </div>
                    </div>
                @endif
            </fieldset>

            <fieldset>
                <legend>Comarca</legend>
                @if (count($comarcas)>0)
                    @foreach($comarcas as $comarca)
                        <div class="vinculo btn-group">
                            <button class="btn btn-sm btn-primary" type="button">{{$comarca->no_comarca}}</button>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="erros alert alert-warning single small">
                            <i class="icon glyphicon glyphicon-exclamation-sign pull-left"></i>
                            <div class="menssagem">Nenhuma comarca foi cadastrada</div>
                        </div>
                    </div>
                @endif
            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados de acesso</legend>
            <div class="col-md-12">
                <div class="checkbox checkbox-inline">
                    <input type="checkbox" name="in_usuario_master" id="in_usuario_master" value="S" @if($usuario->in_usuario_master=='S') checked @endif disabled>
                    <label for="in_usuario_master" class="small">
                        Usuário Master
                    </label>
                </div>
            </div>
            <div class="col-md-12">
                <label class="small">E-mail</label>
                <input type="text" class="form-control" name="email_usuario" value="{{$usuario->email_usuario}}" disabled>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Vínculos do usuário</legend>
            <div id="vinculos-usuario" class="col-md-12">
                @if(count($usuario->usuario_pessoa)>0)
                    @foreach($usuario->usuario_pessoa as $pessoa)
                        <div class="vinculo btn-group">
                            <button class="btn btn-sm btn-primary" type="button">{{$pessoa->pessoa->no_pessoa}}</button>
                        </div> 
                    @endforeach
                @endif                
            </div>
        </fieldset>
    </div>
</div>
@if($usuario->id_cargo>0)
    <div id="cargo-usuario" class="fieldset-group clearfix" style="display: none">
        <div class="col-md-12">
            <fieldset>
                <legend>Cargo do usuário</legend>
                <div class="col-md-6">
                    <label class="small">Cargo</label>
                    <select name="id_cargo" class="form-control" disabled>
                        <option value="{{$usuario->cargo->id_cargo}}">{{$usuario->cargo->no_cargo}}</option>
                    </select>
                </div>
            </fieldset>
        </div>
    </div>
@endif