<fieldset class="clearfix">
    <legend>Filtro</legend>
    <div class="fieldset-group clearfix">
        <input type="hidden" name="id_tipo_pessoa_list" value="{{$request->id_tipo_pessoa_list}}" />
        <div class="col-md-6">
            <fieldset>
                <legend>Nome</legend>
                <div class="col-md-12">
                    <div class="protocolo">
                        <input type="text" name="nome" class="form-control" value="{{$request->nome}}" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset>
                <legend>Email</legend>
                <div class="col-md-12">
                    <input type="text" name="email" class="form-control" value="{{$request->email}}" />
                </div>
            </fieldset>
        </div>
        <div id="filtro-usuario" class="col-md-3">
            <fieldset>
                <legend>
                    <div class="radio radio-inline">
                        <input type="radio" name="id_tipo_pesquisa" id="id_tipo_pesquisa1" value="1">
                        <label for="id_tipo_pesquisa1" class="small"><strong>CPF</strong></label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="id_tipo_pesquisa" id="id_tipo_pesquisa2" value="2">
                        <label for="id_tipo_pesquisa2" class="small "><strong>CNPJ</strong></label>
                    </div>
                </legend>
                <div class="form-group">
                    <input type="text" name="cpf_cnpj" class="form-control" disabled="disabled" />
                </div>
            </fieldset>
        </div>
    </div>
    <div class="fieldset-group clearfix">
        <div class="buttons col-md-12 text-right">
            <input type="reset" class="btn btn-primary limpar-formulario" value="Limpar filtros" />
            <input type="submit" class="btn btn-success" value="Filtrar usuários"/>
        </div>
    </div>
</fieldset>