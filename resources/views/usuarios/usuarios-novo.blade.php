<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Dados pessoais</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Tipo de pessoa</label>
                        <select class="form-control" name="tp_pessoa">
                            <option value="F" selected="selected">Pessoa física</option>
                            <option value="J">Pessoa jurídica</option>
                        </select>
                    </div>
                    <div class="pessoa-fisica">
                        <div class="form-group">
                            <label class="small">Nome completo</label>
                            <input type="text" class="form-control" name="no_pessoa">
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-4">
                                <label class="small">CPF</label>
                                <input type="text" data-mask="999.999.999-99" class="form-control" name="nu_cpf_cnpj">
                            </div>
                            <div class="col-md-4">
                                <label class="small">Gênero</label>
                                <select class="form-control" name="tp_sexo">
                                    <option value="0">Selecione o gênero</option>
                                    <option value="F">Feminino</option>
                                    <option value="M">Masculino</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="small">Data nascimento</label>
                                <input type="text" data-mask="99/99/9999" class="form-control" name="dt_nascimento">
                            </div>
                        </div>
                    </div>
                    <div class="pessoa-juridica" style="display: none;">
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label class="small">CNPJ</label>
                                <input type="text" data-mask="99.999.999/9999-99" class="form-control" name="nu_cpf_cnpj" disabled>
                            </div>
                            <div class="col-md-6">
                                <label class="small">Inscrição municipal</label>
                                <input type="text" class="form-control" name="no_inscricao_municipal" disabled>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-md-6">
                                <label class="small">Razão social</label>
                                <input type="text" class="form-control" name="no_pessoa" disabled>
                            </div>
                            <div class="col-md-6">
                                <label class="small">Nome fantasia</label>
                                <input type="text" class="form-control" name="no_fantasia" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Telefone</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <input type="checkbox" name="in_digitar_telefone" id="in_digitar_telefone" value="S">
                            <label for="in_digitar_telefone" class="small">
                                Desejo digitar o telefone
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-5">
                        <label class="small">Tipo de telefone</label>
                        <select class="form-control" name="id_tipo_telefone" disabled>
                            <option value="0">Tipo de telefone</option>
                            <option value="1">Residencial</option>
                            <option value="2">Comercial</option>
                            <option value="3">Celular</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="small">DDD</label>
                        <input type="text" placeholder="DDD" data-mask="99" class="form-control" name="nu_ddd" disabled>
                    </div>
                    <div class="col-md-5">
                        <label class="small">Número do telefone</label>
                        <input type="text" data-mask="99999999?9" class="form-control" name="nu_telefone" disabled>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Endereço</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <input type="checkbox" name="in_digitar_endereco" id="in_digitar_endereco" value="S">
                            <label for="in_digitar_endereco" class="small">
                                Desejo digitar o endereço
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">CEP</label>
                        <input type="text" data-mask="99999-999" class="form-control" name="nu_cep" disabled>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-10">
                        <label class="small">Endereço</label>
                        <input type="text" class="form-control" name="no_endereco" disabled>
                    </div>
                    <div class="col-md-2">
                        <label class="small">Número</label>
                        <input type="text" class="form-control" name="nu_endereco" disabled>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Bairro</label>
                        <input type="text" class="form-control" name="no_bairro" disabled>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Complemento</label>
                        <input type="text" class="form-control" name="no_complemento" disabled>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Estado</label>
                        <select class="form-control" name="id_estado" disabled>
                            <option value="0">Selecione o estado</option>
                            @if(count($estados)>0)
                                @foreach($estados as $estado)
                                    <option value="{{$estado->id_estado}}">{{$estado->no_estado}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Cidade</label>
                        <select class="form-control" name="id_cidade" disabled>
                            <option value="0">Selecione uma cidade</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados de acesso</legend>
            <div class="col-md-12">
                <div class="checkbox checkbox-inline">
                    <input type="checkbox" name="in_usuario_master" id="in_usuario_master" value="S">
                    <label for="in_usuario_master" class="small">
                        Usuário Master
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input type="checkbox" name="in_confirmado" id="in_confirmado" value="S" checked>
                    <label for="in_confirmado" class="small">
                        Usuário confirmado
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input type="checkbox" name="in_aprovado" id="in_aprovado" value="S" checked>
                    <label for="in_aprovado" class="small">
                        Usuário aprovado
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <label class="small">E-mail</label>
                <input type="text" class="form-control" name="email_usuario">
            </div>
            <div class="col-md-6">
                <label class="small">Senha</label>
                <input type="text" class="form-control" value="A senha será gerada e enviada para o e-mail digitado acima" disabled>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Inserir vínculos do usuário</legend>
            @if(!$tipos_usuarios)
                <input type="hidden" name="in_vincular" value="S" />
                <div class="vinculo-usuario col-md-12">
                    <div class="alert alert-info single small">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">O novo usuário será vinculado ao mesmo usuário logado atualmente.</div>
                    </div>
                </div>
            @else
                <input type="hidden" name="in_vincular" value="N" />
                <div class="col-md-3">
                    <div class="tipo-usuario">
                        <label class="small">Tipo de usuário</label>
                        <select name="id_tipo_usuario" class="form-control">
                            <option value="0">Selecione</option>
                            @foreach ($tipos_usuarios as $tipo_usuario)
                                <option value="{{$tipo_usuario->id_tipo_usuario}}">{{$tipo_usuario->no_tipo_usuario}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div id="campos-serventia" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="form-group clearfix">
                        <div class="col-md-6">
                            <label class="small">Cidade</label>
                            <select name="id_cidade_serventia" class="form-control pull-left" disabled>
                                <option value="0">Selecione uma cidade</option>
                                @if(count($cidades)>0)
                                    @foreach ($cidades as $cidade)
                                        <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="small">Serventia</label>
                            <select name="id_serventia" class="form-control pull-left" disabled>
                                <option value="0">Selecione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="campos-judiciario" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="col-md-6">
                        <label class="small">Comarca</label>
                        <select class="form-control" name="id_comarca" disabled>
                            <option value="0">Selecione uma comarca</option>
                            @if(count($comarcas)>0)
                                @foreach($comarcas as $comarca)
                                    <option value="{{$comarca->id_comarca}}">{{$comarca->no_comarca}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Vara</label>
                        <select class="form-control" name="id_vara" disabled>
                            <option value="0">Selecione uma vara</option>
                        </select>
                    </div>
                </div>
                <div id="campos-unidade-gestora" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="col-md-6">
                        <label class="small">Unidade gestora</label>
                        <select name="id_unidade_gestora" class="form-control pull-left" disabled>
                            <option value="0">Selecione uma unidade</option>
                            @if(count($unidades_gestoras)>0)
                                @foreach ($unidades_gestoras as $unidade_gestora)
                                    <option value="{{$unidade_gestora->id_unidade_gestora}}">{{$unidade_gestora->no_unidade_gestora}}</option>
                                @endforeach
                            @endif
                            <option value="2">Controladoria de Mandados</option>
                        </select>
                    </div>
                    <div id="chk_notificacao_controladoria" class="col-md-6" style="display:none">
                        <label class="small">&nbsp;</label>
                        <div class="checkbox">
                            <input type="checkbox" name="in_master_recebe_notificacoes_sistema" id="in_master_recebe_notificacoes_sistema" value="S" disabled="disabled">
                            <label for="in_master_recebe_notificacoes_sistema" class="small">
                                Encaminhar notificações (email) para usuário master
                            </label>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" name="in_usuario_recebe_notificacoes_sistema" id="in_usuario_recebe_notificacoes_sistema" value="S" disabled="disabled" checked>
                            <label for="in_usuario_recebe_notificacoes_sistema" class="small">
                                Receber notificaçãos (email)
                            </label>
                        </div>
                    </div>

                </div>
                <div id="campos-unidade-judiciaria" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="col-md-6">
                        <label class="small">Unidade judiciária</label>
                        <select name="id_unidade_judiciaria" class="form-control pull-left" disabled>
                            <option value="0">Selecione uma unidade</option>
                            @if(count($unidades_judiciarias)>0)
                                @foreach ($unidades_judiciarias as $unidade_judiciaria)
                                    <option value="{{$unidade_judiciaria->id_unidade_judiciaria}}">{{$unidade_judiciaria->no_unidade_judiciaria}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Subunidade</label>
                        <select name="id_unidade_judiciaria_divisao" class="form-control pull-left" disabled>
                            <option value="0">Selecione</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>&nbsp;</label>
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="incluir-vinculo disabled btn btn-default btn-inline" type="button" disabled>Incluir</button>
                        </div>
                        <div class="btn-group">
                            <button class="incluir-todos-vinculos disabled btn btn-primary btn-inline" type="button" disabled>Incluir todos</button>
                        </div>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
@if($tipos_usuarios)
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Vínculos do usuário</legend>
                <div id="vinculos-usuario" class="col-md-12"></div>
            </fieldset>
        </div>
    </div>
    <div id="cargo-usuario" class="fieldset-group clearfix" style="display: none">
        <div class="col-md-12">
            <fieldset>
                <legend>Cargo do usuário</legend>
                <div class="col-md-6">
                    <label class="small">Cargo</label>
                    <select name="id_cargo" class="form-control" disabled>
                        <option value="0">Selecione um cargo</option>
                        @if(count($cargos)>0)
                            @foreach($cargos as $cargo)
                                <option value="{{$cargo->id_cargo}}">{{$cargo->no_cargo}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </fieldset>
        </div>
    </div>
@endif