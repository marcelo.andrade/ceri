@extends('layout.comum')

@section('scripts')
	<script type="text/javascript" src="{{asset('js/jquery.functions.pesquisaeletronica.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.funcoes.processo.js?v=1.0.0')}}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading gradient01">
                        <h4>Editar Perfil</h4>
                    </div>
                    <div class="panel-body">
                        <div class="container">

                            <div class="row">
                                <!-- edit form column -->
                                <div class="col-md-11 col-sm-6 col-xs-12 personal-info">
                                    <div class="alert alert-info alert-dismissable">
                                        <a class="panel-close close" data-dismiss="alert">×</a>
                                        <i class="fa fa-coffee"></i>
                                        This is an <strong>.alert</strong>. Use this to show important messages to the user.
                                    </div>
                                    <h3>Informações Pessoais</h3><br>
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Nome:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" value="Jane" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Sobrenome:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" value="Bishop" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Endereço:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" value="" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Email:</label>
                                            <div class="col-lg-8">
                                                <input class="form-control" value="janesemail@gmail.com" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Time Zone:</label>
                                            <div class="col-lg-8">
                                                <div class="ui-select">
                                                    <select id="user_time_zone" class="form-control">
                                                        <option value="Hawaii">(GMT-10:00) Hawaii</option>
                                                        <option value="Alaska">(GMT-09:00) Alaska</option>
                                                        <option value="Pacific Time (US &amp; Canada)">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                                                        <option value="Arizona">(GMT-07:00) Arizona</option>
                                                        <option value="Mountain Time (US &amp; Canada)">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                                                        <option value="Central Time (US &amp; Canada)" selected="selected">(GMT-06:00) Central Time (US &amp; Canada)</option>
                                                        <option value="Eastern Time (US &amp; Canada)">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                                                        <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Username:</label>
                                            <div class="col-md-8">
                                                <input class="form-control" value="janeuser" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Password:</label>
                                            <div class="col-md-8">
                                                <input class="form-control" value="11111122333" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Confirm password:</label>
                                            <div class="col-md-8">
                                                <input class="form-control" value="11111122333" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"></label>
                                            <div class="col-md-8">
                                                <input class="btn btn-primary" value="Save Changes" type="button">
                                                <span></span>
                                                <input class="btn btn-default" value="Cancel" type="reset">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            /*
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading gradient01">
                        <h4>Pesquisa eletrônica</h4>
                    </div>
                    <div class="panel-body">
                        <form name="form-eletronic-search" method="post" action="">
                            <div class="form-group">
                                <div class="radio radio-inline">
                                    <input type="radio" name="tipo" id="matricula">
                                    <label for="matricula" class="small">
                                        Matrícula
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" name="tipo" id="cpf">
                                    <label for="cpf" class="small">
                                        CPF
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" name="tipo" id="cpf">
                                    <label for="cpf" class="small">
                                        CNPJ
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <input type="radio" name="tipo" id="cpf">
                                    <label for="cpf" class="small">
                                        Nome
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="search" class="form-control" placeholder="Selecione uma opção acima" disabled="disabled" />
                            </div>
                            <div class="buttons">
                                <input type="reset" class="btn btn-primary pull-left" value="Cancelar" />
                                <input type="submit" class="btn btn-success pull-right" value="Continuar pesquisa..." />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            */
            ?>
        </div>
    </div>


@endsection