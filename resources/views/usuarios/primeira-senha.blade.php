@extends('layout.inicio')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.primeirasenha.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')
    <div class="wrapper row">
        <div class="col-md-4 hidden-xs hidden-sm"></div>
        <div class="box-start col-md-4 col-sm-6">
            <div class="default-login">
                @if (session('status')=='5000')
                    <div class="single alert alert-success">
                        <i class="icon glyphicon glyphicon-ok pull-left"></i>
                        <div class="menssagem">
                            A nova senha foi salva com sucesso.<br /><br />
                            <a href="{{URL::to('/')}}" class="btn btn-black">Acessar o sistema</a>
                        </div>
                    </div>
                @else
                    <h2>Redefinir senha</h2>
                    <p>Para continuar, é preciso definir uma nova senha.</p>
                    <form name="form-primeira-senha" method="post" action="primeira-senha">
    					{{csrf_field()}}
                        <div class="erros alert alert-danger" @if (count($errors)==0) style="display:none" @endif>
                        	<i class="icon glyphicon glyphicon-remove pull-left"></i>
                            <div class="menssagem">
	                            @foreach ($errors->all() as $error)
                                    <?php
                                    switch ($error) {
                                        case '5001':
                                            echo 'A senha atual está incorreta.';
                                            break;
                                        case '5002':
                                            echo 'A nova senha digitada já foi utilizada, digite outra senha.';
                                            break;
                                        default:
                                            echo $error;
                                            break;
                                    }
                                    ?>
                                    <br />
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
    						<label>E-mail do usuário</label>
                            <input type="text" name="usuario" class="form-control" value="{{Auth::User()->email_usuario}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>Senha atual</label>
                            <input type="password" name="senha_usuario_atual" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Nova senha</label>
                            <input type="password" name="senha_usuario" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Confirme a nova senha</label>
                            <input type="password" name="senha_usuario2" class="form-control" />
                        </div>
                        <input type="submit" class="access btn btn-primary btn-block" value="Salvar" />
                    </form>
                @endif
            </div>
        </div>
        <div class="col-md-4 hidden-xs hidden-sm"></div>
    </div>
    @if (!session('status'))
        <div class="sign-in col-md-12 col-sm-12 text-center">
            <a href="{{url('/sair')}}" class="btn btn-black btn-lg">Sair</a>
        </div>
    @endif
@endsection