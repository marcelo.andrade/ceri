<div class="modal fade pessoa{{$usuario->id_pessoa}}" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Editar Usuário</h4>
            </div>
            <div class="modal-body">
                <div class="carregando flutuante text-center"><img src="http://ceri.dev/images/loading01.gif"></div>
                <div class="form">
                    <form name="form-editar-usuario" method="post" action="editarUsuario" class="clearfix">

                        <input type="hidden" name="id_pessoa" value="{{$usuario->id_pessoa}}">
                        <input type="hidden" name="id_telefone" value="{{$usuario->id_telefone}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">


                        <div class="erros alert alert-danger" style="display:none">
                            <i class="icon glyphicon glyphicon-remove pull-left"></i>
                            <div class="menssagem"></div>
                        </div>

                        <div class="fieldset-group clearfix">
                            <div class="col-md-6">
                                <fieldset>
                                    <legend>Nivel do Usuário</legend>
                                    <div class="col-md-12">
                                        <select name="id_tipo_usuario" class="form-control pull-left">
                                            <option value="0">Selecione o nivel do usuário</option>
                                            @foreach($tipo_usuario as $tipo)
                                              <option value={{$tipo->id_tipo_usuario}} @if($tipo->id_tipo_usuario == $usuario->id_tipo_usuario) selected @endif>{{$tipo->no_tipo_usuario}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend>Tipo de Cadastro</legend>
                                    <div class="form-group">
                                        <select name="tp_pessoa" class="form-control">
                                            <option value="0">Selecione o tipo de cadastro</option>
                                            <option value="F" @if($usuario->tp_pessoa == "F") selected @endif>Pessoa física</option>
                                            <option value="J" @if($usuario->tp_pessoa == "J") selected @endif>Pessoa jurídica</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>


                            <div class="col-md-6">
                                <fieldset>
                                    <legend>Dados de Acesso</legend>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" name="email_usuario" class="form-control" placeholder="E-mail" value="{{$usuario->no_email_pessoa}}" />
                                        </div>
                                        <div class="form-group row clearfix">
                                            <div class="col-md-6">
                                                <input type="password" name="senha_usuario" class="form-control" placeholder="Senha" />
                                            </div>
                                            <div class="col-md-6">
                                                <input type="password" name="senha_usuario2" class="form-control" placeholder="Confirme a senha" />
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="fieldset-group clearfix">
                            <fieldset>
                                <legend>Informações Pessoais</legend>

                                <div class="fieldset-group clearfix">
                                    <div class="col-md-6">
                                        <div class="pessoa-fisica" style="@if($usuario->tp_pessoa == "J") display:none @endif">
                                            <fieldset>
                                                <legend>Pessoa Física</legend>
                                            <div class="form-group">
                                                <input type="text" name="no_pessoa" class="form-control" placeholder="Nome completo" value="{{$usuario->no_pessoa}}" />
                                            </div>
                                            <div class="form-group row clearfix">
                                                <div class="col-md-4">
                                                    <input type="text" name="nu_cpf_cnpj" class="form-control" data-mask="999.999.999-99" placeholder="Digite seu CPF" value="{{$usuario->nu_cpf_cnpj}}"  />
                                                </div>
                                                <div class="col-md-4">
                                                    <select name="tp_sexo" class="form-control">
                                                        <option value="0">Selecione o gênero</option>
                                                        <option value="N" @if($usuario->tp_sexo == 'N') selected @endif>Não Informado</option>
                                                        <option value="F" @if($usuario->tp_sexo == 'F') selected @endif>Feminino</option>
                                                        <option value="M" @if($usuario->tp_sexo == 'M') selected @endif>Masculino</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" name="dt_nascimento" class="form-control" data-mask="99/99/9999" placeholder="Data de nascimento" value="{{date('d/m/Y',strtotime($usuario->dt_nascimento))}}" />
                                                </div>
                                            </div>
                                            <div class="form-group row clearfix">
                                                <div class="col-md-5">
                                                    <select name="id_tipo_telefone" class="form-control">
                                                        <option value='0'>Tipo de telefone</option>
                                                        <option value='1' @if($usuario->id_tipo_telefone == '1') selected @endif>Residencial</option>
                                                        <option value='2' @if($usuario->id_tipo_telefone == '2') selected @endif>Comercial</option>
                                                        <option value='3' @if($usuario->id_tipo_telefone == '3') selected @endif>Celular</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="nu_ddd" class="form-control" data-mask="99" placeholder="DDD" value="{{$usuario->nu_ddd}}" />
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" name="nu_telefone" class="form-control" data-mask="99999999?9" placeholder="Número" value="{{$usuario->nu_telefone}}" />
                                                </div>
                                            </div>
                                           </fieldset>
                                        </div>
                                        <div class="pessoa-juridica" style="@if($usuario->tp_pessoa == "F") display:none @endif">
                                            <fieldset>
                                                <legend>Pessoa Juridica</legend>
                                            <div class="form-group row clearfix">
                                                <div class="col-md-6">
                                                    <input type="text" name="nu_cpf_cnpj_j" class="form-control" data-mask="99.999.999/9999-99" placeholder="Digite o CNPJ" value="{{$usuario->nu_cpf_cnpj}}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="nu_inscricao_municipal_j" class="form-control" placeholder="Digite a Inscrição Municipal" value="{{$usuario->nu_inscricao_municipal}}" />
                                                </div>
                                            </div>
                                            <div class="form-group row clearfix">
                                                <div class="col-md-6">
                                                    <input type="text" name="no_pessoa_j" class="form-control" placeholder="Digite a Razão Social" value="{{$usuario->no_pessoa}}"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="no_fantasia_j" class="form-control" placeholder="Digite o Nome Fantasia" value="{{$usuario->no_fantasia}}" />
                                                </div>
                                            </div>
                                            <div class="form-group row clearfix">
                                                <div class="col-md-5">
                                                    <select name="id_tipo_telefone_j" class="form-control">
                                                        <option value="0">Tipo de telefone</option>
                                                        <option value='1' @if($usuario->id_tipo_telefone == '1') selected @endif>Residencial</option>
                                                        <option value='2' @if($usuario->id_tipo_telefone == '2') selected @endif>Comercial</option>
                                                        <option value='3' @if($usuario->id_tipo_telefone == '3') selected @endif>Celular</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="nu_ddd_j" class="form-control" data-mask="99" placeholder="DDD" value="{{$usuario->nu_ddd}}" />
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="text" name="nu_telefone_j" class="form-control" data-mask="99999999?9" placeholder="Número" value="{{$usuario->nu_telefone}}" />
                                                </div>
                                            </div>
                                            </fieldset>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend>Dados de endereço</legend>
                                            <div class="step-3">
                                                <div class="form-group">
                                                    <input type="text" name="nu_cep" class="form-control" data-mask="99999-999" placeholder="Digite o CEP" value="{{$usuario->nu_cep}}" />
                                                </div>
                                                <div class="form-group row clearfix">
                                                    <div class="col-md-10">
                                                        <input type="text" name="no_endereco" class="form-control" placeholder="Digite o Endereço" value="{{$usuario->no_endereco}}" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input type="text" name="nu_endereco" class="form-control" placeholder="Digite o Número" value="{{$usuario->nu_endereco}}" />
                                                    </div>
                                                </div>
                                                <div class="form-group row clearfix">
                                                    <div class="col-md-6">
                                                        <input type="text" name="no_bairro" class="form-control" placeholder="Digite o Bairro" value="{{$usuario->no_bairro}}" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" name="no_complemento" class="form-control" placeholder="Digite o Complemento" value="{{$usuario->no_complemento}}" />
                                                    </div>
                                                </div>
                                                <div class="form-group row clearfix">
                                                    <div class="col-md-6">
                                                        <select name="id_estado" class="form-control">
                                                            <option value="0">Selecione o estado</option>
                                                            @if(count($todosEstados)>0)
                                                                @foreach($todosEstados as $estado)
                                                                    <option value="{{$estado->id_estado}}" @if($estado->uf == $usuario->co_uf) selected @endif>{{$estado->no_estado}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select name="id_cidade" class="form-control" readonly="true">
                                                            <option value="0">Selecione uma cidade</option>
                                                                <option value="{{$usuario->id_cidade}}" @if($usuario->id_cidade <> "" ) selected @endif>{{$usuario->no_cidade}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="fieldset-group clearfix">
                            <input type="reset" class="btn btn-primary pull-left"   value="Cancelar">
                            <input type="submit" class="btn btn-success pull-right editarUsuario" value="Editar Usuário">
                        </div>

                    </form>                    </div>
            </div>

        </div>
    </div>
</div>