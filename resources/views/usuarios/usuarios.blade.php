@extends('layout.comum')
@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.usuario.js')}}?v=<?=time();?>"></script>
@endsection
@section('content')
	<div class="container">
        @if(in_array($class->id_tipo_pessoa,array(9)))
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" @if($request->id_tipo_pessoa_list==3 or $request->id_tipo_pessoa_list=='')class="active"@endif><a href="#" aria-controls="home" role="tab" data-toggle="tab" data-idtipopessoa="3">Comum</a></li>
                    <li role="presentation" @if($request->id_tipo_pessoa_list==2)class="active"@endif><a href="#" aria-controls="settings" role="tab" data-toggle="tab" data-idtipopessoa="2">Serventia</a></li>
                    <li role="presentation" @if($request->id_tipo_pessoa_list==4)class="active"@endif><a href="#" aria-controls="profile" role="tab" data-toggle="tab" data-idtipopessoa="4">Judiciário</a></li>
                    <li role="presentation" @if($request->id_tipo_pessoa_list==6)class="active"@endif><a href="#" aria-controls="messages" role="tab" data-toggle="tab" data-idtipopessoa="6">Poder Público</a></li>
                </ul>
                <form name="form-tipo-usuario" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="id_tipo_pessoa_list" value="" />
                </form>
            </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Usuários</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    @if(in_array($class->id_tipo_pessoa,array(1,2,4,6,7,8,10)))
                        <button data-target="#novo-usuario" data-toggle="modal" class="new btn btn-success">
                            Novo usuário
                        </button>
                    @endif
                </div>
                <div class="col-md-12">
                    <form name="form-filtro" method="post" action="usuarios" class="clearfix">
                        {{csrf_field()}}
                        @include('usuarios.usuarios-filtro')
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Usuários pendentes <span class="small">/ Usuários</span></h4>
            </div>
            <div class="panel-body">
                @include('usuarios.usuarios-pendentes')
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Usuários ativos <span class="small">/ Usuários</span></h4>
            </div>
            <div class="panel-body">
                @include('usuarios.usuarios-ativos')
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Usuários inativos <span class="small">/ Usuários</span></h4>
            </div>
            <div class="panel-body">
                @include('usuarios.usuarios-inativos')
            </div>
        </div>
</div>
<div id="novo-usuario" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01 clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Novo usuário</h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"><form name="form-novo-usuario" method="post" action="" class="clearfix"></form></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                <button type="submit" class="enviar-usuario btn btn-success" name="enviar-usuario">Salvar usuário</button>
            </div>
        </div>
    </div>
</div>
<div id="detalhes-usuario" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient01 clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detalhes do usuário - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div id="alterar-usuario" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header gradient01 clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Alterar o usuário - <span></span></h4>
            </div>
            <div class="modal-body">
                <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                <div class="form"><form name="form-alterar-usuario" method="post" action="" class="clearfix"></form></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                <button type="submit" class="salvar-usuario btn btn-success" name="enviar-usuario">Salvar usuário</button>
            </div>
        </div>
    </div>
</div>
@endsection
