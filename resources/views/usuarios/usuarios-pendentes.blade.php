<table id="usuarios-pendentes" class="table table-striped table-bordered table-fixed small">
    <thead>
    <tr class="gradient01">
        @if(in_array($class->id_tipo_pessoa,array(7,9)))
            <th width="25%">Nome do usuário</th>
            <th width="30%">Vínculo(s)</th>
            <th width="15%">Comarca(s)</th>
            <th width="15%">Data de cadastro</th>
            <th width="15%">Ações</th>
        @else
            <th width="55%">Nome do usuário</th>
            <th width="15%">Comarca(s)</th>
            <th width="15%">Data de cadastro</th>
            <th width="15%">Ações</th>
        @endif
    </tr>
    </thead>
    <tbody>
        @if (count($usuarios_pendentes)>0)
            @foreach ($usuarios_pendentes as $usuario)
                <tr>
                    <td>{{$usuario->no_usuario}}</td>
                    @if(in_array($class->id_tipo_pessoa,array(7,9)))
                        <td class="text-wrap">
                            @if(count($usuario->usuario_pessoa)>0)
                                @foreach($usuario->usuario_pessoa as $pessoa)
                                    <label class="label label-primary label-wrap">{{$pessoa->pessoa->no_pessoa}}</label>
                                @endforeach
                            @endif
                        </td>
                    @endif
                    @if (in_array($class->id_tipo_pessoa,array(1,2,4,6,7,8,9,10,13)))
                        <?php $comarcas = comarca($usuario->pessoa->nu_cpf_cnpj) ?>
                        <td>
                            @if (count($comarcas)>0)
                                @foreach($comarcas as $comarca)
                                    <div class="vinculo btn-group">
                                        <button class="btn btn-sm btn-primary" type="button">{{$comarca->no_comarca}}</button>
                                    </div>
                                @endforeach
                            @else
                                @if(count($usuario->pessoa->enderecos)>0)
                                    @foreach($usuario->pessoa->enderecos as $endereco)
                                        <div class="vinculo btn-group">
                                            <button class="btn btn-sm btn-primary" type="button">{{$endereco->cidade->no_cidade}}</button>
                                        </div>
                                    @endforeach
                                @endif
                            @endif
                        </td>
                    @endif
                    <td>{{Carbon\Carbon::parse($usuario->dt_cadastro)->format('d/m/Y H:i:s')}}</td>
                    <td class="options">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-usuario" data-idusuario="{{$usuario->id_usuario}}" data-nousuario="{{$usuario->no_usuario}}">Detalhes</button>
                            @if(in_array($class->id_tipo_pessoa,array(1,2,4,6,7,8,10)) or ($class->id_tipo_pessoa==9 and in_array($request->id_tipo_pessoa_list,array(1,2,7,9,10))))
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        @if($usuario->in_confirmado=='N')
                                            <li><a href="#" class="reenviar-confirmacao" data-idusuario="{{$usuario->id_usuario}}" data-nousuario="{{$usuario->no_usuario}}">Reenviar confirmação</a></li>
                                        @endif
                                        @if($usuario->in_aprovado=='N')
                                            <li><a href="#" class="aprovar-usuario" data-idusuario="{{$usuario->id_usuario}}" data-nousuario="{{$usuario->no_usuario}}">Aprovar usuário</a></li>
                                        @endif
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="@if(in_array($class->id_tipo_pessoa,array(2,7,9))) 5 @else 4 @endif">
                    <div class="single alert alert-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                        <div class="mensagem">
                            Nenhum usuário pendente foi encontrado.
                        </div>
                    </div>
                </td>
            </tr>
        @endif
    </tbody>
</table>
<div class="col-md-12">
    Exibindo <b>{{count($usuarios_pendentes)}}</b> de <b>{{$usuarios_pendentes->total()}}</b> {{($usuarios_pendentes->total()>1?'usuários':'usuário')}}.
</div>
<div align="center">
    {{$usuarios_pendentes->fragment('usuarios-pendentes')->render()}}
</div>
