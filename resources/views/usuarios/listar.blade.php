@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.listarUsuarios.js')}}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Lista de Usuários <span class="small"> /Serventia</span></h4>
            </div>
            <div class="panel-body">
                <form name="form-filtro" method="post" action="certidao#pedidos" class="clearfix">
                    <input type="hidden" name="_token" value="">
                    <fieldset class="clearfix">
                        <legend>Filtro</legend>
                        <div class="fieldset-group clearfix">
                            <div class="col-md-3">
                                <fieldset>
                                    <legend>Tipo de Usuário</legend>
                                    <div class="col-md-12">
                                        <select name="id_produto" class="form-control pull-left">
                                            <option value="0">Todos os tipos</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>Nome do Usuário</legend>
                                    <div class="col-md-12">
                                        <div class="periodo input-group">
                                            <input type="text" class="form-control pull-left" name="usuario" value="" style="width: 335px;">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <legend>Estado</legend>
                                    <div class="col-md-12">
                                        <select name="id_situacao_pedido_grupo_produto" class="form-control pull-left">
                                            <option value="0">Selecione o Estado</option>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset>
                                    <legend>Outras Opções</legend>
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <input type="checkbox" name="in_penhora" id="in_penhora" value="S">
                                            <label for="in_penhora" class="small">
                                                Usuário Inativo
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="buttons col-md-12 text-right">
                            <input type="reset" class="btn btn-primary" value="Limpar filtros">
                            <input type="submit" class="btn btn-success" value="Filtrar Usuários">
                        </div>
                    </fieldset>
                </form>
                <button type="button" class="new btn btn-success" data-toggle="modal" data-target="#new-eletronic-search">
                    Novo Usuário
                </button>
                <div class="panel table-rounded">
     <table class="table table-striped table-bordered small">
                        <thead>
                        <tr class="gradient01">
                            <th>Tipo de Usuário</th>
                            <th>Nome</th>
                            <th>Login de acesso</th>
                            <th>Estado</th>
                            <th>Status</th>
                            <th>Ação</th>
                        </tr>
                        </thead>
                        <tbody>

                     @foreach($usuarios as $usuario)
                        <tr>
                            <td>{{$usuario->no_tipo_usuario}}</td>
                            <td>{{$usuario->no_pessoa}}</td>
                            <td>{{$usuario->login}}</td>
                            <td>{{$usuario->co_uf_nascimento}}</td>
                            <td>{{$usuario->in_registro_ativo == 'S' ? 'Ativo' : 'Inativo'}}</td>

                            <td class="text-center">
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target=".pessoa{{$usuario->id_pessoa}}" >Editar</a>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target=".pessoa{{$usuario->id_pessoa}}">Apagar</a>
                            </td>

                            @include('usuarios.modalEditarUsuario')
                     @endforeach
                        </tr>

                        </tbody>
                    </table>
                </div>            </div>
        </div>
    </div>



@endsection