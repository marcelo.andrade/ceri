@extends('layout.comum')

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.funcoes.usuario.js')}}?v=<?=time();?>"></script>
@endsection

@section('content')

    <div class="container">

        <div class="panel panel-default">
            <div class="panel-heading gradient01">
                <h4>Usuário <span class="small">/ Novo</span></h4>
            </div>
            <div class="panel-body" id="filtro-novo-usuario">
                <button data-target="#novo-usuario" data-toggle="modal" class="new btn btn-success" type="button">
                    Novo usuário
                </button>
            </div>
        </div>



        <div class="panel table-rounded">
            <table class="table table-striped table-bordered small">
                <thead>
                <tr class="gradient01">
                    <th>Nome do usuário</th>
                    <th>Data de cadastro</th>
                    <th>Ativo</th>
                    <th>E-mail</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @if (count($todosUsuarios)>0)
                    @foreach ($todosUsuarios as $usuario)
                        <tr>
                            <td>{{$usuario->no_usuario}}</td>
                            <td>{{ formatar_data_hora($usuario->dt_cadastro) }}</td>
                            <td>@if ( $usuario->in_registro_ativo == 'S' ) Sim @else Não @endif </td>
                            <td>{{$usuario->email_usuario}}</td>
                            <td class="options">
                                <div class="btn-group">
                                    <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-black dropdown-toggle" type="button">
                                        Ações <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu black">
                                        <li><a class="opcoes_excluir_usuario" data-idusuario="{{$usuario->id_usuario}}" data-target="excluir-usuario" data-toggle="modal" href="#">Excluir</a></li>
                                        <li><a class="opcoes_reenviar_confirmacao" data-idusuario="{{$usuario->id_usuario}}" data-target="reenviar-confirmacao" data-toggle="modal" href="#">Reenviar confirmação</a></li>
                                        @if($usuario->in_registro_ativo=='N')
                                            <li><a class="opcoes_reativar_usuario" data-idusuario="{{$usuario->id_usuario}}"  href="javascript:void(0);">Reativar usuário</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">
                            <div class="single alert alert-danger">
                                <i class="glyphicon glyphicon-remove"></i>
                                <div class="mensagem">
                                    Nenhum usuário foi encontrado.
                                </div>
                            </div>
                        </td>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>

    <div id="novo-usuario" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header gradient01 clearfix">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Novo usuário</h4>
                </div>
                <div class="modal-body">
                    <div class="carregando text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                    <div class="form"><form name="form-usuario" method="post" action="" class="clearfix"></form></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="enviar-usuario btn btn-success" name="enviar-usuario" id="enviar-usuario" >Salvar usuário</button>
                </div>
            </div>
        </div>
    </div>
@endsection