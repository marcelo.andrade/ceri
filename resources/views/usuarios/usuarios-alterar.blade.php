<?php
$disabled_telefone = 'disabled';
$id_tipo_telefone = 0;
$nu_ddd = '';
$nu_telefone = '';
if(count($usuario->pessoa->telefones)>0) {
    $disabled_telefone = '';
    $id_tipo_telefone = $usuario->pessoa->telefones[0]->id_tipo_telefone;
    $nu_ddd = $usuario->pessoa->telefones[0]->nu_ddd;
    $nu_telefone = $usuario->pessoa->telefones[0]->nu_telefone;
}

$disabled_endereco = 'disabled';
$nu_cep = '';
$no_endereco = '';
$nu_endereco = '';
$no_bairro = '';
$no_complemento = '';
$id_estado = 0;
$id_cidade = 0;
if(count($usuario->pessoa->enderecos)>0) {
    $disabled_endereco = '';
    $nu_cep = $usuario->pessoa->enderecos[0]->nu_cep;
    $no_endereco = $usuario->pessoa->enderecos[0]->no_endereco;
    $nu_endereco = $usuario->pessoa->enderecos[0]->nu_endereco;
    $no_bairro = $usuario->pessoa->enderecos[0]->no_bairro;
    $no_complemento = $usuario->pessoa->enderecos[0]->no_complemento;
    $id_estado = $usuario->pessoa->enderecos[0]->cidade->id_estado;
    $id_cidade = $usuario->pessoa->enderecos[0]->id_cidade;
}
?>
<input type="hidden" name="id_pessoa" value="{{$usuario->id_pessoa}}" />
<div class="erros alert alert-danger" style="display:none">
    <i class="icon glyphicon glyphicon-remove pull-left"></i>
    <div class="menssagem"></div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>

                <legend>Dados pessoais</legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="small">Tipo de pessoa</label>
                        <select class="form-control" name="tp_pessoa" disabled>
                            <option value="F" @if($usuario->pessoa->tp_pessoa=='F') selected="selected" @endif>Pessoa física</option>
                            <option value="J" @if($usuario->pessoa->tp_pessoa=='J') selected="selected" @endif>Pessoa jurídica</option>
                        </select>
                    </div>
                    @if($usuario->pessoa->tp_pessoa=='F')
                        <div class="pessoa-fisica">
                            <div class="form-group">
                                <label class="small">Nome completo</label>
                                <input type="text" class="form-control" name="no_pessoa" value="{{$usuario->pessoa->no_pessoa}}">
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-4">
                                    <label class="small">CPF</label>
                                    <input type="text" data-mask="999.999.999-99" class="form-control" name="nu_cpf_cnpj" value="{{$usuario->pessoa->nu_cpf_cnpj}}" disabled>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Gênero</label>
                                    <select class="form-control" name="tp_sexo">
                                        <option value="0">Selecione o gênero</option>
                                        <option value="F" @if($usuario->pessoa->tp_sexo=='F') selected="selected" @endif>Feminino</option>
                                        <option value="M" @if($usuario->pessoa->tp_sexo=='M') selected="selected" @endif>Masculino</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label class="small">Data nascimento</label>
                                    <input type="text" data-mask="99/99/9999" class="form-control" name="dt_nascimento" value="{{Carbon\Carbon::parse($usuario->pessoa->dt_nascimento)->format('d/m/Y')}}">
                                </div>
                            </div>
                        </div>
                    @elseif($usuario->pessoa->tp_pessoa=='J')
                        <div class="pessoa-juridica">
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                    <label class="small">CNPJ</label>
                                    <input type="text" data-mask="99.999.999/9999-99" class="form-control" name="nu_cpf_cnpj" value="{{$usuario->pessoa->nu_cpf_cnpj}}" disabled>
                                </div>
                                <div class="col-md-6">
                                    <label class="small">Inscrição municipal</label>
                                    <input type="text" class="form-control" name="no_inscricao_municipal" value="{{$usuario->pessoa->no_inscricao_municipal}}">
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-md-6">
                                    <label class="small">Razão social</label>
                                    <input type="text" class="form-control" name="no_pessoa" value="{{$usuario->pessoa->no_pessoa}}">
                                </div>
                                <div class="col-md-6">
                                    <label class="small">Nome fantasia</label>
                                    <input type="text" class="form-control" name="no_fantasia" value="{{$usuario->pessoa->no_fantasia}}">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </fieldset>
        </div>
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Telefone</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <input type="checkbox" name="in_digitar_telefone" id="in_digitar_telefone" value="S" @if($disabled_telefone=='') checked @endif>
                            <label for="in_digitar_telefone" class="small">
                                Desejo digitar o telefone
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-5">
                        <label class="small">Tipo de telefone</label>
                        <select class="form-control" name="id_tipo_telefone" {{$disabled_telefone}}>
                            <option value="0">Tipo de telefone</option>
                            <option value="1" @if($id_tipo_telefone==1) selected @endif>Residencial</option>
                            <option value="2" @if($id_tipo_telefone==2) selected @endif>Comercial</option>
                            <option value="3" @if($id_tipo_telefone==3) selected @endif>Celular</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="small">DDD</label>
                        <input type="text" placeholder="DDD" data-mask="99" class="form-control" name="nu_ddd" value="{{$nu_ddd}}" {{$disabled_telefone}}>
                    </div>
                    <div class="col-md-5">
                        <label class="small">Número do telefone</label>
                        <input type="text" data-mask="99999999?9" class="form-control" name="nu_telefone" value="{{$nu_telefone}}" {{$disabled_telefone}}>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="col-md-6">
        <div class="fieldset-group clearfix">
            <fieldset>
                <legend>Endereço</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <input type="checkbox" name="in_digitar_endereco" id="in_digitar_endereco" value="S" @if($disabled_endereco=='') checked @endif>
                            <label for="in_digitar_endereco" class="small">
                                Desejo digitar o endereço
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="small">CEP</label>
                        <input type="text" data-mask="99999-999" class="form-control" name="nu_cep" value="{{$nu_cep}}" {{$disabled_endereco}}>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-10">
                        <label class="small">Endereço</label>
                        <input type="text" class="form-control" name="no_endereco" value="{{$no_endereco}}" {{$disabled_endereco}}>
                    </div>
                    <div class="col-md-2">
                        <label class="small">Número</label>
                        <input type="text" class="form-control" name="nu_endereco" value="{{$nu_endereco}}" {{$disabled_endereco}}>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Bairro</label>
                        <input type="text" class="form-control" name="no_bairro" value="{{$no_bairro}}" {{$disabled_endereco}}>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Complemento</label>
                        <input type="text" class="form-control" name="no_complemento" value="{{$no_complemento}}" {{$disabled_endereco}}>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-md-6">
                        <label class="small">Estado</label>
                        <select class="form-control" name="id_estado" {{$disabled_endereco}}>
                            <option value="0">Selecione o estado</option>
                            @if(count($estados)>0)
                                @foreach($estados as $estado)
                                    <option value="{{$estado->id_estado}}" @if($estado->id_estado==$id_estado) selected @endif>{{$estado->no_estado}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Cidade</label>
                        <select class="form-control" name="id_cidade">
                            <option value="0">Selecione uma cidade</option>
                            @if(count($cidades)>0)
                                @foreach($cidades as $cidade)
                                    <option value="{{$cidade->id_cidade}}" @if($cidade->id_cidade==$id_cidade) selected @endif>{{$cidade->no_cidade}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            <legend>Dados de acesso</legend>
            <div class="col-md-12">
                <div class="checkbox checkbox-inline">
                    <input type="checkbox" name="in_usuario_master" id="in_usuario_master" value="S" @if($usuario->in_usuario_master == 'S') checked @endif>
                    <label for="in_usuario_master" class="small">
                        Usuário Master
                    </label>
                </div>
            </div>
            <div class="col-md-12">
                <label class="small">E-mail</label>
                <input type="text" class="form-control" name="email_usuario" value="{{$usuario->email_usuario}}" disabled>
            </div>
        </fieldset>
    </div>
</div>
<div class="fieldset-group clearfix">
    <div class="col-md-12">
        <fieldset>
            @if(!$tipos_usuarios)
                <legend>Vínculos do usuário</legend>
                <input type="hidden" name="in_vincular" value="S" />
                <div class="vinculo-usuario col-md-12">
                    <div class="alert alert-info single small">
                        <i class="icon glyphicon glyphicon-info-sign pull-left"></i>
                        <div class="menssagem">O novo usuário é vinculado ao mesmo usuário logado atualmente.</div>
                    </div>
                </div>
            @else
                <legend>Inserir vínculos do usuário</legend>
                <input type="hidden" name="in_vincular" value="N" />
                <div class="col-md-3">
                    <div class="tipo-usuario">
                        <label class="small">Tipo de usuário</label>
                        <select name="id_tipo_usuario" class="form-control">
                            <option value="0">Selecione</option>
                            @foreach ($tipos_usuarios as $tipo_usuario)
                                <option value="{{$tipo_usuario->id_tipo_usuario}}">{{$tipo_usuario->no_tipo_usuario}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div id="campos-serventia" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="form-group clearfix">
                        <div class="col-md-6">
                            <label class="small">Cidade</label>
                            <select name="id_cidade_serventia" class="form-control pull-left" disabled>
                                <option value="0">Selecione uma cidade</option>
                                @if(count($cidades_serventia)>0)
                                    @foreach ($cidades_serventia as $cidade)
                                        <option value="{{$cidade->id_cidade}}">{{$cidade->no_cidade}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="small">Serventia</label>
                            <select name="id_serventia" class="form-control pull-left" disabled>
                                <option value="0">Selecione</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="campos-judiciario" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="col-md-6">
                        <label class="small">Comarca</label>
                        <select class="form-control" name="id_comarca" disabled>
                            <option value="0">Selecione uma comarca</option>
                            @if(count($comarcas)>0)
                                @foreach($comarcas as $comarca)
                                    <option value="{{$comarca->id_comarca}}">{{$comarca->no_comarca}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Vara</label>
                        <select class="form-control" name="id_vara" disabled>
                            <option value="0">Selecione uma vara</option>
                        </select>
                    </div>
                </div>
                <div id="campos-unidade-gestora" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="col-md-6">
                        <label class="small">Unidade gestora</label>
                        <select name="id_unidade_gestora" class="form-control pull-left" disabled>
                            <option value="0">Selecione uma unidade</option>
                            @if(count($unidades_gestoras)>0)
                                @foreach ($unidades_gestoras as $unidade_gestora)
                                    <option value="{{$unidade_gestora->id_unidade_gestora}}">{{$unidade_gestora->no_unidade_gestora}}</option>
                                @endforeach
                            @endif
                            <option value="2">Controladoria de Mandados</option>
                        </select>
                    </div>
                    <div id="chk_notificacao_controladoria" class="col-md-6" style="display:none">
                        <label class="small">&nbsp;</label>
                        <div class="checkbox">
                            <input type="checkbox" name="in_master_recebe_notificacoes_sistema" id="in_master_recebe_notificacoes_sistema" value="S" disabled="disabled" @if($usuario->usuario_pessoa[0]->in_master_recebe_notificacoes_sistema == 'S') checked @endif>
                            <label for="in_master_recebe_notificacoes_sistema" class="small">
                                Encaminhar notificações (email) para usuário master
                            </label>
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" name="in_usuario_recebe_notificacoes_sistema" id="in_usuario_recebe_notificacoes_sistema" value="S" disabled="disabled" @if($usuario->usuario_pessoa[0]->in_usuario_recebe_notificacoes_sistema == 'S') checked @endif>
                            <label for="in_usuario_recebe_notificacoes_sistema" class="small">
                                Receber notificaçãos (email)
                            </label>
                        </div>
                    </div>
                </div>
                <div id="campos-unidade-judiciaria" class="campos-tipo-usuario col-md-6" style="display:none">
                    <div class="col-md-6">
                        <label class="small">Unidade judiciária</label>
                        <select name="id_unidade_judiciaria" class="form-control pull-left" disabled>
                            <option value="0">Selecione uma unidade</option>
                            @if(count($unidades_judiciarias)>0)
                                @foreach ($unidades_judiciarias as $unidade_judiciaria)
                                    <option value="{{$unidade_judiciaria->id_unidade_judiciaria}}">{{$unidade_judiciaria->no_unidade_judiciaria}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="small">Subunidade</label>
                        <select name="id_unidade_judiciaria_divisao" class="form-control pull-left" disabled>
                            <option value="0">Selecione</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>&nbsp;</label>
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button class="incluir-vinculo disabled btn btn-default btn-inline" type="button" disabled>Incluir</button>
                        </div>
                        <div class="btn-group">
                            <button class="incluir-todos-vinculos disabled btn btn-primary btn-inline" type="button" disabled>Incluir todos</button>
                        </div>
                    </div>
                </div>
            @endif
        </fieldset>
    </div>
</div>
@if($tipos_usuarios)
    <div class="fieldset-group clearfix">
        <div class="col-md-12">
            <fieldset>
                <legend>Vínculos do usuário</legend>
                <div id="vinculos-usuario" class="col-md-12">
                    @if(count($usuario->usuario_pessoa)>0)
                        @foreach($usuario->usuario_pessoa as $usuario_pessoa)
                            <?php
                            switch ($usuario_pessoa->pessoa->id_tipo_pessoa) {
                                case 2:
                                    $id_vinculo = $usuario_pessoa->pessoa->serventia->id_serventia;
                                    $id_tipo_usuario = 2;
                                    break;
                                case 10:
                                    $id_vinculo = $usuario_pessoa->pessoa->serventia->id_serventia;
                                    $id_tipo_usuario = 10;
                                    break;
                                case 4:
                                    $id_vinculo = $usuario_pessoa->pessoa->vara->id_vara;
                                    $id_tipo_usuario = 4;
                                    break;
                                case 7:
                                    $id_vinculo = $usuario_pessoa->pessoa->unidade_gestora->id_unidade_gestora;
                                    $id_tipo_usuario = 7;
                                    break;
                                case 11:
                                    $id_vinculo = $usuario_pessoa->pessoa->unidade_judiciaria_divisao->id_unidade_judiciaria_divisao;
                                    $id_tipo_usuario = 11;
                                    break;
                            }
                            ?>
                            <div id="{{$id_vinculo}}" class="vinculo btn-group" data-idtipousuario="{{$id_tipo_usuario}}">
                                <button class="btn btn-sm btn-primary" type="button">{{$usuario_pessoa->pessoa->no_pessoa}}</button>
                                <input type="hidden" name="vinculo_id_tipo_usuario[]" value="{{$id_tipo_usuario}}">
                                <input type="hidden" name="id_vinculo[]" value="{{$id_vinculo}}">
                                <button class="remover-vinculo btn btn-sm btn-danger" type="button" data-idvinculo="{{$id_vinculo}}" data-idtipousuario="{{$id_tipo_usuario}}"><i class="fa fa-times"></i></button>
                            </div> 
                        @endforeach
                    @endif
                </div>
            </fieldset>
        </div>
    </div>
    <div id="cargo-usuario" class="fieldset-group clearfix" style="display:none">
        <div class="col-md-12">
            <fieldset>
                <legend>Cargo do usuário</legend>
                <div class="col-md-6">
                    <label class="small">Cargo</label>
                    <select name="id_cargo" class="form-control">
                        <option value="0">Selecione um cargo</option>
                        @if(count($cargos)>0)
                            @foreach($cargos as $cargo)
                                <option value="{{$cargo->id_cargo}}" @if($usuario->id_cargo==$cargo->id_cargo) selected @endif>{{$cargo->no_cargo}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </fieldset>
        </div>
    </div>
@endif