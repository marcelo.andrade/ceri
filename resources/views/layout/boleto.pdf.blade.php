<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Strict//EN">
<html>
    <head>
        <title>SAGRES INFORMÁTICA E COMUNICAÇÃO LTDA</title>
        <meta http-equiv=Content-Type content=text/html charset=UTF-8>
        <meta name="Generator" content="Projeto BoletoPHP - www.boletophp.com.br - Licença GPL" />
        <link rel="stylesheet" type="text/css" href="css/boleto.css" />
    </head>
    <body>
        @yield('content')
    </body>
</html>
