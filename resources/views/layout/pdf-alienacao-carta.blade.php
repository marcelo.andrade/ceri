<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>CERI - {{$titulo}}</title>
        
        <style>
			html{
				margin:30px;;
			}
            body {
                font-family:sans-serif;
                font-size:14px;
				line-height:16px;
            }
			h1,
			h2,
			h3,
			h4,
			h5 {
				margin:0;
				padding:0;
			}
			
            div#header {
                margin:0;
            }
			div#footer {
				bottom:0px;
				position:fixed;
				text-align:center;
				width:100%;
			}
			div.marca {
				height:100%;
				position:absolute;
				width:100%;
			}

            table.table {
                border:1px solid #000000;
                border-collapse:collapse;
				table-layout:fixed;
                width:100%;
            }
            table.table tr>td {
                text-align:left;
                padding:10px 15px;
            }
			
			div.box {
				border-top:1px solid #000000;
				text-align:left;
                padding:10px 25px;
			}
			
			table.table2 {
				font-size:12px;
                width: 100%;
            }
            table.table2 tr>td,
            table.table2 tr>th {
				border-bottom:1px solid #000;
                text-align:left;
                padding:10px 0;
            }
			table.table2 tr>td {
				border-bottom-style:dashed;
			}
			
			table.table3 {
                border:1px solid #000000;
                border-collapse:collapse;
                width:100%;
            }
			table.table3+table.table3 {
				margin-top:5px;
			}
            table.table3 tr>th,
            table.table3 tr>td {
                text-align:center;
                padding:10px 10px;
            }
			table.table3 tr>th {
				border-bottom:1px dashed #000;
			}
			
            .page-break {
                page-break-after: always;
            }
			p.indent {
				margin:0;
				text-indent:234px;
			}
			p.indent.line-break {
				margin-bottom:15px;
			}
        </style>
    </head>
    <body>
        <div id="header">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="../public/images/brasao-brasil01.png" alt="brasil" height="120px" />
                    </td>
                    <td style="padding-left:10px">
                        <h3>{{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->no_serventia}}</h3>
                        {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->no_endereco}}, {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->nu_endereco}}<br />
                        {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->no_cidade}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->cidade->estado->uf}} - CEP: {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->enderecos[0]->nu_cep}}<br />
                        @if(count($alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->telefones)>0)
                            Fone: ({{trim($alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->telefones[0]->nu_ddd)}}) {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->telefones[0]->nu_telefone}} -
                        @endif
                        E-mail: {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->no_email_pessoa}}<br />
                        Horário de Atendimento: {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_inicio_expediente}} às {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_inicio_almoco}} - {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_termino_almoco}} às {{$alienacao->alienacao_pedido->pedido->pedido_pessoa->pessoa->serventia->hora_termino_expediente}}
                    </td>
                </tr>
            </table>
        </div>
        @yield('content')
    </body>
</html>
