<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>CERI - {{$titulo}}</title>
        
        <style>
            body {
                font-family:sans-serif;
                font-size:14px;
            }
            div#header {
                margin:30px 0;
                text-align:center;
            }
			div#footer {
				border:0px;
				position:fixed;
				width:100%;
			}

            table.table {
                border:1px solid #EFEFEF;
                border-collapse:collapse;
                width: 100%;
            }
            table.table tr:nth-child(odd) {
                background:#EFEFEF;
            }
            table.table tr>th {
                text-align:left;
                padding:4px 6px;
            }
            table.table tr>td {
                text-align:left;
                padding:4px 6px;
            }
            table.table tr>td.resultado {
                padding:10px 15px;
            }
            .page-break {
                page-break-after: always;
            }
        </style>
    </head>
    <body>
        @yield('content')
    </body>
</html>
