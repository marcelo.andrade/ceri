<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>CERI - {{$titulo}}</title>

    <style>
        body {
            font-family:sans-serif;
            font-size:14px;
            padding-left: 15px;
            padding-right: 15px;
        }
        div#imagem{
            margin-bottom: 5px;
            margin-left: 5px;
        }
        div#header {
            margin:30px 0;
            text-align:center;
        }
        div#footer {
            bottom:0px;
            position:fixed;
            text-align:center;
            width:100%;
        }
        div.marca {
            height:100%;
            position:absolute;
            width:100%;
        }

        table.table {
            border:1px solid #EFEFEF;
            border-collapse:collapse;
            width: 100%;
        }
        table.table tr:nth-child(odd) {
            background:#EFEFEF;
        }
        table.table tr>th {
            text-align:left;
            padding:4px 6px;
        }
        table.table tr>td {
            text-align:left;
            padding:4px 6px;
        }
        table.table tr>td.resultado {
            padding:10px 15px;
        }


        table.table-impressao {
            border: 1px solid #EFEFEF;
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }
        table.table-impressao tr:nth-child(1) {
            background:#EFEFEF;
        }
        table.table-impressao tr>th {
            text-align:left;
            padding:4px 6px;
        }
        table.table-impressao tr>td {
            text-align:left;
            padding:4px 6px;
        }

        .page-break {
            page-break-after: always;
        }
        table.table tr>td {
            text-align:left;
            padding:10px 15px;
        }
        h4 {
            text-align: center;
            margin:0;
            padding:0;
        }
        hr{
            display: block;
            border-width: 0.5px;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div id="imagem">
            <figure><img src="{{asset('images/caixa-logo-nome.png')}}"  width="120" /></figure>
        </div>
        <div align="center">
            <p><b>Representação de Filial - Gestão da Adimplência</b><br>
                Av. Mato Grosso – 5.500 – Jardim Copacabana<br>
                79031-000 – Campo Grande - MS
            </p>
        </div>
    </div>
</div>

@yield('content')
</body>
</html>
