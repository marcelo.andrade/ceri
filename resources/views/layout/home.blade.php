<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <title>CERI - Central Eletrônico de Registro de Imóveis</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <link href="{{asset('css/bootstrap.min.css')}}?v=3.3.5" rel="stylesheet" type="text/css">
        <link href="{{asset('css/font-awesome.min.css')}}?v=4.3.0" rel="stylesheet" type="text/css">
        <link href="{{asset('css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}?v=1.11.0"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}?v=3.3.5"></script>
        <script type="text/javascript" src="{{asset('js/jquery.easing.min.js')}}?v=1.3"></script>
        <script type="text/javascript" src="{{asset('js/jasny-bootstrap.min.js')}}?v=3.1.3"></script>
        <script type="text/javascript" src="{{asset('js/jquery.matchHeight.min.js')}}?v=0.7"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.pt-BR.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>

        <link href="{{asset('css/home/style-home.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">
        <link href="{{asset('css/home/style-home-elementos.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{asset('js/jquery.funcoes.js')}}?v=<?=time();?>"></script>
        <script type="text/javascript" src="{{asset('js/jquery.home.js')}}?v=<?=time();?>"></script>
        <script type="text/javascript" src="{{asset('js/jquery.funcoes.fale-conosco.js')}}?v=<?=time();?>"></script>

        @yield('scripts')
        <script type="application/javascript">
            var baseUrl    = <?php echo "'".URL::to("/")."/';"; ?>
            var currentUrl = <?php echo "'".Route::current()->getPath()."';"; ?>
            <?php if (sizeof($errors)): ?>
                $(document).ready(function(){
                    $("#div_modal_error").modal("show");
                });
            <?php endif; ?>
        </script>

        @if(env('GTAG_ANALYTICS'))
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id={{env('GTAG_ANALYTICS')}}"></script>
            <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', '{{env("GTAG_ANALYTICS")}}');
            </script>
        @endif
	</head>
    <body>
        <header class="clearfix">

            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false">
                            <span class="sr-only">Abrir menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="col-md-6">
                            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/home/logo01.png')}}" alt="CERI" height="70px" /></a>
                            <ul class="parceiros list-unstyled list-inline pull-left">
                                <li><a href="http:/www.tjms.jus.br" target="_blank"><img src="{{asset('images/home/logo-tjms01.png')}}" alt="TJ-MS" height="55px" /></a></li>
                                <li><a href="http:/www.anoregms.org.br" target="_blank"><img src="{{asset('images/home/logo-anoregms01.png')}}" alt="Anoreg-MS" height="55px" /></a></li>
                            </ul>
                            <div style="float: left; width: 300px">
                                <span style="font-size:30px; color:red; font-weight:bold;"><i class="fa fa-phone-square"></i>&nbsp;0800-9411-245</span>
                                <p style="font-size:16px; color:red; font-weight:bold;">Nosso novo número</p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="usuario col-md-5 pull-right">
                            <div class="col-md-7">
                                <h5>ACESSE UTILIZANDO SEU CADASTRO</h5>
                                <form name="form-login" method="post" action="{{url('/acessar')}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="g-recaptcha-response" value="" />

                                    <div class="form-group input-group">
                                        <span class="input-group-addon" id="addon-email"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" name="usuario" class="form-control" placeholder="E-mail" value="{{old('usuario')}}" />
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon" id="addon-senha"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" name="senha_usuario" class="form-control" placeholder="Senha" />
                                    </div>
                                    <a href="{{url('/acessar/lembrar-senha')}}" class="lembrar-senha">Esqueceu sua senha?</a>

                                    <div class="botoes clearfix">
                                        <input type="button" class="cadastrar btn btn-primary" value="NÃO TENHO CADASTRO">
                                        <input type="button" class="acessar btn btn-primary" value="ACESSAR" id="valida_login">
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-5 text-center">
                                <img src="{{asset('images/home/e-cpf01.png')}}" height="100" />
                                <a href="{{url('/acessar/certificado')}}" class="acessar-certificado btn btn-primary">ACESSAR COM CERTIFICADO</a>
                            </div>
                        </div>
                    </div>
                    <div class="navbar-menu collapse navbar-collapse">
                        <ul class="menu nav nav-pills nav-justified">
                            <li @if(strstr(Route::current()->getPath(),'sobre')) class="active" @endif><a href="{{url('/sobre')}}">INSTITUCIONAL</a></li>
                            <li @if(strstr(Route::current()->getPath(),'cartorios')) class="active" @endif><a href="{{url('/cartorios')}}">CARTÓRIOS</a></li>
                            {{--<li @if(strstr(Route::current()->getPath(),'validar')) class="active" @endif><a href="{{url('/validar')}}">VALIDAR DOCUMENTO</a></li>--}}
                            <li @if(strstr(Route::current()->getPath(),'validar')) class="active" @endif>
                                <a href="{{url('/validar')}}" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">VALIDAR DOCUMENTO <span class="caret"></span></a>
                                <ul class="dropdown-menu black">
                                    <li><a href="{{url('/validar')}}">Certidão</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('validar/validar-carta-intimacao')}}">Carta de intimação</a></li>
                                </ul>
                            </li>
                            <li @if(strstr(Route::current()->getPath(),'servicos')) class="active" @endif><a href="{{(Route::current()->getPath()=='/'?'javascript:void(0);':url('/').'/#index-servicos')}}" id="menu-servicos">SERVIÇOS</a></li>
                            <li @if(strstr(Route::current()->getPath(),'tutorial')) class="active" @endif><a href="{{url('/tutorial')}}">TUTORIAIS</a></li>
                            <li @if(strstr(Route::current()->getPath(),'fale-conosco')) class="active" @endif><a href="javascript:void(0)" id="menu-fale-conosco">FALE CONOSCO</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        @yield('content')
        @include('modais-home')
        <footer>
            <div class="container">
                <div class="col-md-3 logo">
                    <img src="{{asset('images/home/logo02.png')}}" class="img-responsive"><br>
                    <p class="text-justify">Com base no Provimento n° 47/2015 do CNJ e Provimento nº 146 do TJMS, a Central Eletrônica de Registro de Imóveis foi criada primando a facilidade de uso para o usuário comum.</p>
                    <p class="text-justify">A CERI representa um avanço em prol da cidadania e da proteção do patrimônio imobiliário..</p>
                </div>
                <div class="col-md-6 budget">
                    <div class="budget-box" id="index-enviar-mensagem">
                        <h3 class="titulo text-center">ENVIE UMA MENSAGEM</h3>

                        <form class="clearfix" name="form-contato" method="post">
                            <input type="hidden" name="hidden_captcha_contato" />
                            <div class="erros alert alert-danger" style="display:none">
                                <i class="icon glyphicon glyphicon-remove "></i>
                                <div class="menssagem"></div>
                            </div>
                            <div class="carregando" style="display: none">
                                <div class="text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                            </div>
                            <div class="form-group">
                                <label class="small">Nome completo</label>
                                <input type="text" name="nome" class="form-control" />
                            </div>
                            <div class="form-group clearfix">
                                <div class="form-group column col-md-6">
                                    <label class="small">E-mail</label>
                                    <input type="text" name="email" class="form-control" />
                                </div>
                                <div class="form-group column col-md-6">
                                    <label class="small">Telefone ou celular</label>
                                    <input type="text" name="telefone" class="form-control" data-mask="(99) 99999999?9"/>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label>Área</label>
                                    <select class="form-control" name="area" id="area">
                                        <option value="0">Selecione</option>
                                        <option value="suporte">Suporte</option>
                                        <option value="financeiro">Financeiro</option>
                                        <option value="reclamacao">Reclamações</option>
                                        <option value="sugestao">Sugestões</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label class="small">Assunto</label>
                                <input type="text" name="titulo" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="small">Mensagem</label>
                                <textarea name="descricao" cols="40" rows="4" class="form-control"></textarea></span>
                            </div>

                            <div class="form-group" id="captcha_contato">
                                {!! NoCaptcha::display(['render' => 'onload', 'data-callback' => 'verifyCallbackContato']) !!}
                            </div>

                            <div class="form-group botoes">
                                <input type="submit" class="btn btn-primary center-block" value="ENVIAR SOLICITAÇÃO" />
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 info">
                    <p class="contact"><i class="fa fa-map-marker"></i> Trav. Tabelião Nelson Pereira Seba, 50 - Chácara Cachoeira, Campo Grande - MS, 79040-030</p>
                    <p class="contact"><i class="fa fa-phone"></i>0800-9411-245</p>
                    <p class="contact"><i class="glyphicon glyphicon-time"></i>Segunda-feira à sexta-feira de 08:00 as 17:00 (Hora oficial de Campo Grande)</p>
                   <div>
						<img src="{{asset('images/home/cartoes_web.png')}}" alt="Formas de pagametno"/>
				   </div>

                </div>
            </div>
        </footer>
        <div id="copy">
            <div class="container">
                © 2016 Sagres. Todos os direitos reservados.
            </div>
        </div>

        {!! NoCaptcha::renderJs() !!}

    </body>
</html>
