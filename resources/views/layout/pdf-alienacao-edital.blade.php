<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>CERI - Edital</title>
        
        <style>
			html{
				margin:250px 100px 50px 100px;
			}
            body {
                font-family:sans-serif;
                font-size:14px;
				line-height:16px;
            }
			h1,
			h2,
			h3,
			h4,
			h5 {
				margin:0;
				padding:0;
			}
			
            div#header {
                margin:0;
            }
			div#footer {
				bottom:0px;
				position:fixed;
				text-align:center;
				width:100%;
			}
			div.marca {
				height:100%;
				position:absolute;
				width:100%;
			}

            table.table {
                border:none;
                border-collapse:collapse;
                width: 100%;
            }
            table.table tr>td {
                text-align:left;
                padding:10px 15px;
            }
            .page-break {
                page-break-after: always;
            }
			.indent {
				margin:0;
				text-indent:150px;
			}
			.align {
				margin:0 0 0 220px;
			}
			.align2 {
				margin:0 0 0 130px;
			}
			.line-break {
				margin-bottom:15px;
			}
        </style>
    </head>
    <body>
        @yield('content')
    </body>
</html>
