<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <title>CERI - {{$titulo}}</title>

    <style>
        body {
            font-family:sans-serif;
            font-size:14px;
        }
        div#header {
            margin:30px 0;
            text-align:center;
            border-bottom: solid 1px #000;
        }
        div#footer {
            bottom:0px;
            position:fixed;
            text-align:center;
            width:100%;
        }
        div.marca {
            height:100%;
            position:absolute;
            width:100%;
        }

        table.table {
            border:1px solid #EFEFEF;
            border-collapse:collapse;
            width: 100%;
        }
        table.table tr:nth-child(odd) {
            background:#EFEFEF;
        }
        table.table tr>th {
            text-align:left;
            padding:4px 6px;
        }
        table.table tr>td {
            text-align:left;
            padding:4px 6px;
        }
        table.table tr>td.resultado {
            padding:10px 15px;
        }


        table.table-impressao {
            border: 1px solid #EFEFEF;
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }
        table.table-impressao tr:nth-child(1) {
            background:#EFEFEF;
        }
        table.table-impressao tr>th {
            text-align:left;
            padding:4px 6px;
        }
        table.table-impressao tr>td {
            text-align:left;
            padding:4px 6px;
        }

        .page-break {
            page-break-after: always;
        }
        table.table tr>td {
            text-align:left;
            padding:10px 15px;
        }
        h3 {
            margin:0;
            padding:0;
        }

    </style>
</head>
<body>
<div id="header">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="{{asset('images/caixa-logo.png')}}" width="120" />
            </td>
            <td style="padding-left:10px">
                <h3>Caixa Econômica Federal - Superintendência Regional</h3>
                Av. Mato Grosso, 5500 - Centro<br />
                Campo Grande - MS, 79021-003<br />
                Fone: (67) 4009-9400
            </td>
        </tr>
    </table>
</div>
@yield('content')
</body>
</html>
