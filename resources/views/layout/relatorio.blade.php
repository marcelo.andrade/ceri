<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>CERI - Central Eletrônico de Registro de Imóveis</title>

        <style>
            body {
                font-family:Arial, sans-serif;
                font-size:12px;
            }
            div#header {
                margin:30px 0;
                text-align:center;
            }
            table.table {
                border: 1px solid #ddd;
                border-collapse:collapse;
                width: 100%;
            }
            table.table tr:nth-child(odd) {
                background:#EFEFEF;
                border:1px solid #ddd;
            }
            table.table tr>th {
                text-align:left;
                padding:4px 6px;
                border:1px solid #ddd;
            }
            table.table tr>td {
                border:1px solid #ddd;
                text-align:left;
                padding:4px 6px;
            }
            table.table tr>td.resultado {
                padding:10px 15px;
            }
            .tamanho_300{
                width: 30% !important;
            }
            .page-break {
                page-break-after: always;
            }
        </style>
        @if($request->tp_saida!='PDF')
            <link href="{{asset('css/bootstrap.min.css?v=3.3.5')}}" rel="stylesheet" type="text/css">
        @endif
    </head>
    <body>
        <div id="header">
            <img src="{{asset('images/logo06.png')}}" alt="seri" height="60px" />
            <h3>{{$titulo_relatorio}}</h3>
           @if ($request->dt_inicio != "") <span class="small">{{$request->dt_inicio}} a {{$request->dt_fim}}</span>@endif
        </div>
        @yield('content')
        <br />
        <div>
            <div class="col-md-6">
                <small>Data e hora geração do relatório: {{\Carbon\Carbon::now()->format('d/m/Y H:i:s')}}</small>
            </div>
            <div class="col-md-6">
                <small class="pull-right">Usuário emissor: {{Auth::User()->no_usuario}}</small>
            </div>
        </div>
    </body>
</html>
