<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <title>CERI - Central Eletrônico de Registro de Imóveis</title>
        
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300italic,300,400italic,700,700italic' rel='stylesheet' type='text/css'>
        
        <link href="{{asset('css/bootstrap.min.css?v=3.3.5')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/font-awesome.min.css?v=4.3.0')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/jasny-bootstrap.min.css?v=3.1.3')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap.awesome.checkbox.css?v1.0.0')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{asset('js/jquery.min.js?v=1.11.0')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js?v=3.3.5')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.easing.min.js?v=1.3')}}"></script>
        <script type="text/javascript" src="{{asset('js/jasny-bootstrap.min.js?v=3.1.3')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.matchHeight.min.js?v=0.7')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.pt-BR.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>

        <link href="{{asset('css/style-comum.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">
        <link href="{{asset('css/style-elementos.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">        


        <script type="application/javascript">
            var baseUrl    = <?php echo "'".URL::to("/")."/';"; ?>
            var currentUrl = <?php echo "'".Route::current()->getPath()."';"; ?>
        </script>
        @yield('scripts')
	</head>
    <body>
        <section id="content">
	        @yield('content')
		</section>
    </body>
</html>