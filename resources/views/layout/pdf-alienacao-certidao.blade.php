<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>CERI - Certidão de Decurso de Prazo</title>
        
        <style>
			html{
				margin:50px 60px;;
			}
            body {
                font-family:sans-serif;
                font-size:14px;
            }
			h1,
			h2,
			h3,
			h4,
			h5 {
				margin:0;
				padding:0;
			}
			
            div#header {
                margin:0;
            }
			div#footer {
				bottom:10px;
				position:fixed;
				text-align:center;
				width:100%;
			}
			div.marca {
				height:100%;
				position:absolute;
				width:100%;
			}

            table.table {
                border:1px solid #000000;
                border-collapse:collapse;
                width: 100%;
            }
            table.table tr>td {
                text-align:left;
                padding:10px 15px;
            }
            .page-break {
                page-break-after: always;
            }
			p.indent {
				margin:0;
				text-indent:222px;
			}
			p.indent.line-break {
				margin-bottom:15px;
			}
        </style>
    </head>
    <body>
        <div id="header">
			<table border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td>
			            <img src="../public/images/brasao-brasil01.png" alt="brasil" height="120px" />
                    </td>
					<td style="padding-left:10px" align="center">
		            	<h2>
                        	República Federativa do Brasil<br />
                            Estado de {{$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->estado->no_estado}}
                        </h2>
                        <h3>

                        	{{$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->no_pessoa}}
                        </h3>
                        {{$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->no_endereco}}{{($alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->nu_endereco!=''?', nº '.$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->nu_endereco:'')}}{{($alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->no_bairro!=''?', '.$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->no_bairro:'')}}. CEP: {{$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->nu_cep}}. {{$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->no_email_pessoa}}
                    </td>
				</tr>
			</table>
        </div>
        @yield('content')
    </body>
</html>
