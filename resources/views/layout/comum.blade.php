<!doctype html>
<html>
    <?php
    $pessoa_ativa = Session::get('pessoa_ativa');
    ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <title>CERI - Central Eletrônico de Registro de Imóveis</title>

		<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300italic,300,400italic,700,700italic' rel='stylesheet' type='text/css'>

        {{--<script src="https://cdn.jsdelivr.net/es6-promise/latest/es6-promise.min.js"></script> <!-- IE support -->--}}
        <script type="text/javascript" src="{{asset('js/es6-promise.min.js')}}"></script><!-- IE support -->

        <link href="{{asset('css/bootstrap.min.css?v=3.3.5')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/font-awesome.min.css?v=4.7.0')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/jasny-bootstrap.min.css?v=3.1.3')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap.awesome.checkbox.css?v1.0.0')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="{{asset('js/jquery.min.js?v=1.12.4')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js?v=3.3.5')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.easing.min.js?v=1.3')}}"></script>
        <script type="text/javascript" src="{{asset('js/jasny-bootstrap.min.js?v=3.1.3')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.matchHeight.min.js?v=0.7')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.pt-BR.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>

        <link href="{{asset('css/style-comum.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">
        <link href="{{asset('css/style-elementos.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">
        @yield('style')
        <script type="text/javascript" src="{{asset('js/jquery.funcoes.js')}}?v=<?=time();?>"></script>
        <script type="text/javascript" src="{{asset('js/jquery.funcoes.creditos.js')}}?v=<?=time();?>"></script>

        @yield('scripts')
        <script type="application/javascript">
            var url_base = '{{URL::to("/")}}/';
            var url_atual = '{{Route::current()->getPath()}}/';

            var id_tipo_pessoa = {{$pessoa_ativa->pessoa->id_tipo_pessoa}};
        </script>

        @if(env('GTAG_ANALYTICS'))
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id={{env('GTAG_ANALYTICS')}}"></script>
            <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', '{{env("GTAG_ANALYTICS")}}');
            </script>
        @endif
	</head>
    <body>
        <?php
        $modulos = $pessoa_ativa->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
        ?>
        <header class="container clearfix">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false">
                        <span class="sr-only">Abrir menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/logo05.png')}}" alt="seri" height="70px" /></a>
                </div>
                <div class="navbar-right user">
					Olá <b>{{Auth::User()->pessoa->no_pessoa}}</b>!<br />
					{{--0 novas notificações.<br />--}}
                    @if(count(Auth::User()->usuario_pessoa)>0)
                        <div class="btn-group">
                            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Acessando como: {{$pessoa_ativa->pessoa->no_pessoa}} <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu black">
                                @if(count(Auth::User()->usuario_pessoa)>0)
                                    @foreach(Auth::User()->usuario_pessoa as $key => $usuario_pessoa)
                                        @if(count($usuario_pessoa->pessoa->enderecos)>0)
                                            @if(count($usuario_pessoa->pessoa->enderecos[0]->cidade)>0)
                                                <li><a href="#" class="troca-pessoa" data-key="{{$key}}">{{$usuario_pessoa->pessoa->no_pessoa}} ({{$usuario_pessoa->pessoa->enderecos[0]->cidade->no_cidade}})</a></li>
                                            @else
                                                <li><a href="#" class="troca-pessoa" data-key="{{$key}}">{{$usuario_pessoa->pessoa->no_pessoa}}</a></li>
                                            @endif
                                        @else
                                            <li><a href="#" class="troca-pessoa" data-key="{{$key}}">{{$usuario_pessoa->pessoa->no_pessoa}}</a></li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div><br />
                    @endif
					<div class="btn-group">
                        <div class="dropdown small">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Minha conta <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu black">
                                <li><a href="{{url('/configuracoes')}}">Configurações</a></li>
                                <li><a href="{{url('/sair')}}">Sair</a></li>
                            </ul>
                        </div>
                        @if ($pessoa_ativa->pessoa->id_tipo_pessoa==3)
                            <a class="small" href="{{url('/creditos')}}">
                                <b>Saldo:</b> <label class="saldo label label-info"><span class="real">{{Auth::User()->saldo_usuario()}}</span></label>
                            </a>
                        @endif
                        <input type="hidden" name="saldo_atual" value="{{Auth::User()->saldo_usuario()}}" />
                        <input type="hidden" name="id_compra_credito" value="" />
                    </div>
                </div>
                <div class="navbar-menu collapse navbar-collapse">
                    <ul class="menu nav navbar-nav pull-left">
                        <li @if(Route::current()->getPath()=='/') class="active" @endif><a href="{{url('/')}}">Página Inicial</a></li>
                        @if (array_intersect(array(1,2,4,7,8,9,11,6,13),$modulos))
                            @if (Auth::User()->in_usuario_master == 'S')
                                <li @if(strstr(Route::current()->getPath(),'usuario')) class="active" @endif><a href="{{url('/usuarios')}}">Usuários</a></li>
                                @if (array_intersect(array(1,7,13),$modulos))
		                            <li class="dropdown @if(strstr(Route::current()->getPath(),'cadastros')) active @endif">
		                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		                                    Cadastros <span class="caret"></span>
		                                </a>
		                                <ul class="dropdown-menu black">
		                                    <li><a href="{{url('/cadastros/cargos')}}">Cargos</a></li>
		                                    <li><a href="{{url('/cadastros/varas')}}">Varas</a></li>
		                                </ul>
		                            </li>
		                        @endif
                            @endif
                        @endif
			{{--
                        @if (array_intersect(array(2,10),$modulos))
                            <li class="dropdown @if(strstr(Route::current()->getPath(),'detran')) active @endif">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Detran <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu black">
                                    <li><a href="{{url('/detran/venda/comunicacao-venda-veiculo')}}">Comunicação de venda de veículos</a></li>
                                </ul>
                            </li>
                        @endif
			--}}
                        @if (array_intersect(array(2,8,9,12,13),$modulos))
                            <li class="dropdown @if(strstr(Route::current()->getPath(),'servicos/alienacao')) active @endif">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Notificação de alienação fiduciária <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu black">
                                	@if (array_intersect(array(2,8,9,12,13),$modulos))
                                    	<li><a href="{{url('/servicos/alienacao')}}">Notificações</a></li>
									@endif
									@if (array_intersect(array(8),$modulos))
                                        <li><a href="{{url('/servicos/alienacao/cancelar-contratos')}}">Contratos (Cancelamento e Pesquisa em Lote)</a></li>
                                    @endif
                                    @if (array_intersect(array(2),$modulos))
                                       <li><a href="{{url('/servicos/alienacao-pagamento')}}">Pagamento</a></li>
                                    @endif
                                	@if (array_intersect(array(8,9),$modulos))
                                        <li class="dropdown-submenu">
                                            <a tabindex="-1" href="#">Pagamento</a>
                                            <ul class="dropdown-menu black dropdown-sub-menu">
                                                <li><a href="{{url('/servicos/alienacao-pagamento')}}">Prestação de Serviço Anoreg/MS</a></li>
                                                <li><a href="{{url('/servicos/alienacao-pagamento-emolumento')}}">Emolumentos</a></li>
                                            </ul>
                                        </li>
									@endif
                                    @if (array_intersect(array(2,9),$modulos))
                                        <li><a href="{{url('/servicos/alienacao/devolucao-valor')}}">Devolução de valores (Diligências não efetuadas)</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        @if (array_intersect(array(2,3,4,7,9,5,11,6,13),$modulos))
                            <li class="dropdown @if(strstr(Route::current()->getPath(),'servicos') and !strstr(Route::current()->getPath(),'alienacao')) active @endif">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Serviços <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu black">
                                    @if (array_intersect(array(2,3,4,7,5,11,6,13,9),$modulos))
                                        <li><a href="{{url('/servicos/pesquisa-eletronica')}}">Pesquisa eletrônica</a></li>
                                        <li><a href="{{url('/servicos/certidao')}}">Certidão</a></li>
                                    @endif
                                    @if (array_intersect(array(2,4,7,5,11,6,13),$modulos))
                                        <li><a href="{{url('/servicos/oficio')}}">Ofício eletrônico</a></li>
									    @if (array_intersect(array(2,4,5,7,11,6),$modulos))
                                            <li><a href="{{url('/servicos/protocolo')}}">Protocolo eletrônico</a></li>
									    @endif
                                        @if (array_intersect(array(2,4,7,11,6),$modulos))
                                            <li><a href="{{url('/servicos/certidao-matricula')}}">Matrícula Digital</a></li>
                                        @endif
                                        @if (array_intersect(array(2,4,11,13),$modulos))
                                            <li><a href="{{url('/servicos/penhora')}}">Penhora eletrônica de imóveis</a></li>
                                        @endif
                                    @endif
                                    @if (array_intersect(array(2,9),$modulos))
                                        <li class="dropdown-submenu">
                                            <a tabindex="-1" href="#">Pagamento</a>
                                            <ul class="dropdown-menu black dropdown-sub-menu">
                                                @if (!array_intersect(array(2),$modulos))
                                                    <li><a href="{{url('/servicos/produto-pagamento')}}">Prestação de Serviço Anoreg/MS</a></li>
                                                @endif
                                                <li><a href="{{url('/servicos/produto-pagamento-emolumento')}}">Emolumentos</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (array_intersect(array(8,13),$modulos))
                            <li class="dropdown @if(strstr(Route::current()->getPath(),'exportar')) active @endif">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Arquivos <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu black">
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">Importar</a>
                                        <ul class="dropdown-menu black dropdown-sub-menu">
                                            <li><a href="{{url('/servicos/alienacao/nova-alienacao-importar')}}">Notificações</a></li>
                                            <li><a href="{{url('/servicos/alienacao/alienacao-importar-baixa')}}">Baixa Notificações</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">Exportar</a>
                                        <ul class="dropdown-menu black dropdown-sub-menu">
                                            <li><a href="{{url('/exportar/novo-arquivo-xml-fases-caixa')}}">Arquivo de Fase</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        @if (array_intersect(array(2,3,4,7,8,9),$modulos))
                            <li class="dropdown @if(strstr(Route::current()->getPath(),'relatorio')) active @endif">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Relatórios <span class="caret"></span></a>
                                <ul class="dropdown-menu black">
                                    @if (!in_array(2,$modulos) && !in_array(9,$modulos))
                                        <li><a href="{{url('/relatorios/movimentacao-financeira')}}">Movimentação financeira</a></li>
                                    @endif
                                    @if (array_intersect(array(9),$modulos))
                                        <li><a href="{{url('/relatorios/movimentacao-financeira-anoreg')}}">Movimentação financeira (ANOREG)</a></li>
                                        <li><a href="{{url('/relatorios/pagamento-servico-serventia')}}">Movimentação financeira (Pagto Serviço-Aprovado)</a></li>
                                        <li><a href="{{url('/relatorios/acompanhamento-notificacao')}}">Acompanhamento de Notificações (Serventia)</a></li>
                                    @endif
                                    @if (array_intersect(array(2),$modulos))
                                        <li><a href="{{url('/relatorios/movimentacao-pedido')}}">Movimentação pedido</a></li>
                                        <li><a href="{{url('/relatorios/tabela-preco')}}" target="_blank">Tabela de preço</a></li>
                                        <li><a href="{{url('/relatorios/acompanha-servico-nao-pago')}}">Movimentação serviços aprovados e não pagos (Caixa)</a></li>
                                        <li><a href="{{url('/servicos/recibo')}}">Recibo da Notificação</a></li>
                                    @endif
                                    @if (array_intersect(array(9),$modulos))
                                        <li><a href="{{url('/relatorios/controle-acesso')}}">Controle de acesso (Serventia)</a></li>
                                        <li><a href="{{url('/relatorios/dados-cadastrais')}}">Serventia - Dados Cadastrais</a></li>
                                        <li><a href="{{url('/relatorios/creditos-compra')}}">Créditos</a></li>
                                        <li><a href="{{url('/relatorios/alienacao-geral')}}">Lista de notificações geral</a></li>
                                    @endif
                                    @if (array_intersect(array(8),$modulos))                                   
                                        <li><a href="{{url('/relatorios/movimentacao-financeira-anoreg')}}">Movimentação financeira (Acompanhamento)</a></li>
                                        <li><a href="{{url('/relatorios/notificacao-aguardando-interacao')}}">Lista de notificações aguardando interação</a></li>
                                        <li><a href="{{url('/relatorios/alienacao-geral')}}">Lista de notificações geral</a></li>
                                        <li><a href="{{url('/relatorios/observacoes-enviadas-serventia')}}">Observações enviadas à Serventia</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (array_intersect(array(3,4,7),$modulos))
                            <li @if(strstr(Route::current()->getPath(),'creditos')) class="active" @endif><a href="{{url('/creditos')}}">Compras realizadas</a></li>
                        @endif

                        <li @if(strstr(Route::current()->getPath(),'tutorial-interno')) class="active" @endif><a href="{{url('/tutorial-interno')}}">Tutoriais</a></li>
                        <li @if(strstr(Route::current()->getPath(),'contato')) class="active" @endif><a href="{{url('/contato')}}">Contato</a></li>

                        {{--<li><a href="javascript:void(0);">Suporte</a></li>--}}
                    </ul>
                </div>

            </nav>
        </header>
        <section id="content">
	        @yield('content')
            <div id="nova-compra" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header gradient01">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Comprar créditos</h4>
                        </div>
                        <div class="modal-body">
                            <div class="carregando flutuante text-center"><img src="{{asset('images/loading01.gif')}}" /></div>
                            <div class="form"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar</button>
                            <button  type="button" class="btn btn-success pull-right" name="btn_cielo" id="btn_cielo">Prosseguir com a compra</button>
                        </div>
                    </div>
                </div>
            </div>

		</section>
        <div class="container">
            <footer>
                &copy; 2016 - Central Eletrônica de Registro de Imóveis / Sagres Internet.
            </footer>
        </div>

        {!! NoCaptcha::renderJs() !!}

    </body>
</html>
