﻿<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <title>CERI - Central Eletrônico de Registro de Imóveis</title>
        
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300italic,300,400italic,700,700italic' rel='stylesheet' type='text/css'>
        
        <link href="{{asset('css/bootstrap.min.css?v=3.3.5')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/font-awesome.min.css?v=4.7.0')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/jasny-bootstrap.min.css?v=3.1.3')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap.awesome.checkbox.css?v1.0.0')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{asset('js/jquery.min.js?v=1.11.0')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js?v=3.3.5')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.easing.min.js?v=1.3')}}"></script>
        <script type="text/javascript" src="{{asset('js/jasny-bootstrap.min.js?v=3.1.3')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.matchHeight.min.js?v=0.7')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.pt-BR.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mask.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>

        <link href="{{asset('css/style-inicio.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">
        <link href="{{asset('css/style-elementos.css')}}?v=<?=time();?>" rel="stylesheet" type="text/css">        
        <script type="text/javascript" src="{{asset('js/jquery.funcoes.js')}}?v=<?=time();?>"></script>      
        
        @yield('scripts')
	</head>
    <body>
    	<header>
        	<nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                	<div class="navbar-header">
                		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Menu de navegação</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
		                </button>
                		<a class="navbar-brand" href="{{URL::to('/')}}"><img src="{{asset('images/logo06.png')}}" alt="CERI" /></a>
                	</div>
                	<div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                        	<li @if(Route::current()->getPath()=='/') class="active" @endif><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
                        	<li @if(Route::current()->getPath()=='sobre') class="active" @endif><a href="{{URL::to('/sobre')}}">Sobre</a></li>
                        	<li @if(strstr(Route::current()->getPath(),'cadastrar')) class="active" @endif><a href="{{URL::to('/cadastrar')}}">Cadastre-se</a></li>
                        	<li @if(Route::current()->getPath()=='validar') class="active" @endif><a href="{{URL::to('/validar')}}">Validar certidão</a></li>
                        	<li @if(Route::current()->getPath()=='tutorial') class="active" @endif><a href="{{URL::to('/tutorial')}}">Tutoriais</a></li>
                        	<li @if(Route::current()->getPath()=='fale-conosco') class="active" @endif><a href="{{URL::to('/fale-conosco')}}">Fale conosco</a></li>
                        </ul>
                	</div>
                </div>
            </nav>
        </header>
		<div class="container">
        	@yield('content')
        </div>
        <footer>
        	<div class="container clearfix">
                <div class="anoreg pull-right clearfix">
                	<figure class="pull-right"><a href="http://www.anoregms.org.br" target="_blank"><img src="{{asset('images/logo-anoregms-01.png')}}" alt="ANOREG-MS" height="60" /></a></figure>
                    <div class="text pull-right text-right">
                        <b>Endereço:</b> Trav. Tabelião Nelson Pereira Seba, 50 - Chácara Cachoeira, Campo Grande - MS, 79040-030<br />
                        <b>Horário de atendimento:</b> Segunda-feira à sexta-feira de 08:00 as 17:00 <br />
                        <b>Telefones:</b> +55 (67) 3042-1015 <br> +55 (67) 3042-2344
                    </div>
                </div>
            </div>
        </footer>

        {!! NoCaptcha::renderJs() !!}

    </body>
</html>
