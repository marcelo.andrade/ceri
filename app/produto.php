<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class produto extends Model {
	
	protected $table = 'produto';
	protected $primaryKey = 'id_produto';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function certidao() {
		return $this->hasOne('App\certidao','id_produto');
	}
	public function produto_itens() {
		return $this->hasMany('App\produto_item','id_produto');
	}
}