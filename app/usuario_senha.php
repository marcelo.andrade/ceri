<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_senha extends Model {
	
	protected $table = 'usuario_senha';
	protected $primaryKey = 'id_usuario_senha';
	public $timestamps = false;
	protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	
	
}
