<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class protocolo_observacao extends Model {
	
	protected $table = 'protocolo_observacao';
	protected $primaryKey = 'id_protocolo_observacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function protocolo() {
		return $this->belongsTo('App\protocolo','id_protocolo');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad');
	}
}