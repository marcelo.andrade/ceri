<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class taxa extends Model {
	
	protected $table = 'taxa';
	protected $primaryKey = 'id_taxa';
    public $timestamps = false;
    protected $guarded  = array();
	
}