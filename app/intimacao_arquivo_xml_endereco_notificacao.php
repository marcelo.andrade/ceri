<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class intimacao_arquivo_xml_endereco_notificacao extends Model {

	protected   $table          = 'intimacao_arquivo_xml_endereco_notificacao';
	protected   $primaryKey     = 'id_intimacao_arquivo_xml_endereco_notificacao';
    public      $timestamps     = false;
    protected   $guarded        = array();
	
}
