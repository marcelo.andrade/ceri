<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class movimentacao_financeira extends Model {
	
	protected $table = 'movimentacao_financeira';
	protected $primaryKey = 'id_movimentacao_financeira';
    public $timestamps = false;
    protected $guarded  = array();

    public function pedido() {
        return $this->belongsTo('App\pedido', 'id_pedido');
    }
	
}