<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_modelo_veiculo extends Model {
	protected $table        = 'modelo_veiculo';
    protected $primaryKey   = 'id_modelo_veiculo';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function marca(){
        return $this->belongsTo('App\detran_marca_veiculo', 'id_marca_veiculo');
    }
}
