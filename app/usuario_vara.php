<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_vara extends Model {
	
	protected $table = 'usuario_vara';
	protected $primaryKey = 'id_usuario_vara';
	public $timestamps = false;
	protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	public function vara() {
		return $this->belongsTo('App\vara','id_vara');
	}
	
}
