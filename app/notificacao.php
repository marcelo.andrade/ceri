<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class notificacao extends Model {
	
	protected $table = 'notificacao';
	protected $primaryKey = 'id_notificacao';
    public $timestamps = false;
    protected $guarded  = array();
	
}