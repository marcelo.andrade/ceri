<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class parte extends Model {
	
	protected $table = 'parte';
	protected $primaryKey = 'id_parte';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function processos() {
		return $this->belongsToMany('App\processos','processo_parte','id_parte','id_processo');
	}
	public function tipo_parte() {
		return $this->belongsTo('App\tipo_parte','id_tipo_parte');
	}
}