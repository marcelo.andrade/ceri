<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class preco_produto_item extends Model {
	
	protected $table = 'preco_produto_item';
	protected $primaryKey = 'id_preco_produto_item';
	public $timestamps = false;
	protected $guarded  = array();
		
	
}
