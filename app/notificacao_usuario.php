<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class notificacao_usuario extends Model {
	
	protected $table = 'notificacao_usuario';
	protected $primaryKey = 'id_notificacao_usuario';
    public $timestamps = false;
    protected $guarded  = array();
	
}
