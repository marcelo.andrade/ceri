<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class pessoa_telefone extends Model {

	protected $table = 'pessoa_telefone';
	protected $primaryKey = 'id_pessoa_telefone';
    public $timestamps = false;
    protected $guarded  = array();

	public function telefone() {
	    return $this->belongsTo('App\telefone','id_telefone');
    }
}
