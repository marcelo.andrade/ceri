<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class unidade_judiciaria extends Model {
	
	protected $table = 'unidade_judiciaria';
	protected $primaryKey = 'id_unidade_judiciaria';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function unidade_judiciaria_divisao() {
		return $this->hasMany('App/unidade_judiciaria_divisao','id_unidade_judiciaria');
	}
}