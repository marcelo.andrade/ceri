<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_valor_item extends Model {
	
	protected $table = 'alienacao_valor_item';
	protected $primaryKey = 'id_alienacao_valor_item';
    public $timestamps = false;
    protected $guarded  = array();

}
