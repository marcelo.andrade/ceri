<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class situacao_pagamento_repasse extends Model {

    protected $table = 'situacao_pagamento_repasse';
    protected $primaryKey = 'id_situacao_pagamento_repasse';
    public $timestamps = false;
    protected $guarded  = array();

    public function pagamento_repasse_pessoa_lote()
    {
        return $this->hasMany('App\pagamento_repasse_pessoa_lote', 'id_pagamento_repasse_pessoa_lote');
    }
}
