<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class selo_lote_entrada extends Model {

	
	protected $table        = 'selo_lote_entrada';
	protected $primaryKey   = 'id_selo_lote_entrada';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function lote_selos() {
        return $this->hasMany('App\selo_lote_pedido','id_selo_lote_entrada');
    }

    public function getDtCadastroAttribute($value)
    {
        return formatar_data($value);
    }

}
