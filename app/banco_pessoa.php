<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class banco_pessoa extends Model {
	

	protected $table = 'banco_pessoa';
	protected $primaryKey = 'id_banco_pessoa';
    public $timestamps = false;
    protected $guarded  = array();

    public function banco() {
        return $this->belongsTo('App\banco','id_banco')->where('in_registro_ativo','=','S');
    }

    public function pessoa() {
        return $this->belongsTo('App\pessoa','id_pessoa');
    }
}
