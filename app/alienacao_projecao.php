<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_projecao extends Model {
	
	protected   $table          = 'alienacao_projecao';
	protected   $primaryKey     = 'id_alienacao_projecao';
    public      $timestamps     = false;
    protected   $guarded        = array();

    public function alienacao() {
        return $this->belongsTo('App\alienacao','id_alienacao');
    }

}
