<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class segmento_judiciario extends Model {
	
	protected $table = 'segmento_judiciario';
	protected $primaryKey = 'id_segmento_judiciario';
    public $timestamps = false;
    protected $guarded  = array();
	
}