<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class selo_lote_pedido extends Model {

	
	protected $table        = 'selo_lote_pedido';
	protected $primaryKey   = 'id_selo_lote_pedido';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function lote_entrada() {
        return $this->belongsTo('App\selo_lote_entrada','id_selo_lote_entrada');
    }
}
