<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alcada extends Model {
	
    const ID_ALCADA_PESSOA_FISICA   = 1;
    const ID_ALCADA_PESSOA_JURIDICA = 2;
    const ID_ALCADA_SERVENTIA       = 3;
    const ID_ALCADA_SISTEMA         = 4;
	
	protected $table = 'alcada';
	protected $primaryKey = 'id_alcada';
    public $timestamps = false;
    protected $guarded  = array();
	
}
