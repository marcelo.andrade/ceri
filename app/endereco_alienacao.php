<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class endereco_alienacao extends Model {
	
	protected $table = 'endereco_alienacao';
	protected $primaryKey = 'id_endereco_alienacao';
    public $timestamps = false;
    protected $guarded  = array();

    public function alienacao() {
    	return $this->belongsTo('App\alienacao','id_alienacao');
    }
    public function cidade() {
		return $this->belongsTo('App\cidade','id_cidade');
	}
	public function usuario_excluido() {
        return $this->hasOne('App\usuario','id_usuario','id_usuario_excluido');
    }
}
