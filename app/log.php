<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class log extends Model {
	
	protected $table = 'log';
	protected $primaryKey = 'id_log';
    public $timestamps = false;
    protected $guarded  = array();
	
}