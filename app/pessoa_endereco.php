<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pessoa_endereco extends Model {

	protected $table = 'pessoa_endereco';
	protected $primaryKey = 'id_pessoa_endereco';
    public $timestamps = false;
    protected $guarded  = array();

	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
	public function endereco() {
		return $this->belongsTo('App\endereco','id_endereco');
	}
}
