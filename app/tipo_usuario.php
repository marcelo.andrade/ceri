<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_usuario extends Model {
	
	protected $table = 'tipo_usuario';
	protected $primaryKey = 'id_tipo_usuario';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function usuario() {
		return $this->hasOne('App\usuario','id_usuario');
	}
	
}
