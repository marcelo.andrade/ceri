<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_exigencia extends Model {
	
	protected $table = 'penhora_exigencia';
	protected $primaryKey = 'id_penhora_exigencia';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','penhora_exigencia_arquivo_grupo','id_penhora_exigencia','id_arquivo_grupo_produto')->where('id_tipo_arquivo_grupo_produto',11);
	}
}