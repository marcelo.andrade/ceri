<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabela_preco_serventia extends Model
{
    protected $table = 'tabela_preco_serventia';
    protected $primaryKey = 'id_tabela_preco_serventia';
    public $timestamps = false;
    protected $guarded  = array();

    public function tabela_preco() {
        return $this->belongsTo('App\tabela_preco','id_tabela_preco');
    }
    public function serventia(){
        return $this->belongsTo('App\serventia','id_serventia');
    }
}