<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_pessoa_resposta extends Model {
	
	protected $table = 'pedido_pessoa_resposta';
	protected $primaryKey = 'id_pedido_pessoa_resposta';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedido_pessoa() {
		return $this->belongsTo('App\pedido_pessoa','id_pedido_pessoa');
	}
	public function tipo_resposta() {
		return $this->belongsTo('App\tipo_resposta','id_tipo_resposta');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
}