<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class movimentacao_parcela extends Model {
	
	protected $table = 'movimentacao_parcela';
	protected $primaryKey = 'id_movimentacao_parcela';
    public $timestamps = false;
    protected $guarded  = array();

    public function movimentacao_parcela_repasse() {
        return $this->hasMany('App\movimentacao_parcela_repasse','id_movimentacao_parcela');
    }

    public function pedido() {
        return $this->belongsTo('App\pedido','id_pedido');
    }
	
}
