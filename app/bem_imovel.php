<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class bem_imovel extends Model {
	
	protected $table = 'bem_imovel';
	protected $primaryKey = 'id_bem_imovel';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function penhoras() {
		return $this->hasMany('App\penhora_bem_imovel','id_bem_imovel');
	}
	public function proprietarios() {
		return $this->hasMany('App\bem_imovel_proprietario','id_bem_imovel')->with('proprietario_bem_imovel');
	}
	public function tipo_zona_imovel() {
		return $this->belongsTo('App\tipo_zona_imovel','id_tipo_zona_imovel');
	}
}
