<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tela_item_objeto extends Model {
	
	protected $table = 'tela_item_objeto';
	protected $primaryKey = 'id_tela_item_objeto';
    public $timestamps = false;
    protected $guarded  = array();
	
}