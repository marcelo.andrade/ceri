<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_serventia extends Model {
	
	protected $table = 'tipo_serventia';
	protected $primaryKey = 'id_tipo_serventia';
    public $timestamps = false;
    protected $guarded  = array();
	
}