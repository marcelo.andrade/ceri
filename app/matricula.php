<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class matricula extends Model {
	
	protected $table = 'matricula';
	protected $primaryKey = 'id_matricula';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function matriculas_digitais() {
		return $this->hasMany('App\matricula_digital','id_matricula')->orderBy('no_arquivo','asc');
	}
	public function proprietarios() {
		return $this->hasMany('App\proprietario','id_matricula');
	}
	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}
}