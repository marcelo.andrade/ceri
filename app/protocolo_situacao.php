<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class protocolo_situacao extends Model {
	
	protected $table = 'protocolo_situacao';
	protected $primaryKey = 'id_protocolo_situacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function protocolo() {
		return $this->hasOne('App\protocolo','id_protocolo_situacao');
	}
}