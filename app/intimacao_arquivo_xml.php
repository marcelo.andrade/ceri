<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class intimacao_arquivo_xml extends Model {
    
	protected   $table          = 'intimacao_arquivo_xml';
	protected   $primaryKey     = 'id_intimacao_arquivo_xml';
    public      $timestamps     = false;
    protected   $guarded        = array();
	
}
