<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class configurar_senha extends Model {
	
	protected $table = 'configurar_senha';
	protected $primaryKey = 'id_configurar_senha';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	
}
