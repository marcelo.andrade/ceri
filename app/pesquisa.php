<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class pesquisa extends Model {
	
	protected $table = 'pesquisa';
	protected $primaryKey = 'id_pesquisa';
    public $timestamps = false;
	protected $guarded  = array();

	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function chave_pesquisa_pesquisa() {
		return $this->belongsTo('App\chave_pesquisa_pesquisa','id_chave_pesquisa_pesquisa');
	}
	public function tipo_pesquisa() {
		return $this->belongsTo('App\tipo_pesquisa','id_tipo_pesquisa');
	}
	public function processo() {
		return $this->belongsTo('App\processo','id_processo');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','pesquisa_arquivo_grupo','id_pesquisa','id_arquivo_grupo_produto');
	}
    public function tipo_custa() {
        return $this->belongsTo('App\tipo_custa','id_tipo_custa');
    }
    public function observacoes() {
        return $this->hasMany('App\pesquisa_observacao','id_pesquisa')->orderBy('dt_cadastro','desc');
    }
    public function observacoes_nao_lidas() {
        $pessoa_ativa = Session::get('pessoa_ativa');
        $pesquisa_observacao = new pesquisa_observacao();
        $alerta_observacao = $pesquisa_observacao->where('id_pesquisa',$this->id_pesquisa)
            ->where('id_pessoa_dest',$pessoa_ativa->id_pessoa)
            ->where('in_leitura','N')
            ->count();
        return $alerta_observacao;
    }
}