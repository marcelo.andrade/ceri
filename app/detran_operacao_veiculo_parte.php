<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_operacao_veiculo_parte extends Model {
	protected $table        = 'operacao_veiculo_parte';
    protected $primaryKey   = 'id_operacao_veiculo_parte';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function parte_operacao_veiculo() {
        return $this->belongsTo('App\detran_parte_operacao_veiculo','id_parte_operacao_veiculo');
    }
    public function parte_arquivos(){
        return $this->hasMany('App\detran_operacao_veiculo_arquivo_grupo', 'id_operacao_veiculo_parte');
    }
}
