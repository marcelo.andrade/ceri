<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class modelo_certidao_serventia extends Model {
	
	protected $table = 'modelo_certidao_serventia';
	protected $primaryKey = 'id_modelo_certidao';
    public $timestamps = false;
    protected $guarded  = array();
	
}