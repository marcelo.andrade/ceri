<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_documento extends Model {
	
	protected $table = 'tipo_documento';
	protected $primaryKey = 'id_tipo_documento';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function classificacao_documento() {
		return $this->belongsTo('App\classificacao_documento','id_classificacao_documento');
	}	
}