<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class desconto_valor_periodo extends Model {
	
	protected $table = 'desconto_valor_periodo';
	protected $primaryKey = 'id_desconto_valor_periodo';
    public $timestamps = false;
    protected $guarded  = array();
	
}