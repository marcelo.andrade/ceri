<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Session;
use Auth;

class pedido extends Model {
		
	protected $table = 'pedido';
	protected $primaryKey = 'id_pedido';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function certidao() {
		return $this->hasOne('App\certidao','id_pedido');
	}
	public function pesquisa() {
		return $this->hasOne('App\pesquisa','id_pedido');
	}
	public function penhora() {
		return $this->hasOne('App\penhora','id_pedido');
	}
	public function protocolo() {
		return $this->hasOne('App\protocolo','id_pedido');
	}

	public function produto() {
		return $this->belongsTo('App\produto','id_produto');
	}
	public function produto_itens() {
		return $this->hasMany('App\pedido_produto_item','id_pedido');
	}
	public function serventias() {
		return $this->belongsToMany('App\serventia','pedido_serventia','id_pedido','id_serventia');
	}
	public function pedido_serventia() {
		return $this->hasOne('App\pedido_serventia','id_pedido');
	}
	public function situacao_pedido_grupo_produto() {
		return $this->belongsTo('App\situacao_pedido_grupo_produto','id_situacao_pedido_grupo_produto');
	}
	public function historico() {
		return $this->hasMany('App\historico_pedido','id_pedido')->orderBy('dt_cadastro','desc');
	}
	public function pedido_valor() {
		return $this->hasOne('App\pedido_valor','id_pedido');
	}
	public function pedido_resultado() {
		return $this->hasOne('App\pedido_resultado','id_pedido');
	}
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	
	public function pessoas() {
		return $this->belongsToMany('App\pessoa','pedido_pessoa','id_pedido','id_pessoa');
	}
	public function pedido_pessoa() {
		return $this->hasOne('App\pedido_pessoa','id_pedido')->orderBy('dt_cadastro','asc');
	}
	public function pedido_pessoa_atual() {
		return $this->hasOne('App\pedido_pessoa','id_pedido')->orderBy('dt_cadastro','desc');
	}
	public function pessoa_origem() {
		return $this->belongsTo('App\pessoa','id_pessoa_origem','id_pessoa');
	}

	public function pedido_notificacao() {
		$pessoa_ativa = Session::get('pessoa_ativa');
		return $this->hasMany('App\pedido_notificacao','id_pedido')->where('in_visualizada','N')
																   ->where('id_pessoa_destino',$pessoa_ativa->id_pessoa)
																   ->orderBy('dt_cadastro','desc');
	}

	public function registrar_valor_pedido($dados = array()) {
		$id_pedido = $this->id_pedido; 
		$id_usuario = Auth::User()->id_usuario;
		$id_usuario_logado = Auth::User()->id_usuario;
		$va_pedido = ($dados['va_pedido'] == null) ? $this->va_pedido : $dados['va_pedido'];
		$tp_movimentacao_financeira = $dados['tp_movimentacao_financeira'];
		$tp_movimentacao = $dados['tp_movimentacao'];
		$id_forma_pagamento = ($dados['id_forma_pagamento'] == null)?'null':$dados['id_forma_pagamento'];
        $v_id_pedido_desconto = ($dados['v_id_pedido_desconto'] == null)?'null':$dados['v_id_pedido_desconto'];
        $v_va_desconto = $dados['v_va_desconto'];
        $v_in_desconto = $dados['v_in_desconto'];
        $v_va_desconto_perc = ($dados['v_va_desconto_perc'] == null)?'null':$dados['v_va_desconto_perc'];
        $v_id_compra_credito = ($dados['v_id_compra_credito'] == null)?'null':$dados['v_id_compra_credito'];

		DB::select(DB::raw("select ceri.f_registrar_valor_pedido (
		                                                            $id_pedido, 
		                                                            $id_usuario, 
		                                                            $id_usuario_logado, 
		                                                            $va_pedido, 
		                                                            $tp_movimentacao_financeira, 
		                                                            '$tp_movimentacao', 
		                                                            $id_forma_pagamento, 
		                                                            $v_id_pedido_desconto, 
		                                                            $v_va_desconto, 
		                                                            '$v_in_desconto', 
		                                                            $v_va_desconto_perc,
        		                                                    $v_id_compra_credito
		                                                          );"));

	}

	public function registar_utilizacao_selo( $dados = array() )
	{
		$id_produto					= $dados['id_produto'];
		$caracter_delimitador		= ';';
		$id_usuario_logado			= $dados['id_usuario_logado'];
		$id_usuario_cliente			= $dados['id_usuario_cliente'];
		$id_usuario_serventia		= ( $dados['id_usuario_serventia'] == null ) ? 'null' : $dados['id_usuario_serventia'] ;
		$nu_ip_maquina_executou_ato	= $dados['nu_ip_maquina_executou_ato'];
		$modulo_origem				= $dados['modulo_origem'];
		$id_pedido					= $dados['id_pedido'];
		
		//Modulo de origem: "C" -> Cliente     "S" -> Serventia/Cartorio     "J" -> Juduciario

		$nu_selo = DB::select(DB::raw("select 
							ceri.f_retorna_selo
							(
								'$id_produto', 
								'$caracter_delimitador', 
								$id_usuario_logado, 
								$id_usuario_cliente, 
								$id_usuario_serventia, 
								'$nu_ip_maquina_executou_ato', 
								'$modulo_origem',
								$id_pedido
							);"
		));

		return $nu_selo;
	}

	public function inserir_notificacao($id_pessoa_origem,$id_pessoa_destino,$de_notificacao) {
		$nova_notificacao = new \App\pedido_notificacao();
		$nova_notificacao->id_pedido = $this->id_pedido;
		$nova_notificacao->id_pessoa_origem = $id_pessoa_origem;
		$nova_notificacao->id_pessoa_destino = $id_pessoa_destino;
		$nova_notificacao->de_notificacao = $de_notificacao;
		$nova_notificacao->id_usuario_cad = Auth::User()->id_usuario;
		return $nova_notificacao->save();
	}

	public function visualizar_notificacoes() {
		$pessoa_ativa = Session::get('pessoa_ativa');

		$alterar_notificacoes = new \App\pedido_notificacao();
		return $alterar_notificacoes->where('id_pedido',$this->id_pedido)
									->where('id_pessoa_destino',$pessoa_ativa->id_pessoa)
        							->where('in_visualizada','N')
          							->update(['in_visualizada'=>'S','dt_visualizada'=>\Carbon\Carbon::now(),'id_usuario_visualizada'=>Auth::User()->id_usuario]);
	}

	public function verifica_permissao() {
		$pessoa_ativa = Session::get('pessoa_ativa');

		$pedido_pessoa = new \App\pedido_pessoa;
		$pedido_pessoa = $pedido_pessoa->where('id_pedido',$this->id_pedido)
									   ->where('id_pessoa',$pessoa_ativa->id_pessoa)
									   ->count();
		
		if ($pedido_pessoa>0 or $this->id_pessoa_origem==$pessoa_ativa->id_pessoa or in_array($pessoa_ativa->pessoa->id_tipo_pessoa,array(9,13))) {
			return true;
		} else {
			return false;
		}
	}
}
