<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_certidao_chave_pesquisa extends Model {
	
	protected $table = 'tipo_certidao_chave_pesquisa';
	protected $primaryKey = 'id_tipo_certidao_chave_pesquisa';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function certidao() {
		return $this->hasOne('App\certidao','id_tipo_certidao_chave_pesquisa');
	}
}
