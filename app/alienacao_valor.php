<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_valor extends Model {
	
	protected $table = 'alienacao_valor';
	protected $primaryKey = 'id_alienacao_valor';
    public $timestamps = false;
    protected $guarded  = array();

	public function produto_item() {
		return $this->belongsTo('App\produto_item','id_produto_item');
	}

    public function alienacao() {
        return $this->belongsTo('App\alienacao','id_alienacao');
    }

}
