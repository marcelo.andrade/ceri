<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_devedor_devedor extends Model {
	
	protected $table = 'alienacao_devedor_devedor';
	protected $primaryKey = 'id_alienacao_devedor_devedor';
    public $timestamps = false;
    protected $guarded  = array();

    public function alienacao_devedor() {
    	return $this->belongsTo('App\alienacao_devedor','id_alienacao_devedor');
    }
    public function endereco() {
    	return $this->hasMany('App\endereco_alienacao_devedor','id_alienacao_devedor_devedor');
    }
}
