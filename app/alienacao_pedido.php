<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_pedido extends Model {
	
	protected $table = 'alienacao_pedido';
	protected $primaryKey = 'id_alienacao_pedido';
    public $timestamps = false;
    protected $guarded  = array();

    public function pedido() {
    	return $this->belongsTo('App\pedido','id_pedido');
    }
    public function alienacao_fases() {
    	return $this->hasMany('App\alienacao_fase_grupo_produto','id_alienacao_pedido');
    }
	public function andamento_alienacao() {
    	return $this->hasMany('App\andamento_alienacao','id_alienacao_pedido')->orderBy('id_andamento_alienacao','desc');
    }

    public function alienacao_andamento_situacao() {
        return $this->hasOne('App\alienacao_andamento_situacao','id_alienacao_pedido')->where('in_registro_ativo','S')->orderBy('dt_cadastro','desc');
    }

    public function alienacao_andamento_atual() {
        return $this->hasOne('App\alienacao_andamento_atual','id_alienacao_pedido')->where('in_registro_ativo','S')->orderBy('dt_cadastro','desc');
    }
    public function andamento_alienacao_excluido() {
        return $this->hasMany('App\andamento_alienacao_excluido','id_alienacao_pedido')->orderBy('id_andamento_alienacao_excluido','desc');
    }
}
