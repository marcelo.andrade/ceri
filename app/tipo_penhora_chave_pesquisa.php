<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_penhora_chave_pesquisa extends Model {
	
	protected $table = 'tipo_penhora_chave_pesquisa';
	protected $primaryKey = 'id_tipo_penhora_chave_pesquisa';
    public $timestamps = false;
    protected $guarded  = array();
	
}