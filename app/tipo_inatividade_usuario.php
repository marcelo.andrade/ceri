<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_inatividade_usuario extends Model {
	
	protected $table = 'tipo_inatividade_usuario';
	protected $primaryKey = 'id_tipo_inatividade_usuario';
    public $timestamps = false;
    protected $guarded  = array();
	
}