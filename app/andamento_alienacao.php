<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class andamento_alienacao extends Model {
	
	protected $table = 'andamento_alienacao';
	protected $primaryKey = 'id_andamento_alienacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function fase_grupo_produto() {
		return $this->belongsTo('App\fase_grupo_produto','id_fase_grupo_produto');
	}
	public function etapa_fase() {
		return $this->belongsTo('App\etapa_fase','id_etapa_fase');
	}
	public function acao_etapa() {
		return $this->belongsTo('App\acao_etapa','id_acao_etapa');
	}
	public function resultado_acao() {
		return $this->belongsTo('App\resultado_acao','id_resultado_acao');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','andamento_arquivo_grupo','id_andamento_alienacao','id_arquivo_grupo_produto');
	}

    public function alienacao_pedido() {
        return $this->belongsTo('App\alienacao_pedido','id_alienacao_pedido');
    }

    public function endereco_alienacao(){
	    return $this->hasMany('App\endereco_alienacao','id_andamento_alienacao');
    }

    public function remover_andamento_alienacao($opcao_tipo_execucao, $id_usuario_logado, $justificativa_exclusao) {
        $retorno = DB::select(DB::raw("SELECT  * FROM ceri.f_cancelar_interacao_notificacao ($this->id_andamento_alienacao,'$opcao_tipo_execucao',$id_usuario_logado,'$justificativa_exclusao') as resultado"));
        return end($retorno)->resultado;
    }
}
