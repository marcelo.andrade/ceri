<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_credor_alienacao extends Model {
	
	protected $table = 'alienacao_credor_alienacao';
	protected $primaryKey = 'id_alienacao_credor_alienacao';
    public $timestamps = false;
    protected $guarded  = array();

    public function alienacao() {
    	return $this->belongsTo('App\alienacao','id_alienacao');
    }
    public function credor() {
    	return $this->belongsTo('App\credor_alienacao','id_credor_alienacao');
    }
}
