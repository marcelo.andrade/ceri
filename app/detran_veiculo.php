<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_veiculo extends Model {
	protected   $table      = 'veiculo';
    protected   $primaryKey = 'id_veiculo';
    public      $timestamps = false;
    protected   $guarded    = array();

    public function modelo(){
        return $this->belongsTo('App\detran_modelo_veiculo','id_modelo_veiculo');
    }

    public function cor(){
        return $this->belongsTo('App\detran_cor_veiculo','id_cor_veiculo');
    }
	
}
