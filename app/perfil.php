<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class perfil extends Model {
	
	protected $table = 'perfil';
	protected $primaryKey = 'id_perfil';
    public $timestamps = false;
    protected $guarded  = array();
	
}