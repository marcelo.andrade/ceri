<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class serventia extends Model {
	
	protected $table = 'serventia';
	protected $primaryKey = 'id_serventia';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function bem_imovel() {
		return $this->hasMany('App\bem_imovel','id_serventia');
	}
	public function bem_penhora() {
		return $this->hasMany('App\bem_penhora','id_serventia');
	}
	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
	public function pedido_serventia() {
		return $this->hasOne('App\pedido_serventia','id_serventia');
	}
	public function serventia_certificado() {
		return $this->hasOne('App\serventia_certificado','id_serventia')->where('in_registro_ativo','S');
	}
    public function serventia_cliente() {
        return $this->hasOne('App\serventia_cliente','id_serventia');
    }
	public function tipo_serventia() {
		return $this->belongsTo('App\tipo_serventia','id_tipo_serventia');
	}

	public function usuario_serventia() {
		return $this->hasMany('App\usuario_serventia','id_serventia');
	}
	public function alienacao_preco_produto_item_variavel_serventia(){
	    return $this->hasMany('App\alienacao_preco_produto_item_variavel_serventia','id_serventia')->where('in_registro_ativo','S');
    }
}

