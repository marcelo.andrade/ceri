<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class log_processo extends Model {
	
	protected $table = 'log_processo';
	protected $primaryKey = 'id_log_processo';
    public $timestamps = false;
    protected $guarded  = array();
	
}