<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class log_detalhe extends Model {
	
	protected $table = 'log_detalhe';
	protected $primaryKey = 'id_log_detalhe';
    public $timestamps = false;
    protected $guarded  = array();
	
}