<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class banco extends Model {
	

	protected $table = 'banco';
	protected $primaryKey = 'id_banco';
    public $timestamps = false;
    protected $guarded  = array();
	
}
