<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class taxa_valor_periodo extends Model {
	
	protected $table = 'taxa_valor_periodo';
	protected $primaryKey = 'id_taxa_valor_periodo';
    public $timestamps = false;
    protected $guarded  = array();
	
}