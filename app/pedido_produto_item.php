<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_produto_item extends Model {
	
	protected $table = 'pedido_produto_item';
	protected $primaryKey = 'id_pedido_produto_item';
    public $timestamps = false;
    protected $guarded  = array();

	public function produto_item() {
		return $this->belongsTo('App\produto_item','id_produto_item');
	}
	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}	
}