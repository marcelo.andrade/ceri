<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_notificacao extends Model {
	
	protected $table = 'tipo_notificacao';
	protected $primaryKey = 'id_tipo_notificacao';
    public $timestamps = false;
    protected $guarded  = array();
	
}