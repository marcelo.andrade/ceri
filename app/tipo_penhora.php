<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_penhora extends Model {
	
	protected $table = 'tipo_penhora';
	protected $primaryKey = 'id_tipo_penhora';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function penhora() {
		return $this->hasOne('App\penhora','id_tipo_penhora');
	}
}