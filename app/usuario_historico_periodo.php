<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_historico_periodo extends Model {
	
	protected $table = 'usuario_historico_periodo';
	protected $primaryKey = 'id_usuario_historico_periodo';
    public $timestamps = false;
    protected $guarded  = array();
	
}