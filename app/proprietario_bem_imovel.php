<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class proprietario_bem_imovel extends Model {
	
	protected $table = 'proprietario_bem_imovel';
	protected $primaryKey = 'id_proprietario_bem_imovel';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function bens_imoveis() {
		return $this->hasMany('App\bem_imovel_proprietario','id_proprietario_bem_imovel');
	}
}