<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class proprietario extends Model {
	
	protected $table = 'proprietario';
	protected $primaryKey = 'id_proprietario';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function matricula() {
		return $this->belongsTo('App\matricula','id_matricula');
	}
}