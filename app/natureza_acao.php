<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class natureza_acao extends Model {
	
	protected $table = 'natureza_acao';
	protected $primaryKey = 'id_natureza_acao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function processo() {
		return $this->hasOne('App\processo','id_natureza_acao');
	}
}