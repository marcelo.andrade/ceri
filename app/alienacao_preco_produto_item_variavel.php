<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_preco_produto_item_variavel extends Model
{
    protected $table = 'alienacao_preco_produto_item_variavel';
    protected $primaryKey = 'id_alienacao_preco_produto_item_variavel';
    public $timestamps = false;
    protected $guarded  = array();

    public function produto_item(){
        return $this->belongsTo('App\produto_item','id_produto_item');
    }
}