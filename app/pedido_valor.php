<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_valor extends Model {
	
	protected $table = 'pedido_valor';
	protected $primaryKey = 'id_pedido_valor';
    public $timestamps = false;
    protected $guarded  = array();

	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad');
	}	
}