<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora extends Model {
	
	protected $table = 'penhora';
	protected $primaryKey = 'id_penhora';
    public $timestamps = false;
    protected $guarded  = array();

	//protected $with = array('pedido');

	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function processo() {
		return $this->belongsTo('App\processo','id_processo');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
	public function bens_imoveis() {
		return $this->hasMany('App\penhora_bem_imovel','id_penhora');
	}
	public function documentos_penhora() {
		return $this->hasMany('App\documento_penhora','id_penhora');
	}
	public function processo_parte_penhora() {
		return $this->hasMany('App\processo_parte_penhora','id_penhora');
	}
	public function tipo_penhora() {
		return $this->belongsTo('App\tipo_penhora','id_tipo_penhora');
	}
	public function penhora_prenotacao() {
		return $this->hasOne('App\penhora_prenotacao','id_penhora');
	}
	public function penhora_custa() {
		return $this->hasOne('App\penhora_custa','id_penhora');
	}
	public function penhora_averbacao() {
		return $this->hasOne('App\penhora_averbacao','id_penhora');
	}
	public function penhora_exigencia() {
		return $this->hasMany('App\penhora_exigencia','id_penhora')->orderBy('dt_cadastro','desc');
	}
	public function penhora_tipo_custa() {
		return $this->belongsTo('App\tipo_custa','id_tipo_custa');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','penhora_arquivo_grupo','id_penhora','id_arquivo_grupo_produto');
	}
}