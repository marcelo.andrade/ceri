<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class documento_prenotacao extends Model {
	
	protected $table = 'documento_prenotacao';
	protected $primaryKey = 'id_documento_prenotacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function documento() {
		return $this->belongsTo('App\documento','id_documento');
	}
}