<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_arquivo_grupo_produto extends Model {
	
	protected $table = 'alienacao_arquivo_grupo_produto';
	protected $primaryKey = 'id_alienacao_arquivo_grupo_prod';
    public $timestamps = false;
    protected $guarded  = array();

    public function alienacao() {
    	return $this->belongsTo('App\alienacao','id_alienacao');
    }
    public function arquivo_grupo_produto() {
    	return $this->belongsTo('App\arquivo_grupo_produto','id_arquivo_grupo_produto');
    }
    public function fase_grupo_produto() {
    	return $this->belongsTo('App\fase_grupo_produto','id_fase_grupo_produto');
    }

}
