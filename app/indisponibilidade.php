<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class indisponibilidade extends Model {
	
	protected $table = 'indisponibilidade';
	protected $primaryKey = 'id_indisponibilidade';
    public $timestamps = false;
    protected $guarded  = array();
	
}