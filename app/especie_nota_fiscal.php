<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class especie_nota_fiscal extends Model {
	
	protected $table = 'especie_nota_fiscal';
	protected $primaryKey = 'id_especie_nota_fiscal';
    public $timestamps = false;
    protected $guarded  = array();
	
}