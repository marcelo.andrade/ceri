<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tela_item extends Model {
	
	protected $table = 'tela_item';
	protected $primaryKey = 'id_tela_item';
    public $timestamps = false;
    protected $guarded  = array();
	
}