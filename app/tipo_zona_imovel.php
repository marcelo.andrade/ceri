<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_zona_imovel extends Model {
	
	protected $table = 'tipo_zona_imovel';
	protected $primaryKey = 'id_tipo_zona_imovel';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function bem_imovel() {
		return $this->hasOne('App\bem_imovel','id_tipo_zona_imovel');
	}

}
