<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_pagamento extends Model {
	
	protected $table = 'alienacao_pagamento';
	protected $primaryKey = 'id_alienacao_pagamento';
    public $timestamps = false;
    protected $guarded  = array();

    public function alienacoes() {
		return $this->belongsToMany('App\alienacao','alienacao_pagamento_alienacao','id_alienacao_pagamento','id_alienacao');
	}
	public function situacao() {
		return $this->belongsTo('App\situacao_alienacao_pagamento','id_situacao_alienacao_pagamento');
	}
	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}
	public function alienacao_pagamento(){
        return $this->hasMany('App\alienacao_pagamento_alienacao','id_alienacao_pagamento');
    }

    public function alienacao_valor() {
        return $this->belongsToMany('App\alienacao_valor','alienacao_pagamento_alienacao','id_alienacao_pagamento','id_alienacao_valor');
    }
}
