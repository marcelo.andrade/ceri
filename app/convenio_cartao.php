<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class convenio_cartao extends Model {
	
	protected $table = 'convenio_cartao';
	protected $primaryKey = 'id_convenio_cartao';
    public $timestamps = false;
    protected $guarded  = array();
	
}