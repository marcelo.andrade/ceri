<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_averbacao extends Model {
	
	protected $table = 'penhora_averbacao';
	protected $primaryKey = 'id_penhora_averbacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','penhora_averbacao_arquivo_grupo','id_penhora_averbacao','id_arquivo_grupo_produto')->where('id_tipo_arquivo_grupo_produto',13);
	}
}