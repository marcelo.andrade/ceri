<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class certidao_observacao extends Model
{
    protected $table = 'certidao_observacao';
    protected $primaryKey = 'id_certidao_observacao';
    public $timestamps = false;
    protected $guarded  = array();

    public function certidao() {
        return $this->belongsTo('App\certidao','id_certidao');
    }
    public function pessoa() {
        return $this->belongsTo('App\pessoa','id_pessoa');
    }
    public function pessoa_dest() {
        return $this->belongsTo('App\pessoa','id_pessoa_dest','id_pessoa');
    }
    public function usuario_cad() {
        return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
    }
    public function certidao_observacao_arquivo_grupo() {
        return $this->belongsToMany('App\arquivo_grupo_produto','certidao_observacao_arquivo_grupo','id_certidao_observacao','id_arquivo_grupo_produto');
    }
}
