<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class desconto_preco_produto_item extends Model {
	
	protected $table = 'desconto_preco_produto_item';
	protected $primaryKey = 'id_desconto_preco_produto_item';
    public $timestamps = false;
    protected $guarded  = array();
	
}