<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pagamento_repasse_parcela extends Model {

    protected $table = 'pagamento_repasse_parcela';
    protected $primaryKey = 'id_pagamento_repasse_parcela';
    public $timestamps = false;
    protected $guarded  = array();

    public function pagamento_repasse_pessoa_lote() {
        return $this->belongsTo('App\pagamento_repasse_pessoa_lote','id_pagamento_repasse_pessoa_lote');
    }

    public function movimentacao_parcela_repasse() {
        return $this->belongsTo('App\movimentacao_parcela_repasse','id_movimentacao_parcela_repasse');
    }
}
