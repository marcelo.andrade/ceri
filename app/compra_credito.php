<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class compra_credito extends Model {
	
	protected $table = 'compra_credito';
	protected $primaryKey = 'id_compra_credito';
    public $timestamps = false;
    protected $guarded  = array();


    public function situcao_credito() {
        return $this->belongsTo('App\situacao_credito','id_situacao_credito');
    }

    public function getVaCreditoAttribute($value)
    {
        return formatar_valor($value);
    }

    public function getDtCompraAttribute($value)
    {
        return formatar_data($value);
    }

    public function usuario_credito()
    {
        return $this->belongsTo('App\usuario', 'id_usuario');
    }

    public function movimentacao_financeira()
    {
        return $this->hasOne('App\movimentacao_financeira', 'id_compra_credito');
    }

}