<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pesquisa_observacao extends Model
{
    protected $table = 'pesquisa_observacao';
    protected $primaryKey = 'id_pesquisa_observacao';
    public $timestamps = false;
    protected $guarded  = array();

    public function pesquisa() {
        return $this->belongsTo('App\pesquisa','id_pesquisa');
    }
    public function pessoa() {
        return $this->belongsTo('App\pessoa','id_pessoa');
    }
    public function pessoa_dest() {
        return $this->belongsTo('App\pessoa','id_pessoa_dest','id_pessoa');
    }
    public function usuario_cad() {
        return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
    }
    public function pesquisa_observacao_arquivo_grupo() {
        return $this->belongsToMany('App\arquivo_grupo_produto','pesquisa_observacao_arquivo_grupo','id_pesquisa_observacao','id_arquivo_grupo_produto');
    }
}
