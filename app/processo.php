<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class processo extends Model {
	
	protected $table = 'processo';
	protected $primaryKey = 'id_processo';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function certidao() {
		return $this->hasOne('App\certidao','id_processo');
	}
	public function penhora() {
		return $this->hasOne('App\penhora','id_processo');
	}
	public function partes() {
		return $this->belongsToMany('App\parte','processo_parte','id_processo','id_parte')->with('tipo_parte');
	}
	public function processo_parte() {
		return $this->hasMany('App\processo_parte','id_processo');
	}
	public function natureza_acao() {
		return $this->belongsTo('App\natureza_acao','id_natureza_acao');
	}
}