<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_arquivo_xml_registro_status extends Model {


	protected   $table          = 'alienacao_arquivo_xml_registro_status';
	protected   $primaryKey     = 'id_alienacao_arquivo_xml_registro_status';
    public      $timestamps     = false;
    protected   $guarded        = array();

}
