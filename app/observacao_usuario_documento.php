<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class observacao_usuario_documento extends Model {
	
	protected $table = 'observacao_usuario_documento';
	protected $primaryKey = 'id_observacao_usuario_documento';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function documento() {
		return $this->belongsTo('App\documento','id_documento');
	}
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
}