<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class unidade_judiciaria_divisao extends Model {
	
	protected $table = 'unidade_judiciaria_divisao';
	protected $primaryKey = 'id_unidade_judiciaria_divisao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function unidade_judiciaria() {
		return $this->belongsTo('App\unidade_judiciaria','id_unidade_judiciaria');
	}

	public function usuario_unidade_judiciaria_divisao() {
		return $this->hasMany('App\usuario_unidade_judiciaria_divisao','id_unidade_judiciaria_divisao');
	}

	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
}