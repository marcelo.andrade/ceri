<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_movimentacao_financeira extends Model {
	
	protected $table = 'tipo_movimentacao_financeira';
	protected $primaryKey = 'id_tipo_movimentacao_financeira';
    public $timestamps = false;
    protected $guarded  = array();
	
}