<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class nacionalidade extends Model {
	
	protected $table = 'nacionalidade';
	protected $primaryKey = 'id_nacionalidade';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pessoa() {
		return $this->hasOne('App\pessoa','id_pessoa');
	}
	
}
