<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class indisponibilidade_serventia_acao extends Model {
	
	protected $table = 'indisponibilidade_serventia_acao';
	protected $primaryKey = 'id_indisponibilidade_serventia_acao';
    public $timestamps = false;
    protected $guarded  = array();
	
}