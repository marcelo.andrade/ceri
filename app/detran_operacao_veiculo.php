<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_operacao_veiculo extends Model {
	protected $table = 'operacao_veiculo';
    protected $primaryKey = 'id_operacao_veiculo';
    public $timestamps = false;
    protected $guarded  = array();

   /* public function operacao_parte() {
        return $this->belongsToMany('App\detran_parte_operacao_veiculo','operacao_veiculo_parte','id_operacao_veiculo','id_parte_operacao_veiculo');
    }*/
    public function operacao_parte() {
        return $this->hasMany('App\detran_operacao_veiculo_parte','id_operacao_veiculo');
    }
    public function operacao_arquivo() {
        return $this->hasMany('App\detran_operacao_veiculo_arquivo_grupo','id_operacao_veiculo');
    }

    public function pedido(){
        return $this->belongsTo('App\pedido','id_pedido');
    }

    public function veiculo(){
        return $this->belongsTo('App\detran_veiculo','id_veiculo');
    }
}
