<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class relatorio extends Model
{

    public $timestamps = false;
    protected $guarded = array();

    public function movimentacao_financeira($id_usuario, $dt_ini, $dt_fim, $vs_opcao = 'ANALITICO')
    {
        return DB::select(
            DB::raw(
                " select * 
                                from ceri.f_lista_movimentacao_fin_cliente (
                                $id_usuario, 
                                '$dt_ini', 
                                '$dt_fim', 
                                '',
                                '',
                                '$vs_opcao') as  (codigo integer, data timestamp, protocolo_compra text, pedido text, va_movimento NUMERIC(15,2), valor NUMERIC(15,2), status text, tipo text, no_usuario text)
                            "
            ));
    }

    public function movimentacao_pedido($id_modulo, $id_serventia, $id_usuario, $dt_ini, $dt_fim)
    {
        return DB::select(
            DB::raw(
                " select * 
                              from ceri.f_lista_movimento_pedido (
                              '$id_modulo',
                              '$id_serventia',
                              '', 
                              '', 
                              'padrao', 
                              '',
                              '$dt_ini',
                              '$dt_fim',
                              ';') as (id_pedido integer,no_usuario text, no_situacao_pedido_grupo_produto text, no_produto text, no_tipo_usuario text, protocolo_pedido text, dt_pedido timestamp, va_pedido NUMERIC(15,2), nu_selo text, no_serventia text, va_emulumento text, va_fdp text, va_fmp text, va_fpge text, va_funjecc text, va_issqn text, va_administracao text, va_total text)
                         "
            ));
    }

    public function tabela_preco()
    {
        return DB::select(
            DB::raw(
                "select * 
                                from ceri.f_lista_tabela_preco () 
                                as (no_produto_item text, emulumento text, va_fdp text, tx_fdp text, va_fmp text, tx_fmp text, va_fpge text, tx_fpge text, 
                                va_funjecc text, tx_funjecc text, va_issqn text, tx_issqn text, va_administracao text, va_bancaria text, va_total text)
                         "
            ));
    }

    public function controle_acesso($id_serventia, $op_relatorio, $dt_ini, $dt_fim, $caracter_delimitador)
    {
        return DB::select(
            DB::raw(
                "select * 
                    from ceri.f_relatorio_acesso_serventia(
                      '$id_serventia', 
                      '$op_relatorio', 
                      '$dt_ini',
                      '$dt_fim',
                      '$caracter_delimitador'
                    ) as (nu_ordem integer,  no_serventia varchar(200), cidade_serventia text, acesso text, vi_dia integer, vs_mes text, total_acesso integer)"
            )
        );
    }

    public function acomp_pagamento_caixa($id_modulo, $id_serventia, $id_usuario, $id_produto_item, $op_relatorio, $campos_ordem, $dt_ini, $dt_fim, $caracter_delimitador)
    {
        return DB::select(
            DB::raw(
                "select *
                    from ceri.f_relatorio_acomp_pagamento_caixa(
                      '$id_modulo', 
                      '$id_serventia', 
                      '$id_usuario', 
                      '$id_produto_item', 
                      '$op_relatorio',
                      '$campos_ordem',
                      '$dt_ini',
                      '$dt_fim',
                      '$caracter_delimitador'
                    ) as (nu_ordem integer, sr integer, agencia char(1), nss char(1), protocolo_pedido varchar(50), seq_servico integer, numero_contrato varchar(30), no_devedor text, va_tx_servico_em_aberto text, td integer, descricao text)"
            )
        );
    }

    public function movimentacao_financeira_serventia($id_usuario, $id_pessoa, $dt_ini, $dt_fim, $id_produto, $id_serventia, $caracter_delimitador, $vs_opcao)
    {
        return DB::select(
            DB::raw(
                "select * 
                    from ceri.f_relatorio_movimentacao_fin_serventia ( 
                      $id_usuario, 
                      $id_pessoa, 
                      '$dt_ini', 
                      '$dt_fim', 
                      '$id_produto',
                      '$id_serventia', 
                      '$caracter_delimitador',
                      '$vs_opcao') as (codigo integer, data timestamp, protocolo_compra text, pedido text, va_movimento NUMERIC(15,2), valor NUMERIC(15,2), status text, tipo text, no_usuario text)"
            ));
    }

    public function movimentacao_notificacao($id_serventia, $id_cidade)
    {
        $dados =  DB::select(
            DB::raw("select * 
                    from ceri.f_relatorio_pendencia_serventia(
                        '$id_serventia', 
                        '$id_cidade', 
                        '', 
                        '', 
                        '', 
                        '',
                        '', 
                      '', 
                     ';') as (nu_ordem integer, protocolo_pedido  varchar(50),  no_serventia varchar(200), cidade_serventia varchar(50), no_alcada varchar(50), fase_etapa_acao_futuro text, dia_corrido integer, dia_util integer)"
            ));
        return $dados;
    }

    public function notificacao_aguardando_interacao($id_alcada, $id_serventia, $id_cidade, $id_situacao, $id_fase, $id_etapa, $id_acao)
    {

        $id_serventia       = ($id_serventia    == 0 ) ? '' : $id_serventia;
        $id_cidade          = ($id_cidade       == 0 ) ? '' : $id_cidade;
        $id_fase            = ($id_fase         == 0 ) ? '' : $id_fase;
        $id_etapa           = ($id_etapa        == 0 ) ? '' : $id_etapa;
        $id_acao            = ($id_acao         == 0 ) ? '' : $id_acao;
        $id_situacao        = ($id_situacao     == 0 ) ? '' : $id_situacao;


        $dados =  DB::select(
                            DB::raw("SELECT *
                                        FROM ceri.f_relatorio_notificacao_aguardando_interacao(
                                        '$id_alcada', 
                                        '$id_serventia', 
                                        '$id_cidade', 
                                        '$id_fase', 
                                        '$id_etapa', 
                                        '$id_acao',
                                        '',
                                        '', 
                                        '$id_situacao', 
                                        'P', 
                                        ';') AS (nu_ordem integer, protocolo_pedido varchar(50),numero_contrato varchar(30), no_serventia varchar(200), cidade_serventia varchar(50),no_alcada varchar(50),fase_etapa_acao_futuro text, dia_corrido text, dia_util text)"
                            ));
        return $dados;
    }

    public function pedido($protocolo_pedido)
    {
        $pedido = \App\pedido::where('protocolo_pedido','=',$protocolo_pedido)
                               ->first();
        return $pedido;
    }

    public function andamento_atual($id_chave_pesquisa, $opcao_chave_pesquisa, $opcao_retorno_chave, $opcao_retorno_codigo)
    {
        $andamento = DB::select(
            DB::raw(
                "select * 
                    from ceri.f_retorna_fase_etapa_acao_resultado(
                        $id_chave_pesquisa,
                        '$opcao_chave_pesquisa',
                        '$opcao_retorno_chave',
                        '$opcao_retorno_codigo') as andamento")
        );
        return end($andamento)->andamento;
    }

}


