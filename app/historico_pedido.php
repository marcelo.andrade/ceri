<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class historico_pedido extends Model {
	
	protected $table = 'historico_pedido';
	protected $primaryKey = 'id_historico_pedido';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function retornaHistoricoPedido($idPedido){
		$historico_pedido = new historico_pedido();
		$historicoPedido = $historico_pedido->where('id_pedido', $idPedido)
			->join('situacao_pedido_grupo_produto','situacao_pedido_grupo_produto.id_situacao_pedido_grupo_produto','=','historico_pedido.id_situacao_pedido_grupo_produto')
			->join('usuario','usuario.id_usuario','=','historico_pedido.id_usuario_cad')
			->get([
				'situacao_pedido_grupo_produto.no_situacao_pedido_grupo_produto',
				'usuario.no_usuario',
				'historico_pedido.dt_cadastro',
				'historico_pedido.de_observacao',
			]);
		
		return $historicoPedido;
	}

	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad');
	}

    public function usuario_pessoa(){
        return $this->hasMany('App\usuario_pessoa','id_usuario', 'id_usuario_cad');
    }
}