<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class protocolo_bem_imovel extends Model {
	
	protected $table = 'protocolo_bem_imovel';
	protected $primaryKey = 'id_protocolo_bem_imovel';
    public $timestamps = false;
    protected $guarded  = array();

	public function protocolo() {
		return $this->belongsTo('App\protocolo','id_protocolo');
	}
	public function bem_imovel() {
		return $this->belongsTo('App\bem_imovel','id_bem_imovel');
	}
}