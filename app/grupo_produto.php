<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class grupo_produto extends Model {
	
	protected $table = 'grupo_produto';
	protected $primaryKey = 'id_grupo_produto';
    public $timestamps = false;
    protected $guarded  = array();
	
}