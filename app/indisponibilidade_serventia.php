<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class indisponibilidade_serventia extends Model {
	
	protected $table = 'indisponibilidade_serventia';
	protected $primaryKey = 'id_indisponibilidade_serventia';
    public $timestamps = false;
    protected $guarded  = array();
	
}