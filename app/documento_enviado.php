<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class documento_enviado extends Model {
	
	protected $table = 'documento_enviado';
	protected $primaryKey = 'id_documento_enviado';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function documento() {
		return $this->belongsTo('App\documento','id_documento');
	}
	public function usuario_destinatario() {
		return $this->belongsTo('App\usuario','id_usuario_destinatario','id_usuario');
	}
	public function usuario_cadastrado() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
}