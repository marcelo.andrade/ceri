<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class processo_parte extends Model {
	
	protected $table = 'processo_parte';
	protected $primaryKey = 'id_processo_parte';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function processo_parte_penhora() {
		return $this->hasOne('App\processo_parte_penhora','id_processo_parte');
	}
	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}
	public function parte() {
		return $this->belongsTo('App\parte','id_parte');
	}
	public function processo() {
		return $this->belongsTo('App\processo','id_processo');
	}

}