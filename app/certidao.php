<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class certidao extends Model {
	
	protected $table = 'certidao';
	protected $primaryKey = 'id_certidao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function tipo_certidao_chave_pesquisa() {
		return $this->belongsTo('App\tipo_certidao_chave_pesquisa','id_tipo_certidao_chave_pesquisa');
	}
	public function tipo_certidao() {
		return $this->belongsTo('App\tipo_certidao','id_tipo_certidao');
	}
	public function processo() {
		return $this->belongsTo('App\processo','id_processo');
	}
	public function periodo_certidao() {
		return $this->belongsTo('App\periodo_certidao','id_periodo_certidao');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','certidao_arquivo_grupo','id_certidao','id_arquivo_grupo_produto');
	}
    public function tipo_custa() {
        return $this->belongsTo('App\tipo_custa','id_tipo_custa');
    }
    public function observacoes() {
        return $this->hasMany('App\certidao_observacao','id_certidao')->orderBy('dt_cadastro','desc');
    }
    public function observacoes_nao_lidas() {
        $pessoa_ativa = Session::get('pessoa_ativa');
        $certidao_observacao = new certidao_observacao();
        $alerta_observacao = $certidao_observacao->where('id_certidao',$this->id_certidao)
            ->where('id_pessoa_dest',$pessoa_ativa->id_pessoa)
            ->where('in_leitura','N')
            ->count();
        return $alerta_observacao;
    }
}
