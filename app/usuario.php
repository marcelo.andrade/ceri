<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;

use DB;

class usuario extends Authenticatable {
	
	protected $table = 'usuario';
	protected $primaryKey = 'id_usuario';
    public $timestamps = false;
    protected $guarded  = array();
	
    public function getAuthIdentifier() {
        return $this->getKey();
    }
	public function getReminderEmail() {
        return $this->email_usuario;
    }
	public function getAuthPassword() {
		return $this->usuario_senha()->orderBy('dt_cadastro','desc')->first()->senha;
	}
	public function getRememberToken() {
		return $this->lembrar_token;
	}
	public function setRememberToken($value) {
		$this->lembrar_token = $value;
	}
	public function getRememberTokenName() {
		return 'lembrar_token';
	}

	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
	public function usuario_senha() {
		return $this->hasMany('App\usuario_senha','id_usuario');
	}
	public function tipo_usuario() {
		return $this->belongsTo('App\tipo_usuario','id_tipo_usuario');
	}
	public function configurar_senha() {
		return $this->hasOne('App\configurar_senha','id_usuario');
	}
	public function usuario_certificado() {
		return $this->hasOne('App\usuario_certificado','id_usuario')->where('in_registro_ativo','S');
	}
	public function usuario_serventia() {
		return $this->hasOne('App\usuario_serventia','id_usuario');
	}

	public function usuario_modulo() {
		return $this->hasMany('App\usuario_modulo','id_usuario');
	}

	public function usuario_vara() {
		return $this->hasOne('App\usuario_vara','id_usuario');
	}

	public function usuario_unidade_gestora() {
		return $this->hasOne('App\usuario_unidade_gestora','id_usuario');
	}
	public function usuario_unidade_judiciaria_divisao() {
		return $this->hasOne('App\usuario_unidade_judiciaria_divisao','id_usuario');
	}

	public function cargo() {
		return $this->belongsTo('App\cargo','id_cargo');
	}
	
	// Relações para cadastradores e alteradores em todas as tabelas
	// Relação de cadastro/alteração com o modelo usuario
	public function alterado() {
		return $this->belongsTo('App\usuario','id_usuario_alt','id_usuario');
	}
	public function alterador() {
		return $this->hasMany('App\usuario','id_usuario_alt','id_usuario');
	}
	public function cadastrado() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
	public function cadastrador() {
		return $this->hasMany('App\usuario','id_usuario_cad','id_usuario');
	}
	
	// Relação de cadastro/alteração com outras tabelas
	public function saldo_usuario() {
        $id_usuario = $this->id_usuario;

        if(Session::has('troca_pessoa')) {
            $pessoa_ativa = Session::get('pessoa_ativa');
            $id_usuario = $pessoa_ativa->pessoa->getUsuarioRelacionado()->id_usuario;
        }

		return converte_float( DB::select(DB::raw("select * from ceri.vw_saldo_cliente where id_usuario=".$id_usuario.";"))[0]->va_saldo );
	}

	/*
	public function vinculos() {
		$serventias = \App\usuario_serventia::select(DB::raw("2 as id_tipo_usuario"),'usuario_serventia.id_serventia as id_vinculo','serventia.no_serventia as no_vinculo')
											->join('serventia','serventia.id_serventia','=','usuario_serventia.id_serventia')
											->where('usuario_serventia.id_usuario',$this->id_usuario);
		$varas = \App\usuario_vara::select(DB::raw("4 as id_tipo_usuario"),'usuario_vara.id_vara as id_vinculo','vara.no_vara as no_vinculo')
								  ->join('vara','vara.id_vara','=','usuario_vara.id_vara')
								  ->where('usuario_vara.id_usuario',$this->id_usuario)
								  ->union($serventias);
		$unidades_gestoras = \App\usuario_unidade_gestora::select(DB::raw("7 as id_tipo_usuario"),'usuario_unidade_gestora.id_unidade_gestora as id_vinculo','unidade_gestora.no_unidade_gestora as no_vinculo')
								  						 ->join('unidade_gestora','unidade_gestora.id_unidade_gestora','=','usuario_unidade_gestora.id_unidade_gestora')
														 ->where('usuario_unidade_gestora.id_usuario',$this->id_usuario)
														 ->union($varas);
		$unidades_judiciarias = \App\usuario_unidade_judiciaria_divisao::select(DB::raw("11 as id_tipo_usuario"),'usuario_unidade_judiciaria_divisao.id_unidade_judiciaria_divisao as id_vinculo','unidade_judiciaria_divisao.no_unidade_judiciaria_divisao as no_vinculo')
								  						 			   ->join('unidade_judiciaria_divisao','unidade_judiciaria_divisao.id_unidade_judiciaria_divisao','=','usuario_unidade_judiciaria_divisao.id_unidade_judiciaria_divisao')
																	   ->where('usuario_unidade_judiciaria_divisao.id_usuario',$this->id_usuario)
																	   ->union($unidades_gestoras);

		return $unidades_judiciarias->get();
	}*/

	public function usuario_pessoa() {
		return $this->hasMany('App\usuario_pessoa','id_usuario');
	}
}
