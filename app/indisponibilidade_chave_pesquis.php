<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class indisponibilidade_chave_pesquis extends Model {
	
	protected $table = 'indisponibilidade_chave_pesquis';
	protected $primaryKey = 'id_indisponibilidade_chave_pesq';
    public $timestamps = false;
    protected $guarded  = array();
	
}