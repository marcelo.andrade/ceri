<?php
Route::group(['middleware' => ['web']], function () {

	Route::get('/', function () {
		if (Auth::check()) {
			return redirect('inicio');
		} else {
            return view('nova-home');
		}
	});
	
	Route::get('sobre', function () {
		return view('sobre');
	});
	Route::get('cartorios', function () {
		return view('cartorios');
	});
	Route::get('tutorial', function () {
		$acesso 	= "externo";
		$tp_usuario	=  false;
		return view('tutoriais', compact('acesso', 'tp_usuario'));
	});
	Route::get('tutorial-interno', function () {
		$acesso 	= "interno";
		$tp_usuario	= Auth::User()->id_tipo_usuario;
		return view('tutoriais', compact('acesso', 'tp_usuario'));
	});

	Route::post('acessar','AcessarController@acessar');
	Route::get('acessar/certificado','AcessarController@acessar_certificado');
	Route::post('acessar/certificado/autenticar','AcessarController@autenticar_certificado');
	Route::get('acessar/lembrar-senha','AcessarController@lembrar_senha');
	Route::post('acessar/lembrar-senha','UsuarioController@enviar_senha');
	Route::get('acessar/recuperar-senha/{recuperar_token}','AcessarController@recuperar_senha');
	Route::post('acessar/recuperar-senha','UsuarioController@salvar_senha');

	Route::get('fale-conosco','ContatoController@fale_conosco');
	Route::post('fale-conosco/enviar','ContatoController@enviar_fale_conosco');

	Route::get('enviar-email-usuarios-manuais','UsuarioController@enviar_email_usuarios_manuais');

	Route::get('cadastrar','CadastrarController@index');
	Route::get('cadastrar/usuario','CadastrarController@cadastrar_usuario');
	Route::post('cadastrar/usuario','UsuarioController@inserir_usuario');
	/*
	Route::get('cadastrar/cartorio','CadastrarController@cadastrar_cartorio');
	Route::post('cadastrar/cartorio','UsuarioController@inserir_cartorio');
	Route::get('cadastrar/judiciario','CadastrarController@cadastrar_judiciario');
	Route::post('cadastrar/judiciario','UsuarioController@inserir_judiciario');
	*/
	Route::get('cadastrar/confirmar/{confirmar_token}','UsuarioController@confirmar');
	Route::get('cadastrar/reenviar-confirmacao','UsuarioController@reenviar_confirmacao');
	Route::post('cadastrar/reenviar-confirmacao','UsuarioController@enviar_confirmacao');

	Route::post('totalUsuarios','UsuarioController@totalUsuarios');
	Route::post('totalPessoas','PessoaController@totalPessoas');
	Route::post('totalDocumento','OficioController@totalDocumento');

	Route::post('listarCidades','EstadoController@listarCidades');
	Route::post('listarServentias','PesquisaController@listar_serventias');
	Route::post('listarModelos','DetranVendaVeiculoController@listar_modelos');
	Route::post('listarVeiculo','DetranVendaVeiculoController@listar_veiculo');
	Route::post('listarPessoa','PessoaController@listar_pessoa');

	Route::get('validar','ValidarController@index');
    Route::post('validar','ValidarController@validar');
    Route::post('validar/visualizar-arquivo','ValidarController@visualizar_arquivo');
    Route::get('validar/visualizar-arquivo/render/{hash_arquivo}/{no_arquivo}','ValidarController@render_arquivo');
    Route::get('validar/visualizar-arquivo/download/{hash_arquivo}','ValidarController@download_arquivo');
    Route::post('validar/visualizar-arquivo/certificado','ValidarController@certificado_arquivo');
    Route::post('validar/imprimir-resultado','ValidarController@imprimir_resultado');
    Route::get('validar/imprimir-resultado/render/{hash_pedido}','ValidarController@render_resultado');
    Route::match(['get','post'],'validar/validar-carta-intimacao','ValidarController@validar_carta_intimacao');

    Route::get('creditos/atualiza-retorno-debito','ValidaTransacaoController@atualiza_retorno_debito');

	Route::group(['middleware' => 'auth'], function () {
		Route::get('sair','AcessarController@sair');
		
		Route::post('primeira-senha','UsuarioController@salvar_primeira_senha');
		Route::post('troca-pessoa','UsuarioController@troca_pessoa');

		Route::group(['middleware' => 'verifica.senha'], function () {

			Route::get('inicio','InicioController@index');
			Route::post('inicio/alertas/detalhes','InicioController@alertas_detalhes');
			
			Route::get('contato','ContatoController@index');
			Route::post('contato/enviar-contato','ContatoController@enviar_contato');

			Route::group(['prefix' => '/configuracoes/'], function () {
				Route::get('',function() { return redirect('/configuracoes/dados-pessoais'); });
				Route::get('dados-pessoais','UsuarioController@configuracoes_pessoais');
				Route::post('dados-pessoais/salvar','UsuarioController@configuracoes_salvar_pessoais');
				Route::get('dados-acesso','UsuarioController@configuracoes_acesso');
				Route::post('dados-acesso/salvar','UsuarioController@configuracoes_salvar_acesso');
				
				Route::group(['middleware' => 'verifica.modulo:2|5'], function () {
					Route::get('certificado','UsuarioController@configuracoes_certificado');
					Route::post('certificado/salvar','UsuarioController@configuracoes_salvar_certificado');
					Route::get('selos','SeloController@index');
					Route::post('selos/salvar','SeloController@configuracoes_salvar_selos');
					Route::get('selos/exportar-selo','SeloController@exportar_selo');
					Route::get('dados-bancarios','UsuarioController@configuracoes_pessoais');
					Route::post('dados-bancarios/salvar','UsuarioController@configuracoes_salvar_banco');
					Route::get('dados-serventia','UsuarioController@configuracoes_pessoais');
					Route::post('dados-serventia/salvar','UsuarioController@configuracoes_salvar_serventia');
                    Route::get('dados-produtos','UsuarioController@configuracoes_dados_produtos');
                    Route::post('dados-produtos/salvar','UsuarioController@configuracoes_salvar_pordutos');
				});
			});

			Route::group(['prefix' => '/detran/'], function () {
				Route::group(['middleware' => 'verifica.modulo:1'], function () {
					Route::get('venda/comunicacao-venda-veiculo','DetranVendaVeiculoController@index');
					Route::post('venda/nova-comunicacao','DetranVendaVeiculoController@nova_comunicacao');
					Route::post('venda/inserir-comunicacao','DetranVendaVeiculoController@inserir_comunicacao');
					Route::post('venda/detalhes-comunicacao','DetranVendaVeiculoController@detalhes');
				});
			});
	
			Route::group(['prefix' => '/usuarios/'], function () {
				Route::group(['middleware' => 'verifica.modulo:1|2|4|7|8|9|11|6|13'], function () {
					Route::get('','UsuarioController@index');
					Route::post('','UsuarioController@index');
					Route::post('novo-usuario','UsuarioController@novo_usuario_interno');
					Route::post('detalhes','UsuarioController@detalhes');
					Route::post('inserir-usuario','UsuarioController@inserir_usuario_interno');
					Route::post('aprovar-usuario','UsuarioController@aprovar_usuario');
					Route::post('reenviar-confirmacao','UsuarioController@enviar_confirmacao');
					Route::post('desativar-usuario','UsuarioController@desativar_usuario');
					Route::post('reativar-usuario','UsuarioController@reativar_usuario');

					Route::post('alterar-usuario','UsuarioController@alterar_usuario_interno');
					Route::post('salvar-usuario','UsuarioController@salvar_usuario_interno');
					Route::post('gerar-nova-senha','UsuarioController@gerar_nova_senha');
				});
			});
	
			Route::group(['prefix' => '/servicos/'], function () {
				Route::group(['middleware' => 'verifica.modulo:2|3|4|7|5|11|6|9|13'], function () {
					Route::get('certidao','CertidaoController@index');
					Route::post('certidao','CertidaoController@index');
					Route::post('certidao/detalhes','CertidaoController@detalhes');
					Route::post('certidao/resultado','CertidaoController@resultado');
					Route::post('certidao/nova-certidao','CertidaoController@nova');
					Route::post('certidao/inserir-certidao','CertidaoController@inserir');
					Route::post('certidao/enviar-pedido','CertidaoController@enviar_pedido');
					Route::post('certidao/enviar-pedidos','CertidaoController@enviar_pedidos');
					Route::post('certidao/atualizar-valores','CertidaoController@atualizar_valores');
					Route::post('certidao/confirmar-pedido','CertidaoController@confirmar_pedido');
					Route::post('certidao/cancelar-pedido','CertidaoController@cancelar_pedido');
					Route::post('certidao/imprimir-resultado','CertidaoController@imprimir_resultado');
					Route::get('certidao/imprimir-resultado/render/{id_pedido}','CertidaoController@render_resultado');

					Route::post('certidao/nova-resposta','CertidaoController@nova_resposta');
					Route::post('certidao/inserir-andamento','CertidaoController@inserir_andamento');
					Route::post('certidao/inserir-resultado','CertidaoController@inserir_resultado');
                    Route::post('certidao/gerar-recibo-certidao','CertidaoController@gerar_recibo_certidao');
                    Route::get('certidao/imprimir-recibo-certidao/render/{id_pedido}','CertidaoController@imprimir_recibo_certidao');

					Route::post('certidao/relatorio-certidao','CertidaoController@relatorio_certidao');
                    Route::post('certidao/relatorio/salvar','CertidaoController@salvar_relatorio_certidao');
                    Route::post('certidao/observacoes','CertidaoController@observacoes');
                    Route::post('certidao/inserir-observacao','CertidaoController@inserir_observacao');
                    Route::post('certidao/remover-arquivos-sessao','CertidaoController@remover_arquivos_sessao');
                    Route::post('certidao/status-observacoes','CertidaoController@status_observacao');
					Route::post('certidao/detalhes-solicitante','CertidaoController@detalhes_solicitante');                    
                    Route::post('certidao/protocolar','CertidaoController@protocolar');
                    Route::post('certidao/inserir-protocolo','CertidaoController@inserir_protocolo');
                    Route::post('certidao/alterar-protocolo','CertidaoController@alterar_protocolo');
				});
	
				Route::group(['middleware' => 'verifica.modulo:2|4|7|11|6|9'], function () {
					Route::get('certidao-matricula','CertidaoMatriculaController@index');
					Route::post('certidao-matricula','CertidaoMatriculaController@index');
					Route::post('certidao-matricula/detalhes','CertidaoMatriculaController@detalhes');
					Route::post('certidao-matricula/resultado','CertidaoMatriculaController@resultado');
					Route::post('certidao-matricula/nova','CertidaoMatriculaController@nova');
					Route::post('certidao-matricula/inserir','CertidaoMatriculaController@inserir');
					Route::post('certidao-matricula/enviar-pedido','CertidaoMatriculaController@enviar_pedido');
					Route::post('certidao-matricula/confirmar-pedido','CertidaoMatriculaController@confirmar_pedido');
					Route::post('certidao-matricula/cancelar-pedido','CertidaoMatriculaController@cancelar_pedido');
					Route::post('certidao-matricula/enviar-pedidos','CertidaoMatriculaController@enviar_pedidos');
					Route::post('certidao-matricula/nova-resposta','CertidaoMatriculaController@nova_resposta');
					Route::post('certidao-matricula/inserir-andamento','CertidaoMatriculaController@inserir_andamento');
					Route::post('certidao-matricula/inserir-resultado','CertidaoMatriculaController@inserir_resultado');
					Route::post('certidao-matricula/inserir-correcao','CertidaoMatriculaController@inserir_correcao');
					Route::post('certidao-matricula/inserir-precificacao','CertidaoMatriculaController@inserir_precificacao');
				});
	
				Route::group(['middleware' => 'verifica.modulo:4|5|7|11|6'], function () {
					Route::get('processo/novo-processo','ProcessoController@novo');
					Route::post('processo/total-processos','ProcessoController@total');
					Route::post('processo/inserir-processo','ProcessoController@inserir');
					Route::post('processo/atualizar-parte-processo','ProcessoController@atualizar_parte_processo');
					Route::post('processo/detalhes-processo','ProcessoController@detalhes');
					Route::post('processo/busca-parte','ProcessoController@busca_parte');
				});
	
				Route::group(['middleware' => 'verifica.modulo:2|4|7|11|13'], function () {
					Route::get('penhora', 'PenhoraController@index');
					Route::post('penhora', 'PenhoraController@index');
					Route::post('penhora/detalhes','PenhoraController@detalhes');
					Route::post('penhora/resultado','PenhoraController@resultado');
					Route::post('penhora/recibo','PenhoraController@recibo');
					Route::post('penhora/exigencias','PenhoraController@exigencias');
					Route::post('penhora/nova-penhora', 'PenhoraController@nova');
					Route::post('penhora/inserir-penhora', 'PenhoraController@inserir');
					Route::post('penhora/atualizar-penhora', 'PenhoraController@atualizar_penhora');
					Route::post('penhora/atualizar-parte-penhora', 'PenhoraController@atualizar_parte_penhora');
					Route::post('penhora/nova-resposta', 'PenhoraController@nova_resposta');
					Route::post('penhora/inserir-averbacao','PenhoraController@inserir_averbacao');
					Route::post('penhora/inserir-exigencia','PenhoraController@inserir_exigencia');
					Route::post('penhora/inserir-prenotacao', 'PenhoraController@inserir_prenotacao');
					Route::post('penhora/inserir-custa', 'PenhoraController@inserir_custa');
					Route::post('penhora/bem-imovel-certidao', 'PenhoraController@bemimovel_certidao');
					Route::post('penhora/certificado-detalhes','PenhoraController@certificado_detalhes');
				});
	
				Route::group(['middleware' => 'verifica.modulo:2|4|7|11|6'], function () {
					Route::get('bem-imovel/novo','BemImovelController@novo');
					Route::post('bem-imovel/inserir','BemImovelController@inserir');
					Route::post('bem-imovel/total','BemImovelController@total');
					Route::post('bem-imovel/detalhes','BemImovelController@detalhes');
					Route::post('bem-imovel/detalhesJSON','BemImovelController@detalhesJSON');
				});
				
				Route::post('matricula/total','MatriculaController@total');
				Route::post('matricula/detalhesJSON','MatriculaController@detalhesJSON');
	
				Route::post('listarServentias','ServentiaController@listarServentias');
				Route::post('listarVaras','VaraController@listarVaras');
				Route::post('listar_credores','CredorAlienacaoController@listar_credores');
				Route::post('listar_agencia_credores','CredorAlienacaoController@listar_agencia_credores');

				Route::post('produto-item/listar','ProdutoController@listar_itens');
				Route::post('produto-item/calcular','ProdutoController@calcular');

				Route::post('listar-unidade-judiciaria-divisoes','UnidadeJudiciariaController@listar_divisoes');
	
				Route::group(['middleware' => 'verifica.modulo:2|3|4|7|5|11|6|9|13'], function () {
					Route::get('pesquisa-eletronica','PesquisaController@index');
					Route::post('pesquisa-eletronica','PesquisaController@index');
					Route::post('pesquisa-eletronica/detalhes','PesquisaController@detalhes');
					Route::post('pesquisa-eletronica/resultado','PesquisaController@resultado');
					Route::post('pesquisa-eletronica/imprimir-resultado','PesquisaController@imprimir_resultado');
					Route::get('pesquisa-eletronica/imprimir-resultado/render/{id_pedido}','PesquisaController@render_resultado');
					Route::post('pesquisa-eletronica/nova-pesquisa','PesquisaController@nova');
					Route::post('pesquisa-eletronica/inserir-pesquisa','PesquisaController@inserir');
					Route::post('pesquisa-eletronica/nova-resposta','PesquisaController@nova_resposta');
					Route::post('pesquisa-eletronica/inserir-andamento','PesquisaController@inserir_andamento');
					Route::post('pesquisa-eletronica/inserir-resultado','PesquisaController@inserir_resultado');
					Route::post('pesquisa-eletronica/gerar-recibo-pesquisa','PesquisaController@gerar_recibo_pesquisa');
					Route::get('pesquisa-eletronica/imprimir-recibo-pesquisa/render/{id_pedido}','PesquisaController@imprimir_recibo_pesquisa');

					Route::post('pesquisa-eletronica/relatorio-pesquisas','PesquisaController@relatorio_pesquisas');
                    Route::post('pesquisa-eletronica/relatorio/salvar','PesquisaController@salvar_relatorio_pesquisas');
					Route::post('pesquisa-eletronica/observacoes','PesquisaController@observacoes');
					Route::post('pesquisa-eletronica/inserir-observacao','PesquisaController@inserir_observacao');
					Route::post('pesquisa-eletronica/remover-arquivos-sessao','PesquisaController@remover_arquivos_sessao');
					Route::post('pesquisa-eletronica/status-observacoes','PesquisaController@status_observacao');
                    Route::post('pesquisa-eletronica/detalhes-solicitante','PesquisaController@detalhes_solicitante');

					Route::post('pesquisa-eletronica/protocolar','PesquisaController@protocolar');
                    Route::post('pesquisa-eletronica/inserir-protocolo','PesquisaController@inserir_protocolo');
                    Route::post('pesquisa-eletronica/alterar-protocolo','PesquisaController@alterar_protocolo');
                });
	
				Route::group(['middleware' => 'verifica.modulo:2|4|7|5|11|6|13'], function () {
					Route::get('oficio',function() { return redirect('/servicos/oficio/documentos/recebidos'); });
					Route::post('oficio/novo','OficioController@novo');
					Route::post('oficio/inserir','OficioController@inserir');
					Route::post('oficio/excluir','OficioController@excluir');
					Route::post('oficio/recibo','OficioController@recibo');
					Route::post('oficio/custas','OficioController@custas');
					Route::post('oficio/inserir-custas','OficioController@inserir_custas');
					Route::post('oficio/inserir-prenotacao','OficioController@inserir_prenotacao');
					Route::post('oficio/marcar-lida-oficio','OficioController@marcar_lida_oficio');
					Route::post('oficio/detalhes','OficioController@detalhes');
					Route::post('oficio/observacoes','OficioController@observacoes');
					Route::post('oficio/inserir-observacao','OficioController@inserir_observacao');
					Route::post('oficio/encaminhar','OficioController@encaminhar');
					Route::post('oficio/responder','OficioController@responder');
					Route::post('oficio/inserir-encaminhamento','OficioController@inserir_encaminhamento');
					Route::post('oficio/inserir-resposta','OficioController@inserir_resposta');
					Route::match(['get', 'post'], 'oficio/documentos/recebidos','OficioController@recebidos');
					Route::match(['get', 'post'], 'oficio/documentos/enviados','OficioController@enviados');

					Route::post('oficio/listar-pessoas','OficioController@listar_pessoas');
					Route::post('oficio/listar-usuarios','OficioController@listar_usuarios');
					Route::post('oficio/listar-tipos','OficioController@listar_tipos');
				});
	
				Route::group(['middleware' => 'verifica.modulo:2|4|7|5|11|6'], function () {
					Route::get('protocolo','ProtocoloController@index');
					Route::post('protocolo','ProtocoloController@index');
					Route::post('protocolo/detalhes','ProtocoloController@detalhes');
					Route::post('protocolo/arquivo','ProtocoloController@arquivo');
					Route::post('protocolo/novo-protocolo','ProtocoloController@novo');
					Route::post('protocolo/inserir-protocolo','ProtocoloController@inserir');
					Route::post('protocolo/inserir-observacao','ProtocoloController@inserir_observacao');
					Route::post('protocolo/nova-resposta','ProtocoloController@nova_resposta');
					Route::post('protocolo/inserir-resposta','ProtocoloController@inserir_resposta');
				});

                Route::group(['middleware' => 'verifica.modulo:8'], function () {
                    Route::match(['get','post'],'alienacao/cancelar-contratos','AlienacaoController@cancelar_contratos');
                    Route::get('alienacao/alienacao-importar-baixa','AlienacaoController@baixa_importar');
                    Route::post('alienacao/alienacao-visualizar-importacao-baixa','AlienacaoController@visualizar_baixa_importacao');
                    Route::get('alienacao/nova-alienacao-importar','AlienacaoController@nova_importar');
                });


                Route::group(['middleware' => 'verifica.modulo:2|9'], function () {
                    Route::match(['get','post'], 'alienacao/devolucao-valor','AlienacaoController@devolver_valor_produto');
                    Route::post('alienacao/devolucao-valor/novo-comprovante','AlienacaoController@devolver_valor_novo');
                    Route::post('alienacao/devolucao-valor/inserir-comprovante','AlienacaoController@devolver_valor_inserir');
                    Route::post('alienacao/devolucao-valor/relatorio-devolver-valor-produto','AlienacaoController@relatorio_devolver_valor_produto');
                    Route::post('alienacao/devolucao-valor/relatorio-devolver-valor-produto/salvar','AlienacaoController@salvar_relatorio_devolver_valor_produto');
                });

                Route::group(['middleware' => 'verifica.modulo:2'], function () {
                    /*GERACAO RECIBO LOTE*/
                    Route::get('recibo','ReciboLoteController@index');
                    Route::post('recibo','ReciboLoteController@index');
                    Route::post('gerar-recibo-lote','ReciboLoteController@gerar_recibo_lote');
                    Route::get('recibo/filtrar/{grupo}/{filtro?}','ReciboLoteController@index');
                    Route::post('recibo/filtrar/{grupo}/{filtro?}','ReciboLoteController@index');
                });


				Route::group(['middleware' => 'verifica.modulo:2|8|9|12|13'], function () {
					Route::get('alienacao','AlienacaoController@index');
					Route::post('alienacao','AlienacaoController@index');
					Route::get('alienacao/filtrar/{grupo}/{filtro?}','AlienacaoController@index');
					Route::post('alienacao/filtrar/{grupo}/{filtro?}','AlienacaoController@index');
					
					Route::post('alienacao/nova-alienacao','AlienacaoController@nova');
					Route::post('alienacao/inserir-alienacao','AlienacaoController@inserir');
					Route::post('alienacao/alterar-alienacao','AlienacaoController@alterar');
					Route::post('alienacao/detalhes','AlienacaoController@detalhes');
					Route::post('alienacao/nova-resposta','AlienacaoController@nova_resposta');
										
					Route::post('alienacao/gerar-arquivo/previsualizar','AlienacaoController@gerar_arquivo_previsualizacao');
					Route::get('alienacao/gerar-arquivo/render/{tipo}/{id_alienacao}/{no_arquivo}','AlienacaoController@gerar_arquivo_render');
					Route::post('alienacao/gerar-arquivo/salvar','AlienacaoController@gerar_arquivo_salvar');
					
					Route::post('alienacao/novo-andamento','AlienacaoController@novo_andamento');
					Route::post('alienacao/resultado-acao','AlienacaoController@resultado_acao');
					Route::post('alienacao/inserir-andamento','AlienacaoController@inserir_andamento');
					Route::post('alienacao/detalhes-andamento','AlienacaoController@detalhes_andamento');
					Route::post('alienacao/detalhes-andamento/inserir-arquivos','AlienacaoController@detalhes_andamento_inserir_arquivos');

					Route::match(['get','post'],'alienacao/remover-interacao-andamento','AlienacaoController@remover_interacao_andamento');
                    Route::post('alienacao/detalhes-andamento-excluido','AlienacaoController@detalhes_andamento_excluido');

					Route::post('alienacao/gerar-recibo','AlienacaoController@gerar_recibo');
                    Route::get('alienacao/imprimir-recibo/{id_alienacao}','AlienacaoController@imprimir_recibo');

                    Route::post('alienacao/gerar-recibo-caixa','AlienacaoController@gerar_recibo_caixa');
                    Route::get('alienacao/gerar-recibo-caixa/render/{tipo}/{id_alienacao}/','AlienacaoController@imprimir_recibo_caixa');

					Route::post('alienacao/detalhes-custas','AlienacaoController@detalhes_custas');

					Route::post('alienacao/relatorio','AlienacaoController@relatorio_alienacoes');
					Route::post('alienacao/relatorio/salvar','AlienacaoController@salvar_relatorio_alienacoes');
                    
                    Route::post('alienacao/relatorio/cancelamento-notificacoes','AlienacaoController@relatorio_cancelamento');
                    Route::post('alienacao/relatorio/cancelamento-notificacoes/salvar','AlienacaoController@salvar_relatorio_cancelamento');
                    
					Route::post('alienacao/encaminhar-alienacoes','AlienacaoController@encaminhar_alienacoes');
					Route::post('alienacao/aprovar-orcamentos','AlienacaoController@aprovar_orcamentos');
					Route::post('alienacao/aprovar-decursos-prazo','AlienacaoController@aprovar_decursos_prazo');
					Route::post('alienacao/cancelar-alienacoes','AlienacaoController@cancelar_alienacoes');

					Route::post('alienacao/imovel/novo','AlienacaoController@novo_imovel');
					Route::post('alienacao/imovel/detalhes','AlienacaoController@detalhes_imovel');
					Route::post('alienacao/imovel/inserir','AlienacaoController@inserir_imovel');
					Route::post('alienacao/imovel/remover','AlienacaoController@remover_imovel');
					Route::post('alienacao/imovel/alterar-imovel','AlienacaoController@alterar_imovel');

					Route::post('alienacao/devedor/novo','AlienacaoController@novo_devedor');
					Route::post('alienacao/devedor/detalhes','AlienacaoController@detalhes_devedor');
					Route::post('alienacao/devedor/inserir','AlienacaoController@inserir_devedor');
					Route::post('alienacao/devedor/remover','AlienacaoController@remover_devedor');
					Route::post('alienacao/devedor/alterar-devedor','AlienacaoController@alterar_devedor');
	
					Route::post('alienacao/endereco/novo','AlienacaoController@novo_endereco');
					Route::post('alienacao/endereco/detalhes','AlienacaoController@detalhes_endereco');
					Route::post('alienacao/endereco/inserir','AlienacaoController@inserir_endereco');
					Route::post('alienacao/endereco/remover','AlienacaoController@remover_endereco');
	
					Route::post('alienacao/parcela/novo','AlienacaoController@nova_parcela');
					Route::post('alienacao/parcela/detalhes','AlienacaoController@detalhes_parcela');
					Route::post('alienacao/parcela/inserir','AlienacaoController@inserir_parcela');
					Route::post('alienacao/parcela/remover','AlienacaoController@remover_parcela');
					
					Route::post('alienacao/observacoes','AlienacaoController@observacoes');
					Route::post('alienacao/inserir-observacao','AlienacaoController@inserir_observacao');
					Route::post('alienacao/remover-arquivos-sessao','AlienacaoController@remover_arquivos_sessao');
					Route::post('alienacao/status-observacoes','AlienacaoController@status_observacao');

					Route::get('alienacao/credor/novo','CredorAlienacaoController@novo');
					Route::post('alienacao/credor/inserir','CredorAlienacaoController@inserir');
					Route::post('alienacao/credor/total','CredorAlienacaoController@total');
					Route::post('alienacao/credor/detalhes','CredorAlienacaoController@detalhes');
					Route::post('alienacao/credor/detalhesJSON','CredorAlienacaoController@detalhesJSON');

					Route::post('alienacao/alienacao-importar','AlienacaoController@importar');
					Route::post('alienacao/baixa-alienacao-importar','AlienacaoController@importar_baixa');
					Route::post('alienacao/alienacao-importar/assinar-xml','AlienacaoController@nova_assinatura_xml');
					Route::post('alienacao/alienacao-importar/iniciar-assinar-xml','AlienacaoController@iniciar_assinatura_xml');
					Route::post('alienacao/alienacao-importar/finalizar-assinar-xml','AlienacaoController@finalizar_assinatura_xml');
					Route::post('alienacao/alienacao-importar/visualizar-assinatura-xml','AlienacaoController@visualizar_assinatura_xml');

					//pagamento alienacao
					Route::get('alienacao-pagamento','AlienacaoPagamentoController@index');
					Route::post('alienacao-pagamento/novo','AlienacaoPagamentoController@novo');
					Route::post('alienacao-pagamento/detalhes','AlienacaoPagamentoController@detalhes');
					Route::post('alienacao-pagamento/inserir-pagamento','AlienacaoPagamentoController@inserir_pagamento');
					Route::post('alienacao-pagamento/novo-comprovante','AlienacaoPagamentoController@novo_comprovante');
					Route::post('alienacao-pagamento/inserir-comprovante','AlienacaoPagamentoController@inserir_comprovante');
					Route::post('alienacao-pagamento/comprovante','AlienacaoPagamentoController@comprovante');
					Route::post('alienacao-pagamento/aprovar-pagamento','AlienacaoPagamentoController@aprovar_pagamento');
					Route::post('alienacao-pagamento/reprovar-pagamento','AlienacaoPagamentoController@reprovar_pagamento');

					//rotas para previzualização de arquivos
                    Route::post('alienacao-pagamento/gerar-arquivo/previsualizar','AlienacaoPagamentoController@gerar_arquivo_previsualizacao');
                    Route::get('alienacao-pagamento/gerar-arquivo/render/{tipo}/{id_alienacao_pagamento}/{no_arquivo}','AlienacaoPagamentoController@gerar_arquivo_render');
                    Route::get('alienacao-pagamento/salvar-relatorio-pagamento/{id_alienacao_pagamento}', 'AlienacaoPagamentoController@salvar_relatorio_pagamento');

					//pagamento alienacao emolumentos
                    Route::match(['get','post'],'alienacao-pagamento-emolumento','AlienacaoPagamentoEmolumentoController@index');
                    Route::post('alienacao-pagamento-emolumento/novo','AlienacaoPagamentoEmolumentoController@novo');
                    Route::post('alienacao-pagamento-emolumento/detalhes','AlienacaoPagamentoEmolumentoController@detalhes');
                    Route::post('alienacao-pagamento-emolumento/inserir-pagamento','AlienacaoPagamentoEmolumentoController@inserir_pagamento');
                    Route::post('alienacao-pagamento-emolumento/novo-comprovante','AlienacaoPagamentoEmolumentoController@novo_comprovante');
                    Route::post('alienacao-pagamento-emolumento/inserir-comprovante','AlienacaoPagamentoEmolumentoController@inserir_comprovante');
                    Route::post('alienacao-pagamento-emolumento/comprovante','AlienacaoPagamentoEmolumentoController@comprovante');
                    Route::post('alienacao-pagamento-emolumento/aprovar-pagamento','AlienacaoPagamentoEmolumentoController@aprovar_pagamento');
                    Route::post('alienacao-pagamento-emolumento/reprovar-pagamento','AlienacaoPagamentoEmolumentoController@reprovar_pagamento');

                    Route::post('alienacao-pagamento-emolumento/remover-comprovante','AlienacaoPagamentoEmolumentoController@remover_comprovante');
                    Route::post('alienacao-pagamento-emolumento/novo-arquivo-comprovante','AlienacaoPagamentoEmolumentoController@novo_arquivo_comprovante');
                    Route::post('alienacao-pagamento-emolumento/alterar-comprovante','AlienacaoPagamentoEmolumentoController@alterar_comprovante');

                    //imprimir recibo de pagamento emolumento
                    Route::post('alienacao-pagamento-emolumento/gerar-recibo','AlienacaoPagamentoEmolumentoController@imprimir_recibo');
                    Route::get('alienacao-pagamento-emolumento/imprimir-recibo/render/{id_alienacao_valor_repasse_lote}','AlienacaoPagamentoEmolumentoController@render_recibo');
                    Route::get('alienacao-pagamento-emolumento/gerar-relatorio/{tipo}/{id_alienacao_valor_repasse_lote}','AlienacaoPagamentoEmolumentoController@gerar_relatorio_pagamento');

                    //rotas excluivas para pagamento da caixa tanto serviço/emolumentos
                    Route::post('alienacao-pagamento/novo-comprovante-caixa','AlienacaoPagamentoController@novo_comprovante_caixa');
                    Route::post('alienacao-pagamento/inserir-comprovante-caixa','AlienacaoPagamentoController@inserir_comprovante_caixa');
                    Route::post('alienacao-pagamento/detalhe-historico-pagamento-caixa','AlienacaoPagamentoController@detalhe_historico_pagamento_caixa');
                    Route::post('alienacao-pagamento/gerar-recibo-caixa','AlienacaoPagamentoController@gerar_recibo_caixa');
                    Route::get('alienacao-pagamento/imprimir-recibo-caixa/{id_alienacao_valor_repasse_lote}','AlienacaoPagamentoController@imprimir_recibo_caixa');

                    // Filtros
                    Route::post('alienacao/filtros-alertas','AlienacaoController@filtro_alertas');

                    //pagamento produtos [SERTIDAO E PESQUISA ELETRONICA]
                    //no momento os dois pagamentos são identicos somente trocando o repasse lembrar de copiar alterações para ambas as pastas
                    Route::get('produto-pagamento','ProdutoPagamentoController@index');
                    Route::post('produto-pagamento/novo','ProdutoPagamentoController@novo');
                    Route::post('produto-pagamento/detalhes','ProdutoPagamentoController@detalhes');
                    Route::post('produto-pagamento/inserir-pagamento','ProdutoPagamentoController@inserir_pagamento');
                    Route::post('produto-pagamento/novo-comprovante','ProdutoPagamentoController@novo_comprovante');
                    Route::post('produto-pagamento/inserir-comprovante','ProdutoPagamentoController@inserir_comprovante');
                    Route::post('produto-pagamento/comprovante','ProdutoPagamentoController@comprovante');
                    Route::post('produto-pagamento/aprovar-pagamento','ProdutoPagamentoController@aprovar_pagamento');
                    Route::post('produto-pagamento/reprovar-pagamento','ProdutoPagamentoController@reprovar_pagamento');
                    Route::post('produto-pagamento/gerar-recibo','ProdutoPagamentoController@gerar_recibo');
                    Route::get('produto-pagamento/imprimir-recibo/{id_pagamento_repasse_lote}','ProdutoPagamentoController@imprimir_recibo');

                    //pagamento produtos emolumentos [SERTIDAO E PESQUISA ELETRONICA]
                    Route::match(['get','post'],'produto-pagamento-emolumento','ProdutoPagamentoEmolumentoController@index');
                    Route::post('produto-pagamento-emolumento/novo','ProdutoPagamentoEmolumentoController@novo');
                    Route::post('produto-pagamento-emolumento/detalhes','ProdutoPagamentoEmolumentoController@detalhes');
                    Route::post('produto-pagamento-emolumento/inserir-pagamento','ProdutoPagamentoEmolumentoController@inserir_pagamento');
                    Route::post('produto-pagamento-emolumento/novo-comprovante','ProdutoPagamentoEmolumentoController@novo_comprovante');
                    Route::post('produto-pagamento-emolumento/inserir-comprovante','ProdutoPagamentoEmolumentoController@inserir_comprovante');
                    Route::post('produto-pagamento-emolumento/comprovante','ProdutoPagamentoEmolumentoController@comprovante');
                    Route::post('produto-pagamento-emolumento/aprovar-pagamento','ProdutoPagamentoEmolumentoController@aprovar_pagamento');
                    Route::post('produto-pagamento-emolumento/reprovar-pagamento','ProdutoPagamentoEmolumentoController@reprovar_pagamento');
                    Route::post('produto-pagamento-emolumento/gerar-recibo','ProdutoPagamentoEmolumentoController@gerar_recibo');
                    Route::get('produto-pagamento-emolumento/imprimir-recibo/{id_pagamento_repasse_pessoa_lote}','ProdutoPagamentoEmolumentoController@imprimir_recibo');
                    Route::post('produto-pagamento-emolumento/remover-comprovante','ProdutoPagamentoEmolumentoController@remover_comprovante');
                    Route::post('produto-pagamento-emolumento/novo-arquivo-comprovante','ProdutoPagamentoEmolumentoController@novo_arquivo_comprovante');
                    Route::post('produto-pagamento-emolumento/alterar-comprovante','ProdutoPagamentoEmolumentoController@alterar_comprovante');
                    Route::post('produto-pagamento-emolumento/detalhes-lote-pagamentos','ProdutoPagamentoEmolumentoController@detalhes_lote_pagamentos');
                    Route::post('produto-pagamento-emolumento/gerar-recibo-lote','ProdutoPagamentoEmolumentoController@gerar_recibo_lote');
                    Route::get('produto-pagamento-emolumento/gerar-recibo-lote/render/{id_pagamento_repasse_lote}','ProdutoPagamentoEmolumentoController@render_recibo_lote');
				});
			});

            Route::group(['prefix' => '/exportar/'], function () {
                Route::group(['middleware' => 'verifica.modulo:8|13'], function () {
                    Route::get('novo-arquivo-xml-fases-caixa','ExportarArquivosController@novo_arquivo_xml_fases_caixa');
                    Route::get('download-arquivo-fase/{id_alienacao_arquivo_xml}/{assinado}','ExportarArquivosController@download_arquivo_fase');
                    Route::post('exportar-xml-fases-caixa','ExportarArquivosController@exportar_xml_fases_caixa');
                });
            });


			Route::group(['prefix' => '/creditos/'], function () {
				Route::group(['middleware' => 'verifica.modulo:3|4|7'], function () {
					Route::get('','CompraCreditoController@index');
					Route::post('cancelar-transacao','CompraCreditoController@cancelar_transacao');
					Route::post('nova-compra','CompraCreditoController@nova_compra');
					Route::post('buscar-saldo','CreditoController@buscar_saldo');
					Route::post('valida-transacao-debito','CompraCreditoController@valida_transacao_debito');
					Route::post('inserir-compra-cartao-credito','CompraCreditoController@compra_credito');
					Route::post('inserir-compra-cartao-debito','CompraCreditoController@compra_debito');
					Route::post('inserir-compra-boleto','CreditoController@inserir_compra_boleto');
					Route::get('gerar-boleto/{id_boleto}','CreditoController@gerar_boleto')->where('id_boleto', '[0-9]+');
					Route::match(['get','post'],'retorno-debito','CompraCreditoController@retorno_compra_debito');
				});
			});
			
			Route::group(['prefix' => '/relatorios/'], function () {
				Route::group(['middleware' => 'verifica.modulo:3|4|7|8'], function () {
					Route::match(['get', 'post'],'movimentacao-financeira','RelatorioController@movimentacao_financeira');
				});
				Route::group(['middleware' => 'verifica.modulo:2'], function () {
					Route::match(['get', 'post'],'movimentacao-pedido','RelatorioController@movimentacao_pedido');
					Route::get('tabela-preco','RelatorioController@tabela_preco');
					Route::match(['get', 'post'], 'acompanha-servico-nao-pago','RelatorioController@mov_serv_nao_pago_caixa');
				});
				Route::group(['middleware' => 'verifica.modulo:9'], function () {
					Route::match(['get', 'post'], 'controle-acesso','RelatorioController@controle_acesso');
                    Route::match(['get', 'post'],'acompanhamento-notificacao','RelatorioController@acompanhamento_notificacao');
					Route::get('dados-cadastrais','RelatorioController@dados_cadastrais');
					Route::get('salvar-dados-cadastrais','RelatorioController@salvar_dados_cadastrais');
					Route::get('creditos-compra','RelatorioController@creditos_compra');
				});
				Route::group(['middleware' => 'verifica.modulo:8'], function () {
                    Route::match(['get', 'post'],'notificacao-aguardando-interacao','RelatorioController@notificacao_aguardando_interacao');
                    Route::match(['get', 'post'], 'salvar-relatorio-excel-aguardando-interecao','RelatorioController@salvar_relatorio_excel_aguardando_interecao');
					Route::match(['get', 'post'], 'observacoes-enviadas-serventia','RelatorioController@observacoes_enviadas_serventia');
                });
				Route::group(['middleware' => 'verifica.modulo:8|9'], function () {
					Route::match(['get', 'post'], 'movimentacao-financeira-anoreg','RelatorioController@acompanha_pagamento_caixa');
					Route::match(['get', 'post'], 'salvar-relatorio-excel','RelatorioController@salvar_relatorio_excel');
					Route::match(['get', 'post'], 'salvar-relatorio-excel-acomp-notificacao','RelatorioController@salvar_relatorio_excel_acomp_notificacao');
					Route::match(['get', 'post'], 'pagamento-servico-serventia','RelatorioController@mov_fin_pgto_serv_serventia');
                    Route::get('alienacao-geral','RelatorioController@alienacao_geral');
                    Route::post('alienacao-geral/salvar','RelatorioController@alienacao_geral_salvar');
				});
			});
			
			Route::group(['prefix' => '/arquivos/'], function() {
				Route::post('novo','ArquivosController@novo');
				Route::post('inserir','ArquivosController@inserir');
				Route::post('remover','ArquivosController@remover');
				Route::post('remover-todos','ArquivosController@remover_todos');

				
				Route::post('assinatura/nova','ArquivosController@nova_assinatura');
				Route::post('assinatura/iniciar','ArquivosController@iniciar_assinatura');
				Route::post('assinatura/finalizar','ArquivosController@finalizar_assinatura');

				Route::post('visualizar','ArquivosController@visualizar_arquivo');
				Route::get('render/{id_arquivo_grupo_produto}/{no_arquivo}','ArquivosController@render_arquivo');
				Route::get('download/{id_arquivo_grupo_produto}','ArquivosController@download_arquivo');

				Route::post('certificado','ArquivosController@certificado_arquivo');
				
				Route::get('certidao/{id_pedido}/{no_arquivo}','CertidaoController@arquivo_resultado');
				Route::get('penhora/recibo/{id_pedido}','PenhoraController@arquivo_recibo');
				Route::get('oficio/recibo/{id_documento}','OficioController@arquivo_recibo');
				Route::get('certidao-matricula/{id_pedido}/{no_arquivo}','CertidaoMatriculaController@arquivo_resultado');
				Route::get('penhora/{id_penhora_bem_imovel}/{no_arquivo}','PenhoraController@arquivo_averbacao');
				Route::get('pesquisa/{id_pedido}/{no_arquivo}','PesquisaController@arquivo_resultado');
				Route::get('protocolo/{id_protocolo_arquivo}/{no_arquivo}','ProtocoloController@arquivo_arquivo');
				Route::get('alienacao/{id_arquivo_grupo_produto}/{no_arquivo}','AlienacaoController@arquivo_arquivo');
				Route::get('alienacao-pagamento/{id_alienacao_pagamento}/{no_arquivo}','AlienacaoPagamentoController@arquivo_comprovante');

				
				Route::get('download/certidao-matricula/{id_pedido}','CertidaoMatriculaController@download_resultado');
				Route::get('download/certidao/{id_pedido}','CertidaoController@download_resultado');
				Route::get('download/penhora/exigencia/{id_penhora_exigencia}','PenhoraController@download_exigencia');
				Route::get('download/penhoras/{id_documento_penhora}','PenhoraController@download_ducumentos_penhora');
				Route::get('download/penhoras/isencao/{id_penhora}','PenhoraController@download_ducumentos_isencao');
				Route::get('download/penhora/averbacao/{id_penhora_bem_imovel}','PenhoraController@download_averbacao');
				Route::get('download/penhora/averbacao/principal/{id_penhora_averbacao}','PenhoraController@download_penhora_averbacao');
				Route::get('download/pesquisa/{id_pedido}','PesquisaController@download_resultado');
				Route::get('download/oficio/{id_documento}','OficioController@download_arquivo');
				Route::get('download/oficio/isencao/{id_documento}','OficioController@download_arquivo_isencao');
				Route::get('download/protocolo/{id_protocolo_arquivo}','ProtocoloController@download_arquivo');
				Route::get('download/alienacao/{id_arquivo_grupo_produto}','AlienacaoController@download_arquivo');
				Route::get('download/alienacao-andamento/{tipo}/{id_andamento_alienacao}','AlienacaoController@download_arquivo_andamento');
				Route::get('download/alienacao-pagamento/{id_alienacao_pagamento}','AlienacaoPagamentoController@download_comprovante');
                Route::get('download/comprovante-pagamento-caixa/{id_alienacao_valor_repasse_lote}','AlienacaoPagamentoController@download_comprovante_caixa');

                //pagamento produto servico/emolumento
                Route::get('download/produto-pagamento-emolumento/{id_pagamento_repasse_pessoa_lote}','ProdutoPagamentoEmolumentoController@download_comprovante');
                Route::get('download/produto-pagamento/{id_pagamento_repasse_lote}','ProdutoPagamentoController@download_comprovante');

                Route::get('produto-pagamento/{id_pagamento_repasse_lote}/{no_arquivo}','ProdutoPagamentoController@arquivo_comprovante');
                Route::get('produto-pagamento-emolumento/{id_pagamento_repasse_pessoa_lote}/{no_arquivo}','ProdutoPagamentoEmolumentoController@arquivo_comprovante');
				
				Route::get('download/detran/comunicado-venda/{id_arquivo_grupo_produto}','DetranVendaVeiculoController@download_arquivo');
			});

			Route::group(['prefix' => '/cadastros/'], function() {
				Route::group(['middleware' => 'verifica.modulo:1|7|13'], function () {
					Route::get('cargos','CargosController@index');
					Route::post('cargos/novo','CargosController@novo');
					Route::post('cargos/inserir','CargosController@inserir');
					Route::post('cargos/detalhes','CargosController@detalhes');
				});
				Route::group(['middleware' => 'verifica.modulo:1|7|13'], function () {
					Route::get('varas','VaraController@index');
					Route::post('varas/nova','VaraController@nova');
					Route::post('varas/inserir','VaraController@inserir');
					Route::post('varas/detalhes','VaraController@detalhes');
					
					Route::post('varas/alterar','VaraController@alterar');
					Route::post('varas/salvar','VaraController@salvar');
					Route::post('varas/remover','VaraController@remover');
				});
			});
		});
	});

	Route::group(['prefix' => '/ws/'], function () {
		Route::get('xml_matriculas','XMLController@carregar_matriculas');
		Route::get('imagens_matriculas','MatriculaController@carregar_imagens');
	});

	\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
        /*echo '<pre>';
            var_dump($query->sql).'<br>';
            var_dump($query->bindings).'<br>';
        echo '</pre>';*/
    });
});
