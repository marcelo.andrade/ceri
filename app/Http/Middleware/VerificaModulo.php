<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;
use Session;

use App\pessoa_modulo;

class VerificaModulo {

    public function handle($request, Closure $next, $modulos) {
        $modulos = preg_split('/(\s)*\|(\s)*/', $modulos);

        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');
            $id_pessoa = $pessoa_ativa->id_pessoa;
        } else {
            $id_pessoa = Auth::User()->id_pessoa;
        }

        $pessoa_modulo = new pessoa_modulo();
        $verifica = $pessoa_modulo->where('id_pessoa',$id_pessoa)
                                  ->whereIn('id_modulo',$modulos)->count();
        
        if ($verifica>0) {
            return $next($request);
        } else {
            return response()->view('erros.401');
        }
    }
}
