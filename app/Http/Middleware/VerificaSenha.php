<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;

class VerificaSenha {

    public function handle($request, Closure $next) {
        if (Auth::User()->in_alterar_senha=='S') {
			return response()->view('usuarios.primeira-senha');
        } else {
            return $next($request);
        }
    }
}
