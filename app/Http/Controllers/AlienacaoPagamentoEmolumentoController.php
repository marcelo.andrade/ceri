<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\pedido_pessoa;
use App\relatorio;
use App\serventia;
use App\usuario;
use App\usuario_pessoa;
use Illuminate\Http\Request;

use Excel;
use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;

use App\pedido;
use App\alienacao_pagamento;
use App\alienacao_pagamento_alienacao;
use App\alienacao;
use App\alienacao_valor;
use App\estado;
use App\situacao_pedido_grupo_produto;
use App\alienacao_valor_repasse;
use App\alienacao_valor_repasse_lote;

class AlienacaoPagamentoEmolumentoController extends Controller
{

    const ID_GRUPO_PRODUTO = 7;
    const ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP = 1;
    const ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF = 2;
    const ID_SITUACAO_PAGAMENTO_NAOAPROVADO = 3;
    const ID_SITUACAO_PAGAMENTO_APROVADO = 4;
    const ID_SITUACAO_ALIENACAO_EMPROCESSAMENTO = 55;
    const ID_SITUACAO_ALIENACAO_FINALIZADO = 57;
    const ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTO = 65;
    const ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTOFINALIZ = 66;
    const ID_TIPO_PESSOA_CAIXA = 8;
    const ID_TIPO_PESSOA_ANOREG = 9;
    const ID_REPASSE_CAIXA = 1;
    const ID_REPASSE_ANOREG = 3;
    const ID_SITUACAO_PEDIDO_GRUPO_PRODUTO = 54;
    const ID_PRODUTO = 19;
    const ID_PRODUTO_ITEM = 28;
    const ID_NUMERO_LOTE = 8;

    public function __construct(alienacao_pagamento $alienacao_pagamento, CeriFuncoes $ceriFuncoes)
    {
        $this->alienacao_pagamento = $alienacao_pagamento;

        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');
            $this->usuario = Auth::User();

            if(Session::has('troca_pessoa')) {
                $this->usuario = $pessoa_ativa->pessoa->getUsuarioRelacionado()->usuario;
            }

            $this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
            $this->id_pessoa = $pessoa_ativa->id_pessoa;
        } else {
            $this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
            $this->id_pessoa = Auth::User()->id_pessoa;
        }
    }

    public function index(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote, estado $estado)
    {
        $class = $this;

        switch ($this->id_tipo_pessoa) {
            case 8: //caixa
                $paginacao = true;
                $pagamentos_pendentes = $this->getAlienacoesCaixa($paginacao);

                //Filtro de pagamentos
                if ($request->dt_ini_pagamentos != '' and $request->dt_fim_pagamentos != '') {
                    $dt_ini_pagamentos = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ini_pagamentos . ' 00:00:00');
                    $dt_fim_pagamentos = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_fim_pagamentos . ' 23:59:59');
                    $pagamentos_pendentes->whereBetween('alienacao_valor_repasse_lote.dt_repasse_lote', array($dt_ini_pagamentos, $dt_fim_pagamentos));
                }
                if ($request->protocolo > 0) {
                    $num_protocolo = wordwrap($request->protocolo, 4, '.', true);
                    $pagamentos_pendentes->where('alienacao_valor_repasse_lote.protocolo_pedido', $num_protocolo);
                }

                $todos_pagamentos_pendentes = $pagamentos_pendentes->paginate(10, ['*'], 'pagamentos-pendentes');

                $todos_pagamentos_pendentes->appends(Request::capture()->except('_token'))->render();

                return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento', compact('this', 'class', 'request', 'todos_pagamentos_pendentes','total_pagamentos_pendentes'));
                break;
            case 9://anoreg
                $pagamentos_pendentes = $this->alienacao_pagamento->where('id_usuario_cad', $this->usuario->id_usuario)
                    ->whereIn('id_situacao_alienacao_pagamento', array($this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP, $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF))
                    ->get();

                $pagamentos = $this->alienacao_pagamento->where('id_usuario_cad', $this->usuario->id_usuario)
                    ->whereIn('id_situacao_alienacao_pagamento', array($this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO, $this::ID_SITUACAO_PAGAMENTO_APROVADO));

                if ($request->dt_ini_pagamentos != '' and $request->dt_fim_pagamentos != '') {
                    $dt_ini_pagamentos = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ini_pagamentos . ' 00:00:00');
                    $dt_fim_pagamentos = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_fim_pagamentos . ' 23:59:59');
                    $pagamentos->whereBetween('dt_pagamento', array($dt_ini_pagamentos, $dt_fim_pagamentos));
                }
                if ($request->serventia_pagamentos > 0) {
                    $pagamentos->where('id_serventia', '=', $request->serventia_pagamentos);
                }

                $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

                $total_pagamentos = $pagamentos->count();
                $todos_pagamentos = $pagamentos->orderBy('dt_pagamento','id_situacao_alienacao_pagamento','desc')->paginate(10, ['*'], 'pagamentos-pag');

                $paginacao = true;
                $pagamentos_recebidos       = $this->getAlienacoesCaixa($paginacao);

                $total_pagamentos_recebidos = $pagamentos_recebidos->count();
                $todos_pagamentos_recebidos = $pagamentos_recebidos->paginate(10, ['*'], 'pagamentos-recebidos');

                $todos_pagamentos_recebidos->appends(Request::capture()->except('pagamentos-pag'))->render();

                return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento', compact('this', 'class', 'request', 'todos_pagamentos', 'total_pagamentos', 'pagamentos_pendentes', 'todos_pagamentos_recebidos', 'total_pagamentos_recebidos', 'cidades'));
                break;
            case 2:
            case 10:
                $pagamentos_pendentes = $this->alienacao_pagamento->where('id_serventia', Auth::User()->usuario_serventia->id_serventia)
                    ->where('id_situacao_alienacao_pagamento', $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF)
                    ->get();
                $pagamentos = $this->alienacao_pagamento->where('id_serventia', Auth::User()->usuario_serventia->id_serventia)
                    ->whereIn('id_situacao_alienacao_pagamento', array($this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO, $this::ID_SITUACAO_PAGAMENTO_APROVADO))
                    ->get();
                return view('servicos.alienacao-pagamento-emolumento.serventia-alienacao-pagamento', compact('this', 'class', 'request', 'pagamentos', 'pagamentos_pendentes'));
                break;
        }
    }

    public function novo(Request $request, alienacao $alienacao, estado $estado, situacao_pedido_grupo_produto $situacao)
    {

        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        $situacoes = $situacao->where('in_registro_ativo', 'S')
            ->where('id_grupo_produto', $this::ID_GRUPO_PRODUTO)
            ->orderBy('nu_ordem')->get();


        if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_CAIXA) {
            $alienacoes = $alienacao->select(
                                                'pedido.protocolo_pedido',
                                                'pedido.dt_pedido',
                                                'alienacao.numero_contrato',
                                                'alienacao.id_alienacao',
                                                'serventia.no_serventia',
                                                'fase_grupo_produto.no_fase',
                                                'etapa_fase.no_etapa',
                                                'acao_etapa.no_acao',
                                                DB::Raw('ceri.f_lista_devedor_alienacao(alienacao.id_alienacao, \'\', \' / \') as no_devedores'),
                                                DB::Raw('sum(alienacao_valor.va_total) as va_total'),
                                                'alienacao_pedido.id_pedido',
                                                'serventia.id_serventia'

                                            )
                                    ->join('alienacao_valor','alienacao_valor.id_alienacao','=','alienacao.id_alienacao')
                                    ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                    ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                    ->join('serventia','serventia.id_serventia','=','alienacao.id_serventia')
                                    ->join('alienacao_andamento_situacao', function($join){
                                        $join->on('alienacao_andamento_situacao.id_alienacao_pedido',   '=', 'alienacao_pedido.id_alienacao_pedido')
                                             ->on('alienacao_andamento_situacao.in_registro_ativo','=' ,DB::raw("'S'"));
                                    })
                                    ->join('fase_grupo_produto','fase_grupo_produto.id_fase_grupo_produto','=','alienacao_andamento_situacao.id_fase_grupo_produto')
                                    ->join('etapa_fase','etapa_fase.id_etapa_fase','=','alienacao_andamento_situacao.id_etapa_fase')
                                    ->join('acao_etapa','acao_etapa.id_acao_etapa','=','alienacao_andamento_situacao.id_acao_etapa')
                                    ->leftjoin('alienacao_valor_repasse', function($join){
                                        $join->on('alienacao_valor_repasse.id_alienacao_valor',   '=', 'alienacao_valor.id_alienacao_valor')
                                            ->on('alienacao_valor_repasse.id_repasse', '=',  DB::raw('1'));
                                    })
                                    ->where('alienacao_valor.id_produto_item', '<>', DB::raw('28'))
                                    ->whereNull('alienacao_valor_repasse.dt_repasse')
                                    ->whereRaw('ceri.f_verifica_valor_aprovado_alienacao (alienacao.id_alienacao, alienacao_valor.id_produto_item) = \'S\'')
                                    ->groupBy(  'pedido.protocolo_pedido',
                                                'pedido.dt_pedido',
                                                'alienacao.numero_contrato',
                                                'alienacao.id_alienacao',
                                                'serventia.no_serventia',
                                                'fase_grupo_produto.no_fase',
                                                'etapa_fase.no_etapa',
                                                'acao_etapa.no_acao',
                                                'alienacao_pedido.id_pedido',
                                                'serventia.id_serventia'

                );

        } else if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG) {

            $alienacoes = $alienacao->select(
                                            'pedido.protocolo_pedido',
                                            'pedido.dt_pedido',
                                            'alienacao.numero_contrato',
                                            'alienacao.id_alienacao',
                                            'fase_grupo_produto.no_fase',
                                            'etapa_fase.no_etapa',
                                            'acao_etapa.no_acao',
                                            DB::Raw('ceri.f_lista_devedor_alienacao(alienacao.id_alienacao, \'\', \' / \') as no_devedores'),
                                            DB::Raw('sum(alienacao_valor.va_total) as va_total'),
                                            'alienacao_pedido.id_pedido',
                                            'serventia.id_serventia',
                                            'serventia.no_serventia',
                                            'c.no_cidade',
                                            'b.no_banco',
                                            'b.codigo_banco',
                                            'bp.nu_agencia',
                                            'bp.nu_dv_agencia',
                                            'bp.nu_conta',
                                            'bp.nu_dv_conta',
                                            'bp.tipo_conta',
                                            'bp.nu_variacao',
                                            'bp.nu_cpf_cnpj_conta'
                                        )
                                            ->join('alienacao_valor','alienacao_valor.id_alienacao','=','alienacao.id_alienacao')
                                            ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                            ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                            ->join('serventia','serventia.id_serventia','=','alienacao.id_serventia')
                                            ->join('pessoa_endereco as pe','pe.id_pessoa','=','serventia.id_pessoa')
                                            ->join('endereco as e','e.id_endereco','=','pe.id_endereco')
                                            ->join('cidade as c','c.id_cidade','=','e.id_cidade')
											 /*
                                            ->leftjoin('usuario_pessoa as up', function ($join) {
                                                $join->on('up.id_pessoa', '=', 'serventia.id_pessoa')
                                                      ->on('up.id_usuario', 'in', DB::raw('(select up1.id_usuario 
                                                              from ceri.usuario up1 
                                                              inner join ceri.banco_pessoa bp1 on (bp1.id_pessoa = up1.id_pessoa)                                               
                                                              where up1.id_usuario = up.id_usuario
                                                              limit 1
                                                              )'));
                                            })
                                            ->leftjoin('usuario as us','us.id_usuario','=','up.id_usuario')
                                            ->leftjoin('banco_pessoa as bp','bp.id_pessoa','=','us.id_pessoa')
										*/	 
											->leftjoin('banco_pessoa as bp','bp.id_pessoa','=','serventia.id_pessoa')
                                            ->leftjoin('banco as b','b.id_banco','=','bp.id_banco')


                                            ->join('alienacao_andamento_situacao', function($join){
                                                $join->on('alienacao_andamento_situacao.id_alienacao_pedido',   '=', 'alienacao_pedido.id_alienacao_pedido')
                                                    ->on('alienacao_andamento_situacao.in_registro_ativo','=' ,DB::raw("'S'"));
                                            })
                                            ->join('fase_grupo_produto','fase_grupo_produto.id_fase_grupo_produto','=','alienacao_andamento_situacao.id_fase_grupo_produto')
                                            ->join('etapa_fase','etapa_fase.id_etapa_fase','=','alienacao_andamento_situacao.id_etapa_fase')
                                            ->join('acao_etapa','acao_etapa.id_acao_etapa','=','alienacao_andamento_situacao.id_acao_etapa')
                                            ->join('alienacao_valor_repasse as avr1', function ($join) {
                                                $join->on('avr1.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                                    ->on('avr1.id_repasse', '=', DB::raw('1'));
                                            })
                                            ->leftjoin('alienacao_valor_repasse as avr2', function ($join) {
                                                $join->on('avr2.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                                    ->on('avr2.id_repasse', '=', DB::raw('4'));
                                            })
                                            ->where('alienacao_valor.id_produto_item', '<>', DB::raw('28'))
                                            ->whereRaw('ceri.f_verifica_valor_aprovado_alienacao (alienacao.id_alienacao, alienacao_valor.id_produto_item) = \'S\'')
                                            ->whereNull('avr2.dt_repasse')
                                            ->groupBy(  'pedido.protocolo_pedido',
                                                'pedido.dt_pedido',
                                                'alienacao.numero_contrato',
                                                'alienacao.id_alienacao',
                                                'serventia.no_serventia',
                                                'fase_grupo_produto.no_fase',
                                                'etapa_fase.no_etapa',
                                                'acao_etapa.no_acao',
                                                'alienacao_pedido.id_pedido',
                                                'serventia.id_serventia',
                                                'serventia.id_pessoa',
                                                'serventia.no_serventia',
                                                'c.no_cidade',
                                                'b.no_banco',
                                                'b.codigo_banco',
                                                'bp.nu_agencia',
                                                'bp.nu_dv_agencia',
                                                'bp.nu_conta',
                                                'bp.nu_dv_conta',
                                                'bp.tipo_conta',
                                                'bp.nu_variacao',
                                                'bp.nu_cpf_cnpj_conta'
                                            )
											->orderby('c.no_cidade', 'asc');


        }

        if ($request->isMethod('post'))
        {
            if ($request->dt_inicio!='' and $request->dt_fim!='') {
                $dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
                $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                $alienacoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
            }
            if ($request->id_situacao_pedido_grupo_produto>0) {
                $alienacoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
            }
            if ($request->protocolo_pedido > 0){
                $alienacoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
            }
            if($request->numero_contrato>0){
                $alienacoes->where('alienacao.numero_contrato',$request->numero_contrato);
            }

            if ($request->id_serventia>0)
            {
                $alienacoes->where('serventia.id_serventia','=',$request->id_serventia);
            }
        }

        $alienacoes = $alienacoes->orderBy('alienacao.dt_cadastro','desc')->get();

        if ($request->acao == "pesquisar") {
            return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento-notificacoes-historico', compact('request', 'alienacoes', 'cidades', 'situacoes'));
        } else {
            return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento-novo', compact('request', 'alienacoes', 'cidades', 'situacoes'));
        }
    }

    public function inserir_pagamento(Request $request, pedido $pedido, pedido_pessoa $pedido_pessoa, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote, alienacao_valor $alienacao_valor)
    {
        $erro = 0;
        $erro_loop = false;
        DB::beginTransaction();

        if (count($request->id_alienacao) > 0) {
            if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG) {
                foreach ($request->id_alienacao as $key => $alienacoes) {
                    //um pagamento para casa serventia
                    $novo_pagamento = new alienacao_pagamento();
                    $novo_pagamento->va_pagamento = 0;
                    $novo_pagamento->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP;
                    $novo_pagamento->id_serventia = $key;

                    if ($novo_pagamento->save()) {
                        $valor_total = 0;

                        foreach ($alienacoes as $id_alienacao) {
                            //buscando valores alienacao_valor
                            $resultado_aliencao_valor = $this->get_alienacao_valor($alienacao_valor, $id_alienacao);

                            foreach ($resultado_aliencao_valor as $aliencao_valor) {
                                $valor_total += $aliencao_valor->va_total;

                                $nova_alienacao_pagamento_alienacao = new alienacao_pagamento_alienacao();
                                $nova_alienacao_pagamento_alienacao->id_alienacao = $id_alienacao;
                                $nova_alienacao_pagamento_alienacao->id_alienacao_pagamento = $novo_pagamento->id_alienacao_pagamento;
                                $nova_alienacao_pagamento_alienacao->id_alienacao_valor = $aliencao_valor->id_alienacao_valor;
                                $nova_alienacao_pagamento_alienacao->dt_cadastro = Carbon::now();

                                if (!$nova_alienacao_pagamento_alienacao->save()) {
                                    $erro_loop = true;
                                }

                            }

                            /*$alienacao = new alienacao();
                            $alienacao = $alienacao->find($id_alienacao);

                            if ($alienacao) {
                                $novo_pagamento->alienacoes()->attach($alienacao);
                            } else {
                                $erro_loop = true;
                            }*/


                        }

                        //atualizando o valor total
                        $novo_pagamento->va_pagamento = $valor_total;
                        if (!$novo_pagamento->save()) {
                            $erro_loop = true;
                        }
                    }
                }
            }

            //variaveis de controle para insert abaixo

            $va_repasse_lote = 0;
            $nu_quantidade_lote = 0;
            $arrayAlienacaoRepase = [];

            foreach ($request->id_alienacao as $key => $alienacoes) {
                foreach ($alienacoes as $id_alienacao) {
                    $resultado_aliencao_valor = $this->get_alienacao_valor($alienacao_valor, $id_alienacao);

                    foreach ($resultado_aliencao_valor as $aliencao_valor) {
                        $nova_alienacao_valor_repasse = new alienacao_valor_repasse();
                        $nova_alienacao_valor_repasse->id_alienacao_valor = $aliencao_valor->id_alienacao_valor;
                        $nova_alienacao_valor_repasse->id_repasse = ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG) ? 4 : 1;
                        $nova_alienacao_valor_repasse->va_repasse = $aliencao_valor->va_total;
                        $nova_alienacao_valor_repasse->dt_repasse = Carbon::now();
                        $nova_alienacao_valor_repasse->id_usuario_repasse = Auth::User()->id_usuario;
                        $nova_alienacao_valor_repasse->id_usuario_cad = Auth::User()->id_usuario;
                        $nova_alienacao_valor_repasse->save();

                        if (!$nova_alienacao_valor_repasse->save()) {
                            $erro_loop = true;
                        }

                        $va_repasse_lote += $aliencao_valor->va_total;
                        $arrayAlienacaoRepase[] = $nova_alienacao_valor_repasse->id_alienacao_valor_repasse;
                    }
                    $nu_quantidade_lote++;
                }
            }

            if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_CAIXA) {
                $protocoloPedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(" . Auth::User()->id_usuario . ", " . $this::ID_PRODUTO . ");"));

                $pedido->id_usuario = Auth::User()->id_usuario;
                $pedido->id_usuario_cad = Auth::User()->id_usuario;
                $pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_PEDIDO_GRUPO_PRODUTO;
                $pedido->id_produto = $this::ID_PRODUTO;
                $pedido->protocolo_pedido = $protocoloPedido[0]->f_geraprotocolo;
                $pedido->dt_pedido = Carbon::now();
                $pedido->dt_cadastro = Carbon::now();
                $pedido->nu_quantidade = 1;
                $pedido->de_pedido = 'Repasse - Notificação (Emolumento)';
                $pedido->id_pessoa_origem = $this->id_pessoa;

                if (!$pedido->save()) {
                    $erro_loop = true;
                }

                $pedido_pessoa->id_pedido = $pedido->id_pedido;
                $pedido_pessoa->id_pessoa = $this->id_pessoa;

                if (!$pedido_pessoa->save()) {
                    $erro_loop = true;
                }

                //salvando a tabela de lote
                $alienacao_valor_repasse_lote->id_repasse = ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG) ? 4 : 1;
                $alienacao_valor_repasse_lote->id_situacao_alienacao_pagamento = 1;
                $alienacao_valor_repasse_lote->protocolo_pedido = $protocoloPedido[0]->f_geraprotocolo;
                $alienacao_valor_repasse_lote->nu_quantidade_lote = $nu_quantidade_lote;
                $alienacao_valor_repasse_lote->va_repasse_lote = $va_repasse_lote;
                $alienacao_valor_repasse_lote->dt_repasse_lote = Carbon::now();
                $alienacao_valor_repasse_lote->id_usuario_repasse = Auth::User()->id_usuario;
                $alienacao_valor_repasse_lote->id_usuario_cad = Auth::User()->id_usuario;
                $alienacao_valor_repasse_lote->dt_cadastro = Carbon::now();

                if (!$alienacao_valor_repasse_lote->save()) {
                    $erro_loop = true;
                }

                //atualizar o valor do numero do lote
                //montando auto_incremento de acordo com a sequence do banco

                $alienacao_valor_repasse_lote->numero_lote = $alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
                if (!$alienacao_valor_repasse_lote->save()) {
                    $erro_loop = true;
                }

                //atualizando a tabela alienacao_valor_repasse como o codigo da tabela alienacao_valor_repasse_lote
                $atualizar_alienacao_valor_repasse = new alienacao_valor_repasse();

                $atualizar_alienacao_valor_repasse->whereIn('id_alienacao_valor_repasse', $arrayAlienacaoRepase)
                    ->update(['id_alienacao_valor_repasse_lote' => $alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote]);
            }

            //controle da variavel de loop/insert
            if ($erro_loop) {
                $erro = 2;
            }

        }

        // Tratamento do retorno
        if ($erro > 0) {
            DB::rollback();
            return response()->json(array('status' => 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
        } else {
            DB::commit();
            return response()->json(array('status' => 'sucesso',
                'recarrega' => 'true',
                'msg' => 'O pagamento foi inserido com sucesso.'));
        }
    }

    public function get_alienacao_valor($alienacao_valor, $id_alienacao)
    {
        if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_CAIXA) {
            //buscando valores alienacao_valor
            $resultado_aliencao_valor = $alienacao_valor
                                        ->select("alienacao_valor.*")
                                        ->leftjoin('alienacao_valor_repasse', function ($join) {
                                            $join->on('alienacao_valor_repasse.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                                ->on('alienacao_valor_repasse.id_repasse', '=', DB::raw('1'));
                                        })
                                        ->where('alienacao_valor.id_alienacao', '=', $id_alienacao)
                                        ->where('alienacao_valor.id_produto_item', '<>', DB::raw('28'))
                                        ->whereNull('alienacao_valor_repasse.dt_repasse')
                                        ->whereRaw('ceri.f_verifica_valor_aprovado_alienacao (alienacao_valor.id_alienacao, alienacao_valor.id_produto_item) = \'S\'')
                                        ->get();
        }

        if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG) {
            //buscando valores alienacao_valor
            $resultado_aliencao_valor = $alienacao_valor
                                            ->select("alienacao_valor.*")
                                            ->join('alienacao_valor_repasse as avr1', function ($join) {
                                                $join->on('avr1.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                                    ->on('avr1.id_repasse', '=', DB::raw('1'));
                                            })
                                            ->leftjoin('alienacao_valor_repasse as avr2', function ($join) {
                                                $join->on('avr2.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                                    ->on('avr2.id_repasse', '=', DB::raw('4'));
                                            })
                                            ->where('alienacao_valor.id_alienacao', '=', $id_alienacao)
                                            ->where('alienacao_valor.id_produto_item', '<>', DB::raw('28'))
                                            ->whereNull('avr2.dt_repasse')
                                            ->get();
        }

        return $resultado_aliencao_valor;
    }

    public function detalhes(Request $request)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $alienacao_pagamento = $this->alienacao_pagamento;
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);
            foreach ($alienacao_pagamento->alienacoes as $alienacao) {
                $alienacao->alienacao_pedido->pedido->visualizar_notificacoes();
            }

            $id_alienacao_pagamento = $request->id_alienacao_pagamento;
            $alienacao_pagamento_token = str_random(30);
            return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento-detalhes', compact('request', 'alienacao_pagamento', 'id_alienacao_pagamento', 'alienacao_pagamento_token'));
        }
    }

    public function novo_comprovante(Request $request)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $alienacao_pagamento = $this->alienacao_pagamento;
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

            return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento-novo-comprovante', compact('request', 'alienacao_pagamento'));
        }
    }

    public function novo_comprovante_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {
        $alienacao_valor_repasse_lote = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);
        return view('servicos.alienacao-pagamento.caixa-alienacao-pagamento-novo-comprovante', compact('request', 'alienacao_valor_repasse_lote'));
    }

    public function inserir_comprovante(Request $request)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $erro = 0;

            DB::beginTransaction();

            $nome_arquivo = remove_caracteres($request->no_arquivo_comprovante->getClientOriginalName());

            $alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
            $alienacao_pagamento->va_pago = converte_float($request->va_pago);
            $alienacao_pagamento->dt_pagamento = Carbon::createFromFormat('d/m/Y', $request->dt_pagamento);
            $alienacao_pagamento->no_arquivo_comprovante = $nome_arquivo;
            $alienacao_pagamento->no_local_arquivo_comprovante = 'alienacoes-pagamentos/' . $alienacao_pagamento->id_alienacao_pagamento;
            $alienacao_pagamento->no_extensao_comprovante = $request->no_arquivo_comprovante->getClientOriginalExtension();
            $alienacao_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF;

            //inserindo notificacao
            if (count($alienacao_pagamento->alienacoes) > 0) {
                foreach ($alienacao_pagamento->alienacoes as $alienacao) {
                    $pedido = new pedido();
                    $pedido_selecionado = $pedido->find($alienacao->alienacao_pedido->pedido->id_pedido);
                    $pedido_selecionado->inserir_notificacao($this->id_pessoa, $pedido_selecionado->pedido_pessoa_atual->id_pessoa, 'Uma nova notificação foi enviada. Protocolo ' . $pedido_selecionado->protocolo_pedido);

                }
            }

            if ($alienacao_pagamento->save()) {
                $destino = '../storage/app/public/alienacoes-pagamentos/' . $alienacao_pagamento->id_alienacao_pagamento;
                if (!File::isDirectory($destino)) {
                    File::makeDirectory($destino);
                }

                if (!$request->no_arquivo_comprovante->move($destino, $nome_arquivo)) {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }

            // Tratamento do retorno
            if ($erro > 0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();
                return response()->json(array('status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'O comprovante foi salvo com sucesso.'));
            }
        }
    }

    public function inserir_comprovante_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {
        if ($request->id_alienacao_valor_repasse_lote > 0) {
            $erro = 0;
            DB::beginTransaction();
            $arquivo = $request->no_arquivo_comprovante;
            $nome_arquivo = 'comprovante_' . strtolower(remove_caracteres($arquivo->getClientOriginalName()));

            $alienacao_valor_repasse_lote = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);

            $alienacao_valor_repasse_lote->no_arquivo_comprovante = $nome_arquivo;
            $alienacao_valor_repasse_lote->id_situacao_alienacao_pagamento = 4;
            $alienacao_valor_repasse_lote->no_local_arquivo_comprovante = 'public/alienacoes-pagamentos/' . $alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
            $alienacao_valor_repasse_lote->no_extensao_comprovante = $arquivo->getClientOriginalExtension();
            $alienacao_valor_repasse_lote->id_usuario_comprovante = Auth::User()->id_usuario;
            $alienacao_valor_repasse_lote->dt_comprovante = Carbon::now();


            if ($alienacao_valor_repasse_lote->save()) {
                $destino = '../storage/app/public/alienacoes-pagamentos/' . $alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
                if (!File::isDirectory($destino)) {
                    File::makeDirectory($destino);
                }

                if (!$request->no_arquivo_comprovante->move($destino, $nome_arquivo)) {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }


            // Tratamento do retorno
            if ($erro > 0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();
                return response()->json(array('status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'O comprovante foi salvo com sucesso.'));
            }
        }
    }

    public function aprovar_pagamento(Request $request)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $erro = 0;

            DB::beginTransaction();

            $alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
            $alienacao_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_APROVADO;

            if ($alienacao_pagamento->save()) {
                if (count($alienacao_pagamento->alienacoes) > 0) {
                    $erro_loop = false;
                    foreach ($alienacao_pagamento->alienacoes as $alienacao) {
                        switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
                            case $this::ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTO:
                                $nova_situacao = $this::ID_SITUACAO_ALIENACAO_EMPROCESSAMENTO;
                                break;
                            case $this::ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTOFINALIZ:
                                $nova_situacao = $this::ID_SITUACAO_ALIENACAO_FINALIZADO;
                                break;
                            default:
                                $nova_situacao = $alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto;
                                break;
                        }
                        $pedido = new pedido();
                        if (!$pedido->where('id_pedido', $alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $nova_situacao])) {
                            $erro_loop = true;
                        }
                        $alienacao_valor = new alienacao_valor();
                        if (!$alienacao_valor->where('id_alienacao', $alienacao->id_alienacao)->update(['id_alienacao_pagamento' => $alienacao_pagamento->id_alienacao_pagamento])) {
                            $erro_loop = true;
                        }
                    }
                    if ($erro_loop) {
                        $erro = 2;
                    }
                } else {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }

            // Tratamento do retorno
            if ($erro > 0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();
                return response()->json(array('status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'O pagamento foi aprovado com sucesso.'));
            }
        }
    }

    public function reprovar_pagamento(Request $request)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $erro = 0;

            DB::beginTransaction();

            $alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
            $alienacao_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO;

            if (!$alienacao_pagamento->save()) {
                $erro = 1;
            }

            // Tratamento do retorno
            if ($erro > 0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();
                return response()->json(array('status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'O pagamento foi reprovado com sucesso.'));
            }
        }
    }

    public function comprovante(Request $request, alienacao_pagamento $alienacao_pagamento)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

            if ($alienacao_pagamento) {
                if ($alienacao_pagamento->no_arquivo_comprovante != '') {
                    $url_download = URL::to('/arquivos/download/alienacao-pagamento/' . $alienacao_pagamento->id_alienacao_pagamento);

                    return response()->json(array('status' => 'sucesso',
                        'view' => view('servicos.alienacao-pagamento-emolumento.serventia-alienacao-pagamento-comprovante', compact('alienacao_pagamento'))->render(),
                        'download' => 'true',
                        'url_download' => $url_download));
                } else {
                    return response()->json(array('status' => 'erro',
                        'recarrega' => 'false',
                        'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                }
            } else {
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
            }
        }
    }

    public function arquivo_comprovante(Request $request, alienacao_pagamento $alienacao_pagamento)
    {
        if ($request->id_alienacao_pagamento > 0) {
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

            $arquivo = Storage::get('/public/' . $alienacao_pagamento->no_local_arquivo_comprovante . '/' . $alienacao_pagamento->no_arquivo_comprovante);

            $mimes = array('pdf' => 'application/pdf',
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'bmp' => 'image/bmp',
                'gif' => 'image/gif');

            return response($arquivo, 200)->header('Content-Type', $mimes[$alienacao_pagamento->no_extensao_comprovante])
                ->header('Content-Disposition', 'inline; filename="' . $alienacao_pagamento->no_arquivo_comprovante . '"');
        }
    }
    public function download_comprovante(Request $request, alienacao_pagamento $alienacao_pagamento) {
        if ($request->id_alienacao_pagamento>0) {
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

            $no_arquivo_comprovante = $alienacao_pagamento->no_arquivo_comprovante;

            $arquivo = Storage::get('/public/'.$alienacao_pagamento->no_local_arquivo_comprovante.'/'.$no_arquivo_comprovante);
            return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo_comprovante.'"');
        }
    }

    public function getAlienacoesCaixa($paginacao = false)
    {
        $alienacao_valor_repasse_lote = new alienacao_valor_repasse_lote();
        $alienacao_valor_repasse_lote = $alienacao_valor_repasse_lote->select('alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote',
                                            'alienacao_valor_repasse_lote.numero_lote',
                                            'alienacao_valor_repasse_lote.protocolo_pedido',
                                            'alienacao_valor_repasse_lote.nu_quantidade_lote',
                                            'alienacao_valor_repasse_lote.va_repasse_lote',
                                            'alienacao_valor_repasse_lote.dt_repasse_lote',
                                            'alienacao_valor_repasse_lote.dt_comprovante',
                                            'alienacao_valor_repasse_lote.dt_cadastro',
                                            'alienacao_valor_repasse_lote.no_arquivo_comprovante',
                                            'usuario.no_usuario'
                                        )
                                        ->join('usuario','usuario.id_usuario','=','alienacao_valor_repasse_lote.id_usuario_cad')
                                        ->join('alienacao_valor_repasse','alienacao_valor_repasse.id_alienacao_valor_repasse_lote','=','alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote')
                                        ->join('alienacao_valor','alienacao_valor.id_alienacao_valor','=','alienacao_valor_repasse.id_alienacao_valor')
                                        ->where('alienacao_valor.id_produto_item','<>',DB::Raw('28'))
                                        ->orderby('alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote', 'desc')
                                        ->groupBy(  'alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote',
                                            'alienacao_valor_repasse_lote.numero_lote',
                                            'alienacao_valor_repasse_lote.protocolo_pedido',
                                            'alienacao_valor_repasse_lote.nu_quantidade_lote',
                                            'alienacao_valor_repasse_lote.va_repasse_lote',
                                            'alienacao_valor_repasse_lote.dt_repasse_lote',
                                            'alienacao_valor_repasse_lote.dt_comprovante',
                                            'alienacao_valor_repasse_lote.dt_cadastro',
                                            'alienacao_valor_repasse_lote.no_arquivo_comprovante',
                                            'usuario.no_usuario'
                                        );
        if($paginacao == false){
            $alienacao_valor_repasse_lote = $alienacao_valor_repasse_lote->get();
        }


        return $alienacao_valor_repasse_lote;
    }

    public function imprimir_recibo(Request $request)
    {
        /*$protocolo = $request->protocolo;*/
        $id_alienacao_valor_repasse_lote = $request->id_alienacao_valor_repasse_lote;

        return view('servicos.alienacao-pagamento-emolumento.imprimir-recibo-pagamento', compact('id_alienacao_valor_repasse_lote'));
    }

    public function render_recibo(Request $request, alienacao_valor_repasse $alienacao_valor_repasse)
    {
        /*$protocolo = $request->protocolo;*/
        $id_alienacao_valor_repasse_lote = $request->id_alienacao_valor_repasse_lote;

        $alienacao_valor_repasses = $alienacao_valor_repasse->select(
                                                                        'avrl.protocolo_pedido',
                                                                        'avrl.dt_repasse_lote',
                                                                        'avrl.va_repasse_lote',
                                                                        'avrl.nu_quantidade_lote',
                                                                        'avrl.numero_lote',
                                                                        'u.no_usuario',
                                                                        DB::raw('count(distinct (av.id_alienacao, s.id_serventia)) as total'),
                                                                        DB::raw('sum(alienacao_valor_repasse.va_repasse) as valor'),
                                                                        'alienacao_valor_repasse.dt_repasse',
                                                                        's.no_serventia',
                                                                        's.id_serventia',
                                                                        'c.no_cidade',
                                                                        'b.codigo_banco',
                                                                        'b.no_banco',
                                                                        'bp.nu_agencia',
                                                                        'bp.nu_dv_agencia',
                                                                        'bp.nu_conta',
                                                                        'bp.nu_dv_conta',
                                                                        'bp.tipo_conta',
                                                                        'bp.nu_cpf_cnpj_conta',
                                                                        'bp.in_tipo_pessoa_conta'
                                                                    )
                                                                    ->join('alienacao_valor as av','av.id_alienacao_valor','=','alienacao_valor_repasse.id_alienacao_valor')
                                                                    ->join('alienacao_valor_repasse_lote as avrl','avrl.id_alienacao_valor_repasse_lote','=','alienacao_valor_repasse.id_alienacao_valor_repasse_lote')
                                                                    ->join('usuario as u','u.id_usuario','=','avrl.id_usuario_repasse')
                                                                    ->join('alienacao_pedido as ap','ap.id_alienacao','=','av.id_alienacao')
                                                                    ->join('pedido_pessoa as pp','pp.id_pedido','=','ap.id_pedido')
                                                                    ->join('pessoa as ps','ps.id_pessoa','=','pp.id_pessoa')
                                                                    ->join('serventia as s','s.id_pessoa','=','ps.id_pessoa')
                                                                    ->join('pessoa_endereco as pe','pe.id_pessoa','=','s.id_pessoa')
                                                                    ->join('endereco as e','e.id_endereco','=','pe.id_endereco')
                                                                    ->join('cidade as c','c.id_cidade','=','e.id_cidade')
                                                                    ->leftjoin('banco_pessoa as bp','bp.id_pessoa','=','s.id_pessoa')
                                                                    ->leftjoin('banco as b','b.id_banco','=','bp.id_banco')
                                                                    ->where('alienacao_valor_repasse.id_repasse','=', $this::ID_REPASSE_CAIXA)
                                                                    ->where('av.id_produto_item','<>', $this::ID_PRODUTO_ITEM)
                                                                    ->where('avrl.numero_lote','=', $id_alienacao_valor_repasse_lote)
                                                                    ->groupBy(
                                                                                'avrl.protocolo_pedido',
                                                                                'avrl.dt_repasse_lote',
                                                                                'avrl.va_repasse_lote',
                                                                                'avrl.nu_quantidade_lote',
                                                                                'avrl.numero_lote',
                                                                                'u.no_usuario',
                                                                                'alienacao_valor_repasse.dt_repasse',
                                                                                's.no_serventia',
                                                                                's.id_serventia',
                                                                                'c.no_cidade',
                                                                                'b.codigo_banco',
                                                                                'b.no_banco',
                                                                                'bp.nu_agencia',
                                                                                'bp.nu_dv_agencia',
                                                                                'bp.nu_conta',
                                                                                'bp.nu_dv_conta',
                                                                                'bp.tipo_conta',
                                                                                'bp.nu_cpf_cnpj_conta',
                                                                                'bp.in_tipo_pessoa_conta'
                                                                            )
                                                                    ->orderBy('c.no_cidade', 'asc')
                                                                    ->get();


        if (count($alienacao_valor_repasses)>0) {
            $titulo = 'Recibo de pagemnto';
            $pdf = PDF::loadView('pdf.alienacao-pagamento-emolumento-recibo', compact('alienacao_valor_repasses', 'titulo'));

            return $pdf->stream();
        }
    }

    public function gerar_relatorio_pagamento(Request $request, alienacao_valor_repasse $alienacao_valor_repasse, relatorio $relatorio)
    {
        $model_relatorio = $relatorio;

        $operador = ($request->tipo == "servicos") ? "=" : "<>";

        $subQuery = $alienacao_valor_repasse->from("alienacao_valor_repasse as avr")
            ->select(
                "p.protocolo_pedido",
                "a.numero_contrato",
                DB::raw("f_lista_devedor_alienacao(a.id_alienacao, 'no_devedor', ' / ') as no_devedor"),
                DB::raw("sum(av.va_total) as va_tx_servico_em_aberto")
            )
            ->join("alienacao_valor as av", "av.id_alienacao_valor", "=", "avr.id_alienacao_valor")
            ->join("alienacao_valor_repasse_lote as avrl", "avrl.id_alienacao_valor_repasse_lote", "=", "avr.id_alienacao_valor_repasse_lote")
            ->join("alienacao_pedido as ap", "ap.id_alienacao", "=", "av.id_alienacao")
            ->join("alienacao as a", "a.id_alienacao", "=", "ap.id_alienacao")
            ->join("pedido as p", "p.id_pedido", "=", "ap.id_pedido")
            ->where("avr.id_repasse", "=", $this::ID_REPASSE_CAIXA)
            ->where("av.id_produto_item", $operador, $this::ID_PRODUTO_ITEM)
            ->where("avrl.id_alienacao_valor_repasse_lote", "=", $request->id_alienacao_valor_repasse_lote)
            ->groupBy("p.protocolo_pedido", "a.numero_contrato", "a.id_alienacao");

        $tabela = $alienacao_valor_repasse->from(DB::raw("({$subQuery->toSql()}) as publico_lista"))
            ->mergeBindings($subQuery->getQuery())
            ->select(
                DB::raw('2629 as sr'),
                DB::raw('\'\' as agencia'),
                DB::raw('\'\' as nss'),
                'publico_lista.protocolo_pedido',
                DB::raw('1 as seq_servico'),
                'publico_lista.numero_contrato',
                'publico_lista.no_devedor',
                DB::raw('REPLACE(CAST(publico_lista.va_tx_servico_em_aberto as text), \'.\',\',\') as va_tx_servico_em_aberto'),
                DB::raw('1 as td'),
                DB::raw("'NOTIFICACAO / INTIMACAO' as descricao")
            )
            ->orderBy('publico_lista.numero_contrato')
            ->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('Relatório_de_pagamento_' . $time, function ($excel) use ($tabela, $model_relatorio) {

            $excel->sheet('alienacoes', function ($sheet) use ($tabela, $model_relatorio) {

                $sheet->loadView('xls.relatorio-pagamento-caixa', ['tabela' => $tabela, 'model_relatorio' => $model_relatorio]);
                $sheet->setColumnFormat(['A' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'I' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1]);
                $sheet->setAutoSize(true);

                $sheet->getStyle('B3:M3')->applyFromArray(['fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => ['rgb' => '87CEFA']]]);
                $sheet->getStyle('B' . (count($tabela) + 4) . ':M' . (count($tabela) + 4))->applyFromArray(['fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => ['rgb' => '87CEFA']]]);
            });
        })->export('xls');
    }
    public function remover_comprovante(Request $request, alienacao_pagamento $alienacao_pagamento){
        $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);
        if (Storage::delete('/public/'.$alienacao_pagamento->no_local_arquivo_comprovante.'/'.$alienacao_pagamento->no_arquivo_comprovante)) {
            $alienacao_pagamento->no_arquivo_comprovante = '';
            $alienacao_pagamento->no_local_arquivo_comprovante= '';
            $alienacao_pagamento->no_extensao_comprovante = '';
            if($alienacao_pagamento->save()){
                return response()->json(array('status' => 'sucesso',
                    'msg'=> 'O arquivo foi removido com sucesso',
                    'arquivo'=> $alienacao_pagamento));
            } else {
                return response()->json(array('status'=> 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Não foi pssivel remover o arquivo'));
            }
        } else {
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Arquivo não encotrado.'));
        }
    }
    public function novo_arquivo_comprovante(Request $request) {

        return view('servicos.alienacao-pagamento-emolumento.geral-alienacao-pagamento-novo-arquivo-comprovante',compact('request'));
    }
    public function alterar_comprovante(Request $request, alienacao_pagamento $alienacao_pagamento){
        if ($request->id_alienacao_pagamento > 0){
            $erro = 0;

            DB::beginTransaction();

            $nome_arquivo = remove_caracteres($request->no_arquivo->getClientOriginalName());

            $alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
            $alienacao_pagamento->no_arquivo_comprovante = $nome_arquivo;
            $alienacao_pagamento->no_local_arquivo_comprovante = 'alienacoes-pagamentos/' . $alienacao_pagamento->id_alienacao_pagamento;
            $alienacao_pagamento->no_extensao_comprovante = $request->no_arquivo->getClientOriginalExtension();


            if ($alienacao_pagamento->save()) {
                $destino = '../storage/app/public/alienacoes-pagamentos/' . $alienacao_pagamento->id_alienacao_pagamento;
                if (!File::isDirectory($destino)) {
                    File::makeDirectory($destino);
                }

                if (!$request->no_arquivo->move($destino, $nome_arquivo)) {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }

            if ($erro>0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();
                return response()->json(array(
                    'token' => $request->token,
                    'id_alienacao_pagamento' => $alienacao_pagamento->id_alienacao_pagamento,
                    'no_arquivo_comprovante' => $nome_arquivo));
            }
        }
    }
}
