<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;
use DB;
use XMLReader;
use DOMDocument;

use App\matricula;
use App\proprietario;

class XMLController extends Controller {

	public function carregar_matriculas(matricula $matricula, proprietario $proprietario) {
		list($useg, $seg) = explode(' ', microtime());
		$tempo_inicial = (float)$seg + (float)$useg;

		$pasta = '../downloads/';
		
		// Descobrir o primeiro arquivo
		$arquivos = scandir($pasta);
		$arquivo = '';
		foreach ($arquivos as $nome_arquivo) {
			$arquivo_atual = $pasta.$nome_arquivo;
			if (is_file($arquivo_atual) and filesize($arquivo_atual)>0){
				copy($arquivo_atual, $pasta.'em_processamento/'.$nome_arquivo);
				unlink($arquivo_atual);
				$arquivo = $pasta.'em_processamento/'.$nome_arquivo;
				break;
			}
		}

		$arquivo_stream = file_get_contents($arquivo);
		file_put_contents($arquivo, $this->corrigir_xml($arquivo_stream));

		// Leitura do arquivo e inserção dos dados
		if (strlen($arquivo)>0) {
			$xml = new XMLReader;
			$xml->open($arquivo);
			
			$DOM = new DOMDocument;
			
			$total_matriculas = 0;
			$total_existentes = 0;
			$total_proprietarios = 0;
			$total_erro[1] = 0;
			$total_erro[2] = 0;
			$total_erro[3] = 0;
			$total_erro[4] = 0;
			$log_registro = '';
			$log_erro = '';
			
			while ($xml->read() && $xml->name!=='INDIVIDUO');
			while ($xml->name === 'INDIVIDUO') {
				$individuo = simplexml_import_dom($DOM->importNode($xml->expand(), true));
				
				$nu_cpf_cnpj = preg_replace('#[^0-9]#', '', $individuo->CNPJCPF);
				if (strlen($nu_cpf_cnpj)>0 and strlen($individuo->MATRICULA)>0) {
					$busca_matricula = $matricula->select('id_matricula')
												 ->where('id_serventia',4)
												 ->where('codigo_matricula',$individuo->MATRICULA)
												 ->first();
												 
					if ($busca_matricula) {
						$id_matricula = $busca_matricula->id_matricula;
						$log_registro .= "[".date('d/m/Y H:i:s')."] A matrícula '".$individuo->MATRICULA."' já foi inserida com o ID '".$id_matricula."'.".PHP_EOL;
						$total_existentes++;
					} else {
						$nova_matricula = new matricula();
						$nova_matricula->id_serventia = 4;
						$nova_matricula->codigo_matricula = $individuo->MATRICULA;
						$nova_matricula->id_usuario_cad = 1;
						
						if ($nova_matricula->save()) {
							$id_matricula = $nova_matricula->id_matricula;
							$log_registro .= "[".date('d/m/Y H:i:s')."] A matrícula '".$individuo->MATRICULA."' foi inserida com o ID '".$id_matricula."'.".PHP_EOL;
							$total_matriculas++;
						} else {
							$id_matricula = 0;
							$log_erro .= "[".date('d/m/Y H:i:s')."][ERRO_01] A matrícula '".$individuo->NOME."' não foi inserida (SQL).".PHP_EOL;
							$total_erro[1]++;
						}
					}
					
					if ($id_matricula>0) {
						$busca_proprietario = $proprietario->where('id_matricula',$id_matricula)
														   ->where('nu_cpf_cnpj',$nu_cpf_cnpj)
														   ->count();
						
						if ($busca_proprietario==0) {
							if (strlen($nu_cpf_cnpj)==11) {
								$tp_pessoa = 'F';
							} elseif (strlen($nu_cpf_cnpj)==14) {
								$tp_pessoa = 'J';
							} else {
								$tp_pessoa = NULL;
							}
							
							$novo_proprietario = new proprietario();
							$novo_proprietario->id_matricula = $id_matricula;
							$novo_proprietario->no_proprietario = $individuo->NOME;
							$novo_proprietario->tp_pessoa = $tp_pessoa;
							$novo_proprietario->nu_cpf_cnpj = $nu_cpf_cnpj;
							$novo_proprietario->id_usuario_cad = 1;
						
							if ($novo_proprietario->save()) {
								$log_registro .= "[".date('d/m/Y H:i:s')."] O proprietário '".$individuo->NOME."' foi inserido com o ID '".$novo_proprietario->id_proprietario."'.".PHP_EOL;
								$total_proprietarios++;
							} else {
								$log_erro .= "[".date('d/m/Y H:i:s')."][ERRO_02] O proprietário '".$individuo->NOME."' da matricula '".$id_matricula."' não foi inserido (SQL).".PHP_EOL;
								$total_erro[2]++;
							}
						} else {
							$log_erro .= "[".date('d/m/Y H:i:s')."][ERRO_03] O proprietário '".$individuo->NOME." (".$nu_cpf_cnpj.")' da matricula '".$individuo->MATRICULA." (".$id_matricula.")' já existe.".PHP_EOL;
							$total_erro[3]++;
						}
					}						
				} else {
					$log_erro .= "[".date('d/m/Y H:i:s')."][ERRO_04] O CNPJ/CPF ou matrícula do individuo '".$individuo->NOME."' está em branco.".PHP_EOL;
					$total_erro[4]++;
				}
				$xml->next('INDIVIDUO');
			}
			
			// Logs
			$arq_log_registro = fopen($pasta.'logs/log_'.$nome_arquivo.'.txt','a+b');
			fwrite($arq_log_registro,$log_registro);
			fclose($arq_log_registro);
	
			$arq_log_erro = fopen($pasta.'logs/erros_'.$nome_arquivo.'.txt','a+b');
			fwrite($arq_log_erro,$log_erro);
			fclose($arq_log_erro);

			copy($arquivo, $pasta.'processados/'.$nome_arquivo);
			unlink($arquivo);
			
			list($useg, $seg) = explode(' ', microtime());
			$tempo_final = (float) $seg + (float) $useg;
			$tempo_total = round($tempo_final - $tempo_inicial, 0);
			
			$log_geral = '['.date('d/m/Y H:i:s').'] O arquivo "'.$arquivo.'" foi processado em '.$tempo_total.' segundos [Matrículas: '.$total_matriculas.'] [Proprietários: '.$total_proprietarios.'] [Matrículas existentes: '.$total_existentes.'] [ERRO_01: '.$total_erro[1].'] [ERRO_02: '.$total_erro[2].'] [ERRO_03: '.$total_erro[3].'] [ERRO_04: '.$total_erro[4].'].'.PHP_EOL;
			$arq_log_geral = fopen($pasta.'logs/log_geral.txt','a+b');
			fwrite($arq_log_geral,$log_geral);
			fclose($arq_log_geral);

			echo $log_geral;
		}
	}
	function corrigir_xml($xml) {
	    $res = "";
	    $atual;
	    if (empty($xml)) {
	        return $res;
	    }

	    $tam = strlen($xml);
	    for ($i=0; $i < $tam; $i++) {
	    	$atual = ord($xml{$i});
	        if ( ($atual == 34) || ($atual == 38) || ($atual == 0x9) || ($atual == 0xA) || ($atual == 0xD) || (($atual >= 0x28) && ($atual <= 0xD7FF)) || (($atual >= 0xE000) && ($atual <= 0xFFFD)) || (($atual >= 0x10000) && ($atual <= 0x10FFFF)) ) {
	            $res .= chr($atual);
	        } else {
	            $res .= " ";
	        }
	    }
	    return $res;
	}
}