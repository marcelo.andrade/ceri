<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Cielo\API30\Merchant;
use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Ecommerce\Sale;
use Cielo\API30\Ecommerce\CieloEcommerce;
use Cielo\API30\Ecommerce\Payment;
use Cielo\API30\Ecommerce\CreditCard;

use Carbon\Carbon;
use App\forma_pagamento;
use App\convenio_cartao;
use App\compra_credito;
use App\credito_entrada_saida;

class CompraCreditoController extends Controller
{
    const ID_CONVENIO_CARTAO = 1;
    const ID_SITUACAO_CREDITO_EM_APROVACAO = 2;
    const ID_SITUACAO_CREDITO_EM_ANALISE = 3;
    const ID_SITUACAO_CREDITO_CANCELADO = 4;
    const ID_SITUACAO_CREDITO_EM_CANCELAMENTO = 5;
    const ID_SITUACAO_CREDITO_APROVADO = 6;
    const ID_FORMA_PAGAMENTO_CREDITO = 1;
    const ID_FORMA_PAGAMENTO_DEBITO = 2;
    const ID_TIPO_CREDITO = 1;

    /**
     * CompraCreditoController constructor.
     */
    public function __construct()
    {
        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');
            $this->usuario = Auth::User();

            if(Session::has('troca_pessoa')) {
                $this->usuario = $pessoa_ativa->pessoa->getUsuarioRelacionado()->usuario;
            }

            $this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
            $this->id_pessoa = $pessoa_ativa->id_pessoa;
        } else {
            $this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
            $this->id_pessoa = Auth::User()->id_pessoa;
        }

        /**
         * define o embiente que a aplicações está rodando.
         * local (desenvolvimento/homologação) ou produtction (produção)
         */
        if (env('APP_ENV') == 'production') {
            $this->environment = Environment::production();
            $this->merchant_id = "5311caf5-1e1a-4beb-a806-08f3cf5bf80d";
            $this->merchant_key = "JZbeKpqRxGflmPIQvaDQkl7QUMHeFISFPTBvVM8z";
        } else {
            $this->environment = Environment::sandbox();
            $this->merchant_id = "79e3351d-0af5-4a69-b29d-761e6941d6b2";
            $this->merchant_key = "NULFUZHWWKZWQVWECOGKAYMXWLZKHQMKYICTZFVS";
        }
    }

    public function index()
    {
        $todos_credito = \App\compra_credito::where('id_usuario', $this->usuario->id_usuario)
                                            ->orderBy('id_compra_credito', 'desc')
                                            ->paginate(20, ['*'],'credito-pag');

        return view('creditos.creditos', compact('todos_credito'));
    }

    public function nova_compra()
    {
        $todos_estados = \App\estado::orderBy('no_estado')->get();
        return view('creditos.creditos-compra', compact('todos_estados'));
    }

    /**
     * @param Request $request
     * @param convenio_cartao $convenio_cartao
     * @return \Illuminate\Http\JsonResponse
     * @throws \Cielo\API30\Ecommerce\Request\CieloRequestException
     */
    public function compra_credito(Request $request)
    {
        $erro = 0;

        DB::beginTransaction();

        /**
         * Dados de requisição do formulário
         */
        $cardNumber = preg_replace('#[^0-9]#', '', $request->nu_cartao_credito);
        $amount = intval(preg_replace('#[^0-9]#', '', $request->vl_compra));
        $vl_compra = converte_float($request->vl_compra);
        $securityCode = $request->nu_seguranca;

        /**
         * Insere os dados da compra
         */
        $compra_credito = new compra_credito();
        $compra_credito->id_forma_pagamento = $this::ID_FORMA_PAGAMENTO_CREDITO;
        $compra_credito->id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_APROVACAO;
        $compra_credito->id_usuario = Auth::User()->id_usuario;
        $compra_credito->id_usuario_cad = Auth::User()->id_usuario;
        $compra_credito->va_credito = $vl_compra;
        $compra_credito->dt_compra = Carbon::now();
        $compra_credito->dt_cadastro = Carbon::now();
        if (!$compra_credito->save()) {
            $erro = 10001;
        }

        $nu_pedido_convenio = (int) Carbon::now()->format('Y') . Auth::User()->id_unidade_gestora . $compra_credito->id_compra_credito;

        switch ($request->id_bandeira) {
            case 'visa':
                $creditCard = CreditCard::VISA;
                break;
            case 'mastercard':
                $creditCard = CreditCard::MASTERCARD;
                break;
            case 'diners':
                $creditCard = CreditCard::DINERS;
                break;
            case 'amex':
                $creditCard = CreditCard::AMEX;
                break;
            case 'elo':
                $creditCard = CreditCard::ELO;
                break;
            case 'aura':
                $creditCard = CreditCard::AURA;
                break;
            case 'jcb':
                $creditCard = CreditCard::JCB;
                break;
        }

        // Configure seu merchant
        $merchant = new Merchant($this->merchant_id, $this->merchant_key);

        // Crie uma instância de Sale informando o ID do pedido na loja
        $sale = new Sale($nu_pedido_convenio);

        // Crie uma instância de Customer informando o nome do cliente
        $customer = $sale->customer($request->titular_cartao);

        // Crie uma instância de Payment informando o valor do pagamento
        $payment = $sale->payment($amount);

        // Crie uma instância de CreditCard utilizando os dados de teste
        // esses dados estão disponíveis no manual de integração
        $payment->setType(Payment::PAYMENTTYPE_CREDITCARD)
                ->creditCard($securityCode, $creditCard)
                ->setExpirationDate($request->validade_cartao)
                ->setCardNumber($cardNumber)
                ->setHolder($request->titular_cartao);

        try {
            // Configure o SDK com seu merchant e o ambiente apropriado para criar a venda
            $sale = (new CieloEcommerce($merchant, $this->environment))->createSale($sale);
            $paymentId = $sale->getPayment()->getPaymentId();

            /**
             * Adiciona a situação da compra de crédito a partir do código de rertono da CIELO,
             * de acordo com os código abaixo:
             * 1 ou 2 - autorizado;
             * 3, 10, 11 ou 13 - não autorizado;
             * 0 ou 12 * OBS: 20 (somente para cartões de crédito) - aguardando autorização da instituição financeira;
             */
            switch ($sale->getPayment()->getStatus()) {
                case 1:
                case 2:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_APROVADO;
                    break;
                case 3:
                case 10:
                case 11:
                case 13:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                    break;
                case 0:
                case 12:
                case 20:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_ANALISE;
                    break;
                default:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                    break;
            }

            /**
             * Capitura a mensagem de retorno da transação.
             */
            //$return_messege = $sale->getPayment()->getReturnMessage();
            $message = "Transação realizada com sucesso.";

            /**
             * Adiciona informações da compra inserindo os dados de retorno da CIELO na tabela compra_credito.
             * Quando o codigo de retorno for 1 ou 2, o credito é adicionado inserindo as informações
             * na tabela credito_entrada_saida
             */
            if (in_array($sale->getPayment()->getStatus(), [1,2])) {
                $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                $compra_credito->id_situacao_credito = $id_situacao_credito;
                $compra_credito->id_usuario_lib = Auth::User()->id_usuario;
                $compra_credito->dt_liberacao = Carbon::now();
                $compra_credito->nu_pedido_convenio = $nu_pedido_convenio;
                $compra_credito->id_pagamento_cielo = $paymentId;
                $compra_credito->co_retorno_venda_cielo = $sale->getPayment()->getReturnCode();
                if ($compra_credito->save()) {
                    $credito_entrada_saida = new credito_entrada_saida();
                    $credito_entrada_saida->id_compra_credito = $compra_credito->id_compra_credito;
                    $credito_entrada_saida->id_tipo_credito = $this::ID_TIPO_CREDITO;
                    $credito_entrada_saida->tp_movimento = 'E';
                    $credito_entrada_saida->va_movimento = $vl_compra;
                    $credito_entrada_saida->id_usuario = Auth::User()->id_usuario;
                    $credito_entrada_saida->dt_cadastro = Carbon::now();
                    $credito_entrada_saida->dt_movimento = Carbon::now();
                    if (!$credito_entrada_saida->save()) {
                        $erro = 10004;
                    }
                } else {
                    $erro = 10003;
                }
            } else {
                $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                $compra_credito->id_situacao_credito = $id_situacao_credito;
                $compra_credito->nu_pedido_convenio = $nu_pedido_convenio;
                $compra_credito->id_pagamento_cielo = $paymentId;
                $compra_credito->co_retorno_venda_cielo = $sale->getPayment()->getReturnCode();
                if (!$compra_credito->save()) {
                    $erro = 10002;
                }

                switch ($sale->getPayment()->getReturnCode()) {
                    case "05":
                        $erro = 30002;
                        $message = "Transação não Autorizada.";
                        break;
                    case "57":
                        $erro = 30003;
                        $message = "Cartão expirado.";
                        break;
                    case "70":
                        $erro = 30004;
                        $message = "Problemas com o Cartão de Crédito.";
                        break;
                    case "77":
                        $erro = 30005;
                        $message = "Cartão cancelado.";
                        break;
                    case "78":
                        $erro = 30006;
                        $message = "Cartão bloqueado.";
                        break;
                    case "99":
                        $erro = 30007;
                        $message = "Tempo esgotado, não foi possível completar sua transação.";
                        break;
                    default:
                        $erro = 30008;
                        $message = "Transação cancelada por motivos desconhecidos. entre em contato com o suporte";
                        break;
                }
            }

            if ($erro>0) {
                switch ($erro) {
                    case 10001:
                        DB::rollback();

                        // E também podemos fazer seu cancelamento, se for o caso
                        $sale = (new CieloEcommerce($merchant, $this->environment))->cancelSale($paymentId, $amount);

                        return response()->json([
                            'status' => 'erro',
                            'msg' => 'Não foi possível inserir os dados da sua compra. A transação foi cancelada.',
                            'recarrega' => false
                        ]);
                        break;

                    case 10002:
                    case 10003:
                        DB::commit();

                        return response()->json([
                            'status' => 'erro',
                            'msg' => 'Não foi possível atualizar os dados da sua compra. O seu saldo será inserido posteriormente.',
                            'recarrega' => false
                        ]);
                        break;
                    case 10004:
                        DB::commit();

                        return response()->json([
                            'status' => 'erro',
                            'msg' => 'Não foi possível inserir o saldo na sua conta. O seu saldo será inserido posteriormente.',
                            'recarrega' => false
                        ]);
                        break;
                    default:
                        DB::commit();

                        return response()->json([
                            'status' => 'erro',
                            'msg' => $message,
                            'recarrega' => false
                        ]);
                        break;
                }
            } else {
                DB::commit();

                return response()->json([
                    'status' => 'sucesso',
                    'msg' => $message,
                    'id_compra_credito' => $compra_credito->id_compra_credito,
                    'recarrega' => false
                ]);
            }

        // Em caso de erros de integração, podemos tratar o erro aqui.
        // os códigos de erro estão todos disponíveis no manual de integração.
        } catch (CieloRequestException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'erro',
                'msg' => $e->getCieloError()->getMessage(),
                'recarrega' => false
            ]);
        }
    }

    /**
     * @param Request $request
     * @param convenio_cartao $convenio_cartao
     * @throws \Cielo\API30\Ecommerce\Request\CieloRequestException
     */
    public function compra_debito(Request $request, forma_pagamento $forma_pagamento)
    {
        $forma_pagamento = $forma_pagamento->find($this::ID_FORMA_PAGAMENTO_DEBITO);
        $erro = 0;

        DB::beginTransaction();

        /**
         * Dados da requisição do formulário
         */
        $cardNumber = preg_replace('#[^0-9]#', '', $request->nu_cartao_credito);
        $amount = intval(preg_replace('#[^0-9]#', '', $request->vl_compra));
        $vl_compra = converte_float($request->vl_compra);
        $securityCode = $request->nu_seguranca;

        switch ($request->id_bandeira) {
            case 'visa':
                $creditCard = CreditCard::VISA;
                break;
            case 'mastercard':
                $creditCard = CreditCard::MASTERCARD;
                break;
            default:
                $erro = 20001;
                return response()->json(['status' => 'erro',
                                         'msg' => 'Bandeira não disponível para função débito. Erro nº ' . $erro,
                                         'recarrega' => false
                                        ]);
                break;
        }

        /**
         * Insere os dados da compra
         */
        $compra_credito = new compra_credito();
        $compra_credito->id_forma_pagamento = $this::ID_FORMA_PAGAMENTO_DEBITO;
        $compra_credito->id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_APROVACAO;
        $compra_credito->id_usuario = Auth::User()->id_usuario;
        $compra_credito->id_usuario_cad = Auth::User()->id_usuario;
        $compra_credito->va_credito = $vl_compra;
        $compra_credito->dt_compra = Carbon::now();
        $compra_credito->dt_cadastro = Carbon::now();
        if (!$compra_credito->save()) {
            $erro = 10001;
        }

        $nu_pedido_convenio = Carbon::now()->format('Y') . Auth::User()->id_unidade_gestora . $compra_credito->id_compra_credito;
        Session::put('id_compra_credito',$compra_credito->id_compra_credito);

        // Configure seu merchant
        $merchant = new Merchant($this->merchant_id, $this->merchant_key);

        // Crie uma instância de Sale informando o ID do pedido na loja
        $sale = new Sale($nu_pedido_convenio);

        // Crie uma instância de Customer informando o nome do cliente
        $customer = $sale->customer($request->titular_cartao);

        // Crie uma instância de Payment informando o valor do pagamento
        $payment = $sale->payment($amount);

        // Defina a URL de retorno para que o cliente possa voltar para a loja
        // após a autenticação do cartão
        $payment->setReturnUrl($forma_pagamento->url_retorno);

        // Crie uma instância de Debit Card utilizando os dados de teste
        // esses dados estão disponíveis no manual de integração
        $payment->setAuthenticate(TRUE)
                ->setType(Payment::PAYMENTTYPE_DEBITCARD)
                ->debitCard($securityCode, $creditCard)
                ->setExpirationDate($request->validade_cartao)
                ->setCardNumber($cardNumber)
                ->setHolder($request->titular_cartao);

        // Crie o pagamento na Cielo
        try {
            // Configure o SDK com seu merchant e o ambiente apropriado para criar a venda
            $sale = (new CieloEcommerce($merchant, $this->environment))->createSale($sale);

            // Com a venda criada na Cielo, já temos o ID do pagamento, TID e demais
            // dados retornados pela Cielo
            $paymentId = $sale->getPayment()->getPaymentId();

            $nu_pedido_convenio	= (int) $nu_pedido_convenio;

            $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
            $compra_credito->nu_pedido_convenio = $nu_pedido_convenio;
            $compra_credito->id_pagamento_cielo = $paymentId;
            $compra_credito->co_retorno_venda_cielo = $sale->getPayment()->getReturnCode();
            if (!$compra_credito->save()) {
                $erro = 10001;
            }

            // Utilize a URL de autenticação para redirecionar o cliente ao ambiente
            // de autenticação do emissor do cartão
            $authenticationUrl = $sale->getPayment()->getAuthenticationUrl();

            if (!in_array($sale->getPayment()->getStatus(), [0,1,2])) {
                switch ($sale->getPayment()->getReturnCode()) {
                    case "05":
                    case "96":
                        $erro = 30002;
                        $message = "Transação não Autorizada.";
                        break;
                    case "57":
                        $erro = 30003;
                        $message = "Cartão expirado.";
                        break;
                    case "70":
                        $erro = 30004;
                        $message = "Problemas com o Cartão.";
                        break;
                    case "77":
                        $erro = 30005;
                        $message = "Cartão cancelado.";
                        break;
                    case "78":
                        $erro = 30006;
                        $message = "Cartão bloqueado.";
                        break;
                    case "99":
                        $erro = 30007;
                        $message = "Tempo esgotado, não foi possivel completar sua transação.";
                        break;
                    default:
                        $erro = 30008;
                        $message = "Transação cancelada por motivos desconhecidos. entre em contato com o suporte";
                        break;
                }
            }

            if ($erro>0) {
                switch ($erro) {
                    case 10001:
                        DB::rollback();

                        // E também podemos fazer seu cancelamento, se for o caso
                        $sale = (new CieloEcommerce($merchant, $this->environment))->cancelSale($paymentId, $amount);

                        return response()->json([
                            'status' => 'erro',
                            'msg' => 'Erro na inserção da compra, a sua compra foi cancelada.',
                            'recarrega' => false
                        ]);
                        break;
                    default:
                        DB::commit();

                        return response()->json([
                            'status' => 'erro',
                            'msg' => $message,
                            'recarrega' => false
                        ]);
                        break;
                }
            } else {
                DB::commit();
                return response()->json([
                    'status' => 'sucesso',
                    'msg' => 'Neste processo é necessário que nenhuma janela seja fechada para que a transação ocorra com suceso.<br /><br />Você deve desbloquear o gerenciador de popup para este site...<br /><br /><strong>Deseja continuar?</strong>',
                    'tipo' => 'debito',
                    'url' => $authenticationUrl,
                    'recarrega' => false
                ]);
            }
        } catch (CieloRequestException $e) {
            DB::rollback();

            // Em caso de erros de integração, podemos tratar o erro aqui.
            // os códigos de erro estão todos disponíveis no manual de integração.
            return response()->json([
                'status' => 'erro',
                'msg' => $e->getCieloError()->getMessage(),
                'recarrega' => false
            ]);
        }
    }

    public function retorno_compra_debito(Request $request, compra_credito $compra_credito)
    {
        $compra_credito = $compra_credito->where('id_pagamento_cielo',$request->PaymentId)->first();
        $erro = 0;

        DB::beginTransaction();

        try {
            // Configure seu merchant
            $merchant = new Merchant($this->merchant_id, $this->merchant_key);

            // Realiza a consulta da transação
            $sale = (new CieloEcommerce($merchant, $this->environment))->getSale($request->PaymentId);

            /**
             * Adiciona a situação da compra de crédito a partir do código de rertono da CIELO,
             * de acordo com os código abaixo:
             * 1 ou 2 - autorizado;
             * 3, 10, 11 ou 13 - não autorizado;
             * 0 ou 12 * OBS: 20 (somente para cartões de crédito) - aguardando autorização da instituição financeira;
             */
            switch ($sale->getPayment()->getStatus()) {
                case 1:
                case 2:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_APROVADO;
                    break;
                case 3:
                case 10:
                case 11:
                case 13:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                    break;
                case 0:
                case 12:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_ANALISE;
                    break;
                default:
                    $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_CANCELAMENTO;
                    break;
            }

            $message = "Transação realizada com sucesso.";

            /**
             * Adiciona informações da compra inserindo os dados de retorno da CIELO na tabela compra_credito.
             * Quando o codigo de retorno for 4 ou 6, o credito é adicionado inserindo as informações
             * na tabela credito_entrada_saida
             */
            if (in_array($sale->getPayment()->getStatus(), [1,2])) {
                $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                $compra_credito->id_situacao_credito = $id_situacao_credito;
                $compra_credito->id_usuario_lib = Auth::User()->id_usuario;
                $compra_credito->dt_liberacao = Carbon::now();
                if ($compra_credito->save()) {
                    $credito_entrada_saida = new credito_entrada_saida();
                    $credito_entrada_saida->id_compra_credito = $compra_credito->id_compra_credito;
                    $credito_entrada_saida->id_tipo_credito = $this::ID_TIPO_CREDITO;
                    $credito_entrada_saida->tp_movimento = 'E';
                    $credito_entrada_saida->va_movimento = $sale->getPayment()->getAmount() / 100;
                    $credito_entrada_saida->id_usuario = Auth::User()->id_usuario;
                    $credito_entrada_saida->dt_cadastro = Carbon::now();
                    $credito_entrada_saida->dt_movimento = Carbon::now();
                    if (!$credito_entrada_saida->save()) {
                        $erro = 10004;
                    }
                } else {
                    $erro = 10005;
                }
            } else {
                $erro = 10006;
                $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                $compra_credito->id_situacao_credito = $id_situacao_credito;
                if (!$compra_credito->save()) {
                    $erro = 10001;
                }

                switch ($sale->getPayment()->getReturnCode()) {
                    case "05":
                    case "96":
                        $erro = 30002;
                        $message = "Transação não Autorizada.";
                        break;
                    case "57":
                        $erro = 30003;
                        $message = "Cartão expirado.";
                        break;
                    case "70":
                        $erro = 30004;
                        $message = "Problemas com o Cartão de Crédito.";
                        break;
                    case "77":
                        $erro = 30005;
                        $message = "Cartão cancelado.";
                        break;
                    case "78":
                        $erro = 30006;
                        $message = "Cartão bloqueado.";
                        break;
                    case "99":
                        $erro = 30007;
                        $message = "Tempo esgotado, não foi possivel completar sua transação.";
                        break;
                    default:
                        $erro = 30008;
                        $message = "Transação cancelada por motivos desconhecidos. entre em contato com o suporte";
                        break;
                }
            }

            DB::commit();

            if ($erro>0) {
                // E também podemos fazer seu cancelamento, se for o caso
                //$sale = (new CieloEcommerce($merchant, $this->environment))->cancelSale($sale->getPayment()->getPaymentId(), $sale->getPayment()->getAmount());

                switch ($erro) {
                    case 10001:
                        $arrayErro = array('erro' => 'Não foi possível atualizar o satus da compra. O status da compra será atualizado posteriormente.', 'request' => $request->all());
                        return view('creditos.creditos-error', compact('arrayErro'));
                        break;
                    case 10004:
                        $arrayErro = array('erro' => 'Não foi possível inserir os créditos na sua conta. Os seus creditos serão adicionados posteriormente.', 'request' => $request->all());
                        return view('creditos.creditos-error', compact('arrayErro'));
                        break;
                    default:
                        $arrayErro = array('erro' => $message, 'request' => $request->all());
                        return view('creditos.creditos-error', compact('arrayErro'));
                        break;
                }
            }


            $arraySucess = array('success' => 'Parabéns, Transação realizada com sucesso', 'valor' => $compra_credito->va_credito);
            return view('creditos.creditos-success', compact('arraySucess'));

        // Em caso de erros de integração, podemos tratar o erro aqui.
        // os códigos de erro estão todos disponíveis no manual de integração.
        } catch(Exception $e){
            DB::rollback();

            $arrayErro = array('erro' => $e->getCieloError()->getMessage(), 'request' => $request->all());
            return view('creditos.creditos-error', compact('arrayErro'));
        }
    }

    public function valida_transacao_debito(compra_credito $compra_credito)
    {
        $compra_credito = $compra_credito->find(Session::get('id_compra_credito'));

        if ($compra_credito->id_situacao_credito != 2) {
            return response()->json(['total' => Auth::User()->saldo_usuario(),
                                     'id_compra_credito' => $compra_credito->id_compra_credito
                                    ]);
        }
    }

    public function cancelar_transacao(compra_credito $compra_credito)
    {
        $compra_credito = $compra_credito->find(Session::get('id_compra_credito'));

        if ($compra_credito) {
            $compra_credito->id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
            $compra_credito->id_usuario_lib =  Auth::User()->id_usuario;
            if (!$compra_credito->save()) {
                return response()->json(['staus'=>'erro', 'msg' => 'Não foi possível inserir a situação de cancelamento em sua transação.', 'recarrega' => true]);
            }

            try {
                // Configure seu merchant
                $merchant = new Merchant($this->merchant_id, $this->merchant_key);

                // Realiza a consulta da transação
                $sale = (new CieloEcommerce($merchant, $this->environment))->getSale($compra_credito->id_pagamento_cielo);

                // E também podemos fazer seu cancelamento, se for o caso
                $sale = (new CieloEcommerce($merchant, $this->environment))->cancelSale($sale->getPayment()->getPaymentId(), $sale->getPayment()->getAmount());
                Session::forget('id_compra_credito');
                return response()->json(['staus'=>'sucesso', 'msg' => 'Sua transação cancelada.', 'recarrega' => true]);

            // Em caso de erros de integração, podemos tratar o erro aqui.
            // os códigos de erro estão todos disponíveis no manual de integração.
            } catch(Exception $e){
                return response()->json(['staus'=>'erro', 'msg' => $e->getCieloError()->getMessage(), 'recarrega' => true]);
            }
        } else {
            return response()->json(['staus'=>'erro', 'msg' => 'Não foi possivel localizar sua transação.', 'recarrega' => true]);
        }
    }

    /**
     * @param compra_credito $compra_credito
     * @throws \Cielo\API30\Ecommerce\Request\CieloRequestException
     *
     * Método responsável por consultar e atualizar os status das compras que estão com id_situacao_credito
     * igual a 2 (em aprovação) para a situação de acordo com o status retornado pela CIELO.
     * Funcionalidade acionada via crontab configurado no servidor da aplicação.
     *
     * Método não será mais utilizado no controller atual. O método movido para o controller ValidaTrasacaoController que é responsavel por
     * validar e atualizar as transações de débito.
     */
    public function atualiza_retorno_debito(compra_credito $compra_credito)
    {
        $compras_credito = $compra_credito->where('id_situacao_credito', $this::ID_SITUACAO_CREDITO_EM_APROVACAO)
                                          ->whereNotNull('id_pagamento_cielo')
                                          ->get();

        // Configure seu merchant
        $merchant = new Merchant($this->merchant_id, $this->merchant_key);

        if (count($compras_credito) > 0) {
            foreach ($compras_credito as $compra_credito) {

                // Realiza a consulta da transação
                $sale = (new CieloEcommerce($merchant, $this->environment))->getSale($compra_credito->id_pagamento_cielo);

                /**
                 * Adiciona a situação da compra de crédito a partir do código de rertono da CIELO,
                 * de acordo com os código abaixo:
                 * 1 ou 2 - autorizado;
                 * 3, 10, 11 ou 13 - não autorizado;
                 * 0 ou 12 (* OBS: 20 (somente para cartões de crédito)) - aguardando autorização da instituição financeira;
                 */
                switch ($sale->getPayment()->getStatus()) {
                    case 1:
                    case 2:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_APROVADO;
                        $no_situacao_credito = 'Aprovado';
                        break;
                    case 3:
                    case 10:
                    case 11:
                    case 13:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                        $no_situacao_credito = 'Cancelado';
                        break;
                    case 0:
                    case 12:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_ANALISE;
                        $no_situacao_credito = 'Em análise';
                        break;
                    default:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_CANCELAMENTO;
                        $no_situacao_credito = 'Em cancelamento';
                        break;
                }

                /**
                 * Adiciona informações da compra inserindo os dados de retorno da CIELO na tabela compra_credito.
                 * Quando o codigo de retorno for 4 ou 6, o credito é adicionado inserindo as informações
                 * na tabela credito_entrada_saida
                 */
                if (in_array($sale->getPayment()->getStatus(), [1, 2])) {
                    $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                    $compra_credito->id_situacao_credito = $id_situacao_credito;
                    $compra_credito->id_usuario_lib = $compra_credito->id_usuario;
                    $compra_credito->dt_liberacao = Carbon::now();
                    if ($compra_credito->save()) {
                        $credito_entrada_saida = new credito_entrada_saida();
                        $credito_entrada_saida->id_compra_credito = $compra_credito->id_compra_credito;
                        $credito_entrada_saida->id_tipo_credito = $this::ID_TIPO_CREDITO;
                        $credito_entrada_saida->tp_movimento = 'E';
                        $credito_entrada_saida->va_movimento = $sale->getPayment()->getAmount() / 100;
                        $credito_entrada_saida->id_usuario = $compra_credito->id_usuario;
                        $credito_entrada_saida->dt_cadastro = Carbon::now();
                        $credito_entrada_saida->dt_movimento = Carbon::now();
                        $credito_entrada_saida->save();
                    }
                } else {
                    $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                    $compra_credito->id_situacao_credito = $id_situacao_credito;
                    $compra_credito->save();
                }

                echo $sale->getPayment()->getTid() . " - " . $sale->getPayment()->getPaymentId() . " - " .  $sale->getPayment()->getStatus() . " (" . $no_situacao_credito . ") - " . Carbon::now()->format('d/m/Y H:i') . "\r\n";
            }
        }
    }
}


