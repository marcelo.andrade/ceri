<?php
namespace App\Http\Controllers;


class IndicadorController extends Controller 
{
    public function index()
    {
        $highcharts["credits"] = array(
            "enabled" => false,
        );
        $highcharts["chart"] = array(
            "type" => "pie",
            "plotBackgroundColor" => null,
            "plotBorderWidth" => null,
            "plotShadow" => false,
        );
        $highcharts["title"] = array(
            "text" => "Browser market shares in January, 2018"
        );
        $highcharts["tooltip"] = array(
            "pointFormat" => "{series.name}: <b>{point.percentage:.1f}%</b>"
        );
        $highcharts["plotOptions"] = array(
            "pie" => array(
                "allowPointSelect" => true,
                "cursor" => "pointer",
                "dataLabels" => array(
                    "enabled" => true,
                    "format" => "<b>{point.name}</b>: {point.percentage:.1f}%",
                    "style" => array(
                        "color" => "default"
                    )
                )
            )
        );
        $highcharts["series"] = array(
            "name" => "Brands",
            "colorByPoint" => true,
            "data" => array(
                array(
                    "name" => "Chrome",
                    "y" => 61.41,
                    "sliced" => true,
                    "selected" => true
                ),
                array(
                    "name" => "Internet Explorer",
                    "y" => 11.84
                ),
                array(
                    "name" => "Firefox",
                    "y" => 10.85
                ),
                array(
                    "name" => "Edge",
                    "y" => 4.67
                ), 
                array(
                    "name" => "Safari",
                    "y" => 4.18
                ), 
                array(
                    "name" => "Sogou Explorer",
                    "y" => 1.64
                ), 
                array(
                    "name" => "Opera",
                    "y" => 1.6
                ), 
                array(
                    "name" => "QQ",
                    "y" => 1.2
                ), 
                array(
                    "name" => "Other",
                    "y" => 2.61
                ),
            )
        );
echo "<pre>"; var_dump(json_encode($highcharts["series"])); die;
        return view('indicador', compact('highcharts'));
    }
}
