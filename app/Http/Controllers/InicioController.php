<?php
namespace App\Http\Controllers;

use App\alienacao_pagamento;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\alienacao_andamento_situacao;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;
use DB;
use Session;

use App\pedido_notificacao;
use App\alienacao;
use App\alienacao_observacao;
use App\pagamento_repasse_pessoa_lote;

class InicioController extends Controller {

    const ID_ALCADA = 2;
    const ID_SITUACAO_PEDIDO = 55;
    const ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF = 2;
    const ID_REPASSE_ANOREG_SERVENTIA = 4;
    const ID_AGUARDANDO_COMPROVANTE = 1;
    const ID_AGUARDANDO_CONFIRMACAO = 2;
    const ID_REPASSE_ANOREG_TAXA_SERVICO = 3;
    const ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP = 1;


    public function __construct() {
		if (count(Auth::User()->usuario_pessoa)>0) {
			$this->pessoa_ativa = Session::get('pessoa_ativa');
            $this->usuario = Auth::User();

            if(Session::has('troca_pessoa')) {
                $this->usuario = $this->pessoa_ativa->pessoa->getUsuarioRelacionado()->usuario;
            }

			$this->id_tipo_pessoa = $this->pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $this->pessoa_ativa->id_pessoa;
        	$this->modulos = $this->pessoa_ativa->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
		}
	}

	public function index(pedido_notificacao $pedido_notificacao, alienacao_observacao $alienacao_observacao, alienacao_pagamento $alienacao_pagamento, alienacao_andamento_situacao $alienacao_andamento_situacao) {
		$notificacoes = $pedido_notificacao->select(DB::raw('count(id_pedido_notificacao) as total'),'produto.id_grupo_produto')
										   ->join('pedido','pedido_notificacao.id_pedido','=','pedido.id_pedido')
										   ->join('produto','pedido.id_produto','=','produto.id_produto')
										   ->groupBy('produto.id_grupo_produto')
										   ->where('pedido_notificacao.in_visualizada','N')
										   ->where('pedido_notificacao.id_pessoa_destino',$this->id_pessoa)
										   ->get()
										   ->keyBy('id_grupo_produto');

		$total_observacoes = $alienacao_observacao->where('alienacao_observacao.in_leitura','N')
            ->where('alienacao_observacao.id_pessoa_dest',$this->id_pessoa)
            ->count();

		if ($this->id_tipo_pessoa==2){
            $total_pagamentos_pendentes = $alienacao_pagamento->where('alienacao_pagamento.id_serventia',$this->pessoa_ativa->pessoa->serventia->id_serventia)
                                                              ->where('id_situacao_alienacao_pagamento',$this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF)
                                                              ->count();

            $total_pagamentos_servicos_pendentes = \App\pagamento_repasse_pessoa_lote::join('pagamento_repasse_lote', 'pagamento_repasse_lote.id_pagamento_repasse_lote', '=', 'pagamento_repasse_pessoa_lote.id_pagamento_repasse_lote')
                                                                                     ->where('pagamento_repasse_pessoa_lote.id_pessoa', $this->id_pessoa)
                                                                                     ->where('pagamento_repasse_lote.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                                     ->where('pagamento_repasse_pessoa_lote.id_situacao_pagamento_repasse', $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF)
                                                                                     ->count();
        }

        if($this->id_tipo_pessoa==9){
            $total_pagamentos_pendentes = $alienacao_pagamento->where('id_usuario_cad', $this->usuario->id_usuario)
                                                              ->whereIn('id_situacao_alienacao_pagamento', array($this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP, $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF))
                                                              ->count();

            $total_pagamentos_servicos_pendentes =  \App\pagamento_repasse_pessoa_lote::join('pagamento_repasse_lote', 'pagamento_repasse_lote.id_pagamento_repasse_lote', '=', 'pagamento_repasse_pessoa_lote.id_pagamento_repasse_lote')
                                                                                    ->where('pagamento_repasse_lote.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                                    ->whereIn('pagamento_repasse_pessoa_lote.id_situacao_pagamento_repasse', [$this::ID_AGUARDANDO_COMPROVANTE,$this::ID_AGUARDANDO_CONFIRMACAO])
                                                                                    ->count() ;
        }

        $class = $this;

        $alienacao = new alienacao();

        return view('inicio', compact('class','notificacoes','alienacao','total_observacoes', 'total_pagamentos_pendentes','total_pagamentos_servicos_pendentes'));
	}

	public function alertas_detalhes(Request $request) {
        if ($request->grupo!='') {
        	$class = $this;
        	$alienacao = new alienacao();

            return view('inicio-notificacoes-detalhes',compact('request','class','alienacao'));
        }
    }

}
