<?php
namespace App\Http\Controllers;


class BoletoController extends Controller
{
    const IDENTIFICACAO = "SAGRES INFORMÁTICA E COMUNICAÇÃO LTDA";
    const CPF_CNPJ = "01.732.534/0001-75";
    const ENDERECO = "SAUS QUADRA 3, BLOCO C, SALA 1007, ED. BUSINESS POINT";
    const CIDADE_UF = "BRASILIA / DF - CEP: 70.070-934";
    const CEDENTE = "SAGRES INFORMÁTICA E COMUNICAÇÃO LTDA";

    public function imprimir(Request $request)
    {
        $dado = [];

        $dado['dt_vencimento'] = '01/06/2017';
        $dado['valor_cobrado'] = 100.99;
        $dado['id_titulo'] = 002266;
        $dado['nome_sacado'] = 'JONAS BARBOSA DA SILVA';
        $dado['endereco_sacado'] = 'QUADRA 23 CONJUNTO K CASA 7';
        $dado['cidade_sacado'] = 'BRASILIA';
        $dado['uf_sacado'] = 'DF';
        $dado['cep_sacado'] = '71572311';
        $dado['bairro_sacado'] = 'PARANOÁ';

        /**
         * DADOS DO BOLETO PARA O SEU CLIENTE
         */
        $dias_de_prazo_para_pagamento = 5;
        $taxa_boleto = 0;
        $data_venc = $dado['dt_vencimento']; //date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006";
        $valor_cobrado = $dado['valor_cobrado']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
        $valor_cobrado = str_replace(",", ".", $dado['valor_cobrado']);
        $valor_boleto = number_format($valor_cobrado + $taxa_boleto, 2, ',', '');

        $dadosboleto["nosso_numero"] = $dado['id_titulo'];
        $dadosboleto["numero_documento"] = $dado['id_titulo'];    // Num do pedido ou do documento
        $dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        $dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
        $dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
        $dadosboleto["valor_boleto"] = $valor_boleto;    // Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

        // DADOS DO SEU CLIENTE
        $dadosboleto["sacado"]    = $dado['nome_sacado'];
        $dadosboleto["endereco1"] = $dado['endereco_sacado'];
        $dadosboleto["endereco2"] = $dado['cidade_sacado'] . ' - ' . $dado['uf_sacado'] . ' -  CEP: ' . DataValidator::criarMasckCEP($dado['cep_sacado']); //"Cidade - Estado -  CEP: 00000-000";

        // INFORMACOES PARA O CLIENTE
        $dadosboleto["demonstrativo1"] = "Pagamento de taxa cartorial";
        $dadosboleto["demonstrativo2"] = "Mensalidade referente a registro de imovel XXX<br>Taxa bancaria - R$ " . number_format($taxa_boleto, 2, ',', '');
        $dadosboleto["demonstrativo3"] = "";

        /**
         * INSTRUÇÕES PARA O CAIXA
         */
        $dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de 2% apos o vencimento";
        $dadosboleto["instrucoes2"] = "- Receber ate 10 dias apos o vencimento";
        $dadosboleto["instrucoes3"] = "";
        $dadosboleto["instrucoes4"] = "&nbsp; Emitido pelo sistema GRBRSagres";

        /**
         * DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
         */
        $dadosboleto["quantidade"] = "1";
        $dadosboleto["valor_unitario"] = "10";
        $dadosboleto["aceite"] = "N";
        $dadosboleto["especie"] = "R$";
        $dadosboleto["especie_doc"] = "DM";


        /**
         * DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO
         */
        // DADOS DA SUA CONTA - BANCO DO BRASIL
        $dadosboleto["agencia"] = "0452"; // Num da agencia, sem digito
        $dadosboleto["conta"] = "55003";    // Num da conta, sem digito

        // DADOS PERSONALIZADOS - BANCO DO BRASIL
        $dadosboleto["convenio"] = "2953936";  // Num do convênio - REGRA: 6 ou 7 ou 8 dígitos
        $dadosboleto["contrato"] = "2953936"; // Num do seu contrato
        $dadosboleto["carteira"] = "17";
        $dadosboleto["variacao_carteira"] = "01-9";  // Variação da Carteira, com traço (opcional)

        // TIPO DO BOLETO
        $dadosboleto["formatacao_convenio"] = "7"; // REGRA: 8 p/ Convênio c/ 8 dígitos, 7 p/ Convênio c/ 7 dígitos, ou 6 se Convênio c/ 6 dígitos
        $dadosboleto["formatacao_nosso_numero"] = "2"; // REGRA: Usado apenas p/ Convênio c/ 6 dígitos: informe 1 se for NossoNúmero de até 5 dígitos ou 2 para opção de até 17 dígitos

    }

}