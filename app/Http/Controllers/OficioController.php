<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Session;

use App\Library\CeriFuncoes;

use App\bem_imovel;
use App\comarca;
use App\documento_prenotacao;
use App\estado;
use App\pedido;
use App\pedido_produto_item;
use App\serventia;
use App\situacao_pedido_grupo_produto;
use App\tipo_custa;
use App\pessoa;
use App\usuario;
use App\documento;
use App\documento_enviado;
use App\documento_corpo;
use App\classificacao_documento;
use App\tipo_documento;
use App\tipo_prioridade;
use App\observacao_usuario_documento;
use App\vara;
use App\arquivo_grupo_produto;
use App\arquivo_grupo_produto_composicao;
use App\unidade_gestora;
use App\unidade_judiciaria;
use App\unidade_judiciaria_divisao;

class OficioController extends Controller {

	const ID_GRUPO_PRODUTO = 9;
	const ID_PRODUTO = 23;
	const ID_PRODUTO_ITEM = 33;
	const ID_SITUACAO_CADASTRADO = 62;

	const TP_USUARIO_SERVENTIA = 2;
	const TP_USUARIO_JUDICIARIO = 4;
	const TP_USUARIO_CORREGEDORIA = 7;

	public function __construct(documento $documento) {
		$this->documento =$documento;

		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

	public function recebidos(Request $request, documento $documento) {
		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela de oficio aba recebidos','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

		$class = $this;

		$documentos = $documento->join('pedido','pedido.id_pedido','=','documento.id_pedido')
								->join('pedido_pessoa',function($join) use ($class) {
									$join->on('pedido_pessoa.id_pedido','=','pedido.id_pedido');
									if(!in_array($class->id_tipo_pessoa,array(9,13))) {
										$join->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
									}
							  	})
								->join('documento_enviado',function($join) {
									$join->on('documento_enviado.id_documento','=','documento.id_documento')
										 ->where('documento_enviado.in_excluido','=','N');
								});

		if ($request->isMethod('post'))	{
			if($request->numero_documento!='') {
				$documentos->where('numero_documento', $request->numero_documento);
			}
			if($request->numero_processo!='') {
				$documentos->where('numero_processo', $request->numero_processo);
			}
			if($request->protocolo_pedido!=''){
			    $documentos->where('numero_protocolo', $request->protocolo_pedido);
            }
		}

		$documentos = $documentos->orderBy('dt_criacao','desc')->paginate(10);

		return view('servicos.oficio.geral-oficio-recebidos',compact('documentos', 'request', 'class'));
    }

    public function enviados(Request $request, documento $documento) {
		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela de oficio aba enviados','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

    	$documentos = $documento->join('pedido','pedido.id_pedido','=','documento.id_pedido')
                                ->where('in_excluido','N');

        if(!in_array($this->id_tipo_pessoa,array(9,13))) {
            $documentos->where('pedido.id_pessoa_origem','=',$this->id_pessoa);
        }

		if ($request->isMethod('post'))	{
			if($request->numero_documento!='') {
				$documentos->where('numero_documento', $request->numero_documento);
			}
			if($request->numero_processo!='') {
				$documentos->where('numero_processo', $request->numero_processo);
			}
            if($request->protocolo_pedido!=''){
                $documentos->where('numero_protocolo', $request->protocolo_pedido);
            }
		}

		$documentos = $documentos->orderBy('dt_criacao','desc')->paginate(10);

		$class = $this;

		return view('servicos.oficio.geral-oficio-enviados',compact('documentos', 'request', 'class'));
    }

	public function novo(Request $request, classificacao_documento $classificacao_documento, tipo_prioridade $tipo_prioridade, estado $estado, comarca $comarcas, tipo_custa $tipo_custa, vara $vara, unidade_gestora $unidade_gestora, unidade_judiciaria $unidade_judiciaria, unidade_judiciaria_divisao $unidade_judiciaria_divisao) {
		$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		$serventias = $estado->find(env('ID_ESTADO'))->serventias_estado()->orderBy('serventia.no_serventia')->get();
		$comarcas = $comarcas->orderBy('no_comarca')->get();
		$varas = $vara->orderBy('no_vara')->get();
		$unidades_gestoras = $unidade_gestora->where('in_registro_ativo','S')->orderBy('no_unidade_gestora')->get();
		$unidades_judiciarias = $unidade_judiciaria->where('in_registro_ativo','S')->orderBy('no_unidade_judiciaria')->get();
		$unidade_judiciaria_divisoes = $unidade_judiciaria_divisao->where('in_registro_ativo','S')->orderBy('no_unidade_judiciaria_divisao')->get();
		$tipo_custas = $tipo_custa->orderBy('nu_ordem')->get();
		$classificacoes = $classificacao_documento->where('in_registro_ativo','S')
												  ->orderBy('nu_ordem','asc')
												  ->get();
		$prioridades = $tipo_prioridade->where('in_registro_ativo','S')
									   ->orderBy('nu_ordem','asc')
									   ->get();
		$class = $this;
		$oficio_token = str_random(30);
		return view('servicos.oficio.geral-oficio-novo',compact('class','classificacoes','prioridades', 'cidades', 'serventias', 'comarcas', 'varas', 'tipo_custas', 'unidades_gestoras', 'unidades_judiciarias', 'unidade_judiciaria_divisoes', 'oficio_token'));
    }

    public function inserir(Request $request) {
		$erro = 0;

		DB::beginTransaction();

		$CeriFuncoes = new CeriFuncoes();

		foreach ($request->id_destinatario as $id_destinatario) {
			$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

			$novo_pedido = new pedido();
			$novo_pedido->id_usuario = Auth::User()->id_usuario;
			$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
			$novo_pedido->id_produto = $this::ID_PRODUTO;
			switch ($this->id_tipo_pessoa) {
				case 2:
					$novo_pedido->id_alcada = 2;
					break;
				case 4:
					$novo_pedido->id_alcada = 4;
					break;
			}
			$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
			$novo_pedido->dt_pedido = Carbon::now();
			$novo_pedido->nu_quantidade = 0;
			$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
			$novo_pedido->dt_cadastro = Carbon::now();
			$novo_pedido->id_pessoa_origem = $this->id_pessoa;

			if ($novo_pedido->save()) {
				$novo_pedido_produto_item = new pedido_produto_item();
				$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
				$novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM;
				$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
				if (!$novo_pedido_produto_item->save()) {
					$erro = 90002;
				}

				$novo_documento = new documento();
				$novo_documento->id_classificacao_documento = ($request->id_classificacao_documento > 0 ? $request->id_classificacao_documento : NULL);
				$novo_documento->id_tipo_documento = ($request->id_tipo_documento > 0 ? $request->id_tipo_documento : NULL);
				$novo_documento->id_usuario = Auth::User()->id_usuario;
				$novo_documento->id_situacao_documento = 1;
				$novo_documento->id_pedido = $novo_pedido->id_pedido;
				$novo_documento->numero_protocolo = $protocolo_pedido[0]->f_geraprotocolo;
				$novo_documento->numero_documento = $request->numero_documento;
				$novo_documento->numero_processo = $request->numero_processo;
				$novo_documento->no_documento = $request->no_documento;
				$novo_documento->id_tipo_custa = $request->id_tipo_custa;
				$novo_documento->dt_criacao = Carbon::now();
				$novo_documento->in_confirmacao_leitura = 'N';
				$novo_documento->in_encaminhado = 'N';
				$novo_documento->in_respondido = 'N';
				$novo_documento->in_excluido = 'N';
				$novo_documento->in_sigiloso = 'N';
				$novo_documento->id_usuario_cad = Auth::User()->id_usuario;
				$novo_documento->id_tipo_usuario_dest = $request->id_tipo_usuario;
				$novo_documento->tp_processo = $request->tp_processo;

				switch ($request->id_tipo_usuario) {
					case 2:
						$serventia_selecionada = new serventia();
						$serventia_selecionada = $serventia_selecionada->find($id_destinatario);
						$pessoa = $serventia_selecionada->pessoa;
						break;
					case 4:
						$vara_selecionada = new vara();
						$vara_selecionada = $vara_selecionada->find($id_destinatario);
						$pessoa = $vara_selecionada->pessoa;
						break;
					case 7:
						$unidade_gestora_selecionada = new unidade_gestora();
						$unidade_gestora_selecionada = $unidade_gestora_selecionada->find($id_destinatario);
						$pessoa = $unidade_gestora_selecionada->pessoa;
						break;
					case 11:
						$unidade_judiciaria_divisao_selecionada = new unidade_judiciaria_divisao();
						$unidade_judiciaria_divisao_selecionada = $unidade_judiciaria_divisao_selecionada->find($id_destinatario);
						$pessoa = $unidade_judiciaria_divisao_selecionada->pessoa;
						break;
				}
				$novo_pedido->pessoas()->attach($pessoa);

				$novo_pedido->inserir_notificacao($this->id_pessoa,$pessoa->id_pessoa,'Um novo ofício foi inserido. Protocolo '.$protocolo_pedido[0]->f_geraprotocolo);

				if ($novo_documento->save()) {
					$novo_documento_enviado = new documento_enviado();
					$novo_documento_enviado->id_documento = $novo_documento->id_documento;
					$novo_documento_enviado->id_usuario_destinatario = $novo_pedido->pedido_pessoa_atual->pessoa->usuario_pessoa[0]->usuario->id_usuario;
					$novo_documento_enviado->dt_envio = Carbon::now();
					$novo_documento_enviado->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_documento_enviado->save()) {
						$erro = 90004;
					}

					$novo_documento_corpo = new documento_corpo();
					$novo_documento_corpo->id_documento = $novo_documento->id_documento;
					if ($request->no_apresentado != '') {
						$novo_documento_corpo->no_apresentado = $request->no_apresentado;
						$novo_documento_corpo->tp_pessoa_apresentado = $request->tp_pessoa_apresentado;
						$novo_documento_corpo->nu_cpf_cnpj_apresentado = preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj_apresentado);
					}
					if ($request->no_requerido != '') {
						$novo_documento_corpo->no_requerido = $request->no_requerido;
						$novo_documento_corpo->tp_pessoa_requerido = $request->tp_pessoa_requerido;
						$novo_documento_corpo->nu_cpf_cnpj_requerido = preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj_requerido);
					}
					if ($request->no_requerente != '') {
						$novo_documento_corpo->no_requerente = $request->no_requerente;
						$novo_documento_corpo->tp_pessoa_requerente = $request->tp_pessoa_requerente;
						$novo_documento_corpo->nu_cpf_cnpj_requerente = preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj_requerente);
					}
					$novo_documento_corpo->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_documento_corpo->save()) {
						$erro = 90005;
					}

					if ($request->session()->has('arquivos_'.$request->oficio_token)) {
						$destino = '/oficios/'.$novo_documento->id_documento;
						$arquivos = $request->session()->get('arquivos_'.$request->oficio_token);
						$erro_loop_arq = false;
						$erro_loop_sql = false;
						foreach ($arquivos as $key => $arquivo) {
							$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
							Storage::makeDirectory('/public'.$destino_final);

							$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
							$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];
							
							$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
							$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
							$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
							$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
							$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
							$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
							$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
							$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
							if ($arquivo['dt_assinado']!='') {
								$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
							}
							if ($arquivo['id_usuario_certificado']>0) {
								$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
							}
							if (!empty($arquivo['no_arquivo_p7s'])) {
								$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
								$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
							}
							if ($novo_arquivo_grupo_produto->save()) {
								if (Storage::exists($origem_arquivo)) {
									if (Storage::exists($destino_arquivo)) {
										Storage::delete($destino_arquivo);
									}
									if (Storage::copy($origem_arquivo,$destino_arquivo)) {
										if (count($arquivo['no_arquivos_originais'])>0) {
											foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
												$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
												$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

												$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
												$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
												$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
												$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
												$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
												$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
												if ($novo_arquivo_grupo_produto_composicao->save()) {
													if (Storage::exists($origem_arquivo_original)) {
														if (Storage::exists($destino_arquivo_original)) {
															Storage::delete($destino_arquivo_original);
														}
														if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_sql = true;
												}
											}
										}
									} else {
										$erro_loop_arq = true;
									}
									$novo_documento->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_sql = true;
							}
						}
						if ($erro_loop_arq) {
							$erro = 90007;
						}
						if ($erro_loop_sql) {
							$erro = 90008;
						}
					} else {
						$erro = 90006;
					}
				} else {
					$erro = 90003;
				}
			} else {
				$erro = 90001;
			}
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O ofício foi inserido com sucesso.'));
		}
	}

    public function detalhes(Request $request, documento $documento, tipo_custa $tipo_custa) {
    	if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);
			$historico_documento = $documento->where('id_documento_origem', $documento->id_documento_origem)->orderby('id_documento', 'desc')->get();

			if ($request->origem=='R') {
				$documento_enviado = new documento_enviado();
				$documento_enviado->where('id_documento', $documento->id_documento)
								  ->update(['in_leitura' => 'S', 'dt_leitura' => Carbon::now()]);
			}

			$documento->pedido->visualizar_notificacoes();

			$tipo_custas = $tipo_custa->orderBy('nu_ordem')->get();

			$class = $this;
			$origem = $request->origem;

			return view('servicos.oficio.geral-oficio-detalhes',compact('class', 'documento', 'historico_documento', 'origem', 'tipo_custas'));
		}
    }
	
    public function observacoes(Request $request, documento $documento, documento_enviado $documento_enviado) {
    	if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);
			
			if ($request->origem=='R') {
				$documento_enviado = new documento_enviado();
				$documento_enviado->where('id_documento', $documento->id_documento)
								  ->update(['in_leitura' => 'S', 'dt_leitura' => Carbon::now()]);
			}

			$documento->pedido->visualizar_notificacoes();

			$class = $this;

			return view('servicos.oficio.geral-oficio-observacoes',compact('class','documento'));
		}
    }
	public function inserir_observacao(Request $request, documento $documento) {
    	if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);

			$nova_observacao = new observacao_usuario_documento();
			$nova_observacao->id_documento = $documento->id_documento;
			$nova_observacao->id_usuario = Auth::User()->id_usuario;
			$nova_observacao->de_observacao = $request->de_observacao;
			$nova_observacao->dt_observacao = Carbon::now();
			$nova_observacao->id_usuario_cad = Auth::User()->id_usuario;
			
			if ($nova_observacao->save()) {
				if($this->id_pessoa!=$documento->pedido->id_pessoa_origem) {
					$pedido->inserir_notificacao($this->id_pessoa,$pedido->pedido_pessoa_atual->id_pessoa,'Uma observação foi inserida no ofício protocolo '.$pedido->protocolo_pedido);
				}

				return response()->json(array('status'=> 'sucesso', 
										  	  'recarrega' => 'false',
										  	  'msg' => 'A observação foi inserida com sucesso.',
										  	  'observacao'=>$nova_observacao,
										  	  'usuario'=>Auth::User()->pessoa->no_pessoa,
										  	  'dt_formatada'=>Carbon::now()->format('d/m/Y H:i') ));
			} else {
				return response()->json(array('status'=> 'erro',
										  	  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			}
		}
    }
	
    public function encaminhar(Request $request, documento $documento, estado $estado, comarca $comarcas, documento_enviado $documento_enviado, vara $vara, tipo_custa $tipo_custa, unidade_gestora $unidade_gestora, unidade_judiciaria $unidade_judiciaria, unidade_judiciaria_divisao $unidade_judiciaria_divisao) {
    	if ($request->id_documento>0) {
			$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
			$serventias = $estado->find(env('ID_ESTADO'))->serventias_estado()->orderBy('serventia.no_serventia')->get();
			$comarcas = $comarcas->orderBy('no_comarca')->get();
			$varas = $vara->orderBy('no_vara')->get();
			$unidades_gestoras = $unidade_gestora->where('in_registro_ativo','S')->orderBy('no_unidade_gestora')->get();
			$unidades_judiciarias = $unidade_judiciaria->where('in_registro_ativo','S')->orderBy('no_unidade_judiciaria')->get();
			$unidade_judiciaria_divisoes = $unidade_judiciaria_divisao->where('in_registro_ativo','S')->orderBy('no_unidade_judiciaria_divisao')->get();
			$tipo_custas = $tipo_custa->orderBy('nu_ordem')->get();

			$documento = $documento->find($request->id_documento);
			if ($request->origem=='R') {
				$documento_enviado = new documento_enviado();
				$documento_enviado->where('id_documento', $documento->id_documento)
								  ->update(['in_leitura' => 'S', 'dt_leitura' => Carbon::now()]);

				$documento->pedido->visualizar_notificacoes();
			}

			$arquivos = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',15)->get();
			$arquivos_isencao = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',16)->get();

			return view('servicos.oficio.geral-oficio-encaminhar',compact('documento', 'cidades', 'serventias', 'comarcas', 'varas', 'unidades_gestoras', 'unidades_judiciarias', 'unidade_judiciaria_divisoes', 'tipo_custas', 'arquivos', 'arquivos_isencao'));
		}
    }

	public function inserir_encaminhamento(Request $request, documento $documento, documento_enviado $documento_enviado, documento_corpo $documento_corpo, pedido $pedido) {
    	if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);
			$erro = 0;

			DB::beginTransaction();

			$CeriFuncoes = new CeriFuncoes();

			foreach ($request->id_destinatario as $id_destinatario) {
				$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

				$novo_pedido = new pedido();
				$novo_pedido->id_usuario = Auth::User()->id_usuario;
				$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
				$novo_pedido->id_produto = $this::ID_PRODUTO;
				switch ($this->id_tipo_pessoa) {
					case 2:
						$novo_pedido->id_alcada = 2;
						break;
					case 4:
						$novo_pedido->id_alcada = 4;
						break;
				}
				$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
				$novo_pedido->dt_pedido = Carbon::now();
				$novo_pedido->nu_quantidade = 0;
				$novo_pedido->va_pedido = 0;
				$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
				$novo_pedido->dt_cadastro = Carbon::now();
				$novo_pedido->id_pessoa_origem = $this->id_pessoa;
				
				if ($novo_pedido->save()) {
					$novo_pedido_produto_item = new pedido_produto_item();
					$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
					$novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM;
					$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_pedido_produto_item->save()) {
						$erro = 90023;
					}

					$novo_documento = new documento();
					$novo_documento->id_classificacao_documento = $documento->id_classificacao_documento;
					$novo_documento->id_tipo_documento = $documento->id_tipo_documento;
					$novo_documento->id_usuario = Auth::User()->id_usuario;
					$novo_documento->id_situacao_documento = 1;
					$novo_documento->id_pedido = $novo_pedido->id_pedido;
					$novo_documento->numero_protocolo = $protocolo_pedido[0]->f_geraprotocolo;
					$novo_documento->numero_documento = $documento->numero_documento;
					$novo_documento->numero_processo = $documento->numero_processo;
					$novo_documento->no_documento = $documento->no_documento;
					$novo_documento->id_tipo_custa = $documento->id_tipo_custa;
					$novo_documento->dt_criacao = Carbon::now();
					$novo_documento->in_confirmacao_leitura = 'N';
					$novo_documento->in_encaminhado = 'S';
					$novo_documento->in_respondido = 'N';
					$novo_documento->in_excluido = 'N';
					$novo_documento->in_sigiloso = 'N';
					$novo_documento->id_usuario_cad = Auth::User()->id_usuario;
					$novo_documento->id_tipo_usuario_dest = $request->id_tipo_usuario;
					$novo_documento->tp_processo = $documento->tp_processo;
					$novo_documento->id_documento_anterior = $documento->id_documento;
					$novo_documento->id_documento_origem = $documento->id_documento_origem;

					switch ($request->id_tipo_usuario) {
						case 2:
							$serventia_selecionada = new serventia();
							$serventia_selecionada = $serventia_selecionada->find($id_destinatario);
							$pessoa = $serventia_selecionada->pessoa;
							break;
						case 4:
							$vara_selecionada = new vara();
							$vara_selecionada = $vara_selecionada->find($id_destinatario);
							$pessoa = $vara_selecionada->pessoa;
							break;
						case 7:
							$unidade_gestora_selecionada = new unidade_gestora();
							$unidade_gestora_selecionada = $unidade_gestora_selecionada->find($id_destinatario);
							$pessoa = $unidade_gestora_selecionada->pessoa;
							break;
						case 11:
							$unidade_judiciaria_divisao_selecionada = new unidade_judiciaria_divisao();
							$unidade_judiciaria_divisao_selecionada = $unidade_judiciaria_divisao_selecionada->find($id_destinatario);
							$pessoa = $unidade_judiciaria_divisao_selecionada->pessoa;
							break;
					}
					$novo_pedido->pessoas()->attach($pessoa);

					$novo_pedido->inserir_notificacao($this->id_pessoa,$pessoa->id_pessoa,'O ofício protocolo '.$documento->pedido->protocolo_pedido.' foi encaminhado. Novo protocolo '.$protocolo_pedido[0]->f_geraprotocolo);

					if ($novo_documento->save()) {
						$novo_documento_enviado = new documento_enviado();
						$novo_documento_enviado->id_documento = $novo_documento->id_documento;
						$novo_documento_enviado->id_usuario_destinatario = $novo_pedido->pedido_pessoa_atual->pessoa->usuario_pessoa[0]->usuario->id_usuario;
						$novo_documento_enviado->dt_envio = Carbon::now();
						$novo_documento_enviado->id_usuario_cad = Auth::User()->id_usuario;
						if (!$novo_documento_enviado->save()) {
							$erro = 90025;
						}

						$novo_documento_corpo = new documento_corpo();
						$novo_documento_corpo->id_documento = $novo_documento->id_documento;
						$novo_documento_corpo->id_usuario_cad = Auth::User()->id_usuario;
						if (!$novo_documento_corpo->save()) {
							$erro = 90026;
						}

						$arquivos = $documento->arquivos_grupo()->get();
						if (count($arquivos)>0) {
							$destino = '/oficios/'.$novo_documento->id_documento;
							$erro_loop_arq = false;
							$erro_loop_sql = false;
							foreach ($arquivos as $key => $arquivo) {
								$destino_final = $destino.'/'.$arquivo->id_tipo_arquivo_grupo_produto;
								Storage::makeDirectory('/public'.$destino_final);

								$origem_arquivo = '/public'.$arquivo->no_local_arquivo.'/'.$arquivo->no_arquivo;
								$destino_arquivo = '/public'.$destino_final.'/'.$arquivo->no_arquivo;

								$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
								$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
								$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo->id_tipo_arquivo_grupo_produto;
								$novo_arquivo_grupo_produto->no_arquivo = $arquivo->no_arquivo;
								$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
								$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
								$novo_arquivo_grupo_produto->no_extensao = $arquivo->no_extensao;
								$novo_arquivo_grupo_produto->in_ass_digital = $arquivo->in_ass_digital;
								$novo_arquivo_grupo_produto->nu_tamanho_kb = $arquivo->nu_tamanho_kb;
								$novo_arquivo_grupo_produto->no_hash = $arquivo->no_hash;
								$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo->dt_ass_digital;
								$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo->id_usuario_certificado;
								$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo->no_arquivo_p7s;
								$novo_arquivo_grupo_produto->no_hash_p7s = $arquivo->no_hash_p7s;

								if ($novo_arquivo_grupo_produto->save()) {
									if (Storage::exists($origem_arquivo)) {
										if (Storage::exists($destino_arquivo)) {
											Storage::delete($destino_arquivo);
										}
										if (Storage::copy($origem_arquivo,$destino_arquivo)) {
											if (count($arquivo->arquivo_grupo_produto_composicao)>0) {
												foreach ($arquivo->arquivo_grupo_produto_composicao as $arquivo_original) {
													$origem_arquivo_original = '/public'.$arquivo->no_local_arquivo.'/'.$arquivo_original->no_arquivo;
													$destino_arquivo_original = '/public'.$destino_final.'/'.$arquivo_original->no_arquivo;

													$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
													$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
													$novo_arquivo_grupo_produto_composicao->no_arquivo = $arquivo_original->no_arquivo;
													$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
													$novo_arquivo_grupo_produto_composicao->no_extensao = $arquivo_original->no_extensao;
													$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = $arquivo_original->nu_tamanho_kb;
													$novo_arquivo_grupo_produto_composicao->no_hash = $arquivo_original->no_hash;
													$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
													if ($novo_arquivo_grupo_produto_composicao->save()) {
														if (Storage::exists($origem_arquivo_original)) {
															if (Storage::exists($destino_arquivo_original)) {
																Storage::delete($destino_arquivo_original);
															}
															if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
																$erro_loop_arq = true;
															}
														} else {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_sql = true;
													}
												}
											}
										} else {
											$erro_loop_arq = true;
										}
										$novo_documento->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
									} else {
										$erro_loop_arq = true;
									}
								} else {
									$erro_loop_sql = true;
								}
							}
							if ($erro_loop_arq) {
								$erro = 90027;
							}
							if ($erro_loop_sql) {
								$erro = 90028;
							}
						}

					} else {
						$erro = 90024;
					}
				} else {
					$erro = 90022;
				}
			}

			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O encaminhamento do ofício foi inserido com sucesso.'));
			}
		}
    }

	public function responder(Request $request, documento $documento, classificacao_documento $classificacao_documento, tipo_custa $tipo_custa, tipo_documento $tipo_documento) {
    	if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);

			$documento_enviado = new documento_enviado();
			$documento_enviado->where('id_documento', $documento->id_documento)
							  ->update(['in_leitura' => 'S', 'dt_leitura' => Carbon::now()]);

			$documento->pedido->visualizar_notificacoes();
			
			$class = $this;
			$tipo_custas = $tipo_custa->orderBy('nu_ordem')->get();
			$classificacoes = $classificacao_documento->where('in_registro_ativo','S')
													  ->orderBy('nu_ordem','asc')
													  ->get();

    		$tipos_documento = $tipo_documento->where('id_classificacao_documento',$documento->id_classificacao_documento)
    										  ->where('in_registro_ativo','S')
    										  ->orderBy('nu_ordem','asc')
    										  ->get();

			$arquivos = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',15)->get();
			$arquivos_isencao = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',16)->get();
			$oficio_token = str_random(30);

			return view('servicos.oficio.geral-oficio-responder',compact('class', 'documento', 'classificacoes', 'tipos_documento', 'tipo_custas', 'arquivos', 'arquivos_isencao', 'oficio_token'));
		}
    }

    public function inserir_resposta(Request $request, documento $documento) {
		if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);
			$erro = 0;
			
			DB::beginTransaction();

			$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

			$novo_pedido = new pedido();
			$novo_pedido->id_usuario = Auth::User()->id_usuario;
			$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
			$novo_pedido->id_produto = $this::ID_PRODUTO;
			switch ($this->id_tipo_pessoa) {
				case 2:
					$novo_pedido->id_alcada = 2;
					break;
				case 4:
					$novo_pedido->id_alcada = 4;
					break;
			}
			$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
			$novo_pedido->dt_pedido = Carbon::now();
			$novo_pedido->nu_quantidade = 0;
			$novo_pedido->va_pedido = 0;
			$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
			$novo_pedido->dt_cadastro = Carbon::now();
			$novo_pedido->id_pessoa_origem = $this->id_pessoa;
			
			if ($novo_pedido->save()) {
				$novo_pedido_produto_item = new pedido_produto_item();
				$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
				$novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM;
				$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
				if (!$novo_pedido_produto_item->save()) {
					$erro = 90012;
				}

				$novo_documento = new documento();
				$novo_documento->id_usuario = Auth::User()->id_usuario;
				$novo_documento->id_situacao_documento = 1;
				$novo_documento->id_pedido = $novo_pedido->id_pedido;
				$novo_documento->numero_protocolo = $protocolo_pedido[0]->f_geraprotocolo;
				$novo_documento->numero_documento = $documento->numero_documento;
				$novo_documento->numero_processo = $documento->numero_processo;
				$novo_documento->id_tipo_custa = $documento->id_tipo_custa;
				$novo_documento->dt_criacao = Carbon::now();
				$novo_documento->in_confirmacao_leitura = 'N';
				$novo_documento->in_encaminhado = 'N';
				$novo_documento->in_respondido = 'S';
				$novo_documento->in_excluido = 'N';
				$novo_documento->in_sigiloso = 'N';
				$novo_documento->id_usuario_cad = Auth::User()->id_usuario;
				$novo_documento->tp_processo = $documento->tp_processo;
				$novo_documento->id_documento_anterior = $documento->id_documento;
				$novo_documento->id_tipo_usuario_dest = $documento->usuario->id_tipo_usuario;

				$pessoa = $documento->pedido->pessoa_origem;
				$novo_pedido->pessoas()->attach($pessoa);

				$novo_pedido->inserir_notificacao($this->id_pessoa,$pessoa->id_pessoa,'O ofício protocolo '.$documento->pedido->protocolo_pedido.' foi respondido. Novo protocolo '.$protocolo_pedido[0]->f_geraprotocolo);

				$novo_documento->id_classificacao_documento = ($request->id_classificacao_documento>0?$request->id_classificacao_documento:NULL);
				$novo_documento->id_tipo_documento = ($request->id_tipo_documento>0?$request->id_tipo_documento:NULL);
				$novo_documento->no_documento = $request->de_resposta;
				$novo_documento->de_resposta = $request->de_resposta;
				$novo_documento->in_reposta_positivo = $request->in_reposta_positivo;
				$novo_documento->id_documento_origem = ($documento->id_documento_origem>0)?$documento->id_documento_origem:$documento->id_documento;
				$novo_documento->de_custa = ($documento->de_custa!=''?$documento->de_custa:NULL);
				$novo_documento->va_custa = ($documento->va_custa>0?$documento->va_custa:NULL);

				if ($novo_documento->save()) {
					$novo_documento_enviado = new documento_enviado();
					$novo_documento_enviado->id_documento = $novo_documento->id_documento;
					$novo_documento_enviado->id_usuario_destinatario = $novo_pedido->pedido_pessoa_atual->pessoa->usuario_pessoa[0]->usuario->id_usuario;
					$novo_documento_enviado->dt_envio = Carbon::now();
					$novo_documento_enviado->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_documento_enviado->save()) {
						$erro = 90014;
					}

					$update_documento_enviado = new documento_enviado();
                    $update_documento_enviado->where('id_documento', $documento->id_documento)
                        ->update(['in_respondido' => 'S']);

					$novo_documento_corpo = new documento_corpo();
					$novo_documento_corpo->id_documento = $novo_documento->id_documento;
					$novo_documento_corpo->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_documento_corpo->save()) {
						$erro = 90015;
					}

					$arquivos_isencao = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',16)->get();
					if (count($arquivos_isencao)>0) {
						$destino = '/oficios/'.$novo_documento->id_documento;
						$erro_loop_arq = false;
						$erro_loop_sql = false;
						foreach ($arquivos_isencao as $key => $arquivo) {
							$destino_final = $destino.'/'.$arquivo->id_tipo_arquivo_grupo_produto;
							Storage::makeDirectory('/public'.$destino_final);

							$origem_arquivo = '/public'.$arquivo->no_local_arquivo.'/'.$arquivo->no_arquivo;
							$destino_arquivo = '/public'.$destino_final.'/'.$arquivo->no_arquivo;

							$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
							$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
							$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo->id_tipo_arquivo_grupo_produto;
							$novo_arquivo_grupo_produto->no_arquivo = $arquivo->no_arquivo;
							$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
							$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
							$novo_arquivo_grupo_produto->no_extensao = $arquivo->no_extensao;
							$novo_arquivo_grupo_produto->in_ass_digital = $arquivo->in_ass_digital;
							$novo_arquivo_grupo_produto->nu_tamanho_kb = $arquivo->nu_tamanho_kb;
							$novo_arquivo_grupo_produto->no_hash = $arquivo->no_hash;
							$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo->dt_ass_digital;
							$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo->id_usuario_certificado;
							$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo->no_arquivo_p7s;
							$novo_arquivo_grupo_produto->no_hash_p7s = $arquivo->no_hash_p7s;

							if ($novo_arquivo_grupo_produto->save()) {
								if (Storage::exists($origem_arquivo)) {
									if (Storage::exists($destino_arquivo)) {
										Storage::delete($destino_arquivo);
									}
									if (Storage::copy($origem_arquivo,$destino_arquivo)) {
										if (count($arquivo->arquivo_grupo_produto_composicao)>0) {
											foreach ($arquivo->arquivo_grupo_produto_composicao as $arquivo_original) {
												$origem_arquivo_original = '/public'.$arquivo->no_local_arquivo.'/'.$arquivo_original->no_arquivo;
												$destino_arquivo_original = '/public'.$destino_final.'/'.$arquivo_original->no_arquivo;

												$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
												$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
												$novo_arquivo_grupo_produto_composicao->no_arquivo = $arquivo_original->no_arquivo;
												$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
												$novo_arquivo_grupo_produto_composicao->no_extensao = $arquivo_original->no_extensao;
												$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = $arquivo_original->nu_tamanho_kb;
												$novo_arquivo_grupo_produto_composicao->no_hash = $arquivo_original->no_hash;
												$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
												if ($novo_arquivo_grupo_produto_composicao->save()) {
													if (Storage::exists($origem_arquivo_original)) {
														if (Storage::exists($destino_arquivo_original)) {
															Storage::delete($destino_arquivo_original);
														}
														if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_sql = true;
												}
											}
										}
									} else {
										$erro_loop_arq = true;
									}
									$novo_documento->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_sql = true;
							}
						}
						if ($erro_loop_arq) {
							$erro = 90016;
						}
						if ($erro_loop_sql) {
							$erro = 90017;
						}
					}

					if ($request->session()->has('arquivos_'.$request->oficio_token)) {
						$destino = '/oficios/'.$novo_documento->id_documento;
						$arquivos = $request->session()->get('arquivos_'.$request->oficio_token);
						$erro_loop_arq = false;
						$erro_loop_sql = false;
						foreach ($arquivos as $key => $arquivo) {
							$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
							Storage::makeDirectory('/public'.$destino_final);

							$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
							$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];
							
							$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
							$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
							$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
							$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
							$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
							$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
							$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
							$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
							if ($arquivo['dt_assinado']!='') {
								$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
							}
							if ($arquivo['id_usuario_certificado']>0) {
								$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
							}
							if (!empty($arquivo['no_arquivo_p7s'])) {
								$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
								$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
							}
							if ($novo_arquivo_grupo_produto->save()) {
								if (Storage::exists($origem_arquivo)) {
									if (Storage::exists($destino_arquivo)) {
										Storage::delete($destino_arquivo);
									}
									if (Storage::copy($origem_arquivo,$destino_arquivo)) {
										if (count($arquivo['no_arquivos_originais'])>0) {
											foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
												$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
												$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

												$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
												$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
												$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
												$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
												$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
												$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
												if ($novo_arquivo_grupo_produto_composicao->save()) {
													if (Storage::exists($origem_arquivo_original)) {
														if (Storage::exists($destino_arquivo_original)) {
															Storage::delete($destino_arquivo_original);
														}
														if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_sql = true;
												}
											}
										}
									} else {
										$erro_loop_arq = true;
									}
									$novo_documento->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_sql = true;
							}
						}
						if ($erro_loop_arq) {
							$erro = 90018;
						}
						if ($erro_loop_sql) {
							$erro = 90019;
						}
					}
				} else {
					$erro = 90013;
				}
			} else {
				$erro = 90011;
			}

			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'A resposta do ofício foi inserida com sucesso.'));
			}
		}
    }

    public function inserir_custas(Request $request, documento $documento) {
		if ($request->id_documento>0) {
			DB::beginTransaction();

			$documento = $documento->find($request->id_documento);
			$documento->de_custa = $request->de_custa;
			$documento->va_custa = converte_float($request->va_custa);

			if ($documento->save()) {
				if ($documento->id_pedido>0) {
					$dados = ["va_pedido" => converte_float($documento->va_custa),
                              "tp_movimentacao_financeira" => 2,
                              "tp_movimentacao" => 'S',
                              "id_forma_pagamento" => null,
                              "v_id_pedido_desconto" => null,
                              "v_va_desconto" => 0,
                              "v_in_desconto" => 'N',
                              "v_va_desconto_perc" => null,
							  "v_id_compra_credito" => $request->id_compra_credito];
					$documento->pedido->registrar_valor_pedido($dados);
				}
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'false',
											  'msg' => 'As custas foram salvas com sucesso.'));
			} else {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº 90009'));
			}
		}
	}

	public function inserir_prenotacao(Request $request) {
		if ($request->id_documento>0) {
			DB::beginTransaction();

			$nova_prenotacao = new documento_prenotacao();
			$nova_prenotacao->id_documento = $request->id_documento;
			$nova_prenotacao->codigo_prenotacao = $request->codigo_prenotacao;
			$nova_prenotacao->dt_prenotacao = Carbon::createFromFormat('d/m/Y',$request->dt_prenotacao);
			$nova_prenotacao->id_usuario_cad = Auth::User()->id_usuario;

			if ($nova_prenotacao->save()) {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'false',
											  'msg' => 'A prenotação do ofício foi salva com sucesso.'));
			} else {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº 90010'));
			}
		}
	}
	
    public function excluir(Request $request, documento $documento, documento_enviado $documento_enviado) {
		if ($request->id_documento>0) {
			$erro = 0;

			DB::beginTransaction();

			$documento = $documento->find($request->id_documento);

			if(count($documento)>0) {
				if (Auth::User()->id_usuario==$documento->id_usuario) {
					$documento->in_excluido = 'S';
					if (!$documento->save()) {
						$erro = 90020;
					}
				} else {
					$documento_enviado = $documento_enviado->find($documento->documento_enviado->id_documento_enviado);
					$documento_enviado->in_excluido='S';
					$documento_enviado->dt_exclusao=Carbon::now();
					if (!$documento_enviado->save()) {
						$erro = 90021;
					}
				}
			}

			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O ofício foi excluído com sucesso.'));
			}			
		}
	}

    public function marcar_lida_oficio(Request $request, documento $documento, documento_enviado $documento_enviado) {
		if ($request->id_documento>0) {
			if($request->aba_acao == 'recebidos'){
				$documento_enviado->where('id_documento', $request->id_documento)
								  ->update(
									[
										'in_leitura' => 'S',
										'dt_leitura' => Carbon::now()
									]
								  );
			}
			return 'SUCESSO';
		}
	}

    public function listar_tipos(Request $request, tipo_documento $tipo_documento) {
    	if ($request->id_classificacao_documento>0) {
    		$tipos = $tipo_documento->where('id_classificacao_documento',$request->id_classificacao_documento)
    								->where('in_registro_ativo','S')
    								->orderBy('nu_ordem','asc')
    								->get();

    		return response()->json($tipos);
    	}
    }

	public function recibo(Request $request, documento $documento) {
		if ($request->id_documento>0) {
			$documento = $documento->find($request->id_documento);
			$arquivo_recibo = $documento->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',17)->first();

			if (is_null($arquivo_recibo)) {
				$erro = 0;

				$destino = '/oficios/'.$documento->id_documento.'/14';
				$no_arquivo = 'oficio_recibo_'.$documento->numero_protocolo.'.pdf';

				Storage::makeDirectory('/public'.$destino);

				$titulo = 'Recibo de ofício: '.$documento->numero_protocolo;
				$pdf = PDF::loadView('pdf.oficio-recibo', compact('documento', 'titulo'));
				$pdf->save(storage_path('app/public'.$destino.'/'.$no_arquivo));

				$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
				$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
				$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = 17;
				$novo_arquivo_grupo_produto->no_arquivo = $no_arquivo;
				$novo_arquivo_grupo_produto->no_local_arquivo = $destino;
				$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
				$novo_arquivo_grupo_produto->no_extensao = 'pdf';
				$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size('/public'.$destino.'/'.$no_arquivo);
				$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app/public'.$destino.'/'.$no_arquivo));
				$novo_arquivo_grupo_produto->save();

				$documento->arquivos_grupo()->attach($novo_arquivo_grupo_produto);

				$arquivo_recibo = $novo_arquivo_grupo_produto;
			}

			return response()->json(array('view'=>view('servicos.oficio.geral-oficio-recibo',compact('arquivo_recibo'))->render()));
		}
	}
}
