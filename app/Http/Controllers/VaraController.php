<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;
use Mail;

use App\vara;
use App\comarca;
use App\vara_tipo;
use App\usuario_pessoa;
use App\pedido_pessoa;
use App\usuario_vara;
use App\pessoa;

class VaraController extends Controller {
	
	public function __construct(vara $vara) {
		$this->vara=$vara;

		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

	public function index(Request $request, vara $vara){
		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_modulo[0]->id_modulo.", 'Acesso a tela principal de cadastro de vara','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_modulo[0]->id_modulo.");"));

		$varas = $vara->orderBy('id_comarca','asc')->orderBy('dt_cadastro','desc')->orderBy('no_vara','asc')->get();
	
		$class = $this;

	   	return view('cadastros.varas',compact('class','request','varas'));
    }

    public function nova(Request $request, comarca $comarca, vara_tipo $vara_tipo) {
    	$comarcas = $comarca->orderBy('nu_ordem','asc')
    						->get();

    	$vara_tipos = $vara_tipo->where('in_registro_ativo','S')
    							->orderBy('nu_ordem','asc')
    							->get();
		
		return view('cadastros.varas-nova',compact('request','comarcas','vara_tipos'));
    }

	public function inserir(Request $request) {
		$erro = 0;

		DB::beginTransaction();

		$nova_pessoa = new pessoa();
		$nova_pessoa->no_pessoa = $request->no_vara;
		$nova_pessoa->tp_pessoa = 'J';
		$nova_pessoa->no_email_pessoa = $request->no_email_pessoa;
		$nova_pessoa->tp_sexo = 'N';
		$nova_pessoa->id_tipo_pessoa = 4;
		if ($nova_pessoa->save()) {
			$nova_vara = new vara();
			$nova_vara->id_vara_tipo = $request->id_vara_tipo;
			$nova_vara->id_comarca = $request->id_comarca;
			$nova_vara->no_vara = $request->no_vara;
			$nova_vara->abv_vara = $request->abv_vara;
			$nova_vara->dt_ini_vigencia = Carbon::now();
			$nova_vara->id_usuario_cad = Auth::User()->id_usuario;
			$nova_vara->id_pessoa = $nova_pessoa->id_pessoa;
			if (!$nova_vara->save()) {
				$erro = 'CVA002';
			}
		} else {
			$erro = 'CVA001';
		}

		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'A vara foi inserida com sucesso.'));
		}
	}
	
	public function detalhes(Request $request, vara $vara) {
		if ($request->id_vara>0) {
			$vara = $vara->find($request->id_vara);
			return view('cadastros.varas-detalhes',compact('vara'));
		}
	}

    public function alterar(Request $request, vara $vara, comarca $comarca, vara_tipo $vara_tipo) {
    	if ($request->id_vara>0) {
			$vara = $vara->find($request->id_vara);

	    	$comarcas = $comarca->orderBy('nu_ordem','asc')
	    						->get();

	    	$vara_tipos = $vara_tipo->where('in_registro_ativo','S')
	    							->orderBy('nu_ordem','asc')
	    							->get();
		
			return view('cadastros.varas-alterar',compact('vara','comarcas','vara_tipos'));
		}
    }

    public function salvar(Request $request, vara $vara, pessoa $pessoa) {
    	if ($request->id_vara>0) {
			$erro = 0;

			DB::beginTransaction();

			$vara = $vara->find($request->id_vara);

			$alterar_pessoa = $pessoa->find($vara->id_pessoa);
			$alterar_pessoa->no_pessoa = $request->no_vara;
			$alterar_pessoa->no_email_pessoa = $request->no_email_pessoa;
			if ($alterar_pessoa->save()) {
				$vara->id_vara_tipo = $request->id_vara_tipo;
				$vara->id_comarca = $request->id_comarca;
				$vara->no_vara = $request->no_vara;
				$vara->abv_vara = $request->abv_vara;
				if (!$vara->save()) {
					$erro = 'CVA005';
				}
			} else {
				$erro = 'CVA006';
			}

			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'A vara foi alterada com sucesso.'));
			}
		}
	}

	public function remover(Request $request, vara $vara, usuario_pessoa $usuario_pessoa, pedido_pessoa $pedido_pessoa, usuario_vara $usuario_vara, pessoa $pessoa) {
    	if ($request->id_vara>0) {
			$vara = $vara->find($request->id_vara);
	
			$total_pedidos = $pedido_pessoa->where('id_pessoa',$vara->id_pessoa)->count();
			$total_usuarios = $usuario_pessoa->where('id_pessoa',$vara->id_pessoa)->count();
			
			if ($total_pedidos>0) {
				return response()->json(array('status'=> 'alerta',
											  'recarrega' => 'false',
											  'msg' => 'Operação não permitida. '.($total_pedidos>1?'Existem '.$total_pedidos.' pedidos vinculados a esta vara.':'Existe 1 pedido vinculado a esta vara.')));
			} elseif ($total_usuarios>0) {
				return response()->json(array('status'=> 'alerta',
											  'recarrega' => 'false',
											  'msg' => 'Operação não permitida. '.($total_usuarios>1?'Existem '.$total_usuarios.' usuários vinculados a esta vara.':'Existe 1 usuário vinculado a esta vara.')));
			} else {
				$usuario_vara->where('id_vara',$vara->id_vara)->delete();
				if ($vara->delete()) {
					$pessoa->where('id_pessoa',$vara->id_pessoa)->delete();
					return response()->json(array('status'=> 'sucesso', 
												  'recarrega' => 'true',
												  'msg' => 'A vara foi removida com sucesso.'));
				} else {
					return response()->json(array('status'=> 'erro',
												  'recarrega' => 'false',
												  'msg' => 'Por favor, tente novamente mais tarde. Erro CVA004'));
				}
			}
		}
	}

	public function listarVaras(Request $request) {
		if ($request->id_comarca>0) {
			$varas = $this->vara->where('id_comarca',$request->id_comarca)->get();
		} else {
			$varas = $this->vara->get();
		}
		return response()->json($varas);
	}
}
