<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;
use DB;
use XMLReader;
use DOMDocument;
use File;
use Carbon\Carbon;
use Image;

use App\matricula;
use App\matricula_digital;

class MatriculaController extends Controller {

	public function __construct(matricula $matricula) {
		$this->matricula=$matricula;
	}

	public function total(Request $request) {
		$matriculas = $this->matricula->where('codigo_matricula',$request->codigo_matricula)->count();
		return $matriculas;
	}

	public function detalhesJSON(Request $request) {
		if ($matricula = $this->matricula->where('codigo_matricula',$request->codigo_matricula)->first()) {
			return response()->json($matricula);
		} else {
			return 'ERRO';
		}
	}

	public function carregar_imagens(matricula $matricula_obj, matricula_digital $matricula_digital) {
		/*$fn = '../downloads/matriculas/100017/100017_3563_99897_digl2_100_tif.tif';
		echo $fn.'<br />';
		$img = Image::make($fn);

		var_dump(verifica_img_corrompida($fn)); die();*/

		list($useg, $seg) = explode(' ', microtime());
		$tempo_inicial = (float)$seg + (float)$useg;

		$pasta = '../downloads/matriculas/';
		$pasta_inexistentes = '../downloads/matriculas_inexistentes/';

		$storage = '../storage/app/matriculas/';
		$destino_ano = Carbon::now()->format('Y-m');
		if (!File::isDirectory($storage.$destino_ano)) {
			File::makeDirectory($storage.$destino_ano);
		}

		$destino_dia = Carbon::now()->format('d');
		if (!File::isDirectory($storage.$destino_ano.'/'.$destino_dia)) {
			File::makeDirectory($storage.$destino_ano.'/'.$destino_dia);
		}
		$destino = $storage.$destino_ano.'/'.$destino_dia;

		$matriculas = scandir($pasta);
		if (count($matriculas)>2) {
			$total = (count($matriculas)>102?102:count($matriculas));
			$contador_imagens = 0;
			$contador_corrompidas = 0;
			$log_registro = '';
			$log_erro = '';
			for ($i=2; $i<$total; $i++) {
				$matricula_atual = $matriculas[$i];
				$matricula = $matricula_obj->where('codigo_matricula',$matricula_atual)->first();
				if ($matricula) {
					if (!File::isDirectory($storage.$destino_ano.'/'.$destino_dia.'/'.$matricula_atual)) {
						File::makeDirectory($storage.$destino_ano.'/'.$destino_dia.'/'.$matricula_atual);
					}
					$destino_final = $destino.'/'.$matricula_atual.'/';
					$imagens = scandir($pasta.$matricula_atual);
	
					$total_imagens = 0;
					$total_corrompidas = 0;
					foreach ($imagens as $arquivo) {
						$total_arquivo = new matricula_digital();
						$total_arquivo = $total_arquivo->where('id_matricula',$matricula->id_matricula)
									  				   ->where('no_arquivo',$arquivo)
									  				   ->count();
									  
						$arquivo_atual = $pasta.$matricula_atual.'/'.$arquivo;
						if ($total_arquivo<=0) {
							if (is_file($arquivo_atual) and filesize($arquivo_atual)>0) {
								$nova_matricula_digital = new matricula_digital();
								$nova_matricula_digital->id_matricula = $matricula->id_matricula;
								$nova_matricula_digital->no_arquivo = $arquivo;
								$nova_matricula_digital->no_local_arquivo = '/matriculas/'.$destino_ano.'/'.$destino_dia.'/'.$matricula_atual;
								$nova_matricula_digital->nu_tamanho_kb = filesize($arquivo_atual);
								$nova_matricula_digital->no_extensao_arquivo = pathinfo($arquivo_atual, PATHINFO_EXTENSION);
								$nova_matricula_digital->dt_inclusao = Carbon::now();
								$nova_matricula_digital->id_usuario_cad = 1;
								if (verifica_img_corrompida($arquivo_atual) == true) {
									$nova_matricula_digital->in_corrompido = 'S';
									$total_corrompidas++;
								}
								$nova_matricula_digital->save();
								
								rename($arquivo_atual,$destino_final.$arquivo);
								
								$total_imagens++;
							} elseif (!in_array($arquivo,array('.','..'))) {
								unlink($arquivo_atual);
							}
						} else {
							unlink($arquivo_atual);
							$total_imagens++;
						}
					}
					if ($total_imagens>0) {
						rmdir($pasta.$matricula_atual);
						//rename($pasta.$matricula_atual,$destino.'/'.$matricula_atual);
						$log_registro .= "[".date('d/m/Y H:i:s')."] ".$total_imagens." imagens foram processadas na matrícula ".$matricula_atual.". ".$total_corrompidas." está(ão) corrompida(s).".PHP_EOL;
						$contador_imagens += $total_imagens;
						$contador_corrompidas += $total_corrompidas;
					} else {
						rmdir($pasta.$matricula_atual);
						$log_erro .= "[".date('d/m/Y H:i:s')."] A pasta da matrícula ".$matricula_atual." foi removida por não conter arquivos.".PHP_EOL;
					}
				} else {
					rename($pasta.$matricula_atual,$pasta_inexistentes.$matricula_atual);
					$log_erro .= "[".date('d/m/Y H:i:s')."] A pasta da matrícula ".$matricula_atual." foi descartada por não existir no banco de dados.".PHP_EOL;
				}
			}
	
			// Logs
			$arq_log_registro = fopen($pasta.'../logs_matriculas/processadas.txt','a+b');
			fwrite($arq_log_registro,$log_registro);
			fclose($arq_log_registro);
	
			$arq_log_erro = fopen($pasta.'../logs_matriculas/erros.txt','a+b');
			fwrite($arq_log_erro,$log_erro);
			fclose($arq_log_erro);
			
			list($useg, $seg) = explode(' ', microtime());
			$tempo_final = (float) $seg + (float) $useg;
			$tempo_total = round($tempo_final - $tempo_inicial, 0);
			
			$log_geral = '['.date('d/m/Y H:i:s').'] '.($total-2).' matriculas foram processadas em '.$tempo_total.' segundos [Arquivos: '.$contador_imagens.'] [Corrompidos: '.$contador_corrompidas.'].'.PHP_EOL;
			$arq_log_geral = fopen($pasta.'../logs_matriculas/geral.txt','a+b');
			fwrite($arq_log_geral,$log_geral);
			fclose($arq_log_geral);
	
			echo $log_geral;
		}
	}
}