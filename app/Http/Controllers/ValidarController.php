<?php
namespace App\Http\Controllers;

use App\alienacao;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Validator;
use Auth;
use Hash;
use Mail;
use Storage;
use Session;
use PDF;
use URL;

use App\pedido;
use App\arquivo_grupo_produto;

class ValidarController extends Controller {

    const ID_GRUPO_PRODUTO = 2;
    const ID_SITUACAO_FINALIZADO = 17;

	public function index() {
        $id_grupo_produto = $this::ID_GRUPO_PRODUTO;
		return view('validar.validar', compact('id_grupo_produto'));
	}

	public function validar(Request $request, pedido $pedido) {

	    if ($request->protocolo_pedido != '') {
			$pedido = $pedido->join('produto',function($join) {
                                $join->on('produto.id_produto','=','pedido.id_produto')
                                     ->where('produto.id_grupo_produto','=',$this::ID_GRUPO_PRODUTO);
                             })
                             ->where('protocolo_pedido',$request->protocolo_pedido)->first();

            if ($pedido) {
                if ($pedido->id_situacao_pedido_grupo_produto == $this::ID_SITUACAO_FINALIZADO) {
        			$arquivos_resultado = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',21)->get();

                    $hash_arquivos = array();

                    if (count($arquivos_resultado)) {
                        foreach ($arquivos_resultado as $arquivo) {
                            $hash = str_random(32);
                            $hash_arquivos[$hash] = $arquivo->id_arquivo_grupo_produto;
                        }
                        Session::flash('hash_arquivos', $hash_arquivos);
                    }

        			return response()->json(array('status' => 'sucesso',
                                                  'view'=>view('validar.certidao-resultado',compact('class','pedido','arquivos_resultado','hash_arquivos'))->render()));
                } else {
                    return response()->json(array('status' => 'erro',
                                                  'msg'=>'A certidão ainda não foi finalizada.'));
                }
            } else {
                return response()->json(array('status' => 'alert',
                                              'msg'=>'O protocolo digitado é inválido.'));
            }
		}
	}

	public function imprimir_resultado(Request $request, pedido $pedido) {
        if ($request->id_pedido>0) {
            $pedido = $pedido->find($request->id_pedido);

            $hash_pedido = str_random(32);
			Session::flash('hash_pedido', $hash_pedido);
			Session::flash($hash_pedido, $pedido->id_pedido);

            return view('validar.certidao-imprimir-resultado',compact('hash_pedido'));
        }
    }

    public function render_resultado(Request $request, pedido $pedido) {
    	$id_pedido = Session::get($request->hash_pedido);
        if ($id_pedido>0) {
            $pedido = $pedido->find($id_pedido);
            
            $titulo = 'Resultado da Certidão: '.$pedido->protocolo_pedido;
            $pdf = PDF::loadView('pdf.certidao-resultado', compact('pedido', 'titulo'));   
            
            return $pdf->stream();
        }
    }

    public function visualizar_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
        if ($request->hash_arquivo!='') {
            $hash_arquivos = Session::get('hash_arquivos');
            $hash_arquivo = $request->hash_arquivo;

            Session::flash('hash_arquivos', $hash_arquivos);

            $arquivo_grupo_produto = $arquivo_grupo_produto->find($hash_arquivos[$hash_arquivo]);

            if ($arquivo_grupo_produto) {
                $url_download = URL::to('/validar/visualizar-arquivo/download/'.$hash_arquivo);

                return response()->json(array('status' => 'sucesso',
                                              'view'=>view('validar.arquivos-visualizar',compact('arquivo_grupo_produto','hash_arquivo'))->render(),
                                              'download'=>'true',
                                              'url_download'=>$url_download));
            } else {
                return response()->json(array('status' => 'erro',
                                              'recarrega' => 'false',
                                              'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
            }
        }
    }

    public function render_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
        if ($request->hash_arquivo != '') {
            $hash_arquivos = Session::get('hash_arquivos');
            $hash_arquivo = $request->hash_arquivo;

            Session::flash('hash_arquivos', $hash_arquivos);

            $arquivo_grupo_produto = $arquivo_grupo_produto->find($hash_arquivos[$hash_arquivo]);

            $arquivo = Storage::get('/public/' . $arquivo_grupo_produto->no_local_arquivo . '/' . $arquivo_grupo_produto->no_arquivo);
            
            $mimes = array('pdf' => 'application/pdf',
                           'jpg' => 'image/jpeg',
                           'png' => 'image/png',
                           'bmp' => 'image/bmp',
                           'gif' => 'image/gif');
                           
            return response($arquivo, 200)->header('Content-Type', $mimes[$arquivo_grupo_produto->no_extensao])
                                          ->header('Content-Disposition', 'inline; filename="' . $arquivo_grupo_produto->no_arquivo . '"');
        }
    }

    public function download_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
        if ($request->hash_arquivo!='') {
            $hash_arquivos = Session::get('hash_arquivos');
            $hash_arquivo = $request->hash_arquivo;

            Session::flash('hash_arquivos', $hash_arquivos);

            $arquivo_grupo_produto = $arquivo_grupo_produto->find($hash_arquivos[$hash_arquivo]);

            if ($arquivo_grupo_produto) {
                $no_arquivo = $arquivo_grupo_produto->no_arquivo;
                
                $arquivo = Storage::get('/public/'.$arquivo_grupo_produto->no_local_arquivo.'/'.$no_arquivo);
                return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo.'"');
            }
        }
    }

    public function certificado_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
        if ($request->hash_arquivo!='') {
            $hash_arquivos = Session::get('hash_arquivos');
            $hash_arquivo = $request->hash_arquivo;

            Session::flash('hash_arquivos', $hash_arquivos);

            $arquivo_grupo_produto = $arquivo_grupo_produto->find($hash_arquivos[$hash_arquivo]);

            return view('arquivos.arquivos-certificado',compact('arquivo_grupo_produto'));
        }
    }

    public function validar_carta_intimacao(Request $request, alienacao $alienacao, arquivo_grupo_produto $arquivo_grupo_produto) {
        if ($request->isMethod('post')) {
            if ($request->protocolo_pedido_arquivo != '') {
                $arquivo_grupo_produto = $arquivo_grupo_produto->join('andamento_arquivo_grupo as aag','aag.id_arquivo_grupo_produto','=','arquivo_grupo_produto.id_arquivo_grupo_produto')
                                                               ->join('andamento_alienacao as aa','aa.id_andamento_alienacao', '=','aag.id_andamento_alienacao')
                                                               ->join('alienacao_pedido as ap','ap.id_alienacao_pedido','=','aa.id_alienacao_pedido')
                                                               ->join('alienacao as a', 'a.id_alienacao','=','ap.id_alienacao')
                                                               ->join('alienacao_devedor_devedor as add','add.id_alienacao','=','a.id_alienacao')
                                                               ->join('alienacao_devedor as ad','ad.id_alienacao_devedor','=','add.id_alienacao_devedor')
                                                               ->where('protocolo_pedido_arquivo', $request->protocolo_pedido_arquivo)
                                                               ->get();

                if (count($arquivo_grupo_produto) > 0) {
                    $hash_arquivos = array();
                    $hash_arquivo = str_random(32);
                    $hash_arquivos[$hash_arquivo] = $arquivo_grupo_produto[0]->id_arquivo_grupo_produto;
                    Session::flash('hash_arquivos', $hash_arquivos);

                    return response()->json(array('status' => 'sucesso',
                                                  'view' => view('validar.carta-intimacao.carta-intimacao-resultado', compact('request', 'alienacao', 'arquivo_grupo_produto', 'hash_arquivo'))->render()));
                } else {
                    return response()->json(array('status' => 'erro',
                                                  'msg' => 'O protocolo digitado é inválido.'
                    ));
                }
            }
        }

        return view('validar.carta-intimacao.validar-carta-intimacao');
    }

}
