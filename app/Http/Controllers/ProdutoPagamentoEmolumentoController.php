<?php
namespace App\Http\Controllers;


use App\grupo_produto;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\movimentacao_parcela_repasse;
use App\pagamento_repasse_lote;
use App\pagamento_repasse_parcela;
use App\pagamento_repasse_pessoa_lote;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use Image;
use Session;
use App\estado;
use App\situacao_pedido_grupo_produto;


class ProdutoPagamentoEmolumentoController extends Controller {

    const ID_PRODUTO                                                = 1;
    const ID_GRUPO_PRODUTO                                          = 1;
	const ID_TIPO_PESSOA_CARTORIO                                   = 2;
    const ID_TIPO_PESSOA_ANOREG                                     = 9;
    const ID_REPASSE_ANOREG_SERVENTIA                               = 4;
    const ID_REPASSE_ANOREG_TAXA_SERVICO                            = 3;
    const ID_AGUARDANDO_COMPROVANTE                                 = 1;
    const ID_AGUARDANDO_CONFIRMACAO                                 = 2;
    const ID_PAGAMENTO_EFETUADO                                     = 4;
    const ID_PRODUTO_PESQUISA                                       = 1;
    const ID_SITUACAO_PEDIDO_GRUPO_PRODUTO_PESQ_FINALIZADO          = 7;
    const ID_PRODUTO_CERTIDAO                                       = 2;
    const ID_SITUACAO_PEDIDO_GRUPO_PRODUTO_CERTIDAO_FINALIZADO      = 17;
    const ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP                     = 1;
    const ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF                     = 2;
    const ID_SITUACAO_PAGAMENTO_NAOAPROVADO                         = 3;
    const ID_SITUACAO_PAGAMENTO_APROVADO                            = 4;

	public function __construct(CeriFuncoes $ceriFuncoes){

		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote, pagamento_repasse_lote $pagamento_repasse_lote, estado $estado){
	    $class = $this;

        switch ($this->id_tipo_pessoa) {
            case 9://anoreg
                $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

                $pagamentos_pendentes = $pagamento_repasse_pessoa_lote->select('pagamento_repasse_pessoa_lote.*')
                                                                      ->join('pagamento_repasse_lote', 'pagamento_repasse_lote.id_pagamento_repasse_lote', '=', 'pagamento_repasse_pessoa_lote.id_pagamento_repasse_lote')
                                                                      ->join('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'pagamento_repasse_pessoa_lote.id_pessoa')
                                                                      ->join('endereco', 'endereco.id_endereco', '=', 'pessoa_endereco.id_endereco')
                                                                      ->join('cidade', 'cidade.id_cidade', '=', 'endereco.id_cidade')
                                                                      ->join('serventia', 'serventia.id_pessoa', '=', 'pagamento_repasse_pessoa_lote.id_pessoa')
                                                                      ->where('pagamento_repasse_lote.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                      ->whereIn('pagamento_repasse_pessoa_lote.id_situacao_pagamento_repasse', [$this::ID_AGUARDANDO_COMPROVANTE,$this::ID_AGUARDANDO_CONFIRMACAO])
                                                                      ->orderBy('cidade.no_cidade')
                                                                      ->orderBy('serventia.no_serventia')
                                                                      ->orderBy('pagamento_repasse_pessoa_lote.dt_repasse','desc')
                                                                      ->get();

                $pagamentos_efetuados = $pagamento_repasse_pessoa_lote->select('pagamento_repasse_pessoa_lote.*')
                                                                      ->join('pagamento_repasse_lote', 'pagamento_repasse_lote.id_pagamento_repasse_lote', '=', 'pagamento_repasse_pessoa_lote.id_pagamento_repasse_lote')
                                                                      ->join('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'pagamento_repasse_pessoa_lote.id_pessoa')
                                                                      ->join('endereco', 'endereco.id_endereco', '=', 'pessoa_endereco.id_endereco')
                                                                      ->join('cidade', 'cidade.id_cidade', '=', 'endereco.id_cidade')
                                                                      ->join('serventia', 'serventia.id_pessoa', '=', 'pagamento_repasse_pessoa_lote.id_pessoa')
                                                                      ->where('pagamento_repasse_lote.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                      ->where('pagamento_repasse_pessoa_lote.id_situacao_pagamento_repasse', $this::ID_PAGAMENTO_EFETUADO)
                                                                      ->orderBy('cidade.no_cidade')
                                                                      ->orderBy('serventia.no_serventia')
                                                                      ->orderBy('pagamento_repasse_pessoa_lote.dt_repasse', 'desc');

                if ($request->cidade_pagamentos > 0){
                    $pagamentos_efetuados->where('cidade.id_cidade', $request->cidade_pagamentos);
                }
                if ($request->serventia_pagamentos > 0){
                    $pagamentos_efetuados->where('serventia.id_serventia', $request->serventia_pagamentos);
                }
                if (($request->dt_ini_pagamentos != '') && ($request->dt_fim_pagamentos != '')){
                    $dt_ini_pagamentos = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ini_pagamentos . ' 00:00:00');
                    $dt_fim_pagamentos = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_fim_pagamentos . ' 23:59:59');
                    $pagamentos_efetuados->whereBetween('pagamento_repasse_pessoa_lote.dt_repasse', array($dt_ini_pagamentos, $dt_fim_pagamentos));
                }

                $pagamentos_efetuados = $pagamentos_efetuados->paginate(10, ['*'], 'pagamentos-efetuados-pag');
                $pagamentos_efetuados->appends(Request::capture()->except('_token'))->render();

                $lote_pagamentos_efetuados = $pagamento_repasse_lote->select(
                                                                              'prl.id_pagamento_repasse_lote',
                                                                              'prl.protocolo_pedido',
                                                                              'prl.dt_lote',
                                                                              'prl.va_repasse_lote',
                                                                              'prl.nu_quantidade_lote',
                                                                              'prl.numero_lote',
                                                                              'u.no_usuario'
                                                                            )
                                                                            ->from('pagamento_repasse_lote as prl')
                                                                            ->join('usuario as u','u.id_usuario', '=', 'prl.id_usuario_lote')
                                                                            ->where('prl.id_repasse', 4)
                                                                            ->orderBy('prl.dt_lote', 'desc');

                $lote_pagamentos_efetuados = $lote_pagamentos_efetuados->paginate(10, 'lote-pag-efetuados-pag');
                $lote_pagamentos_efetuados->appends(Request::capture()->except('_token'))->render();

                return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento',compact('this','class','request','pagamentos_efetuados','pagamentos_pendentes','lote_pagamentos_efetuados', 'cidades'));
                break;
			case 2: case 10:
                $pagamentos_pendentes = $pagamento_repasse_pessoa_lote->select('pagamento_repasse_pessoa_lote.*')
                                                                      ->join('pagamento_repasse_lote', 'pagamento_repasse_lote.id_pagamento_repasse_lote', '=', 'pagamento_repasse_pessoa_lote.id_pagamento_repasse_lote')
                                                                      ->where('pagamento_repasse_pessoa_lote.id_pessoa', $this->id_pessoa)
                                                                      ->where('pagamento_repasse_lote.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                      ->where('pagamento_repasse_pessoa_lote.id_situacao_pagamento_repasse', $this::ID_AGUARDANDO_CONFIRMACAO)
                                                                      ->orderBy('pagamento_repasse_pessoa_lote.dt_repasse', 'desc')
                                                                      ->get();


                $pagamentos_aprovados = $pagamento_repasse_pessoa_lote->select('pagamento_repasse_pessoa_lote.*')
                                                                      ->join('pagamento_repasse_lote', 'pagamento_repasse_lote.id_pagamento_repasse_lote', '=', 'pagamento_repasse_pessoa_lote.id_pagamento_repasse_lote')
                                                                      ->where('id_pessoa', $this->id_pessoa)
                                                                      ->where('pagamento_repasse_lote.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                      ->where('pagamento_repasse_pessoa_lote.id_situacao_pagamento_repasse', $this::ID_SITUACAO_PAGAMENTO_APROVADO)
                                                                      ->orderBy('dt_repasse', 'desc')
                                                                      ->get();

                $pagamentos_receber = $this->getRepasseAnoregServentia();
                $pagamentos_receber = $pagamentos_receber->where('pedido_pessoa.id_pessoa', $this->id_pessoa)
                                                         ->orderBy('movimentacao_parcela.dt_cadastro','desc')->get();

		    	return view('servicos.produto-pagamento-emolumento.serventia-produto-pagamento',compact('this','class','request','pagamentos_aprovados','pagamentos_pendentes', 'pagamentos_receber'));
				break;
		}
    }
	
	public function novo(Request $request, movimentacao_parcela_repasse $movimentacao_parcela_repasse, estado $estado, situacao_pedido_grupo_produto $situacao, grupo_produto $grupo_produto)
    {
        $cidades   = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        $grupo_produtos  = $grupo_produto->whereIn('id_grupo_produto', [1,2])->orderBy('grupo_produto.no_grupo_produto')->get();

        if ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG) {
            $movimentacao_parcela_repasse = $this->getRepasseAnoregServentia();
        }

		if ($request->isMethod('post'))
		{
			if ($request->dt_inicio!='' and $request->dt_fim!='') {
				$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
				$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                $movimentacao_parcela_repasse->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
			}
			if ($request->id_situacao_pedido_grupo_produto>0) {
                $movimentacao_parcela_repasse->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
			}
			if ($request->protocolo_pedido > 0){
                $movimentacao_parcela_repasse->where('pedido.protocolo_pedido',$request->protocolo_pedido);
			}


            if ($request->id_serventia>0)
            {
                $movimentacao_parcela_repasse->where('serventia.id_serventia','=',$request->id_serventia);
            }

            if ($request->id_grupo_produto>0)
            {
                $movimentacao_parcela_repasse->where('produto.id_grupo_produto','=',$request->id_grupo_produto);
            }

		}

        $movimentacoes_parcela_repasse = $movimentacao_parcela_repasse->orderBy('movimentacao_parcela.dt_cadastro','desc')->get();

		if ($request->acao == "pesquisar") {
            return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-historico', compact('request', 'movimentacoes_parcela_repasse', 'cidades', 'situacoes', 'grupo_produtos'));
        }else{
            return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-novo', compact('request', 'movimentacoes_parcela_repasse', 'cidades', 'situacoes', 'grupo_produtos'));
        }
    }
	
	public function inserir_pagamento(Request $request, pagamento_repasse_lote $pagamento_repasse_lote) {
        $erro           = 0;
        $erro_loop      = false;

		DB::beginTransaction();

        if (count($request->id_movimentacao_parcela_repasse)>0)
        {

            $va_geral_parcela_pedido     = 0;
            $va_geral_parcela_repasse    = 0;
            $nu_geral_quantidade         = 0;

            $protocoloPedido                                        = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));
            $pagamento_repasse_lote->id_situacao_pagamento_repasse  = $this::ID_SITUACAO_PAGAMENTO_APROVADO;
            $pagamento_repasse_lote->id_repasse                     = $this::ID_REPASSE_ANOREG_SERVENTIA;
            $pagamento_repasse_lote->protocolo_pedido               = $protocoloPedido[0]->f_geraprotocolo;
            $pagamento_repasse_lote->dt_lote                        = Carbon::now();
            $pagamento_repasse_lote->numero_lote                    = 0;
            $pagamento_repasse_lote->nu_quantidade_lote             = 0;
            $pagamento_repasse_lote->va_parcela_pedido              = 0;
            $pagamento_repasse_lote->va_repasse_lote                = 0;
            $pagamento_repasse_lote->id_usuario_lote                = Auth::User()->id_usuario;
            $pagamento_repasse_lote->id_usuario_cad                 = Auth::User()->id_usuario;
            $pagamento_repasse_lote->dt_cadastro                    = Carbon::now();

            if (!$pagamento_repasse_lote->save()){
                $erro_loop = true;
            }

            foreach ($request->id_movimentacao_parcela_repasse as $id_pessoa_serventia => $ids_movimentacao_parcela_repasse)
            {
                $pagamentos_repasse_pessoa_lote                                 = new pagamento_repasse_pessoa_lote();
                $pagamentos_repasse_pessoa_lote->id_pagamento_repasse_lote      = $pagamento_repasse_lote->id_pagamento_repasse_lote;
                $pagamentos_repasse_pessoa_lote->id_pessoa                      = $id_pessoa_serventia;
                $pagamentos_repasse_pessoa_lote->protocolo_pedido               = $protocoloPedido[0]->f_geraprotocolo;
                $pagamentos_repasse_pessoa_lote->id_situacao_pagamento_repasse  = 1; //Aguardando comprovante id_situacao_pagamento_repasse
                $pagamentos_repasse_pessoa_lote->va_parcela_pedido              = 0;
                $pagamentos_repasse_pessoa_lote->va_repasse                     = 0;
                $pagamentos_repasse_pessoa_lote->dt_repasse                     = Carbon::now();
                $pagamentos_repasse_pessoa_lote->nu_quantidade                  = 0;
                $pagamentos_repasse_pessoa_lote->id_usuario_cad                 = Auth::User()->id_usuario;

                if (!$pagamentos_repasse_pessoa_lote->save())
                {
                    $erro_loop = true;
                }


                $va_parcela_pedido      = 0;
                $va_parcela_repasse     = 0;
                $nu_quantidade_lote     = 0;
                $arrayProdutoRepasse    = [];

                foreach ($ids_movimentacao_parcela_repasse as $id_movimentacao_parcela_repasse => $valores_repasse)
                {
                    foreach ($valores_repasse as $valor_repasse => $valores_pedido)
                    {
                        foreach ($valores_pedido as $valor_pedido => $id_movimentacao_parcela_repasse_final)
                        {
                            $pagamentos_repasse_parcela = new pagamento_repasse_parcela();

                            $pagamentos_repasse_parcela->id_movimentacao_parcela_repasse    = $id_movimentacao_parcela_repasse;
                            $pagamentos_repasse_parcela->id_pagamento_repasse_pessoa_lote   = $pagamentos_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote;
                            $pagamentos_repasse_parcela->dt_cadastro                        = Carbon::now();

                            if (!$pagamentos_repasse_parcela->save())
                            {
                                $erro_loop = true;
                            }

                            //guardando informacoes para atualizar tabelas acima
                            $va_parcela_pedido        += $valor_pedido;
                            $va_parcela_repasse       += $valor_repasse;
                            $arrayProdutoRepasse[]     = $id_movimentacao_parcela_repasse;
                            $nu_quantidade_lote++;

                            //UPDATE movimentacao_parcela_repasse
                            $atualizar_movimentacao_parcela_repasse = new movimentacao_parcela_repasse();

                            $atualizar_movimentacao_parcela_repasse                         = $atualizar_movimentacao_parcela_repasse->find($id_movimentacao_parcela_repasse);
                            $atualizar_movimentacao_parcela_repasse->in_repasse_aberto      = 'N';
                            $atualizar_movimentacao_parcela_repasse->dt_repasse             =  Carbon::now();
                            $atualizar_movimentacao_parcela_repasse->id_usuario_repasse     =  Auth::User()->id_usuario;

                            if (!$atualizar_movimentacao_parcela_repasse->save()) {
                                DB::rollback();
                                throw new Exception('Erro ao atualizar parcela.');
                            }

                        }
                    }
                }

                //UPDATE table $pagamentos_repasse_pessoa_lote
                $pagamentos_repasse_pessoa_lote->va_parcela_pedido              = $va_parcela_pedido;
                $pagamentos_repasse_pessoa_lote->va_repasse                     = $va_parcela_repasse;
                $pagamentos_repasse_pessoa_lote->nu_quantidade                  = $nu_quantidade_lote;

                if (!$pagamentos_repasse_pessoa_lote->save())
                {
                    $erro_loop = true;
                }


                //GUARDANDO INFORMACOES PARA ATUALIZAR PRIMEIRA TABELA
                $va_geral_parcela_pedido    += $va_parcela_pedido;
                $va_geral_parcela_repasse   += $va_parcela_repasse;
                $nu_geral_quantidade        += $nu_quantidade_lote;

            }

            //UPDATE  tabela $pagamento_repasse_lote
            $pagamento_repasse_lote->va_parcela_pedido   = $va_geral_parcela_pedido;
            $pagamento_repasse_lote->va_repasse_lote     = $va_geral_parcela_repasse;
            $pagamento_repasse_lote->nu_quantidade_lote  = $nu_geral_quantidade;
            $pagamento_repasse_lote->numero_lote         = $pagamento_repasse_lote->id_pagamento_repasse_lote;

            if (!$pagamento_repasse_lote->save())
            {
                $erro_loop = true;
            }


            //UPDATE movimentacao_parcela_repasse
            /*$atualizar_movimentacao_parcela_repasse = new movimentacao_parcela_repasse();

            $retornUpdateMovParcela = $atualizar_movimentacao_parcela_repasse
                                                ->whereIn('id_movimentacao_parcela_repasse', $arrayProdutoRepasse)
                                                ->update([
                                                            'in_repasse_aberto'    =>  'N',
                                                            'dt_repasse'           =>  Carbon::now(),
                                                            'id_usuario_repasse'   => Auth::User()->id_usuario,
                                                    ]);

            if ($retornUpdateMovParcela==false)
            {
                $erro_loop = true;
            }*/


        }

        //controle da variavel de loop/insert
        if ($erro_loop) {
            $erro = 2;
        }
		
		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O pagamento foi inserido com sucesso.'));
		}
	}

    public function detalhes(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote) {
        if ($request->id_pagamento_repasse_pessoa_lote>0) {

            $pagamentos_repasse_pessoa_lote = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);

            //FAZER SELECT DE DETALHE DE PAGAMENTO

            $id_pagamento_repasse_pessoa_lote =  $request->id_pagamento_repasse_pessoa_lote;
            $pagamento_repasse_pessoa_lote_token = str_random('30');

            $class = $this;

            return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-detalhes',compact('request','pagamentos_repasse_pessoa_lote', 'id_pagamento_repasse_pessoa_lote', 'pagamento_repasse_pessoa_lote_token', 'class'));
        }
    }
	
	public function novo_comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote)
    {
        if ($request->id_pagamento_repasse_pessoa_lote>0)
        {
            $pagamentos_repasse_pessoa_lote = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);
            $id_pagamento_repasse_pessoa_lote =  $request->id_pagamento_repasse_pessoa_lote;
            return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-novo-comprovante',compact('request','pagamentos_repasse_pessoa_lote', 'id_pagamento_repasse_pessoa_lote'));
        }

    }


	public function inserir_comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote) {
		if ($request->id_pagamento_repasse_pessoa_lote>0) {
            $erro = 0;

            DB::beginTransaction();

            $arquivo        = $request->no_arquivo_comprovante;
            $nome_arquivo   = strtolower(remove_caracteres($arquivo->getClientOriginalName()));

            $pagamentos_repasse_pessoa_lote                                     = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);
            $pagamentos_repasse_pessoa_lote->id_usuario_comprovante_pagamento   = Auth::User()->id_usuario;
            $pagamentos_repasse_pessoa_lote->dt_comprovante_repasse             = Carbon::createFromFormat('d/m/Y', $request->dt_pagamento);
            $pagamentos_repasse_pessoa_lote->dt_situacao_pagamento_repasse      = Carbon::now();
            $pagamentos_repasse_pessoa_lote->no_arquivo_comprovante             = $nome_arquivo;
            $pagamentos_repasse_pessoa_lote->no_local_arquivo_comprovante       = 'produto-pagamentos/' . $pagamentos_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote;
            $pagamentos_repasse_pessoa_lote->no_extensao_comprovante            = $request->no_arquivo_comprovante->getClientOriginalExtension();
            $pagamentos_repasse_pessoa_lote->id_situacao_pagamento_repasse      = $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF;

            if ($pagamentos_repasse_pessoa_lote->save())
            {
                $destino = '/public/produto-pagamentos/' . $pagamentos_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote;

                Storage::makeDirectory($destino);

                if (!$arquivo->move(storage_path('app'.$destino), $nome_arquivo))
                {
                    $erro = 1;
                }

            } else {
                $erro = 1;
            }

           /* //inserindo notificacao
            if (count($alienacao_pagamento->alienacoes) > 0)
            {
                foreach ($alienacao_pagamento->alienacoes as $alienacao)
                {
                    $pedido                     = new pedido();
                    $pedido_selecionado         = $pedido->find($alienacao->alienacao_pedido->pedido->id_pedido);
                    $pedido_selecionado->inserir_notificacao($this->id_pessoa,$pedido_selecionado->pedido_pessoa_atual->id_pessoa,'Uma nova notificação foi enviada. Protocolo '.$pedido_selecionado->protocolo_pedido);

                }
			}*/
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O comprovante foi salvo com sucesso.'));
			}
		}
	}


	
	public function aprovar_pagamento(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote)
    {
        if ($request->id_pagamento_repasse_pessoa_lote>0)
        {
            $erro = 0;
            DB::beginTransaction();

            $pagamentos_repasse_pessoa_lote                                = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);
            $pagamentos_repasse_pessoa_lote->id_situacao_pagamento_repasse = $this::ID_SITUACAO_PAGAMENTO_APROVADO;
            $pagamentos_repasse_pessoa_lote->id_usuario_pagamento_aprovado = Auth::User()->id_usuario;
            $pagamentos_repasse_pessoa_lote->dt_usuario_pagamento_aprovado = Carbon::now();

            if (!$pagamentos_repasse_pessoa_lote->save()) {
                $erro++;
            }


        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                                          'recarrega' => 'false',
                                          'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            return response()->json(array('status'=> 'sucesso',
                                          'recarrega' => 'true',
                                          'msg' => 'O pagamento foi aprovado com sucesso.'));
        }
	}
	
	public function reprovar_pagamento (Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote) {
		if ($request->id_pagamento_repasse_pessoa_lote>0)
		{
			$erro = 0;
			DB::beginTransaction();

            $pagamentos_repasse_pessoa_lote                                  = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);
            $pagamentos_repasse_pessoa_lote->id_situacao_pagamento_repasse   = $this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO;

			if (!$pagamentos_repasse_pessoa_lote->save()) {
				$erro = 1;
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O pagamento foi reprovado com sucesso.'));
			}
		}
	}

	public function comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote) {
        if ($request->id_pagamento_repasse_pessoa_lote>0)
        {

            $pagamentos_repasse_pessoa_lote    = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);

			if ($pagamentos_repasse_pessoa_lote) {
				if ($pagamentos_repasse_pessoa_lote->no_arquivo_comprovante!='') {
					$url_download = URL::to('/arquivos/download/produto-pagamento-emolumento/'.$pagamentos_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote);


					return response()->json(array('status' => 'sucesso',
												  'view'=>view('servicos.produto-pagamento-emolumento.serventia-produto-pagamento-comprovante',compact('pagamentos_repasse_pessoa_lote'))->render(),
												  'download'=>'true',
												  'url_download'=>$url_download));
				} else {
					return response()->json(array('status' => 'erro',
										  		  'recarrega' => 'false',
										  		  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
				}
			} else {
				return response()->json(array('status' => 'erro',
										 	  'recarrega' => 'false',
										  	  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
			}
		}
	}
	public function arquivo_comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote) {
		if ($request->id_pagamento_repasse_pessoa_lote>0) {

		    $pagamentos_repasse_pessoa_lote  = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);

			$arquivo = Storage::get('/public/'.$pagamentos_repasse_pessoa_lote->no_local_arquivo_comprovante.'/'.$pagamentos_repasse_pessoa_lote->no_arquivo_comprovante);
			
			$mimes = array('pdf' => 'application/pdf',
						   'jpg' => 'image/jpeg',
						   'png' => 'image/png',
						   'bmp' => 'image/bmp',
						   'gif' => 'image/gif');
						   
			return response($arquivo, 200)->header('Content-Type', $mimes[$pagamentos_repasse_pessoa_lote->no_extensao_comprovante])
										  ->header('Content-Disposition', 'inline; filename="'.$pagamentos_repasse_pessoa_lote->no_arquivo_comprovante.'"');
		}
	}
	public function download_comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote) {
		if ($request->id_pagamento_repasse_pessoa_lote>0) {
            $pagamentos_repasse_pessoa_lote                                  = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);

			$no_arquivo_comprovante = $pagamentos_repasse_pessoa_lote->no_arquivo_comprovante;
			
			$arquivo = Storage::get('/public/'.$pagamentos_repasse_pessoa_lote->no_local_arquivo_comprovante.'/'.$no_arquivo_comprovante);
			return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo_comprovante.'"');
		}
	}

    public function gerar_recibo(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote)
    {
        if ($request->id_pagamento_repasse_pessoa_lote>0)
        {
            $pagamentos_repasse_pessoa_lote  = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);

            $destino    = '/produto-pagamentos/'.$pagamentos_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote;
            $no_arquivo = 'recibo_pagamento_lote_'.$pagamentos_repasse_pessoa_lote->protocolo_pedido.'.pdf';

            Storage::makeDirectory('/public'.$destino);

            $titulo         = 'Recibo pagamento: '.$pagamentos_repasse_pessoa_lote->protocolo_pedido;
            $nome_projeto   = 'Emolumentos';
            $pdf        	= PDF::loadView('pdf.produto-pagamento-emolumento-recibo', compact('pagamentos_repasse_pessoa_lote', 'titulo', 'nome_projeto'));


            $pdf->save(storage_path('app/public'.$destino.'/'.$no_arquivo));

            return response()->json(array('view'=>view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-recibo',compact('pagamentos_repasse_pessoa_lote'))->render()));
        }
    }

    public function imprimir_recibo(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote)
    {
        if ($request->id_pagamento_repasse_pessoa_lote>0)
        {
            $pagamentos_repasse_pessoa_lote  = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);

            $nome_arquivo = 'recibo_pagamento_lote_'.$pagamentos_repasse_pessoa_lote->protocolo_pedido.'.pdf';

            $arquivo        = Storage::get('/public/produto-pagamentos/'.$pagamentos_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote.'/'.$nome_arquivo);
            return response($arquivo, 200)->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$nome_arquivo.'"');
        }
    }

    public function getRepasseAnoregServentia()
    {
        $movimentacao_parcela_repasse   = new movimentacao_parcela_repasse();
        $movimentacoes_parcela_repasse  = $movimentacao_parcela_repasse
                                                        ->select(
                                                            'pedido.id_pedido',
                                                            'pedido.protocolo_pedido',
                                                            'pedido.dt_pedido',
                                                            'produto.no_produto',
                                                            'cidade.no_cidade',
                                                            'pedido_pessoa.id_pessoa',
                                                            'serventia.id_pessoa',
                                                            'serventia.id_serventia',
                                                            'serventia.no_serventia',
                                                            'situacao_pedido_grupo_produto.id_grupo_produto',
                                                            'situacao_pedido_grupo_produto.no_situacao_pedido_grupo_produto',
                                                            'movimentacao_parcela.va_parcela as va_total_pedido',
                                                            'movimentacao_parcela_repasse.id_movimentacao_parcela_repasse',
                                                            'movimentacao_parcela_repasse.va_repasse as va_total_repasse'
                                                        )
                                                        ->join('movimentacao_parcela', 'movimentacao_parcela.id_movimentacao_parcela', '=', 'movimentacao_parcela_repasse.id_movimentacao_parcela')
                                                        ->join('pedido', 'pedido.id_pedido', '=', 'movimentacao_parcela.id_pedido')
                                                        ->join('produto', 'produto.id_produto', '=', 'pedido.id_produto')
                                                        ->join('pedido_pessoa', 'pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
                                                        ->join('serventia', 'serventia.id_pessoa', '=', 'pedido_pessoa.id_pessoa')
                                                        ->join('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'serventia.id_pessoa')
                                                        ->join('endereco', 'endereco.id_endereco', '=', 'pessoa_endereco.id_endereco')
                                                        ->join('cidade', 'cidade.id_cidade', '=', 'endereco.id_cidade')
                                                        ->join('situacao_pedido_grupo_produto', 'situacao_pedido_grupo_produto.id_situacao_pedido_grupo_produto', '=', 'pedido.id_situacao_pedido_grupo_produto')
                                                        ->join('historico_pedido', function($join){
                                                            $join->on('historico_pedido.id_pedido', '=', 'pedido.id_pedido')
                                                                ->on('historico_pedido.id_situacao_pedido_grupo_produto', '=',  DB::raw('(case when "produto"."id_grupo_produto" = 2 then 16 else 2 end)'));
                                                        })
                                                        ->where('movimentacao_parcela_repasse.id_repasse', '=', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                        ->whereIn('produto.id_grupo_produto', [ $this::ID_PRODUTO_PESQUISA, $this::ID_PRODUTO_CERTIDAO ])
                                                        ->where('movimentacao_parcela_repasse.in_repasse_aberto', '=', DB::raw('\'S\''))
                                                        ->groupBy(
                                                        'pedido.id_pedido',
                                                        'pedido.protocolo_pedido',
                                                        'pedido.dt_pedido',
                                                        'produto.no_produto',
                                                        'cidade.no_cidade',
                                                        'pedido_pessoa.id_pessoa',
                                                        'serventia.id_pessoa',
                                                        'serventia.id_serventia',
                                                        'serventia.no_serventia',
                                                        'situacao_pedido_grupo_produto.id_grupo_produto',
                                                        'situacao_pedido_grupo_produto.no_situacao_pedido_grupo_produto',
                                                        'movimentacao_parcela.va_parcela',
                                                        'movimentacao_parcela_repasse.id_movimentacao_parcela_repasse',
                                                        'movimentacao_parcela_repasse.va_repasse',
                                                        'movimentacao_parcela.dt_cadastro'
                                                    );
                                                        /*->where(function($query) {
                                                            $query->where('produto.id_grupo_produto', $this::ID_PRODUTO_PESQUISA)
                                                                //->where('pedido.id_situacao_pedido_grupo_produto', $this::ID_SITUACAO_PEDIDO_GRUPO_PRODUTO_PESQ_FINALIZADO)
                                                                ->orWhere(function($query) {
                                                                    $query->where('produto.id_grupo_produto',$this::ID_PRODUTO_CERTIDAO);
                                                                        //->where('pedido.id_situacao_pedido_grupo_produto',$this::ID_SITUACAO_PEDIDO_GRUPO_PRODUTO_CERTIDAO_FINALIZADO);
                                                                });
                                                        });*/
        return $movimentacoes_parcela_repasse;
    }

    public function remover_comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote){
        $pagamento_repasse_pessoa_lote = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);
        if (Storage::delete('/public/'.$pagamento_repasse_pessoa_lote->no_local_arquivo_comprovante.'/'.$pagamento_repasse_pessoa_lote->no_arquivo_comprovante)) {
            $pagamento_repasse_pessoa_lote->no_arquivo_comprovante = '';
            $pagamento_repasse_pessoa_lote->no_local_arquivo_comprovante= '';
            $pagamento_repasse_pessoa_lote->no_extensao_comprovante = '';
            if($pagamento_repasse_pessoa_lote->save()){
                return response()->json(array('status' => 'sucesso',
                    'msg'=> 'O arquivo foi removido com sucesso',
                    'arquivo'=> $pagamento_repasse_pessoa_lote));
            } else {
                return response()->json(array('status'=> 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Não foi pssivel remover o arquivo'));
            }
        } else {
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Arquivo não encotrado.'));
        }
    }
    public function novo_arquivo_comprovante(Request $request) {
        return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-novo-arquivo-comprovante',compact('request'));
    }
    public function alterar_comprovante(Request $request, pagamento_repasse_pessoa_lote $pagamento_repasse_pessoa_lote){
        if ($request->id_pagamento_repasse_pessoa_lote > 0){
            $erro = 0;

            DB::beginTransaction();

            $nome_arquivo = remove_caracteres($request->no_arquivo->getClientOriginalName());

            $pagamento_repasse_pessoa_lote = $pagamento_repasse_pessoa_lote->find($request->id_pagamento_repasse_pessoa_lote);
            $pagamento_repasse_pessoa_lote->no_arquivo_comprovante = $nome_arquivo;
            $pagamento_repasse_pessoa_lote->no_local_arquivo_comprovante = 'produto-pagamentos/' . $pagamento_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote;
            $pagamento_repasse_pessoa_lote->no_extensao_comprovante = $request->no_arquivo->getClientOriginalExtension();


            if ($pagamento_repasse_pessoa_lote->save()) {
                $destino = '../storage/app/public/produto-pagamentos/' . $pagamento_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote;
                if (!File::isDirectory($destino)) {
                    File::makeDirectory($destino);
                }

                if (!$request->no_arquivo->move($destino, $nome_arquivo)) {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }

            if ($erro>0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();
                return response()->json(array(
                    'token' => $request->token,
                    'id_pagamento_repasse_pessoa_lote' => $pagamento_repasse_pessoa_lote->id_pagamento_repasse_pessoa_lote,
                    'no_arquivo_comprovante' => $nome_arquivo,
                    'no_extensao_comprovante'=>$pagamento_repasse_pessoa_lote->no_extensao_comprovante
                ));
            }
        }
    }

    public function detalhes_lote_pagamentos(Request $request, pagamento_repasse_lote $pagamento_repasse_lote){
        if ($request->id_pagamento_repasse_lote > 0){
            $pagamento_repasse_lote = $pagamento_repasse_lote->find($request->id_pagamento_repasse_lote);
            $va_parcela_pedido = $pagamento_repasse_lote->va_parcela_pedido;
            $lote_pagamentos_efetuados = $pagamento_repasse_lote->select(
                'pro.id_grupo_produto',
                'p.id_pedido',
                'pi.id_produto_item',
                'p.protocolo_pedido',
                'p.dt_pedido',
                'pi.no_produto_item',
                'c.no_cidade',
                's.no_serventia',
                'mpr.va_parcela_pedido',
                'mpr.va_repasse',
                DB::raw("ceri.f_retorna_valor_campo(prl.id_pagamento_repasse_lote, 'PRL', '', '','VA_TAXA_BANCARIA_OPERADORA') as va_taxa_bancaria_operadora")
            )
            ->from('pagamento_repasse_lote as prl')
		    ->join('pagamento_repasse_pessoa_lote as prpl','prpl.id_pagamento_repasse_lote','=','prl.id_pagamento_repasse_lote')
		    ->join('usuario as u','u.id_usuario','=','prl.id_usuario_lote')
            ->join('pagamento_repasse_parcela as prp','prp.id_pagamento_repasse_pessoa_lote','=','prpl.id_pagamento_repasse_pessoa_lote')
            ->join('movimentacao_parcela_repasse as mpr','mpr.id_movimentacao_parcela_repasse','=','prp.id_movimentacao_parcela_repasse')
            ->join('pedido as p','p.id_pedido','=','mpr.id_pedido')
            ->join('pedido_produto_item as ppi', 'ppi.id_pedido', '=', 'p.id_pedido')
            ->join('produto_item as pi','pi.id_produto_item','=', 'ppi.id_produto_item')
            ->join('produto as pro','pro.id_produto','=', 'pi.id_produto')
            ->join('serventia as s','s.id_pessoa','=','prpl.id_pessoa')
            ->join('pessoa_endereco as pe', 'pe.id_pessoa','=','s.id_pessoa')
            ->join('endereco as e', 'e.id_endereco','=','pe.id_endereco')
            ->join('cidade as c', 'c.id_cidade', '=','e.id_cidade')
            ->where('prl.id_pagamento_repasse_lote', $request->id_pagamento_repasse_lote)
            ->orderBy('c.no_cidade')
            ->orderBy('s.no_serventia')
            ->orderBy('p.dt_pedido', 'desc')
            ->get();

            return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-lote-detalhes', compact('request', 'pagamento_repasse_lote', 'lote_pagamentos_efetuados','va_parcela_pedido'));
        }
    }

    public function gerar_recibo_lote(Request $request){
        if($request->id_pagamento_repasse_lote > 0){
            return view('servicos.produto-pagamento-emolumento.geral-produto-pagamento-recibo-lote', compact('request'));
        }

    }

    public function render_recibo_lote(Request $request, pagamento_repasse_lote $pagamento_repasse_lote){
        if ($request->id_pagamento_repasse_lote > 0){
            $pagamento_repasse_lote = $pagamento_repasse_lote->select(
                DB::raw("s.no_serventia||' - ('||c.no_cidade||')' as no_serventia"),
                'c.no_cidade',
                'prpl.nu_quantidade',
                'prpl.va_repasse',
                'b.codigo_banco',
                'b.no_banco',
                'bp.in_tipo_pessoa_conta',
                'bp.nu_conta',
                'bp.nu_dv_conta',
                DB::raw("bp.nu_agencia||'/'||bp.nu_dv_agencia as nu_agencia_dv"),
                DB::raw("case
                            when bp.tipo_conta = 'C' then 'Corrente'
                            when bp.tipo_conta = 'P' then 'Poupança'
                        else '' end as tipo_conta"),
                'bp.nu_cpf_cnpj_conta'
                )
                ->from('pagamento_repasse_lote as prl')
                ->join('pagamento_repasse_pessoa_lote as prpl', 'prpl.id_pagamento_repasse_lote', '=', 'prl.id_pagamento_repasse_lote')
                ->join('serventia as s' , 's.id_pessoa', '=', 'prpl.id_pessoa')
                ->join('pessoa_endereco as pe', 'pe.id_pessoa', '=', 's.id_pessoa')
                ->join('endereco as e', 'e.id_endereco', '=', 'pe.id_endereco')
                ->join('cidade as c', 'c.id_cidade', '=', 'e.id_cidade')
                ->leftJoin('banco_pessoa as bp' , 'bp.id_pessoa', '=', 's.id_pessoa')
                ->leftJoin('banco as b' , 'b.id_banco', '=', 'bp.id_banco')
                ->where('prl.id_pagamento_repasse_lote', $request->id_pagamento_repasse_lote)
                ->orderBy('c.no_cidade')
                ->orderBy('s.no_serventia')
                ->get();

            $titulo = 'RECIBO - PAGAMENTO EMOLUMENTO / Serviços';

            $pdf = PDF::loadView('pdf.produto-pagamento-emolumento-recibo-lote', compact('request', 'pagamento_repasse_lote', 'titulo'));

            return $pdf->stream();
        }
    }

}