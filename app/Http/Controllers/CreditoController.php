<?php

namespace App\Http\Controllers;

use App\forma_pagamento;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\usuario;
use Illuminate\Http\Request;

use Session;
use Validator;
use Auth;
use PDF;
use DB;
use Carbon\Carbon;

use App\estado;
use App\convenio_cartao;
use App\compra_credito;
use App\credito_entrada_saida;

use Cielo\Cielo;
use Cielo\CieloException;
use Cielo\Transaction;
use Cielo\Holder;
use Cielo\PaymentMethod;

class CreditoController extends Controller
{

    const ID_CONVENIO_CARTAO = 1;
    const ID_SITUACAO_CREDITO_CANCELADO = 4;
    const ID_SITUACAO_CREDITO_EM_CANCELAMENTO = 5;
    const ID_SITUACAO_CREDITO_EM_APROVACAO = 2;
    const ID_SITUACAO_CREDITO_APROVADO = 6;
    const ID_FORMA_PAGAMENTO_CREDITO = 1;
    const ID_FORMA_PAGAMENTO_DEBITO = 2;

    private $id_compra_credito, $nu_pedido_convenio;

    public function __construct()
    {

    }

    public function index(Request $request, compra_credito $compra_credito)
    {
        $todos_credito = $compra_credito->where('id_usuario', Auth::User()->id_usuario)
                                        ->orderBy('id_compra_credito', 'desc')
                                        ->paginate(20, ['*'], 'credito-pag');

        return view('creditos.creditos', compact('todos_credito'));
    }

    public function nova_compra(Request $request, estado $estado)
    {
        $todos_estados = $estado->orderBy('no_estado')->get();
        return view('creditos.creditos-compra', compact('todos_estados'));
    }

    public function cancelar_transacao(Request $request, compra_credito $compra_credito)
    {
        $retorno = $compra_credito->where('id_usuario', Auth::User()->id_usuario)
                                    ->where('id_situacao_credito', 2)
                                    ->where('id_compra_credito', Session::get('id_compra_credito'))
                                    ->update(
                                        [
                                            'id_situacao_credito' => 4, //cancelado
                                            'id_usuario_lib' => Auth::User()->id_usuario,
                                        ]
                                    );
        //Removendo sessao
        Session::forget('id_compra_credito');
        $retorno = ($retorno == true) ? 'SUCESSO' : 'ERRO';
        return $retorno;
    }

    public function retorno_compra_debito(Request $request, convenio_cartao $convenio_cartao, compra_credito $compra_credito, credito_entrada_saida $credito_entrada_saida)
    {
        $tid = $request->Tid;
        //$tid  = '1077406182000000028A';

        if ($tid == "") {
            $this->cancelar_transacao($request, $compra_credito);

            $arrayErro = array('erro' => 'Tid não encontrado');
            return view('creditos.creditos-error', compact('arrayErro'));
        } else {
            $convenio_cielo = $convenio_cartao->find($this::ID_CONVENIO_CARTAO);
            if (env('APP_ENV') == 'production') {
                $cielo = new Cielo($convenio_cielo->nu_convenio, $convenio_cielo->no_chave, Cielo::PRODUCTION);
            } else {
                $cielo = new Cielo($convenio_cielo->nu_convenio, $convenio_cielo->no_chave, Cielo::TEST);
            }
            $consultation = $cielo->consultation($tid);
            $consultationResponse = $cielo->consultationRequest($consultation);

            $ceriFuncoes = new CeriFuncoes();
            $dadosTransacao = $ceriFuncoes->getDadosTransacaoRetorno($consultationResponse);

            if ($consultationResponse->getAuthorization()->getLR() == '00') {
                if ($dadosTransacao['status'] == 6 && $dadosTransacao['tid'] > 0) {
                    $arrayStatusTransacao = $this->retornoErroTransacao($compra_credito, $dadosTransacao['id_compra_credito'], $dadosTransacao['tid'], 6, $dadosTransacao['nu_pedido_convenio']);

                    if ($arrayStatusTransacao == true) {
                        $vl_total = ($consultationResponse->getOrder()->getTotal() / 100);
                        //INSERINDO NA TABELA DE MOVIMENTACAO
                        $credito_entrada_saida->id_compra_credito = $dadosTransacao['id_compra_credito'];
                        $credito_entrada_saida->id_tipo_credito = 1;
                        $credito_entrada_saida->tp_movimento = 'E';
                        $credito_entrada_saida->va_movimento = $vl_total;
                        $credito_entrada_saida->id_usuario = Auth::User()->id_usuario;
                        $credito_entrada_saida->dt_cadastro = Carbon::now();
                        $credito_entrada_saida->dt_movimento = Carbon::now();
                        $credito_entrada_saida->save();

                        $arraySucess = array('success' => 'Parabéns, Transação realizada com sucesso', 'valor' => $vl_total);
                        return view('creditos.creditos-success', compact('arraySucess'));

                    } else {
                        $this->retornoErroTransacao($compra_credito, $dadosTransacao['id_compra_credito'], $dadosTransacao['tid'], 4, $dadosTransacao['nu_pedido_convenio']);
                        $arrayErro = array('erro' => 'Erro ao inserir informação no banco, porém sua transação foi aprovada e seu crédito adicionado');
                        return view('creditos.creditos-error', compact('arrayErro'));
                    }
                } else {
                    $this->retornoErroTransacao($compra_credito, $dadosTransacao['id_compra_credito'], $dadosTransacao['tid'], 4, $dadosTransacao['nu_pedido_convenio']);
                    $arrayErro = array('erro' => $dadosTransacao['msg']);
                    return view('creditos.creditos-error', compact('arrayErro'));
                }
            } else {
                $this->retornoErroTransacao($compra_credito, $dadosTransacao['id_compra_credito'], $dadosTransacao['tid'], 4, $dadosTransacao['nu_pedido_convenio']);
                $arrayErro = array('erro' => $dadosTransacao['msg'], 'request' => $request->all());
                return view('creditos.creditos-error', compact('arrayErro'));

            }
        }
    }

    public function valida_transacao_debito(compra_credito $compra_credito)
    {
        $compra_credito = $compra_credito->find(Session::get('id_compra_credito'));

        if ($compra_credito->id_situacao_credito != 2) {
            return response()->json(['total'=>$this->buscar_saldo(),'id_compra_credito'=>$compra_credito->id_compra_credito]);
        }
    }


    public function inserir_compra_debito(Request $request, compra_credito $compra_credito, convenio_cartao $convenio_cartao, forma_pagamento $forma_pagamento)
    {
        $bandeiraDebito = array("visa", "mastercard");
        $erro = 0;

        if (!in_array(strtolower($request->id_bandeira), $bandeiraDebito)) {
            $erro = 20001;
        }

        $pagamento = $forma_pagamento->find($this::ID_FORMA_PAGAMENTO_DEBITO);

        #BUSCANDO AS INFORMACOES DE CONEXAO COM A CIELO NO BANCO
        $convenio_cielo = $convenio_cartao->find($this::ID_CONVENIO_CARTAO);

        $nu_cartao = preg_replace('#[^0-9]#', '', $request->nu_cartao_credito);
        $arrayDtValidade = explode("/", $request->validade_cartao);
        $mes = $arrayDtValidade[0];
        $ano = $arrayDtValidade[1];
        $vl_compra_cielo = (int)preg_replace('#[^0-9]#', '', $request->vl_compra);
        $vl_compra = converte_float($request->vl_compra);
        $nu_seguranca = $request->nu_seguranca;

        if (env('APP_ENV') == 'production') {
            $cielo = new Cielo($convenio_cielo->nu_convenio, $convenio_cielo->no_chave, Cielo::PRODUCTION);
        } else {
            $cielo = new Cielo($convenio_cielo->nu_convenio, $convenio_cielo->no_chave, Cielo::TEST);
        }
        $holder = $cielo->holder($nu_cartao, $ano, $mes, Holder::CVV_INFORMED, $nu_seguranca);

        //SALVANDO O DADOS DA COMPRA
        $compra_credito->id_forma_pagamento = $this::ID_FORMA_PAGAMENTO_DEBITO;
        $compra_credito->id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_APROVACAO;
        $compra_credito->id_usuario = Auth::User()->id_usuario;
        $compra_credito->id_usuario_cad = Auth::User()->id_usuario;
        $compra_credito->va_credito = $vl_compra;
        $compra_credito->dt_compra = Carbon::now();
        $compra_credito->dt_cadastro = Carbon::now();
        $compra_credito->save();

        $this->id_compra_credito = $compra_credito->id_compra_credito;
        $this->nu_pedido_convenio = Carbon::now()->format('Y') . Auth::User()->id_unidade_gestora . $this->id_compra_credito;
        Session::put('id_compra_credito', $this->id_compra_credito);

        $order = $cielo->order($this->nu_pedido_convenio, $vl_compra_cielo);
        $paymentMethod = $cielo->paymentMethod(strtolower($request->id_bandeira), 'A', 1);
        $transaction = $cielo->transaction($holder,  $order, $paymentMethod, $pagamento->url_retorno, Transaction::AUTHORIZE_IF_AUTHENTICATED, true);
        $transaction = $cielo->transactionRequest($transaction);
        $strRedirectUrl = $transaction->getAuthenticationURL();

        if ($erro>0) {
            return response()->json([
                'status' => 'erro',
                'msg' => 'Bandeira não disponível para função débito. Erro nº 20001 ',
                'recarrega' => false
            ]);
        } else {
            return response()->json([
                'status' => 'sucesso',
                'msg' => 'Neste processo é necessário que nenhuma janela seja fechada para que a transação ocorra com suceso.<br /><br />Você deve desbloquear o gerenciador de popup para este site...<br /><br /><strong>Deseja continuar?</strong>',
                'tipo' => 'debito',
                'url' => trim($strRedirectUrl),
                'recarrega' => false
            ]);
        }
        //return trim($strRedirectUrl);
    }

    public function inserir_compra_cartao(Request $request, convenio_cartao $convenio_cartao)
    {
        #BUSCANDO AS INFORMACOES DE CONEXAO COM A CIELO NO BANCO
        $convenio_cielo = $convenio_cartao->find($this::ID_CONVENIO_CARTAO);
        $erro = 0;

        DB::beginTransaction();

        /*
         * TRATANDDO OS DADOS DO FORMULÁRIO
         */
        $nu_cartao = preg_replace('#[^0-9]#', '', $request->nu_cartao_credito);
        $arrayDtValidade = explode("/", $request->validade_cartao);
        $mes = $arrayDtValidade[0];
        $ano = $arrayDtValidade[1];
        $vl_compra_cielo = intval(preg_replace('#[^0-9]#', '', $request->vl_compra));
        $vl_compra = converte_float($request->vl_compra);
        $nu_seguranca = $request->nu_seguranca;

        $mid = $convenio_cielo->nu_convenio; //seu merchant id
        $key = $convenio_cielo->no_chave; //chave

        /**
         * Verificação para setar qual é o ombinete que a aplicação está sendo rodada (Produção ou Desnvolvimento/Testes).
         */
        if (env('APP_ENV') == 'production') {
            $cielo = new Cielo($mid, $key, Cielo::PRODUCTION);
        } else {
            $cielo = new Cielo($mid, $key, Cielo::TEST);
        }


        $holder = $cielo->holder($nu_cartao, $ano, $mes, Holder::CVV_INFORMED, $nu_seguranca);
        $holder->setName(substr($request->titular_cartao, 0, 50));

        //SALVANDO O DADOS DA COMPRA
        $compra_credito = new compra_credito();
        $compra_credito->id_forma_pagamento = $this::ID_FORMA_PAGAMENTO_CREDITO;
        $compra_credito->id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_APROVACAO;
        $compra_credito->id_usuario = Auth::User()->id_usuario;
        $compra_credito->id_usuario_cad = Auth::User()->id_usuario;
        $compra_credito->va_credito = $vl_compra;
        $compra_credito->dt_compra = Carbon::now();
        $compra_credito->dt_cadastro = Carbon::now();
        if (!$compra_credito->save()) {
            $erro = 10001;
        }

        $this->id_compra_credito = $compra_credito->id_compra_credito;
        $this->nu_pedido_convenio = Carbon::now()->format('Y') . Auth::User()->id_unidade_gestora . $this->id_compra_credito;
        $order = $cielo->order($this->nu_pedido_convenio, $vl_compra_cielo);

        switch ($request->id_bandeira) {
            case 'visa':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::VISA, PaymentMethod::CREDITO_A_VISTA);
                break;
            case 'mastercard':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::MASTERCARD, PaymentMethod::CREDITO_A_VISTA);
                break;
            case 'diners':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::DINERS, PaymentMethod::CREDITO_A_VISTA);
                break;
            case 'amex':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::AMEX, PaymentMethod::CREDITO_A_VISTA);
                break;
            case 'elo':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::ELO, PaymentMethod::CREDITO_A_VISTA);
                break;
            case 'aura':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::AURA, PaymentMethod::CREDITO_A_VISTA);
                break;
            case 'jcb':
                $paymentMethod = $cielo->paymentMethod(PaymentMethod::JCB, PaymentMethod::CREDITO_A_VISTA);
                break;
        }

        $transaction = $cielo->transaction($holder, $order, $paymentMethod, 'http://localhost/cielo.php', Transaction::AUTHORIZE_WITHOUT_AUTHENTICATION, true);
        $transaction = $cielo->transactionRequest($transaction);
        $arrayStatusTransacao = $this->getStatusTransacao($transaction, $compra_credito, $this->nu_pedido_convenio, $this->id_compra_credito);

        /**
         * Adicionado para inserir o status TID
         */
        if ($transaction->getTid() > 0) {
            $compra_credito->protocolo_compra = $transaction->getTid();
            if (!$compra_credito->save()) {
                $erro = 10002;
            }
        }

        if ($transaction->getAuthorization()->getLR() == '00') {
            if ($arrayStatusTransacao['status'] == 6 && $transaction->getTid() > 0) {
                if ($arrayStatusTransacao['retorno'] == true) {
                    //INSERINDO NA TABELA DE MOVIMENTACAO
                    $credito_entrada_saida = new credito_entrada_saida();
                    $credito_entrada_saida->id_compra_credito = $this->id_compra_credito;
                    $credito_entrada_saida->id_tipo_credito = 1;
                    $credito_entrada_saida->tp_movimento = 'E';
                    $credito_entrada_saida->va_movimento = $vl_compra;
                    $credito_entrada_saida->id_usuario = Auth::User()->id_usuario;
                    $credito_entrada_saida->dt_cadastro = Carbon::now();
                    $credito_entrada_saida->dt_movimento = Carbon::now();
                    if (!$credito_entrada_saida->save()) {
                        $erro = 10006;
                    }
                } else {
                    $erro = 10005;
                }
            } else {
                $erro = 10004;
            }
        } else {
            $erro = 10003;
        }

        if ($erro>0) {
            DB::rollback();
            switch ($erro) {
                case 10003:
                case 10004:
                    $this->retornoErroTransacao($compra_credito, $this->id_compra_credito, $transaction->getTid(), $this::ID_SITUACAO_CREDITO_CANCELADO, $this->nu_pedido_convenio);

                    return response()->json(['status'=>'erro',
                                             'recarrega' => false,
                                             'msg' => $arrayStatusTransacao['msg']
                                        ]);
                    break;
                case 10005:
                    return response()->json(['status'=>'erro',
                                             'recarrega' => false,
                                             'msg' =>'Erro ao inserir informação no banco, porém sua transação foi aprovada e seu crédito adicionado.'
                                        ]);
                    break;
                default:
                    return response()->json(['status' =>'erro',
                                             'recarrega' => false,
                                             'msg' => 'Erro ao fazer uma requisição contacte o administrador do sistema.'
                                        ]);
                    break;
            }
        } else {
            DB::commit();
            return response()->json(['status'=>'sucesso',
                                     'recarrega' => false,
                                     'msg' =>'Transaçao efetuada sucesso!',
                                     'id_compra_credito' => $this->id_compra_credito
                                ]);
        }
    }

    public function buscar_saldo()
    {
        return Auth::User()->saldo_usuario();
    }

    private function getStatusTransacao($transaction, $compra_credito, $nu_pedido_convenio, $id_compra_credito)
    {
        $status = $transaction->getStatus();
        $nu_pedido_convenio = (int)$nu_pedido_convenio;

        switch ($status) {
            case "5":
            case "9":
            case "10":
                $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                break;
            case "12":
                $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_CANCELAMENTO;
                break;
            case "6":
                $id_situacao_credito = $this::ID_SITUACAO_CREDITO_APROVADO;
                break;
            default:
                $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                break;
        }

        $retorno = $compra_credito->where('id_compra_credito', $id_compra_credito)
                                    ->update(
                                        [
                                            'protocolo_compra' => $transaction->getTid(),
                                            'id_situacao_credito' => $id_situacao_credito,
                                            'id_usuario_lib' => Auth::User()->id_usuario,
                                            'dt_liberacao' => Carbon::now(),
                                            'nu_pedido_convenio' => $nu_pedido_convenio
                                        ]
                                    );

        return ["status" => $status, "retorno" => $retorno, "msg" => $transaction->getAuthorization()->getMessage()];
    }

    private function retornoErroTransacao($compra_credito, $id_compra_credito, $tid, $id_situacao_credito, $nu_pedido_convenio)
    {
        $retorno = $compra_credito->where('id_compra_credito', $id_compra_credito)
                                    ->update(
                                        [
                                            'protocolo_compra' => $tid,
                                            'id_situacao_credito' => $id_situacao_credito,
                                            'id_usuario_lib' => Auth::User()->id_usuario,
                                            'dt_liberacao' => Carbon::now(),
                                            'nu_pedido_convenio' => $nu_pedido_convenio
                                        ]
                                    );
        return $retorno;
    }


    public function gerar_boleto($id_boleto)
    {

        //dd(Auth::User()->pessoa);
        //dd(Auth::User()->pessoa->enderecos);


        // DADOS DO BOLETO PARA O SEU CLIENTE
        $dias_de_prazo_para_pagamento = 5;
        $taxa_boleto = 0;
        $data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006";
        $valor_cobrado = "1500,00"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
        $valor_cobrado = str_replace(",", ".", $valor_cobrado);
        $valor_boleto = number_format($valor_cobrado + $taxa_boleto, 2, ',', '');

        $dadosboleto["nosso_numero"] = "87003";
        $dadosboleto["numero_documento"] = date("Ym") . ".00003";    // Num do pedido ou do documento 27.030195.10
        $dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        $dadosboleto["valor_boleto"] = $valor_boleto;    // Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

        // DADOS DO SEU CLIENTE
        $dadosboleto["sacado"] = Auth::User()->pessoa->no_pessoa;
        $dadosboleto["endereco1"] = Auth::User()->pessoa->enderecos[0]->no_endereco;
        $dadosboleto["endereco2"] = Auth::User()->pessoa->enderecos[0]->no_cidade . "-" . Auth::User()->pessoa->enderecos[0]->co_uf . " / " . Auth::User()->pessoa->enderecos[0]->nu_cep;


        // DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
        $dadosboleto["quantidade"] = "1";
        $dadosboleto["valor_unitario"] = "1";


        return view('creditos.boleto-bb', compact('dadosboleto'));

        /*$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
        $cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

        $boleto = new BancoDoBrasil(array(
            // Parâmetros obrigatórios
            'dataVencimento' 	=> Carbon::now(),
            'dataDocumento' 	=> Carbon::parse('06/25/2016'),
            'dataProcessamento' => Carbon::parse('06/25/2016'),
            'valor' 			=> 15.00,
            'sequencial' 		=> 1174133, // Para gerar o nosso número
            'numeroDocumento' 	=> 01174133, // Para gerar o nosso número
            'sacado' 			=> $sacado,
            'cedente' 			=> $cedente,
            'agencia' 			=> 3382, // Até 4 dígitos
            'carteira' 			=> 17,
            'conta' 			=> 4003225, // Até 8 dígitos
            'convenio' 			=> 1234, // 4, 6 ou 7 dígitos
            'instrucoes'		=> array("NÃO CONCEDER DESCONTO.",
                "COBRAR JUROS DE R$ 0.02 POR DIA DE ATRASO SOBRE O VALOR PRINCIPAL.",
                "APÓS VENCIMENTO, PAGAR SOMENTE NAS AGÊNCIAS DO BANCO DO BRASIL.",
                "NÃO RECEBER APÓS 30 DIAS DO VENCIMENTO."),
            'descricaoDemonstrativo'		=> array("BLA BLA BLA.", "NÇA aDÇAD ASDADD"),
        ));

        echo $boleto = $boleto->getOutput();


        /*$pdf = PDF::loadView('pdf.boleto', compact('boleto'));
        $pdf->stream();*/
        //echo $arquivo;
        //$pdf = PDF::loadHTML($arquivo);
        //return $pdf->download();
        //PDF::loadHTML($arquivo);
        //PDF::download('document.pdf');
        /*return response($arquivo, 200)->header('Content-Type', 'application/html')
            ->header('Content-Disposition', 'attachment; filename="boleto.html"');*/
        //echo $boleto->getNossoNumero();
    }

    public function inserir_compra_boleto(Request $request, forma_pagamento $forma_pagamento)
    {
        echo 1;
    }

}

?>