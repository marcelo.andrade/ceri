<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\tipo_ato_emolumento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

use phpDocumentor\Reflection\DocBlockTest;
use Validator;
use Auth;
use Hash;
use Carbon\Carbon;
use File;
use Storage;
use XMLReader;
use DOMDocument;

use App\selo_lote_entrada;
use App\selo_lote_pedido;
use App\selo_lote_pedido_aplicacao;


class SeloController extends Controller {


	const ID_SITUACAO_SELO_ABERTO = 1;

	public function __construct() {

	}

	public function index(Request $request, selo_lote_entrada $selo_lote_entrada)
	{
		set_time_limit(0);
		$dir_arquivo     = storage_path().'/app/public/selos/selodigitais.xml';

		$xml = simplexml_load_file($dir_arquivo);

		echo $xml->attributes()->codigoCartorio.'<br/>';
		echo $xml->attributes()->data.'<br/>';
		echo $xml->attributes()->dataPagamento.'<br/>';
		echo $xml->attributes()->numero.'<br/>';
		echo $xml->attributes()->numeroGuia.'<br/>';
		echo $xml->attributes()->numeroSelos.'<br/>';

		$indice_numero_selo = ($xml->attributes()->numeroSelos - 1 );

		$selo_inicial = $xml->selos->selo[0]->attributes()->dv.'-'.$xml->selos->selo[0]->attributes()->numero;

		$selo_final   = $xml->selos->selo[$indice_numero_selo]->attributes()->dv.'-'.
						$xml->selos->selo[$indice_numero_selo]->attributes()->numero;

		echo $selo_inicial.'<br/>';
		echo $selo_final.'<br/>';


		foreach ($xml->selos->selo as $selo)
		{

			echo 'DV: ' 	 . $selo->attributes()->dv . ' - ';
			echo 'numero: '  . $selo->attributes()->numero . '<br />';
		}


		//$xml = new XMLReader();

		/*$xml->open($dir_arquivo);
		$DOM = new DOMDocument;
		$i=0;

		while( $xml->read() )
		{
			if( 'pedido' === $xml->name )
			{
				echo $xml->name . '<br >/';
			}
		}*/
	}
	
	public function configuracoes_salvar_selos(Request $request, selo_lote_entrada $selo_lote_entrada, selo_lote_pedido $selo_lote_pedido, selo_lote_pedido_aplicacao $selo_lote_pedido_aplicacao, tipo_ato_emolumento $tipo_ato_emolumento)
	{
		set_time_limit(0);

		$diretorio     = storage_path().'/app/public/selos/'.Auth::User()->pessoa->serventia->id_serventia.'/importado/';
		$nome_arquivo  = strtolower( remove_caracteres($request->no_arquivo->getClientOriginalName()) );

		if (!File::isDirectory($diretorio)) {
			if (!File::makeDirectory($diretorio, 0777, true, true)){
				return 'ERRO_02';
			};
		}

		$dir_arquivo = $diretorio.'/'.$nome_arquivo;
		if (file_exists($dir_arquivo)){
			return 'ERRO_03';
		}

		if ($request->no_arquivo->move($diretorio, $nome_arquivo))
		{
			if (!file_exists($dir_arquivo)){
				return 'ERRO_01';
			}

			DB::beginTransaction();
				$xml = new XMLReader();
				$xml->open($dir_arquivo);
				$DOM = new DOMDocument;

				while( $xml->read() )
				{
					if( 'selo_eletronico' == $xml->name )
					{
						
						if ($xml->nodeType == XMLReader::ELEMENT)
						{
							$selo2 = simplexml_import_dom($DOM->importNode($xml->expand(), true));
							#inserindo registro principal
							$selo_lote_entrada->no_arquivo 				= $nome_arquivo;
							$selo_lote_entrada->no_diretorio_arquivo	= $diretorio;
							$selo_lote_entrada->dt_lote_cadastrado		= Carbon::now();
							$selo_lote_entrada->id_usuario_cad			= Auth::User()->id_usuario;
							$selo_lote_entrada->dt_cadastro				= Carbon::now();
							$selo_lote_entrada->nu_hash_arquivo			= $selo2->hash_conferencia_do_arquivo_xml;
							$selo_lote_entrada->save();
						}
					}

					if( $selo_lote_entrada->id_selo_lote_entrada > 0)
					{
						if ('pedido_do_selo_eletronico' == $xml->name)
						{
							if ($xml->nodeType == XMLReader::ELEMENT)
							{
								$selo = simplexml_import_dom($DOM->importNode($xml->expand(), true));
								#inserindo lotes de selo

								$tipo_ato			= $tipo_ato_emolumento->where('nu_tipo_ato_emolumento', $selo->tipo_de_ato)->get();

								if ( $tipo_ato[0]->id_tipo_ato_emolumento < 1  || $tipo_ato[0]->id_tipo_ato_emolumento == NULL)
								{
									DB::rollback();
									return 'ERRO 4';
								}

								$selo_pedido							= new selo_lote_pedido();
								$selo_pedido->id_tipo_ato_emolumento 	= $tipo_ato[0]->id_tipo_ato_emolumento;
								$selo_pedido->id_selo_lote_entrada 		= $selo_lote_entrada->id_selo_lote_entrada;
								$selo_pedido->nu_pedido_selo 			= $selo->identificacao_pedido;
								$selo_pedido->dt_solicitacao 			= $selo->data_da_solicitacao;
								$selo_pedido->nu_ini_lote 				= $selo->codigo_inicial_do_selo;
								$selo_pedido->nu_fim_lote 				= $selo->codigo_final_do_selo;
								$selo_pedido->nu_quantidade_selo 		= $selo->quantidade_de_selos;
								$selo_pedido->in_assinatura_digital_processo = ( $selo->assinatura_digital == 'true' ) ? 'S' : 'N';
								$selo_pedido->id_usuario_cad 			= Auth::User()->id_usuario;
								$selo_pedido->dt_cadastro 				= Carbon::now();
								$selo_pedido->save();

								if ( $selo_pedido->id_selo_lote_pedido > 0 )
								{
									$codigo_inicial 	= $selo->codigo_inicial_do_selo;
									$parte_inicial_selo = substr($codigo_inicial, 0, -6);
									$parte_final_selo 	= substr($codigo_inicial, -6);

									for ($i = 0; $i < $selo->quantidade_de_selos; $i++)
									{
										$novo_selo = $parte_inicial_selo . str_pad($parte_final_selo + $i, 6, 0, STR_PAD_LEFT);

										$pedido_loto_aplicacao = new selo_lote_pedido_aplicacao();
										$pedido_loto_aplicacao->id_selo_lote_pedido = $selo_pedido->id_selo_lote_pedido;
										$pedido_loto_aplicacao->id_situacao_selo = $this::ID_SITUACAO_SELO_ABERTO;
										$pedido_loto_aplicacao->nu_selo = $novo_selo;
										$pedido_loto_aplicacao->dt_lote_cadastrado = Carbon::now();
										$pedido_loto_aplicacao->id_usuario_cad = Auth::User()->id_usuario;
										$pedido_loto_aplicacao->dt_cadastro = Carbon::now();
										$pedido_loto_aplicacao->save();
									}
								}else{
									DB::rollback();
									return 'ERRO';
								}
							}//$xml->nodeType == XMLReader::ELEMENT
						}//('pedido_do_selo_eletronico' == $xml->name)
					}else{
						DB::rollback();
						return 'ERRO';
					}
				}
				DB::commit();
				echo 'SUCESSO';
		}
	}

	public function exportar_selo()
	{
		$diretorio  = storage_path().'/app/public/selos/'.Auth::User()->id_usuario."/exportado";

		if (!File::isDirectory($diretorio)){
			if (!File::makeDirectory($diretorio, 0777, true, true)){
				return 'ERRO_02';
			};
		}

		$doc = new DOMDocument('1.0', 'utf-8');
		$doc->xmlStandalone = true;
		$doc->formatOutput  = true;

		$selo_eletronico = $doc->createElement("selo_eletronico");
		$doc->appendChild($selo_eletronico);

		$lote_dos_atos = $doc->createElement("lote_dos_atos");
		$selo_eletronico->appendChild($lote_dos_atos);

		for( $i=0; $i<2; $i++)
		{
			$ato_utilizado = $doc->createElement("ato_utilizado");
			$lote_dos_atos->appendChild($ato_utilizado);

			$ato_utilizado->appendChild($doc->createElement("identificacao_pedido_na_cgj", '34843'));
			$ato_utilizado->appendChild($doc->createElement("codigo_do_selo", '01641406111910039001555'));
			$ato_utilizado->appendChild($doc->createElement("codigo_do_ato", '01641406111910039001555'));
			$ato_utilizado->appendChild($doc->createElement("tipo_de_ato", '39'));
			$ato_utilizado->appendChild($doc->createElement("nome_do_civil_do_ato", 'MARIA GONÇALVES CORAÇÃO '. $i));
			$ato_utilizado->appendChild($doc->createElement("nome_do_serventuario_que_praticou_ato", 'Cassio Cassimiro Santana (Protocolo: 114.856)'));
			$ato_utilizado->appendChild($doc->createElement("data_hora_da_solicitacao", '2014-11-18 08:31:17 UTC'));
			$ato_utilizado->appendChild($doc->createElement("ip_da_maquina_que_praticou_ato", '192.168.5.12'));
			$ato_utilizado->appendChild($doc->createElement("valor_de_entrada_do_ato", '50,00'));
			$ato_utilizado->appendChild($doc->createElement("emolumento_do_ato", '4,93'));
			$ato_utilizado->appendChild($doc->createElement("fundesp_do_ato", '23,93'));
			$ato_utilizado->appendChild($doc->createElement("taxa_judiciaria_do_ato", '0'));
			$ato_utilizado->appendChild($doc->createElement("assinatura_digital", 'False'));
		}

		$nome_arquivo = "selodigitalxml_".Carbon::now()->format('Y-m-d_H-i-s').'.xml';
		$dir_arquivo  = storage_path().'/app/public/selos/'.Auth::User()->id_usuario."/exportado/".$nome_arquivo;
		$doc->save($dir_arquivo);

		$dir_arquivo = $diretorio.'/'.$nome_arquivo;
		$arquivo = Storage::get('/public/selos/'.Auth::User()->id_usuario.'/exportado/'.$nome_arquivo);


		return response($arquivo, 200)->header('Content-Type', 'application/xml')
			->header('Content-Disposition', 'attachment; filename="'.$nome_arquivo.'');

	}


}
