<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;

use App\processo;
use App\parte;
use App\natureza_acao;

class ProcessoController extends Controller {
	
	public function __construct(processo $processo) {
		$this->processo=$processo;
	}
	
	public function novo(Request $request, natureza_acao $natureza_acao) {
		$acoes = $natureza_acao->orderBy('nu_ordem','desc')->get();
		return view('servicos.processo-novo',compact('acoes'));
	}
	
	public function inserir(Request $request, parte $parte) {
		$novoProcesso = $this->processo;
		$novoProcesso->numero_processo = $request->numero_processo;
		$novoProcesso->id_natureza_acao = $request->id_natureza_acao;
		$novoProcesso->id_usuario_cad = Auth::User()->id_usuario;
		$novoProcesso->dt_cadastro = Carbon::now();
		
		if ($novoProcesso->save()) {
			$i=0;
			foreach ($request->a_nu_cpf_cnpj as $nu_cpf_cnpj) {
				$nu_cpf_cnpj = preg_replace('#[^0-9]#', '', $nu_cpf_cnpj);
				$busca_parte = $parte->where('nu_cpf_cnpj',$nu_cpf_cnpj)
									 ->where('id_tipo_parte',$request->a_id_tipo_parte[$i])
									 ->first();
				if (count($busca_parte)>0) {
					$novoProcesso->partes()->attach($busca_parte);
				} else {
					$nova_parte = new parte();
					$nova_parte->nu_cpf_cnpj = $nu_cpf_cnpj;
					$nova_parte->id_tipo_parte = $request->a_id_tipo_parte[$i];
					$nova_parte->tp_pessoa = $request->a_tp_pessoa[$i];
					$nova_parte->no_parte = $request->a_no_parte[$i];
					$nova_parte->id_usuario_cad = Auth::User()->id_usuario;
					$nova_parte->dt_cadastro = Carbon::now();
					$nova_parte->save();
					$novoProcesso->partes()->attach($nova_parte);
				}
				$i++;
			}
		}
				
		return response()->json($novoProcesso);
	}

	public function atualizar_parte_processo(Request $request, parte $parte)
	{
		if ( $request->numero_processo < 0 )
		{
			return 'ERRO';
		}

		$novo_processo = $this->processo->where('numero_processo',$request->numero_processo)
  		                               ->first();
		$novo_processo->id_usuario_alt = Auth::User()->id_usuario;

		if ($novo_processo->save())
		{
			$i=0;
			foreach ($request->a_nu_cpf_cnpj as $nu_cpf_cnpj)
			{
				$nu_cpf_cnpj 		= preg_replace('#[^0-9]#', '', $nu_cpf_cnpj);
				$busca_parte 		= $parte->where('nu_cpf_cnpj',$nu_cpf_cnpj)->first();

				if (count($busca_parte)>0)
				{
					$novo_processo->partes()->attach($busca_parte);
				} else {
					$nova_parte 				= new parte();
					$nova_parte->nu_cpf_cnpj 	= $nu_cpf_cnpj;
					$nova_parte->id_tipo_parte 	= $request->a_id_tipo_parte[$i];
					$nova_parte->tp_pessoa 		= $request->a_tp_pessoa[$i];
					$nova_parte->no_parte 		= $request->a_no_parte[$i];
					$nova_parte->id_usuario_cad = Auth::User()->id_usuario;
					$nova_parte->dt_cadastro 	= Carbon::now();
					$nova_parte->save();
					$novo_processo->partes()->attach($nova_parte);
				}
				$i++;
			}
		}
		return response()->json($novo_processo);
	}


	public function total(Request $request) {
		$processos = $this->processo->where('numero_processo',$request->numero_processo)->count();
		return $processos;
	}

	public function detalhes(Request $request) {
		if ($processo = $this->processo->with('partes')->where('numero_processo',$request->numero_processo)->first()) {
			return response()->json($processo);
		} else {
			return 'ERRO';
		}
	}

	public function busca_parte(Request $request, parte $parte) {
		if ($request->nu_cpf_cnpj!='') {
			if ($parte = $parte->where('nu_cpf_cnpj',preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj))->first()) {
				return response()->json($parte);
			} else {
				return 'ERRO';
			}
		}
	}
}
