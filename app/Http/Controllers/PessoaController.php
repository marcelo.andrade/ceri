<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Validator;
use Auth;
use Hash;
use Carbon\Carbon;

use App\pessoa;
use App\usuario;
use App\usuario_senha;
use App\telefone;
use App\endereco;

class PessoaController extends Controller {

	protected $pessoa;
	
	public function __construct(pessoa $pessoa) {
		$this->pessoa=$pessoa;
	}
	
	public function totalPessoas(Request $request) {
		$pessoas = $this->pessoa->where('nu_cpf_cnpj',preg_replace('#[^0-9]#', '',$request->nu_cpf_cnpj))->count();
		return $pessoas;
	}

	public function listar_pessoa(Request $request) {
		$pessoas 		= $this->pessoa->where('nu_cpf_cnpj',preg_replace('#[^0-9]#', '',$request->nu_cpf_cnpj))->first();
		if (count($pessoas)>0) {
			$dados_pessoa = [
				'pessoa' => $pessoas->toArray(),
				'telefones' => $pessoas->telefones->toArray()
			];
		}else{
			$dados_pessoa = [];
		}
		return response()->json($dados_pessoa);
	}
}
