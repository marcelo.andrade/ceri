<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\usuario;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Mail;
use Session;

use App\alcada;
use App\estado;
use App\produto;
use App\certidao;
use App\serventia;
use App\pedido;
use App\pedido_serventia;
use App\pedido_serventia_resposta;
use App\situacao_pedido_grupo_produto;
use App\processo;
use App\matricula;
use App\tipo_certidao;
use App\tipo_resposta;
use App\tipo_certidao_chave_pesquisa;
use App\periodo_certidao;
use App\pedido_produto_item;
use App\historico_pedido;
use App\pedido_valor;
use App\pedido_resultado;

class CertidaoMatriculaController extends Controller {

	protected $vl_total_pedido;

	const ID_SITUACAO_CADASTRADO = 43;
	const ID_SITUACAO_CANCELADO = 44;
	const ID_SITUACAO_ANALISE = 45;
	const ID_SITUACAO_APROVACAO = 46;
	const ID_SITUACAO_ENCAMINHADO = 47;
	const ID_SITUACAO_FINALIZADO = 48;
	const ID_SITUACAO_CORRECAO = 51;

	public function __construct(certidao $certidao, CeriFuncoes $ceriFuncoes) {
		$this->certidao=$certidao;
		$this->id_grupo_produto=6;
		//$this->origem_utilizacao_selo = $ceriFuncoes->getTipoClienteSelo();

		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, estado $estado, produto $produto, serventia $serventia, situacao_pedido_grupo_produto $situacao, pedido_serventia_resposta $resposta, tipo_certidao_chave_pesquisa $tipo_chave){
		$ceri_funcoes     = new CeriFuncoes();
		$notificacao      = $ceri_funcoes->buscar_notificacoes();

		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela principal de matricula digital','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

		switch ($this->id_tipo_pessoa) {
			case 3: case 4: case 7: case 11: case 6:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this->id_grupo_produto)
									  ->orderBy('nu_ordem')->get();

				$certidoes_pendentes = $this->certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
													  ->join('produto','produto.id_produto','=','pedido.id_produto')
													  ->where('produto.id_grupo_produto',$this->id_grupo_produto)
													  ->where('pedido.id_pessoa_origem','=',$this->id_pessoa)
													  ->where('pedido.id_situacao_pedido_grupo_produto',$this::ID_SITUACAO_CADASTRADO)
													  ->orderBy('certidao.dt_cadastro','desc')->get();

				$certidoes_a_confirmar = $this->certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
														->join('produto','produto.id_produto','=','pedido.id_produto')
														->where('produto.id_grupo_produto',$this->id_grupo_produto)
														->where('pedido.id_pessoa_origem','=',$this->id_pessoa)
														->where('pedido.id_situacao_pedido_grupo_produto',$this::ID_SITUACAO_APROVACAO)
														->orderBy('certidao.dt_cadastro','desc')->get();

				$todas_certidoes = $this->certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
												  ->join('produto','produto.id_produto','=','pedido.id_produto')
												  ->where('produto.id_grupo_produto',$this->id_grupo_produto)
												  ->where('pedido.id_pessoa_origem','=',$this->id_pessoa)
												  ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_APROVACAO));
				
				//foi colocado dentro do case para seguir o padrao do projeto,porem sem necessidade -ECL
				$tipos_chave_filtro 	= $tipo_chave->where('in_registro_ativo','S')
					->where('in_exibe_filtro','S')
					->orderBy('nu_ordem','asc')
					->get();

				if ($request->isMethod('post')) {
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_certidoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_certidoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_certidoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_tipo_certidao_chave_pesquisa>0){
						$todas_certidoes->where('certidao.id_tipo_certidao_chave_pesquisa',$request->id_tipo_certidao_chave_pesquisa);
						$todas_certidoes->where('certidao.de_chave_certidao',$request->de_chave_pesquisa);
					}
				}

				$todas_certidoes = $todas_certidoes->orderBy('certidao.dt_cadastro','desc')->get();
													  
		    	return view('servicos.certidaomatricula',compact('request','this','cidades','produtos','situacoes','tipos_chave','tipos','periodos','certidoes_pendentes','certidoes_a_confirmar','todas_certidoes', 'tipos_chave_filtro', 'notificacao'));
				break;
			case 2:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this->id_grupo_produto)
									  ->orderBy('nu_ordem')->get();
										   
				$todas_certidoes = $this->certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
												  ->join('produto','produto.id_produto','=','pedido.id_produto')
												  ->join('pedido_pessoa',function($join) {
													  $join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
														   ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
											  	  })
												  ->where('produto.id_grupo_produto',$this->id_grupo_produto)
												  ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_CANCELADO));

				//foi colocado dentro do case para seguir o padrao do projeto,porem sem necessidade -ECL
				$tipos_chave_filtro 	= $tipo_chave->where('in_registro_ativo','S')
					->where('in_exibe_filtro','S')
					->orderBy('nu_ordem','asc')
					->get();

				if ($request->isMethod('post')) {
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_certidoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_certidoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_certidoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_tipo_certidao_chave_pesquisa>0){
						$todas_certidoes->where('certidao.id_tipo_certidao_chave_pesquisa',$request->id_tipo_certidao_chave_pesquisa);
						$todas_certidoes->where('certidao.de_chave_certidao',$request->de_chave_pesquisa);
					}
				}
				
				$todas_certidoes = $todas_certidoes->orderBy('certidao.dt_cadastro','desc')->get();
					
		    	return view('servicos.cartorio.certidaomatricula',compact('request','this','produtos','situacoes','todas_certidoes', 'tipos_chave_filtro', 'notificacao'));
				break;
		}
    }

    public function nova(estado $estado, produto $produto, tipo_certidao $tipo_certidao, tipo_certidao_chave_pesquisa $tipo_chave, periodo_certidao $periodo_certidao) {
		$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		$tipos_chave = $tipo_chave->where('in_registro_ativo','S')
								  ->orderBy('nu_ordem','asc')
								  ->get();
		$ceriFuncoes = new CeriFuncoes();
		$va_produto  = $ceriFuncoes->buscar_valor_produto( 17 );

		return view('servicos.certidaomatricula-nova',compact('cidades','produtos','tipos_chave','tipos','periodos', 'va_produto'));
    }

	public function inserir(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, serventia $serventia, processo $processo, matricula $matricula, pedido_serventia_resposta $resposta, pedido_serventia $pedido_serventia, pedido_produto_item $pedido_produto_item, pedido_resultado $pedido_resultado, historico_pedido $historico_pedido) {
		$nova_certidao = $this->certidao;
		$novo_pedido = $pedido;
		
		$protocoloPedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", 17);"));
		
		$novo_pedido->id_usuario = Auth::User()->id_usuario;
		$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
		$novo_pedido->id_produto = 17;
		if ($this->id_tipo_pessoa == 3) {
			$novo_pedido->id_alcada = 1;
		} elseif ($this->id_tipo_pessoa == 4) {
			$novo_pedido->id_alcada = 2;
		}
		$novo_pedido->protocolo_pedido = $protocoloPedido[0]->f_geraprotocolo;
		$novo_pedido->dt_pedido = Carbon::now();
		$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
		$novo_pedido->dt_cadastro = Carbon::now();
		$novo_pedido->va_pedido = 0;

		$vl_pedido = 0;
		if ($novo_pedido->produto->produto_itens[0]->in_precificacao == "N")
		{
			//Somente precifica o valor e faz o debito na conta do cliente ao enviar o pedido
			if ($this->id_tipo_pessoa == 4 &&  $request->in_isenta == "S") {
				$novo_pedido->va_pedido 	= 0;
			}else{
				$ceriFuncoes 				= new CeriFuncoes();
				$vl_pedido 					= $ceriFuncoes->buscar_valor_produto( 26 );
				$novo_pedido->va_pedido 	= $vl_pedido;
			}
			$retorno = $novo_pedido->save();
		}

		
		if ($retorno) {
			$serventia_selecionada = $serventia->find($request->id_serventia);
			$novo_pedido->serventias()->attach($serventia_selecionada);
			
			$novo_pedido_produto_item = new pedido_produto_item();
			$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
			$novo_pedido_produto_item->id_produto_item = 26;
			$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
			$novo_pedido_produto_item->save();
			
			$nova_certidao->id_pedido = $novo_pedido->id_pedido;
			$nova_certidao->id_tipo_certidao_chave_pesquisa	= $request->id_tipo_certidao_chave_pesquisa;
			$nova_certidao->de_chave_certidao = $request->de_chave_certidao;
			$nova_certidao->in_pesquisa = 'N';
			$nova_certidao->in_penhora = 'N';
			$nova_certidao->in_indisponibilidade = 'N';
			$nova_certidao->dt_cadastro = Carbon::now();
			$nova_certidao->dt_geracao = Carbon::now();
			$nova_certidao->dt_liberacao = Carbon::now();
			$nova_certidao->dt_indisponibilizacao  = Carbon::now()->addDays(30);
			$nova_certidao->nu_dia_disponibilizacao  = 30;
			
			if ($this->id_tipo_pessoa==4 || $this->id_tipo_pessoa==7) {
				$nova_certidao->in_isenta = ($request->in_isenta?$request->in_isenta:'N');
				if ($request->numero_processo>0) {
					$nova_certidao->id_processo = $processo->where('numero_processo',$request->numero_processo)->first()->id_processo;
				}
			} else {
				$nova_certidao->in_isenta = 'N';
			}
			
			if ($nova_certidao->save()) {
				$novo_historico = $historico_pedido;
				$novo_historico->id_pedido = $novo_pedido->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
				switch(Auth::User()->tipo_usuario->no_tipo_usuario) {
					case 'Cliente':
						$novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_FISICA;
						break;
					case 'Judiciário':
						$novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
						break;
				}
				$novo_historico->de_observacao = 'Pedido inserido com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();


				$ceriFuncoes = new CeriFuncoes();
				$ceriFuncoes->inserir_notificacao('SERVENTIA',$novo_pedido->id_pedido,'Nova solicitação de Matrícula Digital, protocolo número: '.$novo_pedido->protocolo_pedido,0,$request->id_serventia);

				return response()->json(['pedido'=>$novo_pedido,'vl_pedido_formatado'=>formatar_valor( $vl_pedido ),'certidao'=>$nova_certidao,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'),'dt_validade'=>Carbon::now()->addDays(30)->format('d/m/Y')]);
			} else {
				return 'ERRO';
			}
		} else {
			return 'ERRO';
		}
	}
	
	public function enviar_pedido(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido, pedido_resultado $pedido_resultado, matricula $matricula) {
		if ($request->id_pedido>0) {
			$pedido_selecionado = $pedido->find($request->id_pedido);

			if ($pedido_selecionado->certidao->in_isenta != 'S' and $pedido_selecionado->produto_itens[0]->produto_item->in_precificacao=='N') {
				//verificando se o preço atual e diferente do preço de tabela
				$ceriFuncoes = new CeriFuncoes();
				//if ($pedido_selecionado->va_pedido != $ceriFuncoes->buscar_valor_produto( $pedido_selecionado->id_produto ))
				if ($pedido_selecionado->va_pedido != $ceriFuncoes->buscar_valor_produto( $pedido_selecionado->produto_itens[0]->id_produto_item )) {
					return 'SALDODIFERENTE';
				}

				//$retornoDebito      = $ceriFuncoes->processar_debito_cliente( $pedido_selecionado );
				//if ( $retornoDebito == false) {	 return 'ERROSALDO'; }

				$auxvl_pedido       = $ceriFuncoes->buscar_valor_produto( $pedido_selecionado->produto_itens[0]->id_produto_item );
				$vl_pedido          = ( $pedido_selecionado->va_pedido > 0 ) ? $pedido_selecionado->va_pedido : $auxvl_pedido;


				if ( Auth::User()->saldo_usuario() <  $vl_pedido ) {
					return 'ERROSALDO';
				}

				if ($pedido_selecionado->id_pedido>0) {
					$dados = [
						"id_pedido" 					=> $pedido_selecionado->id_pedido,
						"id_usuario" 					=> Auth::User()->id_usuario,
						"id_usuario_logado"				=> Auth::User()->id_usuario,
						"vl_pedido" 					=> converte_float($vl_pedido),
						"tp_movimentacao_financeira" 	=> 2,
						"tp_movimentacao" 				=> 'S',
						"id_forma_pagamento" 			=> 6,
                        "v_id_pedido_desconto"          => null,
                        "v_va_desconto"                 => 0,
                        "v_in_desconto"                 => 'N',
                        "v_va_desconto_perc"            => null,
                        "v_id_compra_credito"           => $request->id_compra_credito
					];

					$objpedido	= new pedido();
					$objpedido->registrar_valor_pedido($dados);
				}

			}


			if ($pedido_selecionado->certidao->in_isenta=='S' or $pedido_selecionado->produto_itens[0]->produto_item->in_precificacao=='N') {
				$situacao_pedido = $this::ID_SITUACAO_ENCAMINHADO;
			} else {
				$situacao_pedido = $this::ID_SITUACAO_ANALISE;
			}

			if ($pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $situacao_pedido])) {				   
				$novo_historico = $historico_pedido;
				$novo_historico->id_pedido = $request->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $situacao_pedido;
				switch(Auth::User()->tipo_usuario->no_tipo_usuario) {
					case 'Cliente':
						$novo_historico->id_alcada = 1;
						break;
					case 'Judiciário':
						$novo_historico->id_alcada = 2;
						break;
				}
				$novo_historico->de_observacao = 'Pedido enviado com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();
				
				$busca_matricula = $matricula->where('id_serventia',$pedido_selecionado->pedido_serventia->id_serventia)
											 ->where('codigo_matricula',$pedido_selecionado->certidao->de_chave_certidao)
											 ->first();

				if ($busca_matricula) {
					if (count($busca_matricula->matriculas_digitais)>0) {
						$destino = '../storage/app/public/certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao;
						$destino_final = $destino.'/resultado';
						if (!File::isDirectory($destino)) {
							File::makeDirectory($destino);
						}
						if (!File::isDirectory($destino_final)) {
							File::makeDirectory($destino_final);
						}
						$nome_arquivo = 'matricula_'.$pedido_selecionado->protocolo_pedido.'.pdf';
						$nome_arquivo_p7s = 'matricula_'.$pedido_selecionado->protocolo_pedido.'.p7s';

						$titulo = 'Resultado matrícula digital nº '.$pedido_selecionado->protocolo_pedido;

						$i=1;
						$total = count($busca_matricula->matriculas_digitais);
						$imagens = array();
						$corrompido = false;
						foreach($busca_matricula->matriculas_digitais as $matricula_digital) {
							if (Storage::exists($matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo)) {
								if ($matricula_digital->in_corrompido=='S') {
									$imagens[] = '<h2>Folha corrompida</h2><br /><br />Arquivo corrompido: '.$matricula_digital->no_arquivo;
									$corrompido = true;
								} else {
									if (in_array($matricula_digital->no_extensao_arquivo,array('tif','tiff'))) {
										if (verifica_img_corrompida(storage_path('app'.$matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo))) {
											$imagens[] = '<h2>Folha corrompida</h2><br /><br />Arquivo corrompido: '.$matricula_digital->no_arquivo;
											$matricula_digital->in_corrompido='S';
											$matricula_digital->save();
											$corrompido = true;
										} else {
											$imagem = Image::make(storage_path('app'.$matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo));
									    	
									    	$imagens[] = '<img src="data:image/jpg;base64,'.base64_encode($imagem->encode('jpg')).'" width="100%" />';
									    }
									} else {
										$imagens[] = '<img src="'.storage_path('app'.$matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo).'" width="100%" />';
									}
								}
								if (count($busca_matricula->matriculas_digitais)>1 and $i<$total) {
									$imagens[] = '<div class="page-break"></div>';
								}
							}
							$i++;
						}

						$pdf = PDF::loadView('pdf.certidaomatricula-auto', compact('titulo','imagens'));
							
						if ($pdf->save($destino_final.'/'.$nome_arquivo)) {
							$novo_resultado = $pedido_resultado;
							$novo_resultado->id_pedido = $pedido_selecionado->id_pedido;
							$novo_resultado->in_positivo = 'S';
							$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;
							$novo_resultado->no_arquivo = $nome_arquivo;
							$novo_resultado->no_local_arquivo = 'certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao.'/resultado';;

							if ($pedido_selecionado->pedido_serventia->serventia->serventia_certificado) {
								$certificado = $pedido_selecionado->pedido_serventia->serventia->serventia_certificado->no_local_arquivo.'/'.$pedido_selecionado->pedido_serventia->serventia->serventia_certificado->no_arquivo;
		
								if (Storage::exists($certificado)) {
									$CeriFuncoes = new CeriFuncoes();
									$cn = $pedido_selecionado->pedido_serventia->serventia->serventia_certificado->no_comum;
									$arquivo = storage_path().'/app/public/certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao.'/resultado/'.$nome_arquivo;
									$arquivo_p7s = storage_path().'/app/public/certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao.'/resultado/'.$nome_arquivo_p7s;
									$arquivo_certificado = storage_path().'/app'.$certificado;
									if ($CeriFuncoes->assinar_arquivo($arquivo,$arquivo_p7s,$cn,$arquivo_certificado)) {
										$novo_resultado->no_arquivo_p7s = $nome_arquivo_p7s;
										$novo_resultado->id_serventia_certificado = $pedido_selecionado->pedido_serventia->serventia->serventia_certificado->id_serventia_certificado;
									}
								}
							}

							if ($novo_resultado->save()) {
								if ($corrompido) {
									$situacao_pedido = $this::ID_SITUACAO_CORRECAO;
								} else {
									$situacao_pedido = $this::ID_SITUACAO_FINALIZADO;
								}
								$pedido->where('id_pedido',$pedido_selecionado->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $situacao_pedido]);
			
								$novo_historico = new historico_pedido();
								$novo_historico->id_pedido = $pedido_selecionado->id_pedido;
								$novo_historico->id_situacao_pedido_grupo_produto = $situacao_pedido;
								$novo_historico->id_alcada = 3;
								$novo_historico->de_observacao = 'Resultado enviado! Resultado positivo, salvo automaticamente. Arquivo: '.$nome_arquivo;
								$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
								$novo_historico->save();

								$dadosSelo = array(
									"id_produto" 		    	 => 26 , // $request->id_produto_item
									"caracter_delimitador"  	 => ';',
									"id_usuario_logado" 		 => Auth::User()->id_usuario,
									"id_usuario_cliente" 		 => Auth::User()->id_usuario,
									"id_usuario_serventia" 		 => Auth::User()->usuario_serventia->id_serventia,
									"nu_ip_maquina_executou_ato" => $request->getClientIp(),
									"modulo_origem" 			 => $this->origem_utilizacao_selo,
									"id_pedido" 			 	=> $pedido_selecionado->id_pedido,
								);

								if (! $pedido->registar_utilizacao_selo( $dadosSelo ) ){
									return 'ERRO_';
								}

								if ($corrompido) {
									return 'SUCESSO_CORROMPIDO';
								} else {
									return 'SUCESSO_AUTO';
								}
							}
						}
					}
				}
				return 'SUCESSO';
			} else {
				return 'ERRO';
			}
		} else {
			return 'ERRO';
		}
	}
	
	public function confirmar_pedido(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido) {
		if ($request->id_pedido>0) {
			if ($pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_ANALISE])) {				   
				$novo_historico = $historico_pedido;
				$novo_historico->id_pedido = $request->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_ANALISE;
				switch(Auth::User()->tipo_usuario->no_tipo_usuario) {
					case 'Cliente':
						$novo_historico->id_alcada = 1;
						break;
					case 'Judiciário':
						$novo_historico->id_alcada = 2;
						break;
				}
				$novo_historico->de_observacao = 'Pedido confirmado com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();
				return 'SUCESSO';
			} else {
				return 'ERRO';
			}
		} else {
			return 'ERRO';
		}
	}
	
	public function cancelar_pedido(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
										 
			if ($pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_CANCELADO])) {				   
				$novo_historico = $historico_pedido;
				$novo_historico->id_pedido = $request->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CANCELADO;
				switch(Auth::User()->tipo_usuario->no_tipo_usuario) {
					case 'Cliente':
						$novo_historico->id_alcada = 1;
						break;
					case 'Judiciário':
						$novo_historico->id_alcada = 2;
						break;
				}
				$novo_historico->de_observacao = 'Pedido cancelado com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();
				return 'SUCESSO';
			} else {
				return 'ERRO';
			}
		} else {
			return 'ERRO';
		}
	}
	
	public function enviar_pedidos(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, matricula $matricula) {
		if (count($request->id_pedido)>0) {

			foreach ($request->id_pedido as $id_pedido)
			{
				$pedido_valor     		= $pedido->find($id_pedido);
				$this->vl_total_pedido += $pedido_valor->va_pedido;

			}

			if ( Auth::User()->saldo_usuario() <  $this->vl_total_pedido )
			{
				return 'ERROSALDO';
			}

			$resposta_automatica = false;
			$corrompido_geral = false;

			foreach ($request->id_pedido as $id_pedido)
			{
				$pedido_selecionado = $pedido->find($id_pedido);
				if ($pedido_selecionado->certidao->in_isenta != 'S' and $pedido_selecionado->produto_itens[0]->produto_item->in_precificacao=='N')
				{
					$ceriFuncoes 		= new CeriFuncoes();

					$auxvl_pedido       = $ceriFuncoes->buscar_valor_produto( $pedido_selecionado->produto_itens[0]->id_produto_item );
					$vl_pedido          = ( $pedido_selecionado->va_pedido > 0 ) ? $pedido_selecionado->va_pedido : $auxvl_pedido;


					if (Auth::User()->saldo_usuario() <  $vl_pedido) {
						return 'ERROSALDO';
					}

					if ( $pedido_selecionado->id_pedido > 0 ) {
						$dados = 	[
							"id_pedido" 					=> $pedido_selecionado->id_pedido,
							"id_usuario" 					=> Auth::User()->id_usuario,
							"id_usuario_logado"				=> Auth::User()->id_usuario,
							"vl_pedido" 					=> converte_float($vl_pedido),
							"tp_movimentacao_financeira" 	=> 2,
							"tp_movimentacao" 				=> 'S',
							"id_forma_pagamento" 			=> 6,
                            "v_id_pedido_desconto"          => 'null',
                            "v_va_desconto"                 => 0,
                            "v_in_desconto"                 => 'N',
                            "v_va_desconto_perc"            => 'null',
                            "v_id_compra_credito"           => $request->id_compra_credito
						];

						$objpedido	= new pedido();
						$objpedido->registrar_valor_pedido( $dados );
					}

					//$retornoDebito      = $ceriFuncoes->processar_debito_cliente( $pedido_selecionado );
					//if ( $retornoDebito == false) {	 return 'ERROSALDO'; }
				}

				if ($pedido_selecionado->certidao->in_isenta=='S' or $pedido_selecionado->produto_itens[0]->produto_item->in_precificacao=='N') {
					$situacao_pedido = $this::ID_SITUACAO_ENCAMINHADO;
				} else {
					$situacao_pedido = $this::ID_SITUACAO_ANALISE;
				}

				$pedido->where('id_pedido',$id_pedido)
					   ->update(['id_situacao_pedido_grupo_produto' => $situacao_pedido]);

				$novo_historico = new historico_pedido();
				$novo_historico->id_pedido = $id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $situacao_pedido;
				switch(Auth::User()->tipo_usuario->no_tipo_usuario) {
					case 'Cliente':
						$novo_historico->id_alcada = 1;
						break;
					case 'Judiciário':
						$novo_historico->id_alcada = 2;
						break;
				}
				$novo_historico->de_observacao = 'Pedido enviado com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();
				
				$busca_matricula = $matricula->where('id_serventia',$pedido_selecionado->pedido_serventia->id_serventia)
											 ->where('codigo_matricula',$pedido_selecionado->certidao->de_chave_certidao)
											 ->first();

				if ($busca_matricula) {
					if (count($busca_matricula->matriculas_digitais)>0) {
						$destino = '../storage/app/public/certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao;
						$destino_final = $destino.'/resultado';
						if (!File::isDirectory($destino)) {
							File::makeDirectory($destino);
						}
						if (!File::isDirectory($destino_final)) {
							File::makeDirectory($destino_final);
						}
						$nome_arquivo = 'matricula_'.$pedido_selecionado->protocolo_pedido.'.pdf';
						$nome_arquivo_p7s = 'matricula_'.$pedido_selecionado->protocolo_pedido.'.p7s';

						$titulo = 'Resultado matrícula digital nº '.$pedido_selecionado->protocolo_pedido;

						$i=1;
						$total = count($busca_matricula->matriculas_digitais);
						$imagens = array();
						$corrompido = false;
						foreach($busca_matricula->matriculas_digitais as $matricula_digital) {
							if (Storage::exists($matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo)) {
								if ($matricula_digital->in_corrompido=='S') {
									$imagens[] = '<h2>Folha corrompida</h2><br /><br />Arquivo corrompido: '.$matricula_digital->no_arquivo;
									$corrompido = true;
									$corrompido_geral = true;
								} else {
									if (in_array($matricula_digital->no_extensao_arquivo,array('tif','tiff'))) {
										if (verifica_img_corrompida(storage_path('app'.$matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo))) {
											$imagens[] = '<h2>Folha corrompida</h2><br /><br />Arquivo corrompido: '.$matricula_digital->no_arquivo;
											$matricula_digital->in_corrompido='S';
											$matricula_digital->save();
											$corrompido = true;
											$corrompido_geral = true;
										} else {
											$imagem = Image::make(storage_path('app'.$matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo));
									    	
									    	$imagens[] = '<img src="data:image/jpg;base64,'.base64_encode($imagem->encode('jpg')).'" width="100%" />';
									    }
									} else {
										$imagens[] = '<img src="'.storage_path('app'.$matricula_digital->no_local_arquivo.'/'.$matricula_digital->no_arquivo).'" width="100%" />';
									}
								}
								if (count($busca_matricula->matriculas_digitais)>1 and $i<$total) {
									$imagens[] = '<div class="page-break"></div>';
								}
							}
							$i++;
						}

						$pdf = PDF::loadView('pdf.certidaomatricula-auto', compact('titulo','imagens'));
							
						if ($pdf->save($destino_final.'/'.$nome_arquivo)) {
							$novo_resultado = new pedido_resultado();
							$novo_resultado->id_pedido = $pedido_selecionado->id_pedido;
							$novo_resultado->in_positivo = 'S';
							$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;
							$novo_resultado->no_arquivo = $nome_arquivo;
							$novo_resultado->no_local_arquivo = 'certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao.'/resultado';;

							if ($pedido_selecionado->pedido_serventia->serventia->serventia_certificado) {
								$certificado = $pedido_selecionado->pedido_serventia->serventia->serventia_certificado->no_local_arquivo.'/'.$pedido_selecionado->pedido_serventia->serventia->serventia_certificado->no_arquivo;
		
								if (Storage::exists($certificado)) {
									$CeriFuncoes = new CeriFuncoes();
									$cn = $pedido_selecionado->pedido_serventia->serventia->serventia_certificado->no_comum;
									$arquivo = storage_path().'/app/public/certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao.'/resultado/'.$nome_arquivo;
									$arquivo_p7s = storage_path().'/app/public/certidoes-matricula/'.$pedido_selecionado->certidao->id_certidao.'/resultado/'.$nome_arquivo_p7s;
									$arquivo_certificado = storage_path().'/app'.$certificado;
									if ($CeriFuncoes->assinar_arquivo($arquivo,$arquivo_p7s,$cn,$arquivo_certificado)) {
										$novo_resultado->no_arquivo_p7s = $nome_arquivo_p7s;
										$novo_resultado->id_serventia_certificado = $pedido_selecionado->pedido_serventia->serventia->serventia_certificado->id_serventia_certificado;
									}
								}
							}

							if ($novo_resultado->save()) {
								if ($corrompido) {
									$situacao_pedido = $this::ID_SITUACAO_CORRECAO;
								} else {
									$situacao_pedido = $this::ID_SITUACAO_FINALIZADO;
								}
								$pedido->where('id_pedido',$pedido_selecionado->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $situacao_pedido]);
			
								$novo_historico = new historico_pedido();
								$novo_historico->id_pedido = $pedido_selecionado->id_pedido;
								$novo_historico->id_situacao_pedido_grupo_produto = $situacao_pedido;
								$novo_historico->id_alcada = 3;
								$novo_historico->de_observacao = 'Resultado enviado! Resultado positivo, salvo automaticamente. Arquivo: '.$nome_arquivo;
								$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
								$novo_historico->save();
			
								$resposta_automatica = true;

								$dadosSelo = array(
									"id_produto" 		    	 => 26 , // $request->id_produto_item
									"caracter_delimitador"  	 => ';',
									"id_usuario_logado" 		 => Auth::User()->id_usuario,
									"id_usuario_cliente" 		 => Auth::User()->id_usuario,
									"id_usuario_serventia" 		 => Auth::User()->usuario_serventia->id_serventia,
									"nu_ip_maquina_executou_ato" => $request->getClientIp(),
									"modulo_origem" 			 => $this->origem_utilizacao_selo,
									"id_pedido" 			 	=> $pedido_selecionado->id_pedido,
								);

								if (! $pedido->registar_utilizacao_selo( $dadosSelo ) ){
									return 'ERRO_';
								}
							}
						}
					}
				}
			}
			if ($resposta_automatica) {
				if ($corrompido_geral) {
					return 'SUCESSO_CORROMPIDO';
				} else {
					return 'SUCESSO_AUTO';
				}
			} else {
				return 'SUCESSO';
			}
		} else {
			return 'ERRO';
		}
	}

	public function nova_resposta(Request $request, pedido $pedido, tipo_certidao $tipo_certidao, tipo_resposta $tipo_resposta) {
		if ($request->id_pedido>0) {
			$class = $this;
			$pedido = $pedido->find($request->id_pedido);
			$tipos = $tipo_certidao->where('in_registro_ativo','S')
								   ->orderBy('nu_ordem','asc')
								   ->get();
			$tipos_resposta = $tipo_resposta->where('in_registro_ativo','S')
								   			->orderBy('nu_ordem','asc')
								   			->get();
			return view('servicos.cartorio.certidaomatricula-resposta',compact('class','pedido','tipos','tipos_resposta'));
		}
	}
	
	public function detalhes(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			return view('servicos.certidaomatricula-detalhes',compact('pedido'));
		}
	}
	public function resultado(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			//respondendo a notificacao do usuario
			$ceriFuncoes = new CeriFuncoes();
			$ceriFuncoes->reponder_notificacao($request->id_pedido,Auth::User()->id_usuario);

			if ($pedido->pedido_resultado) {
				if ($pedido->pedido_resultado->in_positivo=='S') {
					if ($pedido->pedido_resultado->no_arquivo!='') {
						$url_download = URL::to('/arquivos/download/certidao-matricula/'.$pedido->id_pedido);

						if ($pedido->pedido_resultado->no_arquivo_p7s!='') {
							$assinatura_digital = 'true';
						} else {
							$assinatura_digital = 'false';
						}



						return response()->json(array('view'=>view('servicos.certidaomatricula-resultado',compact('pedido'))->render(),'assinatura_digital'=>$assinatura_digital,'download'=>'true','url_download'=>$url_download));
					} else {
						return 'ERRO_01';
					}
				} else {
					return 'ERRO_02';
				}
			} else {
				return 'ERRO_01';
			}
		}
	}
	public function arquivo_resultado(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			$arquivo = Storage::get('/public/'.$pedido->pedido_resultado->no_local_arquivo.'/'.$pedido->pedido_resultado->no_arquivo);
			
			return response($arquivo, 200)->header('Content-Type', 'application/pdf')
										  ->header('Content-Disposition', 'inline; filename="'.$pedido->pedido_resultado->no_arquivo.'"');
		}
	}
	public function download_resultado(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			if ($pedido->pedido_resultado->no_arquivo_p7s!='' and Storage::exists('/public/'.$pedido->pedido_resultado->no_local_arquivo.'/'.$pedido->pedido_resultado->no_arquivo_p7s)) {
				$zip = new \ZipArchive();

 				$nome_arquivo = 'matricula_'.$pedido->protocolo_pedido.'.zip';
 				$dir_arquivo = '/public/'.$pedido->pedido_resultado->no_local_arquivo.'/'.$nome_arquivo;
				$mime = 'application/zip';

 				if (Storage::exists($dir_arquivo)) {
 					Storage::delete($dir_arquivo);
 				}

				if($zip->open('../storage/app'.$dir_arquivo, ZipArchive::CREATE) === true) {
					$zip->addFile('../storage/app/public/'.$pedido->pedido_resultado->no_local_arquivo.'/'.$pedido->pedido_resultado->no_arquivo , $pedido->pedido_resultado->no_arquivo);
					$zip->addFile('../storage/app/public/'.$pedido->pedido_resultado->no_local_arquivo.'/'.$pedido->pedido_resultado->no_arquivo_p7s , $pedido->pedido_resultado->no_arquivo_p7s);
					$zip->close();
				}
			} else {
				$nome_arquivo = $pedido->pedido_resultado->no_arquivo;
				$dir_arquivo = '/public/'.$pedido->pedido_resultado->no_local_arquivo.'/'.$nome_arquivo;
				$mime = 'application/pdf';
			}
			$arquivo = Storage::get($dir_arquivo);
			return response($arquivo, 200)->header('Content-Type', $mime)
										  ->header('Content-Disposition', 'attachment; filename="'.$nome_arquivo.'"');
		}
	}

	public function inserir_andamento(Request $request, pedido $pedido, pedido_serventia $pedido_serventia, pedido_serventia_resposta $resposta, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			
			if ($request->id_tipo_resposta>0) {
				$id_pedido_serventia = $pedido_serventia->where('id_pedido',$request->id_pedido)
														->where('id_serventia',Auth::User()->usuario_serventia->id_serventia)
														->first()->id_pedido_serventia;

				$nova_resposta = $resposta;
				$nova_resposta->id_pedido_serventia = $id_pedido_serventia;
				$nova_resposta->id_tipo_resposta = $request->id_tipo_resposta;
				$nova_resposta->de_resposta = $request->de_resposta;
				$nova_resposta->dt_resposta = Carbon::now();
				$nova_resposta->id_usuario_cad = Auth::User()->id_usuario;
				$nova_resposta->dt_cadastro = Carbon::now();
				
				if ($nova_resposta->save()) {
					$novo_historico = $historico_pedido;
					$novo_historico->id_pedido = $request->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $pedido->id_situacao_pedido_grupo_produto;
					$novo_historico->id_alcada = 3;
					$novo_historico->de_observacao = $nova_resposta->tipo_resposta->no_tipo_resposta.': '.$request->de_resposta;
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					$novo_historico->save();

					return response()->json(array('historico'=>$novo_historico,'usuario'=>Auth::User()->pessoa->no_pessoa,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i')));
				} else {
					return 'ERRO';
				}
			}
		} else {
			return 'ERRO';
		}
	}

	public function inserir_precificacao(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido, pedido_valor $pedido_valor) {
		if ($request->id_pedido>0) {
			if (in_array($request->in_positivo,array('S','N'))) {
				$novo_valor = $pedido_valor;
				$novo_valor->id_pedido = $request->id_pedido;
				if ($request->in_positivo=='S') {
					$situacao_pedido = $this::ID_SITUACAO_APROVACAO;
					$novo_valor->nu_quantidade  = intval($request->nu_quantidade);
					$novo_valor->va_pedido = converte_float($request->va_pedido);
				} elseif ($request->in_positivo=='N') {
					$situacao_pedido = $this::ID_SITUACAO_FINALIZADO;
					$novo_valor->nu_quantidade  = 0;
					$novo_valor->va_pedido = 0;
				}
				$novo_valor->in_positivo = $request->in_positivo;
				$novo_valor->id_usuario_cad = Auth::User()->id_usuario;
			
				if ($novo_valor->save()) {
					$pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $situacao_pedido]);

					$novo_historico = $historico_pedido;
					$novo_historico->id_pedido = $request->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $situacao_pedido;
					$novo_historico->id_alcada = 3;
					$novo_historico->de_observacao = 'A precificação foi salva! Resultado '.($request->in_positivo=='S'?' positivo com '.$request->nu_quantidade.' folhas. Valor do pedido: '.$request->va_pedido:'Negativo. O pedido foi finalizado.');
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					$novo_historico->save();

					if ($request->in_positivo=='N') {
						$novo_resultado = $pedido_resultado;
						$novo_resultado->id_pedido = $request->id_pedido;
						$novo_resultado->in_positivo = $request->in_positivo;
						$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;
						$novo_resultado->save();
					}
					
					return response()->json(array('historico'=>$novo_historico,'usuario'=>Auth::User()->pessoa->no_pessoa,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i')));
				} else {
					return 'ERRO';
				}
			}
		} else {
			return 'ERRO';
		}
	}

	public function inserir_resultado(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido, pedido_resultado $pedido_resultado, pedido_serventia_resposta $pedido_serventia_resposta, usuario $usuario) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			if (in_array($request->in_positivo,array('S','N'))) {
				$novo_resultado = $pedido_resultado;
				$novo_resultado->id_pedido = $request->id_pedido;
				$novo_resultado->in_positivo = $request->in_positivo;
				$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;

				if ($request->in_positivo=='S') {
					$destino = '../storage/app/public/certidoes-matricula/'.$request->id_certidao;
					$destino_final = $destino.'/resultado';
					if (!File::isDirectory($destino)) {
						File::makeDirectory($destino);
					}
					if (!File::isDirectory($destino_final)) {
						File::makeDirectory($destino_final);
					}
					$nome_arquivo = 'matricula_'.$pedido->protocolo_pedido.'.pdf';
					$nome_arquivo_p7s = 'matricula_'.$pedido->protocolo_pedido.'.p7s';

					switch ($request->tipo_resultado_envio) {
						case 1:
							$de_resultado = $request->de_resultado;
							$novo_resultado->de_resultado = $de_resultado;
							$de_historico = $de_resultado;

							$titulo = 'Resultado matrícula digital nº '.$pedido->protocolo_pedido;

							$pdf = PDF::loadView('pdf.certidaomatricula', compact('titulo','pedido','de_resultado'));
							
							if ($pdf->save($destino_final.'/'.$nome_arquivo)) {
								$novo_resultado->no_arquivo = $nome_arquivo;
								$novo_resultado->no_local_arquivo = 'certidoes-matricula/'.$request->id_certidao.'/resultado';
								$de_historico = 'Arquivo: '.$nome_arquivo;
							}
							break;
						case 2:
							if (in_array($request->no_arquivo->getClientOriginalExtension(),array('png','jpg','bmp','tif'))) {
								$request->no_arquivo->move($destino_final, $request->no_arquivo->getClientOriginalName());
								$imagem = Image::make($destino_final.'/'.$request->no_arquivo->getClientOriginalName());

								$pdf = PDF::loadView('pdf.imagem', compact('imagem'));
								$pdf->save($destino_final.'/'.$nome_arquivo);
							} else {
								$request->no_arquivo->move($destino_final, $nome_arquivo);
							}
							$novo_resultado->no_arquivo = $nome_arquivo;
							$novo_resultado->no_local_arquivo = 'certidoes-matricula/'.$request->id_certidao.'/resultado';
							$de_historico = 'Arquivo: '.$nome_arquivo;
							break;
					}
					if (Auth::User()->usuario_serventia->serventia->serventia_certificado) {
						$certificado = Auth::User()->usuario_serventia->serventia->serventia_certificado->no_local_arquivo.'/'.Auth::User()->usuario_serventia->serventia->serventia_certificado->no_arquivo;

						if (Storage::exists($certificado)) {
							$CeriFuncoes = new CeriFuncoes();
							$cn = Auth::User()->usuario_serventia->serventia->serventia_certificado->no_comum;
							$arquivo = storage_path().'/app/public/certidoes-matricula/'.$request->id_certidao.'/resultado/'.$nome_arquivo;
							$arquivo_p7s = storage_path().'/app/public/certidoes-matricula/'.$request->id_certidao.'/resultado/'.$nome_arquivo_p7s;
							$arquivo_certificado = storage_path().'/app'.$certificado;
							if ($CeriFuncoes->assinar_arquivo($arquivo,$arquivo_p7s,$cn,$arquivo_certificado)) {
								$novo_resultado->no_arquivo_p7s = $nome_arquivo_p7s;
								$novo_resultado->id_serventia_certificado = Auth::User()->usuario_serventia->serventia->serventia_certificado->id_serventia_certificado;
							}
						}
					}
				}

				if ($novo_resultado->save()) {
					$pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_FINALIZADO]);

					$novo_historico = $historico_pedido;
					$novo_historico->id_pedido = $request->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_FINALIZADO;
					$novo_historico->id_alcada = 3;
					$novo_historico->de_observacao = 'Resultado enviado! Resultado '.($request->in_positivo=='S'?' positivo. '.$de_historico:'Negativo. O pedido foi finalizado.');
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					$novo_historico->save();

					$novo_pedido_serventia_resposta 						= $pedido_serventia_resposta;
					$novo_pedido_serventia_resposta->id_pedido_serventia 	= $pedido->pedido_serventia->id_pedido_serventia;
					$novo_pedido_serventia_resposta->id_tipo_resposta 		= ($request->in_positivo=='S') ? 5 : 1;
					$novo_pedido_serventia_resposta->de_resposta 			= ($request->in_positivo=='S') ? 'Positivo' : 'Negativo';
					$novo_pedido_serventia_resposta->dt_resposta 			= Carbon::now();
					$novo_pedido_serventia_resposta->id_usuario_cad 		= Auth::User()->id_usuario;
					$novo_pedido_serventia_resposta->dt_cadastro 			= Carbon::now();
					$novo_pedido_serventia_resposta->in_resposta_automatica = 'N';
					$novo_pedido_serventia_resposta->in_positiva 			= $request->in_positivo;

					if ( $novo_pedido_serventia_resposta->save())
					{
						$usuario_encontrado = $usuario->where('id_usuario', $pedido->id_usuario)->first();

						//respondendo todas as notificações para todos os usuarios da serventia
						$ceriFuncoes = new CeriFuncoes();
						$ceriFuncoes->reponder_notificacao($request->id_pedido,0);

						//abrindo uma nova notificacao informando o usuario que a sua solicitacao foi respondida
						$ceriFuncoes->inserir_notificacao('USUARIO',$pedido->id_pedido,'Resposta solicitação de Matrícula digital, protocolo número: '.$pedido->protocolo_pedido,$usuario_encontrado->id_usuario,0);

						Mail::send('email.confirmar-resposta', ['pedido' => $pedido, 'usuario_encontrado'=>$usuario_encontrado], function ($mail) use ($request, $pedido, $usuario_encontrado) {
							$mail->to($usuario_encontrado->email_usuario, $usuario_encontrado->no_usuario)
								->subject('CERI/Matrículoa digital - Você tem uma nova resposta para o seu pedido de protocolo número: '.$pedido->protocolo_pedido);
						});
					}

					return response()->json(array('historico'=>$novo_historico,'usuario'=>Auth::User()->pessoa->no_pessoa,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i')));
				} else {
					return 'ERRO';
				}
			}
		} else {
			return 'ERRO';
		}
	}
	
	public function certificado_detalhes(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$resultado = $pedido->find($request->id_pedido)->pedido_resultado;
			
			return view('certificado.certificado-detalhes',compact('resultado'));
		}
	}

	public function inserir_correcao(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, historico_pedido $historico_pedido, pedido_resultado $pedido_resultado) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			$novo_resultado = $pedido_resultado->find($pedido->pedido_resultado->id_pedido_resultado);
			$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;

			$destino = '../storage/app/public/certidoes-matricula/'.$request->id_certidao.'/resultado';

			$nome_arquivo = 'matricula_'.$pedido->protocolo_pedido.'.pdf';
			$nome_arquivo_p7s = 'matricula_'.$pedido->protocolo_pedido.'.p7s';

			unlink($destino.'/'.$nome_arquivo);
			unlink($destino.'/'.$nome_arquivo_p7s);

			if ($request->no_arquivo->move($destino, $nome_arquivo)) {
				$novo_resultado->no_arquivo = $nome_arquivo;
				$novo_resultado->no_local_arquivo = 'certidoes-matricula/'.$request->id_certidao.'/resultado';
				$de_historico = 'Arquivo: '.$nome_arquivo;
			}

			if (Auth::User()->usuario_serventia->serventia->serventia_certificado) {
				$certificado = Auth::User()->usuario_serventia->serventia->serventia_certificado->no_local_arquivo.'/'.Auth::User()->usuario_serventia->serventia->serventia_certificado->no_arquivo;

				if (Storage::exists($certificado)) {
					$CeriFuncoes = new CeriFuncoes();
					$cn = Auth::User()->usuario_serventia->serventia->serventia_certificado->no_comum;
					$arquivo = storage_path().'/app/public/certidoes-matricula/'.$request->id_certidao.'/resultado/'.$nome_arquivo;
					$arquivo_p7s = storage_path().'/app/public/certidoes-matricula/'.$request->id_certidao.'/resultado/'.$nome_arquivo_p7s;
					$arquivo_certificado = storage_path().'/app'.$certificado;
					if ($CeriFuncoes->assinar_arquivo($arquivo,$arquivo_p7s,$cn,$arquivo_certificado)) {
						$novo_resultado->no_arquivo_p7s = $nome_arquivo_p7s;
						$novo_resultado->id_serventia_certificado = Auth::User()->usuario_serventia->serventia->serventia_certificado->id_serventia_certificado;
					}
				}
			}

			if ($novo_resultado->save()) {
				$pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_FINALIZADO]);

				$novo_historico = $historico_pedido;
				$novo_historico->id_pedido = $request->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_FINALIZADO;
				$novo_historico->id_alcada = 3;
				$novo_historico->de_observacao = 'Resultado corrigido! O pedido foi finalizado. '.$de_historico;
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();

				return response()->json(array('historico'=>$novo_historico,'usuario'=>Auth::User()->pessoa->no_pessoa,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i')));
			} else {
				return 'ERRO';
			}
		} else {
			return 'ERRO';
		}
	}
}
