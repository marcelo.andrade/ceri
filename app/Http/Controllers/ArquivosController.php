<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use Storage;
use Session;
use PDF;
use Image;
use URL;

use App\arquivo_grupo_produto;
use App\tipo_arquivo_grupo_produto;
use App\usuario_certificado;

class ArquivosController extends Controller {
	
	public function __construct() {
		
	}
	
	public function novo(Request $request) {
		return view('arquivos.arquivos-novo',compact('request'));
	}
	
	public function inserir(Request $request, tipo_arquivo_grupo_produto $tipo_arquivo_grupo_produto) {
		$tipo_arquivo_grupo_produto = $tipo_arquivo_grupo_produto->find($request->id_tipo_arquivo_grupo_produto);
		if ($tipo_arquivo_grupo_produto) {	
			if (!$request->session()->has('datapasta_'.$request->token)) {
				$request->session()->put('datapasta_'.$request->token,Carbon::now()->format('d-m-Y'));
				
			}
			$data_pasta = $request->session()->get('datapasta_'.$request->token);

			switch ($request->id_tipo_arquivo_grupo_produto) {
				case 9: case 10: case 11: case 12: case 13:	
					$tipo_pasta = 'penhora';
					break;
				case 15: case 16:
					$tipo_pasta = 'oficio';
					break;
				case 18: case 19:
					$tipo_pasta = 'pesquisa';
					break;
				case 20: case 21:
					$tipo_pasta = 'certidao';
					break;
				case 1:
				case 22:
				case 26:
					$tipo_pasta = 'alienacao';
					break;
                case 25:
                    $tipo_pasta = 'observacao';
                    break;
			}
			$destino = '/temp/'.$tipo_pasta.'/'.$data_pasta.'/'.$request->token.'/'.$request->id_tipo_arquivo_grupo_produto;
			$arquivo = $request->no_arquivo;
			$nome_arquivo = strtolower(remove_caracteres($arquivo->getClientOriginalName()));
			$nome_arquivos_originais = array();
			
			Storage::makeDirectory($destino);
	
			if ($arquivo->move(storage_path('app'.$destino), $nome_arquivo)) {
				$ext_arquivo = extensao_arquivo($nome_arquivo);
				switch ($ext_arquivo) {
					case 'png': case 'jpg': case 'bmp': case 'tif':
						array_push($nome_arquivos_originais,$nome_arquivo);

						$imagem = Image::make(storage_path('app'.$destino.'/'.$nome_arquivo));
						$nome_arquivo = str_replace('.'.$ext_arquivo,'.pdf',$nome_arquivo);
						$pdf = PDF::loadView('pdf.imagem', compact('imagem'));
						$pdf->save(storage_path('app'.$destino.'/'.$nome_arquivo));
						break;
					case 'doc': case 'docx': case 'xls': case 'xlsx': case 'ppt': case 'pptx': case 'pps': case 'ppsx':
						array_push($nome_arquivos_originais,$nome_arquivo);

						if (env('LIBOFFICE_ENV')=='local') {
							$processo = new Process('"C:\Program Files\LibreOffice 4\program\soffice.exe" --headless --invisible --convert-to pdf:writer_pdf_Export --outdir "'.storage_path('app'.$destino).'/" "'.storage_path('app'.$destino.'/'.$nome_arquivo).'"');
						} else {
							$processo = new Process('sudo libreoffice --headless --invisible --convert-to pdf:writer_pdf_Export --outdir "'.storage_path('app'.$destino).'/" "'.storage_path('app'.$destino.'/'.$nome_arquivo).'"');
						}

						$processo->run();
						$nome_arquivo = str_replace('.'.$ext_arquivo,'.pdf',$nome_arquivo);
						break;
					case 'txt':
						array_push($nome_arquivos_originais,$nome_arquivo);

						$conteudo_arquivo = Storage::get($destino.'/'.$nome_arquivo);
						$nome_arquivo = str_replace('.txt','.pdf',$nome_arquivo);
						$pdf = PDF::loadHTML($conteudo_arquivo);
						$pdf->save(storage_path('app'.$destino.'/'.$nome_arquivo));
						break;
					default:
						break;
				}

				if ($request->session()->has('arquivos_'.$request->token)) {
					$arquivos = $request->session()->get('arquivos_'.$request->token);
				} else {
					$arquivos = array();
				}

				$arquivo = array('no_arquivo'=>$nome_arquivo,
								 'no_arquivos_originais'=>$nome_arquivos_originais,
								 'no_local_arquivo'=>$destino,
								 'in_ass_digital'=>$tipo_arquivo_grupo_produto->in_ass_digital,
								 'id_tipo_arquivo_grupo_produto'=>$tipo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto,
								 'in_assinado'=>'N',
								 'dt_assinado'=>'',
								 'id_usuario_certificado'=>0,
								 'no_arquivo_p7s'=>'',
								 'id_flex'=>$request->id_flex,
								 'protocolo'=>$request->protocolo);

				$arquivos[$request->index_arquivos] = $arquivo;
		
				$request->session()->put('arquivos_'.$request->token,$arquivos);
				
				return response()->json(array('status' => 'sucesso',
											  'msg'=> 'O arquivo foi inserido com sucesso',
											  'arquivo'=> $arquivo,
											  'token'=>$request->token));
			} else {
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
			}
		}
	}
	public function remover(Request $request) {
		if ($request->session()->has('arquivos_'.$request->token)) {
			$arquivos = $request->session()->get('arquivos_'.$request->token);
			$arquivo = $arquivos[$request->linha];
			if (Storage::delete($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'])) {
				if (count($arquivo['no_arquivos_originais'])>0) {
					foreach ($arquivo['no_arquivos_originais'] as $arquivo_original) {
						Storage::delete($arquivo['no_local_arquivo'].'/'.$arquivo_original);
					}
				}
				unset($arquivos[$request->linha]);
				$request->session()->put('arquivos_'.$request->token,$arquivos);
				return response()->json(array('status' => 'sucesso',
											  'msg'=> 'O arquivo foi removido com sucesso',
											  'arquivo'=> $arquivo));
			} else {
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
			}
		} else {
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
		}
	}
	public function visualizar_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
		if ($request->id_arquivo_grupo_produto>0) {

			$arquivo_grupo_produto = $arquivo_grupo_produto->find($request->id_arquivo_grupo_produto);

			if ($arquivo_grupo_produto) {
				$url_download = URL::to('/arquivos/download/'.$arquivo_grupo_produto->id_arquivo_grupo_produto);

				return response()->json(array('status' => 'sucesso',
											  'view'=>view('arquivos.arquivos-visualizar',compact('arquivo_grupo_produto'))->render(),
											  'download'=>'true',
											  'url_download'=>$url_download));
			} else {
				return response()->json(array('status' => 'erro',
										 	  'recarrega' => 'false',
										  	  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
			}
		}
	}
	public function render_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
		if ($request->id_arquivo_grupo_produto>0) {
			$arquivo_grupo_produto = $arquivo_grupo_produto->find($request->id_arquivo_grupo_produto);

			$arquivo = Storage::get('/public/'.$arquivo_grupo_produto->no_local_arquivo.'/'.$arquivo_grupo_produto->no_arquivo);
			
			$mimes = array('pdf' => 'application/pdf',
						   'jpg' => 'image/jpeg',
						   'png' => 'image/png',
						   'bmp' => 'image/bmp',
						   'gif' => 'image/gif');
						   
			return response($arquivo, 200)->header('Content-Type', $mimes[$arquivo_grupo_produto->no_extensao])
										  ->header('Content-Disposition', 'inline; filename="'.$arquivo_grupo_produto->no_arquivo.'"');
		}
	}
	public function download_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
		if ($request->id_arquivo_grupo_produto>0) {
			$arquivo_grupo_produto = $arquivo_grupo_produto->find($request->id_arquivo_grupo_produto);

			if ($arquivo_grupo_produto) {
				$no_arquivo = $arquivo_grupo_produto->no_arquivo;
				
				$arquivo = Storage::get('/public/'.$arquivo_grupo_produto->no_local_arquivo.'/'.$no_arquivo);
				return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo.'"');
			}
		}
	}
	public function certificado_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto) {
		if ($request->id_arquivo_grupo_produto>0) {
			$arquivo_grupo_produto = $arquivo_grupo_produto->find($request->id_arquivo_grupo_produto);

			return view('arquivos.arquivos-certificado',compact('arquivo_grupo_produto'));
		}
	}
	
	public function nova_assinatura(Request $request) {
		return view('arquivos.arquivos-nova-assinatura',compact('request'));
	}

	public function iniciar_assinatura(Request $request) {
		if ($request->session()->has('arquivos_'.$request->arquivos_token)) {
			$arquivos = $request->session()->get('arquivos_'.$request->arquivos_token);

			$arquivo = $arquivos[$request->id];
			if ($arquivo['in_ass_digital']=='S') {
				$RestPki = new \Lacuna\RestPkiClient(env('RESTPKI_URL'), env('RESTPKI_TOKEN'));
				switch (extensao_arquivo($arquivo['no_arquivo'])) {
					case 'pdf':
						$signatureStarter = new \Lacuna\PadesSignatureStarter($RestPki);
						$signatureStarter->measurementUnits = \Lacuna\PadesMeasurementUnits::CENTIMETERS;
						$signatureStarter->setPdfToSignPath(storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
						$signatureStarter->setSignaturePolicy(\Lacuna\StandardSignaturePolicies::PADES_BASIC);
						if (env('APP_ENV')=='production') {
							$signatureStarter->setSecurityContext(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
						} else {
							$signatureStarter->setSecurityContext('803517ad-3bbc-4169-b085-60053a8f6dbf');
						}
						$signatureStarter->setVisualRepresentation([
							'text' => [
								'text' => '',
								'includeSigningTime' => false,
								'horizontalAlign' => 'Left',
								'container' => [
									'left' => 0,
									'top' => 0,
									'right' => 1.5,
									'bottom' => 0
								],
								
							],
							'image' => [
								'resource' => [
									'content' => base64_encode(file_get_contents(public_path().'/images/logo-assinatura01.png')),
									'mimeType' => 'image/png'
								],
								'opacity' => 50,
								'horizontalAlign' => 'Right',
								'verticalAlign' => 'Center',
							],
							'position' => \Lacuna\PadesVisualElements::getVisualRepresentationPosition(7)
						]);

						$text = 'Assinado digitalmente por {{signerName}} ({{signerNationalId}}) em ' . Carbon::now()->format('d/m/Y H:i') . '.' .
                                (!empty($arquivo['protocolo']) ? ' Protocolo: ' . $arquivo['protocolo'] . '. Para conferir a validade desta certidão acesse: www.cerims.com.br' : '') .
                                (!empty($arquivo['protocolo_pedido_arquivo']) ? ' Protocolo (validação carta): ' . $arquivo['protocolo_pedido_arquivo'] . '. Para conferir a validade desta carta acesse: www.cerims.com.br' : '');

						array_push($signatureStarter->pdfMarks, \Lacuna\PadesVisualElements::getPdfMark(5,$text));
						break;
					default:
						$signatureStarter = new \Lacuna\CadesSignatureStarter($RestPki);
						$signatureStarter->setFileToSign(storage_path().'/app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
						$signatureStarter->setSignaturePolicy(\Lacuna\StandardSignaturePolicies::CADES_ICPBR_ADR_BASICA);
						if (env('APP_ENV')=='production') {
							$signatureStarter->setSecurityContext(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
						} else {
							$signatureStarter->setSecurityContext('803517ad-3bbc-4169-b085-60053a8f6dbf');
						}
						$signatureStarter->setEncapsulateContent(true);
						break;
				}
				$token = $signatureStarter->startWithWebPki();
				return json_encode(array('token'=>$token,'id'=>$request->id,'no_arquivo'=>$arquivo['no_arquivo'],'arquivos_token'=>$request->arquivos_token));
			}
		}
	}

	public function finalizar_assinatura(Request $request) {
		if (!empty($request->token)) {
			if ($request->session()->has('arquivos_'.$request->arquivos_token)) {
				$arquivos = $request->session()->get('arquivos_'.$request->arquivos_token);
				$arquivo = $arquivos[$request->id];
				$ext_arquivo = extensao_arquivo($arquivo['no_arquivo']);
				
				$RestPki = new \Lacuna\RestPkiClient(env('RESTPKI_URL'), env('RESTPKI_TOKEN'));
				
				switch ($ext_arquivo) {
					case 'pdf':
						$signatureFinisher = new \Lacuna\PadesSignatureFinisher($RestPki);
						$signatureFinisher->setToken($request->token);
						$signedFile = $signatureFinisher->finish();
						$signerCert = $signatureFinisher->getCertificateInfo();
		
						$nome_arquivo_ass = str_replace('.'.$ext_arquivo,'.assinado.'.$ext_arquivo,$arquivo['no_arquivo']);
						array_push($arquivos[$request->id]['no_arquivos_originais'],$arquivos[$request->id]['no_arquivo']);
						$arquivos[$request->id]['no_arquivo'] = $nome_arquivo_ass;
						break;
					default:
						$signatureFinisher = new \Lacuna\CadesSignatureFinisher($RestPki);
						$signatureFinisher->setToken($request->token);
						$signedFile = $signatureFinisher->finish();
						$signerCert = $signatureFinisher->getCertificateInfo();

						$nome_arquivo_ass = str_replace('.'.$ext_arquivo,'.p7s',$arquivo['no_arquivo']);
						$arquivos[$request->id]['no_arquivo_p7s'] = $nome_arquivo_ass;
						break;
				}
				file_put_contents(storage_path('app'.$arquivo['no_local_arquivo'].'/'.$nome_arquivo_ass), $signedFile);

				$usuario_certificado = new usuario_certificado();
				$usuario_certificado = $usuario_certificado->where('nu_serial',$signerCert->serialNumber)
														   ->where('id_usuario',Auth::User()->id_usuario)
														   ->first();

				if ($usuario_certificado) {
					$arquivos[$request->id]['id_usuario_certificado'] = $usuario_certificado->id_usuario_certificado;
				} else {
					$novo_usuario_certificado = new usuario_certificado();
					$novo_usuario_certificado->id_usuario = Auth::User()->id_usuario;
					$novo_usuario_certificado->no_comum = $signerCert->subjectName->commonName;
					$novo_usuario_certificado->no_email = $signerCert->emailAddress;
					$novo_usuario_certificado->no_autoridade_raiz = $signerCert->issuerName->organization;
					$novo_usuario_certificado->no_autoridade_unidade = $signerCert->issuerName->organizationUnit;
					$novo_usuario_certificado->no_autoridade_certificadora = $signerCert->issuerName->commonName;
					$novo_usuario_certificado->nu_serial = $signerCert->serialNumber;
					$novo_usuario_certificado->dt_validade_ini = Carbon::parse($signerCert->validityStart);
					$novo_usuario_certificado->dt_validade_fim = Carbon::parse($signerCert->validityEnd);
					$novo_usuario_certificado->tp_certificado = $signerCert->pkiBrazil->certificateType;
					if (!empty($signerCert->pkiBrazil->cpf)) {
						$novo_usuario_certificado->nu_cpf_cnpj = $signerCert->pkiBrazil->cpf;
					} elseif (!empty($signerCert->pkiBrazil->cnpj)) {
						$novo_usuario_certificado->nu_cpf_cnpj = $signerCert->pkiBrazil->cnpj;
					}
					$novo_usuario_certificado->no_responsavel = $signerCert->pkiBrazil->responsavel;
					$novo_usuario_certificado->dt_nascimento = Carbon::parse($signerCert->pkiBrazil->dateOfBirth);
					$novo_usuario_certificado->nu_rg = $signerCert->pkiBrazil->rgNumero;
					$novo_usuario_certificado->nu_rgemissor = $signerCert->pkiBrazil->rgEmissor;
					$novo_usuario_certificado->nu_rguf = $signerCert->pkiBrazil->rgEmissorUF;
					$novo_usuario_certificado->de_campos = json_encode($signerCert);

					$novo_usuario_certificado->save();
					$arquivos[$request->id]['id_usuario_certificado'] = $novo_usuario_certificado->id_usuario_certificado;
				}
				
				$arquivos[$request->id]['in_assinado'] = 'S';
				$arquivos[$request->id]['dt_assinado'] = Carbon::now();
				$request->session()->put('arquivos_'.$request->arquivos_token,$arquivos);

				return json_encode(array('id'=>$request->id,'no_arquivo_ass'=>$nome_arquivo_ass,'no_arquivo'=>$arquivo['no_arquivo']));
			}
		}
	}

    /**
     * Remover todos os arquivos da sessão
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remover_todos(Request $request) {
        if ($request->session()->has('arquivos_'.$request->token)) {
            $arquivos = $request->session()->get('arquivos_'.$request->token);

            foreach ($arquivos as $key => $arquivo) {

                if (Storage::delete($arquivo['no_local_arquivo'] . '/' . $arquivo['no_arquivo'])) {
                    if (count($arquivo['no_arquivos_originais']) > 0) {
                        foreach ($arquivo['no_arquivos_originais'] as $arquivo_original) {
                            Storage::delete($arquivo['no_local_arquivo'] . '/' . $arquivo_original);
                        }
                    }
                    unset($arquivos[$key]);
                    $request->session()->put('arquivos_' . $request->token, $arquivos);
                } else {
                    return response()->json(array('status' => 'erro',
                        'recarrega' => 'false',
                        'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                }
            }
        }
    }
}
