<?php
namespace App\Http\Controllers;

use App\agencia;
use App\alienacao_arquivo_xml;
use App\alienacao_arquivo_xml_registro_status;
use App\alienacao_projecao;
use App\andamento_alienacao_excluido;
use App\alienacao_valor_repasse;
use App\banco_pessoa;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\CeriFuncoes;
use App\produto_item;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;
use XMLReader;
use DOMDocument;
use Excel;

use App\estado;
use App\cidade;
use App\produto;
use App\serventia;
use App\pedido;
use App\pedido_serventia;
use App\pedido_produto_item;
use App\historico_pedido;
use App\situacao_pedido_grupo_produto;
use App\alienacao;
use App\credor_alienacao;
use App\alienacao_pedido;
use App\alienacao_devedor;
use App\alienacao_devedor_devedor;
use App\endereco_alienacao_devedor;
use App\arquivo_grupo_produto;
use App\arquivo_grupo_produto_composicao;
use App\alienacao_arquivo_grupo_produto;
use App\alienacao_fase_grupo_produto;
use App\alienacao_credor_alienacao;
use App\endereco;
use App\fase_grupo_produto;
use App\etapa_fase;
use App\acao_etapa;
use App\andamento_alienacao;
use App\resultado_acao;
use App\endereco_alienacao;
use App\parcela_alienacao;
use App\alienacao_valor;
use App\usuario_certificado;
use App\tipo_arquivo_grupo_produto;
use App\andamento_arquivo_grupo;
use App\alienacao_observacao;
use App\baixa_arquivo_xml;

class AlienacaoController extends Controller {

    const ID_GRUPO_PRODUTO = 7;
    const ID_PRODUTO = 19;
    const ID_SITUACAO_CADASTRADO = 54;
    const ID_SITUACAO_EMPROCESSAMENTO = 55;
    const ID_SITUACAO_AGUARDANDOINTERACAO = 56;
    const ID_SITUACAO_FINALIZADO = 57;
    const ID_SITUACAO_ORCAMENTOGERADO = 63;
    const ID_SITUACAO_DEVOLVIDO = 64;
    const ID_SITUACAO_AGUARDANDOPAGAMENTO = 65;
    const ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ = 66;
    const ID_SITUACAO_CANCELADO = 67;
    const ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO = 68;
    const ID_SITUACAO_AGUARDANDO_AUTORIZACAO_DECURSO_PRAZO = 69;
    const ID_FASE_CADASTRO = 1;
    const ID_FASE_APROVACAO = 2;
    const ID_FASE_NOTIFICACAO = 3;
    const ID_TIPO_PESSOA_CAIXA  = 8;
    const ID_TIPO_PESSOA_ANOREG = 9;
    const ID_TIPO_PESSOA_SERVENTIA = 2;
    const ID_REPASSE_SERVENTIA = 3;
    const ID_REPASSE_CAIXA = 1;
    const ID_REPASSE_ANOREG = 3;
    const ID_PRODUTO_ITEM_SERVICOS_ANOREG = 28;
    const ID_PRODUTO_ITEM_EMOLUMENTOS = 30;
    const ID_PRODUTO_ITEM_AR = 31;
    const ID_PRODUTO_ITEM_DILIGENCIA = 34;
    const ID_PRODUTO_ITEM_EDITAL = 35;
    const ID_PRODUTO_ITEM_CERTIDAO_DECURSO = 36;
    const ID_PRODUTO_ITEM_CERTIDAO_INTEIRO_TEOR = 37;
    const ID_PRODUTO_ITEM_AVERBACAO = 38;
    const ID_PRODUTO_ITEM_AVERBACAO_LEGALIZACAO = 39;
    const ID_PRODUTO_ITEM_NOVO_ENDERECO = 40;
    const ID_REPASSE_ANOREG_SERVENTIA = 4;
    
    public function __construct(alienacao $alienacao) {
        ini_set('max_input_vars', 3000);//aumentando o numero de post no sistema
        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');

            $this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
            $this->id_pessoa = $pessoa_ativa->id_pessoa;
            $this->modulos = $pessoa_ativa->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
        } else {
            $this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
            $this->id_pessoa = Auth::User()->id_pessoa;
            $this->modulos = Auth::User()->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
        }
    }

    /*
     * CASO TENHA CRIADO UMA NOVO ID_situacao_pedido_grupo_produto
     * COLOCAR O CASE NOS ARQUIVOS
     * geral-alienacao-novo-andamento.blade.php e serventia-alienacao-novo-andamento.blade.php
     */
    public function index(Request $request, alienacao $alienacao, estado $estado, serventia $serventia){
        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
        $class = $this;

        if ($request->cidade) {
            $serventias = $serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
                                    ->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
                                    ->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                                    ->where('endereco.id_cidade',$request->cidade)
                                    ->whereIn('serventia.id_tipo_serventia',array(1,3))
                                    ->where('serventia.in_registro_ativo','S')
                                    ->orderBy('serventia.no_serventia')
                                    ->get();
        } else {
            $serventias = array();
        }

        $alienacao_regras = $alienacao->regras_filtros()['regras'];
        $alienacao_titulos = $alienacao->regras_filtros()['titulos'];
        $alienacao_alcadas = $alienacao->regras_filtros()['alcadas'];

        $modulos = $this->modulos;

        switch ($request->ordby) {
            case 'ultima-mov':
                $orderBy = 'alienacao_andamento_atual.dt_cadastro';
                break;
            case 'data': default:
                $orderBy = 'alienacao.dt_cadastro';
                break;
        }

        switch ($this->id_tipo_pessoa) {
            case 8:
                $todas_alienacoes = $alienacao->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                              ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                              ->join('alienacao_andamento_atual',function($join) {
                                                    $join->on('alienacao_andamento_atual.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                                                          ->where('alienacao_andamento_atual.in_registro_ativo','=','S');
                                              })
                                              ->where('pedido.id_pessoa_origem','=',$this->id_pessoa);

                if  ($request->dt_ult_mov!='') {
                    $dt_ult_mov_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ult_mov . ' 00:00:00');
                    $dt_ult_mov_fim = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ult_mov . ' 23:59:59');
                    $todas_alienacoes->whereBetween('alienacao_andamento_atual.dt_cadastro',array($dt_ult_mov_ini,$dt_ult_mov_fim));
                }
                if ($request->dt_ini!='' && $request->dt_fim!='') {
                    $dt_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ini.' 00:00:00');
                    $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                    $todas_alienacoes->whereBetween('pedido.dt_pedido',array($dt_ini,$dt_fim));
                }
                if ($request->protocolo > 0){
                    $todas_alienacoes->where('pedido.protocolo_pedido',$request->protocolo);
                }
                if($request->contrato>0){
                    $todas_alienacoes->where('alienacao.numero_contrato',$request->contrato);
                }
                if($request->cpf>0){
                    $cpf = preg_replace('#[^0-9]#','',$request->cpf);

                    $todas_alienacoes->join('alienacao_devedor_devedor','alienacao_devedor_devedor.id_alienacao','=','alienacao.id_alienacao')
                                     ->join('alienacao_devedor',function($join) use ($cpf) {
                                         $join->on('alienacao_devedor.id_alienacao_devedor','=','alienacao_devedor_devedor.id_alienacao_devedor')
                                              ->where('alienacao_devedor.nu_cpf_cnpj','=',$cpf);
                                     });
                }
                if ($request->serventia>0) {
                    $todas_alienacoes->join('pedido_pessoa','pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                     ->join('serventia',function($join) use ($request) {
                                         $join->on('serventia.id_pessoa','=','pedido_pessoa.id_pessoa')
                                              ->where('serventia.id_serventia','=',$request->serventia);
                                     });
                }

                if (!empty($request->grupo) or !empty($request->filtro)) {
                    if (!empty($request->grupo) and !is_array($request->grupo)) {
                        $replace['grupo'] = array($request->grupo);
                    }
                    if (!empty($request->filtro) and !is_array($request->filtro)) {
                        $replace['filtro'] = array($request->filtro);
                    }
                    if (isset($replace)) {
                        $request->replace($replace);
                    }
                    if (in_array('observacoes',$request->grupo)) {
                        $todas_alienacoes->join('alienacao_observacao', 'alienacao_observacao.id_alienacao','=','alienacao.id_alienacao')
                                         ->where('alienacao_observacao.in_leitura','N')
                                         ->where('alienacao_observacao.id_pessoa_dest',$this->id_pessoa);
                    } else {
                        if (count($request->grupo)>0 and count($request->filtro)<=0) {
                            foreach ($request->grupo as $key => $grupo) {
                                foreach ($alienacao_regras[$grupo] as $key => $regra) {
                                    $regras_array[] = $regra;
                                }
                            }
                        } elseif (count($request->grupo)>0 and count($request->filtro)>0) {
                            foreach ($request->filtro as $key => $filtro) {
                                $filtro_array = explode('_',$filtro);
                                $regras_array[] = $alienacao_regras[$filtro_array[0]][$filtro_array[1]];
                            }
                        }

                        $todas_alienacoes = $alienacao->aplicar_regras($todas_alienacoes,$regras_array);
                    }
                }

                $todas_alienacoes_total = $todas_alienacoes->distinct('alienacao.*')->count('alienacao.*');

                switch($request->total_paginacao) {
                    case 10: case 50: case 100: case 500:
                        $total_paginacao = $request->total_paginacao;
                        break;
                    case -1:
                        $total_paginacao = $todas_alienacoes_total;
                        break;
                    default:
                        $total_paginacao = 10;
                        break;
                }

                $todas_alienacoes = $todas_alienacoes->distinct()->orderBy('alienacao.dt_cadastro','desc')->paginate($total_paginacao, ['alienacao.*'], 'pag');

                $todas_alienacoes->appends(Request::capture()->except('_token'))->render();

                return view('servicos.alienacao.geral-alienacao',compact('class','this','request','alienacao','cidades','modulos','serventias','alienacao_regras','alienacao_titulos','alienacao_alcadas','todas_alienacoes','todas_alienacoes_total'));

                break;
            case 9:
            case 13:
                $todas_alienacoes = $alienacao->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                              ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                              ->join('alienacao_andamento_atual',function($join) {
                                                    $join->on('alienacao_andamento_atual.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                                                         ->where('alienacao_andamento_atual.in_registro_ativo','=','S');
                                              });

                if ($request->dt_ini!='' and $request->dt_fim!='') {
                    $dt_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ini.' 00:00:00');
                    $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                    $todas_alienacoes->whereBetween('pedido.dt_pedido',array($dt_ini,$dt_fim));
                }
                if($request->dt_ult_mov!=''){
                    $dt_ult_mov_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ult_mov . ' 00:00:00');
                    $dt_ult_mov_fim = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ult_mov . ' 23:59:59');
                    $todas_alienacoes->whereBetween('alienacao_andamento_atual.dt_cadastro',array($dt_ult_mov_ini,$dt_ult_mov_fim));
                }
                if ($request->protocolo > 0){
                    $todas_alienacoes->where('pedido.protocolo_pedido',$request->protocolo);
                }
                if($request->contrato>0){
                    $todas_alienacoes->where('alienacao.numero_contrato',$request->contrato);
                }
                if($request->cpf>0){
                    $cpf = preg_replace('#[^0-9]#','',$request->cpf);

                    $todas_alienacoes->join('alienacao_devedor_devedor','alienacao_devedor_devedor.id_alienacao','=','alienacao.id_alienacao')
                                     ->join('alienacao_devedor',function($join) use ($cpf) {
                                         $join->on('alienacao_devedor.id_alienacao_devedor','=','alienacao_devedor_devedor.id_alienacao_devedor')
                                              ->where('alienacao_devedor.nu_cpf_cnpj','=',$cpf);
                                     });
                }
                if ($request->serventia>0) {
                    $todas_alienacoes->join('pedido_pessoa','pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                     ->join('serventia',function($join) use ($request) {
                                         $join->on('serventia.id_pessoa','=','pedido_pessoa.id_pessoa')
                                              ->where('serventia.id_serventia','=',$request->serventia);
                                     });
                }
                
                if (!empty($request->grupo) or !empty($request->filtro)) {
                    if (!empty($request->grupo) and !is_array($request->grupo)) {
                        $replace['grupo'] = array($request->grupo);
                    }
                    if (!empty($request->filtro) and !is_array($request->filtro)) {
                        $replace['filtro'] = array($request->filtro);
                    }
                    if (isset($replace)) {
                        $request->replace($replace);
                    }
                    if (in_array('observacoes',$request->grupo)) {
                        $todas_alienacoes->join('alienacao_observacao', 'alienacao_observacao.id_alienacao','=','alienacao.id_alienacao')
                                         ->where('alienacao_observacao.in_leitura','N')
                                         ->where('alienacao_observacao.id_pessoa_dest',$this->id_pessoa);
                    } else {
                        if (count($request->grupo)>0 and count($request->filtro)<=0) {
                            foreach ($request->grupo as $key => $grupo) {
                                foreach ($alienacao_regras[$grupo] as $key => $regra) {
                                    $regras_array[] = $regra;
                                }
                            }
                        } elseif (count($request->grupo)>0 and count($request->filtro)>0) {
                            foreach ($request->filtro as $key => $filtro) {
                                $filtro_array = explode('_',$filtro);
                                $regras_array[] = $alienacao_regras[$filtro_array[0]][$filtro_array[1]];
                            }
                        }

                        $todas_alienacoes = $alienacao->aplicar_regras($todas_alienacoes,$regras_array);
                    }
                }

                $todas_alienacoes_total = $todas_alienacoes->distinct('alienacao.*')->count('alienacao.*');

                switch($request->total_paginacao) {
                    case 10: case 50: case 100: case 500:
                        $total_paginacao = $request->total_paginacao;
                        break;
                    case -1:
                        $total_paginacao = $todas_alienacoes_total;
                        break;
                    default:
                        $total_paginacao = 10;
                        break;
                }

                $todas_alienacoes = $todas_alienacoes->distinct()->orderBy('alienacao.dt_cadastro','desc')->paginate($total_paginacao, ['alienacao.*'], 'pag');

                $todas_alienacoes->appends(Request::capture()->except('_token'))->render();

                return view('servicos.alienacao.geral-alienacao',compact('class','this','request','alienacao','cidades','modulos','serventias','alienacao_regras','alienacao_titulos','alienacao_alcadas','todas_alienacoes','todas_alienacoes_total'));

                break;
            case 2:
            case 10:
                $todas_alienacoes = $alienacao->select('alienacao.*','alienacao_pedido.*','pedido.*','alienacao_andamento_atual.dt_cadastro')
                                              ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                              ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                              ->join('pedido_pessoa',function($join) {
                                                  $join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
                                                       ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
                                              })
                                              ->join('alienacao_andamento_atual',function($join) {
                                                  $join->on('alienacao_andamento_atual.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                                                       ->where('alienacao_andamento_atual.in_registro_ativo','=','S');
                                              })
                                              ->where('pedido.id_situacao_pedido_grupo_produto','<>',$this::ID_SITUACAO_CADASTRADO);

                if ($request->dt_ini!='' and $request->dt_fim!='') {
                    $dt_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ini.' 00:00:00');
                    $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                    $todas_alienacoes->whereBetween('pedido.dt_pedido',array($dt_ini,$dt_fim));
                }
                if ($request->protocolo>0){
                    $todas_alienacoes->where('pedido.protocolo_pedido',$request->protocolo);
                }
                if($request->contrato>0){
                    $todas_alienacoes->where('alienacao.numero_contrato',$request->contrato);
                }
                if($request->dt_ult_mov!=''){
                    $dt_ult_mov_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ult_mov . ' 00:00:00');
                    $dt_ult_mov_fim = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ult_mov . ' 23:59:59');
                    $todas_alienacoes->whereBetween('alienacao_andamento_atual.dt_cadastro',array($dt_ult_mov_ini,$dt_ult_mov_fim));
                }
                if ($request->protocolo_interno!="") {
                    $todas_alienacoes->join('andamento_alienacao','andamento_alienacao.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                                     ->join('acao_etapa','acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
                                     ->whereIn('acao_etapa.co_acao', array(5,32))
                                     ->where('acao_etapa.in_registro_ativo', 'S')
                                     ->where('andamento_alienacao.de_texto_curto_acao','=',$request->protocolo_interno);
                }
                
                if (!empty($request->grupo) or !empty($request->filtro)) {
                    if (!empty($request->grupo) and !is_array($request->grupo)) {
                        $replace['grupo'] = array($request->grupo);
                    }
                    if (!empty($request->filtro) and !is_array($request->filtro)) {
                        $replace['filtro'] = array($request->filtro);
                    }
                    if (isset($replace)) {
                        $request->replace($replace);
                    }
                    if (in_array('observacoes',$request->grupo)) {
                        $todas_alienacoes->join('alienacao_observacao', 'alienacao_observacao.id_alienacao','=','alienacao.id_alienacao')
                                         ->where('alienacao_observacao.in_leitura','N')
                                         ->where('alienacao_observacao.id_pessoa_dest',$this->id_pessoa);
                    } else {
                        if (count($request->grupo)>0 and count($request->filtro)<=0) {
                            foreach ($request->grupo as $key => $grupo) {
                                foreach ($alienacao_regras[$grupo] as $key => $regra) {
                                    $regras_array[] = $regra;
                                }
                            }
                        } elseif (count($request->grupo)>0 and count($request->filtro)>0) {
                            foreach ($request->filtro as $key => $filtro) {
                                $filtro_array = explode('_',$filtro);
                                $regras_array[] = $alienacao_regras[$filtro_array[0]][$filtro_array[1]];
                            }
                        }

                        $todas_alienacoes = $alienacao->aplicar_regras($todas_alienacoes,$regras_array);
                    }
                }

	            $todas_alienacoes = $todas_alienacoes->distinct()->orderBy($orderBy,($request->ord?$request->ord:'desc'))->get();
	            $todas_alienacoes_total = count($todas_alienacoes);

                switch($request->total_paginacao) {
                    case 50: case 100: case 500:
                        $total_paginacao = $request->total_paginacao;
                        break;
                    case -1:
                        $total_paginacao = $todas_alienacoes_total;
                        break;
                    default:
                        $total_paginacao = 10;
                        break;
                }

	            $todas_alienacoes = paginateCollection($todas_alienacoes,$total_paginacao);
	
	            $todas_alienacoes->appends(Request::capture()->except('_token'))->render();

                return view('servicos.alienacao.serventia-alienacao',compact('class','this','request','alienacao_regras','alienacao_titulos','alienacao_alcadas','todas_alienacoes','todas_alienacoes_total'));
            break;
        }
    }

    public function nova(Request $request, alienacao $alienacao, estado $estado, agencia $agencia) {
        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
        $alienacao_token = str_random(30);

        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);

            $imovel = array('matricula_imovel'=>$alienacao->matricula_imovel,
                            'registro_imovel'=>$alienacao->registro_imovel,
                            'no_endereco'=>$alienacao->endereco_imovel->no_endereco,
                            'nu_numero'=>$alienacao->endereco_imovel->nu_numero,
                            'no_complemento'=>$alienacao->endereco_imovel->no_complemento,
                            'no_bairro'=>$alienacao->endereco_imovel->no_bairro,
                            'id_cidade'=>$alienacao->endereco_imovel->id_cidade,
                            'nu_cep'=>$alienacao->endereco_imovel->nu_cep,
                            'in_imovel'=>'S');
            $request->session()->put('imovel_'.$alienacao_token,$imovel);

            if (count($alienacao->parcela_alienacao)>0) {
                foreach ($alienacao->parcela_alienacao as $parcela) {
                    $parcela = array('id_parcela_alienacao'=>$parcela->id_parcela_alienacao,
                                     'nu_parcela'=>$parcela->nu_parcela,
                                     'dt_vencimento'=>$parcela->dt_vencimento,
                                     'va_valor'=>$parcela->va_valor);

                    $request->session()->push('parcelas_'.$alienacao_token,$parcela);
                }
            }

            if (count($alienacao->alienacao_devedor)>0) {
                foreach ($alienacao->alienacao_devedor as $devedor) {
                    $devedor = array('id_alienacao_devedor_devedor'=>$devedor->id_alienacao_devedor_devedor,
                                     'nu_cpf_cnpj'=>$devedor->nu_cpf_cnpj,
                                     'no_devedor'=>$devedor->no_devedor);

                    $request->session()->push('devedores_'.$alienacao_token,$devedor);
                }
            }

            if (count($alienacao->endereco_alienacao)>0) {
                foreach ($alienacao->endereco_alienacao as $endereco) {
                    $endereco = array('id_endereco_alienacao'=>$endereco->id_endereco_alienacao,
                                      'no_endereco'=>$endereco->no_endereco,
                                      'nu_numero'=>$endereco->nu_numero,
                                      'no_complemento'=>$endereco->no_complemento,
                                      'no_bairro'=>$endereco->no_bairro,
                                      'id_cidade'=>$endereco->id_cidade,
                                      'nu_cep'=>$endereco->nu_cep,
                                      'in_imovel'=>($endereco->id_endereco_alienacao==$alienacao->id_endereco_imovel?'S':'N'));

                    $request->session()->push('enderecos_'.$alienacao_token,$endereco);
                }
            }
        } else {
            $alienacao = NULL;
        }

        return view('servicos.alienacao.geral-alienacao-nova',compact('request','cidades','alienacao_token','alienacao'));
    }

    public function inserir(Request $request, serventia $serventia) {
        $protocoloPedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));
        $erro = 0;

        DB::beginTransaction();

        $novo_pedido = new pedido();
        $novo_pedido->id_usuario = Auth::User()->id_usuario;
        $novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
        $novo_pedido->id_produto = $this::ID_PRODUTO;
        $novo_pedido->id_alcada = 8;
        $novo_pedido->protocolo_pedido = $protocoloPedido[0]->f_geraprotocolo;
        $novo_pedido->dt_pedido = Carbon::now();
        $novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
        $novo_pedido->dt_cadastro = Carbon::now();
        $novo_pedido->va_pedido = 0;
        $novo_pedido->id_pessoa_origem = $this->id_pessoa;

        if ($novo_pedido->save()) {
            $serventia_selecionada = $serventia->find($request->id_serventia);
            $novo_pedido->pessoas()->attach($serventia_selecionada->pessoa);

            $novo_pedido_produto_item = new pedido_produto_item();
            $novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
            $novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM_SERVICOS_ANOREG;
            $novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_pedido_produto_item->save()) {
                $erro = 1;
            }

            $novo_pedido_produto_item = new pedido_produto_item();
            $novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
            $novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM_EMOLUMENTOS;
            $novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_pedido_produto_item->save()) {
                $erro = 1;
            }

            $novo_pedido_produto_item = new pedido_produto_item();
            $novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
            $novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM_AR;
            $novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_pedido_produto_item->save()) {
                $erro = 1;
            }

            if ($request->session()->has('imovel_'.$request->alienacao_token)) {
                $imovel = $request->session()->get('imovel_'.$request->alienacao_token);

                $nova_alienacao = new alienacao();
                $nova_alienacao->numero_contrato = $request->numero_contrato;
                $nova_alienacao->matricula_imovel = $imovel['matricula_imovel'];
                $nova_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $nova_alienacao->id_serventia = $request->id_serventia;
                $nova_alienacao->dt_cadastro = Carbon::now();
                $nova_alienacao->va_divida 	= converte_float($request->va_divida);
                $nova_alienacao->va_leilao 	= converte_float($request->va_leilao);
                $nova_alienacao->id_legado 	= $request->id_legado;

                if ($nova_alienacao->save()) {
                    $nova_alienacao_pedido = new alienacao_pedido();
                    $nova_alienacao_pedido->id_pedido = $novo_pedido->id_pedido;
                    $nova_alienacao_pedido->id_alienacao = $nova_alienacao->id_alienacao;
                    if (!$nova_alienacao_pedido->save()) {
                        $erro = 1;
                    }

                    $nova_alienacao_credor_alienacao = new alienacao_credor_alienacao();
                    $nova_alienacao_credor_alienacao->id_credor_alienacao = $request->id_credor_alienacao;
                    $nova_alienacao_credor_alienacao->id_alienacao = $nova_alienacao->id_alienacao;
                    if (!$nova_alienacao_credor_alienacao->save()) {
                        $erro = 1;
                    }

                    $nova_alienacao_projecao = new alienacao_projecao();
                    $nova_alienacao_projecao->id_alienacao = $nova_alienacao->id_alienacao;
                    $nova_alienacao_projecao->va_periodo_01 = converte_float($request->va_periodo_01);
                    $nova_alienacao_projecao->va_periodo_02 = converte_float($request->va_periodo_02);
                    $nova_alienacao_projecao->va_periodo_03 = converte_float($request->va_periodo_03);
                    $nova_alienacao_projecao->va_periodo_04 = converte_float($request->va_periodo_04);
                    $nova_alienacao_projecao->nu_dia_periodo_01 = 15;
                    $nova_alienacao_projecao->nu_dia_periodo_02 = 15;
                    $nova_alienacao_projecao->nu_dia_periodo_03 = 15;
                    $nova_alienacao_projecao->nu_dia_periodo_04 = 15;
                    $nova_alienacao_projecao->in_registro_ativo = 'S';
                    $nova_alienacao_projecao->id_usuario_cad = Auth::User()->id_usuario;
                    $nova_alienacao_projecao->dt_cadastro = Carbon::now();

                    if (!$nova_alienacao_projecao->save()) {
                        $erro = 1;
                    }

                    if ($request->session()->has('devedores_'.$request->alienacao_token)) {
                        $devedores = $request->session()->get('devedores_'.$request->alienacao_token);

                        $erro_loop = false;
                        if (count($devedores)>0) {
                            foreach ($devedores as $devedor) {
                                $nova_alienacao_devedor = new alienacao_devedor();
                                $nova_alienacao_devedor->nu_cpf_cnpj = preg_replace('#[^0-9]#', '', $devedor['nu_cpf_cnpj']);
                                $nova_alienacao_devedor->no_devedor = $devedor['no_devedor'];
                                $nova_alienacao_devedor->id_usuario_cad = Auth::User()->id_usuario;
                                $nova_alienacao_devedor->in_registro_ativo = 'S';
                                if (!$nova_alienacao_devedor->save()) {
                                    $erro_loop = true;
                                }

                                $nova_alienacao_devedor_devedor = new alienacao_devedor_devedor();
                                $nova_alienacao_devedor_devedor->id_alienacao = $nova_alienacao->id_alienacao;
                                $nova_alienacao_devedor_devedor->id_alienacao_devedor = $nova_alienacao_devedor->id_alienacao_devedor;
                                if (!$nova_alienacao_devedor_devedor->save()) {
                                    $erro_loop = true;
                                }
                            }
                            if ($erro_loop) {
                                $erro = 2;
                            }
                        }
                    } else {
                        $erro = 1;
                    }

                    if ($request->session()->has('enderecos_'.$request->alienacao_token)) {
                        $enderecos = $request->session()->get('enderecos_'.$request->alienacao_token);

                        $erro_loop = false;
                        if (count($enderecos)>0) {
                            foreach ($enderecos as $endereco) {
                                $novo_endereco_alienacao = new endereco_alienacao();
                                $novo_endereco_alienacao->id_alienacao = $nova_alienacao->id_alienacao;
                                $novo_endereco_alienacao->no_endereco = mb_strtoupper($endereco['no_endereco'],'UTF-8');
                                $novo_endereco_alienacao->in_notificacao = 'S';
                                $novo_endereco_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                                $novo_endereco_alienacao->in_imovel = $endereco['in_imovel'];
                                $novo_endereco_alienacao->in_registro_ativo = 'S';
                                if ($novo_endereco_alienacao->save()) {
                                    if ($endereco['in_imovel']=='S') {
                                        $id_endereco_imovel = $novo_endereco_alienacao->id_endereco_alienacao;
                                    }
                                } else {
                                    $erro_loop = true;
                                }
                            }
                            if ($erro_loop) {
                                $erro = 2;
                            }
                        }
                        $nova_alienacao->id_endereco_imovel = $id_endereco_imovel;
                        if (!$nova_alienacao->save()) {
                            $erro = 1;
                        }
                    } else {
                        $erro = 1;
                    }

                    if ($request->session()->has('arquivos_'.$request->alienacao_token)) {
                        $destino = '/alienacoes/'.$nova_alienacao->id_alienacao;
                        $arquivos = $request->session()->get('arquivos_'.$request->alienacao_token);

                        Storage::makeDirectory('/public'.$destino);

                        $erro_loop_arq = false;
                        $erro_loop_sql = false;
                        foreach ($arquivos as $key => $arquivo) {
                            $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                            $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                            $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                            $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                            $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                            if ($arquivo['dt_assinado']!='') {
                                $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                            }
                            if ($arquivo['id_usuario_certificado']>0) {
                                $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                            }
                            if (!empty($arquivo['no_arquivo_p7s'])) {
                                $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                                $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                            }
                            if ($novo_arquivo_grupo_produto->save()) {
                                if (Storage::exists($origem_arquivo)) {
                                    if (Storage::exists($destino_arquivo)) {
                                        Storage::delete($destino_arquivo);
                                    }
                                    if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                        if (count($arquivo['no_arquivos_originais'])>0) {
                                            foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                                $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                                $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                                $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                                $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                                $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                                $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                                $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                                $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                                if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                    if (Storage::exists($origem_arquivo_original)) {
                                                        if (Storage::exists($destino_arquivo_original)) {
                                                            Storage::delete($destino_arquivo_original);
                                                        }
                                                        if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                            $erro_loop_arq = true;
                                                        }
                                                    } else {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_sql = true;
                                                }
                                            }
                                        }
                                    } else {
                                        $erro_loop_arq = true;
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_sql = true;
                            }
                            $nova_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
                        }
                        if ($erro_loop_arq) {
                            $erro = 1;
                        }
                        if ($erro_loop_sql) {
                            $erro = 1;
                        }
                    }

                    // Cadastramento
                    $novo_andamento_alienacao = new andamento_alienacao();
                    $novo_andamento_alienacao->id_fase_grupo_produto = 1;
                    $novo_andamento_alienacao->id_etapa_fase = 1;
                    $novo_andamento_alienacao->id_acao_etapa = 1;
                    $novo_andamento_alienacao->id_resultado_acao = 1;
                    $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_alienacao_pedido = $nova_alienacao_pedido->id_alienacao_pedido;
                    if (!$novo_andamento_alienacao->save()) {
                        $erro = 1;
                    }

                    $novo_historico = new historico_pedido();
                    $novo_historico->id_pedido = $novo_pedido->id_pedido;
                    $novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
                    switch ($this->id_tipo_pessoa) {
                        case 3:
                            $novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_FISICA;
                            break;
                        case 4: case 5:
                        $novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
                        break;
                    }
                    $novo_historico->de_observacao = 'Notificacao inserida com sucesso.';
                    $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_historico->save();
                } else {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }

            $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('".$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG."', ';', 'L')  AS (item text, id2 text);"));
            if (count($valores)>0) {
                $valor_total = 0;
                foreach ($valores as $valor) {
                    $valor_total += converte_float($valor->id2);
                }
                $novo_alienacao_valor = new alienacao_valor();
                $novo_alienacao_valor->id_alienacao = $nova_alienacao->id_alienacao;
                $novo_alienacao_valor->nu_quantidade = 1;
                $novo_alienacao_valor->va_valor = $valor_total;
                $novo_alienacao_valor->va_total = $valor_total;
                $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_SERVICOS_ANOREG;
                $novo_alienacao_valor->in_servico = 'S';
                $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                if (!$novo_alienacao_valor->save()) {
                    $erro = 1;
                }
            }

            $novo_pedido->va_pedido = $valor_total;
            if (!$novo_pedido->save()) {
                $erro = 1;
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            return response()->json(array('status'=> 'sucesso',
                'recarrega' => 'true',
                'msg' => 'A alienação foi inserida com sucesso.'));
        }
    }

    public function alterar(Request $request, alienacao $alienacao, alienacao_pedido $alienacao_pedido, historico_pedido $historico_pedido, serventia $serventia, pedido_produto_item $pedido_produto_item, alienacao_fase_grupo_produto $alienacao_fase_grupo_produto, alienacao_credor_alienacao $alienacao_credor_alienacao, endereco $endereco, andamento_alienacao $andamento_alienacao, resultado_acao $resultado_acao, alienacao_projecao $alienacao_projecao) {
        $erro = 0;

        DB::beginTransaction();

        $alienacao = $alienacao->find($request->id_alienacao);

        if ($alienacao) {
            if ($request->session()->has('imovel_'.$request->alienacao_token)) {
                $imovel = $request->session()->get('imovel_'.$request->alienacao_token);

                $alienacao->numero_contrato = $request->numero_contrato;
                $alienacao->matricula_imovel = $imovel['matricula_imovel'];
                $alienacao->id_usuario_alt = Auth::User()->id_usuario;
                $alienacao->dt_alteracao = Carbon::now();
                $alienacao->va_divida = converte_float($request->va_divida);
                $alienacao->va_leilao = converte_float($request->va_leilao);
                $alienacao->id_legado = $request->id_legado;

                if ($alienacao->save()) {
                    if ($request->id_credor_alienacao!=$alienacao->credor_alienacao->credor->id_credor_alienacao) {
                        if ($alienacao_credor_alienacao->where('id_alienacao',$alienacao->id_alienacao)->delete()) {
                            $nova_alienacao_credor_alienacao = $alienacao_credor_alienacao;
                            $nova_alienacao_credor_alienacao->id_credor_alienacao = $request->id_credor_alienacao;
                            $nova_alienacao_credor_alienacao->id_alienacao = $alienacao->id_alienacao;
                            if (!$nova_alienacao_credor_alienacao->save()) {
                                $erro = 1;
                            }
                        } else {
                            $erro = 1;
                        }
                    }

                    $alienacao_projecao = $alienacao_projecao->where('id_alienacao',$alienacao->id_alienacao)->first();
                    $alienacao_projecao->va_periodo_01 = converte_float($request->va_periodo_01);
                    $alienacao_projecao->va_periodo_02 = converte_float($request->va_periodo_02);
                    $alienacao_projecao->va_periodo_03 = converte_float($request->va_periodo_03);
                    $alienacao_projecao->va_periodo_04 = converte_float($request->va_periodo_04);

                    if (!$alienacao_projecao->save()) {
                        $erro = 1;
                    }

                    if ($request->session()->has('devedores_'.$request->alienacao_token)) {
                        $devedores_atuais = array();
                        if (count($alienacao->alienacao_devedor_devedor)) {
                            foreach($alienacao->alienacao_devedor_devedor as $alienacao_devedor_devedor) {
                                $devedores_atuais[] = $alienacao_devedor_devedor->id_alienacao_devedor_devedor;
                            }
                        }

                        $devedores = $request->session()->get('devedores_'.$request->alienacao_token);

                        $erro_loop = false;
                        if (count($devedores)>0) {
                            foreach ($devedores as $devedor) {
                                if (isset($devedor['id_alienacao_devedor_devedor'])) {
                                    if (in_array($devedor['id_alienacao_devedor_devedor'],$devedores_atuais)) {
                                        unset($devedores_atuais[array_search($devedor['id_alienacao_devedor_devedor'], $devedores_atuais)]);
                                    }
                                } else {
                                    $nova_alienacao_devedor = new alienacao_devedor();
                                    $nova_alienacao_devedor->nu_cpf_cnpj = preg_replace('#[^0-9]#', '', $devedor['nu_cpf_cnpj']);
                                    $nova_alienacao_devedor->no_devedor = $devedor['no_devedor'];
                                    $nova_alienacao_devedor->id_usuario_cad = Auth::User()->id_usuario;
									$nova_alienacao_devedor->in_registro_ativo = 'S';
                                    if (!$nova_alienacao_devedor->save()) {
                                        $erro_loop = true;
                                    }

                                    $nova_alienacao_devedor_devedor = new alienacao_devedor_devedor();
                                    $nova_alienacao_devedor_devedor->id_alienacao = $alienacao->id_alienacao;
                                    $nova_alienacao_devedor_devedor->id_alienacao_devedor = $nova_alienacao_devedor->id_alienacao_devedor;
                                    if (!$nova_alienacao_devedor_devedor->save()) {
                                        $erro_loop = true;
                                    }
                                }
                            }
                            if (count($devedores_atuais)>0) {
                                foreach($devedores_atuais as $id_alienacao_devedor_devedor) {
                                    $deletar_alienacao_devedor_devedor = new alienacao_devedor_devedor();
                                    if (!$deletar_alienacao_devedor_devedor->where('id_alienacao_devedor_devedor','=',$id_alienacao_devedor_devedor)->delete()) {
                                        $erro_loop = true;
                                    }
                                }
                            }
                            if ($erro_loop) {
                                $erro = 2;
                            }
                        }
                    } else {
                        $erro = 1;
                    }

                    if ($request->session()->has('enderecos_'.$request->alienacao_token)) {
                        $enderecos_atuais = array();
                        if (count($alienacao->endereco_alienacao)) {
                            foreach($alienacao->endereco_alienacao as $endereco_alienacao) {
                                $enderecos_atuais[] = $endereco_alienacao->id_endereco_alienacao;
                            }
                        }

                        $enderecos = $request->session()->get('enderecos_'.$request->alienacao_token);

                        $erro_loop = false;
                        if (count($enderecos)>0) {
                            foreach ($enderecos as $endereco) {
                                if (isset($endereco['id_endereco_alienacao'])) {
                                    if (in_array($endereco['id_endereco_alienacao'],$enderecos_atuais)) {
                                        unset($enderecos_atuais[array_search($endereco['id_endereco_alienacao'], $enderecos_atuais)]);
                                    }
                                    $id_endereco_alienacao = $endereco['id_endereco_alienacao'];
                                } else {
                                    $novo_endereco_alienacao = new endereco_alienacao();
                                    $novo_endereco_alienacao->id_alienacao = $alienacao->id_alienacao;
                                    $novo_endereco_alienacao->no_endereco = mb_strtoupper($endereco['no_endereco'],'UTF-8');
                                    $novo_endereco_alienacao->in_imovel = $endereco['in_imovel'];
                                    $novo_endereco_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                                    if (!$novo_endereco_alienacao->save()) {
                                        $erro_loop = true;
                                    }
                                    $id_endereco_alienacao = $novo_endereco_alienacao->id_endereco_alienacao;
                                }
                                if ($endereco['in_imovel']=='S') {
                                    $id_endereco_imovel = $id_endereco_alienacao;
                                }
                            }
                            if (count($enderecos_atuais)>0) {
                                foreach($enderecos_atuais as $id_endereco_alienacao) {
                                    $deletar_endereco_alienacao = new endereco_alienacao();
                                    if (!$deletar_endereco_alienacao->where('id_endereco_alienacao','=',$id_endereco_alienacao)->delete()) {
                                        $erro_loop = true;
                                    }
                                }
                            }
                            if ($erro_loop) {
                                $erro = 2;
                            }
                        }
                        $alienacao->id_endereco_imovel = $id_endereco_imovel;
                        if (!$alienacao->save()) {
                            $erro = 1;
                        }
                    } else {
                        $erro = 1;
                    }

                    /*
                    ***************** Preciso alterar para a variável no_arquivo_atual para permitir a exclusão.

                    $arquivos_atuais = array();
                    if (count($alienacao->alienacao_arquivo)) {
                        $erro_loop = false;

                        if (count($request->id_alienacao_arquivo_grupo_prod)>0) {
                            $arquivos_atuais = $request->id_alienacao_arquivo_grupo_prod;
                        } else {
                            $arquivos_atuais = array();
                        }

                        foreach($alienacao->alienacao_arquivo as $alienacao_arquivo) {
                            if (!in_array($alienacao_arquivo->id_alienacao_arquivo_grupo_prod,$arquivos_atuais)) {
                                $deletar_alienacao_arquivo_grupo_produto = new alienacao_arquivo_grupo_produto();
                                $deletar_alienacao_arquivo_grupo_produto = $deletar_alienacao_arquivo_grupo_produto->where('id_alienacao_arquivo_grupo_prod','=',$alienacao_arquivo->id_alienacao_arquivo_grupo_prod)->first();
                                if (!$deletar_alienacao_arquivo_grupo_produto->delete()) {
                                    $erro_loop = true;
                                }
                                $deletar_arquivo_grupo_produto = new arquivo_grupo_produto();
                                $deletar_arquivo_grupo_produto = $deletar_arquivo_grupo_produto->where('id_arquivo_grupo_produto','=',$deletar_alienacao_arquivo_grupo_produto->id_arquivo_grupo_produto)->first();
                                if ($deletar_arquivo_grupo_produto->delete()) {
                                    Storage::delete('/public/'.$deletar_arquivo_grupo_produto->no_local_arquivo.'/'.$deletar_arquivo_grupo_produto->no_arquivo);
                                } else {
                                    $erro_loop = true;
                                }
                            }
                        }
                        if ($erro_loop) {
                            $erro = 3;
                        }
                    }
                    */

                    if ($request->session()->has('arquivos_'.$request->alienacao_token)) {
                        $destino = '/alienacoes/'.$alienacao->id_alienacao;
                        $arquivos = $request->session()->get('arquivos_'.$request->alienacao_token);

                        Storage::makeDirectory('/public'.$destino);

                        $erro_loop_arq = false;
                        $erro_loop_sql = false;
                        foreach ($arquivos as $key => $arquivo) {
                            $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                            $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                            $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                            $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                            $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                            if ($arquivo['dt_assinado']!='') {
                                $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                            }
                            if ($arquivo['id_usuario_certificado']>0) {
                                $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                            }
                            if (!empty($arquivo['no_arquivo_p7s'])) {
                                $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                                $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                            }
                            if ($novo_arquivo_grupo_produto->save()) {
                                if (Storage::exists($origem_arquivo)) {
                                    if (Storage::exists($destino_arquivo)) {
                                        Storage::delete($destino_arquivo);
                                    }
                                    if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                        if (count($arquivo['no_arquivos_originais'])>0) {
                                            foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                                $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                                $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                                $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                                $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                                $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                                $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                                $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                                $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                                if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                    if (Storage::exists($origem_arquivo_original)) {
                                                        if (Storage::exists($destino_arquivo_original)) {
                                                            Storage::delete($destino_arquivo_original);
                                                        }
                                                        if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                            $erro_loop_arq = true;
                                                        }
                                                    } else {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_sql = true;
                                                }
                                            }
                                        }
                                    } else {
                                        $erro_loop_arq = true;
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_sql = true;
                            }
                            $alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
                        }
                        if ($erro_loop_arq) {
                            $erro = 1;
                        }
                        if ($erro_loop_sql) {
                            $erro = 1;
                        }
                    }
                    
                    $novo_historico = $historico_pedido;
                    $novo_historico->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                    $novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_DEVOLVIDO;
                    switch ($this->id_tipo_pessoa) {
                        case 3:
                            $novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_FISICA;
                            break;
                        case 4: case 5:
                        $novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
                        break;
                    }
                    $novo_historico->de_observacao = 'Notificacao alterada com sucesso.';
                    $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_historico->save();
                } else {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            return response()->json(array('status'=> 'sucesso',
                'recarrega' => 'true',
                'msg' => 'A alienação foi alterada com sucesso.'));
        }
    }

    public function encaminhar_alienacoes(Request $request, pedido $pedido) {
        $erro = 0;

        DB::beginTransaction();

        if ($request->ids_alienacoes!='') {
            foreach ($request->ids_alienacoes as $id_alienacao) {
                $alienacao = new alienacao();
                $alienacao = $alienacao->find($id_alienacao);

                // Encaminhamento para a Anoreg - Automático até segunda ordem
                $novo_andamento_alienacao = new andamento_alienacao();
                $novo_andamento_alienacao->id_fase_grupo_produto = 1;
                $novo_andamento_alienacao->id_etapa_fase = 2;
                $novo_andamento_alienacao->id_acao_etapa = 3;
                $novo_andamento_alienacao->id_resultado_acao = 3;
                $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_alienacao_pedido = $alienacao->alienacao_pedido->id_alienacao_pedido;
                if (!$novo_andamento_alienacao->save()) {
                    $erro = 1;
                }

                // Aprovação ANOREG
                $novo_andamento_alienacao = new andamento_alienacao();
                $novo_andamento_alienacao->id_fase_grupo_produto = 2;
                $novo_andamento_alienacao->id_etapa_fase = 3;
                $novo_andamento_alienacao->id_acao_etapa = 4;
                $novo_andamento_alienacao->id_resultado_acao = 4;
                $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_alienacao_pedido = $alienacao->alienacao_pedido->id_alienacao_pedido;
                if (!$novo_andamento_alienacao->save()) {
                    $erro = 1;
                }

                if ($pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_EMPROCESSAMENTO])) {
                    $novo_historico = new historico_pedido();
                    $novo_historico->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                    $novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_EMPROCESSAMENTO;
                    switch ($this->id_tipo_pessoa) {
                        case 3:
                            $novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_FISICA;
                            break;
                        case 4: case 5:
                        $novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
                        break;
                    }
                    $novo_historico->de_observacao = 'Notificacao encaminhada com sucesso.';
                    $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_historico->save();
                } else {
                    $erro = 1;
                }
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            return response()->json(array('status'=> 'sucesso',
                'recarrega' => 'true',
                'msg' => 'As alienações foram encaminhadas com sucesso.'));
        }
    }

    public function detalhes(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            $class = $this;
            $alienacao = $alienacao->find($request->id_alienacao);
            $alienacao_token = str_random(30);

            return view('servicos.alienacao.geral-alienacao-detalhes',compact('class','request','alienacao','alienacao_token'));
        }
    }

    public function novo_credor(Request $request, estado $estado) {
        $cidades = $estado->find(env('ID_ESTADO'))->cidades()->orderBy('cidade.no_cidade')->get();

        return view('servicos.alienacao.geral-alienacao-novo-credor',compact('request','cidades'));
    }

    public function novo_imovel(Request $request, estado $estado) {
        $cidades = $estado->find(env('ID_ESTADO'))->cidades()->orderBy('no_cidade')->get();
        $alienacao_token = $request->alienacao_token;

        return view('servicos.alienacao.geral-alienacao-novo-imovel',compact('request','cidades','alienacao_token'));
    }
    public function detalhes_imovel(Request $request, alienacao $alienacao) {
        $alienacao = $alienacao->find($request->id_alienacao);
        $modulos = $this->modulos;
        return view('servicos.alienacao.geral-alienacao-detalhes-imovel',compact('request','modulos','alienacao'));
    }
    public function inserir_imovel(Request $request, cidade $cidade) {
        $cidade = $cidade->find($request->id_cidade);
        $imovel = array('matricula_imovel'=>$request->matricula_imovel,
                        'no_endereco'=>$request->no_endereco,
                        'index_endereco'=>$request->index_endereco);

        $endereco = array('no_endereco'=>$request->no_endereco,
                          'in_imovel'=>'S');

        if ($request->session()->has('imovel_'.$request->alienacao_token)) {
            $msg = 'O imóvel foi alterado com sucesso';

            $enderecos = $request->session()->get('enderecos_'.$request->alienacao_token);
            $request->session()->forget('enderecos_'.$request->alienacao_token);
            $enderecos[$request->index_endereco] = $endereco;
            $request->session()->put('enderecos_'.$request->alienacao_token,$enderecos);


        } else {
            $msg = 'O imóvel foi inserido com sucesso';
            $request->session()->push('enderecos_'.$request->alienacao_token,$endereco);
        }

        $request->session()->put('imovel_'.$request->alienacao_token,$imovel);

        return response()->json(array('status' => 'sucesso',
            'msg'=>$msg,
            'imovel'=>$imovel));
    }
    public function remover_imovel(Request $request) {
        if ($request->id_alienacao>0) {

        } else {
            if ($request->session()->has('imovel_'.$request->alienacao_token)) {
                $request->session()->forget('imovel_'.$request->alienacao_token);

                $enderecos = $request->session()->get('enderecos_'.$request->alienacao_token);
                $request->session()->forget('enderecos_'.$request->alienacao_token);
                unset($enderecos[$request->index_endereco]);
                $request->session()->put('enderecos_'.$request->alienacao_token,$enderecos);
            }
        }
    }

    public function novo_devedor(Request $request, estado $estado) {
        $cidades = $estado->find(env('ID_ESTADO'))->cidades()->orderBy('cidade.no_cidade')->get();
        $alienacao_token = $request->alienacao_token;

        return view('servicos.alienacao.geral-alienacao-novo-devedor',compact('request','cidades','alienacao_token'));
    }

    public function inserir_devedor(Request $request, cidade $cidade) {
        $devedores = array();
        if (count($request->a_nu_cpf_cnpj)>0) {
            $i=0;
            foreach ($request->a_nu_cpf_cnpj as $nu_cpf_cnpj) {
                $devedor = array('nu_cpf_cnpj'=>$nu_cpf_cnpj,
                    'no_devedor'=>$request->a_no_devedor[$i]);

                $request->session()->push('devedores_'.$request->alienacao_token,$devedor);

                $devedores[] = $devedor;

                $i++;
            }
        }

        return response()->json(array('status' => 'sucesso',
            'msg'=>(count($request->a_nu_cpf_cnpj)>1?'Os devedores foram inseridos com sucesso':'O devedor foi inserido com sucesso'),
            'devedores'=>$devedores));
    }
    public function remover_devedor(Request $request) {
        if ($request->session()->has('devedores_'.$request->alienacao_token)) {
            $devedores = $request->session()->get('devedores_'.$request->alienacao_token);
            unset($devedores[$request->linha]);
            $request->session()->put('devedores_'.$request->alienacao_token,$devedores);
        }
    }
    public function detalhes_devedor(Request $request, alienacao $alienacao, alienacao_devedor $alienacao_devedor) {
        $alienacao = $alienacao->find($request->id_alienacao);
        $devedor = $alienacao_devedor->find($request->id_alienacao_devedor);

        return view('servicos.alienacao.geral-alienacao-detalhes-devedor',compact('request','alienacao', 'devedor'));
    }
    public function alterar_devedor(Request $request, alienacao_devedor $alienacao_devedor)
    {
        $erro = 0;

        DB::beginTransaction();

        if ($request->id_alienacao > 0) {
            $alterar_alienacao = $alienacao_devedor->where('id_alienacao_devedor','=',$request->id_alienacao_devedor)
                ->where('in_registro_ativo','=','S')
                ->first();

            $alterar_alienacao->id_usuario_alt = Auth::User()->id_usuario;
            $alterar_alienacao->dt_alteracao = Carbon::now();
            $alterar_alienacao->in_registro_ativo = 'N';

            if ($alterar_alienacao->save()) {
                $nova_alienacao_devedor = new alienacao_devedor();
                $nova_alienacao_devedor->nu_cpf_cnpj = preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj);
                $nova_alienacao_devedor->no_devedor = $request->no_devedor;
                $nova_alienacao_devedor->id_usuario_cad = Auth::User()->id_usuario;
                $nova_alienacao_devedor->in_registro_ativo = 'S';
                if ($nova_alienacao_devedor->save()) {
                    $nova_alienacao_devedor_devedor = new alienacao_devedor_devedor();
                    $nova_alienacao_devedor_devedor->id_alienacao = $request->id_alienacao;
                    $nova_alienacao_devedor_devedor->id_alienacao_devedor = $nova_alienacao_devedor->id_alienacao_devedor;
                    if (!$nova_alienacao_devedor_devedor->save()) {
                        $erro = 1;
                    }
                } else {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro > 0) {
            DB::rollback();
            return response()->json(array('status' => 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
        } else {
            DB::commit();
            return response()->json(array('status' => 'sucesso',
                'recarrega' => 'true',
                'msg' => 'O nome do devedor foi alterado com sucesso.'));
        }
    }

    public function novo_endereco(Request $request, estado $estado) {
        $cidades = $estado->find(env('ID_ESTADO'))->cidades()->orderBy('cidade.no_cidade')->get();
        $alienacao_token = $request->alienacao_token;

        return view('servicos.alienacao.geral-alienacao-novo-endereco',compact('request','cidades','alienacao_token'));
    }

    public function inserir_endereco(Request $request, cidade $cidade) {
        $enderecos = array();
        $erro = 0;

        DB::beginTransaction();

        if (count($request->a_no_endereco)>0) {
            $i=0;
            $erro_loop = false;
            foreach ($request->a_no_endereco as $no_endereco) {
                /*$cidade = $cidade->find($request->a_id_cidade[$i]);*/
                $endereco = array('no_endereco' => $no_endereco = $request->a_no_endereco[$i],
                    /*'nu_numero' => $request->a_nu_numero[$i],
                    'no_complemento' => $request->a_no_complemento[$i],
                    'no_bairro' => $request->a_no_bairro[$i],
                    'id_cidade' => $request->a_id_cidade[$i],
                    'nome_cidade' => $cidade->no_cidade,
                    'nu_cep' => $request->a_nu_cep[$i],*/
                    'in_imovel' => 'N');

                if ($request->id_alienacao>0) {
                    $novo_endereco_alienacao = new endereco_alienacao();
                    $novo_endereco_alienacao->id_alienacao = $request->id_alienacao;
                    $novo_endereco_alienacao->no_endereco = $endereco['no_endereco'];

                    /*$novo_endereco_alienacao->nu_numero = $endereco['nu_numero'];
                    $novo_endereco_alienacao->no_complemento = $endereco['no_complemento'];
                    $novo_endereco_alienacao->no_bairro = $endereco['no_bairro'];
                    $novo_endereco_alienacao->nu_cep = preg_replace('#[^0-9]#', '', $endereco['nu_cep']);*/

                    $novo_endereco_alienacao->in_notificacao = 'S';
                    $novo_endereco_alienacao->id_usuario_cad = Auth::User()->id_usuario;

                    /*$novo_endereco_alienacao->id_cidade = $endereco['id_cidade'];*/

                    if (!$novo_endereco_alienacao->save()) {
                        $erro_loop = true;
                    }
                    $endereco['id_endereco_alienacao'] = $novo_endereco_alienacao->id_endereco_alienacao;
                    $endereco['id_alienacao'] = $request->id_alienacao;
                    $enderecos[] = $endereco;
                } else {
                    $enderecos[] = $endereco;
                    $request->session()->push('enderecos_'.$request->alienacao_token,$endereco);
                }

                $i++;
            }
            if ($erro_loop) {
                $erro = 2;
            }
        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                                          'recarrega' => 'false',
                                          'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            return response()->json(array('status' => 'sucesso',
                                          'msg'=>(count($request->a_no_endereco)>1?'Os endereços foram inseridos com sucesso':'O endereço foi inserido com sucesso'),
                                          'enderecos'=>$enderecos));
        }
    }

    public function remover_endereco(Request $request, endereco_alienacao $endereco_alienacao, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            DB::beginTransaction();
            $alienacao = $alienacao->find($request->id_alienacao);

            //Remover endereço, registrando no DB(caixa)
            if (in_array($alienacao->verifica_acao_etapa(),[1,11,81])) {
                if ($endereco_alienacao->where('id_endereco_alienacao',$request->linha)->update(['in_registro_ativo'=>'N','in_excluido'=>'S','dt_excluido'=>Carbon::now(),'id_usuario_excluido'=>$this->id_pessoa])) {
                    $endereco = $endereco_alienacao->find($request->linha);

                    $duplicado = $alienacao->verifica_endereco_duplicado()==="N"?true:false;

                    //atualizando valor de cobranca do AR
                    if ($request->in_atualiza_alienacao_valor == 1) {
                        $alienacao_valor = new alienacao_valor();
                        $alienacao_valor_selecionados = $alienacao_valor->where("id_alienacao", $request->id_alienacao)
                                                                        ->where("id_produto_item", $this::ID_PRODUTO_ITEM_AR)
                                                                        ->where("in_aprovacao_orcamento", 'N')
                                                                        ->first();

                        if ($alienacao_valor_selecionados->id_alienacao_valor > 0) {
                            $nova_quantidade                                = ($alienacao_valor_selecionados->nu_quantidade-1);
                            $alienacao_valor_selecionados->nu_quantidade    = $nova_quantidade;
                            $alienacao_valor_selecionados->va_total         = $alienacao_valor_selecionados->va_valor*$nova_quantidade;

                            if (!$alienacao_valor_selecionados->save()) {
                                DB::rollback();
                                return response()->json(array('status' => 'erro',
                                    'recarrega' => 'false',
                                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                            }

                        } else {
                            DB::rollback();
                            return response()->json(array('status' => 'erro',
                                'recarrega' => 'false',
                                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                        }
                    }

                    DB::commit();
                    return response()->json(array('status' => 'sucesso',
                        'recarrega' => false,
                        'msg'=>'O endereço foi removido com sucesso',
                        'data'=> array('no_usuario'=>$endereco->usuario_excluido->no_usuario,'dt_excluido'=>Carbon::parse($endereco->dt_excluido)->format('d/m/Y H:i'),'duplicado'=>$duplicado)
                    ));
                } else {
                    DB::rollback();
                    return response()->json(array('status' => 'erro',
                        'recarrega' => 'false',
                        'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                }
            } else {
                DB::commit();
                if ($endereco_alienacao->where('id_endereco_alienacao',$request->linha)->delete()) {
                    return response()->json(array('status' => 'sucesso',
                        'recarrega' => false,
                        'msg'=>'O endereço foi removido com sucesso'));

                } else {
                    DB::rollback();
                    return response()->json(array('status' => 'erro',
                        'recarrega' => 'false',
                        'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                }
            }
        } else {
            if ($request->session()->has('enderecos_'.$request->alienacao_token)) {
                $enderecos = $request->session()->get('enderecos_'.$request->alienacao_token);
                unset($enderecos[$request->linha]);
                $request->session()->put('enderecos_'.$request->alienacao_token,$enderecos);
            }
        }
    }

    public function nova_resposta(Request $request, alienacao $alienacao, fase_grupo_produto $fase_grupo_produto) {
        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);
            $class = $this;

            return view('servicos.alienacao.serventia-alienacao-resposta',compact('class','alienacao'));
        }
    }

    public function gerar_recibo(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {

            $class = $this;
            $alienacao = $alienacao->find($request->id_alienacao);

            $destino = '/alienacoes/'.$alienacao->id_alienacao;
            $no_arquivo = 'alienacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';

            Storage::makeDirectory('/public'.$destino);

            $titulo = 'Detalhes da Alienação: '.$alienacao->alienacao_pedido->pedido->protocolo_pedido;
            $pdf = PDF::loadView('pdf.alienacao-recibo', compact('alienacao', 'titulo'));

            $pdf->save(storage_path('app/public'.$destino.'/'.$no_arquivo));

            return response()->json(array('view'=>view('servicos.alienacao.geral-alienacao-recibo',compact('alienacao'))->render()));
        }
    }

    public function imprimir_recibo(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);
            $nome_arquivo = 'alienacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';

            $arquivo = Storage::get('/public/alienacoes/'.$alienacao->id_alienacao.'/'.$nome_arquivo);

            return response($arquivo, 200)->header('Content-Type', 'application/pdf')
                                          ->header('Content-Disposition', 'inline; filename="'.$nome_arquivo.'"');
        }
    }

    public function resultado_acao(Request $request, resultado_acao $resultado_acao, serventia $serventia, alienacao $alienacao) {
        $variaveis = array();
        $resultado_acao = $resultado_acao->find($request->id_resultado_acao);

        switch ($resultado_acao->co_resultado) {
            case 9: case 52:
                $variaveis['rtds'] = $serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
                    //->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
                    //->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                    //->where('endereco.id_cidade',Auth::User()->pessoa->enderecos[0]->id_cidade)
                    ->where('serventia.id_tipo_serventia','=',2)
                    ->orderBy('serventia.no_serventia')
                    ->get();
                break;
            case 152:
                $alienacao = $alienacao->find($request->id_alienacao);

                $variaveis['edital'] = [
                    'dt_acao' => \Carbon\Carbon::parse($alienacao->protocolo()->dt_acao)->format('d/m/Y'),
                    'va_divida' => $alienacao->va_divida,
                ];
                break;
        }

        $andamento_token = $request->andamento_token;

        return view('servicos.alienacao.serventia-alienacao-resultado-acao',compact('resultado_acao','variaveis','andamento_token'));
    }

    public function novo_andamento(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            $class = $this;
            $alienacao = $alienacao->find($request->id_alienacao);

            $andamento_token = str_random(30);

            switch ($this->id_tipo_pessoa) {
                case 8: case 13:
                    return view('servicos.alienacao.geral-alienacao-novo-andamento',compact('class','alienacao','andamento_token'));
                    break;
                case 2: case 10:
                    return view('servicos.alienacao.serventia-alienacao-novo-andamento',compact('class','alienacao','andamento_token'));
                    break;
            }
        }
    }

    public function inserir_andamento(Request $request, alienacao $alienacao, pedido $pedido, andamento_alienacao $andamento_alienacao, resultado_acao $resultado_acao, historico_pedido $historico_pedido, serventia $serventia) {
        $erro = 0;
        $msg_sucesso = false;

        DB::beginTransaction();

        if ($request->id_alienacao) {
            $alienacao = $alienacao->find($request->id_alienacao);
            $pedido = $pedido->find($alienacao->alienacao_pedido->id_pedido);

            $id_andamento_alienacao = $alienacao->alienacao_pedido->andamento_alienacao[0]->id_andamento_alienacao;

            if($alienacao->alienacao_pedido->andamento_alienacao[0]->id_resultado_acao>0) {
                $resultado_acao = $alienacao->alienacao_pedido->andamento_alienacao[0]->resultado_acao;
                $nova_acao = $resultado_acao->nova_acao;

                // Próximo Ação / Etapa / Fase
                $novo_andamento_alienacao = new andamento_alienacao();
                $novo_andamento_alienacao->id_fase_grupo_produto = $resultado_acao->id_nova_fase_grupo_produto;
                $novo_andamento_alienacao->id_etapa_fase = $resultado_acao->id_nova_etapa_fase;
                $novo_andamento_alienacao->id_acao_etapa = $resultado_acao->id_nova_acao_etapa;
                $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;

                if($nova_acao->in_resultado_automatico=='S') {
                    $novo_andamento_alienacao->id_resultado_acao = $nova_acao->resultado_acao[0]->id_resultado_acao;
                    $resultado = array('automatico'=>'true','no_resultado'=>$nova_acao->resultado_acao[0]->no_resultado);
                } else {
                    $resultado = array('automatico'=>'false','no_resultado'=>'Aguardando');
                }


                if($nova_acao->tp_texto_curto_acao>0) {
                    $novo_andamento_alienacao->de_texto_curto_acao = $request->de_texto_curto_acao;
                }
                if($nova_acao->tp_texto_longo_acao>0) {
                    $novo_andamento_alienacao->de_texto_longo_acao = $request->de_texto_longo_acao;
                }
                if($nova_acao->tp_data_acao>0) {
                    $novo_andamento_alienacao->dt_acao = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_acao.' 00:00:00');
                }
                if($nova_acao->tp_valor_acao>0) {
                    $novo_andamento_alienacao->va_valor_acao = converte_float($request->va_valor_acao);
                }

                $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_alienacao_pedido = $alienacao->alienacao_pedido->id_alienacao_pedido;

                //ESTE CASE SERVE PARA ACAO_ETAPA
                switch ($nova_acao->co_acao) {
                    case 6: // Gera carta de intimação - Emitir carta de intimação
                    case 33: // 2º processamento - Gera carta de intimação - Emitir carta de intimação
                        // Inserção do valor na tabela de valores
                        // Produto Item 34 = Carta initimação
                        /*
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('34', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = 1;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_total;
                            $novo_alienacao_valor->id_produto_item = 34;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                                $erro = 1;
                            }
                        }
                        */
                        break;
                    case 15: // Efetuar notificacao EDITAL - Emitir edital de intimação
                    case 41: // 2º processamento - Efetuar notificacao EDITAL - Emitir edital de intimação
                        // Inserção do valor na tabela de valores
                        // Produto Item 35 = Edital de Intimação
                        /*
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('35', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $qtd = count($alienacao->endereco_alienacao)*count($alienacao->alienacao_devedor);
                            $valor_final = $valor_total*$qtd;
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = $qtd;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_final;
                            $novo_alienacao_valor->id_produto_item = 35;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_final])) {
                                $erro = 1;
                            }
                        }
                        */
                        break;
                    case 24: // Emitir certidão (Decurso) - Emitir certidão
                        // Inserção do valor na tabela de valores
                        // Produto Item 36 = Certidão de Decurso de Prazo
                        /*$valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('36', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = 1;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_total;
                            $novo_alienacao_valor->id_produto_item = 36;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                                $erro = 1;
                            }
                        }*/
                        break;
                    case 12: // Efetuar notificacao - 1º Processamento - RTD - Notificar
                    case 38: // Efetuar notificacao - 2º Processamento - RTD - Notificar
                        // Inserção do valor na tabela de valores
                        // Produto Item 30 = Diligência
                        /*
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('30', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = 1;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_total;
                            $novo_alienacao_valor->id_produto_item = 30;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                                $erro = 1;
                            }
                        }
                        */
                        break;
                    case 9: // Efetuar notificacao - 1º Processamento - Notificar
                    case 35: // Efetuar notificacao - 2º Processamento - Notificar
                        // Inserção do valor na tabela de valores
                        // Produto Item 31 = AR
						/*
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('31', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $qtd = count($alienacao->endereco_alienacao)*count($alienacao->alienacao_devedor);
                            $valor_final = $valor_total*$qtd;
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = $qtd;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_final;
                            $novo_alienacao_valor->id_produto_item = 31;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_final])) {
                                $erro = 1;
                            }
                        }
						*/
                        break;
                    case 29: // Pagamento de ITBI - Gerar Documento de Consolidação
                        // Inserção do valor na tabela de valores
                        // Produto Item 37 = Consolidação de imóvel
                        /*
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('37', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = 1;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_total;
                            $novo_alienacao_valor->id_produto_item = 37;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                                $erro = 1;
                            }
                        }
                        */
                        break;
                    case 2: // Informar valor dos emolumentos
                        $novo_alienacao_valor = new alienacao_valor();
                        $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                        $novo_alienacao_valor->nu_quantidade = 1;
                        $novo_alienacao_valor->va_valor = converte_float($request->va_valor_acao);
                        $novo_alienacao_valor->va_total = converte_float($request->va_valor_acao);
                        $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_EMOLUMENTOS;
                        $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                        if (!$novo_alienacao_valor->save()) {
                            $erro = 1;
                        }
                        $pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+converte_float($request->va_valor_acao)]);
                        break;
                    case 83: // Informar Orçamento - Novo(s) Endereço(s)
                        $novo_alienacao_valor = new alienacao_valor();
                        $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                        $novo_alienacao_valor->nu_quantidade = 1;
                        $novo_alienacao_valor->va_valor = converte_float($request->va_valor_acao);
                        $novo_alienacao_valor->va_total = converte_float($request->va_valor_acao);
                        $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_NOVO_ENDERECO;
                        $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                        if (!$novo_alienacao_valor->save()) {
                            $erro = 1;
                        }
                        $pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+converte_float($request->va_valor_acao)]);
                        break;						
                    case 56: case 59: case 81://  Informar Orçamento (Edital)
                        $novo_alienacao_valor = new alienacao_valor();
                        $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                        $novo_alienacao_valor->nu_quantidade = 1;
                        $novo_alienacao_valor->va_valor = converte_float($request->va_valor_acao);
                        $novo_alienacao_valor->va_total = converte_float($request->va_valor_acao);
                        $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_EDITAL;
                        $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                        if (!$novo_alienacao_valor->save()) {
                            $erro = 1;
                        }
                        $pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+converte_float($request->va_valor_acao)]);
                        break;
						
					case 28: // Requerer consolidação (37 - Certidão de Inteiro Teor / 38 - Averbação)
                        // Inserção do valor na tabela de valores
						// Produto Item 37 - Certidão de Inteiro Teor
						$valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('37', ';', 'L')  AS (item text, id2 text);"));
						if (count($valores)>0) {
							$valor_total = 0;
							foreach ($valores as $valor) {
								$valor_total += converte_float($valor->id2);
							}
							$novo_alienacao_valor = new alienacao_valor();
							$novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
							$novo_alienacao_valor->nu_quantidade = 1;
							$novo_alienacao_valor->va_valor = $valor_total;
							$novo_alienacao_valor->va_total = $valor_total;
							$novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_CERTIDAO_INTEIRO_TEOR;
							//$novo_alienacao_valor->in_aprovacao_orcamento = 'S';
							$novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
							
							if (!$novo_alienacao_valor->save()) {
								$erro = 1;
							}
							if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
								$erro = 1;
							}
						}  
                        // Inserção do valor na tabela de valores
						// Produto Item 38 - Averbação
						$valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('38', ';', 'L')  AS (item text, id2 text);"));
						if (count($valores)>0) {
							$valor_total = 0;
							foreach ($valores as $valor) {
								$valor_total += converte_float($valor->id2);
							}
							$novo_alienacao_valor = new alienacao_valor();
							$novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
							$novo_alienacao_valor->nu_quantidade = 1;
							$novo_alienacao_valor->va_valor = $valor_total;
							$novo_alienacao_valor->va_total = $valor_total;
							$novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_AVERBACAO;
							//$novo_alienacao_valor->in_aprovacao_orcamento = 'S';
							$novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
							
							if (!$novo_alienacao_valor->save()) {
								$erro = 1;
							}
							if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
								$erro = 1;
							}
						}  						
						
						break;						
						
					case 63: // Solicitar guia ITBI
                        // Inserção do valor na tabela de valores
						// Produto Item 34 = Diligência 
						$valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('34', ';', 'L')  AS (item text, id2 text);"));
						if (count($valores)>0) {
							$valor_total = 0;
							foreach ($valores as $valor) {
								$valor_total += converte_float($valor->id2);
							}
							$novo_alienacao_valor = new alienacao_valor();
							$novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
							$novo_alienacao_valor->nu_quantidade = 1;
							$novo_alienacao_valor->va_valor = $valor_total;
							$novo_alienacao_valor->va_total = $valor_total;
							$novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_DILIGENCIA;
							$novo_alienacao_valor->in_aprovacao_orcamento = 'S';
							$novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
							
							if (!$novo_alienacao_valor->save()) {
								$erro = 1;
							}
							if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
								$erro = 1;
							}
						}  
						break;
                }

                if ($novo_andamento_alienacao->save()) {
                    $id_andamento_alienacao = $novo_andamento_alienacao->id_andamento_alienacao;
                } else {
                    $erro = 1;
                }

                if($nova_acao->tp_upload_acao>0) {
                    if ($request->session()->has('arquivos_'.$request->andamento_token)) {
                        $destino = '/alienacoes/'.$alienacao->id_alienacao.'/'.$nova_acao->id_acao_etapa;
                        $arquivos = $request->session()->get('arquivos_'.$request->andamento_token);

                        Storage::makeDirectory('/public'.$destino);

                        $erro_loop_arq = false;
                        $erro_loop_sql = false;
                        foreach ($arquivos as $arquivo) {
                            $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                            $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                            $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                            $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                            $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                            $novo_arquivo_grupo_produto->protocolo_pedido_arquivo = $arquivo['protocolo_pedido_arquivo'];

                            if ($arquivo['dt_assinado']!='') {
                                $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                            }
                            if ($arquivo['id_usuario_certificado']>0) {
                                $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                            }
                            if (!empty($arquivo['no_arquivo_p7s'])) {
                                $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                                $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                            }
                            if ($novo_arquivo_grupo_produto->save()) {
                                if (Storage::exists($origem_arquivo)) {
                                    if (Storage::exists($destino_arquivo)) {
                                        Storage::delete($destino_arquivo);
                                    }
                                    if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                        if (count($arquivo['no_arquivos_originais'])>0) {
                                            foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                                $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                                $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                                $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                                $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                                $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                                $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                                $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                                $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                                if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                    if (Storage::exists($origem_arquivo_original)) {
                                                        if (Storage::exists($destino_arquivo_original)) {
                                                            Storage::delete($destino_arquivo_original);
                                                        }
                                                        if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                            $erro_loop_arq = true;
                                                        }
                                                    } else {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_sql = true;
                                                }
                                            }
                                        }
                                    } else {
                                        $erro_loop_arq = true;
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_sql = true;
                            }
                            $novo_andamento_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto,array('in_acao'=>'S'));
                        }
                        if ($erro_loop_arq) {
                            $erro = 1;
                        }
                        if ($erro_loop_sql) {
                            $erro = 1;
                        }
                    }
                }

                if ($nova_acao->id_nova_situacao_pedido_grupo_produto>0) {
                    if ($pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $nova_acao->id_nova_situacao_pedido_grupo_produto])) {
                        $novo_historico = $historico_pedido;
                        $novo_historico->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                        $novo_historico->id_situacao_pedido_grupo_produto = $nova_acao->id_nova_situacao_pedido_grupo_produto;
                        $novo_historico->de_observacao = 'Notificacao teve o status alterado para: '.$nova_acao->id_nova_situacao_pedido_grupo_produto.'.';
                        $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_historico->save();
                    } else {
                        $erro = 1;
                    }
                }

                $response_andamento = array('tipo'=>'andamento','andamento'=>$novo_andamento_alienacao,'fase'=>$resultado_acao->nova_fase,'etapa'=>$resultado_acao->nova_etapa,'acao'=>$resultado_acao->nova_acao,'resultado'=>$resultado,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'));
            }

            if ($request->in_inserir_resultado=='S') {
                $resultado_acao = $resultado_acao->find($request->id_resultado_acao);

                $atualiza_andamento_alienacao = $andamento_alienacao->find($id_andamento_alienacao);
                $atualiza_andamento_alienacao->id_resultado_acao = $request->id_resultado_acao;
                $atualiza_andamento_alienacao->id_usuario_resultado = Auth::User()->id_usuario;
                if($resultado_acao->tp_texto_curto_resultado>0) {
                    $atualiza_andamento_alienacao->de_texto_curto_resultado = $request->de_texto_curto_resultado;
                }
                if($resultado_acao->tp_texto_longo_resultado>0) {
                    $atualiza_andamento_alienacao->de_texto_longo_resultado = $request->de_texto_longo_resultado;
                }
                if($resultado_acao->tp_data_resultado>0) {
                    $atualiza_andamento_alienacao->dt_resultado = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_resultado.' 00:00:00');
                }
                if($resultado_acao->tp_valor_resultado>0) {
                    $atualiza_andamento_alienacao->va_valor_resultado = converte_float($request->va_valor_resultado);
                }
                if (!$atualiza_andamento_alienacao->save()) {
                    $erro = 1;
                }

                //case que trata o resultado da acao
                switch ($resultado_acao->co_resultado) {
                    /*
                    O RTD não existirá mais
                    case 9: case 52: // Encaminhado ao RTD
                        $serventia_selecionada = $serventia->find($request->id_serventia);
                        $pedido->pessoas()->attach($serventia_selecionada->pessoa);
                        break;
                    */
					case 11: // Não Notificado - Enviar AR
                    case 20: // Não Notificado - AR
                    case 54: // Não Notificado - AR
                    case 63: // Não Notificado - AR
                        // Inserção do valor na tabela de valores
                        // Produto Item 31 = AR
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('".$this::ID_PRODUTO_ITEM_AR."', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $qtd = count($alienacao->endereco_alienacao)*count($alienacao->alienacao_devedor);
                            $valor_final = $valor_total*$qtd;
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = $qtd;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_final;
                            $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_AR;
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_final])) {
                                $erro = 1;
                            }
                        }
                        break;
					/*
                    O RTD não existirá mais	
                    case 19: // 1º Processamento - RTD - Notificado
                    case 20: // 1º Processamento - RTD - Não notificado - AR
                    case 21: // 1º Processamento - RTD - Endereço não Existe - Edital
                    case 62: // 2º Processamento - RTD - Notificado
                    case 63: // 2º Processamento - RTD - Não notificado - AR
                    case 64: // 2º Processamento - RTD - Endereço não Existe - Edital
					case 96:
					case 99:
					case 131: // Informar Orçamento (Edital)
                        $pedido->pessoas()->attach($alienacao->alienacao_pedido->pedido->pedido_pessoa->id_pessoa);
                        break;
                    */
                    case 34: // Aguardar autorização de segunda tentaiva - Autorizar Segunda Tentativa
                        $novo_andamento_alienacao = new andamento_alienacao();
                        $novo_andamento_alienacao->id_fase_grupo_produto = 8;
                        $novo_andamento_alienacao->id_etapa_fase = 22;
                        $novo_andamento_alienacao->id_acao_etapa = 31;
                        $novo_andamento_alienacao->id_resultado_acao = 47;
                        $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                        $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                        $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_andamento_alienacao->id_alienacao_pedido = $alienacao->alienacao_pedido->id_alienacao_pedido;
                        if (!$novo_andamento_alienacao->save()) {
                            $erro = 1;
                        }

                        if ($this->duplicar_notificacao($alienacao)) {
                            $msg_sucesso = 'Uma nova notificacao foi criada para que a 2ª tentativa seja executada. Por favor, confira seu histórico de notificações.';
                        } else {
                            $erro = 4;
                        }
                        break;
                    case 113: // Autorizar Legalização (37 - Certidão de Inteiro Teor / 39 - Averbação(Legalização) )
                        // Inserção do valor na tabela de valores
                        // Produto Item 37 - Certidão de Inteiro Teor
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('37', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = 1;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_total;
                            $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_CERTIDAO_INTEIRO_TEOR; // Certidão de Inteiro
                            //$novo_alienacao_valor->in_aprovacao_orcamento = 'S';
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                                $erro = 1;
                            }
                        }  
                        // Inserção do valor na tabela de valores
                        // Produto Item 39 - Averbação(Legalização)
                        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('39', ';', 'L')  AS (item text, id2 text);"));
                        if (count($valores)>0) {
                            $valor_total = 0;
                            foreach ($valores as $valor) {
                                $valor_total += converte_float($valor->id2);
                            }
                            $novo_alienacao_valor = new alienacao_valor();
                            $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                            $novo_alienacao_valor->nu_quantidade = 1;
                            $novo_alienacao_valor->va_valor = $valor_total;
                            $novo_alienacao_valor->va_total = $valor_total;
                            $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_AVERBACAO_LEGALIZACAO; //-- Averbação(Legalização)
                            //$novo_alienacao_valor->in_aprovacao_orcamento = 'S';
                            $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                            
                            if (!$novo_alienacao_valor->save()) {
                                $erro = 1;
                            }
                            if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                                $erro = 1;
                            }
                        }                       
                        
                        break;
                    /*
                    Removido pois agora serão aprovadas por seleção - Douglas
                    case 141: // Aprovado - Aprovar Orçamento - (AR)
                    case 143: // Aprovado - Aprovar Orçamento - (AR)
                        // Setar campo ceri.alienacao_valor.in_aprovacao_orcamento = 'S'
                        // Produto Item 31 = AR
                        /*
                        Exemplo SQL: update ceri.alienacao_valor set in_aprovacao_orcamento = 'S', dt_aprovacao_orcamento = CURRENT_DATE where id_alienacao = 463 and id_produto_item = 31 and in_aprovacao_orcamento = 'N' and in_registro_aberto = 'S'
                        * /
                        if ($alienacao->id_alienacao>0) {
                            $alienacao_valor = new alienacao_valor();
                            $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                                 ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_AR)
                                                                 ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                                 ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                                 ->first();

                            if (count($alienacao_valores)>0) {
                                $alienacao_valores->in_aprovacao_orcamento = 'S';
                                $alienacao_valores->dt_aprovacao_orcamento = Carbon::now();
                                $alienacao_valores->save();
                            }
                        }
                        break;
                    */
                    //------------------------------------------------------------------------------------------------------------------------<     
                    //------------------------------------------------------------------------------------------------------------------------>
                    //--Identificador:  16  (20170919)
                    /*
                    Removido pois agora serão aprovadas por seleção - Douglas
                    case 132: // Aprovado - Aprovar Orçamento (Edital)
                        // Setar campo ceri.alienacao_valor.in_aprovacao_orcamento = 'S'
                        // Produto Item 35 = Edital de Intimação                
                        if ($alienacao->id_alienacao>0) {
                            $alienacao_valor = new alienacao_valor();
                            $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                                 ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_EDITAL)
                                                                 ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                                 ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                                 ->first();
                            if (count($alienacao_valores)>0) {
                                $alienacao_valores->in_aprovacao_orcamento = 'S';
                                $alienacao_valores->dt_aprovacao_orcamento = Carbon::now();
                                $alienacao_valores->save();
                            }
                        }
                        break; 
                    */
                    case 95: // Reprovado - Aprovar Orçamento
                        // Excluir produto_item valor na tabela de valores
                        // Produto Item 30 = Emolumentos
                        $alienacao_valor = new alienacao_valor();
                        $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                             ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_EMOLUMENTOS)
                                                             ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                             ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                             ->first();

                        if (count($alienacao_valores)>0) {
                            $alienacao_valores->delete();
                        }
                        break;
                    case 133: // Reprovado - Aprovar Orçamento (Edital)
                        // Excluir produto_item valor na tabela de valores
                        // Produto Item 35 = Edital de Intimação                                    
                        $alienacao_valor = new alienacao_valor();
                        $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                             ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_EDITAL)
                                                             ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                             ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                             ->first();
                        if (count($alienacao_valores)>0) {
                            $alienacao_valores->delete();
                        }
                        break;
                    case 139: // Reprovado - Aprovar Orçamento - Novos Endereços
                        // Excluir produto_item valor na tabela de valores
                        // Produto Item 40 = Novos Endereços
                        $alienacao_valor = new alienacao_valor();
                        $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                             ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_NOVO_ENDERECO)
                                                             ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                             ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                             ->first();

                        if (count($alienacao_valores)>0) {
                            $alienacao_valores->delete();
                        }
                        break;
                    case 142: // Reprovado - Aprovar Orçamento - (AR)
                    case 144: // Reprovado - Aprovar Orçamento - (AR)
                        // Excluir produto_item valor na tabela de valores
                        // Produto Item 31 = AR                     
                        /*                      
                        Exemplo SQL: delete from ceri.alienacao_valor where id_alienacao = 463 and id_produto_item = 31 and in_aprovacao_orcamento = 'N' and in_registro_aberto = 'S';
                        */                      
                        $alienacao_valor = new alienacao_valor();
                        $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                             ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_AR)
                                                             ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                             ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                             ->first();

                        if (count($alienacao_valores)>0) {
                            $alienacao_valores->delete();
                        }
                        break;
                    case 146: // Voltar andamento (Efetuar Notificação [in loco]) - Aprovar Orçamento (Edital)
                        $alienacao_valor = new alienacao_valor();
                        $alienacao_valores = $alienacao_valor->where('alienacao_valor.id_alienacao', '=', $alienacao->id_alienacao)
                                                             ->where('alienacao_valor.id_produto_item', '=', $this::ID_PRODUTO_ITEM_EDITAL)
                                                             ->where('alienacao_valor.in_aprovacao_orcamento', '=', 'N')
                                                             ->where('alienacao_valor.in_registro_aberto', '=', 'S')
                                                             ->first();
                        if (count($alienacao_valores)>0) {
                            $alienacao_valores->delete();

                            $update_andamento_alienacao = $andamento_alienacao->find($id_andamento_alienacao);
                            $update_andamento_alienacao->de_demanda_manutencao_sistema = 'Via sistema(usuário)';
                            $update_andamento_alienacao->in_registro_desconsiderar = 'S';
                            $update_andamento_alienacao->de_registro_desconsiderar = 'Retornar a fase para "registro de resultado da notificação in loco", mantendo os registros atuais.';
                            $update_andamento_alienacao->id_usuario_registro_desconsiderar = Auth::User()->id_usuario;
                            $update_andamento_alienacao->dt_registro_desconsiderar = Carbon::now();
                            $update_andamento_alienacao->save();
                        }
                        break;
                    case 152: // Sim - Atualizar valores Edital
                        // Gera requerimento de atulizaçao do edital
                        $destino = 'alienacoes/' . $alienacao->id_alienacao . '/' . $novo_andamento_alienacao->id_acao_etapa;
                        $no_arquivo = 'requerimento-atualizacao-edital_' . $alienacao->alienacao_pedido->pedido->protocolo_pedido . '.pdf';
                        $destino_arquivo = '/public/' . $destino . '/' . $no_arquivo;

                        Storage::makeDirectory('/public/' . $destino);

                        $pdf = $this->gera_pdf('requerimento-atualizacao-edital', $alienacao);

                        if (Storage::exists($destino_arquivo)) {
                            Storage::delete($destino_arquivo);
                        }
                        if ($pdf->save(storage_path('app' . $destino_arquivo))) {
                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = 22;
                            $novo_arquivo_grupo_produto->no_arquivo = $no_arquivo;
                            $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($no_arquivo);
                            $novo_arquivo_grupo_produto->in_ass_digital = 'N';
                            $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($destino_arquivo);
                            $novo_arquivo_grupo_produto->no_hash = hash_file('md5', storage_path('app' . $destino_arquivo));
                            if ($novo_arquivo_grupo_produto->save()) {
                                $novo_andamento_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto, array('in_acao' => 'S'));
                            } else {
                                $erro = 1;
                            }
                        } else {
                            $erro = 1;
                        }
                        break;
                }

                if($resultado_acao->tp_upload_resultado>0) {
                    if ($request->session()->has('arquivos_'.$request->andamento_token)) {
                        $destino = '/alienacoes/'.$alienacao->id_alienacao.'/'.$resultado_acao->id_acao_etapa.'/'.$resultado_acao->id_resultado_acao;
                        $arquivos = $request->session()->get('arquivos_'.$request->andamento_token);

                        Storage::makeDirectory('/public'.$destino);

                        $erro_loop_arq = false;
                        $erro_loop_sql = false;
                        foreach ($arquivos as $key => $arquivo) {
                            $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                            $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                            $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                            $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                            $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                            $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                            if ($arquivo['dt_assinado']!='') {
                                $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                            }
                            if ($arquivo['id_usuario_certificado']>0) {
                                $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                            }
                            if (!empty($arquivo['no_arquivo_p7s'])) {
                                $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                                $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                            }
                            if ($novo_arquivo_grupo_produto->save()) {
                                if (Storage::exists($origem_arquivo)) {
                                    if (Storage::exists($destino_arquivo)) {
                                        Storage::delete($destino_arquivo);
                                    }
                                    if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                        if (count($arquivo['no_arquivos_originais'])>0) {
                                            foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                                $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                                $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                                $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                                $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                                $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                                $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                                $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                                $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                                $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                                if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                    if (Storage::exists($origem_arquivo_original)) {
                                                        if (Storage::exists($destino_arquivo_original)) {
                                                            Storage::delete($destino_arquivo_original);
                                                        }
                                                        if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                            $erro_loop_arq = true;
                                                        }
                                                    } else {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_sql = true;
                                                }
                                            }
                                        }
                                    } else {
                                        $erro_loop_arq = true;
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_sql = true;
                            }
                            $atualiza_andamento_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto,array('in_resultado'=>'S'));
                        }
                        if ($erro_loop_arq) {
                            $erro = 1;
                        }
                        if ($erro_loop_sql) {
                            $erro = 1;
                        }
                    }
                }

                if ($request->session()->has('enderecos_'.$request->andamento_token)) {
                    $enderecos = $request->session()->get('enderecos_'.$request->andamento_token);

                    $erro_loop = false;
                    if (count($enderecos)>0) {
                        foreach ($enderecos as $endereco) {
                            $novo_endereco_alienacao = new endereco_alienacao();
                            $novo_endereco_alienacao->id_alienacao = $alienacao->id_alienacao;
                            $novo_endereco_alienacao->no_endereco = mb_strtoupper($endereco['no_endereco'],'UTF-8');
                            $novo_endereco_alienacao->in_notificacao = 'S';
                            $novo_endereco_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_endereco_alienacao->in_imovel = 'N';
                            $novo_endereco_alienacao->id_andamento_alienacao = $id_andamento_alienacao;
                            if (!$novo_endereco_alienacao->save()) {
                                $erro_loop = true;
                            }
                        }
                        if ($erro_loop) {
                            $erro = 2;
                        }
                    }
                }

                if ($resultado_acao->id_nova_situacao_pedido_grupo_produto>0) {
                    if ($pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $resultado_acao->id_nova_situacao_pedido_grupo_produto])) {
                        $novo_historico = $historico_pedido;
                        $novo_historico->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                        $novo_historico->id_situacao_pedido_grupo_produto = $resultado_acao->id_nova_situacao_pedido_grupo_produto;
                        $novo_historico->de_observacao = 'Notificação teve o status alterado para: '.$resultado_acao->id_nova_situacao_pedido_grupo_produto.'.';
                        $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_historico->save();
                    } else {
                        $erro = 1;
                    }
                }

                $response_andamento = array('tipo'=>'resultado','andamento'=>$atualiza_andamento_alienacao,'resultado'=>$resultado_acao);

            }

        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            if ($msg_sucesso) {
                return response()->json(array('status'=> 'mensagem',
                                              'recarrega' => 'true',
                                              'msg' => $msg_sucesso,
                                              'andamento' => $response_andamento));
            } else {
                return response()->json(array('status'=> 'sucesso',
                                              'recarrega' => false,
                                              'msg' => 'O andamento foi inserido com sucesso.',
                                              'andamento' => $response_andamento));
            }
        }
    }

    public function alterar_imovel(Request $request, alienacao $alienacao, endereco_alienacao $endereco_alienacao) {
        $erro = 0;

        DB::beginTransaction();

        if ($request->id_alienacao > 0) {
            $alterar_endereco_alienacao = $endereco_alienacao->where('id_alienacao','=',$request->id_alienacao)
                                                             ->where('in_imovel','=','S')
                                                             ->where('in_registro_ativo','=','S')
                                                             ->first();

            $alterar_endereco_alienacao->in_registro_ativo = 'N';
            $alterar_endereco_alienacao->in_imovel = 'N';
            $alterar_endereco_alienacao->id_usuario_alt = Auth::User()->id_usuario;
            $alterar_endereco_alienacao->dt_alteracao = Carbon::now();

            if ($alterar_endereco_alienacao->save()) {
                $novo_endereco_alienacao = new endereco_alienacao();
                $novo_endereco_alienacao->id_alienacao = $request->id_alienacao;
                $novo_endereco_alienacao->no_endereco = mb_strtoupper($request->no_endereco,'UTF-8');
                $novo_endereco_alienacao->in_notificacao = 'S';
                $novo_endereco_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_endereco_alienacao->in_imovel = 'S';

                if ($novo_endereco_alienacao->save()) {
                    $alterar_endereco_alienacao->alienacao->update(['id_endereco_imovel' => $novo_endereco_alienacao->id_endereco_alienacao]);
                } else {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro > 0) {
            DB::rollback();
            return response()->json(array('status' => 'erro',
                                          'recarrega' => 'false',
                                          'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
        } else {
            DB::commit();
            return response()->json(array('status' => 'sucesso',
                                          'recarrega' => 'true',
                                          'msg' => 'O endereço foi alterado com sucesso.'));
        }
    }

    public function gerar_arquivo_previsualizacao(Request $request, alienacao $alienacao, andamento_alienacao $andamento_alienacao) {
        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);

            if ($request->de_texto_curto_acao!='') {
                $campos['de_texto_curto_acao'] = $request->de_texto_curto_acao;
            } else {
                $campos['de_texto_curto_acao'] = FALSE;
            }
            if ($request->de_texto_longo_acao!='') {
                $campos['de_texto_longo_acao'] = $request->de_texto_longo_acao;
            } else {
                $campos['de_texto_longo_acao'] = FALSE;
            }
            if ($request->va_valor_acao!='') {
                $campos['va_valor_acao'] = $request->va_valor_acao;
            } else {
                $campos['va_valor_acao'] = FALSE;
            }
            if ($request->dt_acao!='') {
                $campos['dt_acao'] = $request->dt_acao;
            } else {
                $campos['dt_acao'] = FALSE;
            }
            if ($request->gerar_capa>0) {
                $campos['gerar_capa'] = $request->gerar_capa;
            } else {
                $campos['gerar_capa'] = FALSE;
            }
            if ($request->gerar_nota>0) {
                $campos['gerar_nota'] = $request->gerar_nota;
            } else {
                $campos['gerar_nota'] = FALSE;
            }
            if ($request->arquivo_novo==="S") {
                $campos['arquivo_novo'] = $request->arquivo_novo;
            } else {
                $campos['arquivo_novo'] = FALSE;
            }

            $request->session()->flash('prev_campos',$campos);

            $tipo = $request->tipo;

            return response()->json(array('status'=>'sucesso','view'=>view('servicos.alienacao.geral-alienacao-previsualizacao',compact('alienacao','tipo'))->render()));
        }
    }

    public function gerar_arquivo_render(Request $request, alienacao $alienacao, $tipo=NULL, $id_alienacao=NULL) {
        if ($id_alienacao>0) {
            $alienacao = $alienacao->find($id_alienacao);
            $campos = $request->session()->get('prev_campos');
            $request->session()->flash('prev_campos',$campos);
            $pdf = $this->gera_pdf($tipo,$alienacao,$campos);

            return $pdf->stream();
        }
    }

    public function gerar_arquivo_salvar(Request $request, alienacao $alienacao, tipo_arquivo_grupo_produto $tipo_arquivo_grupo_produto) {
        $tipo_arquivo_grupo_produto = $tipo_arquivo_grupo_produto->find($request->id_tipo_arquivo_grupo_produto);
        if ($tipo_arquivo_grupo_produto) {
            if ($request->id_alienacao>0) {
                $alienacao = $alienacao->find($request->id_alienacao);
                $campos    = $request->session()->get('prev_campos');
                $pdf       = $this->gera_pdf($request->tipo,$alienacao,$campos);

                if (!$request->session()->has('datapasta_'.$request->token)) {
                    $request->session()->put('datapasta_'.$request->token,Carbon::now()->format('d-m-Y'));

                }
                $data_pasta = $request->session()->get('datapasta_'.$request->token);

                switch ($request->tipo) {
                    case 'carta-intimacao':
                        $no_arquivo = 'carta_intimacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';
                        break;
                    case 'edital-intimacao':
                        $no_arquivo = 'edital_intimacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';
                        break;
                    case 'requerimento-consolidacao':
                        $no_arquivo = 'requerimento_consolidacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';
                        break;
                    case 'certidao-decurso':
                        $no_arquivo = 'certidao_decurso_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';
                        break;
                    case 'cancelamento':
                        $no_arquivo = 'cancelamento-notificacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';
                        break;
                    case 'requerimento-atualizacao':
                        $no_arquivo = 'requerimento_atualizacao_edital_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';
                }

                $destino = '/temp/alienacao/'.$data_pasta.'/'.$request->token.'/'.$request->id_tipo_arquivo_grupo_produto;

                Storage::makeDirectory($destino);

                if ($pdf->save(storage_path('app/'.$destino.'/'.$no_arquivo))) {
                    if ($request->session()->has('arquivos_'.$request->token)) {
                        $arquivos = $request->session()->get('arquivos_'.$request->token);
                    } else {
                        $arquivos = array();
                    }

                    $arquivo = array('no_arquivo'=>$no_arquivo,
                                     'no_arquivos_originais'=>array(),
                                     'no_local_arquivo'=>$destino,
                                     'in_ass_digital'=>$tipo_arquivo_grupo_produto->in_ass_digital,
                                     'id_tipo_arquivo_grupo_produto'=>$tipo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto,
                                     'in_assinado'=>'N',
                                     'dt_assinado'=>'',
                                     'id_usuario_certificado'=>0,
                                     'no_arquivo_p7s'=>'',
                                     'id_flex'=>0,
                                     'protocolo'=>'');

                    if ($request->tipo == 'carta-intimacao') {
                        $protocolo_pedido_arquivo = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo_validacao(".Auth::User()->id_usuario.", ".$alienacao->alienacao_pedido->pedido->id_pedido.", '$no_arquivo');"));
                        $arquivo['protocolo_pedido_arquivo'] = $protocolo_pedido_arquivo[0]->f_geraprotocolo_validacao;
                    }

                    $arquivos[$request->index_arquivos] = $arquivo;
            
                    $request->session()->put('arquivos_'.$request->token,$arquivos);
                    
                    return response()->json(array('status' => 'sucesso',
                                                  'arquivo'=> $arquivo,
                                                  'token'=>$request->token));
                } else {
                    return response()->json(array('status'=> 'erro',
                                                  'recarrega' => 'false',
                                                  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
                }
            }
        }
    }

    public function gera_pdf($tipo,$alienacao,$campos=NULL) {
        switch ($tipo) {
            case 'carta-intimacao':
                $titulo = 'Carta de Intimação nº ';
                $pdf = PDF::loadView('pdf.alienacao.carta-intimacao',compact('titulo','alienacao','campos'));
                break;
            case 'edital-intimacao':
                $titulo = '';
                $pdf = PDF::loadView('pdf.alienacao.edital-intimacao',compact('titulo','alienacao','campos'));
                break;
            case 'requerimento-consolidacao':
                $titulo = '';
                $pdf = PDF::loadView('pdf.alienacao.requerimento-consolidacao',compact('titulo','alienacao','campos'));
                break;
            case 'certidao-decurso':
                $titulo = '';
                $footer = 'Nº Protocolo '.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'. Data de emissão: '.Carbon::now()->format('d/m/Y');
                $pdf = PDF::loadView('pdf.alienacao.certidao-decurso',compact('titulo','alienacao','campos','footer'));
                break;
            case 'cancelamento':
                $titulo = '';
                $footer = 'Nº Protocolo '.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'. Data de emissão: '.Carbon::now()->format('d/m/Y');
                $pdf = PDF::loadView('pdf.alienacao.cancelamento-notificacao',compact('titulo','alienacao','campos','footer'));
                break;
            case 'recibo-caixa':
                $titulo = 'RECIBO / PROTOCOLO';
                $footer = 'Nº Protocolo '.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'. Data de emissão: '.Carbon::now()->format('d/m/Y');
                $pdf = PDF::loadView('pdf.alienacao.alienacao-recibo-caixa',compact('titulo','alienacao','campos','footer'));
                break;
            case 'requerimento-atualizacao-edital':
                $titulo = '';
                $footer = 'Nº Protocolo '.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'. Data de emissão: '.Carbon::now()->format('d/m/Y');
                $pdf = PDF::loadView('pdf.alienacao.requerimento-atualizacao-edital',compact('titulo','alienacao','campos','footer'));
                break;
        }

        return $pdf;
    }

    public function detalhes_andamento(Request $request, andamento_alienacao $andamento_alienacao) {
        if ($request->id_andamento_alienacao>0) {
            $class = $this;
            $token = str_random(30);
            $andamento = $andamento_alienacao->find($request->id_andamento_alienacao);
            $arquivos_acao = $andamento->arquivos_grupo()->where('in_acao','S')->get();
            $arquivos_resultado = $andamento->arquivos_grupo()->where('in_resultado','S')->get();

            return view('servicos.alienacao.geral-alienacao-detalhes-andamento',compact('class','andamento','arquivos_acao','arquivos_resultado', 'token'));
        }
    }

    public function detalhes_andamento_inserir_arquivos(Request $request, andamento_alienacao $andamento_alienacao)
    {
        $erro = 0;
        $aux_origem_arquivo = (trim($request->origem_arquivo) == 'acao') ? 'acao' : 'resultado';

        $novo_andamento_alienacao = $andamento_alienacao->find($request->id_andamento_alienacao);

        if ($request->session()->has('arquivos_'.$request->andamento_token))
        {
            if ( $aux_origem_arquivo == 'acao' ){
                $destino    = '/alienacoes/'.$request->id_alienacao.'/'.$novo_andamento_alienacao->id_acao_etapa;
            }else{
                $destino    = '/alienacoes/'.$request->id_alienacao.'/'.$novo_andamento_alienacao->id_acao_etapa.'/'.$novo_andamento_alienacao->id_resultado_acao;
            }

            $arquivos   = $request->session()->get('arquivos_'.$request->andamento_token);

            Storage::makeDirectory('/public'.$destino);

            DB::beginTransaction();

            $erro_loop_arq = false;
            $erro_loop_sql = false;

            foreach ($arquivos as $key => $arquivo)
            {
                $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                if ($arquivo['dt_assinado']!='') {
                    $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                }
                if ($arquivo['id_usuario_certificado']>0) {
                    $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                }
                if (!empty($arquivo['no_arquivo_p7s'])) {
                    $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                    $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                }
                if ($novo_arquivo_grupo_produto->save()) {
                    if (Storage::exists($origem_arquivo)) {
                        if (Storage::exists($destino_arquivo)) {
                            Storage::delete($destino_arquivo);
                        }
                        if (Storage::copy($origem_arquivo,$destino_arquivo)) {

                            if (count($arquivo['no_arquivos_originais'])>0) {
                                foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                    $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                    $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                    $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                    $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                    $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                    $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                    $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                    $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                    $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                    $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                    if ($novo_arquivo_grupo_produto_composicao->save()) {
                                        if (Storage::exists($origem_arquivo_original)) {
                                            if (Storage::exists($destino_arquivo_original)) {
                                                Storage::delete($destino_arquivo_original);
                                            }
                                            if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                $erro_loop_arq = true;
                                            }
                                        } else {
                                            $erro_loop_arq = true;
                                        }
                                    } else {
                                        $erro_loop_sql = true;
                                    }
                                }
                            }
                        } else {
                            $erro_loop_arq = true;
                        }
                    } else {
                        $erro_loop_arq = true;
                    }
                } else {
                    $erro_loop_sql = true;
                }

                if ( $aux_origem_arquivo == 'acao' )
                {
                    $novo_andamento_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto,array('in_acao'=>'S'));
                }else{
                    $novo_andamento_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto,array('in_resultado'=>'S'));
                }

            }
            if ($erro_loop_arq) {
                $erro = 1;
            }
            if ($erro_loop_sql) {
                $erro = 1;
            }
        }


        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
                return response()->json(array('status'      => 'sucesso',
                                              'recarrega'   => 'true',
                                              'msg'         => 'Arquivo(s) inserido(s) com sucesso.',
                                              ));

        }
    }

    public function duplicar_notificacao($alienacao) {
        $erro = 0;

        DB::beginTransaction();

        $protocoloPedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

        $novo_pedido = new pedido();
        $novo_pedido->id_usuario = Auth::User()->id_usuario;
        $novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_EMPROCESSAMENTO;
        $novo_pedido->id_produto = $this::ID_PRODUTO;
        $novo_pedido->id_alcada = 8;
        $novo_pedido->protocolo_pedido = $protocoloPedido[0]->f_geraprotocolo;
        $novo_pedido->dt_pedido = Carbon::now();
        $novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
        $novo_pedido->dt_cadastro = Carbon::now();
        $novo_pedido->va_pedido = 0;
        $novo_pedido->id_pedido_anterior = $alienacao->alienacao_pedido->id_pedido;
        $novo_pedido->id_pessoa_origem = $alienacao->alienacao_pedido->pedido->id_pessoa_origem;

        if ($novo_pedido->save()) {
            $serventia = new serventia();
            $serventia_selecionada = $serventia->find($alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->serventia->id_serventia);
            $novo_pedido->pessoas()->attach($serventia_selecionada->pessoa);

            $novo_pedido_produto_item = new pedido_produto_item();
            $novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
            $novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM_SERVICOS_ANOREG;
            $novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_pedido_produto_item->save()) {
                $erro = 1;
            }

            $novo_pedido_produto_item = new pedido_produto_item();
            $novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
            $novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM_EMOLUMENTOS;
            $novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_pedido_produto_item->save()) {
                $erro = 1;
            }

            $novo_pedido_produto_item = new pedido_produto_item();
            $novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
            $novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM_AR;
            $novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_pedido_produto_item->save()) {
                $erro = 1;
            }

            $nova_alienacao = new alienacao();
            $nova_alienacao->numero_contrato = $alienacao->numero_contrato;
            $nova_alienacao->registro_imovel = $alienacao->registro_imovel;
            $nova_alienacao->matricula_imovel = $alienacao->matricula_imovel;
            $nova_alienacao->dt_contrato = $alienacao->dt_contrato;
            $nova_alienacao->id_usuario_cad = Auth::User()->id_usuario;
            $nova_alienacao->id_serventia = $alienacao->id_serventia;
            $nova_alienacao->dt_cadastro = Carbon::now();
            $nova_alienacao->va_divida 	= $alienacao->va_divida;
            $nova_alienacao->va_leilao 	= $alienacao->va_leilao;
            $nova_alienacao->id_legado 	= $alienacao->id_legado;

            if ($nova_alienacao->save()) {
                $nova_alienacao_pedido = new alienacao_pedido();
                $nova_alienacao_pedido->id_pedido = $novo_pedido->id_pedido;
                $nova_alienacao_pedido->id_alienacao = $nova_alienacao->id_alienacao;
                if (!$nova_alienacao_pedido->save()) {
                    $erro = 1;
                }

                $nova_alienacao_credor_alienacao = new alienacao_credor_alienacao();
                $nova_alienacao_credor_alienacao->id_credor_alienacao = $alienacao->credor_alienacao->id_credor_alienacao;
                $nova_alienacao_credor_alienacao->id_alienacao = $nova_alienacao->id_alienacao;
                if (!$nova_alienacao_credor_alienacao->save()) {
                    $erro = 1;
                }

                $nova_alienacao_projecao = new alienacao_projecao();
                $nova_alienacao_projecao->id_alienacao  = $nova_alienacao->id_alienacao;
                $nova_alienacao_projecao->va_periodo_01 = $alienacao->alienacao_projecao->va_periodo_01;
                $nova_alienacao_projecao->va_periodo_02 = $alienacao->alienacao_projecao->va_periodo_02;
                $nova_alienacao_projecao->va_periodo_03 = $alienacao->alienacao_projecao->va_periodo_03;
                $nova_alienacao_projecao->va_periodo_04 = $alienacao->alienacao_projecao->va_periodo_04;
                $nova_alienacao_projecao->nu_dia_periodo_01 = 15;
                $nova_alienacao_projecao->nu_dia_periodo_02 = 15;
                $nova_alienacao_projecao->nu_dia_periodo_03 = 15;
                $nova_alienacao_projecao->nu_dia_periodo_04 = 15;
                $nova_alienacao_projecao->in_registro_ativo = 'S';
                $nova_alienacao_projecao->id_usuario_cad = Auth::User()->id_usuario;
                $nova_alienacao_projecao->dt_cadastro = Carbon::now();

                if (!$nova_alienacao_projecao->save()) {
                    $erro = 1;
                }

                if(count($alienacao->parcela_alienacao)>0) {
                    $erro_loop = false;
                    foreach ($alienacao->parcela_alienacao as $parcela) {
                        $nova_parcela_alienacao = new parcela_alienacao();
                        $nova_parcela_alienacao->id_alienacao = $nova_alienacao->id_alienacao;
                        $nova_parcela_alienacao->nu_parcela = $parcela->nu_parcela;
                        $nova_parcela_alienacao->dt_vencimento = $parcela->dt_vencimento;
                        $nova_parcela_alienacao->va_valor = $parcela->va_valor;
                        $nova_parcela_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                        if (!$nova_parcela_alienacao->save()) {
                            $erro_loop = true;
                        }
                    }
                    if ($erro_loop) {
                        $erro = 2;
                    }
                }

                if(count($alienacao->alienacao_devedor)>0) {
                    $erro_loop = false;
                    foreach ($alienacao->alienacao_devedor as $devedor) {
                        $nova_alienacao_devedor = new alienacao_devedor();
                        $nova_alienacao_devedor->nu_cpf_cnpj = $devedor->nu_cpf_cnpj;
                        $nova_alienacao_devedor->no_devedor = $devedor->no_devedor;
                        $nova_alienacao_devedor->id_usuario_cad = Auth::User()->id_usuario;
                        $nova_alienacao_devedor->in_registro_ativo = 'S';
                        if (!$nova_alienacao_devedor->save()) {
                            $erro_loop = true;
                        }

                        $nova_alienacao_devedor_devedor = new alienacao_devedor_devedor();
                        $nova_alienacao_devedor_devedor->id_alienacao = $nova_alienacao->id_alienacao;
                        $nova_alienacao_devedor_devedor->id_alienacao_devedor = $nova_alienacao_devedor->id_alienacao_devedor;
                        if (!$nova_alienacao_devedor_devedor->save()) {
                            $erro_loop = true;
                        }
                    }
                    if ($erro_loop) {
                        $erro = 2;
                    }
                }

                if(count($alienacao->endereco_alienacao)>0) {
                    $erro_loop = false;
                    foreach ($alienacao->endereco_alienacao as $endereco) {
                        $novo_endereco_alienacao = new endereco_alienacao();
                        $novo_endereco_alienacao->id_alienacao = $nova_alienacao->id_alienacao;
                        $novo_endereco_alienacao->no_endereco = $endereco->no_endereco;
                        $novo_endereco_alienacao->nu_numero = $endereco->nu_numero;
                        $novo_endereco_alienacao->no_complemento = $endereco->no_complemento;
                        $novo_endereco_alienacao->no_bairro = $endereco->no_bairro;
                        $novo_endereco_alienacao->nu_cep = $endereco->nu_cep;
                        $novo_endereco_alienacao->in_notificacao = 'S';
                        $novo_endereco_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_endereco_alienacao->id_cidade = $endereco->id_cidade;
                        $novo_endereco_alienacao->in_imovel = $endereco->in_imovel;
                        if (!$novo_endereco_alienacao->save()) {
                            $erro_loop = true;
                        }
                        if ($endereco->in_imovel=='S') {
                            $id_endereco_imovel = $novo_endereco_alienacao->id_endereco_alienacao;
                        }
                    }
                    if ($erro_loop) {
                        $erro = 2;
                    }

                    $nova_alienacao->id_endereco_imovel = $id_endereco_imovel;
                    if (!$nova_alienacao->save()) {
                        $erro = 1;
                    }
                }

                if(count($alienacao->alienacao_arquivo)>0) {
                    $erro_loop = false;
                    foreach ($alienacao->alienacao_arquivo as $arquivo) {
                        if (Storage::copy('/public/'.$arquivo->arquivo_grupo_produto->no_local_arquivo.'/'.$arquivo->arquivo_grupo_produto->no_arquivo,'/public/alienacoes/'.$nova_alienacao->id_alienacao.'/'.$arquivo->arquivo_grupo_produto->no_arquivo)) {
                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = 1;
                            $novo_arquivo_grupo_produto->no_arquivo = $arquivo->arquivo_grupo_produto->no_arquivo;
                            $novo_arquivo_grupo_produto->no_local_arquivo = 'alienacoes/'.$nova_alienacao->id_alienacao;
                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_arquivo_grupo_produto->no_extensao = $arquivo->arquivo_grupo_produto->no_extensao;
                            if (!$novo_arquivo_grupo_produto->save()) {
                                $erro_loop = true;
                            }

                            $nova_alienacao_arquivo_grupo_produto = new alienacao_arquivo_grupo_produto();
                            $nova_alienacao_arquivo_grupo_produto->id_alienacao = $nova_alienacao->id_alienacao;
                            $nova_alienacao_arquivo_grupo_produto->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                            $nova_alienacao_arquivo_grupo_produto->id_fase_grupo_produto = $this::ID_FASE_CADASTRO;
                            if (!$nova_alienacao_arquivo_grupo_produto->save()) {
                                $erro_loop = true;
                            }
                        } else {
                            $erro_loop = true;
                        }
                    }
                    if ($erro_loop) {
                        $erro = 3;
                    }
                }

                // Recadastramento
                $novo_andamento_alienacao = new andamento_alienacao();
                $novo_andamento_alienacao->id_fase_grupo_produto = 10;
                $novo_andamento_alienacao->id_etapa_fase = 34;
                $novo_andamento_alienacao->id_acao_etapa = 52;
                $novo_andamento_alienacao->id_resultado_acao = 90;
                $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_alienacao_pedido = $nova_alienacao_pedido->id_alienacao_pedido;
                if (!$novo_andamento_alienacao->save()) {
                    $erro = 1;
                }

                $novo_historico = new historico_pedido();
                $novo_historico->id_pedido = $novo_pedido->id_pedido;
                $novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
                $novo_historico->id_alcada = 8;
                $novo_historico->de_observacao = 'Notificacao duplicada com sucesso.';
                $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                $novo_historico->save();

                // Reencaminhamento automático
                $novo_andamento_alienacao = new andamento_alienacao();
                $novo_andamento_alienacao->id_fase_grupo_produto = 10;
                $novo_andamento_alienacao->id_etapa_fase = 35;
                $novo_andamento_alienacao->id_acao_etapa = 53;
                $novo_andamento_alienacao->id_resultado_acao = 91;
                $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_alienacao_pedido = $nova_alienacao_pedido->id_alienacao_pedido;
                if (!$novo_andamento_alienacao->save()) {
                    $erro = 1;
                }

                // Inserção do valor principal da alienação na tabela de valores
                // Produto Item 28 = Alienação Fidunciária
                $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('".$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG."', ';', 'L')  AS (item text, id2 text);"));
                if (count($valores)>0) {
                    $valor_total = 0;
                    foreach ($valores as $valor) {
                        $valor_total += converte_float($valor->id2);
                    }
                    $novo_alienacao_valor = new alienacao_valor();
                    $novo_alienacao_valor->id_alienacao = $nova_alienacao->id_alienacao;
                    $novo_alienacao_valor->nu_quantidade = 1;
                    $novo_alienacao_valor->va_valor = $valor_total;
                    $novo_alienacao_valor->va_total = $valor_total;
                    $novo_alienacao_valor->id_produto_item = $this::ID_PRODUTO_ITEM_SERVICOS_ANOREG;
                    $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;
                    if (!$novo_alienacao_valor->save()) {
                        $erro = 1;
                    }
                }
                $pedido = new pedido();
                if (!$pedido->where('id_pedido',$novo_pedido->id_pedido)->update(['va_pedido' => $valor_total])) {
                    $erro = 1;
                }

                $novo_historico = new historico_pedido();
                $novo_historico->id_pedido = $novo_pedido->id_pedido;
                $novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_EMPROCESSAMENTO;
                $novo_historico->id_alcada = 8;
                $novo_historico->de_observacao = 'Notificacao encaminhada com sucesso.';
                $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                $novo_historico->save();
            } else {
                $erro = 1;
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return false;
        } else {
            DB::commit();
            return true;
        }
    }

    public function detalhes_custas(Request $request, alienacao_valor $alienacao_valor) {
        if ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_CAIXA )
        {
            $alienacao_valor = $alienacao_valor->select(
                'alienacao_valor.id_alienacao',
                'alienacao_valor.id_produto_item',
                'alienacao_valor.va_valor',
                'alienacao_valor.va_total',
                'alienacao_valor.dt_cadastro',
                'alienacao_valor.nu_quantidade',
                'produto_item.no_produto_item',
                'avr.dt_repasse as dt_pagamento'
            )->selectRaw(' (select case
                               when avr2.dt_repasse is null then \'(*) Prestação de Serviço ANOREG/MS (Em aberto)\'
                               else \'(*) Prestação de Serviço ANOREG/MS (Pago)\'
                             end as repasse_servico 
                      from ceri.alienacao_valor av2
                      left outer join ceri.alienacao_valor_repasse avr2 on (avr2.id_alienacao_valor = av2.id_alienacao_valor and avr2.id_repasse = 1)
                      where av2.id_alienacao    = alienacao_valor.id_alienacao 
                      and av2.id_produto_item = '.$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG.'
                      limit 1) as legenda'
            )
                ->join('produto_item','produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                ->leftjoin('alienacao_valor_repasse as avr', function($left){
                    $left->on('avr.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                        ->on('avr.id_repasse', '=',  DB::raw('1'));
                })

                ->where('alienacao_valor.id_alienacao', '=', $request->id_alienacao)
                ->where('alienacao_valor.id_produto_item', '<>',$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG)
                ->orderBy('alienacao_valor.dt_cadastro','asc');

        } elseif ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG){

            $alienacao_valor = $alienacao_valor->select(
                'alienacao_valor.id_alienacao',
                'alienacao_valor.id_produto_item',
                'alienacao_valor.va_valor',
                'alienacao_valor.va_total',
                'alienacao_valor.dt_cadastro',
                'alienacao_valor.nu_quantidade',
                'produto_item.no_produto_item',
                'avr.dt_repasse as dt_pagamento',
                'avr2.dt_repasse as dt_repasse'
            )
                ->join('produto_item','produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                ->leftjoin('alienacao_valor_repasse as avr', function($left){
                    $left->on('avr.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                        ->on('avr.id_repasse', '=',  DB::raw('1'));
                })
                ->leftjoin('alienacao_valor_repasse as avr2', function($left){
                    $left->on('avr2.id_alienacao_valor', '=', DB::raw(' alienacao_valor.id_alienacao_valor and ((( avr2.id_repasse = 3) and 
                                                                                                        ( alienacao_valor.id_produto_item = '.$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG.')) or 
                                                                                                        ( (avr2.id_repasse = 4) and (alienacao_valor.id_produto_item <> '.$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG.') ) ) '));

                })
                ->where('alienacao_valor.id_alienacao', '=', $request->id_alienacao)
                ->orderBy('alienacao_valor.dt_cadastro','asc');
        } elseif ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_SERVENTIA){

            $alienacao_valor = $alienacao_valor->select(
                'alienacao_valor.id_alienacao',
                'alienacao_valor.id_produto_item',
                'alienacao_valor.va_valor',
                'alienacao_valor.va_total',
                'alienacao_valor.dt_cadastro',
                'alienacao_valor.nu_quantidade',
                'produto_item.no_produto_item',
                'avr.dt_repasse as dt_pagamento',
                'avr2.dt_repasse as dt_repasse'
            )
                ->join('produto_item','produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                ->leftjoin('alienacao_valor_repasse as avr', function($left){
                    $left->on('avr.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                        ->on('avr.id_repasse', '=',  DB::raw('1'));
                })
                ->leftjoin('alienacao_valor_repasse as avr2', function($left){
                    $left->on('avr2.id_alienacao_valor', '=', DB::raw(' alienacao_valor.id_alienacao_valor and ((( avr2.id_repasse = 3) and 
                                                                                                        ( alienacao_valor.id_produto_item = '.$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG.')) or 
                                                                                                        ( (avr2.id_repasse = 4) and (alienacao_valor.id_produto_item <> '.$this::ID_PRODUTO_ITEM_SERVICOS_ANOREG.') ) ) '));

                })
                ->where('alienacao_valor.id_alienacao', '=', $request->id_alienacao)
                ->orderBy('alienacao_valor.dt_cadastro','asc');
        }

        $alienacao_valor = $alienacao_valor->get();
        $class = $this;

        return view('servicos.alienacao.geral-alienacao-detalhes-custas',compact('alienacao_valor', 'class'));
    }

    public function relatorio_alienacoes(Request $request, alienacao $alienacao) {
        $alienacoes = $alienacao->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
            					->join('alienacao_andamento_situacao',function($join) {
                					$join->on('alienacao_andamento_situacao.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                    					 ->where('alienacao_andamento_situacao.in_registro_ativo','=','S');
            					});

        if($request->ids_alienacoes>0) {
            $alienacoes->whereIn('alienacao.id_alienacao', $request->ids_alienacoes);
        } else {
            $alienacoes->join('intimacao_arquivo_xml','intimacao_arquivo_xml.id_intimacao_arquivo_xml','=','alienacao.id_intimacao_arquivo_xml')
                ->join('alienacao_arquivo_xml',function($join) use ($request) {
                    $join->on('alienacao_arquivo_xml.id_alienacao_arquivo_xml','=','intimacao_arquivo_xml.id_alienacao_arquivo_xml')
                        ->whereIn('intimacao_arquivo_xml.id_alienacao_arquivo_xml',$request->ids_alienacao_arquivo_xml);
                });
        }

        $alienacoes = $alienacoes->orderBy('alienacao_andamento_situacao.dt_cadastro', 'desc')->get();

        return view('servicos.alienacao.geral-alienacao-relatorio',compact('alienacoes'));
    }

    public function salvar_relatorio_alienacoes(Request $request, alienacao $alienacao) {
        $alienacoes = $alienacao->whereIn('id_alienacao',$request->id_alienacao)->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('notificacoes'.$time, function($excel) use ($alienacoes) {

            $excel->sheet('Notificações', function($sheet) use ($alienacoes) {

                $sheet->loadView('xls.relatorio-alienacoes', array('alienacoes' => $alienacoes));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'C' =>\PHPExcel_Style_NumberFormat::FORMAT_NUMBER,
                    'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->getStyle('C')->getNumberFormat()->setFormatCode('0');
                $sheet->setAutoSize(true);
            });

        })->export('xls');
    }

    public function aprovar_decursos_prazo(Request $request, alienacao $alienacao, pedido $pedido) {
        $erro = 0;

        DB::beginTransaction();

        if (count($request->ids_alienacoes)>0) {
            foreach ($request->ids_alienacoes as $id_alienacao) {
                $alienacao = $alienacao->find($id_alienacao);

                $andamento_alienacao = new andamento_alienacao();
                $andamento_alienacao = $andamento_alienacao->find($alienacao->alienacao_pedido->andamento_alienacao[0]->id_andamento_alienacao);
                $andamento_alienacao->id_resultado_acao = 38;
                $andamento_alienacao->dt_resultado = Carbon::now();
                $andamento_alienacao->id_usuario_resultado = Auth::User()->id_usuario;

                if (!$andamento_alienacao->save()) {
                    $erro = 1;
                }

                $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('36', ';', 'L')  AS (item text, id2 text);"));

                if (count($valores)>0) {
                    $valor_total = 0;

                    foreach ($valores as $valor) {
                        $valor_total += converte_float($valor->id2);
                    }
                    $novo_alienacao_valor = new alienacao_valor();
                    $novo_alienacao_valor->id_alienacao = $alienacao->id_alienacao;
                    $novo_alienacao_valor->nu_quantidade = 1;
                    $novo_alienacao_valor->va_valor = $valor_total;
                    $novo_alienacao_valor->va_total = $valor_total;
                    $novo_alienacao_valor->id_produto_item = 36;
                    $novo_alienacao_valor->in_aprovacao_orcamento = 'S';
                    $novo_alienacao_valor->id_usuario_cad = Auth::User()->id_usuario;

                    if (!$novo_alienacao_valor->save()) {
                        $erro = 1;
                    }

                    if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['va_pedido' => $alienacao->alienacao_pedido->pedido->va_pedido+$valor_total])) {
                        $erro = 1;
                    }
                }

                /**
                 * Foi alterado o id_situacao_pedido_grupo_produto para eliminar um andamento que estava sobrando
                 *
                 * Anteriomente id_situacao_pedido_grupo_produto era ID_SITUACAO_EMPROCESSAMENTO (55) agora ID_SITUACAO_AGUARDANDOPAGAMENTO (65)
                 *
                 */
                //inserindo o historico de modificacao
                if ($pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_AGUARDANDOPAGAMENTO])) {
                    $novo_historico = new historico_pedido();
                    $novo_historico->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                    $novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_AGUARDANDOPAGAMENTO;
                    $novo_historico->de_observacao = 'Notificacao teve o status alterado para: '.$this::ID_SITUACAO_AGUARDANDOPAGAMENTO.'.';
                    $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_historico->save();
                } else {
                    $erro = 1;
                }

            }

            // Tratamento do retorno
            if ($erro > 0) {
                DB::rollback();
                return response()->json(array('status' => 'erro',
                                              'recarrega' => 'false',
                                              'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
            } else {
                DB::commit();

                $msg = count($request->ids_alienacoes)>1 ? 'Os decursos de prazo foram aprovados com sucesso.' : 'O decurso de prazo foi aprovada com sucesso.';

                return response()->json(array('status' => 'sucesso',
                                              'recarrega' => 'true',
                                              'msg' => $msg));
            }
        }
    }

    public function aprovar_orcamentos(Request $request, alienacao $alienacao, pedido $pedido, historico_pedido $historico_pedido, andamento_alienacao $andamento_alienacao, alienacao_valor $alienacao_valor) {
        $erro = 0;

        DB::beginTransaction();

        if (count($request->ids_alienacoes)>0) {
            foreach ($request->ids_alienacoes as $id_alienacao) {
                $alienacao = $alienacao->find($id_alienacao);

                if ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto==$this::ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO) {
                    $produto_itens = array($this::ID_PRODUTO_ITEM_EMOLUMENTOS, $this::ID_PRODUTO_ITEM_AR, $this::ID_PRODUTO_ITEM_EDITAL, $this::ID_PRODUTO_ITEM_NOVO_ENDERECO);

                    $alienacao_valor = new alienacao_valor();
                    $alienacao_valor = $alienacao_valor->where('id_alienacao', '=', $alienacao->id_alienacao)
                                                       ->whereIn('id_produto_item', $produto_itens)
                                                       ->where('in_aprovacao_orcamento', '=', 'N')
                                                       ->first();

                    if ($alienacao_valor) {
                        $alienacao_valor->in_aprovacao_orcamento = 'S';
                        $alienacao_valor->dt_aprovacao_orcamento = Carbon::now();
                        if (!$alienacao_valor->save()) {
                            $erro = 1;
                        }
                    } else {
                        $erro = 1;
                    }

                    $ultimo_andamento = $alienacao->alienacao_pedido->andamento_alienacao[0];

                    $resultado_aprovar = new resultado_acao();
                    $resultado_aprovar = $resultado_aprovar->where('id_acao_etapa',$ultimo_andamento->resultado_acao->id_nova_acao_etapa)
                                                           ->where('in_positivo','S')
                                                           ->first();

                    $novo_andamento_alienacao = new andamento_alienacao();
                    $novo_andamento_alienacao->id_fase_grupo_produto = $ultimo_andamento->resultado_acao->id_nova_fase_grupo_produto;
                    $novo_andamento_alienacao->id_etapa_fase = $ultimo_andamento->resultado_acao->id_nova_etapa_fase;
                    $novo_andamento_alienacao->id_acao_etapa = $ultimo_andamento->resultado_acao->id_nova_acao_etapa;
                    $novo_andamento_alienacao->id_resultado_acao = $resultado_aprovar->id_resultado_acao;
                    $novo_andamento_alienacao->dt_resultado = Carbon::now();
                    $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_usuario_resultado = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_andamento_alienacao->id_alienacao_pedido = $alienacao->alienacao_pedido->id_alienacao_pedido;

                    if ($novo_andamento_alienacao->save()) {
                        $id_situacao_pedido_grupo_produto = $novo_andamento_alienacao->resultado_acao->id_resultado_acao === 132 ? $this::ID_SITUACAO_AGUARDANDOINTERACAO : $this::ID_SITUACAO_EMPROCESSAMENTO;
						if ($pedido->where('id_pedido', $alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $id_situacao_pedido_grupo_produto])) {
                            $historico_pedido->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                            $historico_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_EMPROCESSAMENTO;
                            switch ($this->id_tipo_pessoa) {
                                case 3:
                                    $historico_pedido->id_alcada = alcada::ID_ALCADA_PESSOA_FISICA;
                                    break;
                                case 4:
                                case 5:
                                    $historico_pedido->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
                                    break;
                            }
                            $historico_pedido->de_observacao = 'Notificacao retornada para a serventia com sucesso.';
                            $historico_pedido->id_usuario_cad = Auth::User()->id_usuario;
                            $historico_pedido->save();
                        } else {
                            $erro = 1;
                        }
                    } else {
                        $erro = 1;
                    }
                } else {
                    $erro = 1;
                }
            }
        } else {
            $erro = 1;
        }

        // Tratamento do retorno
        if ($erro > 0) {
            DB::rollback();
            return response()->json(array('status' => 'erro',
                                          'recarrega' => 'false',
                                          'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
        } else {
            DB::commit();

            $msg = count($request->ids_alienacoes)>1 ? 'Os orçamentos foram aprovados com sucesso.' : 'O orçamento foi aprovado com sucesso.';

            return response()->json(array('status' => 'sucesso',
                                          'recarrega' => 'true',
                                          'msg' => $msg));
        }
    }
    public function cancelar_alienacoes(Request $request, alienacao $alienacao, pedido $pedido)
    {

        DB::beginTransaction();

        $ceriFuncoes   = new CeriFuncoes();
        $erro          = $ceriFuncoes->cancelar_alienacoes($request->ids_alienacoes, $this);

        // Tratamento do retorno
        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();

            $msg = count($request->ids_alienacoes)>1 ? 'As notificações foram canceladas com sucesso.' : 'A notificação foi cancelada com sucesso.';

            return response()->json(array('status'=> 'sucesso',
                'recarrega' => 'true',
                'msg' => $msg));
        }
    }
    
    public function relatorio_cancelamento(Request $request, alienacao $alienacao) {
        $alienacoes = $alienacao->whereIn('id_alienacao',$request->ids_alienacoes)->orderBy('dt_cadastro','desc')->get();

        return view('servicos.alienacao.geral-alienacao-relatorio-cancelamento',compact('alienacoes', 'request'));
    }

    public function salvar_relatorio_cancelamento(Request $request, alienacao $alienacao) {
        $alienacoes = $alienacao->whereIn('id_alienacao',$request->id_alienacao)->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('cancelamento_'.$time, function($excel) use ($alienacoes) {

            $excel->sheet('Notificações', function($sheet) use ($alienacoes) {

                $sheet->loadView('xls.alienacao-cancelamento-notificacoes', array('alienacoes' => $alienacoes));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->setAutoSize(true);
            });

        })->export('xls');
    }  

    public function cancelar_contratos(Request $request, alienacao $alienacao){

        set_time_limit(500);

        $alienacoes = array();

        $modulos = $this->modulos;

        if ($request->contratos!="") {
            $contratos = explode(PHP_EOL, $request->contratos);
            $contratos = array_filter($contratos);

            $array_contratos = array();

            foreach ($contratos as $contrato) {
                $contrato = str_replace(["\r", "\n"],'',$contrato);
                $array_contratos[] = $contrato;
            }
            $alienacoes = $alienacao->join('alienacao_pedido', 'alienacao_pedido.id_alienacao', '=', 'alienacao.id_alienacao')
                ->join('pedido', 'pedido.id_pedido', '=', 'alienacao_pedido.id_pedido')
                ->join('alienacao_andamento_situacao', function ($join) {
                    $join->on('alienacao_andamento_situacao.id_alienacao_pedido', '=', 'alienacao_pedido.id_alienacao_pedido')
                        ->where('alienacao_andamento_situacao.in_registro_ativo', '=', 'S')
                        ->where('alienacao_andamento_situacao.id_acao_etapa', '<>', DB::raw('48'));
                })
                ->whereNotIn('pedido.id_situacao_pedido_grupo_produto', [$this::ID_SITUACAO_CANCELADO, $this::ID_SITUACAO_FINALIZADO]);

            if(strlen($array_contratos[0])>12){
                $alienacoes = $alienacoes->whereIn('protocolo_pedido', $array_contratos)->orderBy('protocolo_pedido')->get();
            } else {
                $alienacoes = $alienacoes->whereIn('numero_contrato', $array_contratos)->orderBy('numero_contrato')->get();
            }
        }

        return view('servicos.alienacao.geral-alienacao-cancelar-contratos',compact('this','alienacoes','modulos','request'));
    }

    public function gerar_recibo_caixa(Request $request, alienacao $alienacao)
    {
            $id_alienacao  = $request->id_alienacao;
            $tipo  = $request->tipo;

            return response()->json(['view'=>view('servicos.alienacao.geral-recibo-caixa', compact('id_alienacao', 'tipo'))->render()]);
    }

    public function imprimir_recibo_caixa(Request $request, alienacao $alienacao)
    {
        if ($request->id_alienacao>0){
            $tipo = $request->tipo;
            $alienacao = $alienacao->find($request->id_alienacao);
            $pdf = $this->gera_pdf($tipo,$alienacao);

            return $pdf->stream();
        }
    }

    public function observacoes(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);
            $pessoa_ativa = Session::get('pessoa_ativa');

            /*$alienacao_observacao = new alienacao_observacao();
            $alienacao_observacao->where('id_alienacao',$alienacao->id_alienacao)
                                 ->where('id_pessoa_dest',$this->id_pessoa)
                                 ->where('in_leitura','N')
                                 ->update(['in_leitura'=>'S']);*/

            $class = $this;
            $observacao_token = str_random(30);

            return view('servicos.alienacao.geral-alienacao-observacoes',compact('class','alienacao', 'observacoes', 'observacao_token', 'pessoa_ativa'));
        }
    }
    public function inserir_observacao(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);
            $erro = 0;

            switch ($this->id_tipo_pessoa) {
                case 2: case 10:
                    $id_pessoa_dest = $alienacao->alienacao_pedido->pedido->pessoa_origem->id_pessoa;
                    break;
                default:
                    $id_pessoa_dest = $alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->id_pessoa;
                    break;
            }

            DB::beginTransaction();

            $nova_observacao = new alienacao_observacao();
            $nova_observacao->id_alienacao = $alienacao->id_alienacao;
            $nova_observacao->id_pessoa = $this->id_pessoa;
            $nova_observacao->id_pessoa_dest = $id_pessoa_dest;
            $nova_observacao->de_observacao = $request->de_observacao;
            $nova_observacao->id_usuario_cad = Auth::User()->id_usuario;
            $nova_observacao->dt_cadastro = Carbon::now();

            if ($nova_observacao->save()) {
                // ***** CODIGO INSERIR ARQUIVO ******
                if ($request->session()->has('arquivos_'.$request->observacao_token)) {
                    $destino = '/observacoes/'.$nova_observacao->id_alienacao;
                    $arquivos = $request->session()->get('arquivos_'.$request->observacao_token);

                    Storage::makeDirectory('/public'.$destino);

                    $erro_loop_arq = false;
                    $erro_loop_sql = false;
                    foreach ($arquivos as $key => $arquivo) {
                        $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                        $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                        $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                        $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                        $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                        $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                        $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                        $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                        $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                        $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                        $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                        if ($arquivo['dt_assinado']!='') {
                            $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                        }
                        if ($arquivo['id_usuario_certificado']>0) {
                            $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                        }
                        if (!empty($arquivo['no_arquivo_p7s'])) {
                            $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                            $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                        }
                        if ($novo_arquivo_grupo_produto->save()) {
                            if (Storage::exists($origem_arquivo)) {
                                if (Storage::exists($destino_arquivo)) {
                                    Storage::delete($destino_arquivo);
                                }
                                if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                    if (count($arquivo['no_arquivos_originais'])>0) {
                                        foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                            $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                            $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                            $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                            $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                            $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                            $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                            $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                            $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                            $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                            $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                            if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                if (Storage::exists($origem_arquivo_original)) {
                                                    if (Storage::exists($destino_arquivo_original)) {
                                                        Storage::delete($destino_arquivo_original);
                                                    }
                                                    if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_arq = true;
                                                }
                                            } else {
                                                $erro_loop_sql = true;
                                            }
                                        }
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_arq = true;
                            }
                        } else {
                            $erro_loop_sql = true;
                        }
                        $nova_observacao->alienacao_observacao_arquivo_grupo()->attach($novo_arquivo_grupo_produto);
                    }
                    if ($erro_loop_arq) {
                        $erro = 1;
                    }
                    if ($erro_loop_sql) {
                        $erro = 1;
                    }

                    $request->session()->forget('arquivos_' . $request->observacao_token);
                }
                // ***** FIM CODIGO INSERIR ARQUIVO *****
            }

            if ($erro>0) {
                DB::rollback();
                return response()->json(array('status'=> 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status'=> 'sucesso',
                    'recarrega' => 'false',
                    'msg' => 'A observação foi inserida com sucesso.',
                    'observacao'=>$nova_observacao,
                    'pessoa'=>$nova_observacao->pessoa->no_pessoa,
                    'usuario'=>$nova_observacao->usuario_cad->no_usuario,
                    'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'),
                    'arquivo' => $nova_observacao->alienacao_observacao_arquivo_grupo
                ));
            }
        }
    }

    public function status_observacao(Request $request, alienacao_observacao $alienacao_observacao) {
        $pessoa_ativa = Session::get('pessoa_ativa');

        $alienacao_observacao = $alienacao_observacao->where('id_alienacao_observacao',$request->id_alienacao_observacao)
                                                     ->where('id_pessoa_dest',$pessoa_ativa->pessoa->id_pessoa)
                                                     ->first();

        if ($alienacao_observacao && $request->status) {
            $alienacao_observacao->in_leitura = $request->status;
            $alienacao_observacao->id_usuario_leitura = Auth::User()->id_usuario;
            $alienacao_observacao->dt_leitura = Carbon::now();
            $alienacao_observacao->id_usuario_leitura_cancelado = Auth::User()->id_usuario;
            $alienacao_observacao->dt_leitura_cancelado = Carbon::now();
            $alienacao_observacao->save();

            $status = $alienacao_observacao->in_leitura == 'S' ? 'lida' : 'não lida';

            return response()->json(array('status'=>'sucesso',
                'recarrega'=>'false',
                'msg'=>'Observação marcada como '.$status.'.',
                'recarrega' => 'true'
            ));
        } else {
            return response()->json(array('status'=>'erro',
                'recarrega'=>'false',
                'msg'=>'Não foi possível alterar o status do registro.',
                'recarrega' => 'true'
            ));
        }
    }

    public function filtro_alertas(Request $request, alienacao $alienacao) {
        if ($request->grupo) {$regras = array();
            if (!in_array('observacoes',$request->grupo)) {
                foreach ($request->grupo as $grupo) {
                    $alienacao_regras = $alienacao->regras_filtros()['regras'][$grupo];
                    $alienacao_titulos = $alienacao->regras_filtros()['titulos'][$grupo];
                    $alienacao_alcadas = $alienacao->regras_filtros()['alcadas'][$grupo];

                    foreach ($alienacao_regras as $key => $alerta) {
                        $regras[$grupo]['grupo'] = array('key'=>$grupo,'titulo'=>$alienacao_titulos['grupo']);
                        $regras[$grupo]['alertas'][] = array('key'=>$key,'titulo'=>$alienacao_titulos[$key],'alcada'=>$alienacao_alcadas[$key]);
                    }
                }
            }

            return response()->json($regras);
        }
    }

    public function nova_importar(alienacao_arquivo_xml $alienacao_arquivo_xml)
    {

        $token = str_random(30);
        $todas_arquivo_xml  = $alienacao_arquivo_xml->where("id_alienacao_arquivo_xml_tipo", "=", "1")->orderBy('id_alienacao_arquivo_xml', 'desc')->get();

        $class = $this;

        return view('servicos.alienacao-importacao.geral-alienacao-importar-historico', compact('class','token', 'todas_arquivo_xml'));
    }

    public function importar(Request $request, alienacao_arquivo_xml $alienacao_arquivo_xml)
    {
        set_time_limit(0);

        if (!$request->session()->has('datapasta_'.$request->token))
        {
            $request->session()->put('datapasta_'.$request->token,Carbon::now()->format('d-m-Y'));
        }

        $data_pasta                 = $request->session()->get('datapasta_'.$request->token);
        $destino                    = '/public/alienacoes/'.$data_pasta.'/'.$request->token;
        $arquivo                    = $request->no_arquivo;
        $nome_arquivo               = strtolower(remove_caracteres($arquivo->getClientOriginalName()));


        Storage::makeDirectory($destino);

        if ($arquivo->move(storage_path('app'.$destino), $nome_arquivo)) {

            //Carregando o XML do UPLOAD
            $DOM = new DOMDocument();
            $DOM->load(storage_path('app' . $destino . '/' . $nome_arquivo));

            try{
                $retorno = $DOM->schemaValidate(storage_path('app/public/alienacoes/validacao_alienacao.xsd'));

                if ($retorno == true)
                {
                    $objCeriFuncoes = new CeriFuncoes();

                    if ($id_alienacao_arquivo_xml = $objCeriFuncoes->importarAlienacaoXML($destino, $nome_arquivo, $this::ID_PRODUTO, $this->id_pessoa))
                    {
                        //unset('datapasta_'.$request->token);

                        $arquivo = array('no_arquivo' => $nome_arquivo,
                            'no_arquivos_originais' => $arquivo,
                            'no_local_arquivo' => $destino,
                            'in_ass_digital' => '',
                            'id_tipo_arquivo_grupo_produto' => '',
                            'in_assinado' => 'N',
                            'dt_assinado' => '',
                            'id_usuario_certificado' => '',
                            'no_arquivo_p7s' => '',
                            'id_flex' => '');

                        $duplicados = $alienacao_arquivo_xml->from('intimacao_arquivo_xml as iax')
                            ->where('iax.id_alienacao_arquivo_xml','=',$id_alienacao_arquivo_xml)
                            ->whereIn('iax.nu_contrato', function($query){
                                $query->from('intimacao_arquivo_xml as iax2')
                                    ->select('iax2.nu_contrato')
                                    ->join('alienacao as a','a.id_intimacao_arquivo_xml','=','iax2.id_intimacao_arquivo_xml')
                                    ->join('alienacao_pedido as ap','ap.id_alienacao','=','a.id_alienacao')
                                    ->join('pedido as p','p.id_pedido','=','ap.id_pedido')
                                    ->join('alienacao_andamento_situacao as aas','aas.id_alienacao_pedido','=','ap.id_alienacao_pedido')
                                    ->where('iax2.id_alienacao_arquivo_xml','<>',DB::raw('iax.id_alienacao_arquivo_xml'))
                                    ->whereNotIn('p.id_situacao_pedido_grupo_produto',[7, 67])
                                    ->where('aas.in_registro_ativo','=','S')
                                    ->whereNotIn('aas.id_acao_etapa', [31, 48]); //whereNotIn: verifica varios indices do array
                            });

                        $total_duplicados = $duplicados->count();
                        $duplicados = $duplicados->get();
                        $registro[] = '<b>Processo realizado com sucesso!</b>';

                        if ($total_duplicados>0) {
                            $registro[] = '<br/>Os contratos relacionados abaixo já se encontram em andamento na central.<br/>';
                            foreach ($duplicados as $duplicado){
                                $registro[] = 'N° do contrato: <b>'.$duplicado->nu_contrato.'</b>';
                            }
                            $registro[] = '<br/><b>Total: '.$total_duplicados.'</br>';
                        }

                        return response()->json(array('status' => 'sucesso',
                            'msg' => implode('<br />', $registro),
                            'recarrega' => 'true',
                            'count' => $total_duplicados,
                            'arquivo' => $arquivo,
                            'token' => $request->token));

                    } else {
                        return response()->json(array('status' => 'erro',
                            'recarrega' => 'true',
                            'msg' => 'Por favor, tente novamente mais tarde. Erro nº importacao'));
                    }
                }

            } catch (\Exception $e) {

                Storage::delete($destino . '/' . $nome_arquivo);
                Storage::deleteDirectory($destino);

                return response()->json(array('status' => 'erro',
                    'msg' => "XML inválido: " . $e->getMessage(),
                    'recarrega' => 'true',
                    'arquivo' => $arquivo
                ));
            }
        }
    }

    public function nova_assinatura_xml(Request $request, alienacao_arquivo_xml $alienacao_arquivo_xml) {
        $alienacao_arquivo_xml = $alienacao_arquivo_xml->find($request->id_alienacao_arquivo_xml);

        $request->token = str_random(30);

        $arquivo = array('no_arquivo'=>$alienacao_arquivo_xml->no_arquivo,
            'no_arquivos_originais'=>array(),
            'no_local_arquivo'=>$alienacao_arquivo_xml->no_diretorio_arquivo,
            'in_ass_digital'=>'S',
            'id_tipo_arquivo_grupo_produto'=>0,
            'in_assinado'=>'N',
            'dt_assinado'=>'',
            'id_usuario_certificado'=>0,
            'no_arquivo_p7s'=>'',
            'id_alienacao_arquivo_xml'=>$alienacao_arquivo_xml->id_alienacao_arquivo_xml);

        $arquivos[0] = $arquivo;

        $request->session()->put('arquivos_'.$request->token,$arquivos);

        return view('servicos.alienacao-importacao.geral-alienacao-importar-nova-assinatura',compact('request'));
    }

    public function iniciar_assinatura_xml(Request $request) {
        if ($request->session()->has('arquivos_'.$request->arquivos_token)) {
            $arquivos = $request->session()->get('arquivos_'.$request->arquivos_token);
            $arquivo = $arquivos[$request->id];
            $RestPki = new \Lacuna\RestPkiClient(env('RESTPKI_URL'), env('RESTPKI_TOKEN'));
            $signatureStarter = new \Lacuna\FullXmlSignatureStarter($RestPki);
            $signatureStarter->setXmlToSignPath(storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
            $signatureStarter->setSignaturePolicy(\Lacuna\StandardSignaturePolicies::XML_XADES_BES);
            if (env('APP_ENV')=='production') {
                $signatureStarter->setSecurityContext(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
            } else {
                $signatureStarter->setSecurityContext('803517ad-3bbc-4169-b085-60053a8f6dbf');
            }
            $token = $signatureStarter->startWithWebPki();
            return json_encode(array('token'=>$token,'id'=>$request->id,'no_arquivo'=>$arquivo['no_arquivo'],'arquivos_token'=>$request->arquivos_token));
        }
    }

    public function finalizar_assinatura_xml(Request $request, alienacao_arquivo_xml $alienacao_arquivo_xml) {
        if (!empty($request->token)) {
            if ($request->session()->has('arquivos_'.$request->arquivos_token)) {
                $arquivos = $request->session()->get('arquivos_'.$request->arquivos_token);
                $arquivo = $arquivos[$request->id];

                $alienacao_arquivo_xml = $alienacao_arquivo_xml->find($arquivo['id_alienacao_arquivo_xml']);

                $ext_arquivo = extensao_arquivo($arquivo['no_arquivo']);

                $RestPki = new \Lacuna\RestPkiClient(env('RESTPKI_URL'), env('RESTPKI_TOKEN'));

                $signatureFinisher = new \Lacuna\XmlSignatureFinisher($RestPki);
                $signatureFinisher->setToken($request->token);
                $signedFile = $signatureFinisher->finish();
                $signerCert = $signatureFinisher->getCertificateInfo();

                $nome_arquivo_ass = str_replace('.'.$ext_arquivo,'.assinado.'.$ext_arquivo,$arquivo['no_arquivo']);
                array_push($arquivos[$request->id]['no_arquivos_originais'],$arquivos[$request->id]['no_arquivo']);
                $arquivos[$request->id]['no_arquivo'] = $nome_arquivo_ass;

                file_put_contents(storage_path('app'.$arquivo['no_local_arquivo'].'/'.$nome_arquivo_ass), $signedFile);

                $usuario_certificado = new usuario_certificado();
                $usuario_certificado = $usuario_certificado->where('nu_serial',$signerCert->serialNumber)
                    ->where('id_usuario',Auth::User()->id_usuario)
                    ->first();

                if ($usuario_certificado) {
                    $id_usuario_certificado = $usuario_certificado->id_usuario_certificado;
                } else {
                    $novo_usuario_certificado = new usuario_certificado();
                    $novo_usuario_certificado->id_usuario = Auth::User()->id_usuario;
                    $novo_usuario_certificado->no_comum = $signerCert->subjectName->commonName;
                    $novo_usuario_certificado->no_email = $signerCert->emailAddress;
                    $novo_usuario_certificado->no_autoridade_raiz = $signerCert->issuerName->organization;
                    $novo_usuario_certificado->no_autoridade_unidade = $signerCert->issuerName->organizationUnit;
                    $novo_usuario_certificado->no_autoridade_certificadora = $signerCert->issuerName->commonName;
                    $novo_usuario_certificado->nu_serial = $signerCert->serialNumber;
                    $novo_usuario_certificado->dt_validade_ini = Carbon::parse($signerCert->validityStart);
                    $novo_usuario_certificado->dt_validade_fim = Carbon::parse($signerCert->validityEnd);
                    $novo_usuario_certificado->tp_certificado = $signerCert->pkiBrazil->certificateType;
                    if (!empty($signerCert->pkiBrazil->cpf)) {
                        $novo_usuario_certificado->nu_cpf_cnpj = $signerCert->pkiBrazil->cpf;
                    } elseif (!empty($signerCert->pkiBrazil->cnpj)) {
                        $novo_usuario_certificado->nu_cpf_cnpj = $signerCert->pkiBrazil->cnpj;
                    }
                    $novo_usuario_certificado->no_responsavel = $signerCert->pkiBrazil->responsavel;
                    $novo_usuario_certificado->dt_nascimento = Carbon::parse($signerCert->pkiBrazil->dateOfBirth);
                    $novo_usuario_certificado->nu_rg = $signerCert->pkiBrazil->rgNumero;
                    $novo_usuario_certificado->nu_rgemissor = $signerCert->pkiBrazil->rgEmissor;
                    $novo_usuario_certificado->nu_rguf = $signerCert->pkiBrazil->rgEmissorUF;
                    $novo_usuario_certificado->de_campos = json_encode($signerCert);

                    $novo_usuario_certificado->save();

                    $id_usuario_certificado = $novo_usuario_certificado->id_usuario_certificado;
                }

                $alienacao_arquivo_xml->id_usuario_certificado = $id_usuario_certificado;
                $alienacao_arquivo_xml->in_assinatura_digital = 'S';
                $alienacao_arquivo_xml->save();

                return json_encode(array('id'=>$request->id,'no_arquivo_ass'=>$nome_arquivo_ass,'no_arquivo'=>$arquivo['no_arquivo']));
            }
        }
    }

    public function visualizar_assinatura_xml(Request $request, alienacao_arquivo_xml $alienacao_arquivo_xml) {
        if ($request->id_alienacao_arquivo_xml>0) {
            $alienacao_arquivo_xml = $alienacao_arquivo_xml->find($request->id_alienacao_arquivo_xml);

            return view('servicos.alienacao-importacao.geral-alienacao-importar-visualizar-assinatura',compact('alienacao_arquivo_xml'));
        }
    }

    public function baixa_importar(alienacao_arquivo_xml $alienacao_arquivo_xml)
    {

        $token = str_random(30);
        $todas_arquivo_xml  = $alienacao_arquivo_xml->where("id_alienacao_arquivo_xml_tipo", "=", "3")->orderBy('id_alienacao_arquivo_xml', 'desc')->get();

        $class = $this;

        return view('servicos.alienacao-importacao.geral-alienacao-baixa-historico', compact('class','token', 'todas_arquivo_xml'));
    }

    public function visualizar_baixa_importacao(Request $request, baixa_arquivo_xml $baixa_arquivo_xml)
    {
        $class                     = $this;
        $todas_baixa_arquivo_xml  = $baixa_arquivo_xml->where("id_alienacao_arquivo_xml", $request->id_alienacao_arquivo_xml)->get();

        return view('servicos.alienacao-importacao.geral-alienacao-visualizar-historico', compact( 'todas_baixa_arquivo_xml', 'class'));
    }

    public function importar_baixa(Request $request)
    {
        set_time_limit(0);

        if (!$request->session()->has('datapasta_'.$request->token))
        {
            $request->session()->put('datapasta_'.$request->token,Carbon::now()->format('d-m-Y'));
        }

        $data_pasta                 = $request->session()->get('datapasta_'.$request->token);
        $destino                    = '/public/alienacoes/'.$data_pasta.'/'.$request->token;
        $arquivo                    = $request->no_arquivo;
        $nome_arquivo               = 'baixa_'.strtolower(remove_caracteres($arquivo->getClientOriginalName()));


        Storage::makeDirectory($destino);

        if ($arquivo->move(storage_path('app'.$destino), $nome_arquivo)) {

            //Carregando o XML do UPLOAD
            $DOM = new DOMDocument();
            $DOM->load(storage_path('app' . $destino . '/' . $nome_arquivo));

            try{
                $retorno = $DOM->schemaValidate(storage_path('app/public/alienacoes/validacao_alienacao_baixa.xsd'));

                if ($retorno == true)
                {

                    $objCeriFuncoes = new CeriFuncoes();

                    if ($objCeriFuncoes->importarBaixaAlienacaoXML($this, $destino, $nome_arquivo, $this::ID_PRODUTO) == true)
                    {
                        $arquivo = array('no_arquivo' => $nome_arquivo,
                            'no_arquivos_originais' => $arquivo,
                            'no_local_arquivo' => $destino,
                            'in_ass_digital' => '',
                            'id_tipo_arquivo_grupo_produto' => '',
                            'in_assinado' => 'N',
                            'dt_assinado' => '',
                            'id_usuario_certificado' => '',
                            'no_arquivo_p7s' => '',
                            'id_flex' => '');

                        return response()->json(array('status' => 'sucesso',
                            'msg' => 'Processo realizado com sucesso',
                            'recarrega' => 'true',
                            'arquivo' => $arquivo,
                            'token' => $request->token));

                    } else {
                        return response()->json(array('status' => 'erro',
                            'recarrega' => 'true',
                            'msg' => 'Por favor, tente novamente mais tarde. Erro nº importacao'));
                    }
                }

            } catch (\Exception $e) {

                Storage::delete($destino . '/' . $nome_arquivo);
                Storage::deleteDirectory($destino);

                return response()->json(array('status' => 'erro',
                    'msg' => "XML inválido: " . $e->getMessage(),
                    'recarrega' => 'true',
                    'arquivo' => $arquivo
                ));
            }
        }
    }

    public function remover_arquivos_sessao(Request $request){
        if($request->session()->forget('arquivos_' . $request->observacao_token) == null){
            return response()->json(array('status' => 'sucesso',
                'recarrega' => 'false'
            ));
        } else {
            return response()->json(array('status' => 'erro',
                'recarrega' => 'true',
                'msg' => "Não foi possível remover o(s) arquvo(s)",
            ));
        }
    }

    public function remover_interacao_andamento(Request $request, andamento_alienacao $andamento_alienacao) {
        $andamento_alienacao = $andamento_alienacao->find($request->id_andamento_alienacao);

        if ($request->method() === "POST") {
            if ($andamento_alienacao) {
                $retorno = $andamento_alienacao->remover_andamento_alienacao('E', Auth::User()->id_usuario, $request->justificativa);

                return  response()->json(['status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => $retorno
                ]);
            } else {
                return  response()->json(['status' => 'alerta',
                    'recarrega' => 'true',
                    'msg' => 'Não foi possivel localizar o registro.'
                ]);
            }

        } else {
            return view('servicos.alienacao.geral-alienacao-excluir-interacao-andamento', compact('andamento_alienacao'));
        }
    }

    public function detalhes_andamento_excluido(Request $request, andamento_alienacao_excluido $andamento_alienacao_excluido) {
        if ($request->id_andamento_alienacao_excluido>0) {
            $class = $this;
            $token = str_random(30);
            $andamento = $andamento_alienacao_excluido->find($request->id_andamento_alienacao_excluido);
            $arquivos_acao = $andamento->arquivos_grupo()->where('in_acao','S')->get();
            $arquivos_resultado = $andamento->arquivos_grupo()->where('in_resultado','S')->get();

            return view('servicos.alienacao.geral-alienacao-detalhes-andamento-excluido',compact('class','andamento','arquivos_acao','arquivos_resultado', 'token'));
        }
    }

    public function devolver_valor_produto(Request $request, alienacao_valor $alienacao_valor, produto_item $produto_item, serventia $serventia, estado $estado) {
        $alienacoes_valores = [];
        $serventias = [];
        $cidades = [];
        $class = $this;

        switch ($this->id_tipo_pessoa) {
            case 2:
                $produto_item = $produto_item->find($this::ID_PRODUTO_ITEM_DILIGENCIA);

                if ($request->protocolo_pedido>0) {
                    $alienacoes_valores = $alienacao_valor->select('alienacao_valor.*', 'pedido.protocolo_pedido', 'produto_item.no_produto_item', 'alienacao_valor_repasse.dt_repasse_devolucao', 'alienacao_valor_repasse.va_repasse_devolucao', 'alienacao_valor_repasse.de_repasse_devolucao')
                                                            ->join('produto_item', 'produto_item.id_produto_item', '=', 'alienacao_valor.id_produto_item')
                                                            ->join('alienacao_valor_repasse', 'alienacao_valor_repasse.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                                            ->join('alienacao_pedido', 'alienacao_pedido.id_alienacao', '=', 'alienacao_valor.id_alienacao')
                                                            ->join('pedido', 'pedido.id_pedido', '=', 'alienacao_pedido.id_pedido')
                                                            ->join('pedido_pessoa', function ($join) {
                                                                $join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
                                                                    ->where('pedido_pessoa.id_pessoa', '=', $this->id_pessoa);
                                                            })
                                                            ->where('alienacao_valor_repasse.id_repasse', $this::ID_REPASSE_ANOREG_SERVENTIA)
                                                            ->where('alienacao_valor_repasse.in_repasse_devolucao', 'N')
                                                            ->where('alienacao_valor.id_produto_item', $this::ID_PRODUTO_ITEM_DILIGENCIA)
                                                            ->where('pedido.protocolo_pedido', $request->protocolo_pedido)
                                                            ->distinct()
                                                            ->get();
                }

                $alienacoes_valores_devolvidas = $alienacao_valor->select('alienacao_valor.id_alienacao', 'pedido.protocolo_pedido', 'produto_item.no_produto_item', 'alienacao_valor_repasse.dt_repasse_devolucao', 'alienacao_valor_repasse.va_repasse_devolucao', 'alienacao_valor_repasse.de_repasse_devolucao')
                                                                ->join('produto_item', 'produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                                                                ->join('alienacao_valor_repasse', 'alienacao_valor_repasse.id_alienacao_valor','=','alienacao_valor.id_alienacao_valor')
                                                                ->join('alienacao_pedido', 'alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                                                ->join('pedido', 'pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                                                ->join('pedido_pessoa',function($join) {
                                                                    $join->on('pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                                                        ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
                                                                })
                                                                ->where('alienacao_valor_repasse.id_repasse',$this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                ->where('alienacao_valor_repasse.in_repasse_devolucao','S')
                                                                ->where('alienacao_valor.id_produto_item',$this::ID_PRODUTO_ITEM_DILIGENCIA);
                break;
            case 9:
                $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

                if ($request->cidade) {
                    $serventias = $serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
                                            ->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
                                            ->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                                            ->where('endereco.id_cidade',$request->cidade)
                                            ->whereIn('serventia.id_tipo_serventia',array(1,3))
                                            ->where('serventia.in_registro_ativo','S')
                                            ->orderBy('serventia.no_serventia')
                                            ->get();
                }

                $alienacoes_valores_devolvidas = $alienacao_valor->select('alienacao_valor.id_alienacao', 'pedido.protocolo_pedido', 'produto_item.no_produto_item', 'alienacao_valor_repasse.dt_repasse_devolucao', 'alienacao_valor_repasse.va_repasse_devolucao', 'alienacao_valor_repasse.de_repasse_devolucao')
                                                                 ->join('produto_item', 'produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                                                                 ->join('alienacao_valor_repasse', 'alienacao_valor_repasse.id_alienacao_valor','=','alienacao_valor.id_alienacao_valor')
                                                                 ->join('alienacao_pedido', 'alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                                                 ->join('pedido', 'pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                                                 ->where('alienacao_valor_repasse.id_repasse',$this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                 ->where('alienacao_valor_repasse.in_repasse_devolucao','S')
                                                                 ->where('alienacao_valor.id_produto_item',$this::ID_PRODUTO_ITEM_DILIGENCIA);

                if ($request->serventia>0) {
                    $alienacoes_valores_devolvidas->join('pedido_pessoa','pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                                  ->join('serventia',function($join) use ($request) {
                                                      $join->on('serventia.id_pessoa','=','pedido_pessoa.id_pessoa')
                                                           ->where('serventia.id_serventia','=',$request->serventia);
                                                  });
                }
                break;
        }

        if ($request->dt_ini!='' and $request->dt_fim!='') {
            $dt_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ini.' 00:00:00');
            $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
            $alienacoes_valores_devolvidas->whereBetween('alienacao_valor_repasse.dt_repasse_devolucao',[$dt_ini,$dt_fim]);
        }
        if ($request->protocolo_pedido_devolvidos > 0){
            $alienacoes_valores_devolvidas->where('pedido.protocolo_pedido',$request->protocolo_pedido_devolvidos);
        }

        $total_alienacoes_valores_devolvidos = $alienacoes_valores_devolvidas->distinct('alienacao_valor.*')->count('alienacao_valor.*');

        switch ($request->total_paginacao) {
            case 10: case 50: case 100: case 500:
            $total_paginacao = $request->total_paginacao;
            break;
            case -1:
                $total_paginacao = $total_alienacoes_valores_devolvidos;
                break;
            default:
                $total_paginacao = 10;
                break;
        }

        $alienacoes_valores_devolvidas = $alienacoes_valores_devolvidas->distinct()->paginate($total_paginacao, ['alienacao_valor.*'], 'pag');

        return view('servicos.alienacao.geral-alienacao-devolver-valor', compact('class', 'request', 'alienacoes_valores', 'alienacoes_valores_devolvidas', 'produto_item', 'total_alienacoes_valores_devolvidos', 'serventias', 'cidades'));
    }

    public function devolver_valor_novo(Request $request, alienacao_valor $alienacao_valor, banco_pessoa $banco_pessoa) {
        $banco_pessoa = $banco_pessoa->select('banco.codigo_banco','banco.no_banco','banco_pessoa.nu_agencia','banco_pessoa.nu_dv_agencia','banco_pessoa.nu_conta','banco_pessoa.nu_dv_conta','banco_pessoa.no_pessoa_conta',DB::raw("case when banco_pessoa.tipo_conta = 'C' then 'CORRENTE' else '' end tipo_conta, banco_pessoa.nu_variacao, case when banco_pessoa.in_tipo_pessoa_conta = 'F' then 'Física' when banco_pessoa.in_tipo_pessoa_conta = 'J' then 'Jurídica' else '' end as in_tipo_pessoa_conta, banco_pessoa.nu_cpf_cnpj_conta"))
                                     ->join('banco','banco.id_banco','=','banco_pessoa.id_banco')
                                     ->where('banco_pessoa.id_pessoa', 62)
                                     ->first();

        $alienacao_valor = $alienacao_valor->select('alienacao_valor.dt_cadastro','produto_item.no_produto_item','alienacao_valor_repasse.dt_repasse','alienacao_valor.nu_quantidade','alienacao_valor.va_valor','alienacao_valor.va_total','alienacao_valor_repasse.id_alienacao_valor_repasse')
                                           ->join('produto_item','produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                                           ->join('alienacao_valor_repasse','alienacao_valor_repasse.id_alienacao_valor','=','alienacao_valor.id_alienacao_valor')
                                           ->where('alienacao_valor.id_alienacao',$request->id_alienacao)
                                           ->where('alienacao_valor.id_produto_item',$this::ID_PRODUTO_ITEM_DILIGENCIA)
                                           ->where('alienacao_valor_repasse.id_repasse',$this::ID_REPASSE_ANOREG_SERVENTIA)
                                           ->orderby('alienacao_valor_repasse.dt_repasse','desc')
                                           ->first();

        $alienacao_valor_token = str_random(30);

        return view('servicos.alienacao.serventia-devolver-valor-produto-comprovante', compact('alienacao_valor', 'banco_pessoa', 'alienacao_valor_token'));
    }

    public function devolver_valor_inserir(Request $request, alienacao_valor_repasse $alienacao_valor_repasse) {
        $erro = 0;
        DB::beginTransaction();

        $alterar_alienacao_valor_repasse = $alienacao_valor_repasse->find($request->id_alienacao_valor_repasse);
        $alterar_alienacao_valor_repasse->in_repasse_devolucao = 'S';
        $alterar_alienacao_valor_repasse->va_repasse_devolucao = converte_float($request->va_pago);
        $alterar_alienacao_valor_repasse->de_repasse_devolucao = $request->de_repasse_devolucao;
        $alterar_alienacao_valor_repasse->id_usuario_repasse_devolucao = Auth::User()->id_usuario;
        $alterar_alienacao_valor_repasse->dt_repasse_devolucao = formatar_data_sql($request->dt_pagamento);

        if ($alterar_alienacao_valor_repasse->save()) {
            // ***** CODIGO INSERIR ARQUIVO ******
            if ($request->session()->has('arquivos_'.$request->alienacao_valor_token)) {
                $destino = '/alienacoes/'.$alterar_alienacao_valor_repasse->aliencao_valor->id_alienacao;
                $arquivos = $request->session()->get('arquivos_'.$request->alienacao_valor_token);

                Storage::makeDirectory('/public'.$destino);

                $erro_loop_arq = false;
                $erro_loop_sql = false;
                foreach ($arquivos as $key => $arquivo) {
                    $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                    $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                    $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                    $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                    $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                    $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                    $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                    $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                    $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                    $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                    $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                    if ($arquivo['dt_assinado']!='') {
                        $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                    }
                    if ($arquivo['id_usuario_certificado']>0) {
                        $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                    }
                    if (!empty($arquivo['no_arquivo_p7s'])) {
                        $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                        $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                    }
                    if ($novo_arquivo_grupo_produto->save()) {
                        if (Storage::exists($origem_arquivo)) {
                            if (Storage::exists($destino_arquivo)) {
                                Storage::delete($destino_arquivo);
                            }
                            if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                if (count($arquivo['no_arquivos_originais'])>0) {
                                    foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                        $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                        $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                        $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                        $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                        $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                        $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                        $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                        $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                        $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                        $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                        if ($novo_arquivo_grupo_produto_composicao->save()) {
                                            if (Storage::exists($origem_arquivo_original)) {
                                                if (Storage::exists($destino_arquivo_original)) {
                                                    Storage::delete($destino_arquivo_original);
                                                }
                                                if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                    $erro_loop_arq = true;
                                                }
                                            } else {
                                                $erro_loop_arq = true;
                                            }
                                        } else {
                                            $erro_loop_sql = true;
                                        }
                                    }
                                }
                            } else {
                                $erro_loop_arq = true;
                            }
                        } else {
                            $erro_loop_arq = true;
                        }
                    } else {
                        $erro_loop_sql = true;
                    }
                    $alterar_alienacao_valor_repasse->aliencao_valor->alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
                }
                if ($erro_loop_arq) {
                    $erro = 1;
                }
                if ($erro_loop_sql) {
                    $erro = 1;
                }

                $request->session()->forget('arquivos_' . $request->alienacao_valor_token);
            }
            // ***** FIM CODIGO INSERIR ARQUIVO *****
        }

        if ($erro>0) {
            DB::rollback();
            return response()->json(array('status'=> 'erro',
                'recarrega' => 'false',
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
        } else {
            DB::commit();
            return response()->json(array('status'=> 'sucesso',
                'recarrega' => 'true',
                'msg' => 'O comprovante de devolução de valor do produto inserido com sucesso.'
            ));
        }
    }

    public function relatorio_devolver_valor_produto(Request $request, alienacao_valor $alienacao_valor) {
        $class = $this;
        $alienacoes_valores_devolvidas = $alienacao_valor->select('alienacao_valor.id_alienacao', 'pedido.protocolo_pedido', 'produto_item.no_produto_item', 'alienacao_valor_repasse.dt_repasse_devolucao', 'alienacao_valor_repasse.va_repasse_devolucao', 'alienacao_valor_repasse.de_repasse_devolucao')
                                                         ->join('produto_item', 'produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                                                         ->join('alienacao_valor_repasse', 'alienacao_valor_repasse.id_alienacao_valor','=','alienacao_valor.id_alienacao_valor')
                                                         ->join('alienacao_pedido', 'alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                                         ->join('pedido', 'pedido.id_pedido','=','alienacao_pedido.id_pedido');

        if ($this->id_tipo_pessoa == 2) {
            $alienacoes_valores_devolvidas->join('pedido_pessoa',function($join) {
                $join->on('pedido_pessoa.id_pedido','=','pedido.id_pedido')
                     ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
            });
        }

        $alienacoes_valores_devolvidas  = $alienacoes_valores_devolvidas->where('alienacao_valor_repasse.id_repasse',$this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                        ->where('alienacao_valor_repasse.in_repasse_devolucao','S')
                                                                        ->where('alienacao_valor.id_produto_item',$this::ID_PRODUTO_ITEM_DILIGENCIA)
                                                                        ->whereIn('alienacao_valor.id_alienacao', $request->ids_alienacoes)
                                                                        ->distinct()
                                                                        ->get();

        return view('servicos.alienacao.geral-alienacao-relatorio-devolver-valor-produto', compact('request', 'class','alienacoes_valores_devolvidas'));
    }

    public function salvar_relatorio_devolver_valor_produto(Request $request, alienacao_valor $alienacao_valor) {
        $alienacoes_valores_devolvidas = $alienacao_valor->select('alienacao_valor.id_alienacao', 'pedido.protocolo_pedido', 'produto_item.no_produto_item', 'alienacao_valor_repasse.dt_repasse_devolucao', 'alienacao_valor_repasse.va_repasse_devolucao', 'alienacao_valor_repasse.de_repasse_devolucao')
                                                         ->join('produto_item', 'produto_item.id_produto_item','=','alienacao_valor.id_produto_item')
                                                         ->join('alienacao_valor_repasse', 'alienacao_valor_repasse.id_alienacao_valor','=','alienacao_valor.id_alienacao_valor')
                                                         ->join('alienacao_pedido', 'alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                                         ->join('pedido', 'pedido.id_pedido','=','alienacao_pedido.id_pedido');

        if ($this->id_tipo_pessoa == 2) {
            $alienacoes_valores_devolvidas->join('pedido_pessoa',function($join) {
                $join->on('pedido_pessoa.id_pedido','=','pedido.id_pedido')
                    ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
            });
        }

        $alienacoes_valores_devolvidas  = $alienacoes_valores_devolvidas->where('alienacao_valor_repasse.id_repasse',$this::ID_REPASSE_ANOREG_SERVENTIA)
                                                                        ->where('alienacao_valor_repasse.in_repasse_devolucao','S')
                                                                        ->where('alienacao_valor.id_produto_item',$this::ID_PRODUTO_ITEM_DILIGENCIA)
                                                                        ->whereIn('alienacao_valor.id_alienacao', $request->ids_alienacoes)
                                                                        ->distinct()
                                                                        ->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');
        $class = $this;

        Excel::create('notificacoes'.$time, function($excel) use ($alienacoes_valores_devolvidas, $class) {
            $excel->sheet('Notificações', function($sheet) use ($alienacoes_valores_devolvidas, $class) {
                $sheet->loadView('xls.relatorio-devolver-valor-produto', array('alienacoes_valores' => $alienacoes_valores_devolvidas, 'class' => $class));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'C' =>\PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'D' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }
}
