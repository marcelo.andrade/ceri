<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;

use App\serventia;
use App\estado;

class ServentiaController extends Controller {
	
	public function __construct(serventia $serventia) {
		$this->serventia=$serventia;
	}

	public function listarServentias(Request $request) {
		if ($request->id_cidade>0) {
			$serventias = $this->serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
										  ->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
										  ->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
										  ->where('endereco.id_cidade',$request->id_cidade)
										  ->whereIn('serventia.id_tipo_serventia',array(1,3))
										  ->where('serventia.in_registro_ativo','S')
										  ->orderBy('serventia.no_serventia')
										  ->get();
		} else {
			$estado = new estado();
			$serventias = $estado->find(env('ID_ESTADO'))->serventias_estado()->orderBy('serventia.no_serventia')->get();
		}
		return response()->json($serventias);
	}
}
