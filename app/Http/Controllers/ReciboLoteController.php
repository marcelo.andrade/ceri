<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\CeriFuncoes;
use App\produto_item;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;
use XMLReader;
use DOMDocument;
use Excel;

use App\alienacao;
use App\estado;
use App\cidade;
use App\produto;
use App\serventia;
use App\pedido;

class ReciboLoteController extends Controller
{

    const ID_GRUPO_PRODUTO = 7;
    const ID_PRODUTO = 19;
    const ID_SITUACAO_CADASTRADO = 54;
    const ID_SITUACAO_EMPROCESSAMENTO = 55;
    const ID_SITUACAO_AGUARDANDOINTERACAO = 56;
    const ID_SITUACAO_FINALIZADO = 57;
    const ID_SITUACAO_ORCAMENTOGERADO = 63;
    const ID_SITUACAO_DEVOLVIDO = 64;
    const ID_SITUACAO_AGUARDANDOPAGAMENTO = 65;
    const ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ = 66;
    const ID_SITUACAO_CANCELADO = 67;
    const ID_SITUACAO_AGUARDANDO_APROCAO_ORCAMENTO = 68;
    const ID_SITUACAO_AGUARDANDO_AUTORIZACAO_DECURSO_PRAZO = 69;

    public function __construct(alienacao $alienacao)
    {
        ini_set('max_input_vars', 3000);//aumentando o numero de post no sistema
        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');

            $this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
            $this->id_pessoa = $pessoa_ativa->id_pessoa;
            $this->modulos = $pessoa_ativa->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
        } else {
            $this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
            $this->id_pessoa = Auth::User()->id_pessoa;
            $this->modulos = Auth::User()->pessoa->pessoa_modulo()->lists('id_modulo')->toArray();
        }
    }

    /*
     * CASO TENHA CRIADO UMA NOVO ID_situacao_pedido_grupo_produto
     * COLOCAR O CASE NOS ARQUIVOS
     * geral-alienacao-novo-andamento.blade.php e serventia-alienacao-novo-andamento.blade.php
     */
    public function index(Request $request, alienacao $alienacao, estado $estado, serventia $serventia){
        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
        $class = $this;

        if ($request->cidade) {
            $serventias = $serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
                ->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
                ->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                ->where('endereco.id_cidade',$request->cidade)
                ->whereIn('serventia.id_tipo_serventia',array(1,3))
                ->where('serventia.in_registro_ativo','S')
                ->orderBy('serventia.no_serventia')
                ->get();
        } else {
            $serventias = array();
        }

        $alienacao_regras = $alienacao->regras_filtros()['regras'];
        $alienacao_titulos = $alienacao->regras_filtros()['titulos'];
        $alienacao_alcadas = $alienacao->regras_filtros()['alcadas'];

        $modulos = $this->modulos;

        switch ($request->ordby)
        {
            case 'ultima-mov':
                $orderBy = 'alienacao_andamento_atual.dt_cadastro';
                break;
            case 'data': default:
            $orderBy = 'alienacao.dt_cadastro';
            break;
        }

        switch ($this->id_tipo_pessoa)
        {
            case 2:
            case 10:
                $todas_alienacoes = $alienacao->select('alienacao.*','alienacao_pedido.*','pedido.*','alienacao_andamento_atual.dt_cadastro')
                    ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                    ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                    ->join('pedido_pessoa',function($join) {
                        $join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
                            ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
                    })
                    ->join('alienacao_andamento_atual',function($join) {
                        $join->on('alienacao_andamento_atual.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                            ->where('alienacao_andamento_atual.in_registro_ativo','=','S');
                    })
                    ->where('pedido.id_situacao_pedido_grupo_produto','<>',$this::ID_SITUACAO_CADASTRADO);

                if ($request->dt_ini!='' and $request->dt_fim!='') {
                    $dt_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ini.' 00:00:00');
                    $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                    $todas_alienacoes->whereBetween('pedido.dt_pedido',array($dt_ini,$dt_fim));
                }
                if ($request->protocolo>0){
                    $todas_alienacoes->where('pedido.protocolo_pedido',$request->protocolo);
                }
                if($request->contrato>0){
                    $todas_alienacoes->where('alienacao.numero_contrato',$request->contrato);
                }
                if($request->dt_ult_mov!=''){
                    $dt_ult_mov_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ult_mov . ' 00:00:00');
                    $dt_ult_mov_fim = Carbon::createFromFormat('d/m/Y H:i:s', $request->dt_ult_mov . ' 23:59:59');
                    $todas_alienacoes->whereBetween('alienacao_andamento_atual.dt_cadastro',array($dt_ult_mov_ini,$dt_ult_mov_fim));
                }
                if ($request->protocolo_interno!="") {
                    $todas_alienacoes->join('andamento_alienacao','andamento_alienacao.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                        ->join('acao_etapa','acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
                        ->whereIn('acao_etapa.co_acao', array(5,32))
                        ->where('acao_etapa.in_registro_ativo', 'S')
                        ->where('andamento_alienacao.de_texto_curto_acao','=',$request->protocolo_interno);
                }

                if (!empty($request->grupo) or !empty($request->filtro))
                {
                    if (!empty($request->grupo) and !is_array($request->grupo)) {
                        $replace['grupo'] = array($request->grupo);
                    }
                    if (!empty($request->filtro) and !is_array($request->filtro)) {
                        $replace['filtro'] = array($request->filtro);
                    }
                    if (isset($replace)) {
                        $request->replace($replace);
                    }
                    if (in_array('observacoes',$request->grupo)) {
                        $todas_alienacoes->join('alienacao_observacao', 'alienacao_observacao.id_alienacao','=','alienacao.id_alienacao')
                            ->where('alienacao_observacao.in_leitura','N')
                            ->where('alienacao_observacao.id_pessoa_dest',$this->id_pessoa);
                    } else {
                        if (count($request->grupo)>0 and count($request->filtro)<=0) {
                            foreach ($request->grupo as $key => $grupo) {
                                foreach ($alienacao_regras[$grupo] as $key => $regra) {
                                    $regras_array[] = $regra;
                                }
                            }
                        } elseif (count($request->grupo)>0 and count($request->filtro)>0) {
                            foreach ($request->filtro as $key => $filtro) {
                                $filtro_array = explode('_',$filtro);
                                $regras_array[] = $alienacao_regras[$filtro_array[0]][$filtro_array[1]];
                            }
                        }
                        $todas_alienacoes = $alienacao->aplicar_regras($todas_alienacoes,$regras_array);
                    }
                }

                $todas_alienacoes_total = $todas_alienacoes->distinct('alienacao.*')->count('alienacao.*');

                switch($request->total_paginacao) {
                    case 10: case 50: case 100: case 500:
                    $total_paginacao = $request->total_paginacao;
                    break;
                    case -1:
                        $total_paginacao = $todas_alienacoes_total;
                        break;
                    default:
                        $total_paginacao = 10;
                        break;
                }

                $todas_alienacoes = $todas_alienacoes->distinct()->orderBy($orderBy,($request->ord?$request->ord:'desc'))->paginate($total_paginacao, ['alienacao.*','pedido.*'], 'pag');

                $todas_alienacoes->appends(Request::capture()->except('_token'))->render();

                return view('servicos.recibo.serventia-alienacao-recibo-lote',compact('class','this','request','alienacao_regras','alienacao_titulos','alienacao_alcadas','todas_alienacoes','todas_alienacoes_total'));
                break;
        }
    }






    public function gerar_recibo_lote(Request $request, alienacao $alienacao)
    {
        set_time_limit(0);
        if ($request->ids_alienacoes>0)
        {
            $class = $this;
            $arrayAlienacoes= $request->ids_alienacoes;
            $alienacoes = $alienacao->whereIn('id_alienacao', $arrayAlienacoes)->get();

            $destino = '/alienacoes/'.$alienacoes[0]->id_alienacao;
            $no_arquivo = 'alienacao_'.$alienacoes[0]->alienacao_pedido->pedido->protocolo_pedido.'.pdf';

            Storage::makeDirectory('/public'.$destino);

            $titulo = 'Detalhes da Alienação: '.$alienacoes[0]->alienacao_pedido->pedido->protocolo_pedido;
            $pdf = PDF::loadView('pdf.alienacao-recibo-lote', compact('alienacoes', 'titulo'));

            $pdf->save(storage_path('app/public'.$destino.'/'.$no_arquivo));

            $alienacao = $alienacoes[0];

            return response()->json(array('view'=>view('servicos.alienacao.geral-alienacao-recibo',compact('alienacao'))->render()));
        }
    }

    public function imprimir_recibo_lote(Request $request, alienacao $alienacao) {
        if ($request->id_alienacao>0) {
            $alienacao = $alienacao->find($request->id_alienacao);
            $nome_arquivo = 'alienacao_'.$alienacao->alienacao_pedido->pedido->protocolo_pedido.'.pdf';

            $arquivo = Storage::get('/public/alienacoes/'.$alienacao->id_alienacao.'/'.$nome_arquivo);

            return response($arquivo, 200)->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$nome_arquivo.'"');
        }
    }

}
