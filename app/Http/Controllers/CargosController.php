<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\usuario;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;
use Mail;

use App\cargo;
use App\tipo_usuario;

class CargosController extends Controller {

	public function __construct() {
		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, cargo $cargo){

		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_modulo[0]->id_modulo.", 'Acesso a tela principal de cadastro de cargo','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_modulo[0]->id_modulo.");"));

		$cargos = $cargo->where('in_registro_ativo','S')
						->orderBy('nu_ordem','desc')->get();

		$class = $this;
	
	   	return view('cadastros.cargos',compact('class','request','cargos'));
    }

    public function novo(Request $request, tipo_usuario $tipo_usuario) {
    	$tipos_usuario = $tipo_usuario->where('in_registro_ativo','S')
									  ->orderBy('nu_ordem','asc')
									  ->get();
		
		return view('cadastros.cargos-novo',compact('request','tipos_usuario'));
    }

	public function inserir(Request $request) {
		$novo_cargo = new cargo();
		
		$novo_cargo->id_tipo_usuario = $request->id_tipo_usuario;
		$novo_cargo->codigo_cargo = $request->codigo_cargo;
		$novo_cargo->no_cargo = $request->no_cargo;
		$novo_cargo->abv_cargo = $request->abv_cargo;
		$novo_cargo->id_usuario_cad = Auth::User()->id_usuario;
				
		if ($novo_cargo->save()) {
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O cargo foi inserido com sucesso.'));
		} else {
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		}
	}
	
	public function detalhes(Request $request, cargo $cargo) {
		if ($request->id_cargo>0) {
			$cargo = $cargo->find($request->id_cargo);
			return view('cadastros.cargos-detalhes',compact('cargo'));
		}
	}
}
