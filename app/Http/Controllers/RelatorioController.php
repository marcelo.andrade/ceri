<?php
namespace App\Http\Controllers;

use App\acao_etapa;
use App\alcada;
use App\cidade;
use App\estado;
use App\etapa_fase;
use App\fase_grupo_produto;
use App\modulo;
use App\movimentacao_financeira;
use App\relatorio;
use App\serventia;
use App\compra_credito;
use App\situacao_pedido_grupo_produto;
use App\alienacao;
use App\alienacao_observacao;
use Illuminate\Support\Facades\Session;
use PDF;
use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;
use Carbon\Carbon;
use Excel;
use DB;

class RelatorioController extends Controller
{
    const ID_GRUPO_PRODUTO = 7;
    const ID_ALCADA_SERVENTIA = 2;
    const ID_ALCADA_BANCO = 8;

    public function __construct(alienacao $alienacao) {
        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');

            $this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
            $this->id_pessoa = $pessoa_ativa->id_pessoa;
        } else {
            $this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
            $this->id_pessoa = Auth::User()->id_pessoa;
        }
    }

    public function movimentacao_financeira(Request $request, relatorio $relatorio)
    {
        $titulo_relatorio = 'Relatório de Movimentação Financeira';
        if ($request->isMethod('post')) {
            $movimentacao_financeira = $relatorio->movimentacao_financeira(Auth::User()->id_usuario, formatar_data_sql($request->dt_inicio), formatar_data_sql($request->dt_fim));

            switch ($request->tp_saida) {
                case "PDF":
                    $pdf = PDF::loadView('relatorios.movimentacao-financeira', compact('titulo_relatorio', 'request', 'movimentacao_financeira'))->setPaper('a4', 'landscape');
                    return $pdf->stream();
                    break;
                case "HTML":
                    return view('relatorios.movimentacao-financeira', compact('titulo_relatorio', 'request', 'movimentacao_financeira'));
                    break;
            }
        } else {
            return view('relatorios.movimentacao-financeira-filtro', compact('titulo_relatorio', 'request'));
        }
    }


    public function movimentacao_pedido(Request $request, relatorio $relatorio, modulo $modulo)
    {
        $modulos = $modulo->orderBy('modulo.no_modulo')->where('in_registro_ativo', 'S')
            ->whereNotIn('codigo_modulo',[0,1])
            ->orderBy('no_modulo', 'asc')
            ->get();
        $titulo_relatorio = 'Relatório de Movimentação Pedido';
        if ($request->isMethod('post')) {

            $id_modulo = ($request->id_modulo == -1) ? '' : $request->id_modulo;

            $movimentacao_pedido = $relatorio->movimentacao_pedido($id_modulo, Auth::User()->usuario_serventia->id_serventia, Auth::User()->id_usuario, formatar_data_sql($request->dt_inicio), formatar_data_sql($request->dt_fim));

            switch ($request->tp_saida) {
                case "PDF":
                    $pdf = PDF::loadView('relatorios.movimentacao-pedido', compact('titulo_relatorio', 'request', 'movimentacao_pedido'))->setPaper('a4', 'landscape');
                    return $pdf->stream();
                    break;
                case "HTML":
                    return view('relatorios.movimentacao-pedido', compact('titulo_relatorio', 'request', 'movimentacao_pedido'));
                    break;
            }
        } else {
            return view('relatorios.movimentacao-pedido-filtro', compact('titulo_relatorio', 'request', 'modulos'));
        }
    }

    public function tabela_preco(Request $request, relatorio $relatorio)
    {
        $titulo_relatorio = 'Tabela de preço';
        $tabela_preco = $relatorio->tabela_preco();
        return view('relatorios.tabela-preco', compact('titulo_relatorio', 'request', 'tabela_preco'));
    }

    public function controle_acesso(Request $request, relatorio $relatorio)
    {
        $titulo_relatorio = 'Controle de Acesso';

        $id_serventia = '';
        $op_relatorio = $request->serventias;
        $caracter_delimitador = ';';

        if ($request->isMethod('post')) {
            $tabela_controle_acesso = $relatorio->controle_acesso($id_serventia, $op_relatorio, $request->dt_inicio, $request->dt_fim, $caracter_delimitador);

            return view('relatorios.controle-acesso', compact('titulo_relatorio', 'request', 'tabela_controle_acesso'));

            /*switch ($request->tp_saida) {
                case "PDF":
                    $pdf = PDF::loadView('relatorios.controle-acesso', compact('titulo_relatorio', 'request', 'tabela_controle_acesso'))->setPaper('a4', 'landscape');
                    return $pdf->stream();
                    break;
                case "HTML":
                    return view('relatorios.controle-acesso', compact('titulo_relatorio', 'request', 'tabela_controle_acesso'));
                    break;
            }*/

        } else {
            return view('relatorios.controle-acesso-filtro', compact('titulo_relatorio', 'request'));
        }
    }

    public function acompanha_pagamento_caixa(Request $request, relatorio $relatorio)
    {
        $titulo_relatorio = 'Acompanha Pagamento (Caixa)';
        $id_modulo = 8;
        $id_serventia = '';
        $id_usuaio = '';
        $id_produto_item = '';
        $op_relatorio = 'PG_EXCEL';
        $campos_ordem = '';
        $caracter_delimitador = ';';
        $model_relatorio = $relatorio;

        if ($request->isMethod('post')) {
            $tabela_acomp_pagamento_caixa = $relatorio->acomp_pagamento_caixa($id_modulo, $id_serventia, $id_usuaio, $id_produto_item, $op_relatorio, $campos_ordem, formatar_data_sql($request->dt_inicio), formatar_data_sql($request->dt_fim), $caracter_delimitador);
            return view('relatorios.movimentacao-financeira-anoreg', compact('titulo_relatorio', 'request', 'tabela_acomp_pagamento_caixa', 'model_relatorio'));
        } else {
            return view('relatorios.movimentacao-financeira-anoreg-filtro', compact('titulo_relatorio', 'request', 'modulos'));
        }
    }

    public function salvar_relatorio_excel(Request $request, relatorio $relatorio)
    {
        $id_modulo = 8;
        $id_serventia = '';
        $id_usuaio = '';
        $id_produto_item = '';
        $op_relatorio = 'PG_EXCEL';
        $campos_ordem = '';
        $caracter_delimitador = ';';

        $model_relatorio = $relatorio;

        $tabela_acomp_pagamento_caixa = $relatorio->acomp_pagamento_caixa($id_modulo, $id_serventia, $id_usuaio, $id_produto_item, $op_relatorio, $campos_ordem, formatar_data_sql($request->dt_inicio), formatar_data_sql($request->dt_fim), $caracter_delimitador);

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('acompanha_pagamento_caixa_' . $time, function ($excel) use ($tabela_acomp_pagamento_caixa, $model_relatorio) {

            $excel->sheet('Acompanha Pagamento Caixa', function ($sheet) use ($tabela_acomp_pagamento_caixa, $model_relatorio) {

                $sheet->loadView('xls.movimentacao-financeira-anoreg', ['tabela_acomp_pagamento_caixa' => $tabela_acomp_pagamento_caixa, 'model_relatorio' => $model_relatorio]);
                $sheet->setColumnFormat(['C' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'I' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1]);

                $sheet->getStyle('A4:M4')->applyFromArray(
                    ['fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => ['rgb' => 'E0EEEE']]]
                );
                $sheet->getStyle('A'.(count($tabela_acomp_pagamento_caixa)+5).':M'.(count($tabela_acomp_pagamento_caixa)+5))->applyFromArray(
                    ['fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => ['rgb' => 'E0EEEE']]]
                );
                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }

    public function mov_fin_pgto_serv_serventia(Request $request, relatorio $relatorio, estado $estado)
    {
        $titulo_relatorio = 'Relatório de Movimentação Financeira (Pagamento de Serviço)';
        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
        $pessoa_ativa = Session::get('pessoa_ativa');
        $id_serventia = $request->id_serventia ?: '';

        if ($request->isMethod('post')) {

            $movimentacao_financeira = $relatorio->movimentacao_financeira_serventia(Auth::User()->id_usuario, $pessoa_ativa->pessoa->id_pessoa, formatar_data_sql($request->dt_inicio), formatar_data_sql($request->dt_fim), $request->id_produto, $id_serventia, ';', 'CX_ANOREG_SERV_VL_A_PAGAR_00');

            switch ($request->tp_saida) {
                case "PDF":
                    $pdf = PDF::loadView('relatorios.pagamento-servico-serventia]', compact('titulo_relatorio', 'request', 'movimentacao_financeira'))->setPaper('a4', 'landscape');
                    return $pdf->stream();
                    break;
                case "HTML":
                    return view('relatorios.pagamento-servico-serventia', compact('titulo_relatorio', 'request', 'movimentacao_financeira'));
                    break;
            }
        } else {
            return view('relatorios.pagamento-servico-serventia-filtro', compact('titulo_relatorio', 'request', 'cidades'));
        }
    }

    public function acompanhamento_notificacao(Request $request, relatorio $relatorio, estado $estado)
    {
        $titulo_relatorio           = 'Relatórios / Acompanhamento de Notificações (Serventia)';
        $cidades                    = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        if ($request->isMethod('post')) {
            $acompanhamento_notificacao = $relatorio->movimentacao_notificacao($request->id_serventia,$request->id_cidade);
            return view('relatorios.acompanhamento-notificacao', compact('titulo_relatorio', 'request', 'acompanhamento_notificacao'));
        } else {
            return view('relatorios.acompanhamento-notificacao-filtro', compact('titulo_relatorio', 'request', 'cidades'));
        }
    }

    public function notificacao_aguardando_interacao(Request $request, relatorio $relatorio, estado $estado, fase_grupo_produto $fase_grupo_produto, etapa_fase $etapa_fase, acao_etapa $acao_etapa, situacao_pedido_grupo_produto $situacao, alcada $alcada)
    {
        $titulo_relatorio           = 'Relatórios / Lista de notificações aguardando interação';
        $cidades                    = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        $fases = $fase_grupo_produto->where('in_registro_ativo','S')
            ->orderBy('nu_ordem')
            ->get();

        $etapas = $etapa_fase->where('in_registro_ativo','S')
            ->orderBy('nu_ordem')
            ->get();

        $acoes = $acao_etapa->where('in_registro_ativo','S')
            ->orderBy('nu_ordem')
            ->get();

        $situacoes = $situacao->where('in_registro_ativo','S')
                            ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
                            ->orderBy('nu_ordem')->get();

        $alcadas = $alcada->where('in_registro_ativo','S')
                            ->whereIn('codigo_alcada', array( $this::ID_ALCADA_SERVENTIA, $this::ID_ALCADA_BANCO) )
                            ->orderBy('nu_ordem')->get();

        if ($request->isMethod('post')) {
            $acompanhamento_notificacao = $relatorio
                                                    ->notificacao_aguardando_interacao(
                                                                $request->id_alcada,
                                                                $request->id_serventia,
                                                                $request->id_cidade,
                                                                $request->id_situacao,
                                                                $request->id_fase,
                                                                $request->id_etapa,
                                                                $request->id_acao
                                                    );
            return view('relatorios.notificacao-aguardando-interacao', compact('titulo_relatorio', 'request', 'acompanhamento_notificacao'));
        } else {
            return view('relatorios.notificacao-aguardando-interacao-filtro', compact('titulo_relatorio', 'request', 'cidades', 'fases', 'etapas', 'acoes', 'situacoes', 'alcadas'));
        }
    }

    public function salvar_relatorio_excel_aguardando_interecao (Request $request, relatorio $relatorio)
    {

        $titulo_relatorio           = 'Relatórios / Lista de notificações aguardando interação';


        $acompanhamento_notificacao = $relatorio
                                                ->notificacao_aguardando_interacao(
                                                    $request->id_alcada,
                                                    $request->id_serventia,
                                                    $request->id_cidade,
                                                    $request->id_situacao,
                                                    $request->id_fase,
                                                    $request->id_etapa,
                                                    $request->id_acao
                                                );

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('aguardando_interacao_' . $time, function ($excel) use ($acompanhamento_notificacao, $titulo_relatorio) {

            $excel->sheet('Acomp. de Notificações', function ($sheet) use ($acompanhamento_notificacao, $titulo_relatorio) {

                $sheet->loadView('xls.movimentacao-aguardando-interacao', [
                        'acompanhamento_notificacao' => $acompanhamento_notificacao,
                        'titulo_relatorio' => $titulo_relatorio,
                    ]
                );


                $sheet->setColumnFormat(
                    [
                        'C' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                        'G' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER,
                        'H' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER
                    ]
                );



                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }

    public function salvar_relatorio_excel_acomp_notificacao (Request $request, relatorio $relatorio)
    {
        $id_cidade              = '';
        $id_serventia           = '';
        $op_relatorio           = 'PG_EXCEL';
        $campos_ordem           = '';
        $caracter_delimitador   = ';';

        $titulo_relatorio           = 'Relatórios / Acompanhamento de Notificações (Serventia)';


        $acompanhamento_notificacao = $relatorio->movimentacao_notificacao($request->id_serventia,$request->id_cidade);

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('acompanha_notificacao_serventia_' . $time, function ($excel) use ($acompanhamento_notificacao) {

            $excel->sheet('Acomp. de Notificações', function ($sheet) use ($acompanhamento_notificacao) {

                $sheet->loadView('xls.movimentacao-notificacao-anoreg', [
                                                                            'acompanhamento_notificacao' => $acompanhamento_notificacao
                                                                        ]
                                                                         );


                $sheet->setColumnFormat(
                                [
                                    'C' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                                    'G' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER,
                                    'H' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER
                                ]
                                );



                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }




    public function mov_serv_nao_pago_caixa(Request $request, relatorio $relatorio, serventia $serventia)
    {
        $titulo_relatorio = 'Relatório de Movimentação de Serviços Aprovados e Não Pagos (Caixa)';
        $pessoa_ativa = Session::get('pessoa_ativa');
        $serventia = $serventia->where('id_pessoa',$pessoa_ativa->pessoa->id_pessoa)->get();
        $id_serventia = $serventia[0]->id_serventia;
        $id_produto = "E";

        if ($request->isMethod('post')) {

            $movimentacao_financeira = $relatorio->movimentacao_financeira_serventia(Auth::User()->id_usuario, $pessoa_ativa->pessoa->id_pessoa, formatar_data_sql($request->dt_inicio), formatar_data_sql($request->dt_fim), $id_produto, $id_serventia, ';', 'CX_ANOREG_SERV_VL_APROV_00');

            switch ($request->tp_saida) {
                case "PDF":
                    $pdf = PDF::loadView('relatorios.acompanha-servico-nao-pago-caixa', compact('titulo_relatorio', 'request', 'movimentacao_financeira'))->setPaper('a4', 'landscape');
                    return $pdf->stream();
                    break;
                case "HTML":
                    return view('relatorios.acompanha-servico-nao-pago-caixa', compact('titulo_relatorio', 'request', 'movimentacao_financeira'));
                    break;
            }
        } else {
            return view('relatorios.acompanha-servico-nao-pago-caixa-filtro', compact('titulo_relatorio', 'request'));
        }
    }
    public function dados_cadastrais(Request $request, serventia $serventia)
    {
        $titulo_relatorio = 'Serventias - Dados Cadastrais';

        $serventias = $serventia->select(
            'serventia.no_serventia',
            'serventia.no_oficial',
            'serventia.no_substituto',
            'serventia.hora_inicio_expediente',
            'serventia.hora_inicio_almoco',
            'serventia.hora_termino_almoco',
            'serventia.hora_termino_expediente',
            'ts.no_tipo_serventia',
            'e.no_endereco',
            'e.no_complemento',
            'e.no_bairro',
            'c.no_cidade',
            'c.uf',
            'b.no_banco',
            'b.codigo_banco',
            'bp.nu_agencia',
            'bp.nu_dv_agencia',
            'bp.nu_conta',
            'bp.nu_dv_conta',

            DB::raw("CASE 
                                                    WHEN bp.tipo_conta = 'C' THEN 'Corrente' ELSE 'Poupança' 
                                                 END AS tipo_conta, 
                                                 bp.nu_variacao
                                                "),

            'bp.nu_variacao',
            'bp.in_tipo_pessoa_conta',
            'bp.nu_cpf_cnpj_conta',
            'sc.dv_codigo_cns',
            'sc.codigo_cns'
        )
            ->join('pessoa_endereco as pe', 'pe.id_pessoa', '=', 'serventia.id_pessoa')
            ->join('endereco as e', 'e.id_endereco', '=', 'pe.id_endereco')
            ->join('cidade as c', 'c.id_cidade', '=', 'e.id_cidade')
            ->leftjoin('serventia_cliente as sc', function ($left) {
                $left->on('sc.id_serventia', '=', 'serventia.id_serventia');
            })
            ->join('tipo_serventia as ts', function ($join) {
                $join->on('ts.id_tipo_serventia', '=', 'serventia.id_tipo_serventia');
            })
            ->leftjoin('usuario_pessoa as up', function ($join) {
                $join->on('up.id_pessoa', '=', 'serventia.id_pessoa')
                    ->on('up.id_usuario', 'in', DB::raw('(select up1.id_usuario 
                                                                                          from ceri.usuario up1 
                                                                                          inner join ceri.banco_pessoa bp1 on (bp1.id_pessoa = up1.id_pessoa)                                               
                                                                                          where up1.id_usuario = up.id_usuario
                                                                                          limit 1
                                                                                          )'));

            })
            ->leftjoin('usuario as us', 'us.id_usuario', '=', 'up.id_usuario')
            ->leftjoin('banco_pessoa as bp', 'bp.id_pessoa', '=', 'serventia.id_pessoa')
            ->leftjoin('banco as b', 'b.id_banco', '=', 'bp.id_banco')
            ->where('serventia.in_registro_ativo', '=', 'S')
            ->orderBy(
                'c.no_cidade',
                'serventia.no_serventia'
            )
            ->get();

        return view('relatorios.dados-cadastrais', compact('titulo_relatorio', 'request', 'serventias'));

    }

    public function salvar_dados_cadastrais(Request $request, serventia $serventia)
    {
        $serventias = $serventia->select(
            'serventia.no_serventia',
            'serventia.no_oficial',
            'serventia.no_substituto',
            'serventia.hora_inicio_expediente',
            'serventia.hora_inicio_almoco',
            'serventia.hora_termino_almoco',
            'serventia.hora_termino_expediente',
            'ts.no_tipo_serventia',
            'e.no_endereco',
            'e.no_complemento',
            'e.no_bairro',
            'c.no_cidade',
            'c.uf',
            'b.no_banco',
            'b.codigo_banco',
            'bp.nu_agencia',
            'bp.nu_dv_agencia',
            'bp.nu_conta',
            'bp.nu_dv_conta',
            DB::raw("CASE 
                                                    WHEN bp.tipo_conta = 'C' THEN 'Corrente' ELSE 'Poupança' 
                                                 END AS tipo_conta, 
                                                 bp.nu_variacao
                                                "),
            'bp.nu_variacao',
            'bp.in_tipo_pessoa_conta',
            'bp.nu_cpf_cnpj_conta',
            'sc.dv_codigo_cns',
            'sc.codigo_cns'

        )
            ->join('pessoa_endereco as pe', 'pe.id_pessoa', '=', 'serventia.id_pessoa')
            ->join('endereco as e', 'e.id_endereco', '=', 'pe.id_endereco')
            ->join('cidade as c', 'c.id_cidade', '=', 'e.id_cidade')
            ->leftjoin('serventia_cliente as sc', function ($left) {
                $left->on('sc.id_serventia', '=', 'serventia.id_serventia');
            })
            ->join('tipo_serventia as ts', function ($join) {
                $join->on('ts.id_tipo_serventia', '=', 'serventia.id_tipo_serventia');
            })
            ->leftjoin('usuario_pessoa as up', function ($join) {
                $join->on('up.id_pessoa', '=', 'serventia.id_pessoa')
                    ->on('up.id_usuario', 'in', DB::raw('(select up1.id_usuario 
                                                                                          from ceri.usuario up1 
                                                                                          inner join ceri.banco_pessoa bp1 on (bp1.id_pessoa = up1.id_pessoa)                                               
                                                                                          where up1.id_usuario = up.id_usuario
                                                                                          limit 1
                                                                                          )'));

            })
            ->leftjoin('usuario as us', 'us.id_usuario', '=', 'up.id_usuario')
            ->leftjoin('banco_pessoa as bp', 'bp.id_pessoa', '=', 'serventia.id_pessoa')
            ->leftjoin('banco as b', 'b.id_banco', '=', 'bp.id_banco')
            ->where('serventia.in_registro_ativo', '=', 'S')
            ->orderBy(
                'c.no_cidade',
                'serventia.no_serventia'
            )
            ->get();


        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('Lista_serventia_' . $time, function ($excel) use ($serventias) {

            $excel->sheet('serventias', function ($sheet) use ($serventias) {

                $sheet->loadView('xls.dados-cadastrais', array('serventias' => $serventias));
                $sheet->setColumnFormat(array('A' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));

                $sheet->getStyle('A1:AA3')->applyFromArray(['fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => ['rgb' => 'E0EEEE']]]);

                $sheet->setAutoSize(true);
            });

        })->export('xls');
    }
    public function creditos_compra(Request $request, compra_credito $compra_credito)
    {
        $todos_credito = $compra_credito->orderBy('id_compra_credito', 'desc')->paginate(20, ['*'], 'credito-pag');

        return view('creditos.creditos', compact('todos_credito', 'request'));
    }

    public function alienacao_geral(Request $request, estado $estado, alienacao $alienacao, serventia $serventia) {
        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        if ($request->cidade) {
            $serventias = $serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
                                    ->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
                                    ->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                                    ->where('endereco.id_cidade',$request->cidade)
                                    ->whereIn('serventia.id_tipo_serventia',array(1,3))
                                    ->where('serventia.in_registro_ativo','S')
                                    ->orderBy('serventia.no_serventia')
                                    ->get();
        } else {
            $serventias = array();
        }

        $alienacao_regras = $alienacao->regras_filtros()['regras'];
        $alienacao_titulos = $alienacao->regras_filtros()['titulos'];

        return view('relatorios.alienacao-geral-filtro',compact('this','request','cidades','serventias','alienacao_regras','alienacao_titulos'));
    }

    public function alienacao_geral_salvar(Request $request, alienacao $alienacao) {
        set_time_limit(0);

        $alienacoes = $alienacao->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido');

        $alienacao_regras = $alienacao->regras_filtros()['regras'];
        $alienacao_titulos = $alienacao->regras_filtros()['titulos'];

        switch ($this->id_tipo_pessoa) {
            case 8:
                $alienacoes = $alienacoes->where('pedido.id_pessoa_origem','=',$this->id_pessoa);
                break;
            case 2:
            case 10:
                $alienacoes = $alienacoes->join('pedido_pessoa','pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                         ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
                break;
        }

        if ($request->dt_ini!='' && $request->dt_fim!='') {
            $dt_ini = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_ini.' 00:00:00');
            $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
            $alienacoes->whereBetween('pedido.dt_pedido',array($dt_ini,$dt_fim));
        }
        if ($request->protocolo > 0){
            $alienacoes->where('pedido.protocolo_pedido',$request->protocolo);
        }
        if($request->contrato>0){
            $alienacoes->where('alienacao.numero_contrato',$request->contrato);
        }
        if($request->cpf>0){
            $cpf = preg_replace('#[^0-9]#','',$request->cpf);

            $alienacoes->join('alienacao_devedor_devedor','alienacao_devedor_devedor.id_alienacao','=','alienacao.id_alienacao')
                             ->join('alienacao_devedor',function($join) use ($cpf) {
                                 $join->on('alienacao_devedor.id_alienacao_devedor','=','alienacao_devedor_devedor.id_alienacao_devedor')
                                      ->where('alienacao_devedor.nu_cpf_cnpj','=',$cpf);
                             });
        }
        if ($request->serventia>0) {
            $alienacoes->join('pedido_pessoa','pedido_pessoa.id_pedido','=','pedido.id_pedido')
                             ->join('serventia',function($join) use ($request) {
                                 $join->on('serventia.id_pessoa','=','pedido_pessoa.id_pessoa')
                                      ->where('serventia.id_serventia','=',$request->serventia);
                             });
        }
        if (!empty($request->grupo) or !empty($request->filtro)) {
            if (count($request->grupo)>0 and count($request->filtro)<=0) {
                foreach ($request->grupo as $key => $grupo) {
                    foreach ($alienacao_regras[$grupo] as $key => $regra) {
                        $regras_array[] = $regra;
                    }
                }
            } elseif (count($request->grupo)>0 and count($request->filtro)>0) {
                foreach ($request->filtro as $key => $filtro) {
                    $filtro_array = explode('_',$filtro);
                    $regras_array[] = $alienacao_regras[$filtro_array[0]][$filtro_array[1]];
                }
            }
            $alienacoes->join('alienacao_andamento_atual',function($join) {
                                    $join->on('alienacao_andamento_atual.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                                         ->where('alienacao_andamento_atual.in_registro_ativo','=','S');
                             });
            if ($request->grupo=='observacao'){
                $alienacoes->leftJoin('alienacao_observacao','alienacao_observacao.id_alienacao','=','alienacao.id_alienacao');
            }

            $alienacoes = $alienacao->aplicar_regras($alienacoes,$regras_array);
        }

        $alienacoes = $alienacoes->orderBy('alienacao.dt_cadastro','desc')->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('notificacoes-geral_'.$time, function($excel) use ($alienacoes) {

            $excel->sheet('Notificações', function($sheet) use ($alienacoes) {
                $sheet->loadView('xls.relatorio-alienacoes', array('alienacoes' => $alienacoes));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                                              'B'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                                              'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }

    public function observacoes_enviadas_serventia(Request $request, alienacao_observacao $alienacao_observacao, estado $estado)
    {
        $titulo_relatorio = 'Observações enviadas à Serventia';

        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        if ($request->isMethod('post')){

            $todas_observacoes_enviadas = $alienacao_observacao->select(
                                                                        'a.numero_contrato',
                                                                        'p.protocolo_pedido',
                                                                        'ao.dt_cadastro',
                                                                        'uc.no_usuario as no_usuario_cad',
                                                                        's.no_serventia',
                                                                        'c.no_cidade',
                                                                        'ao.in_leitura',
                                                                        'ul.no_usuario as no_usuario_leitura',
                                                                        'ao.dt_leitura',
                                                                        'ulc.no_usuario as no_usuario_leitura_cancelamento',
                                                                        'ao.dt_leitura_cancelado',
                                                                        'ao.de_observacao'
                                                                        )
                                                                        ->from('alienacao_observacao as ao')
                                                                        ->join('alienacao as a','a.id_alienacao','=','ao.id_alienacao')
                                                                        ->join('alienacao_pedido as ap','ap.id_alienacao','=','a.id_alienacao')
                                                                        ->join('pedido as p','p.id_pedido','=','ap.id_pedido')
                                                                        ->join('usuario as uc','uc.id_usuario','=','ao.id_usuario_cad')
                                                                        ->join('serventia as s','s.id_pessoa','=','ao.id_pessoa_dest' )
                                                                        ->join('pessoa_endereco as pe','pe.id_pessoa', '=', 's.id_pessoa')
                                                                        ->join('endereco as e','e.id_endereco','=','pe.id_endereco')
                                                                        ->join('cidade as c','c.id_cidade','=','e.id_cidade')
                                                                        ->leftJoin('usuario as ul','ul.id_usuario','=','ao.id_usuario_leitura')
                                                                        ->leftJoin('usuario as ulc','ulc.id_usuario','=','ao.id_usuario_leitura_cancelado')
                                                                        ->where('ao.id_pessoa','=', 61);

            if($request->in_leitura != 'T'){
                $todas_observacoes_enviadas->where('ao.in_leitura', $request->in_leitura);
            }

            if($request->cidade > 0){
                $todas_observacoes_enviadas->where('c.id_cidade', $request->cidade);
            }
            if($request->serventia > 0){
                $todas_observacoes_enviadas->where('s.id_serventia', $request->serventia);
            }
            if ($request->dt_inicio != '' && $request->dt_fim != '') {
                $dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
                $dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                $todas_observacoes_enviadas->whereBetween('ao.dt_cadastro',array($dt_inicio,$dt_fim));
            }

            $todas_observacoes_enviadas = $todas_observacoes_enviadas->get();

            switch ($request->tp_saida){
                case "PDF":
                    $pdf = PDF::loadview('relatorios.observacoes-enviadas-serventia', compact('titulo_relatorio','request', 'todas_observacoes_enviadas'))->setPaper('a4','landscape');
                    return $pdf->stream();
                    break;
                case "HTML":
                    return view('relatorios.observacoes-enviadas-serventia', compact('titulo_relatorio', 'request','todas_observacoes_enviadas'));
                    break;
            }
        } else {
            return view('relatorios.observacoes-enviadas-serventia-filtro', compact('titulo_relatorio','request', 'cidades'));
        }
    }

}
