<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;

use App\estado;
use App\produto;
use App\certidao;
use App\serventia;
use App\pedido;
use App\pedido_serventia;
use App\pedido_serventia_resposta;
use App\situacao_pedido_grupo_produto;

class PedidoController extends Controller {
	
	public function __construct(pedido $pedido) {
		$this->pedido=$pedido;
	}
}
