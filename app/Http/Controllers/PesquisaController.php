<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\pessoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Mail;
use Excel;

use App\usuario;
use App\alcada;
use App\estado;
use App\produto_item;
use App\pesquisa;
use App\serventia;
use App\pedido;
use App\pedido_serventia;
use App\pedido_serventia_resposta;
use App\situacao_pedido_grupo_produto;
use App\processo;
use App\matricula;
use App\tipo_pesquisa;
use App\tipo_resposta;
use App\chave_pesquisa_pesquisa;
use App\pedido_produto_item;
use App\historico_pedido;
use App\pedido_valor;
use App\pedido_resultado;
use App\preco_produto_item;
use App\pedido_resultado_matricula;
use App\tipo_custa;
use App\arquivo_grupo_produto;
use App\arquivo_grupo_produto_composicao;
use App\pesquisa_observacao;

class PesquisaController extends Controller {

	const ID_GRUPO_PRODUTO = 1;
	const ID_PRODUTO = 1;
	const ID_PRODUTO_ITEM = 25;
	const ID_SITUACAO_ENVIADO = 2;
	const ID_SITUACAO_ANALISE = 4;
	const ID_SITUACAO_FINALIZADO = 7;
    const ID_TIPO_ARQUIVO_REQUERIMENO_PESQUISA = 23; 

	public function __construct(pesquisa $pesquisa) {
		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, pesquisa $pesquisa, serventia $serventia, situacao_pedido_grupo_produto $situacao, chave_pesquisa_pesquisa $chave_pesquisa_filtro, estado $estado){

		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela principal de pesquisa','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		$class = $this;

		switch ($this->id_tipo_pessoa) {
			case 3: case 4: case 5: case 7: case 11: case 6:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
									  ->orderBy('nu_ordem')->get();

				$todas_pesquisas = $pesquisa->join('pedido','pedido.id_pedido','=','pesquisa.id_pedido')
											->join('produto','produto.id_produto','=','pedido.id_produto')
											->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
											->where('pedido.id_pessoa_origem','=',$this->id_pessoa);

				$tipos_chave_filtro = $chave_pesquisa_filtro->where('in_registro_ativo','S')
															->where('in_exibe_filtro','S')
															->orderBy('nu_ordem','asc')
															->get();

					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_pesquisas->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_pesquisas->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ( $request->protocolo_pedido > 0){
						$todas_pesquisas->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_chave_pesquisa_pesquisa>0){
						$todas_pesquisas->where('pesquisa.id_chave_pesquisa_pesquisa',$request->id_chave_pesquisa_pesquisa);
                        switch ($request->id_chave_pesquisa_pesquisa) {
                        	case 2: case 3:
                        		$todas_pesquisas->where('pesquisa.de_chave_pesquisa',str_replace("'", "\\''", $request->de_chave_pesquisa));
                        		if ($request->de_chave_complementar!='') {
                        			$todas_pesquisas->where('pesquisa.de_chave_complementar',str_replace("'", "\\''", $request->de_chave_pesquisa));
                        			$todas_pesquisas->where('pesquisa.de_chave_complementar',str_replace("'", "\\''", $request->de_chave_pesquisa));
                        		}
                        		break;
                        	case 4:
                            	$todas_pesquisas->whereRaw("lower(pesquisa.de_chave_pesquisa) LIKE '%".strtolower(str_replace("'", "\\''", $request->de_chave_pesquisa))."%'");
                            	break;
                            default:
                            	$todas_pesquisas->where('pesquisa.de_chave_pesquisa',str_replace("'","\\''",$request->de_chave_pesquisa));
                            	break;
                        }
					}
                    if ((!is_null($request->id_serventia)) && (!empty($request->id_serventia))){
                        $todas_pesquisas->join('pedido_pessoa', 'pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                        ->join('pessoa', 'pedido_pessoa.id_pessoa', '=', 'pessoa.id_pessoa')
                                        ->join('serventia', 'serventia.id_pessoa','=','pessoa.id_pessoa')
                                        ->where('serventia.id_serventia',$request->id_serventia);
                    }
                    if ((!is_null($request->id_cidade)) && (!empty($request->id_cidade))){
                        $todas_pesquisas->join ('pedido_pessoa as pp','pp.id_pedido', '=','pedido.id_pedido')
                                        ->join ('pessoa as p','p.id_pessoa','=','pp.id_pessoa')
                                        ->join ('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'p.id_pessoa')
                                        ->join ('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                                        ->where('endereco.id_cidade',$request->id_cidade);
                    }
                    if (in_array($this->id_tipo_pessoa,[6,11])) {
                        if (count(Auth::User()->usuario_pessoa[0]->pessoa->unidade_judiciaria_divisao)>0){
                            if ((Auth::User()->usuario_pessoa[0]->pessoa->unidade_judiciaria_divisao->in_compartilha_servico == 'N') && (Auth::User()->in_usuario_master == 'N')){
                                $todas_pesquisas->where('pedido.id_usuario','=', Auth::User()->id_usuario);
                            }
                        }
                    }

                $todas_pesquisas = $todas_pesquisas->orderBy('pedido.dt_cadastro',($request->ord?$request->ord:'desc'))->paginate(10, ['*'], 'pesquisa-pag');

                $todas_pesquisas->appends(Request::capture()->except('pesquisa-pag'))->render();

		    	return view('servicos.pesquisa.geral-pesquisa',compact('request','class','situacoes','todas_pesquisas', 'tipos_chave_filtro', 'cidades'));
				break;
			case 2:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
									  ->orderBy('nu_ordem')->get();

				$tipos_chave_filtro = $chave_pesquisa_filtro->where('in_registro_ativo','S')
															->where('in_exibe_filtro','S')
															->orderBy('nu_ordem','asc')
															->get();

				$todas_pesquisas = $pesquisa->join('pedido','pedido.id_pedido','=','pesquisa.id_pedido')
                                            ->join('situacao_pedido_grupo_produto','situacao_pedido_grupo_produto.id_situacao_pedido_grupo_produto','=','pedido.id_situacao_pedido_grupo_produto')
											->join('produto','produto.id_produto','=','pedido.id_produto')
											->join('pedido_pessoa',function($join) {
												$join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
											 		 ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
											})
                                            ->where('situacao_pedido_grupo_produto.in_cliente','=','S');
											//->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO);

					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_pesquisas->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_pesquisas->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ( $request->protocolo_pedido > 0){
						$todas_pesquisas->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
                    if( $request->id_chave_pesquisa_pesquisa>0){
                        $todas_pesquisas->where('pesquisa.id_chave_pesquisa_pesquisa',$request->id_chave_pesquisa_pesquisa);
                        switch ($request->id_chave_pesquisa_pesquisa) {
                        	case 2: case 3:
                        		if ($request->de_chave_pesquisa!='') {
                        			$todas_pesquisas->where('pesquisa.de_chave_pesquisa',$request->de_chave_pesquisa);
                        		}
                        		if ($request->de_chave_complementar!='') {
                        			$todas_pesquisas->where('pesquisa.de_chave_complementar',$request->de_chave_complementar);
                        		}
                        		break;
                        	case 4:
                        		if ($request->de_chave_pesquisa!='') {
                            		$todas_pesquisas->whereRaw("lower(pesquisa.de_chave_pesquisa) LIKE '%".strtolower($request->de_chave_pesquisa)."%'");
                            	}
                            	break;
                            default:
                            	if ($request->de_chave_pesquisa!='') {
                        			$todas_pesquisas->where('pesquisa.de_chave_pesquisa',$request->de_chave_pesquisa);
                        		}
                            	break;
                        }
                    }

				$todas_pesquisas = $todas_pesquisas->orderBy('pedido.dt_cadastro',($request->ord?$request->ord:'desc'))->paginate(10,['*'],'pesquisa-pag');

                $todas_pesquisas->appends(Request::capture()->except('pesquisa-pag'))->render();

		    	return view('servicos.pesquisa.serventia-pesquisa',compact('request','class','situacoes','todas_pesquisas', 'tipos_chave_filtro'));
				break;
            case 13: case 9:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
									  ->orderBy('nu_ordem')->get();

				$todas_pesquisas = $pesquisa->join('pedido','pedido.id_pedido','=','pesquisa.id_pedido')
											->join('produto','produto.id_produto','=','pedido.id_produto')
											->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO);

				$tipos_chave_filtro = $chave_pesquisa_filtro->where('in_registro_ativo','S')
															->where('in_exibe_filtro','S')
															->orderBy('nu_ordem','asc')
															->get();


					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_pesquisas->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_pesquisas->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ( $request->protocolo_pedido > 0){
						$todas_pesquisas->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if ($request->id_chave_pesquisa_pesquisa>0){
						$todas_pesquisas->where('pesquisa.id_chave_pesquisa_pesquisa',$request->id_chave_pesquisa_pesquisa);
                        switch ($request->id_chave_pesquisa_pesquisa) {
                        	case 2: case 3:
                        		$todas_pesquisas->where('pesquisa.de_chave_pesquisa',$request->de_chave_pesquisa);
                        		if ($request->de_chave_complementar!='') {
                        			$todas_pesquisas->where('pesquisa.de_chave_complementar',$request->de_chave_complementar);
                        		}
                        		break;
                        	case 4:
                            	$todas_pesquisas->whereRaw("lower(pesquisa.de_chave_pesquisa) LIKE '%".strtolower($request->de_chave_pesquisa)."%'");
                            	break;
                            default:
                            	$todas_pesquisas->where('pesquisa.de_chave_pesquisa',$request->de_chave_pesquisa);
                            	break;
                        }
					}
                    if ((!is_null($request->id_serventia)) && (!empty($request->id_serventia))){
                        $todas_pesquisas->join('pedido_pessoa', 'pedido_pessoa.id_pedido','=','pedido.id_pedido')
                            ->join('pessoa', 'pedido_pessoa.id_pessoa', '=', 'pessoa.id_pessoa')
                            ->join('serventia', 'serventia.id_pessoa','=','pessoa.id_pessoa')
                            ->where('serventia.id_serventia',$request->id_serventia);
                    }
                    if ((!is_null($request->id_cidade)) && (!empty($request->id_cidade))){
                        $todas_pesquisas->join ('pedido_pessoa as pp','pp.id_pedido', '=','pedido.id_pedido')
                            ->join ('pessoa as p','p.id_pessoa','=','pp.id_pessoa')
                            ->join ('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'p.id_pessoa')
                            ->join ('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco' )
                            ->where('endereco.id_cidade',$request->id_cidade);
                    }
				$todas_pesquisas = $todas_pesquisas->orderBy('pedido.dt_cadastro',($request->ord?$request->ord:'desc'))->paginate(10, ['*'], 'pesquisa-pag');

                $todas_pesquisas->appends(Request::capture()->except('_token'))->render();

		    	return view('servicos.pesquisa.geral-pesquisa',compact('request','class','situacoes','todas_pesquisas', 'tipos_chave_filtro', 'cidades'));
				break;
		}
    }

    public function nova(estado $estado, produto_item $produto_item, tipo_pesquisa $tipo_pesquisa, chave_pesquisa_pesquisa $chave_pesquisa, tipo_custa $tipo_custa) {
		$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		$serventias = $estado->find(env('ID_ESTADO'))->serventias_estado()->orderBy('serventia.no_serventia')->get();

		$tipos_chave = $chave_pesquisa->where('in_registro_ativo','S')
									  ->orderBy('nu_ordem','asc')
									  ->where('tp_chave_pesquisa_pesquisa','1')
									  ->get();

		if ($this->id_tipo_pessoa!=3) {
			$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();
		} else {
			$tipos_custa = array();
		}

		$tipos = $tipo_pesquisa->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();

		$class = $this;
		$pesquisa_token = str_random(30);
		
		return view('servicos.pesquisa.geral-pesquisa-nova',compact('class','cidades','serventias','tipos_chave','tipos_custa','tipos','pesquisa_token'));
    }


    public function inserir(Request $request, produto_item $produto_item) {
		$erro = 0;

		DB::beginTransaction();

		$saldo_usuario = Auth::User()->saldo_usuario();
		$valor_total = 0;
	
		foreach ($request->id_serventia as $id_serventia) {
			$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

			$novo_pedido = new pedido();
			$novo_pedido->id_usuario = Auth::User()->id_usuario;
			$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_ENVIADO;
			$novo_pedido->id_produto = $this::ID_PRODUTO;
			$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
			$novo_pedido->dt_pedido = Carbon::now();
			$novo_pedido->nu_quantidade = 0;
			$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
			$novo_pedido->dt_cadastro = Carbon::now();
			$novo_pedido->id_pessoa_origem = $this->id_pessoa;

			if ($novo_pedido->save()) {
				$serventia_selecionada = new serventia();
				$serventia_selecionada = $serventia_selecionada->find($id_serventia);

				$novo_pedido->pessoas()->attach($serventia_selecionada->pessoa);
				
				$novo_pedido_produto_item = new pedido_produto_item();
				$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
				$novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM;
				$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
				if (!$novo_pedido_produto_item->save()) {
					$erro = 10003;
				}

				$nova_pesquisa = new pesquisa();
				$nova_pesquisa->id_pedido = $novo_pedido->id_pedido;
				$nova_pesquisa->id_tipo_pesquisa = (!empty($request->id_tipo_pesquisa)?$request->id_tipo_pesquisa:NULL);
				$nova_pesquisa->id_chave_pesquisa_pesquisa = $request->id_chave_pesquisa_pesquisa;
				$nova_pesquisa->de_chave_pesquisa = $request->de_chave_pesquisa;
				$nova_pesquisa->in_certidao = 'N';
				$nova_pesquisa->in_penhora = 'N';
				$nova_pesquisa->in_indisponibilidade = 'N';
				$nova_pesquisa->dt_cadastro = Carbon::now();
				$nova_pesquisa->dt_geracao = Carbon::now();
				$nova_pesquisa->dt_liberacao = Carbon::now();
				$nova_pesquisa->dt_indisponibilizacao = Carbon::now()->addDays(30);
				$nova_pesquisa->nu_dia_disponibilizacao = 30;
				$nova_pesquisa->id_tipo_custa = ($request->id_tipo_custa>0?$request->id_tipo_custa:2);
				$nova_pesquisa->de_chave_complementar = (!empty($request->de_chave_complementar)?trim($request->de_chave_complementar):NULL);
                $nova_pesquisa->ds_observacao = $request->ds_observacao;

				switch ($request->id_tipo_custa) {
					case 1:
						$novo_pedido->va_pedido = 0;

						$processo = new processo();
						$processo = $processo->where('numero_processo',$request->numero_processo)->first();

						if ($processo) {
							$nova_pesquisa->id_processo	= $processo->id_processo;
						} else {
							$novo_processo = new processo();
							$novo_processo->numero_processo = $request->numero_processo;
							$novo_processo->id_usuario_cad = Auth::User()->id_usuario;
							$novo_processo->dt_cadastro = Carbon::now();
							if($novo_processo->save()) {
								$nova_pesquisa->id_processo = $novo_processo->id_processo;
							} else {
								$erro = 10010;
							}
						}
						break;
					case 3:
						$novo_pedido->va_pedido = 0;
						break;
					default:
						$produto_item = $produto_item->find($this::ID_PRODUTO_ITEM);
						$valor_unitario = $produto_item->preco_total($serventia_selecionada->pessoa->id_pessoa);
						$valor_total += $valor_unitario;

						$novo_pedido->va_pedido = $valor_unitario;

						$valor_args = [
						    'va_pedido'                     =>$novo_pedido->va_pedido,
                            "tp_movimentacao_financeira"    => 2,
                            "tp_movimentacao"               => 'S',
                            "id_forma_pagamento"            => 6,
                            "v_id_pedido_desconto"          => 'null',
                            "v_va_desconto"                 => 0,
                            "v_in_desconto"                 => 'N',
                            "v_va_desconto_perc"            => 'null',
                            "v_id_compra_credito"           => $request->id_compra_credito
                        ];

						$novo_pedido->registrar_valor_pedido($valor_args);
						break;
				}
				if (!$novo_pedido->save()) {
					$erro = 10009;
				}

				if ($nova_pesquisa->save()) {
					$novo_historico = new historico_pedido();
					$novo_historico->id_pedido = $novo_pedido->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $novo_pedido->id_situacao_pedido_grupo_produto;
					$novo_historico->de_observacao = 'Pedido de pesquisa inserido com sucesso.';
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_historico->save()) {
						$erro = 10004;
					}

					/*
					** FUNÇÃO DESABILITADA A PEDIDO DO SEGISMAR POIS AS MATRÍCULAS AUTOMÁTICAS NÃO FORAM POPULADAS CORRETAMENTE
					** Douglas 20-07-2017

					$matricula = new matricula;
					switch ($nova_pesquisa->id_chave_pesquisa_pesquisa) {
						case 1:
							$busca_matriculas = $matricula->where('id_serventia', $serventia_selecionada->id_serventia)
								->where('codigo_matricula', $nova_pesquisa->de_chave_pesquisa)
								->select('matricula.id_matricula','matricula.codigo_matricula')
								->get();
							break;
						case 2:
							$busca_matriculas = $matricula->leftJoin('proprietario', 'proprietario.id_matricula', '=', 'matricula.id_matricula')
								->where('matricula.id_serventia', $serventia_selecionada->id_serventia)
								->where('proprietario.nu_cpf_cnpj', preg_replace('#[^0-9]#', '', $nova_pesquisa->de_chave_pesquisa))
								->where('proprietario.tp_pessoa', 'F')
								->select('matricula.id_matricula','matricula.codigo_matricula')
								->get();
							break;
						case 3:
							$busca_matriculas = $matricula->leftJoin('proprietario', 'proprietario.id_matricula', '=', 'matricula.id_matricula')
								->where('matricula.id_serventia', $serventia_selecionada->id_serventia)
								->where('proprietario.nu_cpf_cnpj', preg_replace('#[^0-9]#', '', $nova_pesquisa->de_chave_pesquisa))
								->where('proprietario.tp_pessoa', 'J')
								->select('matricula.id_matricula','matricula.codigo_matricula')
								->get();
							break;
						case 4:
							$busca_matriculas = $matricula->leftJoin('proprietario', 'proprietario.id_matricula', '=', 'matricula.id_matricula')
								->where('matricula.id_serventia', $serventia_selecionada->id_serventia)
								->whereRaw("UPPER(proprietario.no_proprietario) = UPPER('" . $nova_pesquisa->de_chave_pesquisa . "')")
								->select('matricula.id_matricula','matricula.codigo_matricula')
								->get();
							break;
					}

					if (count($busca_matriculas) > 0) {
						$novo_resultado = new pedido_resultado;
						$novo_resultado->id_pedido = $novo_pedido->id_pedido;
						$novo_resultado->in_positivo = 'S';
						$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;

						if ($novo_resultado->save()) {
							$matriculas = array();
							$erro_loop = false;
							foreach ($busca_matriculas as $matricula) {
								$novo_resultado_matricula = new pedido_resultado_matricula();
								$novo_resultado_matricula->id_pedido_resultado = $novo_resultado->id_pedido_resultado;
								$novo_resultado_matricula->id_matricula = $matricula->id_matricula;
								if (!$novo_resultado_matricula->save()) {
									$erro_loop = true;
								}
								
								$matriculas[] = $matricula->codigo_matricula;
							}
							if ($erro_loop) {
								$erro = 10005;
							}
							
							$novo_pedido->where('id_pedido', $novo_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_FINALIZADO]);

							$novo_historico = new historico_pedido();
							$novo_historico->id_pedido = $novo_pedido->id_pedido;
							$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_FINALIZADO;
							$novo_historico->de_observacao = 'Resultado enviado! Resultado positivo, salvo automaticamente.';
							$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
							if (!$novo_historico->save()) {
								$erro = 10006;
							}
							
							$retorno_msg[] = '<b>'.$serventia_selecionada->no_serventia.':</b><br />Foram localizadas as seguintes matrículas: '.implode(', ',$matriculas).'.';

							$novo_pedido->inserir_notificacao($serventia_selecionada->id_pessoa,$novo_pedido->id_pessoa_origem,'A pesquisa protocolo '.$protocolo_pedido[0]->f_geraprotocolo.' obteve resultados automáticos.');
						}
					} else {
					*/
						$novo_pedido->inserir_notificacao($this->id_pessoa,$serventia_selecionada->id_pessoa,'Uma nova pesquisa foi inserida. Protocolo '.$protocolo_pedido[0]->f_geraprotocolo);
						$retorno_msg[] = '<b>'.$serventia_selecionada->no_serventia.':</b><br />Não foram localizados registros automáticos. A busca será processada pelo cartório e a resposta será disponibilizada no portal em até 48 (quarenta e oito) horas.';

                        /**
                         * Criação arquivo requerimento
                         */
                        if (in_array($nova_pesquisa->id_tipo_custa,[1,2])) {
							$destino = '/pesquisas/'.$nova_pesquisa->id_pesquisa.'/'.$this::ID_TIPO_ARQUIVO_REQUERIMENO_PESQUISA;
                            $no_arquivo = 'requerimento_'.$nova_pesquisa->pedido->protocolo_pedido.'.pdf';
                            $destino_arquivo = '/public/'.$destino.'/'.$no_arquivo;
                            $opcao = 'pesquisa';

                            Storage::makeDirectory('/public'.$destino);

                            $pdf = PDF::loadView('pdf.requerimento', compact('nova_pesquisa', 'opcao'));

                            if (Storage::exists($destino_arquivo)) {
                                Storage::delete($destino_arquivo);
                            }
                            if ($pdf->save(storage_path('app' . $destino_arquivo))) {
                                $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                                $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                                $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $this::ID_TIPO_ARQUIVO_REQUERIMENO_PESQUISA;
                                $novo_arquivo_grupo_produto->no_arquivo = $no_arquivo;
                                $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                                $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                                $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($no_arquivo);
                                $novo_arquivo_grupo_produto->in_ass_digital = 'N';
                                $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($destino_arquivo);
                                $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$destino_arquivo));
                                if($novo_arquivo_grupo_produto->save()){
                                    $nova_pesquisa->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
                                }
                            }
                        }
                    /**
                     * Fim criação requerimento
                     */

					//}

					if ($request->session()->has('arquivos_'.$request->pesquisa_token)) {
						$destino = '/pesquisas/'.$nova_pesquisa->id_pesquisa;
						$arquivos = $request->session()->get('arquivos_'.$request->pesquisa_token);
						$erro_loop_arq = false;
						$erro_loop_sql = false;
						foreach ($arquivos as $key => $arquivo) {
							$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
							Storage::makeDirectory('/public'.$destino_final);

							$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
							$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];

							$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
							$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
							$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
							$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
							$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
							$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
							$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
							$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
							if ($arquivo['dt_assinado']!='') {
								$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
							}
							if ($arquivo['id_usuario_certificado']>0) {
								$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
							}
							if (!empty($arquivo['no_arquivo_p7s'])) {
								$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
								$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
							}
							if ($novo_arquivo_grupo_produto->save()) {
								if (Storage::exists($origem_arquivo)) {
									if (Storage::exists($destino_arquivo)) {
										Storage::delete($destino_arquivo);
									}
									if (Storage::copy($origem_arquivo,$destino_arquivo)) {
										if (count($arquivo['no_arquivos_originais'])>0) {
											foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
												$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
												$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

												$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
												$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
												$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
												$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
												$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
												$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
												if ($novo_arquivo_grupo_produto_composicao->save()) {
													if (Storage::exists($origem_arquivo_original)) {
														if (Storage::exists($destino_arquivo_original)) {
															Storage::delete($destino_arquivo_original);
														}
														if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_sql = true;
												}
											}
										}
									} else {
										$erro_loop_arq = true;
									}
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_sql = true;
							}
							$nova_pesquisa->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
						}
						if ($erro_loop_arq) {
							$erro = 10007;
						}
						if ($erro_loop_sql) {
							$erro = 10008;
						}
					}

				} else {
					$erro = 10002;
				}
			} else {
				$erro = 10001;
			}
		}

		//correcao de if para comparacao de float no PHP 5.6
        $saldo_usuario  = (float)$saldo_usuario;
        $valor_total    = (float)$valor_total;

        if (round($saldo_usuario, 2) < round($valor_total, 2))
		{
			$erro = 10011;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			switch ($erro) {
				case 10011:
					return response()->json(array('status'=> 'saldo_insuficiente',
												  'recarrega' => 'false'));
					break;
				default:
					return response()->json(array('status'=> 'erro',
												  'recarrega' => 'false',
												  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
			}			
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => implode('<br /><br />',$retorno_msg)));
		}
	}

	public function nova_resposta(Request $request, pedido $pedido, tipo_pesquisa $tipo_pesquisa, tipo_resposta $tipo_resposta, chave_pesquisa_pesquisa $chave_pesquisa, tipo_custa $tipo_custa) {
		if ($request->id_pedido>0) {
			$class = $this;
			
			$pedido = $pedido->find($request->id_pedido);
			
			if ($pedido) {
				if (!$pedido->verifica_permissao()) {
					return view('erros.401-pesquisa');
				}
			
				$tipos_chave = $chave_pesquisa->where('in_registro_ativo','S')
										  ->orderBy('nu_ordem','asc')
										  ->get();

				$tipos_resposta = $tipo_resposta->where('in_registro_ativo','S')
									   			->orderBy('nu_ordem','asc')
									   			->get();

				$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();
				
				$tipos = $tipo_pesquisa->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();

				if ($pedido->id_situacao_pedido_grupo_produto==$this::ID_SITUACAO_ENVIADO) {
					$pedido->update(['id_situacao_pedido_grupo_produto'=>$this::ID_SITUACAO_ANALISE]);
				}

				$arquivos_isencao = $pedido->pesquisa->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',18)->get();
				$arquivos_requerimento = $pedido->pesquisa->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',$this::ID_TIPO_ARQUIVO_REQUERIMENO_PESQUISA)->get();
				$pesquisa_token = str_random(30);

				$pedido->visualizar_notificacoes();
				
				return view('servicos.pesquisa.serventia-pesquisa-resposta',compact('class','pedido','tipos_chave','tipos_resposta','tipos_custa','tipos','arquivos_isencao','arquivos_requerimento','pesquisa_token'));
			}
		}
	}

	public function detalhes(Request $request, pedido $pedido, produto_item $produto_item, tipo_pesquisa $tipo_pesquisa, chave_pesquisa_pesquisa $chave_pesquisa, tipo_custa $tipo_custa) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			if ($pedido) {
				if (!$pedido->verifica_permissao()) {
					return view('erros.401-pesquisa');
				}

				$tipos_chave = $chave_pesquisa->where('in_registro_ativo','S')
										  ->orderBy('nu_ordem','asc');
				if ($this->id_tipo_pessoa==3) {
					$tipos_chave = $tipos_chave->where('tp_chave_pesquisa_pesquisa','1');
				}
				$tipos_chave = $tipos_chave->get();

				if ($this->id_tipo_pessoa!=3) {
					$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();
				} else {
					$tipos_custa = array();
				}

				$tipos = $tipo_pesquisa->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();

				$arquivos_isencao = $pedido->pesquisa->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',18)->get();

				$pedido->visualizar_notificacoes();

				$class = $this;

				return view('servicos.pesquisa.geral-pesquisa-detalhes',compact('class','pedido','tipos_chave','tipos_custa','tipos','arquivos_isencao'));
			}
		}
	}
	public function resultado(Request $request, pedido $pedido, produto_item $produto_item, tipo_pesquisa $tipo_pesquisa, chave_pesquisa_pesquisa $chave_pesquisa, tipo_custa $tipo_custa) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			if ($pedido) {
				if (!$pedido->verifica_permissao()) {
					return view('erros.401-pesquisa');
				}

				$tipos_chave = $chave_pesquisa->where('in_registro_ativo','S')
										  ->orderBy('nu_ordem','asc');
				if ($this->id_tipo_pessoa==3) {
					$tipos_chave = $tipos_chave->where('tp_chave_pesquisa_pesquisa','1');
				}
				$tipos_chave = $tipos_chave->get();

				if ($this->id_tipo_pessoa!=3) {
					$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();
				} else {
					$tipos_custa = array();
				}

				$tipos = $tipo_pesquisa->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();

				$arquivos_isencao = $pedido->pesquisa->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',18)->get();
				$arquivos_resultado = $pedido->pesquisa->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',19)->get();
				$arquivos_requerimento = $pedido->pesquisa->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',$this::ID_TIPO_ARQUIVO_REQUERIMENO_PESQUISA)->get();

				$pedido->visualizar_notificacoes();

				$class = $this;

				switch ($this->id_tipo_pessoa) {
					case 3: case 4: case 5: case 7: case 11: case 6: case 13: case 9:
						return view('servicos.pesquisa.geral-pesquisa-resultado',compact('class','pedido','tipos_chave','tipos_custa','tipos','arquivos_isencao','arquivos_resultado'));
						break;
					case 2:
						return view('servicos.pesquisa.serventia-pesquisa-resultado',compact('pedido','tipos_chave','tipos_custa','tipos','arquivos_isencao','arquivos_resultado','arquivos_requerimento'));
						break;
				}
			}
		}
	}

	public function inserir_resultado(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$erro = 0;
            $matriculas_transcricoes = [];
            $a_codigo_matricula = count($request->a_codigo_matricula)>0 ? $request->a_codigo_matricula : [];
            $a_codigo_transcricao = count($request->a_codigo_transcricao)>0 ? $request->a_codigo_transcricao : [];

			DB::beginTransaction();

			if (in_array($request->in_positivo,array('S','N'))) {
				$pedido = $pedido->find($request->id_pedido);

				$novo_resultado = new pedido_resultado();
				$novo_resultado->id_pedido = $request->id_pedido;
				$novo_resultado->in_positivo = $request->in_positivo;
				$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;

				if ($request->in_positivo=='S') {
					switch ($request->tipo_resultado_envio) {
						case 1:
						    $matriculas_transcricoes = array_merge($a_codigo_matricula, $a_codigo_transcricao);
							$de_historico = 'Matrículas e/ou transcrições inseridas: '.implode(', ',$matriculas_transcricoes).'.';
							break;
						case 2:
							if ($request->session()->has('arquivos_'.$request->pesquisa_token)) {
								$destino = '/pesquisas/'.$pedido->pesquisa->id_pesquisa;
								$arquivos = $request->session()->get('arquivos_'.$request->pesquisa_token);
								$erro_loop_arq = false;
								$erro_loop_sql = false;
								$no_arquivos = array();
								foreach ($arquivos as $key => $arquivo) {
									$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
									Storage::makeDirectory('/public'.$destino_final);

									$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
									$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];

									$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
									$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
									$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
									$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
									$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
									$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
									$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
									$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
									$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
									$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
									if ($arquivo['dt_assinado']!='') {
										$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
									}
									if ($arquivo['id_usuario_certificado']>0) {
										$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
									}
									if (!empty($arquivo['no_arquivo_p7s'])) {
										$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
										$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
									}
									if ($novo_arquivo_grupo_produto->save()) {
										if (Storage::exists($origem_arquivo)) {
											if (Storage::exists($destino_arquivo)) {
												Storage::delete($destino_arquivo);
											}
											if (Storage::copy($origem_arquivo,$destino_arquivo)) {
												if (count($arquivo['no_arquivos_originais'])>0) {
													foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
														$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
														$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

														$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
														$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
														$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
														$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
														$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
														$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
														$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
														$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
														if ($novo_arquivo_grupo_produto_composicao->save()) {
															if (Storage::exists($origem_arquivo_original)) {
																if (Storage::exists($destino_arquivo_original)) {
																	Storage::delete($destino_arquivo_original);
																}
																if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
																	$erro_loop_arq = true;
																}
															} else {
																$erro_loop_arq = true;
															}
														} else {
															$erro_loop_sql = true;
														}
													}
												}
											} else {
												$erro_loop_arq = true;
											}
										} else {
											$erro_loop_arq = true;
										}
									} else {
										$erro_loop_sql = true;
									}
									$pedido->pesquisa->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
									$no_arquivos[] = $novo_arquivo_grupo_produto->no_arquivo;
								}
								if ($erro_loop_arq) {
									$erro = 10103;
								}
								if ($erro_loop_sql) {
									$erro = 10104;
								}
							}
							$de_historico = 'Arquivo(s): '.implode(', ',$no_arquivos);
							break;
					}
				} elseif ($request->in_positivo=='N') {
					$de_historico = 'Não foram localizados registros com a chave '.$pedido->pesquisa->de_chave_pesquisa.' pesquisada nesta serventia.';
				}
					
				if ($novo_resultado->save()) {
					if($request->tipo_resultado_envio==1) {
						$erro_loop = false;
						if (count($a_codigo_matricula)>0) {
                            foreach ($a_codigo_matricula as $codigo_matricula) {
                                $matricula = new matricula();
                                $novo_resultado_matricula = new pedido_resultado_matricula();
                                $novo_resultado_matricula->id_pedido_resultado = $novo_resultado->id_pedido_resultado;
                                if ($busca_matricula = $matricula->where('codigo_matricula',$codigo_matricula)->where('id_serventia',$pedido->pedido_pessoa_atual->id_pessoa)->first()) {
                                    $novo_resultado_matricula->id_matricula = $busca_matricula->id_matricula;
                                } else {
                                    $novo_resultado_matricula->codigo_matricula = $codigo_matricula;
                                }
                                if (!$novo_resultado_matricula->save()) {
                                    $erro_loop = true;
                                }
                            }
                        }
                        if (count($a_codigo_matricula)>0) {
                            foreach ($a_codigo_transcricao as $codigo_transcricao) {
                                $matricula = new matricula();
                                $novo_resultado_matricula = new pedido_resultado_matricula();
                                $novo_resultado_matricula->id_pedido_resultado = $novo_resultado->id_pedido_resultado;
                                if ($busca_matricula = $matricula->where('codigo_matricula', $codigo_transcricao)->where('id_serventia', $pedido->pedido_pessoa_atual->id_pessoa)->first()) {
                                    $novo_resultado_matricula->id_matricula = $busca_matricula->id_matricula;
                                } else {
                                    $novo_resultado_matricula->codigo_matricula = $codigo_transcricao;
                                    $novo_resultado_matricula->in_transcricao = 'S';
                                }
                                if (!$novo_resultado_matricula->save()) {
                                    $erro_loop = true;
                                }
                            }
                        }
						if ($erro_loop) {
							$erro = 10105;
						}
					}

					$pedido->where('id_pedido',$pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_FINALIZADO]);

					$novo_historico = new historico_pedido();
					$novo_historico->id_pedido = $pedido->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_FINALIZADO;
					$novo_historico->de_observacao = 'Resultado enviado! Resultado '.($request->in_positivo=='S'?'positivo. ':'negativo. ').$de_historico;
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_historico->save()) {
						$erro = 10106;
					}
				} else {
					$erro = 10102;
				}
			}
		} else {
			$erro = 10101;
		}
		// Tratamento do retorno
		if ($erro > 0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro ' . $erro));
		} else {
			$pedido->inserir_notificacao($this->id_pessoa,$pedido->id_pessoa_origem,'Resultado da pesquisa protocolo '.$pedido->protocolo_pedido.' foi inserido.');

            if ($pedido->pessoa_origem->no_email_pessoa != '') {
                $url_email = URL::to('/servicos/pesquisa-eletronica');
                if (in_array($pedido->pessoa_origem->id_tipo_pessoa, array(7, 11))) {
                    if ($pedido->usuario->usuario_pessoa[0]->in_master_recebe_notificacoes_sistema == 'S') {
                        Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
                            $mail->to($pedido->pessoa_origem->no_email_pessoa, $pedido->pessoa_origem->no_pessoa)
                                 ->subject('CERI - Nova notificação do pedido de pesquisa protocolo ' . $pedido->protocolo_pedido);
                        });
                    }
                    if ($pedido->usuario->usuario_pessoa[0]->in_usuario_recebe_notificacoes_sistema == 'S'){
                        Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
                            $mail->to($pedido->usuario->email_usuario, $pedido->usuario->no_usuario)
                                 ->subject('CERI - Nova notificação do pedido de pesquisa protocolo ' . $pedido->protocolo_pedido);
                        });
                    }
                } else {
                    Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
                        $mail->to($pedido->pessoa_origem->no_email_pessoa, $pedido->pessoa_origem->no_pessoa)
                             ->subject('CERI - Nova notificação do pedido de pesquisa protocolo ' . $pedido->protocolo_pedido);
                    });
                }
            }

			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O resultado foi inserido com sucesso.'));
		}
	}

	public function imprimir_resultado(Request $request, pedido $pedido) {
        if ($request->id_pedido>0) {
            $pedido = $pedido->find($request->id_pedido);

            return view('servicos.pesquisa.geral-pesquisa-imprimir-resultado',compact('pedido'));
        }
    }
    public function render_resultado($id_pedido, pedido $pedido) {
        if ($id_pedido>0) {
            $pedido = $pedido->find($id_pedido);
            
            $titulo = 'Resultado da Pesquisa: '.$pedido->protocolo_pedido;
            $pdf = PDF::loadView('pdf.pesquisa-resultado', compact('pedido', 'titulo'));   
            
            return $pdf->stream();
        }
    }

    public function gerar_recibo_pesquisa(Request $request, pedido $pedido)
    {
        if($request->id_pedido>0) {
            $pedido = $pedido->find($request->id_pedido);

            return response()->json(['view'=>view('servicos.pesquisa.geral-pesquisa-recibo', compact('pedido'))->render()]);
        }
    }

    public function imprimir_recibo_pesquisa($id_pedido, pedido $pedido){
        if ($id_pedido>0){
            $pedido = $pedido->find($id_pedido);
            $titulo = 'Recibo de Pesquisa eletrônica: ' .$pedido->protocolo_pedido;
            $class = $this;
            $pdf = PDF::loadView('pdf.pesquisa-recibo', compact('pedido','titulo', 'class'));
            return $pdf->stream();
        }
    }

	public function relatorio_pesquisas(Request $request, pesquisa $pesquisa) {
       $todas_pesquisas = $pesquisa->join('pedido','pedido.id_pedido','=','pesquisa.id_pedido')
            ->join('produto','produto.id_produto','=','pedido.id_produto')
            ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO);

       if($request->ids_pesquisas>0) {
            $todas_pesquisas->whereIn('pedido.id_pedido', $request->ids_pesquisas);
        }

        $todas_pesquisas = $todas_pesquisas->orderBy('pedido.dt_cadastro','desc')->get();

        return view('servicos.pesquisa.geral-pesquisa-relatorio',compact('todas_pesquisas'));
    }

	public function salvar_relatorio_pesquisas(Request $request, pesquisa $pesquisa) {
        $todas_pesquisas = $pesquisa->join('pedido','pedido.id_pedido','=','pesquisa.id_pedido')
            ->join('produto','produto.id_produto','=','pedido.id_produto')
            ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO);


        $todas_pesquisas->whereIn('pedido.id_pedido', $request->id_pesquisa);
        $todas_pesquisas = $todas_pesquisas->orderBy('pedido.dt_cadastro','desc')->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('pesquisa_eletronica'.$time, function($excel) use ($todas_pesquisas) {

            $excel->sheet('Pesquisa eletrônica', function($sheet) use ($todas_pesquisas) {

                $sheet->loadView('xls.relatorio-pesquisa', array('todas_pesquisas' => $todas_pesquisas));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->setAutoSize(true);
            });

        })->export('xls');
    }

    public function observacoes(Request $request, pesquisa $pesquisa){
        if($request->id_pesquisa > 0){
            $pesquisa = $pesquisa->find($request->id_pesquisa);

            $pessoa_ativa = Session::get('pessoa_ativa');

            $class = $this;
            $observacao_token = str_random(30);

            return view('servicos.pesquisa.geral-pesquisa-observacoes', compact('pesquisa', 'pessoa_ativa', 'class', 'observacao_token'));
        }
    }
    public function remover_arquivos_sessao(Request $request){
        if($request->session()->forget('arquivos_' . $request->observacao_token) == null){
            return response()->json(array('status' => 'sucesso',
                'recarrega' => 'false'
            ));
        } else {
            return response()->json(array('status' => 'erro',
                'recarrega' => 'true',
                'msg' => "Não foi possível remover o(s) arquivo(s)",
            ));
        }
    }
    public function inserir_observacao(Request $request, pesquisa $pesquisa) {
        if ($request->id_pesquisa>0) {
            $pesquisa = $pesquisa->find($request->id_pesquisa);
            $erro = 0;

            switch ($this->id_tipo_pessoa) {
                case 2: case 10:
                $id_pessoa_dest = $pesquisa->pedido->pessoa_origem->id_pessoa;
                break;
                default:
                    $id_pessoa_dest = $pesquisa->pedido->pedido_pessoa_atual->pessoa->id_pessoa;
                    break;
            }

            DB::beginTransaction();

            $nova_observacao = new pesquisa_observacao();
            $nova_observacao->id_pesquisa = $pesquisa->id_pesquisa;
            $nova_observacao->id_pessoa = $this->id_pessoa;
            $nova_observacao->id_pessoa_dest = $id_pessoa_dest;
            $nova_observacao->de_observacao = $request->de_observacao;
            $nova_observacao->id_usuario_cad = Auth::User()->id_usuario;
            $nova_observacao->dt_cadastro = Carbon::now();

            if ($nova_observacao->save()) {
                // ***** CODIGO INSERIR ARQUIVO ******
                if ($request->session()->has('arquivos_'.$request->observacao_token)) {
                    $destino = '/observacoes-pesquisa/'.$nova_observacao->id_pesquisa;
                    $arquivos = $request->session()->get('arquivos_'.$request->observacao_token);

                    Storage::makeDirectory('/public'.$destino);

                    $erro_loop_arq = false;
                    $erro_loop_sql = false;
                    foreach ($arquivos as $key => $arquivo) {
                        $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                        $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                        $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                        $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                        $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                        $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                        $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                        $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                        $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                        $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                        $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                        if ($arquivo['dt_assinado']!='') {
                            $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                        }
                        if ($arquivo['id_usuario_certificado']>0) {
                            $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                        }
                        if (!empty($arquivo['no_arquivo_p7s'])) {
                            $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                            $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                        }
                        if ($novo_arquivo_grupo_produto->save()) {
                            if (Storage::exists($origem_arquivo)) {
                                if (Storage::exists($destino_arquivo)) {
                                    Storage::delete($destino_arquivo);
                                }
                                if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                    if (count($arquivo['no_arquivos_originais'])>0) {
                                        foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                            $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                            $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                            $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                            $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                            $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                            $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                            $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                            $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                            $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                            $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                            if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                if (Storage::exists($origem_arquivo_original)) {
                                                    if (Storage::exists($destino_arquivo_original)) {
                                                        Storage::delete($destino_arquivo_original);
                                                    }
                                                    if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_arq = true;
                                                }
                                            } else {
                                                $erro_loop_sql = true;
                                            }
                                        }
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_arq = true;
                            }
                        } else {
                            $erro_loop_sql = true;
                        }
                        $nova_observacao->pesquisa_observacao_arquivo_grupo()->attach($novo_arquivo_grupo_produto);
                    }
                    if ($erro_loop_arq) {
                        $erro = 1;
                    }
                    if ($erro_loop_sql) {
                        $erro = 1;
                    }

                    $request->session()->forget('arquivos_' . $request->observacao_token);
                }
                // ***** FIM CODIGO INSERIR ARQUIVO *****
            }

            if ($erro>0) {
                DB::rollback();
                return response()->json(array('status'=> 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status'=> 'sucesso',
                    'recarrega' => 'false',
                    'msg' => 'A observação foi inserida com sucesso.',
                    'observacao'=>$nova_observacao,
                    'pessoa'=>$nova_observacao->pessoa->no_pessoa,
                    'usuario'=>$nova_observacao->usuario_cad->no_usuario,
                    'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'),
                    'arquivo' => $nova_observacao->pesquisa_observacao_arquivo_grupo
                ));
            }
        }
    }
    public function status_observacao(Request $request, pesquisa_observacao $pesquisa_observacao) {
        $pessoa_ativa = Session::get('pessoa_ativa');

        if ($request->id_pesquisa_observacao){
            $pesquisa_observacao = $pesquisa_observacao->where('id_pesquisa_observacao',$request->id_pesquisa_observacao)
                                                       ->where('id_pessoa_dest',$pessoa_ativa->id_pessoa);
            if ($request->status == 'S'){
                $pesquisa_observacao->where('in_leitura','N')
                    ->update(['in_leitura'=>'S']);
                $status = 'lida';
            } else {
                $pesquisa_observacao->where('in_leitura','S')
                    ->update(['in_leitura'=>'N']);
                $status = 'não lida';
            }

            return response()->json(array('status'=>'sucesso',
                'recarrega'=>'false',
                'msg'=>'Observação marcada como '.$status.'.',
                'recarrega' => 'true'
            ));
        } else {
            return response()->json(array('status'=>'erro',
                'recarrega'=>'false',
                'msg'=>'Não foi possível alterar o status do registro.',
                'recarrega' => 'true'
            ));
        }
    }

    public function detalhes_solicitante(Request $request, usuario $usuario) {
        if ($request->id_usuario>0) {
            $usuario = $usuario->find($request->id_usuario);
            return view('servicos.pesquisa.serventia-pesquisa-detalhes-solicitante',compact('usuario'));
        }
    }
    
    public function protocolar(Request $request, pedido $pedido) {
        if ($request->id_pedido > 0) {
            $pedido = $pedido->find($request->id_pedido);
            return view('servicos.pesquisa.serventia-pesquisa-protocolar',compact('pedido'));
        }
    }

    public function inserir_protocolo(Request $request, pedido $pedido){
        $erro = 0;

        DB::beginTransaction();

        if ($request->id_pedido > 0){
            $pedido = $pedido->find($request->id_pedido);
            $pedido->nu_protocolo_legado = $request->nu_protocolo_legado;
            $pedido->dt_protocolo_legado = Carbon::now();
            if($pedido->save()){
                $historico_pedido = new historico_pedido();
                $historico_pedido->id_pedido = $request->id_pedido;
                $historico_pedido->id_situacao_pedido_grupo_produto = 72;
                $historico_pedido->de_observacao = 'Protocolo interno inserido.';
                $historico_pedido->id_usuario_cad = Auth::User()->id_usuario;
                if(!$historico_pedido->save()){
                    $erro = 2;
                }
            } else {
                $erro = 1;
            }
            if ($erro>0){
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor tente mais tarde erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'Protocolo interno inserido com sucesso'));
            }
        }
    }

    public function alterar_protocolo(Request $request, pedido $pedido){
        $erro = 0;

        DB::beginTransaction();

        if($request->id_pedido > 0){
            $alterar_protocolo = $pedido->where('pedido.id_pedido', '=', $request->id_pedido)
                                        ->first();
            $alterar_protocolo->nu_protocolo_legado = $request->nu_protocolo_legado;
            $alterar_protocolo->dt_protocolo_legado = Carbon::now();
            if($alterar_protocolo->save()){
                $historico_pedido = new historico_pedido();
                $historico_pedido->id_pedido = $request->id_pedido;
                $historico_pedido->id_situacao_pedido_grupo_produto = 72;
                $historico_pedido->de_observacao = 'Protocolo interno alterado.';
                $historico_pedido->id_usuario_cad = Auth::User()->id_usuario;
                if(!$historico_pedido->save()){
                    $erro = 2;
                }
            } else {
                $erro = 1;
            }
            if($erro > 0){
                DB::roolback();
                return response()->json(array('status'=>'erro',
                    'recarrega'=>'false',
                    'msg' => 'Por favor tente mais tarde erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status'=>'sucesso',
                    'recarrega'=>'true',
                    'msg'=>'Protocolo alterado com sucesso.'));
            }
        }
    }
}
