<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;
use Mail;
use Excel;

use App\pessoa;
use App\usuario;
use App\alcada;
use App\estado;
use App\produto;
use App\certidao;
use App\serventia;
use App\pedido;
use App\pedido_pessoa;
use App\preco_produto_item;
use App\pedido_pessoa_resposta;
use App\situacao_pedido_grupo_produto;
use App\processo;
use App\matricula;
use App\tipo_certidao;
use App\tipo_resposta;
use App\tipo_certidao_chave_pesquisa;
use App\periodo_certidao;
use App\pedido_produto_item;
use App\historico_pedido;
use App\pedido_valor;
use App\pedido_resultado;
use App\pedido_resultado_matricula;
use App\tipo_custa;
use App\arquivo_grupo_produto;
use App\arquivo_grupo_produto_composicao;
use App\produto_item;
use App\certidao_observacao;
use App\pessoa_telefone;

class CertidaoController extends Controller { 

	const ID_GRUPO_PRODUTO = 2;
	const ID_SITUACAO_CADASTRADO = 12;
	const ID_SITUACAO_CANCELADO = 13;
	const ID_SITUACAO_ANALISE = 14;
	const ID_SITUACAO_APROVACAO = 15;
	const ID_SITUACAO_ENCAMINHADO = 16;
	const ID_SITUACAO_FINALIZADO = 17;
	const ID_SITUACAO_RECUSADO = 20;
	const ID_SITUACAO_CORRECAO = 19;
    const ID_TIPO_ARQUIVO_REQUERIMENO_CERTIDAO = 24;

	public function __construct() {
		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, certidao $certidao, estado $estado, produto $produto, serventia $serventia, situacao_pedido_grupo_produto $situacao, tipo_certidao_chave_pesquisa $tipo_chave){
		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela principal de certidão','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

		$produtos = $produto->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
							->where('in_disponivel_usuario','S')
							->orderBy('nu_ordem','asc')
							->get();

		$situacoes = $situacao->where('in_registro_ativo','S')
							  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
                              ->whereNotIn('id_situacao_pedido_grupo_produto',[75])//status: Cancelado sistema
							  ->orderBy('nu_ordem')
							  ->get();

		$tipos_chave_filtro = $tipo_chave->where('in_registro_ativo','S')
										 ->where('in_exibe_filtro','S')
										 ->orderBy('nu_ordem','asc')
										 ->get();

		$class = $this;
        $cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

		switch ($this->id_tipo_pessoa) {
			case 3: case 4: case 5: case 11: case 7: case 6:
				$certidoes_pendentes = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
													  ->join('produto','produto.id_produto','=','pedido.id_produto')
													  ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
													  ->where('pedido.id_pessoa_origem','=',$this->id_pessoa)
													  ->where('pedido.id_situacao_pedido_grupo_produto',$this::ID_SITUACAO_CADASTRADO)
													  ->orderBy('certidao.dt_cadastro','desc')->get();
				
				$certidoes_a_confirmar = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
														->join('produto','produto.id_produto','=','pedido.id_produto')
														->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
														->where('pedido.id_pessoa_origem','=',$this->id_pessoa)
														->where('pedido.id_situacao_pedido_grupo_produto',$this::ID_SITUACAO_APROVACAO)
														->orderBy('certidao.dt_cadastro','desc')->get();

				$todas_certidoes = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
												  ->join('produto','produto.id_produto','=','pedido.id_produto')
												  ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
												  ->where('pedido.id_pessoa_origem','=',$this->id_pessoa)
												  ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_APROVACAO));


					if ($request->id_produto>0) {
						$todas_certidoes->where('pedido.id_produto',$request->id_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_certidoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_certidoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->in_penhora!='') {
						$todas_certidoes->where('certidao.in_penhora',$request->in_penhora);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_certidoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_tipo_certidao_chave_pesquisa>0){
						$todas_certidoes->where('certidao.id_tipo_certidao_chave_pesquisa',$request->id_tipo_certidao_chave_pesquisa);
						$todas_certidoes->where('certidao.de_chave_certidao',$request->de_chave_pesquisa);
					}
                    if(($request->id_serventia) && (!empty($request->id_serventia))){
                        $todas_certidoes->join('pedido_pessoa', 'pedido_pessoa.id_pedido','=','pedido.id_pedido')
                                        ->join('pessoa', 'pedido_pessoa.id_pessoa', '=', 'pessoa.id_pessoa')
                                        ->join('serventia', 'serventia.id_pessoa','=','pessoa.id_pessoa')
                                        ->where('serventia.id_serventia',$request->id_serventia);
                    }
                    if((!is_null($request->id_cidade)) && (!empty($request->id_cidade))){
                        $todas_certidoes->join ('pedido_pessoa as pp','pp.id_pedido', '=','pedido.id_pedido')
                                        ->join ('pessoa as p','p.id_pessoa','=','pp.id_pessoa')
                                        ->join ('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'p.id_pessoa')
                                        ->join ('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                                        ->where('endereco.id_cidade',$request->id_cidade);
                    }
                    if (in_array($this->id_tipo_pessoa,[6,11])) {
                        if (count(Auth::User()->usuario_pessoa[0]->pessoa->unidade_judiciaria_divisao)>0){
                           if ((Auth::User()->usuario_pessoa[0]->pessoa->unidade_judiciaria_divisao->in_compartilha_servico == 'N') && (Auth::User()->in_usuario_master == 'N')) {
                               $todas_certidoes->where('pedido.id_usuario','=', Auth::User()->id_usuario);
                           }
                        }
                    }

				$todas_certidoes = $todas_certidoes->orderBy('pedido.dt_cadastro',($request->ord?$request->ord:'desc'))->paginate(10, ['*'], 'certidao-pag');

                $todas_certidoes->appends(Request::capture()->except('_token'))->render();
				
		    	return view('servicos.certidao.geral-certidao',compact('class','request','cidades','produtos','situacoes','tipos_chave','tipos','periodos','certidoes_pendentes','certidoes_a_confirmar','todas_certidoes', 'tipos_chave_filtro'));
				break;
			case 2:
				$todas_certidoes = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
												  ->join('produto','produto.id_produto','=','pedido.id_produto')
												  ->join('situacao_pedido_grupo_produto','situacao_pedido_grupo_produto.id_situacao_pedido_grupo_produto','=','pedido.id_situacao_pedido_grupo_produto')
												  ->join('pedido_pessoa',function($join) {
													  $join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
														   ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
											  	  })
												  ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
												  ->where('situacao_pedido_grupo_produto.in_serventia','=','S');
												  //->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_CANCELADO));

					if ($request->id_produto>0) {
						$todas_certidoes->where('pedido.id_produto',$request->id_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_certidoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_certidoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->in_penhora!='') {
						$todas_certidoes->where('certidao.in_penhora',$request->in_penhora);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_certidoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_tipo_certidao_chave_pesquisa>0){
						$todas_certidoes->where('certidao.id_tipo_certidao_chave_pesquisa',$request->id_tipo_certidao_chave_pesquisa);
						$todas_certidoes->where('certidao.de_chave_certidao',$request->de_chave_pesquisa);
					}
				
				$todas_certidoes = $todas_certidoes->orderBy('pedido.dt_cadastro',($request->ord?$request->ord:'desc'))->paginate(10, ['*'], 'certidao-pag');

                $todas_certidoes->appends(Request::capture()->except('_token'))->render();
					
		    	return view('servicos.certidao.serventia-certidao',compact('class','request','produtos','situacoes','todas_certidoes', 'tipos_chave_filtro'));
				break;
			case 13: case 9:
				$todas_certidoes = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
												  ->join('produto','produto.id_produto','=','pedido.id_produto')
												  ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
												  ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_APROVACAO));

					if ($request->id_produto>0) {
						$todas_certidoes->where('pedido.id_produto',$request->id_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_certidoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_certidoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->in_penhora!='') {
						$todas_certidoes->where('certidao.in_penhora',$request->in_penhora);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_certidoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_tipo_certidao_chave_pesquisa>0){
						$todas_certidoes->where('certidao.id_tipo_certidao_chave_pesquisa',$request->id_tipo_certidao_chave_pesquisa);
						$todas_certidoes->where('certidao.de_chave_certidao',$request->de_chave_pesquisa);
					}
                    if((!is_null($request->id_serventia)) && (!empty($request->id_serventia))){
                        $todas_certidoes->join('pedido_pessoa', 'pedido_pessoa.id_pedido','=','pedido.id_pedido')
                            ->join('pessoa', 'pedido_pessoa.id_pessoa', '=', 'pessoa.id_pessoa')
                            ->join('serventia', 'serventia.id_pessoa','=','pessoa.id_pessoa')
                            ->where('serventia.id_serventia',$request->id_serventia);
                    }
                    if((!is_null($request->id_cidade)) && (!empty($request->id_cidade))){
                        $todas_certidoes->join ('pedido_pessoa as pp','pp.id_pedido', '=','pedido.id_pedido')
                            ->join ('pessoa as p','p.id_pessoa','=','pp.id_pessoa')
                            ->join ('pessoa_endereco', 'pessoa_endereco.id_pessoa', '=', 'p.id_pessoa')
                            ->join ('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco' )
                            ->where('endereco.id_cidade',$request->id_cidade);
                    }

				$todas_certidoes = $todas_certidoes->orderBy('pedido.dt_cadastro',($request->ord?$request->ord:'desc'))->paginate(10, ['*'], 'certidao-pag');

                $todas_certidoes->appends(Request::capture()->except('_token'))->render();
				
		    	return view('servicos.certidao.geral-certidao',compact('class','request','cidades','produtos','situacoes','tipos_chave','tipos','periodos','certidoes_pendentes','certidoes_a_confirmar','todas_certidoes', 'tipos_chave_filtro'));
				break;
		}
    }

    public function nova(Request $request, estado $estado, produto $produto, tipo_certidao_chave_pesquisa $tipo_chave, matricula $matricula, pedido_resultado_matricula $pedido_resultado_matricula, tipo_custa $tipo_custa) {
    	$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
    	$serventias = $estado->find(env('ID_ESTADO'))->serventias_estado()->orderBy('serventia.no_serventia')->get();

		$produtos = $produto->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
							->where('dt_ini_vigencia','<',Carbon::now())
							->where('in_disponivel_usuario','S')
							->orderBy('nu_ordem','asc')
							->get();

		$tipos_chave = $tipo_chave->where('in_registro_ativo','S')
								  ->orderBy('nu_ordem','asc')
								  ->get();
									 
		if ($request->id_pedido_resultado_matricula!='') {
			$pedido_resultado_matricula = $pedido_resultado_matricula->find($request->id_pedido_resultado_matricula);
		} else {
			$pedido_resultado_matricula = NULL;
		}

		if ($this->id_tipo_pessoa!=3) {
			$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();
		} else {
			$tipos_custa = array();
		}

		$class = $this;
		$certidao_token = str_random(30);

		return view('servicos.certidao.geral-certidao-nova',compact('class','request','cidades','serventias','produtos','tipos_chave','periodos','pedido_resultado_matricula','tipos_custa','certidao_token'));
    }

	public function inserir(Request $request) {
		$erro = 0;
		$certidoes = array();

		DB::beginTransaction();

		$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$request->id_produto.");"));

		$novo_pedido = new pedido();
		$novo_pedido->id_usuario = Auth::User()->id_usuario;
		$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
		$novo_pedido->id_produto = $request->id_produto;
		$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
		$novo_pedido->dt_pedido = Carbon::now();
		$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
		$novo_pedido->dt_cadastro = Carbon::now();
		$novo_pedido->id_pessoa_origem = $this->id_pessoa;
	
		if ($novo_pedido->save()) {
			$serventia_selecionada = new serventia();

			if($request->id_pedido_resultado_matricula>0) {
				$pedido_resultado_matricula = new pedido_resultado_matricula();
				$pedido_resultado_matricula = $pedido_resultado_matricula->find($request->id_pedido_resultado_matricula);

				$serventia_selecionada = $pedido_resultado_matricula->pedido_resultado->pedido->pedido_pessoa_atual->pessoa->serventia;
			} else {
				$serventia_selecionada = $serventia_selecionada->find($request->id_serventia);
			}

			$novo_pedido->pessoas()->attach($serventia_selecionada->pessoa);		
		
			if (count($novo_pedido->produto->produto_itens)>1) {
				$id_produto_item = $request->id_produto_item;
			} else {
				$id_produto_item = $novo_pedido->produto->produto_itens[0]->id_produto_item;
			}

			$novo_pedido_produto_item = new pedido_produto_item();
			$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
			$novo_pedido_produto_item->id_produto_item = $id_produto_item;
			$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
			if (!$novo_pedido_produto_item->save()) {
				$erro = 20003;
			}
		
			$nova_certidao = new certidao();
			$nova_certidao->id_pedido = $novo_pedido->id_pedido;
			$nova_certidao->id_tipo_certidao = $request->id_tipo_certidao;
			$nova_certidao->id_tipo_certidao_chave_pesquisa	= $request->id_tipo_certidao_chave_pesquisa;
			$nova_certidao->de_chave_certidao = $request->de_chave_certidao;
			$nova_certidao->in_pesquisa = 'N';
			$nova_certidao->in_penhora = 'N';
			$nova_certidao->in_indisponibilidade = 'N';
			$nova_certidao->dt_cadastro = Carbon::now();
			$nova_certidao->dt_geracao = Carbon::now();
			$nova_certidao->dt_liberacao = Carbon::now();
			$nova_certidao->dt_indisponibilizacao  = Carbon::now()->addDays(30);
			$nova_certidao->nu_dia_disponibilizacao  = 30;
			$nova_certidao->id_periodo_certidao = $request->id_periodo_certidao;
			$nova_certidao->id_tipo_custa = ($request->id_tipo_custa>0?$request->id_tipo_custa:2);
            $nova_certidao->ds_observacao = $request->ds_observacao;
            $nova_certidao->id_pedido_resultado_matricula = $request->id_pedido_resultado_matricula?:NULL;
            $nova_certidao->de_chave_complementar = trim($request->de_chave_complementar)?:NULL;

			switch ($request->id_tipo_custa) {
				case 1:
					$novo_pedido->va_pedido = 0;

					$processo = new processo();
					$processo = $processo->where('numero_processo',$request->numero_processo)->first();

					if ($processo) {
						$nova_certidao->id_processo	= $processo->id_processo;
					} else {
						$novo_processo = new processo();
						$novo_processo->numero_processo = $request->numero_processo;
						$novo_processo->id_usuario_cad = Auth::User()->id_usuario;
						$novo_processo->dt_cadastro = Carbon::now();
						if($novo_processo->save()) {
							$nova_certidao->id_processo = $novo_processo->id_processo;
						} else {
							$erro = 20021;
						}
					}
					break;
				case 3:
					$novo_pedido->va_pedido = 0;
					break;
				default:
					$produto_item = new produto_item();
					$produto_item = $produto_item->find($id_produto_item);
                    $va_desconto = $this->desconto($request->certidao_token, $request->id_pedido_resultado_matricula);
					$novo_pedido->va_pedido = $produto_item->preco_total($serventia_selecionada->id_pessoa) - $va_desconto;
					break;
			}
			if (!$novo_pedido->save()) {
				$erro = 20020;
			}
		
			if ($nova_certidao->save()) {
				$novo_historico = new historico_pedido();
				$novo_historico->id_pedido = $novo_pedido->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
				$novo_historico->de_observacao = 'Pedido de certidão inserido com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				if (!$novo_historico->save()) {
					$erro = 20004;
				}

				if ($request->session()->has('arquivos_'.$request->certidao_token)) {
					$destino = '/certidoes/'.$nova_certidao->id_certidao;
					$arquivos = $request->session()->get('arquivos_'.$request->certidao_token);
					$erro_loop_arq = false;
					$erro_loop_sql = false;
					foreach ($arquivos as $key => $arquivo) {
						$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
						Storage::makeDirectory('/public'.$destino_final);

						$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
						$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];

						$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
						$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
						$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
						$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
						$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
						$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
						$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
						$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
						$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
						$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
						if ($arquivo['dt_assinado']!='') {
							$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
						}
						if ($arquivo['id_usuario_certificado']>0) {
							$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
						}
						if (!empty($arquivo['no_arquivo_p7s'])) {
							$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
							$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
						}
						if ($novo_arquivo_grupo_produto->save()) {
							if (Storage::exists($origem_arquivo)) {
								if (Storage::exists($destino_arquivo)) {
									Storage::delete($destino_arquivo);
								}
								if (Storage::copy($origem_arquivo,$destino_arquivo)) {
									if (count($arquivo['no_arquivos_originais'])>0) {
										foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
											$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
											$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

											$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
											$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
											$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
											$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
											$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
											$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
											$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
											$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
											if ($novo_arquivo_grupo_produto_composicao->save()) {
												if (Storage::exists($origem_arquivo_original)) {
													if (Storage::exists($destino_arquivo_original)) {
														Storage::delete($destino_arquivo_original);
													}
													if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_arq = true;
												}
											} else {
												$erro_loop_sql = true;
											}
										}
									}
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_arq = true;
							}
						} else {
							$erro_loop_sql = true;
						}
						$nova_certidao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
                        $request->session()->put('certidao_'.$request->certidao_token,$nova_certidao);
					}
					if ($erro_loop_arq) {
						$erro = 20005;
					}
					if ($erro_loop_sql) {
						$erro = 20006;
					}
				}

				$certidoes[] = array('pedido'=>$novo_pedido,'certidao'=>$nova_certidao,'vl_pedido'=>$novo_pedido->va_pedido,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'),'dt_validade'=>Carbon::now()->addDays(30)->format('d/m/Y'),'no_pessoa'=>$serventia_selecionada->pessoa->no_pessoa);
			} else {
				$erro = 20002;
			}
		} else {
			$erro = 20001;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'false',
										  'msg' => 'As certidões foram inseridas com sucesso.',
										  'certidoes'=>$certidoes));
		}
	}
	
	public function enviar_pedidos(Request $request) {
		$erro = 0;
        $v_in_desconto = 'N';
        $v_id_pedido = null;

		DB::beginTransaction();

		if (count($request->id_pedido)>0) {
			$valor_total = 0;

			if(!is_array($request->id_pedido)) {
                $v_id_pedido = $request->id_pedido;
				$replace['id_pedido'] = array($request->id_pedido);
				$request->replace($replace);
			}

			foreach ($request->id_pedido as $id_pedido) {
				$pedido_selecionado = new pedido();
				$pedido_selecionado = $pedido_selecionado->find($id_pedido);

				if ($pedido_selecionado->certidao->id_tipo_custa==2) {
					$produto_item = new produto_item();
					$produto_item = $produto_item->find($pedido_selecionado->produto_itens[0]->id_produto_item);

					$pedido_resultado_matricula = new pedido_resultado_matricula();
                    $pedido_resultado_matricula = $pedido_resultado_matricula->find($pedido_selecionado->certidao->id_pedido_resultado_matricula);
                    $va_desconto = isset($pedido_resultado_matricula)?$pedido_resultado_matricula->desconto()==0?$pedido_resultado_matricula->pedido_resultado->pedido->va_pedido:0:0;

                    $valor = $produto_item->preco_total($pedido_selecionado->pedido_pessoa_atual->id_pessoa) - $va_desconto;

                    $this->desconto_aplicado($pedido_resultado_matricula);
                    $v_in_desconto = $va_desconto>0?'S':'N';
				} else {
					$valor = 0;
				}

				if ($valor==$pedido_selecionado->va_pedido) {
					if ($pedido_selecionado->where('id_pedido',$id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_ENCAMINHADO])) {
						$novo_historico = new historico_pedido();
						$novo_historico->id_pedido = $id_pedido;
						$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_ENCAMINHADO;
						$novo_historico->de_observacao = 'Pedido enviado com sucesso.';
						$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
						if (!$novo_historico->save()) {
							$erro = 20017;
						}

						$pedido_selecionado->inserir_notificacao($this->id_pessoa,$pedido_selecionado->pedido_pessoa_atual->id_pessoa,'Uma nova certidão foi enviada. Protocolo '.$pedido_selecionado->protocolo_pedido);

                        if (in_array($pedido_selecionado->certidao->id_tipo_custa,[1,2])){
	                        $certidao = $pedido_selecionado->certidao;
	                        $destino = '/certidoes/'.$certidao->id_certidao.'/'.$this::ID_TIPO_ARQUIVO_REQUERIMENO_CERTIDAO;
	                        $no_arquivo = 'requerimento_'.$certidao->pedido->protocolo_pedido.'.pdf';
                            $destino_arquivo = '/public/'.$destino.'/'.$no_arquivo;
	                        $opcao = 'certidao';

	                        Storage::makeDirectory('/public'.$destino);

	                        $pdf = PDF::loadView('pdf.requerimento', compact('certidao', 'opcao'));

	                        if (Storage::exists($destino_arquivo)) {
	                            Storage::delete($destino_arquivo);
	                        }
	                        if ($pdf->save(storage_path('app' . $destino_arquivo))) {
	                            $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
	                            $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
	                            $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $this::ID_TIPO_ARQUIVO_REQUERIMENO_CERTIDAO;
	                            $novo_arquivo_grupo_produto->no_arquivo = $no_arquivo;
	                            $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
	                            $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
	                            $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($no_arquivo);
	                            $novo_arquivo_grupo_produto->in_ass_digital = 'N';
	                            $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($destino_arquivo);
	                            $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$destino_arquivo));
	                            if($novo_arquivo_grupo_produto->save()){
	                                $certidao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
	                            }
	                        }
	                    }
                        /**
                         * Fim criação certidão
                         */
					} else {
						$erro = 20018;
					}

					$valor_total += $valor;
				} else {
					$erro = 20015;
				}
			}

			if (converte_float(Auth::User()->saldo_usuario())<$valor_total) {
				$erro = 20016;
                $this->desconto_aplicado($pedido_resultado_matricula, false);
			} else {
				foreach ($request->id_pedido as $id_pedido) {
					$pedido_selecionado = new pedido();
					$pedido_selecionado = $pedido_selecionado->find($id_pedido);

					if ($pedido_selecionado->certidao->id_tipo_custa==2) {

					    $args = [
					        'va_pedido'                     => $pedido_selecionado->va_pedido,
                            "tp_movimentacao_financeira"    => 2,
                            "tp_movimentacao"               => 'S',
                            "id_forma_pagamento"            => 6,
                            "v_id_pedido_desconto"          => isset($pedido_resultado_matricula)?$pedido_resultado_matricula->pedido_resultado->pedido->pesquisa->id_pedido:null,
                            "v_va_desconto"                 => $va_desconto,
                            "v_in_desconto"                 => $v_in_desconto,
                            "v_va_desconto_perc"            => $va_desconto>0?100:null,
                            "v_id_compra_credito"           => $request->id_compra_credito
                        ];

						$pedido_selecionado->registrar_valor_pedido($args);
					}
				}
			}
		} else {
			$erro = 20019;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			switch ($erro) {
				case 20015:
					return response()->json(array('status'=> 'alerta_valor',
												  'tipo' => 'valor_diferente',
												  'recarrega' => 'false',
												  'msg' => 'Uma ou mais certidões estão com o valor diferente do atual. Deseja atualizar os valores?'));
					break;
				case 20016:
					return response()->json(array('status'=> 'alerta_valor',
												  'tipo' => 'saldo_insuficiente',
												  'recarrega' => 'false',
												  'msg' => 'O saldo do usuário é insuficiente para enviar os pedidos de certidões. Deseja adquirir novos créditos?',
                                                  'v_id_pedido' => $v_id_pedido));
					break;
				default:
					return response()->json(array('status'=> 'erro',
												  'recarrega' => 'false',
												  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
					break;
			}
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => (count($request->id_pedido)>1?'Os pedidos de certidões foram enviados com sucesso.':'O pedido de certidão foi enviado com sucesso.')));
		}
	}

	public function atualizar_valores(Request $request) {
		$erro = 0;

		DB::beginTransaction();

		if (!is_array($request->id_pedido)) {
			$request->id_pedido = array($request->id_pedido);
		}

		if (count($request->id_pedido)>0) {
			$retorno_pedidos = array();

			foreach ($request->id_pedido as $id_pedido) {
				$pedido = new pedido();
				$pedido = $pedido->find($id_pedido);

				$produto_item = new produto_item();
				$produto_item = $produto_item->find($pedido->produto_itens[0]->id_produto_item);
				$novo_valor = $produto_item->preco_total($pedido->pedido_pessoa_atual->id_pessoa);

				$pedido->va_pedido = $novo_valor;

				if ($pedido->save()) {
					array_push($retorno_pedidos,array('id_pedido'=>$pedido->id_pedido,'va_pedido'=>$pedido->va_pedido));
				} else {
					$erro = 20023;
				}
			}
		} else {
			$erro = 20022;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'false',
										  'pedidos' => $retorno_pedidos,
										  'msg' => (count($request->id_pedido)>1?'Os valores das certidões foram atualizados com sucesso.':'O valor da certidão foi atualizado com sucesso.')));
		}
	}

	public function cancelar_pedido(Request $request, pedido $pedido) {
		$erro = 0;

		DB::beginTransaction();

		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
												 
			if ($pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_CANCELADO])) {
				$novo_historico = new historico_pedido();
				$novo_historico->id_pedido = $request->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CANCELADO;
				$novo_historico->de_observacao = 'Pedido cancelado com sucesso.';
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				if (!$novo_historico->save()) {
					$erro = 20012;
				}
				
			} else {
				$erro = 20013;
			}
		} else {
			$erro = 20014;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O pedido de certidão foi cancelado com sucesso.'));
		}
	}

	public function nova_resposta(Request $request, pedido $pedido, tipo_resposta $tipo_resposta, tipo_custa $tipo_custa, tipo_certidao_chave_pesquisa $tipo_chave) {
		if ($request->id_pedido>0) {
			$class = $this;
			$pedido = $pedido->find($request->id_pedido);

			if ($pedido) {
				if (!$pedido->verifica_permissao()) {
					return view('erros.401-certidao');
				}
			
				$tipos_resposta = $tipo_resposta->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();
				$tipos_chave = $tipo_chave->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();
				$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();

				$arquivos_isencao = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',20)->get();
	            $arquivos_requerimento = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',$this::ID_TIPO_ARQUIVO_REQUERIMENO_CERTIDAO)->get();

				$pedido->visualizar_notificacoes();

				$certidao_token = str_random(30);

				return view('servicos.certidao.serventia-certidao-resposta',compact('class','certidao_token','pedido','tipos_resposta','tipos_chave','tipos_custa','arquivos_isencao','arquivos_requerimento'));
			}
		}
	}
	
	public function detalhes(Request $request, pedido $pedido, produto $produto, tipo_custa $tipo_custa, tipo_certidao_chave_pesquisa $tipo_chave) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			if ($pedido) {
				if (!$pedido->verifica_permissao()) {
					return view('erros.401-certidao');
				}

				$tipos_chave = $tipo_chave->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();
				
				if ($this->id_tipo_pessoa!=3) {
					$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();
				} else {
					$tipos_custa = array();
				}

				$arquivos_isencao = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',20)->get();

				$pedido->visualizar_notificacoes();

				$class = $this;

				return view('servicos.certidao.geral-certidao-detalhes',compact('class','pedido','tipos_chave','tipos_custa','tipos','periodos','arquivos_isencao'));
			}
		}
	}
	public function resultado(Request $request, pedido $pedido, tipo_certidao_chave_pesquisa $tipo_chave, tipo_custa $tipo_custa) {
		if ($request->id_pedido>0) {
			$class = $this;
			$pedido = $pedido->find($request->id_pedido);

			if ($pedido) {
				if (!$pedido->verifica_permissao()) {
					return view('erros.401-certidao');
				}

				$tipos_chave = $tipo_chave->where('in_registro_ativo','S')->orderBy('nu_ordem','asc')->get();
				$tipos_custa = $tipo_custa->where('id_tipo_custa','<>',2)->orderBy('nu_ordem')->get();

				$arquivos_isencao = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',20)->get();
				$arquivos_resultado = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',21)->get();
				$arquivos_requerimento = $pedido->certidao->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',$this::ID_TIPO_ARQUIVO_REQUERIMENO_CERTIDAO)->get();

				$pedido->visualizar_notificacoes();

				switch ($this->id_tipo_pessoa) {
					case 3: case 4: case 5: case 11: case 6: case 7: case 13: case 9:
						return view('servicos.certidao.geral-certidao-resultado',compact('class','pedido','tipos_resposta','tipos_chave','tipos_custa','arquivos_isencao','arquivos_resultado'));
						break;
					case 2:
						return view('servicos.certidao.serventia-certidao-resultado',compact('class','pedido','tipos_resposta','tipos_chave','tipos_custa','arquivos_isencao','arquivos_resultado','arquivos_requerimento'));
						break;
				}
			}
		}
	}

	public function inserir_andamento(Request $request, pedido $pedido, pedido_pessoa $pedido_pessoa, pessoa $pessoa) {
		$erro = 0;

		DB::beginTransaction();

		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			
			if ($request->id_tipo_resposta>0) {
				$pessoa = $pessoa->find($this->id_pessoa);

				$id_pedido_pessoa = $pedido_pessoa->where('id_pedido',$request->id_pedido)
												  ->where('id_pessoa',$this->id_pessoa)
												  ->first()
												  ->id_pedido_pessoa;

				$nova_resposta = new pedido_pessoa_resposta();
				$nova_resposta->id_pedido_pessoa = $id_pedido_pessoa;
				$nova_resposta->id_tipo_resposta = $request->id_tipo_resposta;
				$nova_resposta->de_resposta = $request->de_resposta;
				$nova_resposta->dt_resposta = Carbon::now();
				$nova_resposta->id_usuario_cad = Auth::User()->id_usuario;
				$nova_resposta->dt_cadastro = Carbon::now();
				
				if ($nova_resposta->save()) {
					$novo_historico = new historico_pedido();
					$novo_historico->id_pedido = $request->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $pedido->id_situacao_pedido_grupo_produto;
					$novo_historico->de_observacao = $nova_resposta->tipo_resposta->no_tipo_resposta.': '.$request->de_resposta;
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_historico->save()) {
						$erro = 20101;
					}
				} else {
					$erro = 20102;
				}
			}
		} else {
			$erro = 20103;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'false',
										  'msg' => 'O andamento da certidão foi inserido com sucesso.',
										  'andamento' => array('historico'=>$novo_historico,'usuario'=>Auth::User()->no_usuario,'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'))));
		}
	}	

	public function inserir_resultado(Request $request, pedido $pedido, pessoa $pessoa, pedido_pessoa $pedido_pessoa) {
		$erro = 0;

		DB::beginTransaction();

		if ($request->id_pedido>0) {
			if (in_array($request->in_positivo,array('S','N'))) {
				$pedido = $pedido->find($request->id_pedido);

				$novo_resultado = new pedido_resultado();
				$novo_resultado->id_pedido = $request->id_pedido;
				$novo_resultado->in_positivo = $request->in_positivo;
				$novo_resultado->id_usuario_cad = Auth::User()->id_usuario;

				if ($request->in_positivo=='S') {
					switch ($request->tipo_resultado_envio) {
						case 1:
							$novo_resultado->de_resultado = $request->de_resultado;
							$de_historico = $request->de_resultado;
							break;
						case 2:
							if ($request->session()->has('arquivos_'.$request->certidao_token)) {
								$destino = '/certidoes/'.$pedido->certidao->id_certidao;
								$arquivos = $request->session()->get('arquivos_'.$request->certidao_token);
								$erro_loop_arq = false;
								$erro_loop_sql = false;
								$no_arquivos = array();
								foreach ($arquivos as $key => $arquivo) {
									$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
									Storage::makeDirectory('/public'.$destino_final);

									$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
									$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];

									$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
									$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
									$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
									$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
									$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
									$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
									$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
									$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
									$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
									$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
									if ($arquivo['dt_assinado']!='') {
										$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
									}
									if ($arquivo['id_usuario_certificado']>0) {
										$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
									}
									if (!empty($arquivo['no_arquivo_p7s'])) {
										$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
										$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
									}
									if ($novo_arquivo_grupo_produto->save()) {
										if (Storage::exists($origem_arquivo)) {
											if (Storage::exists($destino_arquivo)) {
												Storage::delete($destino_arquivo);
											}
											if (Storage::copy($origem_arquivo,$destino_arquivo)) {
												if (count($arquivo['no_arquivos_originais'])>0) {
													foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
														$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
														$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

														$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
														$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
														$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
														$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
														$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
														$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
														$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
														$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
														if ($novo_arquivo_grupo_produto_composicao->save()) {
															if (Storage::exists($origem_arquivo_original)) {
																if (Storage::exists($destino_arquivo_original)) {
																	Storage::delete($destino_arquivo_original);
																}
																if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
																	$erro_loop_arq = true;
																}
															} else {
																$erro_loop_arq = true;
															}
														} else {
															$erro_loop_sql = true;
														}
													}
												}
											} else {
												$erro_loop_arq = true;
											}
										} else {
											$erro_loop_arq = true;
										}
									} else {
										$erro_loop_sql = true;
									}
									$pedido->certidao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
									$no_arquivos[] = $novo_arquivo_grupo_produto->no_arquivo;
								}
								if ($erro_loop_arq) {
									$erro = 20103;
								}
								if ($erro_loop_sql) {
									$erro = 20104;
								}
							}
							$de_historico = 'Arquivo(s): '.implode(', ',$no_arquivos);
							break;
					}
				} elseif ($request->in_positivo=='N') {
					$de_historico = 'Não foram localizados registros com a chave '.$pedido->certidao->de_chave_certidao.' pesquisada nesta serventia.';
				}

				$novo_resultado->de_resposta = $de_historico;
				
				if ($novo_resultado->save()) {
					$pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_FINALIZADO]);

					$novo_historico = new historico_pedido();
					$novo_historico->id_pedido = $request->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_FINALIZADO;
					$novo_historico->de_observacao = 'Resultado enviado! Resultado '.($request->in_positivo=='S'?'positivo. ':'negativo. ').$de_historico;
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					if (!$novo_historico->save()) {
						$erro = 20105;
					}

					$id_pedido_pessoa = $pedido_pessoa->where('id_pedido',$request->id_pedido)
													  ->where('id_pessoa',$this->id_pessoa)
													  ->first()
													  ->id_pedido_pessoa;

					$novo_pedido_pessoa_resposta = new pedido_pessoa_resposta();
					$novo_pedido_pessoa_resposta->id_pedido_pessoa = $id_pedido_pessoa;
					$novo_pedido_pessoa_resposta->id_tipo_resposta = ($request->in_positivo=='S') ? 5 : 1;
					$novo_pedido_pessoa_resposta->de_resposta = ($request->in_positivo=='S') ? 'Positivo' : 'Negativo';
					$novo_pedido_pessoa_resposta->dt_resposta = Carbon::now();
					$novo_pedido_pessoa_resposta->id_usuario_cad = Auth::User()->id_usuario;
					$novo_pedido_pessoa_resposta->dt_cadastro = Carbon::now();
					$novo_pedido_pessoa_resposta->in_resposta_automatica = 'N';
					$novo_pedido_pessoa_resposta->in_positiva = $request->in_positivo;
					if (!$novo_pedido_pessoa_resposta->save()) {
						$erro = 20106;
					}
				} else {
					$erro = 20107;
				}
			}
		} else {
			$erro = 20106;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro ' . $erro));
		} else {
			$pedido->inserir_notificacao($this->id_pessoa,$pedido->id_pessoa_origem,'Resultado da certidão protocolo ' . $pedido->protocolo_pedido . ' foi inserido.');

			if ($pedido->pessoa_origem->no_email_pessoa != '') {
				$url_email = URL::to('/servicos/certidao');
                if (in_array($pedido->pessoa_origem->id_tipo_pessoa, array(7,11))) {
                    if ($pedido->pessoa_origem->usuario->usuario_pessoa[0]->in_master_recebe_notificacoes_sistema == 'S') {
                        Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
                            $mail->to($pedido->pessoa_origem->no_email_pessoa, $pedido->pessoa_origem->no_pessoa)
                                ->subject('CERI - Nova notificação do pedido de pesquisa protocolo ' . $pedido->protocolo_pedido);
                        });
                    }
                    if ($pedido->usuario->usuario_pessoa[0]->in_usuario_recebe_notificacoes_sistema == 'S'){
                        Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
                            $mail->to($pedido->usuario->email_usuario, $pedido->usuario->no_usuario)
                                ->subject('CERI - Nova notificação do pedido de pesquisa protocolo ' . $pedido->protocolo_pedido);
                        });
                    }
                } else {
                    Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
                        $mail->to($pedido->pessoa_origem->no_email_pessoa, $pedido->pessoa_origem->no_pessoa)
                            ->subject('CERI - Nova notificação do pedido de pesquisa protocolo ' . $pedido->protocolo_pedido);
                    });
                }
			}

			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O resultado da certidão foi inserido com sucesso.'));
		}
	}

    public function imprimir_resultado(Request $request, pedido $pedido) {
        if ($request->id_pedido>0) {
            $pedido = $pedido->find($request->id_pedido);

            return view('servicos.certidao.geral-certidao-imprimir-resultado',compact('pedido'));
        }
    }

    public function render_resultado($id_pedido, pedido $pedido) {
        if ($id_pedido>0) {
            $pedido = $pedido->find($id_pedido);
            
            $titulo = 'Resultado da Certidão: '.$pedido->protocolo_pedido;
            $pdf = PDF::loadView('pdf.certidao-resultado', compact('pedido', 'titulo'));   
            
            return $pdf->stream();
        }
    }

    public function desconto_aplicado($pedido_resultado_matricula, $aplicado=true) {
        if (isset($pedido_resultado_matricula)){
            $aplicado?$pedido_resultado_matricula->update(['in_desconto'=>'S']):$pedido_resultado_matricula->update(['in_desconto'=>'N']);
        }
    }

    private function desconto($certidao_token, $id_pedido_resultado_matricula) {
        if(session()->has('certidao_'.$certidao_token)) {
            $va_desconto = 0;
        } else {
            $pedido_resultado_matricula = new pedido_resultado_matricula();
            $pedido_resultado_matricula = $pedido_resultado_matricula->find($id_pedido_resultado_matricula);
            $va_desconto = isset($pedido_resultado_matricula)?$pedido_resultado_matricula->desconto()==0?$pedido_resultado_matricula->pedido_resultado->pedido->va_pedido:0:0;
            session()->put('certidao_'.$certidao_token,$pedido_resultado_matricula);
        }
        return  $va_desconto;
    }

	public function gerar_recibo_certidao(Request $request, pedido $pedido)
    {
        if($request->id_pedido>0) {
            $pedido = $pedido->find($request->id_pedido);

            return response()->json(['view'=>view('servicos.certidao.geral-certidao-recibo', compact('pedido'))->render()]);
        }
    }

    public function imprimir_recibo_certidao($id_pedido, pedido $pedido){
        if ($id_pedido>0){
            $pedido = $pedido->find($id_pedido);
            $titulo = 'Recibo de Certidão: ' .$pedido->protocolo_pedido;
            $class = $this;
            $pdf = PDF::loadView('pdf.certidao-recibo', compact('pedido','titulo', 'class'));
            return $pdf->stream();
        }
    }

	public function relatorio_certidao(Request $request, certidao $certidao) {
        $todas_certidoes = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
            ->join('produto','produto.id_produto','=','pedido.id_produto')
            ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
            ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_APROVACAO));

        if($request->ids_certidoes>0) {
            $todas_certidoes->whereIn('pedido.id_pedido', $request->ids_certidoes);
        }

        $todas_certidoes = $todas_certidoes->orderBy('pedido.dt_cadastro','desc')->get();


        return view('servicos.certidao.geral-certidao-relatorio',compact('todas_certidoes'));
    }

	public function salvar_relatorio_certidao(Request $request, certidao $certidao) {
        $todas_certidoes = $certidao->join('pedido','pedido.id_pedido','=','certidao.id_pedido')
            ->join('produto','produto.id_produto','=','pedido.id_produto')
            ->where('produto.id_grupo_produto',$this::ID_GRUPO_PRODUTO)
            ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',array($this::ID_SITUACAO_CADASTRADO,$this::ID_SITUACAO_APROVACAO));

        $todas_certidoes->whereIn('pedido.id_pedido', $request->id_certidao);
        $todas_certidoes = $todas_certidoes->orderBy('pedido.dt_cadastro','desc')->get();

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('certidao'.$time, function($excel) use ($todas_certidoes) {

            $excel->sheet('Certidão', function($sheet) use ($todas_certidoes) {
                $sheet->loadView('xls.relatorio-certidao', array('todas_certidoes' => $todas_certidoes));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }

    public function observacoes(Request $request, certidao $certidao){
        if($request->id_certidao > 0){
            $certidao = $certidao->find($request->id_certidao);

            $pessoa_ativa = Session::get('pessoa_ativa');

            $class = $this;
            $observacao_token = str_random(30);

            return view('servicos.certidao.geral-certidao-observacoes', compact('certidao', 'pessoa_ativa', 'class', 'observacao_token'));
        }
    }

    public function remover_arquivos_sessao(Request $request){
        if($request->session()->forget('arquivos_' . $request->observacao_token) == null){
            return response()->json(array('status' => 'sucesso',
                'recarrega' => 'false'
            ));
        } else {
            return response()->json(array('status' => 'erro',
                'recarrega' => 'true',
                'msg' => "Não foi possível remover o(s) arquivo(s)",
            ));
        }
    }

    public function inserir_observacao(Request $request, certidao $certidao) {
        if ($request->id_certidao>0) {
            $certidao = $certidao->find($request->id_certidao);
            $erro = 0;

            switch ($this->id_tipo_pessoa) {
                case 2: case 10:
                $id_pessoa_dest = $certidao->pedido->pessoa_origem->id_pessoa;
                break;
                default:
                    $id_pessoa_dest = $certidao->pedido->pedido_pessoa_atual->pessoa->id_pessoa;
                    break;
            }

            DB::beginTransaction();

            $nova_observacao = new certidao_observacao();
            $nova_observacao->id_certidao = $certidao->id_certidao;
            $nova_observacao->id_pessoa = $this->id_pessoa;
            $nova_observacao->id_pessoa_dest = $id_pessoa_dest;
            $nova_observacao->de_observacao = $request->de_observacao;
            $nova_observacao->id_usuario_cad = Auth::User()->id_usuario;
            $nova_observacao->dt_cadastro = Carbon::now();

            if ($nova_observacao->save()) {
                // ***** CODIGO INSERIR ARQUIVO ******
                if ($request->session()->has('arquivos_'.$request->observacao_token)) {
                    $destino = '/observacoes-certidao/'.$nova_observacao->id_certidao;
                    $arquivos = $request->session()->get('arquivos_'.$request->observacao_token);

                    Storage::makeDirectory('/public'.$destino);

                    $erro_loop_arq = false;
                    $erro_loop_sql = false;
                    foreach ($arquivos as $key => $arquivo) {
                        $origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
                        $destino_arquivo = '/public'.$destino.'/'.$arquivo['no_arquivo'];

                        $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                        $novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
                        $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
                        $novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
                        $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                        $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
                        $novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
                        $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
                        $novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
                        if ($arquivo['dt_assinado']!='') {
                            $novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
                        }
                        if ($arquivo['id_usuario_certificado']>0) {
                            $novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
                        }
                        if (!empty($arquivo['no_arquivo_p7s'])) {
                            $novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
                            $novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
                        }
                        if ($novo_arquivo_grupo_produto->save()) {
                            if (Storage::exists($origem_arquivo)) {
                                if (Storage::exists($destino_arquivo)) {
                                    Storage::delete($destino_arquivo);
                                }
                                if (Storage::copy($origem_arquivo,$destino_arquivo)) {
                                    if (count($arquivo['no_arquivos_originais'])>0) {
                                        foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
                                            $origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
                                            $destino_arquivo_original = '/public'.$destino.'/'.$no_arquivo_original;

                                            $novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
                                            $novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
                                            $novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
                                            $novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino;
                                            $novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
                                            $novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
                                            $novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
                                            $novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
                                            if ($novo_arquivo_grupo_produto_composicao->save()) {
                                                if (Storage::exists($origem_arquivo_original)) {
                                                    if (Storage::exists($destino_arquivo_original)) {
                                                        Storage::delete($destino_arquivo_original);
                                                    }
                                                    if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
                                                        $erro_loop_arq = true;
                                                    }
                                                } else {
                                                    $erro_loop_arq = true;
                                                }
                                            } else {
                                                $erro_loop_sql = true;
                                            }
                                        }
                                    }
                                } else {
                                    $erro_loop_arq = true;
                                }
                            } else {
                                $erro_loop_arq = true;
                            }
                        } else {
                            $erro_loop_sql = true;
                        }
                        $nova_observacao->certidao_observacao_arquivo_grupo()->attach($novo_arquivo_grupo_produto);
                    }
                    if ($erro_loop_arq) {
                        $erro = 1;
                    }
                    if ($erro_loop_sql) {
                        $erro = 1;
                    }

                    $request->session()->forget('arquivos_' . $request->observacao_token);
                }
                // ***** FIM CODIGO INSERIR ARQUIVO *****
            }

            if ($erro>0) {
                DB::rollback();
                return response()->json(array('status'=> 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status'=> 'sucesso',
                    'recarrega' => 'false',
                    'msg' => 'A observação foi inserida com sucesso.',
                    'observacao'=>$nova_observacao,
                    'pessoa'=>$nova_observacao->pessoa->no_pessoa,
                    'usuario'=>$nova_observacao->usuario_cad->no_usuario,
                    'dt_formatada'=>Carbon::now()->format('d/m/Y H:i'),
                    'arquivo' => $nova_observacao->certidao_observacao_arquivo_grupo
                ));
            }
        }
    }
    public function status_observacao(Request $request, certidao_observacao $certidao_observacao) {
        $pessoa_ativa = Session::get('pessoa_ativa');

        if ($request->id_certidao_observacao){
            $certidao_observacao = $certidao_observacao->where('id_certidao_observacao',$request->id_certidao_observacao)
                ->where('id_pessoa_dest',$pessoa_ativa->id_pessoa);
            if ($request->status == 'S'){
                $certidao_observacao->where('in_leitura','N')
                    ->update(['in_leitura'=>'S']);
                $status = 'lida';
            } else {
                $certidao_observacao->where('in_leitura','S')
                    ->update(['in_leitura'=>'N']);
                $status = 'não lida';
            }

            return response()->json(array('status'=>'sucesso',
                'recarrega'=>'false',
                'msg'=>'Observação marcada como '.$status.'.',
                'recarrega' => 'true'
            ));
        } else {
            return response()->json(array('status'=>'erro',
                'recarrega'=>'false',
                'msg'=>'Não foi possível alterar o status do registro.',
                'recarrega' => 'true'
            ));
        }
    }

    public function detalhes_solicitante(Request $request, usuario $usuario) {
        if ($request->id_usuario>0) {
            $usuario = $usuario->find($request->id_usuario);
            return view('servicos.certidao.serventia-certidao-detalhes-solicitante',compact('usuario'));
        }
    }
    
    public function protocolar(Request $request, pedido $pedido) {
        if ($request->id_pedido > 0) {
            $pedido = $pedido->find($request->id_pedido);
            return view('servicos.certidao.serventia-certidao-protocolar',compact('pedido'));
        }
    }

    public function inserir_protocolo(Request $request, pedido $pedido){
        $erro = 0;

        DB::beginTransaction();

        if ($request->id_pedido > 0){
            $pedido = $pedido->find($request->id_pedido);
            $pedido->nu_protocolo_legado = $request->nu_protocolo_legado;
            $pedido->dt_protocolo_legado = Carbon::now();
            if($pedido->save()){
                $historico_pedido = new historico_pedido();
                $historico_pedido->id_pedido = $request->id_pedido;
                $historico_pedido->id_situacao_pedido_grupo_produto = 71;
                $historico_pedido->de_observacao = 'Protocolo interno inserido.';
                $historico_pedido->id_usuario_cad = Auth::User()->id_usuario;
                if(!$historico_pedido->save()){
                    $erro = 2;
                }
            } else {
                $erro = 1;
            }
            if ($erro>0){
                DB::rollback();
                return response()->json(array('status' => 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor tente mais tarde erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status' => 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'Protocolo interno inserido com sucesso'));
            }
        }
    }

    public function alterar_protocolo(Request $request, pedido $pedido){
        $erro = 0;

        DB::beginTransaction();

        if($request->id_pedido > 0){
            $alterar_protocolo = $pedido->where('pedido.id_pedido', '=', $request->id_pedido)
                                        ->first();
            $alterar_protocolo->nu_protocolo_legado = $request->nu_protocolo_legado;
            $alterar_protocolo->dt_protocolo_legado = Carbon::now();
            if($alterar_protocolo->save()){
                $historico_pedido = new historico_pedido();
                $historico_pedido->id_pedido = $request->id_pedido;
                $historico_pedido->id_situacao_pedido_grupo_produto = 71;
                $historico_pedido->de_observacao = 'Protocolo interno alterado.';
                $historico_pedido->id_usuario_cad = Auth::User()->id_usuario;
                if(!$historico_pedido->save()){
                    $erro = 2;
                }
            } else {
                $erro = 1;
            }
            if($erro > 0){
                DB::roolback();
                return response()->json(array('status'=>'erro',
                    'recarrega'=>'false',
                    'msg' => 'Por favor tente mais tarde erro nº '.$erro));
            } else {
                DB::commit();
               return response()->json(array('status'=>'sucesso',
                   'recarrega'=>'true',
                   'msg'=>'Protocolo alterado com sucesso.'));
            }
        }
    }
}
