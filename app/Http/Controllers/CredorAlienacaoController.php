<?php
namespace App\Http\Controllers;

use App\agencia;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;
use DB;

use App\estado;
use App\credor_alienacao;

class CredorAlienacaoController extends Controller {
	
	public function __construct(credor_alienacao $credor_alienacao) {
		$this->credor_alienacao=$credor_alienacao;
	}

	public function novo(Request $request, estado $estado, agencia $agencia) {
    	$cidades 	= $estado->find(env('ID_ESTADO'))->cidades()->orderBy('cidade.no_cidade')->get();
		$agencias 	= $agencia->agencia_credor_alienacao();
		return view('servicos.credor-alienacao-novo',compact('request','cidades', 'agencias'));
    }

    public function inserir(Request $request, credor_alienacao $credor_alienacao) {
    	DB::beginTransaction();

		$novo_credor_alienacao = $this->credor_alienacao;
		$novo_credor_alienacao->id_cidade = $request->id_cidade;
		$novo_credor_alienacao->nu_cpf_cnpj = preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj);
		$novo_credor_alienacao->no_credor = $request->no_credor;
		$novo_credor_alienacao->no_endereco = $request->no_endereco;
		$novo_credor_alienacao->nu_endereco = $request->nu_endereco;
		$novo_credor_alienacao->no_complemento = $request->no_complemento;
		$novo_credor_alienacao->no_bairro = $request->no_bairro;
		$novo_credor_alienacao->nu_cep = preg_replace('#[^0-9]#', '', $request->nu_cep);
		$novo_credor_alienacao->id_usuario_cad = Auth::User()->id_usuario;
		$novo_credor_alienacao->id_agencia = $request->id_agencia;

		if ($novo_credor_alienacao->save()) {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O credor foi inserido com sucesso.',
										  'novo_credor' => $novo_credor_alienacao));
		} else {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde.'));
		}
	}

	public function detalhes(Request $request) {
    	$credor = $this->credor_alienacao->find($request->id_credor);

		return view('servicos.credor-alienacao-detalhes',compact('request','credor'));
    }

	public function total(Request $request) {
		$credores = $this->credor_alienacao->where('nu_cpf_cnpj',$request->nu_cpf_cnpj)->count();
		return $credores;
	}

	public function listar_credores(Request $request) {
		$credores = $this->credor_alienacao->where('id_cidade',$request->id_cidade)
										   ->orderBy('no_credor')
										   ->get();
		return response()->json($credores);
	}

	public function listar_agencia_credores(Request $request) {

		if ( $request->id_credor_alienacao > 0)
		{
			$credores = $this->credor_alienacao->select('credor_alienacao.id_credor_alienacao','credor_alienacao.no_credor', 'agencia.id_agencia', 'agencia.codigo_agencia', 'agencia.no_agencia')
											   ->where('id_credor_alienacao',$request->id_credor_alienacao)
											   ->join('agencia','agencia.id_agencia','=','credor_alienacao.id_agencia')
											   ->orderBy('codigo_agencia')
											   ->get();

		}else if ($request->id_cidade > 0 ){
			$credores = $this->credor_alienacao->select('credor_alienacao.id_credor_alienacao','credor_alienacao.no_credor', 'agencia.id_agencia', 'agencia.codigo_agencia', 'agencia.no_agencia')
												->where('credor_alienacao.id_cidade',$request->id_cidade)
												->join('agencia','agencia.id_agencia','=','credor_alienacao.id_agencia')
												->orderBy('codigo_agencia')
												->get();
		}else if ($request->id_agencia > 0){
			$credores = $this->credor_alienacao->select('credor_alienacao.id_credor_alienacao','credor_alienacao.no_credor', 'agencia.id_agencia', 'agencia.codigo_agencia', 'agencia.no_agencia')
				->where('credor_alienacao.id_agencia',$request->id_agencia)
				->join('agencia','agencia.id_agencia','=','credor_alienacao.id_agencia')
				->orderBy('codigo_agencia')
				->get();
		}

		return response()->json($credores);
	}

}
