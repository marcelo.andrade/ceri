<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\pedido_produto_item;
use App\pedido_serventia_resposta;
use App\usuario;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use Storage;
use URL;
use Mail;
use Session;

use App\alcada;
use App\estado;
use App\pedido;
use App\serventia;
use App\situacao_pedido_grupo_produto;
use App\pedido_serventia;
use App\historico_pedido;
use App\bem_imovel;
use App\protocolo;
use App\protocolo_natureza;
use App\protocolo_observacao;
use App\penhora_bem_imovel;
use App\protocolo_arquivo;

class ProtocoloController extends Controller {

	const ID_SITUACAO_ANALISE = 52;
	const ID_SITUACAO_PROTOCOLADO = 53;

	public function __construct(protocolo $protocolo) {
		$this->protocolo=$protocolo;
		$this->id_grupo_produto=5;
		$this->id_produto=16;

		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, situacao_pedido_grupo_produto $situacao){
    	$class = $this;

		$ceri_funcoes     = new CeriFuncoes();
		$notificacao      = $ceri_funcoes->buscar_notificacoes();

		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela principal de protocolo','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

		switch ($this->id_tipo_pessoa) {
			case 3: case 4: case 5: case 7: case 11: case 6:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this->id_grupo_produto)
									  ->orderBy('nu_ordem')->get();
				
				$todos_protocolos = $this->protocolo->join('pedido','pedido.id_pedido','=','protocolo.id_pedido')
													->join('produto','produto.id_produto','=','pedido.id_produto')
													->where('produto.id_grupo_produto',$this->id_grupo_produto)
													->where('pedido.id_pessoa_origem','=',$this->id_pessoa);

				if ($request->isMethod('post')) {
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_pesquisas->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_pesquisas->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ( $request->protocolo_pedido > 0){
						$todas_pesquisas->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_chave_pesquisa_pesquisa>0){
						$todas_pesquisas->where('pesquisa.id_chave_pesquisa_pesquisa',$request->id_chave_pesquisa_pesquisa);
						$todas_pesquisas->where('pesquisa.de_chave_pesquisa',$request->de_chave_pesquisa);
					}
				}

				$todos_protocolos = $todos_protocolos->orderBy('protocolo.dt_cadastro','desc')->get();
													  
		    	return view('servicos.protocolo',compact('class','request','situacoes','todos_protocolos', 'notificacao'));
				break;
			case 2:
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this->id_grupo_produto)
									  ->orderBy('nu_ordem')->get();

				$todos_protocolos = $this->protocolo->join('pedido','pedido.id_pedido','=','protocolo.id_pedido')
													->join('produto','produto.id_produto','=','pedido.id_produto')
													->join('pedido_pessoa',function($join) {
														$join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
															 ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
											  	  	})
													->where('produto.id_grupo_produto',$this->id_grupo_produto);
				
				if ($request->isMethod('post')) {
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_pesquisas->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_pesquisas->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ( $request->protocolo_pedido > 0){
						$todas_pesquisas->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if( $request->id_chave_pesquisa_pesquisa>0){
						$todas_pesquisas->where('pesquisa.id_chave_pesquisa_pesquisa',$request->id_chave_pesquisa_pesquisa);
						$todas_pesquisas->where('pesquisa.de_chave_pesquisa',$request->de_chave_pesquisa);
					}
				}
				
				$todos_protocolos = $todos_protocolos->orderBy('protocolo.dt_cadastro','desc')->get();

		    	return view('servicos.cartorio.protocolo',compact('class','request','situacoes','todos_protocolos', 'notificacao'));
				break;
		}
    }

    public function novo(estado $estado) {
		$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		
		return view('servicos.protocolo-novo',compact('cidades'));
    }

	public function inserir(Request $request, pedido $pedido, situacao_pedido_grupo_produto $situacao, serventia $serventia, pedido_serventia $pedido_serventia, historico_pedido $historico_pedido) {
		$erro = 0;

		DB::beginTransaction();

		$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(" . Auth::User()->id_usuario . ", " . $this->id_produto . ");"));

		$novo_pedido = $pedido;
		$novo_pedido->id_usuario = Auth::User()->id_usuario;
		$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_ANALISE;
		$novo_pedido->id_produto = $this->id_produto;
		$novo_pedido->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
		$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
		$novo_pedido->dt_pedido = Carbon::now();
		$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
		$novo_pedido->dt_cadastro = Carbon::now();
		$novo_pedido->va_pedido = 0;

		if ($novo_pedido->save()) {
			$serventia = new serventia();
			$serventia_selecionada = $serventia->find($request->id_serventia);
			$novo_pedido->serventias()->attach($serventia_selecionada);

			$novo_protocolo = $this->protocolo;
			$novo_protocolo->id_pedido = $novo_pedido->id_pedido;
			$novo_protocolo->id_usuario_cad = Auth::User()->id_usuario;
			$novo_protocolo->dt_cadastro = Carbon::now();

			$novo_pedido_produto_item = new pedido_produto_item();
			$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
			$novo_pedido_produto_item->id_produto_item = 29;
			$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
			$novo_pedido_produto_item->save();

			if ($novo_protocolo->save()) {
				$destino = '../storage/app/public/protocolos/'.$novo_protocolo->id_protocolo;
				if (!File::isDirectory($destino)) {
					File::makeDirectory($destino);
				}

				foreach ($request->no_arquivo as $arquivo) {
					$nome_arquivo = remove_caracteres($arquivo->getClientOriginalName());
					if ($arquivo->move($destino, $nome_arquivo)) {
						$novo_protocolo_arquivo = new protocolo_arquivo();
						$novo_protocolo_arquivo->id_protocolo = $novo_protocolo->id_protocolo;
						$novo_protocolo_arquivo->no_arquivo = $nome_arquivo;
						$novo_protocolo_arquivo->no_local_arquivo = 'protocolos/'.$novo_protocolo->id_protocolo;
						$novo_protocolo_arquivo->id_usuario_cad = Auth::User()->id_usuario;
						$novo_protocolo_arquivo->dt_cadastro = Carbon::now();
						$novo_protocolo_arquivo->no_extensao = $arquivo->getClientOriginalExtension();
						if (!$novo_protocolo_arquivo->save()) {
							$erro = 1;
						}
					}
				}

				$novo_historico = new historico_pedido();
				$novo_historico->id_pedido = $novo_pedido->id_pedido;
				$novo_historico->id_situacao_pedido_grupo_produto = $novo_pedido->id_situacao_pedido_grupo_produto;
				$novo_historico->id_alcada = alcada::ID_ALCADA_PESSOA_JURIDICA;
				$novo_historico->de_observacao = 'Protocolo inserido com sucesso. Total de arquivos: '.count($request->no_arquivo);
				$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
				$novo_historico->save();

				$ceriFuncoes = new CeriFuncoes();
				$ceriFuncoes->inserir_notificacao('SERVENTIA',$novo_pedido->id_pedido,'Nova solicitação de Protocolo Eletrônico, protocolo número: '.$novo_pedido->protocolo_pedido,0,$request->id_serventia);

			} else {
				$erro = 1;
			}
		} else {
			$erro = 1;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O protocolo foi inserido com sucesso.'));
		}
	}

	public function nova_resposta(Request $request, pedido $pedido, protocolo_natureza $protocolo_natureza) {
		if ($request->id_pedido>0) {
			$class = $this;
			$pedido = $pedido->find($request->id_pedido);

			$naturezas = $protocolo_natureza->where('in_registro_ativo','S')
											->orderBy('nu_ordem','asc')
											->get();
			
			return view('servicos.cartorio.protocolo-resposta',compact('class','pedido','naturezas'));
		}
	}

	public function detalhes(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$class = $this;
			$pedido = $pedido->find($request->id_pedido);
			//respondendo a notificacao do usuario
			$ceriFuncoes = new CeriFuncoes();
			$ceriFuncoes->reponder_notificacao($request->id_pedido,Auth::User()->id_usuario);
			return view('servicos.protocolo-detalhes',compact('class','pedido'));
		}
	}
	public function arquivo(Request $request, protocolo_arquivo $protocolo_arquivo) {
		if ($request->id_protocolo_arquivo>0) {
			$protocolo_arquivo = $protocolo_arquivo->find($request->id_protocolo_arquivo);

			if ($protocolo_arquivo) {
				if ($protocolo_arquivo->no_arquivo!='') {
					$url_download = URL::to('/arquivos/download/protocolo/'.$protocolo_arquivo->id_protocolo_arquivo);
	
					return response()->json(array('status' => 'sucesso',
												  'view'=>view('servicos.protocolo-arquivo',compact('protocolo_arquivo'))->render(),
												  'download'=>'true',
												  'url_download'=>$url_download));
				} else {
					return response()->json(array('status' => 'erro',
										  		  'recarrega' => 'false',
										  		  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
				}
			} else {
				return response()->json(array('status' => 'erro',
										 	  'recarrega' => 'false',
										  	  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
			}
		}
	}
	public function arquivo_arquivo(Request $request, protocolo_arquivo $protocolo_arquivo) {
		if ($request->id_protocolo_arquivo>0) {
			$protocolo_arquivo = $protocolo_arquivo->find($request->id_protocolo_arquivo);

			$arquivo = Storage::get('/public/'.$protocolo_arquivo->no_local_arquivo.'/'.$protocolo_arquivo->no_arquivo);
			
			$mimes = array('pdf' => 'application/pdf',
						   'jpg' => 'image/jpeg',
						   'png' => 'image/png',
						   'bmp' => 'image/bmp',
						   'gif' => 'image/gif');
						   
			return response($arquivo, 200)->header('Content-Type', $mimes[$protocolo_arquivo->no_extensao])
										  ->header('Content-Disposition', 'inline; filename="'.$protocolo_arquivo->no_arquivo.'"');
		}
	}
	public function download_arquivo(Request $request, protocolo_arquivo $protocolo_arquivo) {
		if ($request->id_protocolo_arquivo>0) {
			$protocolo_arquivo = $protocolo_arquivo->find($request->id_protocolo_arquivo);

			$nome_arquivo = $protocolo_arquivo->no_arquivo;
			
			$arquivo = Storage::get('/public/'.$protocolo_arquivo->no_local_arquivo.'/'.$nome_arquivo);
			return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$nome_arquivo.'"');
		}
	}

	public function inserir_resposta(Request $request, pedido $pedido, protocolo $protocolo, historico_pedido $historico_pedido, pedido_serventia_resposta $pedido_serventia_resposta, usuario $usuario) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			if ($pedido) {	
				$erro = 0;
	
				DB::beginTransaction();
				
				$alterar_protocolo = $protocolo->find($pedido->protocolo->id_protocolo);
				$alterar_protocolo->id_protocolo_natureza = $request->id_protocolo_natureza;
				$alterar_protocolo->no_assunto = $request->no_assunto;
				$alterar_protocolo->no_apresentante = $request->no_apresentante;
				$alterar_protocolo->tp_pessoa_apresentante = $request->tp_pessoa_apresentante;
				$alterar_protocolo->nu_cpf_cnpj_apresentante = $request->nu_cpf_cnpj_apresentante;
				$alterar_protocolo->de_conteudo = $request->de_conteudo;
				
				if ($alterar_protocolo->save()) {
					foreach ($request->a_id_bem_imovel as $id_bem_imovel) {
						$bem_imovel_selecionado = new bem_imovel();
						$bem_imovel_selecionado = $bem_imovel_selecionado->find($id_bem_imovel);
						$alterar_protocolo->bem_imoveis()->attach($bem_imovel_selecionado);
					}
					
					if (!$pedido->where('id_pedido',$pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_PROTOCOLADO])) {
						$erro = 1;
					}
					
					$novo_historico = $historico_pedido;
					$novo_historico->id_pedido = $pedido->id_pedido;
					$novo_historico->id_situacao_pedido_grupo_produto = $pedido->id_situacao_pedido_grupo_produto;
					$novo_historico->id_alcada = 3;
					$novo_historico->de_observacao = 'Protocolo respondido com sucesso';
					$novo_historico->id_usuario_cad = Auth::User()->id_usuario;
					$novo_historico->save();

					$novo_pedido_serventia_resposta 						= $pedido_serventia_resposta;
					$novo_pedido_serventia_resposta->id_pedido_serventia 	= $pedido->pedido_serventia->id_pedido_serventia;
					$novo_pedido_serventia_resposta->id_tipo_resposta 		= 5;
					$novo_pedido_serventia_resposta->de_resposta 			= $request->de_conteudo;
					$novo_pedido_serventia_resposta->dt_resposta 			= Carbon::now();
					$novo_pedido_serventia_resposta->id_usuario_cad 		= Auth::User()->id_usuario;
					$novo_pedido_serventia_resposta->dt_cadastro 			= Carbon::now();
					$novo_pedido_serventia_resposta->in_resposta_automatica = 'N';

					if ( $novo_pedido_serventia_resposta->save())
					{
						$usuario_encontrado = $usuario->where('id_usuario', $pedido->id_usuario)->first();

						//respondendo todas as notificações para todos os usuarios da serventia
						$ceriFuncoes = new CeriFuncoes();
						$ceriFuncoes->reponder_notificacao($request->id_pedido,0);

						//abrindo uma nova notificacao informando o usuario que a sua solicitacao foi respondida
						$ceriFuncoes->inserir_notificacao('USUARIO',$pedido->id_pedido,'Resposta solicitação de Pesquisa eletônica, protocolo número: '.$pedido->protocolo_pedido,$usuario_encontrado->id_usuario,0);

						Mail::send('email.confirmar-resposta', ['pedido' => $pedido, 'usuario_encontrado'=>$usuario_encontrado], function ($mail) use ($request, $pedido, $usuario_encontrado) {
							$mail->to($usuario_encontrado->email_usuario, $usuario_encontrado->no_usuario)
								->subject('CERI/Protocolo - Você tem uma nova resposta para o seu pedido de protocolo número: '.$pedido->protocolo_pedido);
						});
					}


				} else {
					$erro = 1;
				}
			} else {
				$erro = 1;
			}
		} else {
			$erro = 1;
		}
		
		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'A resposta foi inserida com sucesso.'));
		}
	}
	
	public function inserir_observacao(Request $request, pedido $pedido, protocolo_observacao $protocolo_observacao, historico_pedido $historico_pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			if ($pedido) {	
				$erro = 0;
	
				DB::beginTransaction();
				
				$nova_observacao = $protocolo_observacao;
				$nova_observacao->id_protocolo = $pedido->protocolo->id_protocolo;
				$nova_observacao->de_observacao = $request->de_observacao;
				$nova_observacao->id_usuario_cad = Auth::User()->id_usuario;
				$nova_observacao->dt_cadastro = Carbon::now();
				
				if (!$nova_observacao->save()) {
					$erro = 1;
				}
			} else {
				$erro = 1;
			}
		} else {
			$erro = 1;
		}
		
		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'false',
										  'msg' => 'A observação foi inserida com sucesso',
										  'observacao' => $nova_observacao,
										  'usuario'=>Auth::User()->pessoa->no_pessoa,
										  'dt_formatada'=>Carbon::now()->format('d/m/Y H:i')));
		}
	}
}
