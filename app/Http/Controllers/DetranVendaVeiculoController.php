<?php
namespace App\Http\Controllers;

use App\arquivo_grupo_produto;
use App\detran_cor_veiculo;
use App\detran_marca_veiculo;
use App\detran_modelo_veiculo;
use App\detran_operacao_veiculo;
use App\detran_operacao_veiculo_arquivo_grupo;
use App\detran_operacao_veiculo_parte;
use App\detran_parte_operacao_veiculo;
use App\detran_veiculo;
use App\alcada;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\CeriFuncoes;
use App\pedido;
use App\pedido_produto_item;
use App\pessoa;
use App\pessoa_telefone;
use App\produto_item;
use App\serventia;
use App\telefone;
use App\usuario;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;


class DetranVendaVeiculoController extends Controller {

	const ID_GRUPO_PRODUTO 		 = 8;
	const ID_PRODUTO 			 = 22;
	const ID_PRODUTO_ITEM		 = 32;
	const ID_SITUACAO_CADASTRADO = 58;
	const TIPO_PARTE_VENDEDOR	 = 1;
	const TIPO_PARTE_COMPRADOR	 = 2;
	const ARQUIVO_CRV	 		 = 2;
	const ARQUIVO_CPF	 		 = 3;
	const ARQUIVO_CARTAO_CNPJ	 = 4;
	const ARQUIVO_RG	 		 = 5;
	const ARQUIVO_COMP_ENDERECO	 = 6;
	const ARQUIVO_COM_VENDA		 = 7;
	const ARQUIVO_CONT_SOCIAL	 = 8;


	public function index(Request $request, detran_operacao_veiculo $detran_operacao_veiculo)
	{

		$todos_veiculos_venda = $detran_operacao_veiculo->join('pedido','pedido.id_pedido',  '=','operacao_veiculo.id_pedido')
														->join('veiculo','veiculo.id_veiculo','=','operacao_veiculo.id_veiculo')
														//vendedor
														->join('operacao_veiculo_parte as ovp1', function($join){
															$join->on('ovp1.id_operacao_veiculo', '=', 'operacao_veiculo.id_operacao_veiculo')
																 ->where('ovp1.id_tipo_parte_operacao_veiculo', '=', '1');

														})
														->join('parte_operacao_veiculo as pov1','pov1.id_parte_operacao_veiculo','=','ovp1.id_parte_operacao_veiculo')
														//comprador
														->join('operacao_veiculo_parte as ovp2', function($join){
															$join->on('ovp2.id_operacao_veiculo', '=', 'operacao_veiculo.id_operacao_veiculo')
																->where('ovp2.id_tipo_parte_operacao_veiculo', '=', '2');

														})
														->join('parte_operacao_veiculo as pov2','pov2.id_parte_operacao_veiculo','=','ovp2.id_parte_operacao_veiculo')
														->join('pedido_serventia',function($join) {
															  $join->on('pedido_serventia.id_pedido', '=', 'pedido.id_pedido')
															  ->where('pedido_serventia.id_serventia','=',Auth::User()->usuario_serventia->id_serventia);
														})->select(
																	'operacao_veiculo.id_operacao_veiculo',
																	'protocolo_pedido',
																	'dt_operacao',
																	'placa',
																	'crv',
																	'renavam',
																	'chassi',
																	'pov1.no_parte_operacao_veiculo as vendedor',
																	'pov2.no_parte_operacao_veiculo as comprador'
														);

		$todos_veiculos_venda = $todos_veiculos_venda->orderBy('operacao_veiculo.dt_cadastro','desc')->get();

		//dd($todos_veiculos_venda);

    	return view('detran.venda.detran-venda-veiculo', compact('todos_veiculos_venda'));
	}

    public function nova_comunicacao(Request $request, detran_cor_veiculo $detran_cor_veiculo, detran_marca_veiculo $detran_marca_veiculo)
	{
		$todas_cores  = $detran_cor_veiculo->where('in_registro_ativo', '=', 'S')->orderBy('no_cor_veiculo')->get();
		$todas_marcas = $detran_marca_veiculo->where('in_registro_ativo', '=', 'S')->orderBy('no_marca_veiculo')->get();
    	return view('detran.venda.detran-nova-venda', compact('todas_cores', 'todas_marcas'));
	}

	public function detalhes(Request $request, detran_operacao_veiculo $detran_operacao_veiculo)
	{
		$resultado 			= $detran_operacao_veiculo->find($request->id_operacao_veiculo);
		$tipo_arquivo_crv	=$this::ARQUIVO_CRV;
		return view('detran.venda.detran-detalhe-venda', compact('resultado', 'tipo_arquivo_crv'));
	}

	public function download_arquivo(Request $request, arquivo_grupo_produto $arquivo_grupo_produto)
	{
		if ($request->id_arquivo_grupo_produto>0) {
			$arquivo_grupo_produto = $arquivo_grupo_produto->find($request->id_arquivo_grupo_produto);

			$nome_arquivo = $arquivo_grupo_produto->no_arquivo;

			$arquivo = Storage::get('/public/'.$arquivo_grupo_produto->no_local_arquivo.'/'.$nome_arquivo);
			return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$nome_arquivo.'"');
		}
	}

	public function inserir_comunicacao(Request $request, pedido $pedido, serventia $serventia, pedido_produto_item $pedido_produto_item, detran_operacao_veiculo $detran_operacao_veiculo, detran_parte_operacao_veiculo $detran_parte_operacao_veiculo, detran_veiculo $detran_veiculo)
	{
		DB::beginTransaction();

		$protocoloPedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));
		$erro = 0;

		$novo_pedido = $pedido;
		$novo_pedido->id_usuario 						= Auth::User()->id_usuario;
		$novo_pedido->id_situacao_pedido_grupo_produto  = $this::ID_SITUACAO_CADASTRADO;
		$novo_pedido->id_produto 						= $this::ID_PRODUTO;
		$novo_pedido->id_alcada 						= alcada::ID_ALCADA_PESSOA_JURIDICA;
		$novo_pedido->protocolo_pedido 					= $protocoloPedido[0]->f_geraprotocolo;
		$novo_pedido->dt_pedido 						= Carbon::now();
		$novo_pedido->id_usuario_cad 					= Auth::User()->id_usuario;
		$novo_pedido->dt_cadastro 						= Carbon::now();
		$novo_pedido->va_pedido 						= 0;

		if ($novo_pedido->save())
		{
			$serventia_selecionada = $serventia->find(Auth::User()->usuario_serventia->id_serventia);
			$novo_pedido->serventias()->attach($serventia_selecionada);

			$novo_pedido_produto_item 					= $pedido_produto_item;
			$novo_pedido_produto_item->id_pedido 		= $novo_pedido->id_pedido;
			$novo_pedido_produto_item->id_produto_item 	= $this::ID_PRODUTO_ITEM;
			$novo_pedido_produto_item->id_usuario_cad 	= Auth::User()->id_usuario;


			#checando se o veiculo ja existe
			if ( $request->id_veiculo == 0 )
			{
				#inserindo dados do veiculo
				$novo_veiculo = $detran_veiculo;
				$novo_veiculo->id_modelo_veiculo =  $request->id_modelo_veiculo;
				$novo_veiculo->id_cor_veiculo	 =  $request->id_cor_veiculo;
				$novo_veiculo->placa 			 =  $request->placa;
				$novo_veiculo->crv 				 =  $request->crv;
				$novo_veiculo->renavam 			 =  $request->renavam;
				$novo_veiculo->chassi 			 =  $request->chassi;
				$novo_veiculo->nu_ano_modelo 	 =  $request->nu_ano_modelo;
				$novo_veiculo->nu_ano_fabricacao =  $request->nu_ano_fabricacao;
				$novo_veiculo->in_registro_ativo =  'S';
				$novo_veiculo->id_usuario_cad	 =  Auth::User()->id_usuario;
				$novo_veiculo->dt_cadastro		 = Carbon::now();

				if (!$novo_veiculo->save())
				{
					DB::rollback();
					return response()->json(array('status'     => 'erro',
												  'recarrega'  => 'false',
												  'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 6'
												));
				}
			}

			$id_veiculo = ( $request->id_veiculo > 0 ) ? $request->id_veiculo : $novo_veiculo->id_veiculo;

			if ($novo_pedido_produto_item->save())
			{
				$nova_operacao_veiculo_venda					= $detran_operacao_veiculo;
				$nova_operacao_veiculo_venda->id_pedido 		= $novo_pedido->id_pedido;
				$nova_operacao_veiculo_venda->id_veiculo 		= $id_veiculo;
				$nova_operacao_veiculo_venda->dt_operacao		= Carbon::now();
				$nova_operacao_veiculo_venda->de_operacao		= 'COMUNICAÇÃO DE VENDA';
				$nova_operacao_veiculo_venda->id_usuario_cad	= Auth::User()->id_usuario;
				$nova_operacao_veiculo_venda->dt_cadastro		= Carbon::now();

				if ($nova_operacao_veiculo_venda->save())
				{
					//checar se o vendedor ja e uma pessoa no sistema
					if ( $request->id_pessoa_vendedor == 0)
					{
						//cadastrando a pessoa no sistema
						$new_id_pessoa = $this->inserirPessoasParte( $request, 'vendedor' );
						if ($new_id_pessoa <= 0)
						{
							DB::rollback();
							return response()->json(array('status'     => 'erro',
								                          'recarrega'  => 'false',
														  'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 8'
														));
						}
					}

					$id_pessoa = ( $request->id_pessoa_vendedor == 0 ) ? $new_id_pessoa : $request->id_pessoa_vendedor;

					//cadastrando os telefones da pessoa no sistema
					if (!$this->inserirTelefonesPartes($id_pessoa, $request->telefones_selecionado_vendedor ))
					{
						DB::rollback();
						return response()->json(array('status'     => 'erro',
							                          'recarrega'  => 'false',
													  'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 7'
													));
					}

					#inserindo dados do vendedor
					$nova_parte_operacao_veiculo_vendedor 								= new detran_parte_operacao_veiculo();
					$nova_parte_operacao_veiculo_vendedor->id_pessoa					= $id_pessoa;
					$nova_parte_operacao_veiculo_vendedor->no_parte_operacao_veiculo	= strtoupper($request->no_vendedor);
					$nova_parte_operacao_veiculo_vendedor->nu_cpf_cnpj					= preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj_vendedor);
					$nova_parte_operacao_veiculo_vendedor->nu_rg						= preg_replace('#[^0-9]#', '', $request->rg_numero_vendedor);
					$nova_parte_operacao_veiculo_vendedor->id_usuario_cad				= Auth::User()->id_usuario;
					$nova_parte_operacao_veiculo_vendedor->dt_cadastro					= Carbon::now();

					if ($nova_parte_operacao_veiculo_vendedor->save())
					{
						/*foi retirado pois precisava do id de retorno desta tabela e para evitar de fazer um insert e depois um select
						  optei por popular a tabela de relacionamento na mao
						 * $nova_operacao_veiculo_venda->operacao_parte()
							->attach($nova_parte_operacao_veiculo_vendedor,
								[
									'id_tipo_parte_operacao_veiculo' => $this::TIPO_PARTE_VENDEDOR,
									'dt_cadastro' 					 => Carbon::now(),
									'id_usuario_cad' 				 => Auth::User()->id_usuario,
								]
							);*/


						#populando a tabela de relcionamento de operacao e parte para o vendedor
						$nova_operacao_veiculo_parte 									= new  detran_operacao_veiculo_parte();
						$nova_operacao_veiculo_parte->id_operacao_veiculo				= $nova_operacao_veiculo_venda->id_operacao_veiculo;
						$nova_operacao_veiculo_parte->id_parte_operacao_veiculo			= $nova_parte_operacao_veiculo_vendedor->id_parte_operacao_veiculo;
						$nova_operacao_veiculo_parte->id_tipo_parte_operacao_veiculo	= $this::TIPO_PARTE_VENDEDOR;
						$nova_operacao_veiculo_parte->dt_cadastro						= Carbon::now();
						$nova_operacao_veiculo_parte->id_usuario_cad					= Auth::User()->id_usuario;

						if (! $nova_operacao_veiculo_parte->save())
						{
							DB::rollback();
							return response()->json(array('status'     => 'erro',
														  'recarrega'  => 'false',
														  'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 8'
													));
						}

						$find_operacao_veiculo_parte 	= new  detran_operacao_veiculo_parte();
						$id_operacao_veiculo_parte 		= $find_operacao_veiculo_parte->where('id_operacao_veiculo', '=', $nova_operacao_veiculo_venda->id_operacao_veiculo)->where('id_tipo_parte_operacao_veiculo', '=', $this::TIPO_PARTE_VENDEDOR)->first()->id_operacao_veiculo_parte;

						#inserindo documentos do vendedor
						$erro_upload_vendedor = 0;


						switch ( $request->tp_pessoa_vendedor )
						{
							case 'F':
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CPF, $request->arquivo_cpf_vendedor)){
									$erro_upload_vendedor++;
								}
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_RG, $request->arquivo_rg_vendedor)){
									$erro_upload_vendedor++;
								}
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_COMP_ENDERECO, $request->arquivo_comprovante_endereco_vendedor)){
									$erro_upload_vendedor++;
								}
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CRV, $request->arquivo_veiculo_crv)){
									$erro_upload_vendedor++;
								}
							break;
							case 'J':
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CONT_SOCIAL,  $request->arquivo_contrato_social_vendedor)){
									$erro_upload_vendedor++;
								}
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CARTAO_CNPJ, $request->arquivo_crt_cnpj_vendedor)){
									$erro_upload_vendedor++;
								}
								if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_COMP_ENDERECO, $request->arquivo_comprovante_endereco_vendedor)){
									$erro_upload_vendedor++;
								}
							break;
						}

						if ($erro_upload_vendedor > 0 )
						{
							DB::rollback();
							return response()->json(array(  'status'     => 'erro',
															'recarrega'  => 'false',
															'msg' 	     => 'Por favor, tente novamente mais tarde. Erro nº 9'
														));
						}



						//checar se o comprador ja e uma pessoa no sistema
						if ( $request->id_pessoa_comprador == 0)
						{
							//cadastrando a pessoa no sistema
							$new_id_pessoa = $this->inserirPessoasParte( $request, 'comprador' );
							if ($new_id_pessoa <= 0)
							{
								DB::rollback();
								return response()->json(array('status'     => 'erro',
									'recarrega'  => 'false',
									'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 8'
								));
							}
						}

						$id_pessoa = ( $request->id_pessoa_comprador == 0 ) ? $new_id_pessoa : $request->id_pessoa_comprador;

						//cadastrando os telefones da pessoa no sistema
						if (!$this->inserirTelefonesPartes($id_pessoa, $request->telefones_selecionado_comprador ))
						{
							DB::rollback();
							return response()->json(array('status'     => 'erro',
								'recarrega'  => 'false',
								'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 7'
							));
						}

						#inserindo dados do comprador
						$nova_parte_operacao_veiculo_comprador 								= new detran_parte_operacao_veiculo();
						$nova_parte_operacao_veiculo_comprador->id_pessoa					= $id_pessoa;
						$nova_parte_operacao_veiculo_comprador->no_parte_operacao_veiculo	= strtoupper($request->no_comprador);
						$nova_parte_operacao_veiculo_comprador->nu_cpf_cnpj					= preg_replace('#[^0-9]#', '', $request->nu_cpf_cnpj_comprador);
						$nova_parte_operacao_veiculo_comprador->nu_rg						= preg_replace('#[^0-9]#', '', $request->rg_numero_comprador);
						$nova_parte_operacao_veiculo_comprador->id_usuario_cad				= Auth::User()->id_usuario;
						$nova_parte_operacao_veiculo_comprador->dt_cadastro					= Carbon::now();

						if ($nova_parte_operacao_veiculo_comprador->save())
						{
							/*foi retirado pois precisava do id de retorno desta tabela e para evitar de fazer um insert e depois um select
							  optei por popular a tabela de relacionamento na mao
							 * $nova_operacao_veiculo_venda->operacao_parte()
							$nova_operacao_veiculo_venda->operacao_parte()
														->attach($nova_parte_operacao_veiculo_comprador,
															[
																'id_tipo_parte_operacao_veiculo' => $this::TIPO_PARTE_COMPRADOR,
																'dt_cadastro' 					 => Carbon::now(),
																'id_usuario_cad' 				 => Auth::User()->id_usuario,
															]
														);*/

							#populando a tabela de relcionamento de operacao e parte para o comprador
							$nova_operacao_veiculo_parte 									= new  detran_operacao_veiculo_parte();
							$nova_operacao_veiculo_parte->id_operacao_veiculo				= $nova_operacao_veiculo_venda->id_operacao_veiculo;
							$nova_operacao_veiculo_parte->id_parte_operacao_veiculo			= $nova_parte_operacao_veiculo_comprador->id_parte_operacao_veiculo;
							$nova_operacao_veiculo_parte->id_tipo_parte_operacao_veiculo	= $this::TIPO_PARTE_COMPRADOR;
							$nova_operacao_veiculo_parte->dt_cadastro						= Carbon::now();
							$nova_operacao_veiculo_parte->id_usuario_cad					= Auth::User()->id_usuario;

							if (! $nova_operacao_veiculo_parte->save())
							{
								DB::rollback();
								return response()->json(array('status'     => 'erro',
															  'recarrega'  => 'false',
															  'msg' 	   => 'Por favor, tente novamente mais tarde. Erro nº 8'
															));
							}

							$find_operacao_veiculo_parte 	= new  detran_operacao_veiculo_parte();
							$id_operacao_veiculo_parte 		= $find_operacao_veiculo_parte->where('id_operacao_veiculo', '=', $nova_operacao_veiculo_venda->id_operacao_veiculo)->where('id_tipo_parte_operacao_veiculo', '=', $this::TIPO_PARTE_COMPRADOR)->first()->id_operacao_veiculo_parte;

							#inserindo documentos do comprador
							$erro_upload_comprador = 0;
							switch ( $request->tp_pessoa_comprador )
							{
								case 'F':
									if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CPF, $request->arquivo_cpf_comprador)){
										$erro_upload_comprador++;
									}
									if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_RG, $request->arquivo_rg_comprador)){
										$erro_upload_comprador++;
									}
									if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_COMP_ENDERECO, $request->arquivo_comprovante_endereco_comprador)){
										$erro_upload_comprador++;
									}
									break;
								case 'J':
									if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CONT_SOCIAL, $request->arquivo_contrato_social_comprador)){
										$erro_upload_comprador++;
									}
									if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_CARTAO_CNPJ, $request->arquivo_cartao_cnpj_comprador)){
										$erro_upload_comprador++;
									}
									if (!$this->inserirDocumento( $nova_operacao_veiculo_venda->id_operacao_veiculo, $id_operacao_veiculo_parte, $this::ARQUIVO_COMP_ENDERECO, $request->arquivo_comprovante_endereco_comprador)){
										$erro_upload_comprador++;
									}
									break;
							}

							if ($erro_upload_comprador > 0 )
							{
								DB::rollback();
								return response()->json(array(  'status'     => 'erro',
																'recarrega'  => 'false',
																'msg' 	     => 'Por favor, tente novamente mais tarde. Erro nº 10'
															));
							}
						}else{
							$erro = 5;
						}
					}else{
						$erro = 4;
					}
				}else{
					$erro = 3;
				}
			}else{
				$erro = 2;
			}
		}else{
			$erro = 1;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'    => 'erro',
				'recarrega' => 'false',
				'msg' 	  => 'Por favor, tente novamente mais tarde. Erro nº '.$erro
			));
		} else {
			DB::commit();
			return response()->json(array('status'	=> 'sucesso',
										'recarrega' => 'true',
										'msg' 		=> 'A Comunicação de venda foi inserida com sucesso.'));
		}

	}

	public function listar_modelos(Request $request, detran_modelo_veiculo $detran_modelo_veiculo)
	{
		$detran_modelo_veiculo = $detran_modelo_veiculo->where('id_marca_veiculo', '=', $request->id_marca_veiculo)
													   ->orderBy('no_modelo_veiculo')
													   ->get();
		return response()->json($detran_modelo_veiculo);
	}

	public function listar_veiculo(Request $request, detran_veiculo $detran_veiculo)
	{
		$detran_veiculo = $detran_veiculo->where('placa', '=', $request->placa)->first();

		if ( count($detran_veiculo) > 0 )
		{
			$detran_veiculo = array_merge( $detran_veiculo->toArray() , $detran_veiculo->modelo->toArray() );
		}

		return response()->json($detran_veiculo);
	}

	public function inserirTelefonesPartes( $id_pessoa, $arrayTelefone )
	{
		foreach ( $arrayTelefone as $telefone )
		{
			$dadosTelefone 	= explode("#", $telefone);
			$id_telefone	= $dadosTelefone[0];
			$tipo_telefone	= $dadosTelefone[1];
			$ddd_telefone	= $dadosTelefone[2];
			$telefone		= $dadosTelefone[3];

			if ( $id_telefone == 0 )
			{
				$novo_telefone 								= new telefone();
				$novo_telefone->id_tipo_telefone	  		= (int) $tipo_telefone;
				$novo_telefone->id_classificacao_telefone 	= 1;
				$novo_telefone->nu_ddi	  					= 55 ;
				$novo_telefone->nu_ddd 	  					= (int) $ddd_telefone;
				$novo_telefone->nu_telefone 	  			= (int) $telefone;
				$novo_telefone->in_registro_ativo 	  		= 'S';
				$novo_telefone->dt_cadastro 	  			= Carbon::now();

				if ($novo_telefone->save())
				{
					$pessoa_telefone 						= new pessoa_telefone();
					$pessoa_telefone->id_pessoa				= $id_pessoa;
					$pessoa_telefone->id_telefone			= $novo_telefone->id_telefone;
					$pessoa_telefone->dt_cadastro 	  		= Carbon::now();
					if (! $pessoa_telefone->save())
					{
						DB::rollback();
						return false;
					}
				}else{
					DB::rollback();
					return false;
				}
			}
		}
		return true;
	}

	public function inserirPessoasParte( $request, $tipo_pessoa )
	{
		$nova_pessoa            		= new pessoa();

		$nova_pessoa->no_pessoa 		= $request['no_'.$tipo_pessoa];
		$nova_pessoa->tp_pessoa 		= $request['tp_pessoa_'.$tipo_pessoa];
		$nova_pessoa->nu_cpf_cnpj 		= preg_replace( '#[^0-9]#', '', $request['nu_cpf_cnpj_'.$tipo_pessoa]);
		$nova_pessoa->no_email_pessoa   = $request['no_email_'.$tipo_pessoa];
		$nova_pessoa->tp_sexo 			= 'N';

		if ( $request['tp_pessoa_'.$tipo_pessoa] == 'J')
		{
			$nova_pessoa->no_fantasia 	= $request['no_'.$tipo_pessoa];
			$nova_pessoa->tp_sexo 		= 'N';
		}else{
			$nova_pessoa->nu_rg 		= preg_replace( '#[^0-9]#', '', $request[''.$tipo_pessoa]);
		}

		if (! $nova_pessoa->save()) {
			DB::rollback();
			return false;
		}
		return $nova_pessoa->id_pessoa;
	}

	public function inserirDocumento( $id_operacao_veiculo, $id_operacao_veiculo_parte, $id_tipo_arquivo_grupo_produto, $arquivo)
	{
		$destino = '../storage/app/public/detran/comunicacao_venda/' . $id_operacao_veiculo;

		if (!File::isDirectory($destino))
		{
			File::makeDirectory($destino, 0777, true, true);
		}

		$nome_arquivo = $id_operacao_veiculo.'_'.$id_operacao_veiculo_parte.'_'.remove_caracteres($arquivo->getClientOriginalName());

		if ( $arquivo->move($destino, $nome_arquivo) )
		{
			$novo_arquivo_grupo_produto 								= new arquivo_grupo_produto();
			$novo_arquivo_grupo_produto->id_grupo_produto 				= $this::ID_GRUPO_PRODUTO;
			$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto 	= $id_tipo_arquivo_grupo_produto;
			$novo_arquivo_grupo_produto->no_arquivo						= $nome_arquivo;
			$novo_arquivo_grupo_produto->no_local_arquivo 				= 'detran/comunicacao_venda/' . $id_operacao_veiculo;
			$novo_arquivo_grupo_produto->id_usuario_cad 				= Auth::User()->id_usuario;
			$novo_arquivo_grupo_produto->no_extensao 					= $arquivo->getClientOriginalExtension();

			if ($novo_arquivo_grupo_produto->save())
			{
				$nova_operacao_veiculo_arquivo_grupo 							= new detran_operacao_veiculo_arquivo_grupo();
				$nova_operacao_veiculo_arquivo_grupo->id_operacao_veiculo 		= $id_operacao_veiculo;
				$nova_operacao_veiculo_arquivo_grupo->id_arquivo_grupo_produto 	= $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
				$nova_operacao_veiculo_arquivo_grupo->id_operacao_veiculo_parte = $id_operacao_veiculo_parte;
				if (! $nova_operacao_veiculo_arquivo_grupo->save())
				{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
		return true;
	}

}
?>