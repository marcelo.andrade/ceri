<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use League\Flysystem\Config;
use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;


class ContatoController extends Controller
{
    public function index(Request $request)
	{
		return view('email.contato-interno', compact('dados'));
    }

	public function enviar_contato(Request $request)
	{
		$telefone =  ( count(Auth::User()->pessoa->telefones) > 0 ) ? Auth::User()->pessoa->telefones[0]['nu_telefone'] : '-';

		$dados =  	[
			'nome' 		=> Auth::User()->no_usuario,
			'email' 	=> Auth::User()->email_usuario,
			'sexo' 		=> (Auth::User()->pessoa->tp_sexo 	== 'M') ? "Masculino" : "Feminino",
			'pessoa' 	=> (Auth::User()->pessoa->tp_pessoa == 'F') ? "Física" 	  : "Jurídica",
			'telefone' 	=> $telefone,
			'tipo' 		=> Auth::User()->pessoa->tipo_pessoa->no_tipo_pessoa,
			'titulo' 	=> $request->titulo,
			'descricao' => $request->descricao,
		];

		switch ( $request->area )
		{
			case "suporte":
				$email = "contatocerims@sagres.com.br";
				$area  = "Suporte";
			break;
			case "financeiro":
				$email = "contatocerims@sagres.com.br";
				$area  = "Financeiro";
			break;
			case "reclamacao":
				$email = "contatocerims@sagres.com.br";
				$area  = "Reclamação";
			break;
			case "sugestao":
				$email = "contatocerims@sagres.com.br";
				$area  = "Sugestão";
			break;
		}

        Mail::send('email.envio-contato-interno', ['dados' => $dados], function ($mail) use ($request, $email, $area) {
            $mail->to($email, 'CERI - '.$area)
                ->from('naoresponda@cerims.com.br',$area)
                ->replyTo(Auth::User()->email_usuario, Auth::User()->no_usuario)
                ->subject('CERI - Formulário de contato');
        });

        if (count(Mail::failures()) > 0) {
            return response()->json(['status'=> 'erro',
                'recarrega' => false,
                'msg' => 'Erro ao enviar solicitacação'
            ]);
        } else {
            return response()->json(['status'=> 'sucesso',
                'recarrega' => true,
                'msg' => 'Solicitação enviada com sucesso. Você receberá sua resposta no prazo de até 24h no seu gerenciador de e-mail!'
            ]);
        }
    }


	public function fale_conosco() {
		return view('fale-conosco');
    }
	public function enviar_fale_conosco(Request $request) {
		$dados = ['nome' => $request->nome,
				  'email' => $request->email,
				  'telefone' => $request->telefone,
				  'titulo' => $request->titulo,
				  'descricao' => $request->descricao];

		switch ($request->area) {
			case "suporte":
				$email = "contatocerims@sagres.com.br";
				$area  = "Suporte";
				break;
			case "financeiro":
				$email = "contatocerims@sagres.com.br";
				$area  = "Financeiro";
				break;
			case "reclamacao":
				$email = "contatocerims@sagres.com.br";
				$area  = "Reclamação";
				break;
			case "sugestao":
				$email = "contatocerims@sagres.com.br";
				$area  = "Sugestão";
				break;
		}

		Mail::send('email.envio-fale-conosco', ['dados' => $dados], function ($mail) use ($request, $email, $area){
			$mail->to($email, 'CERI - '.$area)
			     ->from('naoresponda@cerims.com.br',$area)
			     ->replyTo($request->email, $request->nome)
				 ->subject('CERI - Formulário de contato');
		});

        if (count(Mail::failures()) > 0) {
            return response()->json(['status'=> 'erro',
                                     'recarrega' => false,
                                     'msg' => 'Erro ao enviar solicitacação'
                                    ]);
        } else {
            return response()->json(['status'=> 'sucesso',
                                     'recarrega' => true,
                                     'msg' => 'Solicitação enviada com sucesso. Você receberá sua resposta no prazo de até 24h no seu gerenciador de e-mail!'
                                    ]);
        }
    }
}
