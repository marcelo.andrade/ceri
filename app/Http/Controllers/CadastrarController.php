<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;

use App\estado;
use App\nacionalidade;
use App\estado_civil;
use App\tipo_serventia;
use App\vara_tipo;
use App\comarca;

class CadastrarController extends Controller {

	public function index() {
		return view('cadastrar');
	}
	
	public function cadastrar_usuario(estado $estado, nacionalidade $nacionalidade, estado_civil $estado_civil) {
		$todosEstados = $estado->orderBy('no_estado')->get();
		$todasNacionalidades = $nacionalidade->orderBy('no_nacionalidade')->get();
		$todosEstadosCivis = $estado_civil->orderBy('no_estado_civil')->get();
		return view('cadastrar-usuario',compact('todosEstados','todasNacionalidades','todosEstadosCivis'));
	}
	
	public function cadastrar_cartorio(estado $estado, nacionalidade $nacionalidade, tipo_serventia $tipo_serventia) {
		$todosEstados = $estado->orderBy('no_estado')->get();
		$todasNacionalidades = $nacionalidade->orderBy('no_nacionalidade')->get();
		$todosTiposServentia = $tipo_serventia->orderBy('nu_ordem')->get();
		return view('cadastrar-cartorio',compact('todosEstados','todasNacionalidades','todosEstadosCivis','todosTiposServentia'));
	}
	
	public function cadastrar_judiciario(estado $estado, nacionalidade $nacionalidade, vara_tipo $vara_tipo, comarca $comarca) {
		$todosEstados = $estado->orderBy('no_estado')->get();
		$todasNacionalidades = $nacionalidade->orderBy('no_nacionalidade')->get();
		$todosVaraTipos = $vara_tipo->orderBy('nu_ordem')->get();
		$todasComarcas = $comarca->orderBy('nu_ordem')->get();
		return view('cadastrar-judiciario',compact('todosEstados','todasNacionalidades','todosEstadosCivis','todosVaraTipos','todasComarcas'));
	}

	public function reenviar_confirmacao() {
		return view('reenviar-confirmacao');
	}
}
