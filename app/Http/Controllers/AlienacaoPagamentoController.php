<?php
namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\pedido_pessoa;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use ZipArchive;
use Image;
use Session;

use App\pedido;
use App\alienacao_pagamento;
use App\alienacao_pagamento_alienacao;
use App\alienacao;
use App\alienacao_valor;
use App\estado;
use App\situacao_pedido_grupo_produto;
use App\alienacao_valor_repasse;
use App\alienacao_valor_repasse_lote;
use Excel;

class AlienacaoPagamentoController extends Controller {

    const ID_GRUPO_PRODUTO = 7;
	const ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP = 1;
	const ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF = 2;
	const ID_SITUACAO_PAGAMENTO_NAOAPROVADO = 3;
	const ID_SITUACAO_PAGAMENTO_APROVADO = 4;
	const ID_SITUACAO_ALIENACAO_EMPROCESSAMENTO = 55;
	const ID_SITUACAO_ALIENACAO_FINALIZADO = 57;
	const ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTO = 65;
	const ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTOFINALIZ = 66;
    const ID_TIPO_PESSOA_CAIXA  = 8;
    const ID_TIPO_PESSOA_ANOREG = 9;
    const ID_TIPO_PESSOA_SUPORTE = 13;
    const ID_REPASSE_CAIXA = 1;
    const ID_REPASSE_ANOREG = 3;
    const ID_SITUACAO_PEDIDO_GRUPO_PRODUTO = 54;
    const ID_PRODUTO = 19;
    const ID_PRODUTO_ITEM_SERVICOS_ANOREG = 28;

	public function __construct(alienacao_pagamento $alienacao_pagamento, CeriFuncoes $ceriFuncoes) {
		$this->alienacao_pagamento=$alienacao_pagamento;

		if (count(Auth::User()->usuario_pessoa)>0) {
			$this->pessoa_ativa = Session::get('pessoa_ativa');
            $this->usuario = Auth::User();

            if(Session::has('troca_pessoa')) {
                $this->usuario = $this->pessoa_ativa->pessoa->getUsuarioRelacionado()->usuario;
            }

			$this->id_tipo_pessoa = $this->pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $this->pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote){
	    $class = $this;

        switch ($this->id_tipo_pessoa) {
			case 8: //caixa
                $id_repasse = 1; //repasse caixa
                $pagamentos_pendentes = $this->getAlienacoes($id_repasse);

                return view('servicos.alienacao-pagamento.geral-alienacao-pagamento',compact('this','class','request','pagamentos','pagamentos_pendentes'));
                break;
            case 9: case 13://anoreg e suporte
              /*$pagamentos_pendentes = $this->alienacao_pagamento->where('id_usuario_cad',Auth::User()->id_usuario)
                ->whereIn('id_situacao_alienacao_pagamento',array($this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP,$this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF))
                ->get();*/
                $id_repasse = 3; //repasse anoreg
                $pagamentos_pendentes = $this->getAlienacoes($id_repasse);

            $pagamentos = $this->alienacao_pagamento->where('id_usuario_cad',$this->usuario->id_usuario)
                ->whereIn('id_situacao_alienacao_pagamento',array($this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO,$this::ID_SITUACAO_PAGAMENTO_APROVADO))
                ->get();
                return view('servicos.alienacao-pagamento.geral-alienacao-pagamento',compact('this','class','request','pagamentos','pagamentos_pendentes'));
                break;
			case 2: case 10:
            $pagamentos_pendentes = $this->alienacao_pagamento
                                                            ->select(
                                                                'alienacao_pagamento.id_alienacao_pagamento',
                                                                'alienacao_pagamento.id_situacao_alienacao_pagamento',
                                                                'alienacao_pagamento.va_pagamento',
                                                                'alienacao_pagamento.va_pago',
                                                                'alienacao_pagamento.dt_pagamento',
                                                                'alienacao_pagamento.no_arquivo_comprovante',
                                                                'alienacao_pagamento.no_local_arquivo_comprovante',
                                                                'alienacao_pagamento.no_extensao_comprovante',
                                                                'alienacao_pagamento.id_usuario_cad',
                                                                'alienacao_pagamento.dt_cadastro',
                                                                'alienacao_pagamento.id_serventia'
                                                            )
                                                            ->join('alienacao_pagamento_alienacao','alienacao_pagamento_alienacao.id_alienacao_pagamento','=','alienacao_pagamento.id_alienacao_pagamento')
                                                            ->join('alienacao_valor','alienacao_valor.id_alienacao','=','alienacao_pagamento_alienacao.id_alienacao')
                                                            ->where('alienacao_pagamento.id_serventia',$this->pessoa_ativa->pessoa->serventia->id_serventia/*Auth::User()->usuario_serventia->id_serventia*/)
                                                            ->where('alienacao_pagamento.id_situacao_alienacao_pagamento',$this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF)
                                                            ->where('alienacao_valor.id_produto_item','<>', DB::raw('28'))
                                                            ->groupBy(
                                                                'alienacao_pagamento.id_alienacao_pagamento',
                                                                'alienacao_pagamento.id_situacao_alienacao_pagamento',
                                                                'alienacao_pagamento.va_pagamento',
                                                                'alienacao_pagamento.va_pago',
                                                                'alienacao_pagamento.dt_pagamento',
                                                                'alienacao_pagamento.no_arquivo_comprovante',
                                                                'alienacao_pagamento.no_local_arquivo_comprovante',
                                                                'alienacao_pagamento.no_extensao_comprovante',
                                                                'alienacao_pagamento.id_usuario_cad',
                                                                'alienacao_pagamento.dt_cadastro',
                                                                'alienacao_pagamento.id_serventia'
                                                            )
                                                            ->get();

				$pagamentos = $this->alienacao_pagamento
                                                        ->select(
                                                            'alienacao_pagamento.id_alienacao_pagamento',
                                                            'alienacao_pagamento.id_situacao_alienacao_pagamento',
                                                            'alienacao_pagamento.va_pagamento',
                                                            'alienacao_pagamento.va_pago',
                                                            'alienacao_pagamento.dt_pagamento',
                                                            'alienacao_pagamento.no_arquivo_comprovante',
                                                            'alienacao_pagamento.no_local_arquivo_comprovante',
                                                            'alienacao_pagamento.no_extensao_comprovante',
                                                            'alienacao_pagamento.id_usuario_cad',
                                                            'alienacao_pagamento.dt_cadastro',
                                                            'alienacao_pagamento.id_serventia'
                                                        )
                                                        ->join('alienacao_pagamento_alienacao','alienacao_pagamento_alienacao.id_alienacao_pagamento','=','alienacao_pagamento.id_alienacao_pagamento')
                                                        ->join('alienacao_valor','alienacao_valor.id_alienacao','=','alienacao_pagamento_alienacao.id_alienacao')
                                                        ->where('alienacao_pagamento.id_serventia',$this->pessoa_ativa->pessoa->serventia->id_serventia/*Auth::User()->usuario_serventia->id_serventia*/)
                                                        ->whereIn('alienacao_pagamento.id_situacao_alienacao_pagamento',array($this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO,$this::ID_SITUACAO_PAGAMENTO_APROVADO))
                                                        ->where('alienacao_valor.id_produto_item','<>', DB::raw('28'))
                                                        ->groupBy(
                                                            'alienacao_pagamento.id_alienacao_pagamento',
                                                            'alienacao_pagamento.id_situacao_alienacao_pagamento',
                                                            'alienacao_pagamento.va_pagamento',
                                                            'alienacao_pagamento.va_pago',
                                                            'alienacao_pagamento.dt_pagamento',
                                                            'alienacao_pagamento.no_arquivo_comprovante',
                                                            'alienacao_pagamento.no_local_arquivo_comprovante',
                                                            'alienacao_pagamento.no_extensao_comprovante',
                                                            'alienacao_pagamento.id_usuario_cad',
                                                            'alienacao_pagamento.dt_cadastro',
                                                            'alienacao_pagamento.id_serventia'
                                                        )
                                                        ->orderBy('dt_pagamento')->get();


                $pagamentos_receber = $this->getPagamentosNaoEfetuados();

		    	return view('servicos.alienacao-pagamento.serventia-alienacao-pagamento',compact('this','class','request','pagamentos','pagamentos_pendentes', 'pagamentos_receber'));
				break;
		}
    }
	
	public function novo(Request $request,alienacao $alienacao,  estado $estado, situacao_pedido_grupo_produto $situacao)
    {
        $cidades   = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();

        $situacoes = $situacao->where('in_registro_ativo','S')
                              ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
                              ->orderBy('nu_ordem')->get();

        $alienacoes = $this->get_alienacao_valor($alienacao, '');

		if ($request->isMethod('post'))
		{
			if ($request->dt_inicio!='' and $request->dt_fim!='') {
				$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
				$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
                $alienacoes->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
			}
			if ($request->id_situacao_pedido_grupo_produto>0) {
                $alienacoes->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
			}
			if ($request->protocolo_pedido > 0){
                $alienacoes->where('pedido.protocolo_pedido',$request->protocolo_pedido);
			}
			if($request->numero_contrato>0){
                $alienacoes->where('alienacao.numero_contrato',$request->numero_contrato);
			}

            if ($request->id_serventia>0)
            {
                $alienacoes->where('serventia.id_serventia','=',$request->id_serventia);
            }

		}
		//$alienacoes->whereIn('serventia.id_serventia',['4','6']);

		//$alienacoes->where('alienacao.numero_contrato','155551905694');
		//echo $alienacoes->orderBy('alienacao.dt_cadastro','desc')->toSql();exit();
		$alienacoes = $alienacoes->orderBy('alienacao.dt_cadastro','desc')->get();

		if ($request->acao == "pesquisar") {
            return view('servicos.alienacao-pagamento.geral-alienacao-pagamento-notificacoes-historico', compact('request', 'alienacoes', 'cidades', 'situacoes'));
        }else{
            return view('servicos.alienacao-pagamento.geral-alienacao-pagamento-novo', compact('request', 'alienacoes', 'cidades', 'situacoes'));
        }
    }
	
	public function inserir_pagamento(Request $request, pedido $pedido, pedido_pessoa $pedido_pessoa, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote, alienacao $alienacao) {
        $erro = 0;
        $erro_loop = false;
		DB::beginTransaction();
		if (count($request->id_alienacao)>0)
		{
            if ($this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG )
            {
                foreach ($request->id_alienacao as $key => $alienacoes)
                {

                    /* FOI RETIRADO PORQUE ESTE PAGMENTO NÃO FOI DISTRIBUIDO PARA A SERVENTIA - SEG 07/06/2017

                     * $novo_pagamento = new alienacao_pagamento();
                    $novo_pagamento->va_pagamento = 0;
                    $novo_pagamento->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_COMP;
                    $novo_pagamento->id_serventia = $key;*/

                   // if ($novo_pagamento->save())
                   // {
                        $valor_total = 0;
                        foreach ($alienacoes as $key_id_repasse => $valor_repasse) {

                            foreach ($valor_repasse as $key_vl_repasse => $vl_repasse) {
                                foreach ($vl_repasse as $id_alienacao) {

                                    //buscando valores alienacao_valor
                                    $resultado_aliencao_valor = $this->get_alienacao_valor($alienacao, $id_alienacao);

                                    foreach ($resultado_aliencao_valor as $aliencao_valor)
                                    {
                                       /* $valor_total += $aliencao_valor->va_total;

                                        //para atender a individualizacao dos produtos dentro da tabela de alienacao_pagamento_alienacao

                                        $nova_alienacao_pagamento_alienacao                          = new alienacao_pagamento_alienacao();
                                        $nova_alienacao_pagamento_alienacao->id_alienacao            = $id_alienacao;
                                        $nova_alienacao_pagamento_alienacao->id_alienacao_pagamento  = $novo_pagamento->id_alienacao_pagamento;
                                        $nova_alienacao_pagamento_alienacao->id_alienacao_valor      = $aliencao_valor->id_alienacao_valor;
                                        $nova_alienacao_pagamento_alienacao->dt_cadastro             = Carbon::now();

                                        if (!$nova_alienacao_pagamento_alienacao->save()) {
                                            $erro_loop = true;
                                        }*/

                                        $nova_alienacao_valor = new alienacao_valor();

                                        //dando baixa no pagamento de prestação de serviço da ANOREG
                                        $nova_alienacao_valor->where('id_alienacao_valor', $aliencao_valor->id_alienacao_valor)
                                                            ->update(['in_registro_aberto' => 'N']);

                                    }

                                    //atualizando o valor do pagmento da tabela  alienacao_pagamento
                                    //$novo_pagamento->va_pagamento = $valor_total;

                                }
                                /*if (!$novo_pagamento->save()) {
                                    $erro_loop = true;
                                }*/
                            }
                        }
                   // }
                }

            }

            //variaveis de controle para insert abaixo

            $va_repasse_lote        = 0;
            $nu_quantidade_lote     = 0;
            $arrayAlienacaoRepase   = [];

            foreach ($request->id_alienacao as $key => $alienacoes)
            {
                foreach ($alienacoes as $key_id_repasse => $valor_repasse)
                {
                    foreach ($valor_repasse as $key_vl_repasse => $vl_repasse )
                    {
                        $nova_alienacao_valor_repasse                           = new alienacao_valor_repasse();
                        $nova_alienacao_valor_repasse->id_alienacao_valor       = $key_id_repasse;
                        $nova_alienacao_valor_repasse->id_repasse               = ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG ) ? 3 : 1;
                        $nova_alienacao_valor_repasse->va_repasse               = $key_vl_repasse;
                        $nova_alienacao_valor_repasse->dt_repasse               = Carbon::now();
                        $nova_alienacao_valor_repasse->id_usuario_repasse       = Auth::User()->id_usuario;
                        $nova_alienacao_valor_repasse->id_usuario_cad           = Auth::User()->id_usuario;
                        $nova_alienacao_valor_repasse->save();

                        if (!$nova_alienacao_valor_repasse->save()) {
                            $erro_loop = true;
                        }

                        $va_repasse_lote    += $key_vl_repasse;
                        $nu_quantidade_lote++;
                        $arrayAlienacaoRepase[] =   $nova_alienacao_valor_repasse->id_alienacao_valor_repasse;

                    }
                }
            }



            //if ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_CAIXA )
            //{
                $protocoloPedido                                    = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

                $pedido->id_usuario                                 =  Auth::User()->id_usuario;
                $pedido->id_usuario_cad                             =  Auth::User()->id_usuario;
                $pedido->id_situacao_pedido_grupo_produto           =  $this::ID_SITUACAO_PEDIDO_GRUPO_PRODUTO;
                $pedido->id_produto                                 =  $this::ID_PRODUTO;
                $pedido->protocolo_pedido                           =  $protocoloPedido[0]->f_geraprotocolo;
                $pedido->dt_pedido                                  =  Carbon::now();
                $pedido->dt_cadastro                                =  Carbon::now();
                $pedido->nu_quantidade                              =  1;
                $pedido->de_pedido                                  =  'Repasse - Notificação (Alienação Fiduciária)';
                $pedido->id_pessoa_origem                           =  $this->id_pessoa;

                if (!$pedido->save()) {
                    $erro_loop = true;
                }

                $pedido_pessoa->id_pedido = $pedido->id_pedido;
                $pedido_pessoa->id_pessoa = $this->id_pessoa;

                if (!$pedido_pessoa->save()) {
                    $erro_loop = true;
                }


                //salvando a tabela de lote
                $alienacao_valor_repasse_lote->id_repasse                       =  ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG ) ? 3 : 1;
                $alienacao_valor_repasse_lote->id_situacao_alienacao_pagamento  =  1;
                $alienacao_valor_repasse_lote->protocolo_pedido                 =  $protocoloPedido[0]->f_geraprotocolo;
                $alienacao_valor_repasse_lote->nu_quantidade_lote               =  $nu_quantidade_lote;
                $alienacao_valor_repasse_lote->va_repasse_lote                  =  $va_repasse_lote;
                $alienacao_valor_repasse_lote->dt_repasse_lote                  = Carbon::now();
                $alienacao_valor_repasse_lote->id_usuario_repasse               = Auth::User()->id_usuario;
                $alienacao_valor_repasse_lote->id_usuario_cad                   = Auth::User()->id_usuario;
                $alienacao_valor_repasse_lote->dt_cadastro                      = Carbon::now();

                if (!$alienacao_valor_repasse_lote->save()) {
                    $erro_loop = true;
                }

                //atualizar o valor do numero do lote
                //montando auto_incremento de acordo com a sequence do banco

                $alienacao_valor_repasse_lote->numero_lote      = $alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
                if (!$alienacao_valor_repasse_lote->save()){
                    $erro_loop = true;
                }

                //atualizando a tabela alienacao_valor_repasse como o codigo da tabela alienacao_valor_repasse_lote
                $atualizar_alienacao_valor_repasse = new alienacao_valor_repasse();

                $atualizar_alienacao_valor_repasse->whereIn('id_alienacao_valor_repasse', $arrayAlienacaoRepase)
                    ->update(['id_alienacao_valor_repasse_lote' => $alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote]);
            //}

            //controle da variavel de loop/insert
            if ($erro_loop) {
                $erro = 2;
            }


        }
		
		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O pagamento foi inserido com sucesso.'));
		}
	}

    public function detalhes(Request $request, alienacao_valor_repasse $alienacao_valor_repasse) {
        if ($request->id_alienacao_pagamento>0) {
            $alienacao_pagamento = $this->alienacao_pagamento;
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

            foreach ( $alienacao_pagamento->alienacoes as $alienacao )
            {
                $alienacao->alienacao_pedido->pedido->visualizar_notificacoes();
            }

            $id_alienacao_valor = $alienacao_pagamento->alienacao_pagamento[0]->id_alienacao_valor;

            $periodo_pagamento = $alienacao_valor_repasse->select('avr.dt_repasse as dt_fim',
                                                            DB::raw('case
                                                                     when (select avrl.dt_repasse_lote as dt_inicio 
                                                                           from ceri.alienacao_valor_repasse_lote avrl
                                                                           inner join ceri.alienacao_valor_repasse avr2 on (avr2.id_alienacao_valor_repasse_lote = avrl.id_alienacao_valor_repasse_lote)
                                                                           inner join ceri.alienacao_valor av2          on (av2.id_alienacao_valor = avr2.id_alienacao_valor)
                                                                           where avrl.id_alienacao_valor_repasse_lote <  avr.id_alienacao_valor_repasse_lote
                                                                                and avrl.id_repasse      = 1
                                                                                and av2.id_produto_item <> 28
                                                                           order by avrl.id_alienacao_valor_repasse_lote desc
                                                                           limit 1 
                                                                           )  is null Then to_date(\'20/03/2017 17:56:10\', \'dd-mm-yyyy HH24:MI:SS\') -- 20/03/2017 17:56:10
                                                                     else (select avrl.dt_repasse_lote as dt_inicio 
                                                                           from ceri.alienacao_valor_repasse_lote avrl
                                                                           inner join ceri.alienacao_valor_repasse avr2 on (avr2.id_alienacao_valor_repasse_lote = avrl.id_alienacao_valor_repasse_lote)
                                                                           inner join ceri.alienacao_valor av2          on (av2.id_alienacao_valor               = avr2.id_alienacao_valor)
                                                                           where avrl.id_alienacao_valor_repasse_lote <  avr.id_alienacao_valor_repasse_lote
                                                                                and avrl.id_repasse       = 1
                                                                                and av2.id_produto_item <> 28
                                                                           order by avrl.id_alienacao_valor_repasse_lote desc
                                                                           limit 1 
                                                                           )  
                                                                    end 
                                                                ')
                                                        )
                                                            ->from('alienacao_valor_repasse as avr')
                                                            ->join('ceri.alienacao_valor as av','av.id_alienacao_valor','=', 'avr.id_alienacao_valor')
                                                            ->where('av.id_alienacao_valor', '=', $id_alienacao_valor)
                                                            ->where('avr.id_repasse', '=', DB::raw('1'))
                                                            ->where('av.id_produto_item', '<>', DB::raw('28'))
                                                            ->get();

            $id_alienacao_pagamento =  $request->id_alienacao_pagamento;
            return view('servicos.alienacao-pagamento.geral-alienacao-pagamento-detalhes',compact('request','alienacao_pagamento', 'id_alienacao_pagamento', 'periodo_pagamento'));
        }
    }
	
	public function novo_comprovante(Request $request)
    {
        if ($request->id_alienacao_pagamento>0) {
            $alienacao_pagamento = $this->alienacao_pagamento;
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);
            return view('servicos.alienacao-pagamento.geral-alienacao-pagamento-novo-comprovante',compact('request','alienacao_pagamento'));
        }

    }

    public function novo_comprovante_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {
        $alienacao_valor_repasse_lote = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);
        return view('servicos.alienacao-pagamento.caixa-alienacao-pagamento-novo-comprovante',compact('request','alienacao_valor_repasse_lote'));
    }
	
	public function inserir_comprovante(Request $request) {
		if ($request->id_alienacao_pagamento>0) {
            $erro = 0;

            DB::beginTransaction();

            $nome_arquivo = remove_caracteres($request->no_arquivo_comprovante->getClientOriginalName());

            $alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
            $alienacao_pagamento->va_pago = converte_float($request->va_pago);
            $alienacao_pagamento->dt_pagamento = Carbon::createFromFormat('d/m/Y', $request->dt_pagamento);
            $alienacao_pagamento->no_arquivo_comprovante = $nome_arquivo;
            $alienacao_pagamento->no_local_arquivo_comprovante = 'alienacoes-pagamentos/' . $alienacao_pagamento->id_alienacao_pagamento;
            $alienacao_pagamento->no_extensao_comprovante = $request->no_arquivo_comprovante->getClientOriginalExtension();
            $alienacao_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_AGUARDANDO_CONF;

            if ($alienacao_pagamento->save()) {
                $destino = '../storage/app/public/alienacoes-pagamentos/' . $alienacao_pagamento->id_alienacao_pagamento;
                if (!File::isDirectory($destino)) {
                    File::makeDirectory($destino);
                }

                if (!$request->no_arquivo_comprovante->move($destino, $nome_arquivo)) {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }

            //inserindo notificacao
            if (count($alienacao_pagamento->alienacoes) > 0)
            {
                foreach ($alienacao_pagamento->alienacoes as $alienacao)
                {
                    $pedido                     = new pedido();
                    $pedido_selecionado         = $pedido->find($alienacao->alienacao_pedido->pedido->id_pedido);
                    $pedido_selecionado->inserir_notificacao($this->id_pessoa,$pedido_selecionado->pedido_pessoa_atual->id_pessoa,'Uma nova notificação foi enviada. Protocolo '.$pedido_selecionado->protocolo_pedido);

                }
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O comprovante foi salvo com sucesso.'));
			}
		}
	}

    public function inserir_comprovante_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {
        if ( $request->id_alienacao_valor_repasse_lote>0 )
        {
            $erro = 0;
            DB::beginTransaction();
            $arquivo 					    = $request->no_arquivo_comprovante;
            $nome_arquivo 				    = 'comprovante_'.strtolower(remove_caracteres($arquivo->getClientOriginalName()));

            $alienacao_valor_repasse_lote   = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);

            $alienacao_valor_repasse_lote->no_arquivo_comprovante           = $nome_arquivo;
            $alienacao_valor_repasse_lote->id_situacao_alienacao_pagamento  = 4;
            $alienacao_valor_repasse_lote->no_local_arquivo_comprovante     = 'public/alienacoes-pagamentos/'.$alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
            $alienacao_valor_repasse_lote->no_extensao_comprovante          = $arquivo->getClientOriginalExtension();
            $alienacao_valor_repasse_lote->id_usuario_comprovante           =  Auth::User()->id_usuario;
            $alienacao_valor_repasse_lote->dt_comprovante                   = Carbon::now();


            if ($alienacao_valor_repasse_lote->save()) {
                $destino = '../storage/app/public/alienacoes-pagamentos/'.$alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
                if (!File::isDirectory($destino)) {
                    File::makeDirectory($destino);
                }

                if (!$request->no_arquivo_comprovante->move($destino, $nome_arquivo)) {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }


            // Tratamento do retorno
            if ($erro>0) {
                DB::rollback();
                return response()->json(array('status'=> 'erro',
                    'recarrega' => 'false',
                    'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
            } else {
                DB::commit();
                return response()->json(array('status'=> 'sucesso',
                    'recarrega' => 'true',
                    'msg' => 'O comprovante foi salvo com sucesso.'));
            }
        }
    }
	
	public function aprovar_pagamento(Request $request) {
		if ($request->id_alienacao_pagamento>0) {
			$erro = 0;
	
			DB::beginTransaction();

			$alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
			$alienacao_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_APROVADO;
		
			if ($alienacao_pagamento->save()) {
				if (count($alienacao_pagamento->alienacoes)>0) {
					$erro_loop = false;


					//cada alienacao_pagamento contem n alienacoes
                    //precisamos depois inserir dentro do mesmo foreach o for logo abaixo

					foreach ($alienacao_pagamento->alienacoes as $alienacao)
					{

					   // echo $alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto;exit();
						switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
							case $this::ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTO:
								$nova_situacao = $this::ID_SITUACAO_ALIENACAO_EMPROCESSAMENTO;
								break;
							case $this::ID_SITUACAO_ALIENACAO_AGUARDANDOPAGAMENTOFINALIZ:
								$nova_situacao = $this::ID_SITUACAO_ALIENACAO_FINALIZADO;
								break;
                            default:
                                $nova_situacao = $alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto;
                                break;
						}

						$pedido = new pedido();

						if (!$pedido->where('id_pedido',$alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $nova_situacao])) {
							$erro_loop = true;
						}

						/*$alienacao_valor = new alienacao_valor();

						if (!$alienacao_valor->where('id_alienacao',$alienacao->id_alienacao)
                                             ->update(['id_alienacao_pagamento' => $alienacao_pagamento->id_alienacao_pagamento])) {
							$erro_loop = true;
						}*/
					}

					//atualizando cada linha do produto de casa alienacao
                     foreach ( $alienacao_pagamento->alienacao_pagamento as $alienacao_pagamento_alienacao )
                     {
                         $nova_alienacao_valor = new  alienacao_valor();
                         if (!$nova_alienacao_valor->where('id_alienacao_valor', $alienacao_pagamento_alienacao->id_alienacao_valor)
                                                     ->update([
                                                                'id_alienacao_pagamento'  => $alienacao_pagamento_alienacao->id_alienacao_pagamento,
                                                                'in_registro_aberto'      => 'N',
                                                              ])) {
                             $erro_loop = true;
                         }

                     }


					if ($erro_loop) {
						$erro = 2;
					}
				} else {
					$erro = 1;
				}
			} else {
				$erro = 1;
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O pagamento foi aprovado com sucesso.'));
			}
		}
	}
	
	public function reprovar_pagamento(Request $request) {
		if ($request->id_alienacao_pagamento>0) {
			$erro = 0;
	
			DB::beginTransaction();

			$alienacao_pagamento = $this->alienacao_pagamento->find($request->id_alienacao_pagamento);
			$alienacao_pagamento->id_situacao_alienacao_pagamento = $this::ID_SITUACAO_PAGAMENTO_NAOAPROVADO;

			if (!$alienacao_pagamento->save()) {
				$erro = 1;
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O pagamento foi reprovado com sucesso.'));
			}
		}
	}
	
	public function comprovante(Request $request, alienacao_pagamento $alienacao_pagamento) {
		if ($request->id_alienacao_pagamento>0) {
			$alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

			if ($alienacao_pagamento) {
				if ($alienacao_pagamento->no_arquivo_comprovante!='') {
					$url_download = URL::to('/arquivos/download/alienacao-pagamento/'.$alienacao_pagamento->id_alienacao_pagamento);
	
					return response()->json(array('status' => 'sucesso',
												  'view'=>view('servicos.alienacao-pagamento.serventia-alienacao-pagamento-comprovante',compact('alienacao_pagamento'))->render(),
												  'download'=>'true',
												  'url_download'=>$url_download));
				} else {
					return response()->json(array('status' => 'erro',
										  		  'recarrega' => 'false',
										  		  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
				}
			} else {
				return response()->json(array('status' => 'erro',
										 	  'recarrega' => 'false',
										  	  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '));
			}
		}
	}
	public function arquivo_comprovante(Request $request, alienacao_pagamento $alienacao_pagamento) {
		if ($request->id_alienacao_pagamento>0) {
			$alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

			$arquivo = Storage::get('/public/'.$alienacao_pagamento->no_local_arquivo_comprovante.'/'.$alienacao_pagamento->no_arquivo_comprovante);
			
			$mimes = array('pdf' => 'application/pdf',
						   'jpg' => 'image/jpeg',
						   'png' => 'image/png',
						   'bmp' => 'image/bmp',
						   'gif' => 'image/gif');
						   
			return response($arquivo, 200)->header('Content-Type', $mimes[$alienacao_pagamento->no_extensao_comprovante])
										  ->header('Content-Disposition', 'inline; filename="'.$alienacao_pagamento->no_arquivo_comprovante.'"');
		}
	}
	public function download_comprovante(Request $request, alienacao_pagamento $alienacao_pagamento) {
		if ($request->id_alienacao_pagamento>0) {
			$alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

			$no_arquivo_comprovante = $alienacao_pagamento->no_arquivo_comprovante;
			
			$arquivo = Storage::get('/public/'.$alienacao_pagamento->no_local_arquivo_comprovante.'/'.$no_arquivo_comprovante);
			return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo_comprovante.'"');
		}
	}

    //comum para servicos e emolumentos
    public function download_comprovante_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote) {
        if ($request->id_alienacao_valor_repasse_lote>0) {
            $alienacao_valor_repasse_lote = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);

            $no_arquivo_comprovante = $alienacao_valor_repasse_lote->no_arquivo_comprovante;

            $arquivo = Storage::get(''.$alienacao_valor_repasse_lote->no_local_arquivo_comprovante.'/'.$no_arquivo_comprovante);
            return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo_comprovante.'"');
        }
    }

    //comum para servicos e emolumentos
	public function detalhe_historico_pagamento_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {
        if ($request->id_alienacao_valor_repasse_lote>0){


            $alienacoes = $alienacao_valor_repasse_lote
                                ->select(
                                            'pedido.protocolo_pedido',
                                            'pedido.dt_pedido',
                                            'serventia.no_serventia',
                                            'alienacao.numero_contrato',
                                             DB::Raw('ceri.f_lista_devedor_alienacao(alienacao.id_alienacao, \'\', \' / \') as no_devedores'),
                                             DB::Raw('sum(alienacao_valor_repasse.va_repasse) as va_repasse'),
                                            'alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote',
                                            'alienacao_valor_repasse_lote.no_arquivo_comprovante',
                                            'alienacao_valor_repasse_lote.va_repasse_lote',
                                            'alienacao_valor_repasse_lote.dt_repasse_lote',
                                            'alienacao.id_alienacao'
                                    )
                                ->join('alienacao_valor_repasse','alienacao_valor_repasse.id_alienacao_valor_repasse_lote','=','alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote')
                                ->join('alienacao_valor','alienacao_valor.id_alienacao_valor','=','alienacao_valor_repasse.id_alienacao_valor')
                                ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
                                ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                ->join('alienacao','alienacao.id_alienacao','=','alienacao_pedido.id_alienacao')
                                ->join('serventia','serventia.id_serventia','=','alienacao.id_serventia')
                                ->where('alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote', '=', $request->id_alienacao_valor_repasse_lote)
                                ->groupBy(  'pedido.protocolo_pedido',
                                            'pedido.dt_pedido',
                                            'alienacao.numero_contrato',
                                            'alienacao.id_alienacao',
                                            'serventia.no_serventia',
                                            'alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote',
                                            'alienacao_valor_repasse_lote.va_repasse_lote',
                                            'alienacao_valor_repasse_lote.dt_repasse_lote'
                                )
                                ->get();


            //$alienacoes = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);
            return view('servicos.alienacao-pagamento.caixa-alienacao-pagamento-detalhe-historico',compact('request','alienacoes'));
        }

    }

    //comum para servicos e emolumentos
    public function gerar_recibo_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {

        if ( $request->id_alienacao_valor_repasse_lote>0)
        {

            $class      = $this;
            $alienacao_valor_repasse_lote  = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);

            $destino    = '/alienacoes-pagamentos/'.$alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote;
            $no_arquivo = 'recibo_pagamento_lote_'.$alienacao_valor_repasse_lote->protocolo_pedido.'.pdf';

            Storage::makeDirectory('/public'.$destino);

            $titulo         = 'Recibo pagamento: '.$alienacao_valor_repasse_lote->protocolo_pedido;
            $nome_projeto   = ( $request->origem == 'alienacao-pagamento' ) ? 'Prestação de Serviço Anoreg/MS' : 'Emolumentos';
            $pdf        = PDF::loadView('pdf.alienacao-pagamento-recibo-caixa', compact('alienacao_valor_repasse_lote', 'titulo', 'nome_projeto'));


            $pdf->save(storage_path('app/public'.$destino.'/'.$no_arquivo));

            return response()->json(array('view'=>view('servicos.alienacao-pagamento.geral-alienacao-pagamento-recibo',compact('alienacao_valor_repasse_lote'))->render()));
        }
    }

    //comum para servicos e emolumentos
    public function imprimir_recibo_caixa(Request $request, alienacao_valor_repasse_lote $alienacao_valor_repasse_lote)
    {
        if ( $request->id_alienacao_valor_repasse_lote>0)
        {
            $alienacao_valor_repasse_lote  = $alienacao_valor_repasse_lote->find($request->id_alienacao_valor_repasse_lote);

           // echo $request->segment(2);;exit();

            $nome_arquivo = 'recibo_pagamento_lote_'.$alienacao_valor_repasse_lote->protocolo_pedido.'.pdf';

            $arquivo        = Storage::get('/public/alienacoes-pagamentos/'.$alienacao_valor_repasse_lote->id_alienacao_valor_repasse_lote.'/'.$nome_arquivo);

            return response($arquivo, 200)->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', 'inline; filename="'.$nome_arquivo.'"');
        }
    }

    public function get_alienacao_valor($alienacao, $id_alienacao)
    {
        if ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_CAIXA )
        {
            $alienacoes = $alienacao->select('alienacao.*', 'alienacao_valor.*')
                                    ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                    ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                    ->join('serventia','alienacao.id_serventia','=','serventia.id_serventia')
                                    ->join('alienacao_valor', function($join){
                                        $join->on('alienacao_valor.id_alienacao', '=', 'alienacao.id_alienacao')
                                            ->on('alienacao_valor.id_produto_item', '=',  DB::raw('28'));
                                    })
                                    ->leftjoin('alienacao_valor_repasse', function($left){
                                        $left->on('alienacao_valor_repasse.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                            ->on('alienacao_valor_repasse.id_repasse', '=',  DB::raw('1'));
                                    })
                                    ->whereNull('alienacao_valor_repasse.dt_repasse');

        }else if ( $this->id_tipo_pessoa == $this::ID_TIPO_PESSOA_ANOREG){
            $alienacoes = $alienacao->select('alienacao.*', 'alienacao_valor.*')
                                    ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                    ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                    ->join('serventia','alienacao.id_serventia','=','serventia.id_serventia')
                                    ->join('alienacao_valor', function($join){
                                        $join->on('alienacao_valor.id_alienacao', '=', 'alienacao.id_alienacao')
                                            ->on('alienacao_valor.id_produto_item', '=',  DB::raw('28'));  // Prestação de Serviços ANOREG/MS
                                    })
                                    ->join('alienacao_valor_repasse as avr1', function($join){
                                        $join->on('avr1.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                            ->on('avr1.id_repasse', '=',  DB::raw('1')); //Repasse da Caixa para ANOREG-MS
                                    })
                                    ->leftjoin('alienacao_valor_repasse as avr2', function($left){
                                        $left->on('avr2.id_alienacao_valor', '=', 'alienacao_valor.id_alienacao_valor')
                                            ->on('avr2.id_repasse', '=',  DB::raw('3')); //Repasse da ANOREG-MS para Taxa de serviço da Central
                                    })

                                    ->whereNull('avr2.dt_repasse');
        }

        if ($id_alienacao != "")
        {
            $alienacoes->where("alienacao.id_alienacao", '=', $id_alienacao);
            $alienacoes = $alienacoes->orderBy('alienacao.dt_cadastro','desc')->get();
        }



        return $alienacoes;
    }

    public function getAlienacoes( $id_repasse )
    {
        $alienacao_valor_repasse_lote   = new alienacao_valor_repasse_lote();
        $pagamentos_pendentes           = $alienacao_valor_repasse_lote->select('alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote',
                                                'alienacao_valor_repasse_lote.numero_lote',
                                                'alienacao_valor_repasse_lote.protocolo_pedido',
                                                'alienacao_valor_repasse_lote.nu_quantidade_lote',
                                                'alienacao_valor_repasse_lote.va_repasse_lote',
                                                'alienacao_valor_repasse_lote.dt_repasse_lote',
                                                'alienacao_valor_repasse_lote.dt_comprovante',
                                                'alienacao_valor_repasse_lote.dt_cadastro',
                                                'alienacao_valor_repasse_lote.no_arquivo_comprovante',
                                                'usuario.no_usuario'
                                            )
                                            ->join('usuario','usuario.id_usuario','=','alienacao_valor_repasse_lote.id_usuario_cad')
                                            ->join('alienacao_valor_repasse','alienacao_valor_repasse.id_alienacao_valor_repasse_lote','=','alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote')
                                            ->join('alienacao_valor','alienacao_valor.id_alienacao_valor','=','alienacao_valor_repasse.id_alienacao_valor')
                                            ->where('alienacao_valor.id_produto_item','=',DB::Raw('28'))
                                            ->where('alienacao_valor_repasse_lote.id_repasse', '=', $id_repasse)
                                            ->orderby('alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote', 'desc')
                                            ->groupBy(  'alienacao_valor_repasse_lote.id_alienacao_valor_repasse_lote',
                                                'alienacao_valor_repasse_lote.numero_lote',
                                                'alienacao_valor_repasse_lote.protocolo_pedido',
                                                'alienacao_valor_repasse_lote.nu_quantidade_lote',
                                                'alienacao_valor_repasse_lote.va_repasse_lote',
                                                'alienacao_valor_repasse_lote.dt_repasse_lote',
                                                'alienacao_valor_repasse_lote.dt_comprovante',
                                                'alienacao_valor_repasse_lote.dt_cadastro',
                                                'alienacao_valor_repasse_lote.no_arquivo_comprovante',
                                                'usuario.no_usuario'
                                            )
                                            ->get();
        return $pagamentos_pendentes;
    }
    public function getPagamentosNaoEfetuados(){
        $alienacao = new alienacao();
        $alienacoes = $alienacao->select(
            'pedido.protocolo_pedido',
            'pedido.dt_pedido',
            'alienacao.numero_contrato',
            'alienacao.id_alienacao',
            'serventia.no_serventia',
            'fase_grupo_produto.no_fase',
            'etapa_fase.no_etapa',
            'acao_etapa.no_acao',
            DB::Raw('ceri.f_lista_devedor_alienacao(alienacao.id_alienacao, \'\', \' / \') as no_devedores'),
            DB::Raw('sum(alienacao_valor.va_total) as va_total'),
            'alienacao_pedido.id_pedido',
            'serventia.id_serventia'

        )
            ->join('alienacao_valor','alienacao_valor.id_alienacao','=','alienacao.id_alienacao')
            ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao_valor.id_alienacao')
            ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
            ->join('serventia','serventia.id_serventia','=','alienacao.id_serventia')
            ->join('alienacao_andamento_situacao', function($join){
                $join->on('alienacao_andamento_situacao.id_alienacao_pedido',   '=', 'alienacao_pedido.id_alienacao_pedido')
                    ->on('alienacao_andamento_situacao.in_registro_ativo','=' ,DB::raw("'S'"));
            })
            ->join('fase_grupo_produto','fase_grupo_produto.id_fase_grupo_produto','=','alienacao_andamento_situacao.id_fase_grupo_produto')
            ->join('etapa_fase','etapa_fase.id_etapa_fase','=','alienacao_andamento_situacao.id_etapa_fase')
            ->join('acao_etapa','acao_etapa.id_acao_etapa','=','alienacao_andamento_situacao.id_acao_etapa')
            ->leftjoin('alienacao_valor_repasse', function($join){
                $join->on('alienacao_valor_repasse.id_alienacao_valor',   '=', 'alienacao_valor.id_alienacao_valor')
                    ->on('alienacao_valor_repasse.id_repasse', '=',  DB::raw('1'));
            })
            ->where('alienacao_valor.id_produto_item', '<>', DB::raw('28'))
            ->whereNull('alienacao_valor_repasse.dt_repasse')
            ->whereRaw('ceri.f_verifica_valor_aprovado_alienacao (alienacao.id_alienacao, alienacao_valor.id_produto_item) = \'S\'')
            ->where('serventia.id_serventia', '=',$this->pessoa_ativa->pessoa->serventia->id_serventia)
            ->groupBy(  'pedido.protocolo_pedido',
                'pedido.dt_pedido',
                'alienacao.numero_contrato',
                'alienacao.id_alienacao',
                'serventia.no_serventia',
                'fase_grupo_produto.no_fase',
                'etapa_fase.no_etapa',
                'acao_etapa.no_acao',
                'alienacao_pedido.id_pedido',
                'serventia.id_serventia'
            )
            ->orderBy('alienacao.dt_cadastro','desc')
            ->get();

        return $alienacoes;
    }
    public function gerar_arquivo_previsualizacao(Request $request, alienacao_pagamento $alienacao_pagamento) {
        if ($request->id_alienacao_pagamento>0) {
            $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);
            $tipo = $request->tipo;

            if($request->dt_pagamento != ''){
                $campos['dt_pagamento'] = $request->dt_pagamento;
            }

            $request->session()->flash('prev_campos',$campos);

            return response()->json(array('status'=>'sucesso','view'=>view('servicos.alienacao-pagamento.geral-alienacao-pagamento-previzualizacao',compact('alienacao_pagamento','tipo'))->render()));
        }
    }
    public function gerar_arquivo_render(Request $request, alienacao_pagamento $alienacao_pagamento, $tipo=NULL, $id_alienacao_pagamento=NULL)
    {
        if ($id_alienacao_pagamento>0) {
            $alienacao_pagamento = $alienacao_pagamento->find($id_alienacao_pagamento);
            $campos = $request->session()->get('prev_campos');

            $pdf = $this->gera_pdf($tipo,$alienacao_pagamento,$campos);

            return $pdf->stream();
        }
    }
    public function gera_pdf($tipo, $alienacao_pagamento,$campos=NULL)
    {
        switch ($tipo) {
            case 'notificacao-alienacao-fiduciaria':
                $titulo = 'RELATÓRIO DE PAGAMENTO';
                $pdf = PDF::loadView('pdf.alienacao-pagamento-relatorio',compact('titulo','alienacao_pagamento','campos'));
                break;
        }

        return $pdf;
    }
    public function salvar_relatorio_pagamento(Request $request, alienacao_pagamento $alienacao_pagamento)
    {
        $alienacao_pagamento = $alienacao_pagamento->find($request->id_alienacao_pagamento);

        $time = Carbon::now()->format('d-m-y_h-i-s');

        Excel::create('relatorio_'.$time, function($excel) use ($alienacao_pagamento) {

            $excel->sheet('Notificações', function($sheet) use ($alienacao_pagamento) {

                $sheet->loadView('xls.alienacao-pagamento-relatorio', array('alienacao_pagamento' => $alienacao_pagamento));
                $sheet->setColumnFormat(array('A'=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'C' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'E' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1));
                $sheet->getStyle('A1:F5')->applyFromArray(['fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color' => ['rgb' => 'E0EEEE']]]);
                $sheet->getStyle('A1:D1' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
                $sheet->setAutoSize(true);
            });
        })->export('xls');
    }
}
