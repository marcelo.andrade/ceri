<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;

use App\usuario;
use App\usuario_senha;
use App\estado;

class EstadoController extends Controller {

	public function listarCidades(Request $request, estado $estado) {
		$cidades = $estado->find($request->id_estado)->cidades()->orderBy('no_cidade')->get();
		return response()->json($cidades);
	}
	
}
