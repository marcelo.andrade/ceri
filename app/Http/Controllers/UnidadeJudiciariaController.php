<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Hash;

use App\unidade_judiciaria_divisao;

class UnidadeJudiciariaController extends Controller {
	
	public function __construct() {
	}

	public function listar_divisoes(Request $request, unidade_judiciaria_divisao $unidade_judiciaria_divisao) {
		if ($request->id_unidade_judiciaria>0) {
			$divisoes = $unidade_judiciaria_divisao->where('id_unidade_judiciaria',$request->id_unidade_judiciaria)->get();
		} else {
			$divisoes = $unidade_judiciaria_divisao->get();
		}
		return response()->json($divisoes);
	}
}
