<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;

use App\estado;
use App\processo;
use App\proprietario_bem_imovel;
use App\bem_imovel;
use App\natureza_acao;
use App\tipo_zona_imovel;
use App\bem_imovel_proprietario;

class BemImovelController extends Controller {
	
	public function __construct(bem_imovel $bem_imovel) {
		$this->bem_imovel=$bem_imovel;
	}
	
	public function novo(Request $request, estado $estado, tipo_zona_imovel $tipo_zona_imovel) {
		$cidades = $estado->find(env('ID_ESTADO'))->cidades()->orderBy('no_cidade')->get();
		$tipos_imovel = $tipo_zona_imovel->where('in_registro_ativo','S')->orderBy('nu_ordem')->get();
		return view('servicos.judiciario/imovel-novo',compact('cidades','tipos_imovel'));
	}

	public function inserir(Request $request, proprietario_bem_imovel $proprietario) {
		$novo_bem_imovel = $this->bem_imovel;
		$novo_bem_imovel->matricula_bem_imovel = $request->matricula_bem_imovel;
//		$novo_bem_imovel->id_serventia = $request->id_serventia;
		$novo_bem_imovel->de_bem_imovel = $request->de_bem_imovel;
		$novo_bem_imovel->id_cidade = ($request->bemimovel_id_cidade > 0) ? $request->bemimovel_id_cidade : Auth::User()->pessoa->enderecos[0]->id_cidade;
		$novo_bem_imovel->no_endereco = $request->no_endereco;
		$novo_bem_imovel->no_bairro = $request->no_bairro;
		$novo_bem_imovel->id_usuario_cad = Auth::User()->id_usuario;
		$novo_bem_imovel->dt_cadastro = Carbon::now();
		$novo_bem_imovel->id_tipo_zona_imovel = $request->id_tipo_zona_imovel;
		
		if ($novo_bem_imovel->save()) {
			$i=0;
			if(count($request->a_nu_cpf_cnpj)>0)
			{
				foreach ($request->a_nu_cpf_cnpj as $nu_cpf_cnpj)
				{
					$nu_cpf_cnpj = preg_replace( '#[^0-9]#', '', $nu_cpf_cnpj);
					$busca_proprietario = $proprietario->where('nu_cpf_cnpj',$nu_cpf_cnpj)->first();
					if (count($busca_proprietario)>0) {
						$id_proprietario_bem_imovel = $busca_proprietario->id_proprietario_bem_imovel;
					} else {
						$novo_proprietario = new proprietario_bem_imovel();
						$novo_proprietario->nu_cpf_cnpj = $nu_cpf_cnpj;
						$novo_proprietario->tp_pessoa = $request->a_tp_pessoa[$i];
						$novo_proprietario->no_proprietario_bem_imovel = $request->a_no_proprietario_bem_imovel[$i];
						$novo_proprietario->id_usuario_cad = Auth::User()->id_usuario;
						$novo_proprietario->dt_cadastro = Carbon::now();
						$novo_proprietario->save();
						$id_proprietario_bem_imovel = $novo_proprietario->id_proprietario_bem_imovel;
					}
					if ($id_proprietario_bem_imovel>0) {
						$novo_bem_imovel_proprietario = new bem_imovel_proprietario();
						$novo_bem_imovel_proprietario->id_bem_imovel = $novo_bem_imovel->id_bem_imovel;
						$novo_bem_imovel_proprietario->id_proprietario_bem_imovel = $id_proprietario_bem_imovel;
						$novo_bem_imovel_proprietario->in_passivo_penhora = $request->a_in_passivo_penhora[$i];
						$novo_bem_imovel_proprietario->save();
					}
					$i++;
				}
			}
		}
		
		return response()->json($novo_bem_imovel);
	}
	
	public function total(Request $request) {
		$bens_imoveis = $this->bem_imovel->where('matricula_bem_imovel',$request->matricula_bem_imovel)->count();
		return $bens_imoveis;
	}

	public function detalhes(Request $request, estado $estado, tipo_zona_imovel $tipo_zona_imovel) {
		if ($request->id_bem_imovel>0) {
			$bem_imovel = $this->bem_imovel->find($request->id_bem_imovel);
			$cidades = $estado->find(env('ID_ESTADO'))->cidades()->orderBy('no_cidade')->get();
			$tipos_imovel = $tipo_zona_imovel->where('in_registro_ativo','S')->orderBy('nu_ordem')->get();
			return view('servicos.judiciario.imovel-detalhes',compact('bem_imovel','cidades','tipos_imovel'));
		}
	}

	public function detalhesJSON(Request $request) {
		//esta linha foi alterada a pedido do juiz do MS porque quer que toda penhora tenha o cadastro novo do imovel mesmo ele existindo - SEGISMAR CIENTE ECL -06102016
		//$bem_imovel = $this->bem_imovel->with('proprietarios')->where('matricula_bem_imovel',$request->matricula_bem_imovel)->first()
		if ($bem_imovel = $this->bem_imovel->with('proprietarios')->where('matricula_bem_imovel',$request->matricula_bem_imovel)->orderBy('id_bem_imovel', 'desc')->first()){
			return response()->json($bem_imovel);
		} else {
			return 'ERRO';
		}
	}
}
