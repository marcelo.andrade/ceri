<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\usuario;
use App\usuario_senha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use Validator;
use Auth;
use Hash;
use Mail;
use DB;
use ZipArchive;
use Storage;
use URL;
use Response;

use App\Library\CeriFuncoes;

use App\estado;
use App\pessoa;

class AcessarController extends Controller {

	public function acessar(Request $request) {
		$acessar = true;
                if ($request['g-recaptcha-response'] === ''){
                    $acessar = false;
                }
		if (!Auth::attempt(array('email_usuario' => strtolower(trim($request->usuario)), 'password' => $request->senha_usuario),false, false)) {
			if (!Auth::attempt(array('login' => $request->usuario, 'password' => $request->senha_usuario),false, false)) {
				$acessar = false;
			}
		}
		if ($acessar) {
			$usuario = Auth::getLastAttempted();
			if ($usuario->in_registro_ativo=='S' and $usuario->in_confirmado=='S' and $usuario->in_aprovado=='S') {
				if (count($usuario->usuario_pessoa)>0) {
					Auth::login($usuario, $request->lembrar_usuario); 
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$usuario->id_usuario.", 'AC', ".$usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$usuario->id_usuario.");"));

					$usuario_pessoas = $usuario->usuario_pessoa;
					Session::put('pessoa_ativa', $usuario_pessoas[0]);

					return redirect()->intended('/');
				} else {
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$usuario->id_usuario.", 'AC', ".$usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema não permitido (Usuário não possui vínculos)','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$usuario->id_usuario.");"));
					return Redirect::back()->withInput($request->except('senha_usuario'))
										   ->withErrors(['usuario' => '1005']);
				}
			} else {
				if ($usuario->in_registro_ativo=='N') {
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$usuario->id_usuario.", 'AC', ".$usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema não permitido (Usuário inativo)','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$usuario->id_usuario.");"));
					return Redirect::back()->withInput($request->except('senha_usuario'))
										   ->withErrors(['usuario' => '1004']);
				} elseif ($usuario->in_confirmado=='N') {
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$usuario->id_usuario.", 'AC', ".$usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema não permitido (Usuário não confirmado)','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$usuario->id_usuario.");"));
					return Redirect::back()->withInput($request->except('senha_usuario'))
										   ->withErrors(['usuario' => '1002']);
				} elseif ($usuario->in_aprovado=='N') {
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$usuario->id_usuario.", 'AC', ".$usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema não permitido (Usuário não aprovado)','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$usuario->id_usuario.");"));
					return Redirect::back()->withInput($request->except('senha_usuario'))
										   ->withErrors(['usuario' => '1003']);
				}
			}
        } else {
			$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, 1, 'ER', 1, 'Acesso ao sistema ERRO: Usuário ou senha inválidos Login usado: ".$request->usuario."' ,'Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, 1);"));

            if ($request['g-recaptcha-response'] === ''){
                return Redirect::back()->withInput($request->except('senha_usuario'))
                                                            ->withErrors(['usuario' => '999']);
            } else{
                return Redirect::back()->withInput($request->except('senha_usuario'))
                                                            ->withErrors(['usuario' => '1001']);
            }
	}
    }

	public function acessar_certificado(pessoa $pessoa) {
		$RestPki = new \Lacuna\RestPkiClient(env('RESTPKI_URL'), env('RESTPKI_TOKEN'));
		
		$auth = $RestPki->getAuthentication();
		if (env('APP_ENV')=='production') {
			$token = $auth->startWithWebPki(\Lacuna\StandardSecurityContexts::PKI_BRAZIL);
		} else {
			$token = $auth->startWithWebPki('803517ad-3bbc-4169-b085-60053a8f6dbf');
		}
		
		return view('acessar-certificado', compact('token'));
	}
	
	public function autenticar_certificado(Request $request) {
		$RestPki = new \Lacuna\RestPkiClient(env('RESTPKI_URL'), env('RESTPKI_TOKEN'));
		
		$auth = $RestPki->getAuthentication();
		$vr = $auth->completeWithWebPki($request->token);

		if ($vr->isValid()) {
			$certificado = $auth->getCertificate();
					
			$pessoa = new pessoa();
			$pessoa = $pessoa->where('nu_cpf_cnpj',$certificado->pkiBrazil->cpf)->first();
			
			if (count($pessoa)>0 and $certificado->pkiBrazil->cpf!='') {
				//
				Auth::loginUsingId($pessoa->usuario->id_usuario);
				
				$usuario_pessoas = $pessoa->usuario->usuario_pessoa;
                                Session::put('pessoa_ativa', $usuario_pessoas[0]);

				Session::put('certificado', $certificado);
				$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$pessoa->usuario->id_usuario.", 'AC', ".$pessoa->usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema','Acesso e-CPF', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$pessoa->usuario->id_usuario.");"));
				return redirect()->intended('/');
			} else {
				$empresa = new pessoa();
				$empresa = $empresa->where('nu_cpf_cnpj',$certificado->pkiBrazil->cnpj)->first();
			
				if (count($empresa)>0 and $certificado->pkiBrazil->cnpj!='') {
					//
					Auth::loginUsingId($empresa->usuario->id_usuario);

        	                        $usuario_pessoas = $empresa->usuario->usuario_pessoa;
	                                Session::put('pessoa_ativa', $usuario_pessoas[0]);

					Session::put('certificado', $certificado);
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".$empresa->usuario->id_usuario.", 'AC', ".$empresa->usuario->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso ao sistema','Acesso e-CPF', '".$_SERVER['REMOTE_ADDR']."', null, null, ".$empresa->usuario->id_usuario.");"));
					return redirect()->intended('/');
				} else {
					$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, 1, 'ER', 1, 'Acesso ao sistema ERRO: Empresa não encontrada e-CNPJ usado: ".$certificado->pkiBrazil->cnpj."' ,'Acesso e-CPF', '".$_SERVER['REMOTE_ADDR']."', null, null, 1);"));
					return Redirect::back()->withErrors(['certificado' => '6003']);
				}
				$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, 1, 'ER', 1, 'Acesso ao sistema ERRO: Pessoa não encontrada e-CPF usado: ".$certificado->pkiBrazil->cpf."' ,'Acesso e-CPF', '".$_SERVER['REMOTE_ADDR']."', null, null, 1);"));
				return Redirect::back()->withErrors(['certificado' => '6001']);
			}
			
			/*
			echo 'User certificate information:
			<ul>
				<li>Subject: '.$userCert->subjectName->commonName.'</li>
				<li>Email: '.$userCert->emailAddress.'</li>
				<li>
					ICP-Brasil fields
					<ul>
						<li>Tipo de certificado: '.$userCert->pkiBrazil->certificateType.'</li>
						<li>CPF: '.$userCert->pkiBrazil->cpf.'</li>
						<li>Responsavel: '.$userCert->pkiBrazil->responsavel.'</li>
						<li>Empresa: '.$userCert->pkiBrazil->companyName.'</li>
						<li>CNPJ: '.$userCert->pkiBrazil->cnpj.'</li>
					</ul>
				</li>
			</ul>';
			*/
		} else {
			/*
			$vrHtml = str_replace("\n", '<br/>', $vr);
			$vrHtml = str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;', $vrHtml);
			echo $vrHtml;
			*/
			return Redirect::back()->withErrors(['certificado' => '6002']);
		}
	}

    public function sair(){
    	Auth::logout();
		Session::flush();
    	return redirect('/');
    }

	public function lembrar_senha(){
		return view('lembrar-senha');
    }
    public function recuperar_senha($recuperar_token, usuario $usuario) {
    	if ($recuperar_token!='') {
			$usuario = $usuario->where('recuperar_token',$recuperar_token)->first();
			
			if ($usuario) {
				return view('recuperar-senha', compact('usuario','recuperar_token'));
			}
		}
		return redirect('/');
    }
	
	public function cadastrar(estado $estado) {
		$todosEstados = $estado->orderBy('no_estado')->get();
		return view('cadastrar',compact('todosEstados'));
	}
	
}
