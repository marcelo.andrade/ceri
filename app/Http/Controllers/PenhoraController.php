<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;
use App\pedido_produto_item;
use App\penhora_tipo_custa;
use App\tipo_custa;
use App\usuario;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Validator;
use Auth;
use DB;
use PDF;
use Carbon\Carbon;
use File;
use Storage;
use URL;
use ZipArchive;
use Session;

use App\estado;
use App\produto;
use App\penhora;
use App\serventia;
use App\pedido;
use App\situacao_pedido_grupo_produto;
use App\processo;
use App\bem_imovel;
use App\processo_parte;
use App\processo_parte_penhora;
use App\penhora_bem_imovel;
use App\penhora_prenotacao;
use App\penhora_custa;
use App\penhora_averbacao;
use App\penhora_exigencia;
use App\tipo_penhora;
use App\natureza_acao;
use App\documento_penhora;
use App\arquivo_grupo_produto;
use App\arquivo_grupo_produto_composicao;

class PenhoraController extends Controller {
	
	const ID_GRUPO_PRODUTO = 3;
	const ID_PRODUTO = 14;
	const ID_PRODUTO_ITEM = 23;
	const ID_SITUACAO_CADASTRADO = 22;
	const ID_SITUACAO_EMAPROVACAO = 25;
	const ID_SITUACAO_ENCAMINHADO = 26;
	const ID_SITUACAO_FINALIZADO = 27;

	public function __construct(penhora $penhora) {
		$this->penhora=$penhora;

		if (count(Auth::User()->usuario_pessoa)>0) {
			$pessoa_ativa = Session::get('pessoa_ativa');

			$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
			$this->id_pessoa = $pessoa_ativa->id_pessoa;
		} else {
			$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
			$this->id_pessoa = Auth::User()->id_pessoa;
		}
	}

    public function index(Request $request, estado $estado, produto $produto, serventia $serventia, situacao_pedido_grupo_produto $situacao, tipo_penhora $tipo_penhora){
		$log = DB::select(DB::raw("SELECT ceri.f_registrar_log_sistema (1, ".Auth::User()->id_usuario.", 'AC', ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.", 'Acesso a tela principal de penhora','Acesso Convencional', '".$_SERVER['REMOTE_ADDR']."', null, null, ".Auth::User()->usuario_pessoa[0]->pessoa->pessoa_modulo[0]->id_modulo.");"));

		switch ($this->id_tipo_pessoa) {
			case 4: case 11:
				$tipos = $tipo_penhora->orderBy('nu_ordem')->get();
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
									  ->orderBy('nu_ordem')->get();

				$todas_penhoras = $this->penhora->join('pedido','pedido.id_pedido','=','penhora.id_pedido')
                                                ->where('pedido.id_pessoa_origem','=',$this->id_pessoa);

				if ($request->isMethod('post')) {
					if ($request->id_tipo_penhora>0) {
						$todas_penhoras->where('penhora.id_tipo_penhora',$request->id_tipo_penhora);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_penhoras->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_penhoras->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_penhoras->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if ( $request->numero_processo>0){
						$todas_penhoras->where('penhora.numero_processo',$request->numero_processo);
					}
				}

				$todas_penhoras = $todas_penhoras->orderBy('penhora.dt_cadastro','desc')->get();
				
				$class = $this;

		    	return view('servicos.judiciario.penhora',compact('request','class','cidades','tipos','situacoes','todas_penhoras'));
				break;
			case 2:
				$tipos = $tipo_penhora->orderBy('nu_ordem')->get();
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
									  ->orderBy('nu_ordem')->get();

				$todas_penhoras = $this->penhora->join('pedido','pedido.id_pedido','=','penhora.id_pedido')
												->join('pedido_pessoa',function($join) {
													$join->on('pedido_pessoa.id_pedido', '=', 'pedido.id_pedido')
														 ->where('pedido_pessoa.id_pessoa','=',$this->id_pessoa);
											  	});
				
				if ($request->isMethod('post')) {
					if ($request->id_tipo_penhora>0) {
						$todas_penhoras->where('penhora.id_tipo_penhora',$request->id_tipo_penhora);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_penhoras->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_penhoras->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_penhoras->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if ( $request->numero_processo>0){
						$todas_penhoras->where('penhora.numero_processo',$request->numero_processo);
					}
				}

				$todas_penhoras = $todas_penhoras->orderBy('penhora.dt_cadastro','desc')->get();
				
				$class = $this;

		    	return view('servicos.cartorio.penhora',compact('request','class','tipos','situacoes','todas_penhoras'));
				break;
			case 13:
				$tipos = $tipo_penhora->orderBy('nu_ordem')->get();
				$situacoes = $situacao->where('in_registro_ativo','S')
									  ->where('id_grupo_produto',$this::ID_GRUPO_PRODUTO)
									  ->orderBy('nu_ordem')->get();

				$todas_penhoras = $this->penhora->join('pedido','pedido.id_pedido','=','penhora.id_pedido');

				if ($request->isMethod('post')) {
					if ($request->id_tipo_penhora>0) {
						$todas_penhoras->where('penhora.id_tipo_penhora',$request->id_tipo_penhora);
					}
					if ($request->dt_inicio!='' and $request->dt_fim!='') {
						$dt_inicio = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_inicio.' 00:00:00');
						$dt_fim = Carbon::createFromFormat('d/m/Y H:i:s',$request->dt_fim.' 23:59:59');
						$todas_penhoras->whereBetween('pedido.dt_pedido',array($dt_inicio,$dt_fim));
					}
					if ($request->id_situacao_pedido_grupo_produto>0) {
						$todas_penhoras->where('pedido.id_situacao_pedido_grupo_produto',$request->id_situacao_pedido_grupo_produto);
					}
					if ( $request->protocolo_pedido > 0){
						$todas_penhoras->where('pedido.protocolo_pedido',$request->protocolo_pedido);
					}
					if ( $request->numero_processo>0){
						$todas_penhoras->where('penhora.numero_processo',$request->numero_processo);
					}
				}

				$todas_penhoras = $todas_penhoras->orderBy('penhora.dt_cadastro','desc')->get();
				
				$class = $this;

		    	return view('servicos.judiciario.penhora',compact('request','class','cidades','tipos','situacoes','todas_penhoras'));
				break;
		}
    }

	public function nova(estado $estado, tipo_penhora $tipo_penhora, natureza_acao $natureza_acao, tipo_custa $tipo_custa) {
		$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		$serventias = $estado->find(env('ID_ESTADO'))->serventias_estado()->orderBy('serventia.no_serventia')->get();
		$tipos = $tipo_penhora->orderBy('nu_ordem')->get();
		$tipo_custa = $tipo_custa->orderBy('nu_ordem')->get();
		$naturezas = $natureza_acao->orderBy('nu_ordem','desc')->get();
		$penhora_token = str_random(30);
		return view('servicos.judiciario.penhora-nova',compact('cidades','serventias','tipos','situacoes','naturezas', 'tipo_custa','penhora_token'));
	}

	public function inserir(Request $request) {
		$erro = 0;

		DB::beginTransaction();

		foreach ($request->id_serventia as $id_serventia) {
			$protocolo_pedido = DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$this::ID_PRODUTO.");"));

			$novo_pedido = new pedido();
			$novo_pedido->id_usuario = Auth::User()->id_usuario;
			$novo_pedido->id_situacao_pedido_grupo_produto = $this::ID_SITUACAO_CADASTRADO;
			$novo_pedido->id_produto = $this::ID_PRODUTO;

			switch ($this->id_tipo_pessoa) {
				case 3:
					$novo_pedido->id_alcada = 1;
					break;
				case 4:
					$novo_pedido->id_alcada = 2;
					break;
			}

			$va_emitir_certidao = 39.15 * count($request->matriculas_cadastradas[$id_serventia]);

			$novo_pedido->protocolo_pedido = $protocolo_pedido[0]->f_geraprotocolo;
			$novo_pedido->dt_pedido = Carbon::now();
			$novo_pedido->nu_quantidade = 0;
			$novo_pedido->va_pedido = converte_float($va_emitir_certidao);
			$novo_pedido->id_usuario_cad = Auth::User()->id_usuario;
			$novo_pedido->dt_cadastro = Carbon::now();
			$novo_pedido->id_pessoa_origem = $this->id_pessoa;

			if ($novo_pedido->save()) {
				$serventia_selecionada = new serventia();
				$serventia_selecionada = $serventia_selecionada->find($id_serventia);

				if ($serventia_selecionada) {
					$novo_pedido->pessoas()->attach($serventia_selecionada->pessoa);
					$id_cidade = $serventia_selecionada->pessoa->enderecos[0]->cidade->id_cidade;
				} else {
					$erro = 30002;
					$id_cidade = 0;
				}

				$novo_pedido->inserir_notificacao($this->id_pessoa,$serventia_selecionada->id_pessoa,'Uma nova penhora foi inserida. Protocolo '.$protocolo_pedido[0]->f_geraprotocolo);

				$novo_pedido_produto_item = new pedido_produto_item();
				$novo_pedido_produto_item->id_pedido = $novo_pedido->id_pedido;
				$novo_pedido_produto_item->id_produto_item = $this::ID_PRODUTO_ITEM;
				$novo_pedido_produto_item->id_usuario_cad = Auth::User()->id_usuario;
				if (!$novo_pedido_produto_item->save()) {
					$erro = 30003;
				}

				$nova_penhora = new penhora();
				$nova_penhora->id_pedido = $novo_pedido->id_pedido;
				$nova_penhora->id_tipo_penhora = $request->id_tipo_penhora;
				$nova_penhora->id_usuario = Auth::User()->id_usuario;
				$nova_penhora->id_usuario_cad = Auth::User()->id_usuario;
				$nova_penhora->dt_cadastro = Carbon::now();
				$nova_penhora->dt_penhora = ($request->dt_penhora?Carbon::createFromFormat('d/m/Y', $request->dt_penhora):NULL);
				$nova_penhora->va_penhora = converte_float($request->va_penhora);
				$nova_penhora->va_emitir_certidao = converte_float($va_emitir_certidao);
				$nova_penhora->numero_processo = $request->numero_processo;
				$nova_penhora->id_tipo_custa = $request->id_tipo_custa;
				$nova_penhora->in_emitir_certidao = $request->in_emitir_certidao;
				if ($nova_penhora->save()) {
					$erro_loop = false;
					foreach ($request->matriculas_cadastradas[$id_serventia] as $matricula) {
						$novo_bem_imovel = new bem_imovel();
						$novo_bem_imovel->id_usuario_cad = Auth::User()->id_usuario;
						$novo_bem_imovel->id_cidade = $id_cidade;
						$novo_bem_imovel->matricula_bem_imovel = $matricula;
						$novo_bem_imovel->de_bem_imovel = 'Bem imóvel cadastrado pela tela de penhora';
						if ($novo_bem_imovel->save()) {
							$nova_penhora_bem_imovel = new penhora_bem_imovel();
							$nova_penhora_bem_imovel->id_penhora = $nova_penhora->id_penhora;
							$nova_penhora_bem_imovel->id_bem_imovel = $novo_bem_imovel->id_bem_imovel;
							$nova_penhora_bem_imovel->dt_cadastro = Carbon::now();
							if (!$nova_penhora_bem_imovel->save()) {
								$erro_loop = true;
							}
						} else {
							$erro_loop = true;
						}
					}
					if ($erro_loop) {
						$erro = 30005;
					}

					if ($request->session()->has('arquivos_'.$request->penhora_token)) {
						$destino = '/penhoras/'.$nova_penhora->id_penhora;
						$arquivos = $request->session()->get('arquivos_'.$request->penhora_token);
						$erro_loop_arq = false;
						$erro_loop_sql = false;
						foreach ($arquivos as $key => $arquivo) {
							$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'];
							Storage::makeDirectory('/public'.$destino_final);

							$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
							$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];
							
							$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
							$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
							$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
							$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
							$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
							$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
							$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
							$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
							if ($arquivo['dt_assinado']!='') {
								$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
							}
							if ($arquivo['id_usuario_certificado']>0) {
								$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
							}
							if (!empty($arquivo['no_arquivo_p7s'])) {
								$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
								$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
							}
							if ($novo_arquivo_grupo_produto->save()) {
								if (Storage::exists($origem_arquivo)) {
									if (Storage::exists($destino_arquivo)) {
										Storage::delete($destino_arquivo);
									}
									if (Storage::copy($origem_arquivo,$destino_arquivo)) {
										if (count($arquivo['no_arquivos_originais'])>0) {
											foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
												$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
												$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

												$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
												$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
												$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
												$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
												$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
												$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
												if ($novo_arquivo_grupo_produto_composicao->save()) {
													if (Storage::exists($origem_arquivo_original)) {
														if (Storage::exists($destino_arquivo_original)) {
															Storage::delete($destino_arquivo_original);
														}
														if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_sql = true;
												}
											}
										}
									} else {
										$erro_loop_arq = true;
									}
									$nova_penhora->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_sql = true;
							}
						}
						if ($erro_loop_arq) {
							$erro = 30007;
						}
						if ($erro_loop_sql) {
							$erro = 30008;
						}
					} else {
						$erro = 30006;
					}
				} else {
					$erro = 30004;
				}
			} else {
				$erro = 30001;
			}
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'A penhora foi inserida com sucesso.'));
		}
	}

	public function recibo(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			$arquivo_recibo = $pedido->penhora->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',14)->first();

			if (is_null($arquivo_recibo)) {
				$erro = 0;

				$destino = '/penhoras/'.$pedido->penhora->id_penhora.'/14';
				$no_arquivo = 'penhora_recibo_'.$pedido->protocolo_pedido.'.pdf';

				Storage::makeDirectory('/public'.$destino);

				$titulo = 'Recibo de penhora: '.$pedido->protocolo_pedido;
				$pdf = PDF::loadView('pdf.penhora-recibo', compact('pedido', 'titulo'));
				$pdf->save(storage_path('app/public'.$destino.'/'.$no_arquivo));

				$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
				$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
				$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = 14;
				$novo_arquivo_grupo_produto->no_arquivo = $no_arquivo;
				$novo_arquivo_grupo_produto->no_local_arquivo = $destino;
				$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
				$novo_arquivo_grupo_produto->no_extensao = 'pdf';
				$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size('/public'.$destino.'/'.$no_arquivo);
				$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app/public'.$destino.'/'.$no_arquivo));
				$novo_arquivo_grupo_produto->save();

				$pedido->penhora->arquivos_grupo()->attach($novo_arquivo_grupo_produto);

				$arquivo_recibo = $novo_arquivo_grupo_produto;
			}

			return response()->json(array('view'=>view('servicos.judiciario.penhora-recibo',compact('arquivo_recibo'))->render()));
		}
	}

	public function nova_resposta(Request $request, pedido $pedido, tipo_penhora $tipo_penhora, tipo_custa $tipo_custa) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			$tipos = $tipo_penhora->orderBy('nu_ordem')->get();
			$tipo_custa = $tipo_custa->orderBy('nu_ordem')->get();
			$arquivos = $pedido->penhora->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',9)->get();
			$arquivos_isencao = $pedido->penhora->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',10)->get();
			$penhora_token = str_random(30);

			$pedido->visualizar_notificacoes();

			return view('servicos.cartorio.penhora-resposta',compact('pedido','tipos','tipo_custa','arquivos','arquivos_isencao','penhora_token'));
		}
	}

	public function inserir_averbacao(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$erro = 0;

			DB::beginTransaction();

			$CeriFuncoes = new CeriFuncoes();

			$pedido = $pedido->find($request->id_pedido);

			$nova_averbacao = new penhora_averbacao();
			$nova_averbacao->id_penhora = $pedido->penhora->id_penhora;
			$nova_averbacao->de_resposta = $request->de_resposta;
			$nova_averbacao->id_usuario_cad = Auth::User()->id_usuario;

			if ($nova_averbacao->save()) {
				if ($request->session()->has('arquivos_'.$request->penhora_token)) {
					$destino = '/penhoras/'.$pedido->penhora->id_penhora;
					$arquivos = $request->session()->get('arquivos_'.$request->penhora_token);
					$erro_loop_arq = false;
					$erro_loop_sql = false;
					foreach ($arquivos as $key => $arquivo) {
						if (in_array($arquivo['id_tipo_arquivo_grupo_produto'],array(12,13))) {
							$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'].($arquivo['id_flex']!='0'?'/'.$arquivo['id_flex']:'');
							Storage::makeDirectory('/public'.$destino_final);

							$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
							$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];

							$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
							$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
							$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
							$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
							$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
							$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
							$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
							$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
							$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
							if ($arquivo['dt_assinado']!='') {
								$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
							}
							if ($arquivo['id_usuario_certificado']>0) {
								$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
							}
							if (!empty($arquivo['no_arquivo_p7s'])) {
								$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
								$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
							}
							if ($novo_arquivo_grupo_produto->save()) {
								if (Storage::exists($origem_arquivo)) {
									if (Storage::exists($destino_arquivo)) {
										Storage::delete($destino_arquivo);
									}
									if (Storage::copy($origem_arquivo,$destino_arquivo)) {
										if (count($arquivo['no_arquivos_originais'])>0) {
											foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
												$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
												$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

												$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
												$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
												$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
												$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
												$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
												$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
												$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
												if ($novo_arquivo_grupo_produto_composicao->save()) {
													if (Storage::exists($origem_arquivo_original)) {
														if (Storage::exists($destino_arquivo_original)) {
															Storage::delete($destino_arquivo_original);
														}
														if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
															$erro_loop_arq = true;
														}
													} else {
														$erro_loop_arq = true;
													}
												} else {
													$erro_loop_sql = true;
												}
											}
										}
									} else {
										$erro_loop_arq = true;
									}
								} else {
									$erro_loop_arq = true;
								}
							} else {
								$erro_loop_sql = true;
							}
						}
						switch ($arquivo['id_tipo_arquivo_grupo_produto']) {
							case 12: // Certidão averbada
								$penhora_bem_imovel = new penhora_bem_imovel();
								$penhora_bem_imovel = $penhora_bem_imovel->find($arquivo['id_flex']);
								$penhora_bem_imovel->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								break;
							case 13: // Penhora averbação
								$nova_averbacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								break;
						}
					}
					if ($erro_loop_arq) {
						$erro = 30103;
					}
					if ($erro_loop_sql) {
						$erro = 30104;
					}
				} else {
					$erro = 30102;
				}

				$pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $this::ID_SITUACAO_FINALIZADO]);

				
			} else {
				$erro = 30101;
			}
		} else {
			$erro = 30100;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			$pedido->inserir_notificacao($this->id_pessoa,$pedido->id_pessoa_origem,'Averbação da penhora protocolo '.$pedido->protocolo_pedido.' foi inserida.');

			if ($pedido->pessoa_origem->no_email_pessoa!='') {
				$url_email = URL::to('/servicos/penhora');
				Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
					$mail->to($pedido->pessoa_origem->no_email_pessoa, $pedido->pessoa_origem->no_pessoa)
						 ->subject('CERI - Nova notificação do pedido de penhora protocolo '.$pedido->protocolo_pedido);
				});
			}

			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'A averbação foi inserida com sucesso.'));
		}
	}

	public function inserir_prenotacao(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			DB::beginTransaction();

			$pedido = $pedido->find($request->id_pedido);

			$nova_prenotacao = new penhora_prenotacao();
			$nova_prenotacao->id_penhora = $pedido->penhora->id_penhora;
			$nova_prenotacao->codigo_prenotacao = $request->codigo_prenotacao;
			$nova_prenotacao->dt_prenotacao = Carbon::createFromFormat('d/m/Y',$request->dt_prenotacao);
			$nova_prenotacao->dt_vencimento_prenotacao = Carbon::createFromFormat('d/m/Y',$request->dt_prenotacao)->addDays(30);
			$nova_prenotacao->id_usuario_cad = Auth::User()->id_usuario;

			if ($nova_prenotacao->save()) {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'false',
											  'msg' => 'A prenotação foi salva com sucesso.'));
			} else {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro 14105'));
			}
		}
	}

	public function inserir_custa(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$erro = 0;

			DB::beginTransaction();

			$pedido = $pedido->find($request->id_pedido);

			$nova_custa = new penhora_custa();
			$nova_custa->id_penhora = $pedido->penhora->id_penhora;
			$nova_custa->de_custa = $request->de_custa;
			$nova_custa->va_custa = converte_float($request->va_custa);
			$nova_custa->id_usuario_cad = Auth::User()->id_usuario;

			if ($nova_custa->save()) {
				$dados = [
				    "va_pedido"                     => converte_float($request->va_custa),
                    "tp_movimentacao_financeira"    => 2,
                    "tp_movimentacao"               => 'S',
                    "id_forma_pagamento"            => 'null',
                    "v_id_pedido_desconto"          => 'null',
                    "v_va_desconto"                 => 0,
                    "v_in_desconto"                 => 'N',
                    "v_va_desconto_perc"            => 'null',
                    "v_id_compra_credito"           => $request->id_compra_credito
                ];

				$pedido->registrar_valor_pedido($dados);
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'false',
											  'msg' => 'As custas foram salvas com sucesso.'));
			} else {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro 14106'));
			}
		}
	}

	public function inserir_exigencia(Request $request, pedido $pedido, penhora_exigencia $penhora_exigencia, situacao_pedido_grupo_produto $situacao, usuario $usuario) {
		if ($request->id_pedido>0) {
			$erro = 0;

			DB::beginTransaction();

			$CeriFuncoes = new CeriFuncoes();

			$pedido = $pedido->find($request->id_pedido);
			
			$nova_exigencia = $penhora_exigencia;
			$nova_exigencia->id_penhora = $pedido->penhora->id_penhora;
			$nova_exigencia->de_resposta = $request->de_resposta;
			$nova_exigencia->id_usuario_cad = Auth::User()->id_usuario;
			switch($this->id_tipo_pessoa) {
				case 4:
					$nova_exigencia->id_alcada = 2;
					$id_nova_situacao = $this::ID_SITUACAO_ENCAMINHADO;
					break;
				case 11:
					$nova_exigencia->id_alcada = 11;
					$id_nova_situacao = $this::ID_SITUACAO_ENCAMINHADO;
					break;
				case 2:
					$nova_exigencia->id_alcada = 3;
					$id_nova_situacao = $this::ID_SITUACAO_EMAPROVACAO;
					break;
			}

			if ($nova_exigencia->save()) {
				switch ($request->tipo_resultado_envio) {
					case '1':
						$nova_exigencia->de_resultado = $request->de_resultado;
						$nova_exigencia->save();
						break;
					case '2': case '3':
						if ($request->session()->has('arquivos_'.$request->penhora_token)) {
							$destino = '/penhoras/'.$pedido->penhora->id_penhora;
							$arquivos = $request->session()->get('arquivos_'.$request->penhora_token);
							$erro_loop_arq = false;
							$erro_loop_sql = false;
							foreach ($arquivos as $key => $arquivo) {
								if ($arquivo['id_tipo_arquivo_grupo_produto']==11) {
									$destino_final = $destino.'/'.$arquivo['id_tipo_arquivo_grupo_produto'].'/'.$nova_exigencia->id_penhora_exigencia;
									Storage::makeDirectory('/public'.$destino_final);

									$origem_arquivo = $arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo'];
									$destino_arquivo = '/public'.$destino_final.'/'.$arquivo['no_arquivo'];

									$novo_arquivo_grupo_produto = new arquivo_grupo_produto();
									$novo_arquivo_grupo_produto->id_grupo_produto = $this::ID_GRUPO_PRODUTO;
									$novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = $arquivo['id_tipo_arquivo_grupo_produto'];
									$novo_arquivo_grupo_produto->no_arquivo = $arquivo['no_arquivo'];
									$novo_arquivo_grupo_produto->no_local_arquivo = $destino_final;
									$novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
									$novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($arquivo['no_arquivo']);
									$novo_arquivo_grupo_produto->in_ass_digital = $arquivo['in_assinado'];
									$novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']);
									$novo_arquivo_grupo_produto->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo']));
									if ($arquivo['dt_assinado']!='') {
										$novo_arquivo_grupo_produto->dt_ass_digital = $arquivo['dt_assinado'];
									}
									if ($arquivo['id_usuario_certificado']>0) {
										$novo_arquivo_grupo_produto->id_usuario_certificado = $arquivo['id_usuario_certificado'];
									}
									if (!empty($arquivo['no_arquivo_p7s'])) {
										$novo_arquivo_grupo_produto->no_arquivo_p7s = $arquivo['no_arquivo_p7s'];
										$novo_arquivo_grupo_produto->no_hash_p7s = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$arquivo['no_arquivo_p7s']));
									}
									if ($novo_arquivo_grupo_produto->save()) {
										if (Storage::exists($origem_arquivo)) {
											if (Storage::exists($destino_arquivo)) {
												Storage::delete($destino_arquivo);
											}
											if (Storage::copy($origem_arquivo,$destino_arquivo)) {
												if (count($arquivo['no_arquivos_originais'])>0) {
													foreach ($arquivo['no_arquivos_originais'] as $no_arquivo_original) {
														$origem_arquivo_original = $arquivo['no_local_arquivo'].'/'.$no_arquivo_original;
														$destino_arquivo_original = '/public'.$destino_final.'/'.$no_arquivo_original;

														$novo_arquivo_grupo_produto_composicao = new arquivo_grupo_produto_composicao();
														$novo_arquivo_grupo_produto_composicao->id_arquivo_grupo_produto = $novo_arquivo_grupo_produto->id_arquivo_grupo_produto;
														$novo_arquivo_grupo_produto_composicao->no_arquivo = $no_arquivo_original;
														$novo_arquivo_grupo_produto_composicao->no_local_arquivo = $destino_final;
														$novo_arquivo_grupo_produto_composicao->no_extensao = extensao_arquivo($no_arquivo_original);
														$novo_arquivo_grupo_produto_composicao->nu_tamanho_kb = Storage::size($arquivo['no_local_arquivo'].'/'.$no_arquivo_original);
														$novo_arquivo_grupo_produto_composicao->no_hash = hash_file('md5',storage_path('app'.$arquivo['no_local_arquivo'].'/'.$no_arquivo_original));
														$novo_arquivo_grupo_produto_composicao->id_usuario_cad = Auth::User()->id_usuario;
														if ($novo_arquivo_grupo_produto_composicao->save()) {
															if (Storage::exists($origem_arquivo_original)) {
																if (Storage::exists($destino_arquivo_original)) {
																	Storage::delete($destino_arquivo_original);
																}
																if (!Storage::copy($origem_arquivo_original,$destino_arquivo_original)) {
																	$erro_loop_arq = true;
																}
															} else {
																$erro_loop_arq = true;
															}
														} else {
															$erro_loop_sql = true;
														}
													}
												}
											} else {
												$erro_loop_arq = true;
											}
										} else {
											$erro_loop_arq = true;
										}
									} else {
										$erro_loop_sql = true;
									}
									$nova_exigencia->arquivos_grupo()->attach($novo_arquivo_grupo_produto);
								}
							}
							if ($erro_loop_arq) {
								$erro = 30108;
							}
							if ($erro_loop_sql) {
								$erro = 30109;
							}
						}
						break;
				}

				$pedido->where('id_pedido',$request->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $id_nova_situacao]);

			} else {
				$erro = 30106;
			}
		} else {
			$erro = 30100;
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			switch($this->id_tipo_pessoa) {
				case 4: case 7: case 11:
					$pedido->inserir_notificacao($this->id_pessoa,$pedido->pedido_pessoa_atual->id_pessoa,'Uma exigencia foi inserida na penhora protocolo '.$pedido->protocolo_pedido);

					if ($pedido->pedido_pessoa_atual->pessoa->no_email_pessoa!='') {
						$url_email = URL::to('/servicos/penhora');
						Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pedido_pessoa_atual->pessoa->no_pessoa], function ($mail) use ($request, $pedido) {
							$mail->to($pedido->pedido_pessoa_atual->pessoa->no_email_pessoa, $pedido->pedido_pessoa_atual->pessoa->no_pessoa)
								 ->subject('CERI - Nova notificação do pedido de penhora protocolo '.$pedido->protocolo_pedido);
						});
					}
					break;
				case 2:
					$pedido->inserir_notificacao($this->id_pessoa,$pedido->id_pessoa_origem,'Uma exigencia foi inserida na penhora protocolo '.$pedido->protocolo_pedido);

					if ($pedido->pessoa_origem->no_email_pessoa!='') {
						$url_email = URL::to('/servicos/penhora');
						Mail::send('email.notificacao-pedido', ['pedido' => $pedido, 'url_email' => $url_email, 'no_pessoa' => $pedido->pessoa_origem->no_pessoa], function ($mail) use ($request, $pedido) {
							$mail->to($pedido->pessoa_origem->no_email_pessoa, $pedido->pessoa_origem->no_pessoa)
								 ->subject('CERI - Nova notificação do pedido de penhora protocolo '.$pedido->protocolo_pedido);
						});
					}
					break;
			}

			DB::commit();
			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'A exigência foi inserida com sucesso.'));
		}
	}
	
	public function detalhes(Request $request, pedido $pedido, tipo_penhora $tipo_penhora, tipo_custa $tipo_custa) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);
			$tipos = $tipo_penhora->orderBy('nu_ordem')->get();
			$tipo_custa = $tipo_custa->orderBy('nu_ordem')->get();
			$arquivos = $pedido->penhora->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',9)->get();
			$arquivos_isencao = $pedido->penhora->arquivos_grupo()->where('id_tipo_arquivo_grupo_produto',10)->get();

			$pedido->visualizar_notificacoes();

			return view('servicos.judiciario.penhora-detalhes',compact('pedido','tipos','tipo_custa','arquivos','arquivos_isencao'));
		}
	}
	
	public function resultado(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			$pedido->visualizar_notificacoes();

			return view('servicos.judiciario.penhora-resultado',compact('pedido'));
		}
	}
	
	public function exigencias(Request $request, pedido $pedido) {
		if ($request->id_pedido>0) {
			$pedido = $pedido->find($request->id_pedido);

			$pedido->visualizar_notificacoes();

			$penhora_token = str_random(30);

			$class = $this;

			return view('servicos.judiciario.penhora-exigencias',compact('class','pedido','penhora_token'));
		}
	}
}
