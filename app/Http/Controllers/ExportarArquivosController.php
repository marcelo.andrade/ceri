<?php
namespace App\Http\Controllers;

use App\alienacao_arquivo_xml;
use App\exportar_aquivo;
use App\fase_arquivo_xml;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use Auth;
use DB;
use Carbon\Carbon;
use File;
use PDF;
use Storage;
use URL;
use Image;
use Session;
use XMLReader;
use DOMDocument;
use Excel;


class ExportarArquivosController extends Controller
{
    public function __construct()
    {
        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');

            $this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
            $this->id_pessoa = $pessoa_ativa->id_pessoa;
        } else {
            $this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
            $this->id_pessoa = Auth::User()->id_pessoa;
        }
    }

    public function novo_arquivo_xml_fases_caixa(alienacao_arquivo_xml $alienacao_arquivo_xml)
    {
        $token = str_random(30);
        $todas_arquivo_xml  = $alienacao_arquivo_xml->where("id_alienacao_arquivo_xml_tipo", "=", "2")->orderBy('id_alienacao_arquivo_xml', 'desc')->get();

        $class = $this;

        return view('exportar_arquivos.caixa_xml_fase.geral-xml-fase-caixa-historico', compact('class', 'token', 'todas_arquivo_xml'));
    }

    public function exportar_xml_fases_caixa(Request $request, exportar_aquivo $exportar_aquivo, fase_arquivo_xml $fase_arquivo_xml)
    {
        set_time_limit(0);
        ini_set("memory_limit",-1); //esta clausula sera retirada depois que rodar o primeiro lote

        /*
         * Atendendo a demanda
         * Por oportuno, solicitamos alterar, se possível, a nomenclatura do arquivo xml gerado, que deve seguir o padrão FASES_ ddmmyyyy_ANOREGMS, onde ddmmyyyy representa a data de geração do arquivo.
         * foi questionado que se o processo for gerado mais de uma vez no dia o arquivo sera sobreescrito.
         */
        //$no_arquivo                 = 'FASES_'.Carbon::now()->format('dmy_his').'_ANOREGMS.xml';
        $no_arquivo                 = 'FASES_'.Carbon::now()->format('dmY').'_ANOREGMS.xml';
        $no_diretorio               = '/public/exportar/caixa_fase/'.Carbon::now()->format('d-m-Y');

        Storage::makeDirectory($no_diretorio);

        $retorno_xml_fase_caixa = $exportar_aquivo->executar_xml_fases_caixa(
                                                                                Auth::User()->id_usuario,
                                                                                $this->id_tipo_pessoa,
                                                                                $no_arquivo, $no_diretorio
                                                                            );


        $id_alienacao_arquivo_xml = (int) $retorno_xml_fase_caixa[0]->f_popular_notificacao_fase_xml;

        switch ($id_alienacao_arquivo_xml)
        {
            case 0:
                $retorno = array('status'    => 'alerta',
                                 'recarrega' => 'true',
                                 'msg'       => 'ATENÇÃO: Não existe registros a serem processados.',
                                );
            break;
            case -1:
                $retorno = array('status'    => 'alerta',
                                 'recarrega' => 'true',
                                 'msg'       => 'ATENÇÃO: Não é possível gerar dois arquivos de fase para o mesmo dia.',
                                );
            break;
            default:
                //so para certificar
                if ($id_alienacao_arquivo_xml>0)
                {
                    $dom = new DOMDocument("1.0", "UTF-8");
                    $dom->preserveWhiteSpace = false;
                    $dom->formatOutput = true;

                    $root = $dom->createElement("root");

                    $nova_fase_arquivo_xml  = new fase_arquivo_xml();
                    //a clausula (limit) sera retirada depois que rodar o primeiro lote
                    $linhas_arquivo_xml     = $nova_fase_arquivo_xml->where('id_alienacao_arquivo_xml', $id_alienacao_arquivo_xml)->limit(50000)->get();

                    foreach ($linhas_arquivo_xml as $dadosxml)
                    {
                        $fase           = $dom->createElement("fase");
                        $idsialf        = $dom->createElement("idsialf", $dadosxml->id_legado);
                        $protocolo      = $dom->createElement("protocolo",  $dadosxml->nu_protocolo);
                        $dtprotocolo    = $dom->createElement("dtprotocolo", Carbon::parse($dadosxml->dt_protocolo)->format('Ymd'));
                        $contrato       = $dom->createElement("contrato",  $dadosxml->nu_contrato);
                        $contratodv     = $dom->createElement("contratodv",  $dadosxml->nu_dv_contrato);
                        $cpf            = $dom->createElement("cpf",  $dadosxml->nu_cpf_cnpj);
                        $codfase        = $dom->createElement("codfase",  $dadosxml->nu_tipo_fase_xml_cef);
                        $dtfase         = $dom->createElement("dtfase",  Carbon::parse($dadosxml->dt_etapa_fase)->format('Ymd'));

                        $fase->appendChild($idsialf);
                        $fase->appendChild($protocolo);
                        $fase->appendChild($dtprotocolo);
                        $fase->appendChild($contrato);
                        $fase->appendChild($contratodv);
                        $fase->appendChild($cpf);
                        $fase->appendChild($codfase);
                        $fase->appendChild($dtfase);

                        $root->appendChild($fase);
                    }

                    $dom->appendChild($root);
                    $dom->save(storage_path() . '/app' . $no_diretorio . '/' . $no_arquivo);
                }

                $retorno = array('status'    => 'sucesso',
                    'recarrega' => 'true',
                    'msg'       => 'Arquivo gerado com sucesso: ',
                );
            break;
        }

        return response()->json($retorno);

    }

    public function download_arquivo_fase(Request $request, alienacao_arquivo_xml $alienacao_arquivo_xml)
    {
        if ($request->id_alienacao_arquivo_xml>0)
        {
            $alienacao_arquivo_xml = $alienacao_arquivo_xml->find($request->id_alienacao_arquivo_xml);

            if ($alienacao_arquivo_xml)
            {
                $no_arquivo = ($request->assinado==1) ? str_replace(".xml", ".assinado.xml", $alienacao_arquivo_xml->no_arquivo) : $alienacao_arquivo_xml->no_arquivo;
                $arquivo    = Storage::get($alienacao_arquivo_xml->no_diretorio_arquivo.'/'.$no_arquivo);
                return response($arquivo, 200)->header('Content-Disposition', 'attachment; filename="'.$no_arquivo.'"');
            }
        }
    }

}
