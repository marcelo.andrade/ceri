<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cielo\API30\Merchant;
use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Ecommerce\CieloEcommerce;

use Carbon\Carbon;
use App\compra_credito;
use App\credito_entrada_saida;

class ValidaTransacaoController extends Controller
{
    const ID_SITUACAO_CREDITO_EM_APROVACAO = 2;
    const ID_SITUACAO_CREDITO_EM_ANALISE = 3;
    const ID_SITUACAO_CREDITO_CANCELADO = 4;
    const ID_SITUACAO_CREDITO_EM_CANCELAMENTO = 5;
    const ID_SITUACAO_CREDITO_APROVADO = 6;
    const ID_TIPO_CREDITO = 1;

    /**
     * ValidaTrasacaoController constructor.
     */
    public function __construct()
    {
        /**
         * define o embiente que a aplicações está rodando.
         * local (desenvolvimento/homologação) ou produtction (produção)
         */
        if (env('APP_ENV') == 'production') {
            $this->environment = Environment::production();
            $this->merchant_id = "5311caf5-1e1a-4beb-a806-08f3cf5bf80d";
            $this->merchant_key = "JZbeKpqRxGflmPIQvaDQkl7QUMHeFISFPTBvVM8z";
        } else {
            $this->environment = Environment::sandbox();
            $this->merchant_id = "79e3351d-0af5-4a69-b29d-761e6941d6b2";
            $this->merchant_key = "NULFUZHWWKZWQVWECOGKAYMXWLZKHQMKYICTZFVS";
        }
    }

    /**
     * @param compra_credito $compra_credito
     * @throws \Cielo\API30\Ecommerce\Request\CieloRequestException
     *
     * Método responsável por consultar e atualizar os status das compras que estão com id_situacao_credito
     * igual a 2 (em aprovação) para a situação de acordo com o status retornado pela CIELO.
     * Funcionalidade acionada via crontab configurado no servidor da aplicação.
     */
    public function atualiza_retorno_debito(compra_credito $compra_credito)
    {
        $compras_credito = $compra_credito->where('id_situacao_credito', $this::ID_SITUACAO_CREDITO_EM_APROVACAO)
            ->whereNotNull('id_pagamento_cielo')
            ->get();

        // Configure seu merchant
        $merchant = new Merchant($this->merchant_id, $this->merchant_key);

        if (count($compras_credito) > 0) {
            foreach ($compras_credito as $compra_credito) {

                // Realiza a consulta da transação
                $sale = (new CieloEcommerce($merchant, $this->environment))->getSale($compra_credito->id_pagamento_cielo);

                /**
                 * Adiciona a situação da compra de crédito a partir do código de rertono da CIELO,
                 * de acordo com os código abaixo:
                 * 1 ou 2 - autorizado;
                 * 3, 10, 11 ou 13 - não autorizado;
                 * 0 ou 12 (* OBS: 20 (somente para cartões de crédito)) - aguardando autorização da instituição financeira;
                 */
                switch ($sale->getPayment()->getStatus()) {
                    case 1:
                    case 2:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_APROVADO;
                        $no_situacao_credito = 'Aprovado';
                        break;
                    case 3:
                    case 10:
                    case 11:
                    case 13:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_CANCELADO;
                        $no_situacao_credito = 'Cancelado';
                        break;
                    case 0:
                    case 12:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_ANALISE;
                        $no_situacao_credito = 'Em análise';
                        break;
                    default:
                        $id_situacao_credito = $this::ID_SITUACAO_CREDITO_EM_CANCELAMENTO;
                        $no_situacao_credito = 'Em cancelamento';
                        break;
                }

                /**
                 * Adiciona informações da compra inserindo os dados de retorno da CIELO na tabela compra_credito.
                 * Quando o codigo de retorno for 4 ou 6, o credito é adicionado inserindo as informações
                 * na tabela credito_entrada_saida
                 */
                if (in_array($sale->getPayment()->getStatus(), [1,2])) {
                    $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                    $compra_credito->id_situacao_credito = $id_situacao_credito;
                    $compra_credito->id_usuario_lib = $compra_credito->id_usuario;
                    $compra_credito->dt_liberacao = Carbon::now();
                    if ($compra_credito->save()) {
                        $credito_entrada_saida = new credito_entrada_saida();
                        $credito_entrada_saida->id_compra_credito = $compra_credito->id_compra_credito;
                        $credito_entrada_saida->id_tipo_credito = $this::ID_TIPO_CREDITO;
                        $credito_entrada_saida->tp_movimento = 'E';
                        $credito_entrada_saida->va_movimento = $sale->getPayment()->getAmount() / 100;
                        $credito_entrada_saida->id_usuario = $compra_credito->id_usuario;
                        $credito_entrada_saida->dt_cadastro = Carbon::now();
                        $credito_entrada_saida->dt_movimento = Carbon::now();
                        $credito_entrada_saida->save();
                    }
                } else {
                    $compra_credito->protocolo_compra = $sale->getPayment()->getTid();
                    $compra_credito->id_situacao_credito = $id_situacao_credito;
                    $compra_credito->save();
                }

                echo $sale->getPayment()->getTid() . " - " . $sale->getPayment()->getPaymentId() . " - " .  $sale->getPayment()->getStatus() . " (" . $no_situacao_credito . ") - " . Carbon::now()->format('d/m/Y H:i') . "\r\n";
            }
        }
    }
}
