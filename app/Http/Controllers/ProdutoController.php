<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\pedido_resultado_matricula;
use App\serventia;
use Illuminate\Http\Request;

use App\produto;
use App\produto_item;

class ProdutoController extends Controller {
	
	public function __construct() {}

	public function listar_itens(Request $request, produto $produto) {
		$produto_itens = $produto->find($request->id_produto)->produto_itens;
		return response()->json($produto_itens);
	}

	public function calcular(Request $request, produto_item $produto_item, serventia $serventia) {
		if ($request->id_produto_item>0 && count($request->ids_serventias)>0)
		{
            $arrayServentia = $request->ids_serventias;
            foreach ($arrayServentia as $valor_serventia )
            {
                $serventia_sel    = $serventia->find($valor_serventia);
                $arrayIdPessoa[]  = $serventia_sel->id_pessoa;
                $ids              = implode(";", $arrayIdPessoa);
            }
            $produto_item = $produto_item->find($request->id_produto_item);
            $valores = $produto_item->lista_preco($ids);

            if($produto_item->produto->id_grupo_produto == 2 &&  $this->desconto($request->certidao_token, $request->id_pedido_resultado_matricula)>0){
                $total = \array_pop($valores);
                array_push($valores, ['descricao'=>'Desconto de Pesquisa', 'va_preco'=> $this->desconto($request->certidao_token, $request->id_pedido_resultado_matricula)]);
                array_push($valores, $total);
            }
            return response()->json($valores);
		}else{
            return response()->json('');
        }
	}
	private function desconto($certidao_token, $id_pedido_resultado_matricula) {
        $va_desconto = 0;
	    if(session()->has('certidao_'.$certidao_token)){
            return $va_desconto;
        } elseif($id_pedido_resultado_matricula>0) {
            $pedido_resultado_matricula = new pedido_resultado_matricula();
            $pedido_resultado_matricula = $pedido_resultado_matricula->find($id_pedido_resultado_matricula);
            $va_desconto = $pedido_resultado_matricula->desconto()==0?$pedido_resultado_matricula->pedido_resultado->pedido->va_pedido:0;
        }
        return $va_desconto;
    }
}
