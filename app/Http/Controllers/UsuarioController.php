<?php
namespace App\Http\Controllers;

use App\alienacao_preco_produto_item_variavel;
use App\alienacao_preco_produto_item_variavel_serventia;
use App\banco;
use App\banco_pessoa;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\CeriFuncoes;

use App\serventia_cliente;
use App\usuario_unidade_gestora;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

use Validator;
use Auth;
use DB;
use Hash;
use Carbon\Carbon;
use File;
use Mail;
use Storage;
use XMLReader;
use DOMDocument;
use Session;

use App\serventia;
use App\tipo_usuario;
use App\usuario_modulo;
use App\usuario_vara;
use App\pessoa;
use App\usuario;
use App\usuario_senha;
use App\telefone;
use App\endereco;
use App\estado;
use App\nacionalidade;
use App\estado_civil;
use App\serventia_certificado;
use App\usuario_serventia;
use App\vara;
use App\cargo;
use App\comarca;
use App\unidade_gestora;
use App\unidade_judiciaria;
use App\unidade_judiciaria_divisao;
use App\usuario_unidade_judiciaria_divisao;
use App\cidade;
use App\usuario_pessoa;
use App\pessoa_telefone;
use App\pessoa_endereco;
use App\pessoa_modulo;
use App\tabela_preco_serventia;
use App\tabela_preco;

class UsuarioController extends Controller {

	const TP_USUARIO_SERVENTIA = 2;
	const TP_USUARIO_JUDICIARIO = 4;
	const TP_USUARIO_CORREGEDORIA = 7;
	const TP_USUARIO_ANOREG = 9;
	const ID_UNIDADE_JUDICIARIA_DIVISAO = 43;

	public function __construct(usuario $usuario) {
		$this->usuario=$usuario;

		if (Auth::check()) {
			if (count(Auth::User()->usuario_pessoa)>0) {
				$pessoa_ativa = Session::get('pessoa_ativa');
                $this->usuario = Auth::User();

                if(Session::has('troca_pessoa')) {
                    $this->usuario = $pessoa_ativa->pessoa->getUsuarioRelacionado()->usuario;
                }

				$this->id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
				$this->id_pessoa = $pessoa_ativa->id_pessoa;
			} else {
				$this->id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
				$this->id_pessoa = Auth::User()->id_pessoa;
			}
		}
	}
	
	public function index(Request $request, pessoa $pessoa, usuario $usuario) {

        $usuarios_pendentes = $usuario->select('usuario.*','pessoa.id_tipo_pessoa','p2.id_tipo_pessoa')
                                        ->join('pessoa','pessoa.id_pessoa','=','usuario.id_pessoa')
                                        ->join('usuario_pessoa','usuario_pessoa.id_usuario','=','usuario.id_usuario')
                                        ->join('pessoa_modulo','pessoa_modulo.id_pessoa','=','usuario_pessoa.id_pessoa')
                                        ->join('pessoa as p2','p2.id_pessoa','=','pessoa_modulo.id_pessoa')
                                        ->where('usuario.in_registro_ativo','S')
                                        ->where(function($query) {
                                            $query->where('usuario.in_confirmado','N')
                                                ->orWhere('usuario.in_aprovado','N');
                                        });

        $usuarios_inativos = $usuario->select('usuario.*','pessoa.id_tipo_pessoa','p2.id_tipo_pessoa')
                                    ->join('pessoa','pessoa.id_pessoa','=','usuario.id_pessoa')
                                    ->join('usuario_pessoa','usuario_pessoa.id_usuario','=','usuario.id_usuario')
                                    ->join('pessoa_modulo','pessoa_modulo.id_pessoa','=','usuario_pessoa.id_pessoa')
                                    ->join('pessoa as p2','p2.id_pessoa','=','pessoa_modulo.id_pessoa')
                                    ->where('usuario.in_registro_ativo','N');

		$todos_usuarios = $usuario->select('pessoa.id_tipo_pessoa','p2.id_tipo_pessoa','usuario.*')
                                    ->join('pessoa','pessoa.id_pessoa','=','usuario.id_pessoa')
                                    ->join('usuario_pessoa','usuario_pessoa.id_usuario','=','usuario.id_usuario')
                                    ->join('pessoa_modulo','pessoa_modulo.id_pessoa','=','usuario_pessoa.id_pessoa')
                                    ->join('pessoa as p2','p2.id_pessoa','=','pessoa_modulo.id_pessoa')
                                    ->where('usuario.in_registro_ativo','S')
                                    ->whereNotIn('usuario.id_pessoa', function($query){
                                        $query->select('id_pessoa')
                                              ->from('pessoa_modulo')
                                              ->whereIn('id_modulo', array(0,1));
                                  })
								  ->where('usuario.in_confirmado','S')
								  ->where('usuario.in_aprovado','S');

		switch ($this->id_tipo_pessoa) {
			case 1:
				$usuarios_pendentes = $usuarios_pendentes->whereIn('pessoa.id_tipo_pessoa',array(2,4,7,11));
				$usuarios_inativos = $usuarios_inativos->whereIn('pessoa.id_tipo_pessoa',array(2,4,7,11));
				$todos_usuarios = $todos_usuarios->whereIn('pessoa.id_tipo_pessoa',array(2,4,7,11));
				break;
			case 2: case 4: case 10: case 11: case 6: case 8:
				$usuarios_pendentes = $usuarios_pendentes->where('usuario_pessoa.id_pessoa','=',$this->id_pessoa);
				$usuarios_inativos = $usuarios_inativos->where('usuario_pessoa.id_pessoa','=',$this->id_pessoa);
				$todos_usuarios = $todos_usuarios->where('usuario_pessoa.id_pessoa','=',$this->id_pessoa);
				break;
			case 7:
				$usuarios_pendentes = $usuarios_pendentes->whereIn('p2.id_tipo_pessoa',array(4,7,11));
				$usuarios_inativos = $usuarios_inativos->whereIn('p2.id_tipo_pessoa',array(4,7,11));
				$todos_usuarios = $todos_usuarios->whereIn('p2.id_tipo_pessoa',array(4,7,11));
				break;
			case 9:
			    $id_tipo_pessoa_list = $request->id_tipo_pessoa_list?:3;
                $usuarios_pendentes = $usuarios_pendentes->where('p2.id_tipo_pessoa','=',$id_tipo_pessoa_list);
                $usuarios_inativos = $usuarios_inativos->where('p2.id_tipo_pessoa','=',$id_tipo_pessoa_list);
                $todos_usuarios = $todos_usuarios->where('p2.id_tipo_pessoa','=',$id_tipo_pessoa_list);
				break;
		}

        if ($request->isMethod('POST')) {
            if ($request->nome !='') {
                $usuarios_pendentes = $usuarios_pendentes->where('usuario.no_usuario', 'ilike', '%' . $request->nome . '%');
                $usuarios_inativos = $usuarios_inativos->where('usuario.no_usuario', 'ilike', '%' . $request->nome . '%');
                $todos_usuarios = $todos_usuarios->where('usuario.no_usuario', 'ilike', '%' . $request->nome . '%');
            }
            if ($request->cpf_cnpj !='') {
                $usuarios_pendentes = $usuarios_pendentes->where('pessoa.nu_cpf_cnpj', '=', limpar_mascara($request->cpf_cnpj));
                $usuarios_inativos = $usuarios_inativos->where('pessoa.nu_cpf_cnpj', '=', limpar_mascara($request->cpf_cnpj));
                $todos_usuarios = $todos_usuarios->where('pessoa.nu_cpf_cnpj', '=', limpar_mascara($request->cpf_cnpj));
            }
            if ($request->email !='') {
                $usuarios_pendentes = $usuarios_pendentes->where('usuario.email_usuario', '=', $request->email);
                $usuarios_inativos = $usuarios_inativos->where('usuario.email_usuario', '=', $request->email);
                $todos_usuarios = $todos_usuarios->where('usuario.email_usuario', '=', $request->email);
            }
        }

        $usuarios_pendentes = $usuarios_pendentes->orderBy('usuario.dt_cadastro','desc')->paginate(10, ['*'], 'usuarios-pendentes-pag');
        $usuarios_inativos = $usuarios_inativos->orderBy('usuario.dt_cadastro','desc')->paginate(10, ['*'], 'usuarios-inativos-pag');
        $todos_usuarios = $todos_usuarios->orderBy('usuario.dt_cadastro','desc')->paginate(10, ['*'], 'usuarios-ativos-pag');

        $usuarios_pendentes->appends(Request::capture()->except('_token'))->render();
        $usuarios_inativos->appends(Request::capture()->except('_token'))->render();
        $todos_usuarios->appends(Request::capture()->except('_token'))->render();

		$class = $this;

		return view('usuarios.usuarios',compact('class','todos_usuarios','usuarios_pendentes','usuarios_inativos','request'));
		
	}

	public function inserir_usuario(Request $request) {
		$novo_usuario = $this->inserir($request, 1, 3);

		if ($novo_usuario) {
			Mail::send('email.confirmar-cadastro', ['pessoa' => $novo_usuario->pessoa, 'confirmar_token' => $novo_usuario->confirmar_token], function ($mail) use ($request) {
				$mail->to($request->email_usuario, $request->no_pessoa)
					 ->subject('CERI - Confirmação de cadastro');
			});
			return Redirect::back()->with('status', 'SUCESSO');
		} else {
			return Redirect::back()->with('status', 'ERRO');
		}
	}
	public function inserir_cartorio(Request $request) {
		$novo_usuario = $this->inserir($request, 1, 2);

		if ($novo_usuario) {
			Mail::send('email.sucesso-cadastro', ['pessoa' => $novo_usuario->pessoa], function ($mail) use ($request) {
				$mail->to($request->email_usuario, $request->no_pessoa)
					 ->subject('CERI - Cadastro realizado com sucesso');
			});
			return Redirect::back()->with('status', 'SUCESSO');
		} else {
			return Redirect::back()->with('status', 'ERRO');
		}
	}
	public function inserir_judiciario(Request $request) {
		$novo_usuario = $this->inserir($request, 1, 4);

		if ($novo_usuario) {
			Mail::send('email.sucesso-cadastro', ['pessoa' => $novo_usuario->pessoa], function ($mail) use ($request) {
				$mail->to($request->email_usuario, $request->no_pessoa)
					 ->subject('CERI - Cadastro realizado com sucesso');
			});
			return Redirect::back()->with('status', 'SUCESSO');
		} else {
			return Redirect::back()->with('status', 'ERRO');
		}
	}
	public function inserir($request, $id_usuario_cad, $id_tipo_pessoa) {
		$erro = 0;

		DB::beginTransaction();
		
		$nova_pessoa = new pessoa();
		$nova_pessoa->no_pessoa = $request->no_pessoa;
		$nova_pessoa->tp_pessoa = $request->tp_pessoa;
		$nova_pessoa->nu_cpf_cnpj = preg_replace( '#[^0-9]#', '', $request->nu_cpf_cnpj);
		$nova_pessoa->no_email_pessoa = strtolower(trim($request->email_usuario));
		
		if ($request->tp_pessoa=='F') {
			$nova_pessoa->dt_nascimento = Carbon::createFromFormat('d/m/Y',$request->dt_nascimento);
			$nova_pessoa->tp_sexo = $request->tp_sexo;
		} elseif ($request->tp_pessoa=='J') {
			$nova_pessoa->nu_inscricao_municipal = $request->nu_inscricao_municipal;
			$nova_pessoa->no_fantasia = $request->no_fantasia;
			$nova_pessoa->tp_sexo = 'N';
		}

		$nova_pessoa->id_tipo_pessoa = $id_tipo_pessoa;

		if ($nova_pessoa->save()) {
			$confirmar_token = str_random(30);
			
			$novo_usuario = new usuario();
			$novo_usuario->id_pessoa = $nova_pessoa->id_pessoa;
			$novo_usuario->codigo_usuario = '';
			$novo_usuario->no_usuario = $request->no_pessoa;
			$novo_usuario->email_usuario = strtolower(trim($request->email_usuario));
			$novo_usuario->login = '';
			$novo_usuario->dt_usuario_ativo = Carbon::now();
			$novo_usuario->in_registro_ativo = 'S';
			$novo_usuario->id_unidade_gestora = 1;
			$novo_usuario->in_aprovado = 'S';
			$novo_usuario->in_usuario_master = 'S';
            $novo_usuario->id_tipo_usuario = $id_tipo_pessoa;
			$novo_usuario->id_usuario_cad = $id_usuario_cad;

			switch($id_tipo_pessoa) {
				case 2:
					$novo_usuario->in_confirmado = 'S';
					$novo_usuario->dt_usuario_confirmado = Carbon::now();
					break;
				/*
				case 4:
					$novo_usuario->id_cargo = $request->id_cargo;
					break;
				*/
				case 3:
					$novo_usuario->in_confirmado = 'N';
					$novo_usuario->confirmar_token = $confirmar_token;
					break;
			}
			
			if (!$novo_usuario->save()) {
				$erro = 1;
			}

			$nova_senha = new usuario_senha();
			$nova_senha->id_usuario = $novo_usuario->id_usuario;
			$nova_senha->senha = Hash::make($request->senha_usuario);
			if (!$nova_senha->save()) {
				$erro = 1;
			}

			$novo_endereco = new endereco();
			$novo_endereco->id_cidade = $request->id_cidade;
			$novo_endereco->no_endereco = $request->no_endereco;
			$novo_endereco->nu_endereco = $request->nu_endereco;
			$novo_endereco->no_bairro = $request->no_bairro;
			$novo_endereco->nu_cep = preg_replace('#[^0-9]#', '', $request->nu_cep);
			if (!empty($request->no_complemento)) {
				$novo_endereco->no_complemento = $request->no_complemento;
			}
			if ($novo_endereco->save()) {
				$nova_pessoa->enderecos()->attach($novo_endereco);
			} else {
				$erro = 1;
			}

			$novo_telefone = new telefone();
			$novo_telefone->id_tipo_telefone = $request->id_tipo_telefone;
			$novo_telefone->id_classificacao_telefone = 1;
			$novo_telefone->nu_ddd = $request->nu_ddd;
			$novo_telefone->nu_telefone = $request->nu_telefone;
			if ($novo_telefone->save()) {
				$nova_pessoa->telefones()->attach($novo_telefone);
			} else {
				$erro = 1;
			}

			switch ($id_tipo_pessoa) {
				/*
				case 4:
					$nova_entidade = new pessoa();
					$nova_entidade->no_pessoa = $request->no_vara;
					$nova_entidade->tp_pessoa = 'J';
					$nova_entidade->nu_cpf_cnpj = preg_replace( '#[^0-9]#', '', $request->nu_cpf_cnpj_vara);
					$nova_entidade->no_email_pessoa = $request->email_vara;
					$nova_entidade->tp_sexo = 'N';
					$nova_entidade->id_tipo_pessoa = $id_tipo_pessoa;
					if (!$nova_entidade->save()) {
						$erro = 1;
					}
					$nova_entidade_modulo = new pessoa_modulo();
					$nova_entidade_modulo->id_modulo = 4;
					$nova_entidade_modulo->id_pessoa = $nova_entidade->id_pessoa;
					$nova_entidade_modulo->id_usuario_cad = $id_usuario_cad;
					$nova_entidade_modulo->in_registro_ativo = 'S';
					$nova_entidade_modulo->dt_cadastro = Carbon::now();
					if (!$nova_entidade_modulo->save()) {
						$erro = 1;
					}
					$novo_usuario_entidade = new usuario_pessoa();
					$novo_usuario_entidade->id_usuario = $novo_usuario->id_usuario;
					$novo_usuario_entidade->id_pessoa = $nova_entidade->id_pessoa;
					$novo_usuario_entidade->id_usuario_cad = $id_usuario_cad;
					$novo_usuario_entidade->dt_cadastro = Carbon::now();
					if (!$novo_usuario_entidade->save()) {
						$erro = 1;
					}

					$nova_vara = new vara();
					$nova_vara->id_vara_tipo = $request->id_vara_tipo;
					$nova_vara->id_comarca = $request->id_comarca;
					$nova_vara->no_vara = $request->no_vara;
					$nova_vara->abv_vara = $request->no_vara;
					$nova_vara->dt_ini_vigencia = Carbon::now();
					$nova_vara->id_usuario_cad = $id_usuario_cad;
					$nova_vara->id_pessoa = $nova_entidade->id_pessoa;
					if (!$nova_vara->save()) {
						$erro = 1;
					}
					break;
				*/
				case 2:
					$nova_entidade = new pessoa();
					$nova_entidade->no_pessoa = $request->no_serventia;
					$nova_entidade->tp_pessoa = 'J';
					$nova_entidade->nu_cpf_cnpj = preg_replace( '#[^0-9]#', '', $request->nu_cpf_cnpj_serventia);
					$nova_entidade->no_email_pessoa = strtolower(trim($request->email_serventia));
					$nova_entidade->tp_sexo = 'N';
					$nova_entidade->id_tipo_pessoa = $id_tipo_pessoa;
					if (!$nova_entidade->save()) {
						$erro = 1;
					}
					$nova_entidade_modulo = new pessoa_modulo();
					$nova_entidade_modulo->id_modulo = 2;
					$nova_entidade_modulo->id_pessoa = $nova_entidade->id_pessoa;
					$nova_entidade_modulo->id_usuario_cad = $id_usuario_cad;
					$nova_entidade_modulo->in_registro_ativo = 'S';
					$nova_entidade_modulo->dt_cadastro = Carbon::now();
					if (!$nova_entidade_modulo->save()) {
						$erro = 1;
					}
					$novo_usuario_entidade = new usuario_pessoa();
					$novo_usuario_entidade->id_usuario = $novo_usuario->id_usuario;
					$novo_usuario_entidade->id_pessoa = $nova_entidade->id_pessoa;
					$novo_usuario_entidade->id_usuario_cad = $id_usuario_cad;
					$novo_usuario_entidade->dt_cadastro = Carbon::now();
					if (!$novo_usuario_entidade->save()) {
						$erro = 1;
					}

					$nova_serventia = new serventia();
					$nova_serventia->id_tipo_serventia = $request->id_tipo_serventia;
					$nova_serventia->id_pessoa = $nova_entidade->id_pessoa;
					$nova_serventia->id_unidade_gestora = 1;
					$nova_serventia->no_serventia = $request->no_serventia;
					$nova_serventia->abv_serventia = $request->no_serventia;
					$nova_serventia->in_registro_ativo = 'N';
					$nova_serventia->id_usuario_cad = $id_usuario_cad;
					$nova_serventia->hora_inicio_expediente = $request->hora_inicio_expediente;
					$nova_serventia->hora_inicio_almoco = $request->hora_inicio_almoco;
					$nova_serventia->hora_termino_almoco = $request->hora_termino_almoco;
					$nova_serventia->hora_termino_expediente = $request->hora_termino_expediente;
					$nova_serventia->no_oficial = $nova_pessoa->no_pessoa;
					if (!$nova_serventia->save()) {
						$erro = 1;
					}
					break;
				default:
                    $novo_pessoa_modulo = new pessoa_modulo();
					$novo_pessoa_modulo->id_modulo = 3;
					$novo_pessoa_modulo->id_pessoa = $nova_pessoa->id_pessoa;
					$novo_pessoa_modulo->id_usuario_cad = $id_usuario_cad;
					$novo_pessoa_modulo->in_registro_ativo = 'S';
					$novo_pessoa_modulo->dt_cadastro = Carbon::now();
					if (!$novo_pessoa_modulo->save()) {
						$erro = 1;
					}

					$novo_usuario_pessoa = new usuario_pessoa();
					$novo_usuario_pessoa->id_usuario = $novo_usuario->id_usuario;
					$novo_usuario_pessoa->id_pessoa = $nova_pessoa->id_pessoa;
					$novo_usuario_pessoa->id_usuario_cad = $id_usuario_cad;
					$novo_usuario_pessoa->dt_cadastro = Carbon::now();
					if (!$novo_usuario_pessoa->save()) {
						$erro = 1;
					}
					break;
			}
		
		}
		
		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return false;
		} else {
			DB::commit();
			return $novo_usuario;
		}
	}

	public function novo_usuario_interno(Request $request, estado $estado, cargo $cargo, comarca $comarca, unidade_gestora $unidade_gestora, unidade_judiciaria $unidade_judiciaria, tipo_usuario $tipo_usuario) {
		$estados = $estado->orderBy('no_estado')->get();
		$cidades = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
		$cargos = $cargo->where('in_registro_ativo','S')
						->orderBy('nu_ordem','desc')->get();
		$comarcas = $comarca->orderBy('nu_ordem','asc')->get();
		$unidades_gestoras = $unidade_gestora->where('in_registro_ativo','S')->orderBy('no_unidade_gestora')->get();
		$unidades_judiciarias = $unidade_judiciaria->where('in_registro_ativo','S')->orderBy('no_unidade_judiciaria')->get();

		$tipos_usuarios = NULL;
		switch ($this->id_tipo_pessoa) {
		    case 1:
		    	$tipos_usuarios = $tipo_usuario->where('in_registro_ativo','S')
		    								   ->whereIn('id_tipo_usuario',array(2,4,7,11))
		    								   ->get();
		        break;
		    case 7:
		        $tipos_usuarios = $tipo_usuario->where('in_registro_ativo','S')
		    								   ->whereIn('id_tipo_usuario',array(4,7,11))
		    								   ->get();
		        break;
		}

		$class = $this;

		return view('usuarios.usuarios-novo', compact('class','estados','cidades','cargos','comarcas','unidades_gestoras','unidades_judiciarias','tipos_usuarios'));
	}
	public function inserir_usuario_interno(Request $request){
		$erro = 0;

		DB::beginTransaction();

		$nova_pessoa = new pessoa();
		$nova_pessoa->id_tipo_pessoa = $this->id_tipo_pessoa;
		$nova_pessoa->no_pessoa = $request->no_pessoa;
		$nova_pessoa->tp_pessoa = $request->tp_pessoa;
		$nova_pessoa->nu_cpf_cnpj = preg_replace( '#[^0-9]#', '', $request->nu_cpf_cnpj);
		$nova_pessoa->no_email_pessoa = strtolower(trim($request->email_usuario));
		
		if ($request->tp_pessoa=='F') {
			$nova_pessoa->dt_nascimento = Carbon::createFromFormat('d/m/Y',$request->dt_nascimento);
			$nova_pessoa->tp_sexo = $request->tp_sexo;
		} elseif ($request->tp_pessoa=='J') {
			$nova_pessoa->nu_inscricao_municipal = $request->nu_inscricao_municipal;
			$nova_pessoa->no_fantasia = $request->no_fantasia;
			$nova_pessoa->tp_sexo = 'N';
		}

        if ($nova_pessoa->save()) {
            $pessoa_modulo = new pessoa_modulo();
            $pessoa_modulo->id_pessoa = $nova_pessoa->id_pessoa;
            $pessoa_modulo->id_modulo = $this->id_tipo_pessoa;
            $pessoa_modulo->in_registro_ativo = 'S';
            $pessoa_modulo->id_usuario_cad = Auth::User()->id_usuario;
            if (!$pessoa_modulo->save()){
                $erro = 'USU010';
            }

            $novo_usuario = new usuario();
            $novo_usuario->id_pessoa = $nova_pessoa->id_pessoa;
            $novo_usuario->id_tipo_usuario = $this->id_tipo_pessoa;
            $novo_usuario->codigo_usuario = '';
			$novo_usuario->no_usuario = $request->no_pessoa;
			$novo_usuario->email_usuario = strtolower(trim($request->email_usuario));
			$novo_usuario->login = '';
			$novo_usuario->dt_usuario_ativo = Carbon::now();
			$novo_usuario->in_registro_ativo = 'S';
			$novo_usuario->id_unidade_gestora = 1;
			$novo_usuario->in_usuario_master = ($request->in_usuario_master=='S'?'S':'N');
			$novo_usuario->id_usuario_cad = Auth::User()->id_usuario;
			$novo_usuario->in_alterar_senha = 'S';
			$novo_usuario->id_cargo = ($request->id_cargo>0?$request->id_cargo:NULL);

			if ($request->in_confirmado=='S') {
				$novo_usuario->in_confirmado = 'S';
                $novo_usuario->dt_usuario_confirmado = Carbon::now();
            } else {
                $novo_usuario->in_confirmado = 'N';
                $confirmar_token = str_random(30);
                $novo_usuario->confirmar_token = $confirmar_token;
            }
            if ($request->in_aprovado=='S') {
                $novo_usuario->in_aprovado = 'S';
                $novo_usuario->dt_usuario_aprovado = Carbon::now();
            } else {
                $novo_usuario->in_aprovado = 'N';
            }

            if ($novo_usuario->save()) {
                $usuario_modulo = new usuario_modulo();
                $usuario_modulo->id_modulo = $this->id_tipo_pessoa;
                $usuario_modulo->id_usuario = $novo_usuario->id_usuario;
                $usuario_modulo->id_usuario_cad = Auth::User()->id_usuario;
                if(!$usuario_modulo->save()){
                    $erro = 'USU011';
                }

                if ($novo_usuario->in_aprovado=='S') {
                    $senha_gerada = str_random(10);

                    $nova_senha = new usuario_senha();
                    $nova_senha->id_usuario = $novo_usuario->id_usuario;
                    $nova_senha->senha = Hash::make($senha_gerada);
                    if (!$nova_senha->save()) {
                        $erro = 'USU003';
                    }
                }

                if ($request->in_digitar_endereco=='S') {
                    $novo_endereco = new endereco();
                    $novo_endereco->id_cidade = $request->id_cidade;
                    $novo_endereco->no_endereco = $request->no_endereco;
                    $novo_endereco->nu_endereco = $request->nu_endereco;
                    $novo_endereco->no_bairro = $request->no_bairro;
                    $novo_endereco->nu_cep = preg_replace('#[^0-9]#', '', $request->nu_cep);
                    $novo_endereco->no_complemento = $request->no_complemento;

                    if ($novo_endereco->save()) {
                        $nova_pessoa->enderecos()->attach($novo_endereco);
                    } else {
                        $erro = 'USU004';
                    }
                }

                if ($request->in_digitar_telefone=='S') {
                    $novo_telefone = new telefone();
                    $novo_telefone->id_tipo_telefone = $request->id_tipo_telefone;
                    $novo_telefone->id_classificacao_telefone = 1;
                    $novo_telefone->nu_ddd = $request->nu_ddd;
                    $novo_telefone->nu_telefone = $request->nu_telefone;

                    if ($novo_telefone->save()) {
                        $nova_pessoa->telefones()->attach($novo_telefone);
                    } else {
                        $erro = 'USU005';
                    }
                }

				if ($request->in_vincular=='S') {
					$novo_usuario_pessoa = new usuario_pessoa();
					$novo_usuario_pessoa->id_usuario = $novo_usuario->id_usuario;
					$novo_usuario_pessoa->id_pessoa = $this->id_pessoa;
					$novo_usuario_pessoa->id_usuario_cad = Auth::User()->id_usuario;
					
					if (!$novo_usuario_pessoa->save()) {
						$erro = 'USU006';
					}
				} else {
					if (count($request->id_vinculo)>0) {
						$erro_loop_rel = false;
						$i=0;
						foreach ($request->id_vinculo as $id_vinculo) {
							switch ($request->vinculo_id_tipo_usuario[$i]) {
								case 4:
									$vara_selecionada = new vara();
									$vara_selecionada = $vara_selecionada->find($id_vinculo);
									$id_pessoa = $vara_selecionada->id_pessoa;
									break;
								case 2: case 5: case 10:
									$serventia_selecionada = new serventia();
									$serventia_selecionada = $serventia_selecionada->find($id_vinculo);
									$id_pessoa = $serventia_selecionada->id_pessoa;
									break;
								case 7:
									$unidade_gestora_selecionada = new unidade_gestora();
									$unidade_gestora_selecionada = $unidade_gestora_selecionada->find($id_vinculo);
									$id_pessoa = $unidade_gestora_selecionada->id_pessoa;
									break;
								case 11:
									$unidade_judiciaria_divisao_selecionada = new unidade_judiciaria_divisao();
									$unidade_judiciaria_divisao_selecionada = $unidade_judiciaria_divisao_selecionada->find($id_vinculo);
									$id_pessoa = $unidade_judiciaria_divisao_selecionada->id_pessoa;
									break;
							}
							$novo_usuario_pessoa = new usuario_pessoa();
							$novo_usuario_pessoa->id_usuario = $novo_usuario->id_usuario;
							$novo_usuario_pessoa->id_pessoa = $id_pessoa;
							$novo_usuario_pessoa->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_usuario_pessoa->in_master_recebe_notificacoes_sistema = $request->in_master_recebe_notificacoes_sistema;
                            $novo_usuario_pessoa->in_usuario_recebe_notificacoes_sistema = $request->in_usuario_recebe_notificacoes_sistema;
                            if (!$novo_usuario_pessoa->save()) {
                                $erro_loop_rel = true;
                            }

							$i++;
						}
						if ($erro_loop_rel) {
							$erro = 'USU009';
						}
					}
				}
			} else {
				$erro = 'USU002';
			}
		} else {
			$erro = 'USU001';
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();

			if($novo_usuario->in_aprovado=='S') {
				Mail::send('email.cadastro-criado', ['pessoa' => $novo_usuario->pessoa, 'senha_gerada' => $senha_gerada], function ($mail) use ($novo_usuario) {
					$mail->to($novo_usuario->email_usuario, $novo_usuario->pessoa->no_pessoa)
						 ->subject('CERI - Cadastro criado com sucesso');
				});
			}
			if($novo_usuario->in_confirmado=='N') {
				Mail::send('email.confirmar-cadastro', ['pessoa' => $novo_usuario->pessoa, 'confirmar_token' => $novo_usuario->confirmar_token], function ($mail) use ($novo_usuario) {
					$mail->to($novo_usuario->email_usuario, $novo_usuario->pessoa->no_pessoa)
						 ->subject('CERI - Confirmação de cadastro');
				});
			}

			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O usuário foi inserido com sucesso.'));
		}
	}
	public function alterar_usuario_interno(Request $request, usuario $usuario, estado $estado, cidade $cidade, cargo $cargo, comarca $comarca, vara $vara, unidade_gestora $unidade_gestora, unidade_judiciaria $unidade_judiciaria, tipo_usuario $tipo_usuario, unidade_judiciaria_divisao $unidade_judiciaria_divisao) {
		if ($request->id_usuario > 0) {
			$usuario = $usuario->find($request->id_usuario);
			$estados = $estado->orderBy('no_estado')->get();

			$cidades = null;
			if (count($usuario->pessoa->enderecos) > 0) {
				$cidades = $cidade->where('id_estado',$usuario->pessoa->enderecos[0]->cidade->id_estado)
								  ->orderBy('cidade.no_cidade')
								  ->get();
			}

			$cidades_serventia = $estado->find(env('ID_ESTADO'))->cidades_serventia()->orderBy('cidade.no_cidade')->get();
			$cargos = $cargo->where('in_registro_ativo','S')
						->orderBy('nu_ordem','desc')->get();
			$comarcas = $comarca->orderBy('nu_ordem','asc')->get();
			$unidades_gestoras = $unidade_gestora->where('in_registro_ativo','S')->orderBy('no_unidade_gestora')->get();
			$unidades_judiciarias = $unidade_judiciaria->where('in_registro_ativo','S')->orderBy('no_unidade_judiciaria')->get();

			$tipos_usuarios = NULL;
			switch ($this->id_tipo_pessoa) {
			    case 1:
			    	$tipos_usuarios = $tipo_usuario->where('in_registro_ativo','S')
			    								   ->whereIn('id_tipo_usuario',array(2,4,7,11))
			    								   ->get();
			        break;
			    case 7:
			        $tipos_usuarios = $tipo_usuario->where('in_registro_ativo','S')
			    								   ->whereIn('id_tipo_usuario',array(4,7,11))

			    								   ->get();
			  		break;
				case 9:
			        $tipos_usuarios = $tipo_usuario->where('in_registro_ativo','S')
			    								   ->whereIn('id_tipo_usuario',array(2))
			    								   ->get();
			        break;
			}

			$class = $this;

			return view('usuarios.usuarios-alterar', compact('usuario','estados','cidades','cidades_serventia','cargos','comarcas','varas','unidades_gestoras','unidades_judiciarias','unidade_judiciaria_divisoes','tipos_usuarios', 'class'));
		}
	}

	public function salvar_usuario_interno(Request $request){
		$erro = 0;

		DB::beginTransaction();

		$alterar_pessoa = new pessoa();
		$alterar_pessoa = $alterar_pessoa->find($request->id_pessoa);
		$alterar_pessoa->no_pessoa = $request->no_pessoa;
		
		if ($request->tp_pessoa=='F') {
			$alterar_pessoa->dt_nascimento = Carbon::createFromFormat('d/m/Y',$request->dt_nascimento);
			$alterar_pessoa->tp_sexo = $request->tp_sexo;

			// Limpando campos do outro tipo
			$alterar_pessoa->nu_inscricao_municipal = NULL;
			$alterar_pessoa->no_fantasia = NULL;
		} elseif ($request->tp_pessoa=='J') {
			$alterar_pessoa->dt_nascimento = NULL;
			$alterar_pessoa->tp_sexo = 'N';
		}

		if ($alterar_pessoa->save()) {
			$alterar_usuario = new usuario();
			$alterar_usuario = $alterar_usuario->find($alterar_pessoa->usuario->id_usuario);
			$alterar_usuario->no_usuario = $request->no_pessoa;
			$alterar_usuario->in_usuario_master = ($request->in_usuario_master=='S'?'S':'N');
			$alterar_usuario->id_usuario_alt = Auth::User()->id_usuario;
			$alterar_usuario->id_cargo = ($request->id_cargo>0?$request->id_cargo:NULL);

			if ($alterar_usuario->save()) {
				$pessoa_enderecos = new pessoa_endereco();
				$pessoa_enderecos = $pessoa_enderecos->where('id_pessoa',$alterar_pessoa->id_pessoa);

				if (!$pessoa_enderecos->delete()) {
					$erro = 'USU018';
				}
				if ($request->in_digitar_endereco=='S') {
					$novo_endereco = new endereco();
					$novo_endereco->id_cidade = $request->id_cidade;
					$novo_endereco->no_endereco = $request->no_endereco;
					$novo_endereco->nu_endereco = $request->nu_endereco;
					$novo_endereco->no_bairro = $request->no_bairro;
					$novo_endereco->nu_cep = preg_replace('#[^0-9]#', '', $request->nu_cep);
					$novo_endereco->no_complemento = $request->no_complemento;

					if ($novo_endereco->save()) {
						$alterar_pessoa->enderecos()->attach($novo_endereco);
					} else {
						$erro = 'USU010';
					}
					
				}

				$pessoa_telefones = new pessoa_telefone();
				$pessoa_telefones = $pessoa_telefones->where('id_pessoa',$alterar_pessoa->id_pessoa);

				if (!$pessoa_telefones->delete()) {
					$erro = 'USU020';
				}
				if ($request->in_digitar_telefone=='S') {
					$novo_telefone = new telefone();
					$novo_telefone->id_tipo_telefone = $request->id_tipo_telefone;
					$novo_telefone->id_classificacao_telefone = 1;
					$novo_telefone->nu_ddd = $request->nu_ddd;
					$novo_telefone->nu_telefone = $request->nu_telefone;

					if ($novo_telefone->save()) {
						$alterar_pessoa->telefones()->attach($novo_telefone);
					} else {
						$erro = 'USU011';
					}
				}

				if (count($request->id_vinculo)>0) {
					// Isso precisa ser trocado URGENTEMENTE, não se deve deletar relações antes de incluir novas
					$usuario_pessoas = new usuario_pessoa();
					$usuario_pessoas = $usuario_pessoas->where('id_usuario',$alterar_usuario->id_usuario);

					if ($usuario_pessoas->delete()) {
						$erro_loop_rel = false;
						$i=0;
						foreach ($request->id_vinculo as $id_vinculo) {
							switch ($request->vinculo_id_tipo_usuario[$i]) {
								case 4:
									$vara_selecionada = new vara();
									$vara_selecionada = $vara_selecionada->find($id_vinculo);
									$id_pessoa = $vara_selecionada->id_pessoa;
									break;
								case 2: case 5: case 10:
									$serventia_selecionada = new serventia();
									$serventia_selecionada = $serventia_selecionada->find($id_vinculo);
									$id_pessoa = $serventia_selecionada->id_pessoa;
									break;
								case 7:
									$unidade_gestora_selecionada = new unidade_gestora();
									$unidade_gestora_selecionada = $unidade_gestora_selecionada->find($id_vinculo);
									$id_pessoa = $unidade_gestora_selecionada->id_pessoa;
									break;
								case 11:
									$unidade_judiciaria_divisao_selecionada = new unidade_judiciaria_divisao();
									$unidade_judiciaria_divisao_selecionada = $unidade_judiciaria_divisao_selecionada->find($id_vinculo);
									$id_pessoa = $unidade_judiciaria_divisao_selecionada->id_pessoa;
									break;
							}
							$novo_usuario_pessoa = new usuario_pessoa();
							$novo_usuario_pessoa->id_usuario = $alterar_usuario->id_usuario;
							$novo_usuario_pessoa->id_pessoa = $id_pessoa;
							$novo_usuario_pessoa->id_usuario_cad = Auth::User()->id_usuario;
                            $novo_usuario_pessoa->in_master_recebe_notificacoes_sistema = $request->in_master_recebe_notificacoes_sistema ? $request->in_master_recebe_notificacoes_sistema : 'N';
                            $novo_usuario_pessoa->in_usuario_recebe_notificacoes_sistema = $request->in_usuario_recebe_notificacoes_sistema ? $request->in_usuario_recebe_notificacoes_sistema : 'N';
                            if (!$novo_usuario_pessoa->save()) {
								$erro_loop_rel = true;
							}
							
							$i++;
						}
						if ($erro_loop_rel) {
							$erro = 'USU013';
						}
					} else {
						$erro = 'USU012';
					}
				}
			} else {
				$erro = 'USU014';
			}
		} else {
			$erro = 'USU015';
		}

		// Tratamento do retorno
		if ($erro>0) {
			DB::rollback();
			return response()->json(array('status'=> 'erro',
										  'recarrega' => 'false',
										  'msg' => 'Por favor, tente novamente mais tarde. Erro '.$erro));
		} else {
			DB::commit();

			return response()->json(array('status'=> 'sucesso', 
										  'recarrega' => 'true',
										  'msg' => 'O usuário foi alterado com sucesso.'));
		}
	}
	
	public function totalUsuarios(Request $request) {
		$usuarios = $this->usuario->where('email_usuario',$request->email_usuario)->count();
		return $usuarios;
	}
	
	public function confirmar($confirmar_token, usuario $usuario, usuario_senha $usuario_senha) {
		if ($confirmar_token!='') {
			$usuario = $usuario->where('confirmar_token',$confirmar_token)->first();
			
			if ($usuario) {
				$usuario->in_confirmado = 'S';
				$usuario->confirmar_token = NULL;
				$usuario->dt_usuario_confirmado = Carbon::now();
				$usuario->save();

				$total_senhas = $usuario_senha->where('id_usuario',$usuario->id_usuario)->count();
				if ($total_senhas<=0) {
					$senha_gerada = str_random(10);

					$nova_senha = new usuario_senha();
					$nova_senha->id_usuario = $usuario->id_usuario;
					$nova_senha->senha = Hash::make($senha_gerada);
					$nova_senha->save();

					Mail::send('email.cadastro-confirmado', ['pessoa' => $usuario->pessoa, 'senha_gerada' => $senha_gerada], function ($mail) use ($usuario) {
						$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
							 ->subject('CERI - Cadastro confirmado com sucesso');
					});
				}

				$status = 'SUCESSO';
			} else {
				$status = 'ERRO';
			}
			
			return view('confirmado',compact('status','usuario'));
		}
	}
	
	public function aprovar_usuario(Request $request, usuario $usuario, serventia $serventia, vara $vara) {
		if ($request->id_usuario>0) {
			$erro = 0;

			DB::beginTransaction();
			
			$usuario = $usuario->find($request->id_usuario);
			
			if ($usuario) {
				$usuario->in_aprovado = 'S';
				$usuario->dt_usuario_aprovado = Carbon::now();
				if (!$usuario->save()) {
					$erro = 1;
				}

				$senha_gerada = '';
				if ($usuario->in_alterar_senha=='S') {
					$senha_gerada = str_random(10);

					$nova_senha = new usuario_senha();
					$nova_senha->id_usuario = $usuario->id_usuario;
					$nova_senha->senha = Hash::make($senha_gerada);
					if (!$nova_senha->save()) {
						$erro = 1;
					}
				}
			} else {
				$erro = 1;
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				Mail::send('email.cadastro-aprovado', ['pessoa' => $usuario->pessoa, 'senha_gerada' => $senha_gerada], function ($mail) use ($usuario) {
					$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
						 ->subject('CERI - Cadastro aprovado com sucesso');
				});
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O usuário foi aprovado com sucesso.'));
			}
		}
	}
	
	public function desativar_usuario(Request $request, usuario $usuario) {
		if ($request->id_usuario>0) {
			$erro = 0;

			DB::beginTransaction();
			
			$usuario = $usuario->find($request->id_usuario);
			
			if ($usuario) {
				$usuario->in_registro_ativo = 'N';
				
				if (!$usuario->save()) {
					$erro = 1;
				}
			} else {
				$erro = 1;
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O usuário foi desativado com sucesso.'));
			}
		}
	}
	
	public function reativar_usuario(Request $request, usuario $usuario) {
		if ($request->id_usuario>0) {
			$erro = 0;

			DB::beginTransaction();
			
			$usuario = $usuario->find($request->id_usuario);
			
			if ($usuario) {
				$usuario->in_registro_ativo = 'S';
				
				if (!$usuario->save()) {
					$erro = 1;
				}
			} else {
				$erro = 1;
			}
			
			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'O usuário foi reativado com sucesso.'));
			}
		}
	}

	public function reenviar_confirmacao() {
		return view('reenviar-confirmacao');
	}

	public function enviar_confirmacao(Request $request) {
		if ($request->in_interno == 'S') {
			$usuario = $this->usuario->where('id_usuario',$request->id_usuario)->first();
			if ($usuario->in_confirmado=='N') {
				Mail::send('email.confirmar-cadastro', ['pessoa'=>$usuario->pessoa,'confirmar_token'=>$usuario->confirmar_token] , function($mail) use ($usuario) {
					$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
						->subject('CERI - Confirmação de cadastro');
				});
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'A confirmação foi reenviada com sucesso.'));
			} else {
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'O usuário já confirmou seu cadastro.'));
			}
		} else {
			$usuario = $this->usuario->where('email_usuario',$request->usuario)
									 ->whereOr('login_usuario',$request->usuario)
									 ->first();

			if ($usuario) {
				if ($usuario->in_confirmado=='N') {
					Mail::send('email.confirmar-cadastro', ['pessoa'=>$usuario->pessoa,'confirmar_token'=>$usuario->confirmar_token] , function($mail) use ($usuario) {
						$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
							->subject('CERI - Confirmação de cadastro');
					});
					return Redirect::back()->with('status', '3000');
				} else {
					return Redirect::back()->withInput($request->all())
						->withErrors(['usuario' => '3002']);
				}
			} else {
				return Redirect::back()->withInput($request->all())
					->withErrors(['usuario' => '3001']);
			}

		}
	}

	public function gerar_nova_senha(Request $request, usuario $usuario) {
		if ($request->id_usuario>0) {
			$erro = 0;

			DB::beginTransaction();

			$usuario = $usuario->find($request->id_usuario);
			
			if ($usuario) {
				$senha_gerada = str_random(10);

				$nova_senha = new usuario_senha();
				$nova_senha->id_usuario = $usuario->id_usuario;
				$nova_senha->senha = Hash::make($senha_gerada);
				if(!$nova_senha->save()) {
					$erro = 'USU017';
				}

				Mail::send('email.usuario-nova-senha', ['pessoa' => $usuario->pessoa, 'senha_gerada' => $senha_gerada], function ($mail) use ($usuario) {
					$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
						 ->subject('CERI - Nova senha gerada com sucesso');
				});
			} else {
				$erro = 'USU016';
			}

			// Tratamento do retorno
			if ($erro>0) {
				DB::rollback();
				return response()->json(array('status'=> 'erro',
											  'recarrega' => 'false',
											  'msg' => 'Por favor, tente novamente mais tarde. Erro nº '.$erro));
			} else {
				DB::commit();
				return response()->json(array('status'=> 'sucesso', 
											  'recarrega' => 'true',
											  'msg' => 'Uma nova senha foi gerada com sucesso para o usuário.'));
			}
		}
	}

	public function enviar_senha(Request $request) {
		$usuario = $this->usuario->where('email_usuario',$request->usuario)
								 ->whereOr('login_usuario',$request->usuario)
								 ->first();

		if ($usuario) {
			if ($usuario->in_registro_ativo=='S') {
				$recuperar_token = str_random(30);
				$usuario->recuperar_token = $recuperar_token;
				if ($usuario->save()) {
					Mail::send('email.recuperar-senha', ['pessoa'=>$usuario->pessoa,'recuperar_token'=>$recuperar_token] , function($mail) use ($usuario) {
						$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
							 ->subject('CERI - Recuperação de senha');
					});
					return Redirect::back()->with('status', '4000')->withInput($request->all());
				} else {
					return Redirect::back()->withInput($request->all())
										   ->withErrors(['usuario' => '4003']);
				}
			} else {
				return Redirect::back()->withInput($request->all())
									   ->withErrors(['usuario' => '4002']);
			}
		} else {
			return Redirect::back()->withInput($request->all())
								   ->withErrors(['usuario' => '4001']);
		}
		return redirect('/');
	}

	public function salvar_senha(Request $request, usuario $usuario, usuario_senha $usuario_senha) {
		if ($request->recuperar_token!='') {
			$usuario = $usuario->where('recuperar_token',$request->recuperar_token)->first();
			
			if ($usuario) {
				$senhas = $usuario_senha->where('id_usuario',$usuario->id_usuario)->get();
				$senha_existente = false;
				if (count($senhas)>0) {
					foreach ($senhas as $senha) {
						if (Hash::check($request->senha_usuario, $senha->senha)) {
							$senha_existente = true;
						}
					}
				}
				if ($senha_existente==false) {
					$nova_senha = $usuario_senha;
					$nova_senha->id_usuario = $usuario->id_usuario;
					$nova_senha->senha = Hash::make($request->senha_usuario);
					
					if ($nova_senha->save()) {
						return Redirect::back()->with('status', '5000');
					}
				} else {
					return Redirect::back()->withInput($request->all())
								   		   ->withErrors(['usuario' => '5002']);
				}
			} else {
				return Redirect::back()->withInput($request->all())
								   	   ->withErrors(['usuario' => '5001']);
			}
		}
	}

    public function configuracoes_pessoais(Request $request, estado $estado, banco $banco, pessoa_endereco $pessoa_endereco) {
        $usuario = $this->usuario;

        $estados        = $estado->orderBy('no_estado')->get();
        $codigo_bancos  = $banco->where("in_registro_ativo", "=", "S")->orderBy('codigo_banco')->get();
        $bancos         = $banco->where("in_registro_ativo", "=", "S")->orderBy('no_banco')->get();
        $endereco_atual = $pessoa_endereco->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
                                            ->where("id_pessoa", $this->id_pessoa)
                                            ->where("in_registro_ativo", 'S')
                                            ->orderBy('id_pessoa_endereco', 'desc')
                                            ->limit(1)
                                            ->get();


        if (strstr(Route::current()->getPath(),'dados-bancarios')){
            return view('configuracoes.configuracoes-dados-bancarios',compact('codigo_bancos', 'bancos','usuario'));

        } elseif(strstr(Route::current()->getPath(),'dados-serventia')){
           return view('configuracoes.configuracoes-dados-serventia',compact('estados', 'endereco_atual','usuario'));

        } else {
            return view('configuracoes.configuracoes-dadospessoais',compact('estados', 'endereco_atual','usuario'));
        }
    }

    public function configuracoes_salvar_serventia(Request $request, pessoa $pessoa, usuario_senha $usuario_senha, telefone $telefone, endereco $endereco, serventia $serventia, serventia_cliente $serventia_cliente){
        $alterar_pessoa_serventia = $pessoa->find(Auth::User()->usuario_serventia->serventia->pessoa->id_pessoa);
        $erro = 0;

        DB::beginTransaction();

        if (count(Auth::User()->usuario_serventia->serventia->pessoa->enderecos) > 0) {
            $alterar_endereco_serventia = $endereco->find(Auth::User()->usuario_serventia->serventia->pessoa->enderecos[0]->id_endereco);
        } else {
            $alterar_endereco_serventia = $endereco;
        }
        if (count(Auth::User()->usuario_serventia->serventia->pessoa->telefones) > 0) {
            $alterar_telefone_serventia = $telefone->find(Auth::User()->usuario_serventia->serventia->pessoa->telefones[0]->id_telefone);
        } else {
            $alterar_telefone_serventia = $telefone;
        }

        $alterar_pessoa_serventia->no_email_pessoa = $request->email_pessoa_serventia;

        if ($alterar_pessoa_serventia->save()) {
            if ($request->id_serventia > 0) {
                $alterar_serventia = $serventia->find($request->id_serventia);
                $alterar_serventia->no_serventia = strtoupper($request->no_serventia);
                $alterar_serventia->abv_serventia = strtoupper($request->no_serventia);
                $alterar_serventia->hora_inicio_expediente = $request->hora_inicio_expediente;
                $alterar_serventia->hora_inicio_almoco = $request->hora_inicio_almoco;
                $alterar_serventia->hora_termino_almoco = $request->hora_termino_almoco;
                $alterar_serventia->hora_termino_expediente = $request->hora_termino_expediente;
                $alterar_serventia->no_oficial = strtoupper($request->no_oficial);
                $alterar_serventia->no_substituto = strtoupper($request->no_substituto);
                if ($alterar_serventia->save()) {
                    if ($request->id_tipo_telefone_serventia > 0) {
                        $alterar_telefone_serventia->id_tipo_telefone = $request->id_tipo_telefone_serventia;
                        $alterar_telefone_serventia->id_classificacao_telefone = 1;
                        $alterar_telefone_serventia->nu_ddd = $request->nu_ddd_serventia;
                        $alterar_telefone_serventia->nu_telefone = $request->nu_telefone_serventia;
                        if (!$alterar_telefone_serventia->save()) {
                            $erro = 1;
                        }
                        if (count(Auth::User()->usuario_serventia->serventia->pessoa->telefones) == 0) {
                            $alterar_pessoa_serventia->telefones()->attach($alterar_telefone_serventia);
                        }
                    }

                    $alterar_serventia_cliente = $serventia_cliente->where('id_serventia', $request->id_serventia)->first();
                    $alterar_serventia_cliente->codigo_cns = ($request->codigo_cns != "") ? $request->codigo_cns : null;
                    $alterar_serventia_cliente->dv_codigo_cns = ($request->dv_codigo_cns != "") ? $request->dv_codigo_cns : null;
                    if (!$alterar_serventia_cliente->save()) {
                        $erro = 1;
                    }

                    $alterar_endereco_serventia->no_endereco = $request->no_endereco_serventia;
                    $alterar_endereco_serventia->no_complemento = $request->no_complemento_serventia;
                    $alterar_endereco_serventia->nu_endereco = $request->nu_endereco_serventia;
                    $alterar_endereco_serventia->no_bairro = $request->no_bairro_serventia;
                    $alterar_endereco_serventia->id_cidade = $request->id_cidade_serventia;
                    $alterar_endereco_serventia->co_uf = $request->co_uf_serventia;
                    $alterar_endereco_serventia->nu_cep = preg_replace('#[^0-9]#', '', $request->nu_cep_serventia);
                    $alterar_endereco_serventia->id_cidade = $request->id_cidade_serventia;
                    if (!$alterar_endereco_serventia->save()) {
                        $erro = 1;
                    }
                    if (count(Auth::User()->usuario_serventia->serventia->pessoa->enderecos) == 0) {
                        $alterar_pessoa_serventia->enderecos()->attach($alterar_endereco_serventia);
                    }

                } else {
                    $erro = 1;
                }
            } else {
                $erro = 1;
            }
            if ($erro > 0) {
                DB::rollback();
                return 'ERRO';
            } else {
                DB::commit();
                return 'SUCESSO';
            }
        }

    }
    public function configuracoes_salvar_banco (Request $request, banco_pessoa $banco_pessoa){
	    $erro = 0;

	    DB::beginTransaction();

        if (count(Auth::User()->usuario_serventia->serventia->pessoa->banco_pessoa)>0) {
            $alterar_banco_pessoa = $banco_pessoa->find(Auth::User()->usuario_serventia->serventia->pessoa->banco_pessoa->id_banco_pessoa);
        } else {
            $alterar_banco_pessoa = $banco_pessoa;
        }

        //SALVANDO ARQUIVOS
        $alterar_banco_pessoa->id_banco                 = $request->id_banco;
        $alterar_banco_pessoa->id_pessoa                = Auth::User()->usuario_serventia->serventia->pessoa->id_pessoa;
        $alterar_banco_pessoa->nu_agencia               = $request->nu_agencia;
        $alterar_banco_pessoa->nu_dv_agencia            = $request->nu_dv_agencia;
        $alterar_banco_pessoa->nu_conta                 = $request->nu_conta;
        $alterar_banco_pessoa->nu_dv_conta              = $request->nu_dv_conta;
        $alterar_banco_pessoa->tipo_conta               = $request->tipo_conta;
        $alterar_banco_pessoa->nu_variacao              = $request->nu_variacao;
        $alterar_banco_pessoa->nu_ordem                 = $request->nu_ordem;
        $alterar_banco_pessoa->in_registro_ativo        = $request->in_registro_ativo;
        $alterar_banco_pessoa->id_usuario_alt           = $request->id_usuario_alt;
        $alterar_banco_pessoa->dt_alteracao             = $request->dt_alteracao;
        $alterar_banco_pessoa->in_registro_ativo        = 'S';
        $alterar_banco_pessoa->id_usuario_cad           = Auth::User()->id_usuario;
        $alterar_banco_pessoa->dt_cadastro              = Carbon::now();
        $alterar_banco_pessoa->in_tipo_pessoa_conta		= $request->tp_pessoa;
        $alterar_banco_pessoa->nu_cpf_cnpj_conta		= preg_replace( '#[^0-9]#', '', $request->nu_cpf_cnpj_conta);
        if (!$alterar_banco_pessoa->save()){
            $erro = 1;
        }

        if ($erro>0) {
            DB::rollback();
            return 'ERRO';
        } else {
            DB::commit();
            return 'SUCESSO';
        }

    }
    public function configuracoes_salvar_pessoais(Request $request, pessoa $pessoa, usuario_senha $usuario_senha, telefone $telefone, endereco $endereco) {
        $alterar_pessoa = $pessoa->find(Auth::User()->pessoa->id_pessoa);
        $alterar_usuario = $this->usuario->find(Auth::User()->id_usuario);
        $erro = 0;

        DB::beginTransaction();

        if (count(Auth::User()->pessoa->enderecos)>0) {
            $alterar_endereco = $endereco->find(Auth::User()->pessoa->enderecos[0]->id_endereco);
        } else {
            $alterar_endereco = $endereco;
        }
        if (count(Auth::User()->pessoa->telefones)>0) {
            $alterar_telefone = $telefone->find(Auth::User()->pessoa->telefones[0]->id_telefone);
        } else {
            $alterar_telefone = $telefone;
        }

        $alterar_pessoa->no_pessoa = $request->no_pessoa;
        $alterar_pessoa->tp_pessoa = $request->tp_pessoa;


        if (Auth::User()->pessoa->tp_pessoa=='F') {
            $data_nascimento = Carbon::createFromFormat('d/m/Y',$request->dt_nascimento);
            $alterar_pessoa->dt_nascimento = $data_nascimento;
            $alterar_pessoa->tp_sexo = $request->tp_sexo;
        } elseif (Auth::User()->pessoa->tp_pessoa=='J') {
            $alterar_pessoa->no_fantasia = $request->no_fantasia;
            $alterar_pessoa->tp_sexo = 'N';
        }

        if ($alterar_pessoa->save()) {
                $alterar_usuario->no_usuario = $request->no_pessoa;
                if (!$alterar_usuario->save()){
                    $erro = 1;
                }

                $alterar_endereco->id_cidade = $request->id_cidade;
                $alterar_endereco->no_endereco = $request->no_endereco;
                $alterar_endereco->nu_endereco = $request->nu_endereco;
                $alterar_endereco->no_bairro = $request->no_bairro;
                $alterar_endereco->nu_cep = preg_replace( '#[^0-9]#', '', $request->nu_cep);
                $alterar_endereco->no_complemento = $request->no_complemento;
                if (!$alterar_endereco->save()){
                    $erro = 1;
                }

                if (count(Auth::User()->pessoa->enderecos)==0) {
                    $alterar_pessoa->enderecos()->attach($alterar_endereco);
                }

                if ($request->id_tipo_telefone>0) {
                    $alterar_telefone->id_tipo_telefone = $request->id_tipo_telefone;
                    $alterar_telefone->id_classificacao_telefone = 1;
                    $alterar_telefone->nu_ddd = $request->nu_ddd;
                    $alterar_telefone->nu_telefone = $request->nu_telefone;
                    if (!$alterar_telefone->save()){
                        $erro = 1;
                    }
                    if (count(Auth::User()->pessoa->telefones)==0) {
                        $alterar_pessoa->telefones()->attach($alterar_telefone);
                    }
                }

        } else {
            $erro = 1;
        }
        if ($erro>0) {
            DB::rollback();
            return 'ERRO';
        } else {
            DB::commit();
            return 'SUCESSO';
        }
	}

	public function configuracoes_acesso(Request $request) {
        $usuario = $this->usuario;
		return view('configuracoes.configuracoes-dadosacesso',compact('usuario'));
	}

	public function configuracoes_salvar_acesso(Request $request, usuario_senha $usuario_senha) {
		if (Hash::check($request->senha_usuario, Auth::User()->usuario_senha()->orderBy('dt_cadastro','desc')->first()->senha)) {
			$senha_atual = Auth::User()->usuario_senha()->orderBy('dt_cadastro','desc')->first();
			$senha_atual->update(['dt_fim_periodo' => Carbon::now()]);

		    $senhas = $usuario_senha->where('id_usuario',Auth::User()->id_usuario)->get();
			$senha_existente = false;

			if (count($senhas)>0) {
				foreach ($senhas as $senha) {
					if (Hash::check($request->nova_senha_usuario, $senha->senha)) {
						$senha_existente = true;
					}
				}
			}
			if ($senha_existente==false) {
				$nova_senha = $usuario_senha;
                $nova_senha->dt_ini_periodo = Carbon::now();
				$nova_senha->id_usuario = Auth::User()->id_usuario;
				$nova_senha->senha = Hash::make($request->nova_senha_usuario);
				
				if ($nova_senha->save()) {
					return 'SUCESSO';
				}
			} else {
				return 'ERRO_02';
			}
		} else {
			return 'ERRO_01';
		}
	}

	public function configuracoes_certificado(Request $request) {
        $usuario = $this->usuario;
		return view('configuracoes.configuracoes-certificado',compact('usuario'));
	}
	public function configuracoes_salvar_certificado(Request $request) {
		$destino = '../storage/app/certificados/'.Auth::User()->usuario_serventia->id_serventia;
		if (!File::isDirectory($destino)) {
			File::makeDirectory($destino);
		}

		openssl_pkcs12_read(File::get($request->no_arquivo->getRealPath()), $certificado_x509, "1234");
		$certificado = openssl_x509_parse($certificado_x509['cert']);

		if ($certificado['validTo_time_t']>Carbon::now()->timestamp) {
			$partes_cn = explode(':',$certificado['subject']['CN']);

			$novo_certificado = new serventia_certificado();
			$novo_certificado->id_serventia = Auth::User()->usuario_serventia->id_serventia;
			
			$novo_certificado->no_comum = $certificado['subject']['CN'];

			if ($certificado['extensions']['subjectAltName']!='') {
				$email = explode(',',$certificado['extensions']['subjectAltName']);
				$email = str_replace('email:', '', $email[0]);

				if ($email!='') {
					$novo_certificado->no_email = $email;
				}
			}
			if (isset($partes_cn[1])) {
				$novo_certificado->nu_cpf_cnpj = $partes_cn[1];
			}
			$novo_certificado->tp_certificado = $certificado['subject']['OU'][1];
			$novo_certificado->no_autoridade_raiz = $certificado['issuer']['O'];
			$novo_certificado->no_autoridade_certificadora = $certificado['issuer']['CN'];
			$novo_certificado->dt_validade_ini = Carbon::createFromTimestamp($certificado['validFrom_time_t']);
			$novo_certificado->dt_validade_fim = Carbon::createFromTimestamp($certificado['validTo_time_t']);
			$novo_certificado->in_registro_ativo = 'S';

			if ($novo_certificado->save()) {
				$destino_final = $destino.'/'.$novo_certificado->id_serventia_certificado;
				if (!File::isDirectory($destino_final)) {
					File::makeDirectory($destino_final);
				}
				$nome_arquivo = 'certificado.pfx';
				if ($request->no_arquivo->move($destino_final, $nome_arquivo)) {
					$novo_certificado->no_arquivo = $nome_arquivo;
					$novo_certificado->no_local_arquivo = '/certificados/'.Auth::User()->usuario_serventia->id_serventia.'/'.$novo_certificado->id_serventia_certificado;
					$novo_certificado->save();

					$alterar_antigos = new serventia_certificado();
					$alterar_antigos->where('id_serventia',Auth::User()->usuario_serventia->id_serventia)
									->where('id_serventia_certificado','<>',$novo_certificado->id_serventia_certificado)
									->update(['in_registro_ativo'=>'N']);
				}
				return 'SUCESSO';
			} else {
				return 'ERRO';
			}
		} else {
			return 'ERRO_01';
		}
	}

	public function listar(Request $request, pessoa $pessoa, usuario_senha $usuario_senha, telefone $telefone, endereco $endereco,tipo_usuario $tipo_usuario,estado $estado, nacionalidade $nacionalidade, estado_civil $estado_civil){
		$usuarios = $pessoa->leftjoin('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
			->leftjoin('pessoa_telefone','pessoa_telefone.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('telefone','telefone.id_telefone','=','pessoa_telefone.id_telefone')
			->leftjoin('nacionalidade','nacionalidade.id_nacionalidade','=','pessoa.id_nacionalidade')
			->leftjoin('estado_civil','estado_civil.id_estado_civil','=','pessoa.id_estado_civil')
			->join('usuario','usuario.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('tipo_usuario','tipo_usuario.id_tipo_usuario','=','usuario.id_tipo_usuario')
			->get();
		$todosEstados = $estado->orderBy('no_estado')->get();
		$todasNacionalidades = $nacionalidade->orderBy('no_nacionalidade')->get();
		$todosEstadosCivis = $estado_civil->orderBy('no_estado_civil')->get();
		$tipo_usuario = $tipo_usuario->all();

		return view('usuarios.listar',compact('tipo_usuario','usuarios','todosEstados','todasNacionalidades','todosEstadosCivis'));
	}

	public function editarInformacoesPessoais(Request $request, pessoa $pessoa, usuario_senha $usuario_senha, telefone $telefone, endereco $endereco,tipo_usuario $tipo_usuario,estado $estado, nacionalidade $nacionalidade, estado_civil $estado_civil){
		$usuarios = $pessoa->leftjoin('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
			->leftjoin('estado','estado.uf','=','endereco.co_uf')
			->leftjoin('pessoa_telefone','pessoa_telefone.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('telefone','telefone.id_telefone','=','pessoa_telefone.id_telefone')
			->leftjoin('nacionalidade','nacionalidade.id_nacionalidade','=','pessoa.id_nacionalidade')
			->leftjoin('estado_civil','estado_civil.id_estado_civil','=','pessoa.id_estado_civil')
			->join('usuario','usuario.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('tipo_usuario','tipo_usuario.id_tipo_usuario','=','usuario.id_tipo_usuario')
			->where('id_usuario', Auth::User()->id_usuario)
			->get();
		$todosEstados = $estado->orderBy('no_estado')->get();
		$todasNacionalidades = $nacionalidade->orderBy('no_nacionalidade')->get();
		$todosEstadosCivis = $estado_civil->orderBy('no_estado_civil')->get();
		$tipo_usuario = $tipo_usuario->all();

		return view('usuarios.informacoesPessoais',compact('tipo_usuario','usuarios','todosEstados','todasNacionalidades','todosEstadosCivis'));
	}

	public function listarUsuariosJson(Request $request, pessoa $pessoa, usuario_senha $usuario_senha, telefone $telefone, endereco $endereco){
		$UsuariosDoSistema = $pessoa->leftjoin('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
			->leftjoin('estado','estado.uf','=','endereco.co_uf')
			->leftjoin('pessoa_telefone','pessoa_telefone.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('telefone','telefone.id_telefone','=','pessoa_telefone.id_telefone')
			->leftjoin('nacionalidade','nacionalidade.id_nacionalidade','=','pessoa.id_nacionalidade')
			->leftjoin('estado_civil','estado_civil.id_estado_civil','=','pessoa.id_estado_civil')
			->join('usuario','usuario.id_pessoa','=','pessoa.id_pessoa')
			->leftjoin('tipo_usuario','tipo_usuario.id_tipo_usuario','=','usuario.id_tipo_usuario')
			->get();
		return response()->json($UsuariosDoSistema);
	}

	public function listarCidades(Request $request, estado $estado) {
		$cidades = $estado->find($request->id_estado)->cidades()->orderBy('no_cidade')->get();
		return response()->json($cidades);
	}

	public function detalhes(Request $request, pessoa $pessoa) {
		if ($request->id_usuario>0) {
			$usuario = $this->usuario->find($request->id_usuario);

			$comarcas = $pessoa->join('usuario as u', 'u.id_pessoa', '=', 'pessoa.id_pessoa')
				->join('usuario_pessoa as up','up.id_usuario','=','u.id_usuario')
				->join('pessoa as p2','p2.id_pessoa','=','up.id_pessoa')
				->join('vara as v','v.id_pessoa','=','p2.id_pessoa')
				->join('comarca as c','c.id_comarca','=','v.id_comarca')
				->select('u.id_pessoa', 'pessoa.id_pessoa', 'up.id_pessoa', 'pessoa.id_tipo_pessoa','c.no_comarca')
				->where('pessoa.nu_cpf_cnpj', limpar_mascara($usuario->pessoa->nu_cpf_cnpj))->get();

			return view('usuarios.usuarios-detalhes',compact('usuario','comarcas'));
		}
	}

	public function salvar_primeira_senha(Request $request, usuario $usuario, usuario_senha $usuario_senha) {
		if (Auth::attempt(array('id_usuario' => Auth::User()->id_usuario, 'password' => $request->senha_usuario_atual),false, false)) {
			$senhas = $usuario_senha->where('id_usuario',Auth::User()->id_usuario)->get();
			$senha_existente = false;
			if (count($senhas)>0) {
				foreach ($senhas as $senha) {
					if (Hash::check($request->senha_usuario, $senha->senha)) {
						$senha_existente = true;
					}
				}
			}
			if ($senha_existente==false) {
				$nova_senha = new usuario_senha();
				$nova_senha->id_usuario = Auth::User()->id_usuario;
				$nova_senha->senha = Hash::make($request->senha_usuario);
				
				if ($nova_senha->save()) {
					$usuario = $usuario->find(Auth::User()->id_usuario);
					$usuario->in_alterar_senha='N';
					$usuario->save();
					
					return Redirect::back()->with('status', '5000');
				}
			} else {
				return Redirect::back()->withInput($request->all())
							   		   ->withErrors(['usuario' => '5002']);
			}
		} else {
			return Redirect::back()->withInput($request->all())
							   	   ->withErrors(['usuario' => '5001']);
		}
	}

	public function enviar_email_usuarios_manuais(Request $request) {
		$usuarios = new usuario();
		$usuarios = $usuarios->where('dt_cadastro', '>', '2018-03-16 00:00:00')
							 ->where('in_confirmado', 'N')
							 ->orderBy('no_usuario','asc')
							 ->orderBy('dt_cadastro','desc')
							 ->get();

		foreach ($usuarios as $usuario) {
			if (strpos($usuario->email_usuario, '@hotmail')<=0 and 
				strpos($usuario->email_usuario, '@live')<=0 and 
				strpos($usuario->email_usuario, '@outlook')<=0 and 
				strpos($usuario->email_usuario, '@msn')<=0) {

				$mail = Mail::send('email.confirmar-cadastro', ['pessoa' => $usuario->pessoa, 'confirmar_token' => $usuario->confirmar_token], function ($mail) use ($usuario) {
					$mail->to($usuario->email_usuario, $usuario->pessoa->no_pessoa)
						 ->subject('CERI - Confirmação de cadastro');
				});
				
				if ($mail) {
					echo 'Enviado;'.$usuario->id_usuario.';'.$usuario->pessoa->id_pessoa.';'.$usuario->no_usuario.';'.$usuario->email_usuario.';'.$usuario->pessoa->id_tipo_pessoa.';'.$usuario->pessoa->nu_cpf_cnpj.';('.rtrim($usuario->pessoa->telefones[0]->nu_ddd).') '.rtrim($usuario->pessoa->telefones[0]->nu_telefone).';'.$usuario->dt_cadastro.'<br />';
				} else {
					echo 'Erro no envio;'.$usuario->id_usuario.';'.$usuario->pessoa->id_pessoa.';'.$usuario->no_usuario.';'.$usuario->email_usuario.';'.$usuario->pessoa->id_tipo_pessoa.';'.$usuario->pessoa->nu_cpf_cnpj.';('.rtrim($usuario->pessoa->telefones[0]->nu_ddd).') '.rtrim($usuario->pessoa->telefones[0]->nu_telefone).';'.$usuario->dt_cadastro.'<br />';
				}
			} else {
				echo 'Não enviado por ser @LIVE;'.$usuario->id_usuario.';'.$usuario->pessoa->id_pessoa.';'.$usuario->no_usuario.';'.$usuario->email_usuario.';'.$usuario->pessoa->id_tipo_pessoa.';'.$usuario->pessoa->nu_cpf_cnpj.';('.rtrim($usuario->pessoa->telefones[0]->nu_ddd).') '.rtrim($usuario->pessoa->telefones[0]->nu_telefone).';'.$usuario->dt_cadastro.'<br />';
			}
		}
	}

	public function troca_pessoa(Request $request) {
	    if ($request->key>=0) {
			$usuario_pessoas = Auth::User()->usuario_pessoa;
			if (isset($usuario_pessoas[$request->key])) {
			    Session::put('pessoa_ativa', $usuario_pessoas[$request->key]);
			    Session::put('troca_pessoa', true);
				return response()->json(array('status'=> 'sucesso',
											  'msg' => 'Troca efetuada com sucesso.'));
			} else {
				return response()->json(array('status'=> 'erro',
											  'msg' => 'A pessoa selecionada não existe.'));
			}
		}
	}

    public function configuracoes_dados_produtos(tabela_preco $tabela_preco, tabela_preco_serventia $tabela_preco_serventia, serventia $serventia) {
        $usuario = $this->usuario;

        $serventia = $serventia->find($usuario->usuario_serventia->serventia->id_serventia);
        $tabela_preco = $tabela_preco->all();

        $tabela_preco_serventia = $tabela_preco_serventia->where('id_serventia', $usuario->usuario_serventia->serventia->id_serventia)
            ->where('in_registro_ativo', 'S')
            ->first();

        foreach ($serventia->alienacao_preco_produto_item_variavel_serventia as $alienacao_preco_produto_item_variavel_serventia) {
            switch ($alienacao_preco_produto_item_variavel_serventia->alienacao_preco_produto_item_variavel->id_produto_item) {
                case 31:
                    $valor_produto_item['ar'] = $alienacao_preco_produto_item_variavel_serventia->alienacao_preco_produto_item_variavel->va_preco;
                    break;
                case 39:
                    $valor_produto_item['averbacao']  = $alienacao_preco_produto_item_variavel_serventia->alienacao_preco_produto_item_variavel->va_preco;
                    break;
            }
        }

        return view('configuracoes.configuracoes-dados-produtos',compact('tabela_preco', 'tabela_preco_serventia', 'valor_produto_item','usuario'));
    }

    public function atualizar_valor_produtos($dados){
        $erro = 0;

        $validator = Validator::make($dados, [
            'va_preco'          =>  'required',
            'dt_ini_vigencia'   =>  'required',
            'dt_fim_vigencia'   =>  'required',
            'alterar_alienacao_preco_produto_item_variavel_serventia' => 'required',
            'id_serventia'      =>  'required',
        ]);
        if ($validator->fails()) {
            return 1;
        }

        $alterar_alienacao_preco_produto_item_variavel_serventia = $dados['alterar_alienacao_preco_produto_item_variavel_serventia'];

        //Desabilita todos valores variáveis
        $alterar_alienacao_preco_produto_item_variavel_serventia->dt_fim_vigencia = $dados['dt_fim_vigencia'];
        $alterar_alienacao_preco_produto_item_variavel_serventia->in_registro_ativo = 'N';
        $alterar_alienacao_preco_produto_item_variavel_serventia->id_usuario_alt = Auth::User()->id_usuario;
        $alterar_alienacao_preco_produto_item_variavel_serventia->dt_alteracao = Carbon::now();
        if ($alterar_alienacao_preco_produto_item_variavel_serventia->save()) {
            //insere novo valor
            $novo_alienacao_preco_produto_item_variavel = new alienacao_preco_produto_item_variavel();
            $novo_alienacao_preco_produto_item_variavel->id_produto_item = $alterar_alienacao_preco_produto_item_variavel_serventia->id_produto_item;
            $novo_alienacao_preco_produto_item_variavel->va_preco = converte_float($dados['va_preco']);
            $novo_alienacao_preco_produto_item_variavel->dt_ini_vigencia = $dados['dt_ini_vigencia'];
            $novo_alienacao_preco_produto_item_variavel->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_alienacao_preco_produto_item_variavel->save()) {
                $erro = 1;
            }

            //atualiza tabela com novo valor
            $novo_alienacao_preco_produto_item_variavel_serventia = new alienacao_preco_produto_item_variavel_serventia();
            $novo_alienacao_preco_produto_item_variavel_serventia->id_alienacao_preco_produto_item_variavel = $novo_alienacao_preco_produto_item_variavel->id_alienacao_preco_produto_item_variavel;
            $novo_alienacao_preco_produto_item_variavel_serventia->id_produto_item = $novo_alienacao_preco_produto_item_variavel->id_produto_item;
            $novo_alienacao_preco_produto_item_variavel_serventia->id_serventia = $dados['id_serventia'];
            $novo_alienacao_preco_produto_item_variavel_serventia->dt_ini_vigencia = $dados['dt_ini_vigencia'];
            $novo_alienacao_preco_produto_item_variavel_serventia->id_usuario_cad = Auth::User()->id_usuario;
            if (!$novo_alienacao_preco_produto_item_variavel_serventia->save()) {
                $erro = 1;
            }
        } else {
            $erro = 1;
        }

        return $erro;
    }

    public function configuracoes_salvar_pordutos(Request $request, tabela_preco_serventia $tabela_preco_serventia, serventia $serventia){
        DB::beginTransaction();
        $erro = 0;
        $info = 0;
        $serventia = $serventia->find(Auth::User()->usuario_serventia->serventia->id_serventia);

        $dt_fim_vigencia_ar = $request->has('dt_ini_vigencia_ar') ? Carbon::createFromFormat('d/m/Y',$request->dt_ini_vigencia_ar)->subDay(1)->format('d/m/Y') : '';
        $dt_fim_vigencia_averbacao = $request->has('dt_ini_vigencia_averbacao') ? Carbon::createFromFormat('d/m/Y',$request->dt_ini_vigencia_averbacao)->subDay(1)->format('d/m/Y') : '';
        $dt_fim_vigencia_issqn = $request->has('dt_ini_vigencia_issqn') ? Carbon::createFromFormat('d/m/Y',$request->dt_ini_vigencia_issqn)->subDay(1)->format('d/m/Y'): '';

        $taxa_atual_issqn = $tabela_preco_serventia->where('id_serventia',$request->id_serventia)
                                                    ->where('in_registro_ativo','S')
                                                    ->first();

        if ($request->has('id_taxa_issqn') && $taxa_atual_issqn->id_tabela_preco != $request->id_taxa_issqn) {
            //Desabilita taxa atual issqn
            $taxa_atual_issqn->dt_fim_vigencia = $dt_fim_vigencia_issqn;
            $taxa_atual_issqn->in_registro_ativo = 'N';
            $taxa_atual_issqn->id_usuario_alt = Auth::User()->id_usuario;
            $taxa_atual_issqn->dt_alteracao = Carbon::now();
            if ($taxa_atual_issqn->save()) {
                //Insere nova taxa issqn
                $nova_taxa_issqn = $tabela_preco_serventia;

                $nova_taxa_issqn->id_serventia = $request->id_serventia;
                $nova_taxa_issqn->id_tabela_preco = $request->id_taxa_issqn;
                $nova_taxa_issqn->dt_ini_vigencia = $request->dt_ini_vigencia_issqn;
                $nova_taxa_issqn->id_usuario_cad = Auth::User()->id_usuario;
                if (!$nova_taxa_issqn->save()) {

                    $erro = 1;
                }
            } else {
                $erro = 1;
            }
        } else {
            $info++;
        }

        foreach ($serventia->alienacao_preco_produto_item_variavel_serventia as $alterar_alienacao_preco_produto_item_variavel_serventia) {
            switch ($alterar_alienacao_preco_produto_item_variavel_serventia->id_produto_item) {
                case 31:
                    if ($request->has('valor_ar') && $alterar_alienacao_preco_produto_item_variavel_serventia->alienacao_preco_produto_item_variavel->va_preco != converte_float($request->valor_ar)) {

                        $dados = [  'va_preco'          => $request->valor_ar,
                                    'dt_ini_vigencia'   => $request->dt_ini_vigencia_ar,
                                    'dt_fim_vigencia'   => $dt_fim_vigencia_ar,
                                    'alterar_alienacao_preco_produto_item_variavel_serventia' => $alterar_alienacao_preco_produto_item_variavel_serventia,
                                    'id_serventia'      => $request->id_serventia,
                                 ];

                        $erro = $this->atualizar_valor_produtos($dados);

                    } else {
                        $info++;
                    }
                    break;
                case 39:
                    if ($request->has('valor_averbacao') && $alterar_alienacao_preco_produto_item_variavel_serventia->alienacao_preco_produto_item_variavel->va_preco != converte_float($request->valor_averbacao)) {

                        $dados = [  'va_preco'          => $request->valor_averbacao,
                                    'dt_ini_vigencia'   => $request->dt_ini_vigencia_averbacao,
                                    'dt_fim_vigencia'   => $dt_fim_vigencia_averbacao,
                                    'alterar_alienacao_preco_produto_item_variavel_serventia' => $alterar_alienacao_preco_produto_item_variavel_serventia,
                                    'id_serventia'      => $request->id_serventia,
                                 ];

                        $erro = $this->atualizar_valor_produtos($dados);

                    } else {
                        $info++;
                    }
                    break;
            }
        }

        if ($erro > 0) {
            DB::rollback();
            return response()->json(array('status' => 'erro',
                'recarrega' => false,
                'msg' => 'Por favor, tente novamente mais tarde. Erro nº ' . $erro));
        } elseif ($info > 2) {
            return response()->json(array('status' => 'info',
                'recarrega' => false,
                'msg' => 'Os dados não foram atualizados pois não houve alteração nos valores!'));
        } else {
            DB::commit();
            return response()->json(array('status' => 'sucesso',
                'recarrega' => true,
                'msg' => 'Os valores dos produtos foram atualizados!'));
        }
    }
}
