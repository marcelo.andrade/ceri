<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class situacao_alienacao_pagamento extends Model {
	
	protected $table = 'situacao_alienacao_pagamento';
	protected $primaryKey = 'id_situacao_alienacao_pagamento';
    public $timestamps = false;
    protected $guarded  = array();
	
}
