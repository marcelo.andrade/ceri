<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class periodo_certidao extends Model {
	
	protected $table = 'periodo_certidao';
	protected $primaryKey = 'id_periodo_certidao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function certidao() {
		return $this->hasOne('App\certidao','id_periodo_certidao');
	}
}
