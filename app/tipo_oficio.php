<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_oficio extends Model {
	
	protected $table = 'tipo_oficio';
	protected $primaryKey = 'id_tipo_oficio';
    public $timestamps = false;
    protected $guarded  = array();
	
}