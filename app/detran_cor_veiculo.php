<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_cor_veiculo extends Model {
	protected $table = 'cor_veiculo';
	protected $primaryKey = 'id_cor_veiculo';
    public $timestamps = false;
    protected $guarded  = array();
	
}
