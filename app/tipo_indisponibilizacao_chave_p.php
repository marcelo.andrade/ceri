<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_indisponibilizacao_chave_p extends Model {
	
	protected $table = 'tipo_indisponibilizacao_chave_p';
	protected $primaryKey = 'id_tipo_indisponibilizacao_chav';
    public $timestamps = false;
    protected $guarded  = array();
	
}