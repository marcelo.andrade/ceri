<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_serventia_notificacao extends Model {
	
	protected $table = 'pedido_serventia_notificacao';
	protected $primaryKey = 'id_pedido_serventia_notificacao';
    public $timestamps = false;
    protected $guarded  = array();
	
}