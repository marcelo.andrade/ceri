<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class cargo extends Model {

	protected $table = 'cargo';
	protected $primaryKey = 'id_cargo';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function tipo_usuario() {
    	return $this->belongsTo('App\tipo_usuario','id_tipo_usuario');
    }
}
