<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class chave_pesquisa_pesquisa extends Model {
	
	protected $table = 'chave_pesquisa_pesquisa';
	protected $primaryKey = 'id_chave_pesquisa_pesquisa';
    public $timestamps = false;
    protected $guarded  = array();
	
}