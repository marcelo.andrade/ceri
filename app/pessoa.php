<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
//use cidade;
use app\estado;

class pessoa extends Model {
	
	protected $table = 'pessoa';
	protected $primaryKey = 'id_pessoa';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function enderecos() {
		return $this->belongsToMany('App\endereco','pessoa_endereco','id_pessoa','id_endereco');
	}
	public function telefones() {
		return $this->belongsToMany('App\telefone','pessoa_telefone','id_pessoa','id_telefone');
	}
	public function nacionalidade() {
		return $this->belongsTo('App\nacionalidade','id_nacionalidade');
	}
	public function estado_civil() {
		return $this->belongsTo('App\estado_civil','id_estado_civil');
	}
	public function tipo_pessoa() {
		return $this->belongsTo('App\tipo_pessoa','id_tipo_pessoa');
	}
	public function usuario() {
		return $this->hasOne('App\usuario','id_pessoa');
	}
	public function serventia() {
		return $this->hasOne('App\serventia','id_pessoa');
	}
	public function vara() {
		return $this->hasOne('App\vara','id_pessoa');
	}
	public function unidade_gestora() {
		return $this->hasOne('App\unidade_gestora','id_pessoa');
	}
	public function unidade_judiciaria_divisao() {
		return $this->hasOne('App\unidade_judiciaria_divisao','id_pessoa');
	}

	public function pessoa_endereco() {
		return $this->hasMany('App\pessoa_endereco','id_pessoa');
	}
	public function pessoa_telefone() {
		return $this->hasMany('App\pessoa_telefone','id_pessoa');
	}

	public function usuario_pessoa() {
		return $this->hasMany('App\usuario_pessoa','id_pessoa');
	}
	public function pessoa_modulo() {
		return $this->hasMany('App\pessoa_modulo','id_pessoa');
	}
    public function banco_pessoa() {
        return $this->hasOne('App\banco_pessoa','id_pessoa')->where('in_registro_ativo','=','S');
    }


	public function trasCidade($idEstado,$idCidadeUsuario){
      $cidades = \App\cidade::where('id_estado',$idEstado)->get();

        $listCidade = '';
        if(!empty($cidades)) {
            foreach ($cidades as $cidade) {
                $selecionado = ($cidade->id_cidade == $idCidadeUsuario) ? 'selected' : '';
                $listCidade .= "<option value='".$cidade->id_cidade."' $selecionado>$cidade->no_cidade</option>";
            }
        }
        echo $listCidade;
    }
	
	// Atributos
	public function getNuCpfCnpjAttribute($nu_cpf_cnpj) {
		if (strlen($nu_cpf_cnpj)==11) {
			return preg_replace('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', '${1}.${2}.${3}-${4}', $nu_cpf_cnpj);
        } elseif (strlen($nu_cpf_cnpj)==14) {
			return preg_replace('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', '${1}.${2}.${3}/${4}-${5}', $nu_cpf_cnpj);
        }
    }
    /**
     * Método que reotrna o usuário através da pessoa ativa na sessão
     * @return mixed
     */
    public function getUsuarioRelacionado() {
        $usuario = \App\usuario_pessoa::join('usuario','usuario.id_usuario','=','usuario_pessoa.id_usuario')
                                        ->where('usuario_pessoa.id_pessoa', $this->id_pessoa) //Pessoa selecionada no Combo (Acessando como:)
                                        ->where('usuario.in_registro_ativo', 'S')
                                        ->where('usuario.in_usuario_master', 'S')
                                        ->where(function($query){
                                            $query->whereNull('usuario.id_tipo_usuario')
                                                ->orWhere(function($query) {
                                                    $query->whereNotIn('usuario.id_tipo_usuario', [0, 1, 13]);
                                                });
                                        })
                                        ->orderBy('usuario_pessoa.dt_cadastro','asc')
                                        ->first();
        return $usuario;
    }
}
