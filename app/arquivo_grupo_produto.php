<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class arquivo_grupo_produto extends Model {
	
	protected $table = 'arquivo_grupo_produto';
	protected $primaryKey = 'id_arquivo_grupo_produto';
    public $timestamps = false;
    protected $guarded  = array();

    public function grupo_produto() {
    	return $this->belongsTo('App\grupo_produto','id_grupo_produto');
    }
    public function tipo_arquivo_grupo_produto() {
    	return $this->belongsTo('App\tipo_arquivo_grupo_produto','id_tipo_arquivo_grupo_produto');
    }
    public function usuario_certificado() {
        return $this->belongsTo('App\usuario_certificado','id_usuario_certificado');
    }

    public function usuario_cad() {
        return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
    }

    public function arquivo_grupo_produto_composicao() {
        return $this->hasMany('App\arquivo_grupo_produto_composicao','id_arquivo_grupo_produto');
    }
}
