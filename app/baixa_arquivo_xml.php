<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class baixa_arquivo_xml extends Model {

    CONST ID_ALIENACAO_ARQUIVO_FASE_XML_TIPO  = 2;
    CONST ID_ALIENACAO_ARQUIVO_STATUS_CADASTRADO = 9;
    CONST ID_ALIENACAO_ARQUIVO_STATUS_PROCESSADO_COM_SUCESSO = 10;
    CONST ID_ALIENACAO_ARQUIVO_STATUS_NAO_PROCESSADO = 14;

	protected   $table          = 'baixa_arquivo_xml';
	protected   $primaryKey     = 'id_baixa_arquivo_xml';
    public      $timestamps     = false;
    protected   $guarded        = array();

    public function getDtCadastroAttribute($value)
    {
        return formatar_data($value);
    }

    public function alienacao_arquivo_xml() {
        return $this->belongsTo('App\alienacao_arquivo_xml','id_alienacao_arquivo_xml');
    }

    public function alienacao_arquivo_xml_registro_status() {
        return $this->belongsTo('App\alienacao_arquivo_xml_registro_status','id_alienacao_arquivo_xml_registro_status');
    }
}
