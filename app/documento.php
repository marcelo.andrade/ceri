<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Library\CeriFuncoes;

class documento extends Model {
	
	protected $table = 'documento';
	protected $primaryKey = 'id_documento';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function classificacao_documento() {
		return $this->belongsTo('App\classificacao_documento','id_classificacao_documento');
	}
	public function tipo_documento() {
		return $this->belongsTo('App\tipo_documento','id_tipo_documento');
	}
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
    public function usuario_pessoa(){
        return $this->hasMany('App\usuario_pessoa','id_usuario', 'id_usuario_cad');
    }
	public function documento_anterior() {
		return $this->belongsTo('App\documento','id_documento_anterior','id_documento');
	}
	public function documento_enviado() {
		return $this->hasOne('App\documento_enviado','id_documento');
	}
	public function documento_corpo() {
		return $this->hasOne('App\documento_corpo','id_documento');
	}
	public function situacao_documento() {
		return $this->belongsTo('App\situacao_documento','id_situacao_documento');
	}
	public function tipo_prioridade() {
		return $this->belongsTo('App\tipo_prioridade','id_tipo_prioridade');
	}
	public function usuario_cadastrado() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
	public function observacoes() {
		return $this->hasMany('App\observacao_usuario_documento','id_documento')->orderBy('dt_cadastro','desc');
	}

	public function bem_imoveis() {
		return $this->belongsToMany('App\bem_imovel','documento_bem_imovel','id_documento','id_bem_imovel');
	}

	public function documento_cidade() {
		return $this->hasOne('App\cidade','id_cidade', 'id_cidade_dest');
	}

	public function documento_serventia() {
		return $this->hasOne('App\serventia','id_serventia', 'id_serventia_dest');
	}

	public function documento_comarca() {
		return $this->hasOne('App\comarca','id_comarca', 'id_comarca_dest');
	}

	public function documento_vara() {
		return $this->hasOne('App\vara','id_vara', 'id_vara_dest');
	}

	public function documento_unidade_gestora() {
		return $this->hasOne('App\unidade_gestora','id_unidade_gestora', 'id_unidade_gestora_dest');
	}
	public function documento_unidade_judiciaria_divisao() {
		return $this->hasOne('App\unidade_judiciaria_divisao','id_unidade_judiciaria_divisao', 'id_unidade_judiciaria_divisao_dest');
	}

	public function documento_tipo_usuario() {
		return $this->hasOne('App\tipo_usuario','id_tipo_usuario', 'id_tipo_usuario_dest');
	}

	public function usuario_serventia() {
		return $this->belongsTo('App\usuario_serventia','id_usuario', 'id_usuario');
	}

	public function usuario_vara() {
		return $this->belongsTo('App\usuario_vara','id_usuario', 'id_usuario');
	}

	public function documento_prenotacao() {
		return $this->hasOne('App\documento_prenotacao','id_documento');
	}
	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','documento_arquivo_grupo','id_documento','id_arquivo_grupo_produto');
	}

	public function listar_cidade_serventia(serventia $serventia, $id_serventia)
	{
		$serventias = $serventia->join('pessoa','pessoa.id_pessoa','=','serventia.id_pessoa')
								->join('pessoa_endereco','pessoa_endereco.id_pessoa','=','pessoa.id_pessoa')
								->join('endereco','endereco.id_endereco','=','pessoa_endereco.id_endereco')
								->where('serventia.id_serventia', $id_serventia)
								->first();

		return $serventias;
	}
}