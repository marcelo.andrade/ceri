<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_pessoa extends Model {
	
	protected $table = 'pedido_pessoa';
	protected $primaryKey = 'id_pedido_pessoa';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}

}
