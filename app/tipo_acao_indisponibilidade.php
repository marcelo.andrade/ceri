<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_acao_indisponibilidade extends Model {
	
	protected $table = 'tipo_acao_indisponibilidade';
	protected $primaryKey = 'id_tipo_acao_indisponibilidade';
    public $timestamps = false;
    protected $guarded  = array();
	
}