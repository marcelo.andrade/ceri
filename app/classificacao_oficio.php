<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class classificacao_oficio extends Model {
	
	protected $table = 'classificacao_oficio';
	protected $primaryKey = 'id_classificacao_oficio';
    public $timestamps = false;
    protected $guarded  = array();
}
