<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class perfil_acesso extends Model {
	
	protected $table = 'perfil_acesso';
	protected $primaryKey = 'id_perfil_acesso';
    public $timestamps = false;
    protected $guarded  = array();
	
}