<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class andamento_alienacao_excluido extends Model {
	
	protected $table = 'andamento_alienacao_excluido';
	protected $primaryKey = 'id_andamento_alienacao_excluido';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function fase_grupo_produto() {
		return $this->belongsTo('App\fase_grupo_produto','id_fase_grupo_produto');
	}
	public function etapa_fase() {
		return $this->belongsTo('App\etapa_fase','id_etapa_fase');
	}
	public function acao_etapa() {
		return $this->belongsTo('App\acao_etapa','id_acao_etapa');
	}
	public function resultado_acao() {
		return $this->belongsTo('App\resultado_acao','id_resultado_acao');
	}
	public function arquivos_grupo() {
	    return $this->belongsToMany('App\arquivo_grupo_produto','andamento_arquivo_grupo_excluido', 'id_andamento_alienacao_excluido', 'id_arquivo_grupo_produto');
	}
    public function alienacao_pedido() {
        return $this->belongsTo('App\alienacao_pedido','id_alienacao_pedido');
    }
    public function endereco_alienacao() {
        return $this->hasMany('App\endereco_alienacao','id_andamento_alienacao');
    }
}
