<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_tipo_custa extends Model {
	
	protected $table        = 'penhora_tipo_custa';
	protected $primaryKey   = 'id_tipo_custa';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function tipo_custa_penhora() {
        return $this->hasOne('App\penhora','id_tipo_custa');
    }
}
