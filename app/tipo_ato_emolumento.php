<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_ato_emolumento extends Model {
	
	protected $table = 'tipo_ato_emolumento';
	protected $primaryKey = 'id_tipo_ato_emolumento';
    public $timestamps = false;
    protected $guarded  = array();
	
}