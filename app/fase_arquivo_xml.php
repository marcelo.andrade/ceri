<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class fase_arquivo_xml extends Model {
	
	protected $table = 'fase_arquivo_xml';
	protected $primaryKey = 'id_fase_arquivo_xml';
    public $timestamps = false;
    protected $guarded  = array();
	
}