<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_prenotacao extends Model {
	
	protected $table = 'penhora_prenotacao';
	protected $primaryKey = 'id_penhora_prenotacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}
}