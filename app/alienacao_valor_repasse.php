<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_valor_repasse extends Model {
	
	protected $table = 'alienacao_valor_repasse';
	protected $primaryKey = 'id_alienacao_valor_repasse';
    public $timestamps = false;
    protected $guarded  = array();

    public function aliencao_valor() {
        return $this->belongsTo('App\alienacao_valor','id_alienacao_valor');
    }

}
