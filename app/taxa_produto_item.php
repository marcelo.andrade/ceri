<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class taxa_produto_item extends Model {
	
	protected $table = 'taxa_produto_item';
	protected $primaryKey = 'id_taxa_produto_item';
    public $timestamps = false;
    protected $guarded  = array();
	
}