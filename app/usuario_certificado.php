<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_certificado extends Model {
	
	protected $table = 'usuario_certificado';
	protected $primaryKey = 'id_usuario_certificado';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	public function pedidos_resultados() {
		return $this->hasMany('App\pedido_resultado','id_usuario_certificado');
	}
	public function penhoras_averbacoes() {
		return $this->hasMany('App\penhora_bem_imovel','id_usuario_certificado');
	}
}
