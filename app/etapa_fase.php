<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class etapa_fase extends Model {
	
	protected $table = 'etapa_fase';
	protected $primaryKey = 'id_etapa_fase';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function proxima_etapa() {
		return $this->belongsTo('App\etapa_fase','id_etapa_fase_proxima','id_etapa_fase');
	}
	public function fase_grupo_produto() {
		return $this->belongsTo('App\fase_grupo_produto','id_fase_grupo_produto');
	}
	public function acao_etapa() {
		return $this->hasMany('App\acao_etapa','id_etapa_fase');
	}
	
}
