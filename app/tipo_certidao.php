<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_certidao extends Model {
	
	protected $table = 'tipo_certidao';
	protected $primaryKey = 'id_tipo_certidao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function certidao() {
		return $this->hasOne('App\certidao','id_tipo_certidao');
	}
}
