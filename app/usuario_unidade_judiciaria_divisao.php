<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_unidade_judiciaria_divisao extends Model {
	
	protected $table = 'usuario_unidade_judiciaria_divisao';
	protected $primaryKey = 'id_usuario_unidade_judiciaria_divisao';
	public $timestamps = false;
	protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	public function unidade_judiciaria_divisao() {
		return $this->belongsTo('App\unidade_judiciaria_divisao','id_unidade_judiciaria_divisao');
	}
	
}
