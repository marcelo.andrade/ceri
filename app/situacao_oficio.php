<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class situacao_oficio extends Model {
	
	protected $table = 'situacao_oficio';
	protected $primaryKey = 'id_situacao_oficio';
    public $timestamps = false;
    protected $guarded  = array();
	
}