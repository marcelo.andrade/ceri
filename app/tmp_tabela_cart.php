<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tmp_tabela_cart extends Model {
	
	protected $table = 'tmp_tabela_cart';
	protected $primaryKey = 'codigo_id';
    public $timestamps = false;
    protected $guarded  = array();
	
}