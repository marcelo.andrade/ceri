<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class unidade_gestora extends Model {
	
	protected $table = 'unidade_gestora';
	protected $primaryKey = 'id_unidade_gestora';
    public $timestamps = false;
    protected $guarded  = array();

	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}

}