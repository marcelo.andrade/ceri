<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class selo_lote_pedido_aplicacao extends Model {

	
	protected $table        = 'selo_lote_pedido_aplicacao';
	protected $primaryKey   = 'id_selo_lote_pedido_aplicacao';
    public    $timestamps   = false;
    protected $guarded      = array();
	
}
