<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_resultado extends Model {
	
	protected $table = 'pedido_resultado';
	protected $primaryKey = 'id_pedido_resultado';
    public $timestamps = false;
    protected $guarded  = array();

	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad');
	}
	public function serventia_certificado() {
		return $this->belongsTo('App\serventia_certificado','id_serventia_certificado');
	}
	public function matriculas() {
		return $this->hasMany('App\pedido_resultado_matricula','id_pedido_resultado');
	}
}