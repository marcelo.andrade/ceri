<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_custa extends Model {
	
	protected $table = 'penhora_custa';
	protected $primaryKey = 'id_penhora_custa';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}
}