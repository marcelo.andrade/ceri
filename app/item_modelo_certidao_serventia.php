<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class item_modelo_certidao_serventia extends Model {
	
	protected $table = 'item_modelo_certidao_serventia';
	protected $primaryKey = 'id_item_modelo_certidao_servent';
    public $timestamps = false;
    protected $guarded  = array();
	
}