<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_arquivo_xml extends Model {

    CONST ID_ALIENACAO_ARQUIVO_XML_TIPO       = 1;
    CONST ID_ALIENACAO_ARQUIVO_FASE_XML_TIPO  = 2;
    CONST ID_ALIENACAO_ARQUIVO_BAIXA_XML_TIPO = 3;

	protected   $table          = 'alienacao_arquivo_xml';
	protected   $primaryKey     = 'id_alienacao_arquivo_xml';
    public      $timestamps     = false;
    protected   $guarded        = array();

    public function getDtArquivoAttribute($value)
    {
        return formatar_data($value);
    }

    public function usuario_certificado() {
        return $this->belongsTo('App\usuario_certificado','id_usuario_certificado');
    }
}
