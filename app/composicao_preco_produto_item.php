<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class composicao_preco_produto_item extends Model {
	
	protected $table = 'composicao_preco_produto_item';
	protected $primaryKey = 'id_composicao_preco_produto_ite';
    public $timestamps = false;
    protected $guarded  = array();
}
