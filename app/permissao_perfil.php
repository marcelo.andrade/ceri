<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class permissao_perfil extends Model
{
	protected 	$table 		= 'permissao_perfil';
	protected 	$primaryKey = 'id_permisao_perfil';
    public 		$timestamps = false;
    protected 	$guarded  	= array();
}
