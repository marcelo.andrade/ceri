<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pais extends Model {
	
	protected $table = 'pais';
	protected $primaryKey = 'id_pais';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function estado() {
		return $this->belongsTo('App\estado','id_estado');
	}
	public function estados() {
		return $this->hasMany('App\estado','id_pais');
	}
		
}
