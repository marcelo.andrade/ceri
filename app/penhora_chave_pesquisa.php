<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_chave_pesquisa extends Model {
	
	protected $table = 'penhora_chave_pesquisa';
	protected $primaryKey = 'id_penhora_chave_pesquisa';
    public $timestamps = false;
    protected $guarded  = array();
	
}