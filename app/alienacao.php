<?php
namespace App;

use DB;
use Session;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class alienacao extends Model {

	protected $table = 'alienacao';
	protected $primaryKey = 'id_alienacao';
    public $timestamps = false;
    protected $guarded  = array();

    public function credor_alienacao() {
    	return $this->hasOne('App\alienacao_credor_alienacao','id_alienacao');
    }
    public function usuario_cad() {
    	return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
    }

    public function alienacao_devedor_devedor() {
    	return $this->hasMany('App\alienacao_devedor_devedor','id_alienacao');
    }

    public function alienacao_devedor() {
        return $this->belongsToMany('App\alienacao_devedor','alienacao_devedor_devedor','id_alienacao','id_alienacao_devedor')->where('in_registro_ativo','=','S');
    }
    // Retirar a função alienacao_arquivo após testes.
    public function alienacao_arquivo() {
    	return $this->hasMany('App\alienacao_arquivo_grupo_produto','id_alienacao');
    }

    public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','alienacao_arquivo_grupo_produto','id_alienacao','id_arquivo_grupo_produto');
	}

    public function alienacao_pedido() {
    	return $this->hasOne('App\alienacao_pedido','id_alienacao');
    }

	public function alienacao_projecao() {
		return $this->hasOne('App\alienacao_projecao','id_alienacao')->where('in_registro_ativo','=','S');
	}

    public function endereco_imovel() {
        return $this->belongsTo('App\endereco_alienacao','id_endereco_imovel','id_endereco_alienacao');
    }

    public function alienacao_valor() {
        return $this->hasMany('App\alienacao_valor','id_alienacao');
    }
    public function alienacao_valor_total() {
    	$pessoa_ativa = Session::get('pessoa_ativa');
        switch ($pessoa_ativa->pessoa->id_tipo_pessoa) {
        	case 8: case 2: case 10:
        		return $this->hasMany('App\alienacao_valor','id_alienacao')->where('in_servico','=','N')->sum('va_total');
        		break;
        	default:
        		return $this->hasMany('App\alienacao_valor','id_alienacao')->sum('va_total');
        		break;
        }
    }

    public function endereco_alienacao() {
        return $this->hasMany('App\endereco_alienacao','id_alienacao')->where("in_registro_ativo",'=','S');
    }

    public function endereco_alienacao_excluido() {
        return $this->hasMany('App\endereco_alienacao','id_alienacao')->where("in_registro_ativo",'=','N')->where("in_excluido",'=','S');
    }

    public function parcela_alienacao() {
    	return $this->hasMany('App\parcela_alienacao','id_alienacao')->orderBy('nu_parcela','asc');
    }
    public function soma_parcelas() {
    	return $this->hasMany('App\parcela_alienacao','id_alienacao')->sum('va_valor');
    }
    public function primeira_parcela() {
    	return $this->hasOne('App\parcela_alienacao','id_alienacao')->orderBy('dt_vencimento','asc');
    }
    public function ultima_parcela() {
    	return $this->hasOne('App\parcela_alienacao','id_alienacao')->orderBy('dt_vencimento','desc');
    }

	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}

	public function alienacao_pagamento_alienacao() {
		return $this->hasOne('App\alienacao_pagamento_alienacao','id_alienacao');
	}

    // Atributos referentes aos andamentos
    public function protocolo() {
        $protocolo = \App\andamento_alienacao::select('andamento_alienacao.de_texto_curto_acao','andamento_alienacao.dt_acao')
											 ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
											 ->join('acao_etapa',function($join) {
											 	 $join->on('acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
											 		  ->whereIn('acao_etapa.co_acao',array(5,32));
											 })
											 ->orderBy('andamento_alienacao.dt_cadastro','desc')
											 ->first();

		return $protocolo;
	}

    public function numero_carta_intimacao() {
        $numero_carta_intimacao = \App\andamento_alienacao::select('andamento_alienacao.de_texto_curto_acao')
            ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
            ->join('acao_etapa',function($join) {
                $join->on('acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
                    ->whereIn('acao_etapa.co_acao',array(6,33));
            })
            ->orderBy('andamento_alienacao.dt_cadastro','desc')
            ->first();

        return trim($numero_carta_intimacao->de_texto_curto_acao);
    }

    public function dt_encaminhamento() {
        $data_encaminhamento = \App\andamento_alienacao::select('andamento_alienacao.dt_cadastro')
                                ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
                                                                 ->join('acao_etapa',function($join) {
                                                                     $join->on('acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
                                                                          ->whereIn('acao_etapa.co_acao',[4,53]);
                                                                 })
                                ->orderBy('andamento_alienacao.dt_cadastro','desc')
                                ->first();

        return ( $data_encaminhamento ) ? $data_encaminhamento->dt_cadastro : null;
    }

    public function tipo_notificacao() {
        $notificacoes = \App\andamento_alienacao::select('andamento_alienacao.de_texto_curto_resultado','andamento_alienacao.dt_resultado')
												->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
												->join('resultado_acao',function($join) {
													$join->on('resultado_acao.id_resultado_acao','=','andamento_alienacao.id_resultado_acao')
											 			 ->whereIn('resultado_acao.co_resultado',array(10,13,16,19,22,25,53,56,59,62,65,68));
												});

        $editais = \App\andamento_alienacao::select('andamento_alienacao.de_texto_curto_acao','andamento_alienacao.dt_acao')
                                                 ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
                                                 ->join('resultado_acao',function($join) {
                                                     $join->on('resultado_acao.id_resultado_acao','=','andamento_alienacao.id_resultado_acao')
                                                         ->whereIn('resultado_acao.co_resultado',array(30,73));
                                                });


		$ars = \App\andamento_alienacao::select('andamento_alienacao.de_texto_curto_resultado','andamento_alienacao.dt_acao')
									   ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
									   ->join('resultado_acao',function($join) {
										   $join->on('resultado_acao.id_resultado_acao','=','andamento_alienacao.id_resultado_acao')
											    ->whereIn('resultado_acao.co_resultado',array(32,75,33,76));
									   });

		if ($notificacoes->count()>0) {
			$ultima_notificacao = $notificacoes->orderBy('andamento_alienacao.dt_cadastro','desc')->first();
			return array('tipo'=>'notificacao','notificacao'=>$ultima_notificacao);
		}
		if ($editais->count()>0) {
			$ultimo_edital = $editais->orderBy('andamento_alienacao.dt_cadastro','desc')->first();
			return array('tipo'=>'edital','edital'=>$ultimo_edital);
		}
		if ($ars->count()>0) {
			$ultimo_ar = $ars->orderBy('andamento_alienacao.dt_cadastro','desc')->first();
			return array('tipo'=>'ar','ar'=>$ultimo_ar);
		}
	}
	public function valor_itbi() {
        $valor = \App\andamento_alienacao::select('andamento_alienacao.va_valor_resultado','andamento_alienacao.dt_resultado')
										 ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
											 ->join('resultado_acao',function($join) {
											 	 $join->on('resultado_acao.id_resultado_acao','=','andamento_alienacao.id_resultado_acao')
											 		  ->where('resultado_acao.co_resultado','=',40);
											 })
										 ->orderBy('andamento_alienacao.dt_cadastro','desc')
										 ->first();

		return $valor;
	}
	public function inicio_decurso() {
        $valor = \App\andamento_alienacao::select('andamento_alienacao.dt_acao')
										 ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
											 ->join('resultado_acao',function($join) {
											 	 $join->on('resultado_acao.id_resultado_acao','=','andamento_alienacao.id_resultado_acao')
											 		  ->where('resultado_acao.co_resultado','=',37);
											 })
										 ->orderBy('andamento_alienacao.dt_cadastro','desc')
										 ->first();

		return $valor;
	}
    public function fim_decurso() {
        $alienacao = $this;
        if ($alienacao->inicio_decurso()) {
            $dt_acao = Carbon::parse($alienacao->inicio_decurso()->dt_acao)->addWeekdays(15);
            $dt_inicio = Carbon::parse($alienacao->inicio_decurso()->dt_acao);

            $feriados = \App\feriado::where(function($where) use ($dt_inicio,$dt_acao) {
                $where->where('dt_feriado', '>=', $dt_inicio)
                    ->where('dt_feriado', '<=', $dt_acao);
            })
                ->where(function($where) use ($alienacao) {
                    if ($alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]) {
                        $where->where(function($where) use ($alienacao) {
                            $where->whereNull('id_cidade')
                                ->whereNull('id_estado');
                        })
                            ->orWhere(function($where) use ($alienacao) {
                                $where->where('id_cidade',$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->id_cidade)
                                    ->where('id_estado',$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->id_estado);
                            })
                            ->orWhere(function($where) use ($alienacao) {
                                $where->whereNull('id_cidade')
                                    ->where('id_estado',$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->id_estado);
                            });
                    }
                })
                ->where('in_registro_ativo','S')
                ->count();

            return $dt_acao->addWeekdays($feriados);
        } else {
            return false;
        }
    }
    public function tempo_decurso() {
        $dt_hoje = Carbon::now()->endOfDay();
        $dt_acao = $this->fim_decurso();
        if ($dt_acao) {
            if ($dt_hoje<=$dt_acao) {
                return array('dt_acao'=>$dt_acao,'dias'=>$dt_acao->diffInDaysFiltered(function(Carbon $date) { return $date->isWeekday(); }, $dt_hoje));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	public function valor_nao_pago() {
    	return $this->hasMany('App\alienacao_valor','id_alienacao')->where('id_produto_item', '<>', 28)->where('id_alienacao_pagamento',NULL)->sum('va_total');
    }
    public function prenotacao() {
        $protocolo = \App\andamento_alienacao::select('andamento_alienacao.de_texto_curto_acao','andamento_alienacao.dt_acao')
											 ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
											 ->join('acao_etapa',function($join) {
											 	 $join->on('acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
											 		  ->whereIn('acao_etapa.co_acao',array(5,32));
											 })
											 ->orderBy('andamento_alienacao.dt_cadastro','asc')
											 ->first();

		return $protocolo;
	}

    public function valor_pago($id_alienacao_pagamento) {
        $valor_pago = \App\alienacao_pagamento_alienacao::join('alienacao_valor as av','av.id_alienacao_valor','=','alienacao_pagamento_alienacao.id_alienacao_valor')
            ->where('alienacao_pagamento_alienacao.id_alienacao_pagamento','=',$id_alienacao_pagamento)
            ->where('av.id_alienacao','=',$this->id_alienacao)
            ->sum('av.va_total');

        return $valor_pago;
    }


	/*
	 * Apos a CEF cancelar a notificao, mesmo o cartorio nao tendo nada a receber,
	 * fica o "Andamento" " Aguardando Pagamento",
	 * causando a impressão de que o Cartório teria pagamentos a receber,
	 * então alterar para "Processo Finalizado (Cancelado)
	 */
    public function checar_cancelamento_pagamento() {
        $registro_cancelado = \App\alienacao_valor::select('alienacao_valor.id_alienacao_valor')
                                                   ->where('id_alienacao',$this->id_alienacao)
                                                   ->where('in_servico', 'N')
                                                   ->where('in_aprovacao_orcamento', 'S')/*orcamento foi aprovado*/
                                                   ->where('in_registro_aberto', 'S')
                                                   ->first();
        return $registro_cancelado;
    }

	/*
	public function valores_totais() {
		if (count($this->alienacao_pedido->pedido->produto_itens)>0) {
			$produtos = array();
			foreach ($this->alienacao_pedido->pedido->produto_itens as $produto_item) {
				$produtos[] = $produto_item->id_produto_item;
			}
			$valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto ('".implode(';',$produtos)."', ';', 'L')  AS (item text, id2 text);"));

			return $valores;
		} else {
			return NULL;
		}
	}*/

	public function vencimentos() {
		$datas = array();

        if ($this->dt_encaminhamento()) {
            $datas['dt_vencimento_periodo_1'] = \Carbon\Carbon::parse($this->dt_encaminhamento());

            if ($this->alienacao_projecao) {
	            $datas['dt_vencimento_periodo_2'] = $datas['dt_vencimento_periodo_1']->copy()->addDays($this->alienacao_projecao->nu_dia_periodo_02);
	            $datas['dt_vencimento_periodo_3'] = $datas['dt_vencimento_periodo_2']->copy()->addDays($this->alienacao_projecao->nu_dia_periodo_03);
	            $datas['dt_vencimento_periodo_4'] = $datas['dt_vencimento_periodo_3']->copy()->addDays($this->alienacao_projecao->nu_dia_periodo_04);
	        } else {
	        	$datas['dt_vencimento_periodo_2'] = $datas['dt_vencimento_periodo_1']->copy()->addDays(15);
	            $datas['dt_vencimento_periodo_3'] = $datas['dt_vencimento_periodo_2']->copy()->addDays(15);
	            $datas['dt_vencimento_periodo_4'] = $datas['dt_vencimento_periodo_3']->copy()->addDays(15);
	        }
        }

        return $datas;
    }

    public function total_alerta($regras,$tipo='alerta') {
		$pessoa_ativa = Session::get('pessoa_ativa');
		$alienacoes = $this->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                           ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                           ->join('alienacao_andamento_atual',function($join) {
                                $join->on('alienacao_andamento_atual.id_alienacao_pedido','=','alienacao_pedido.id_alienacao_pedido')
                                     ->where('alienacao_andamento_atual.in_registro_ativo','=','S');
                           });

        switch ($pessoa_ativa->pessoa->id_tipo_pessoa) {
            case 8:
            	$alienacoes = $alienacoes->where('pedido.id_pessoa_origem','=',$pessoa_ativa->id_pessoa);
            	break;
            case 2:
            case 10:
            	$alienacoes = $alienacoes->join('pedido_pessoa','pedido_pessoa.id_pedido','=','pedido.id_pedido')
                						 ->where('pedido_pessoa.id_pessoa','=',$pessoa_ativa->id_pessoa);
            	break;
        }

        switch ($tipo) {
         	case 'unica':
         		$regras_array[] = $regras;
         		break;
         	case 'grupo':
				$regras_array = $regras;
				break;
			case 'total':
				foreach ($regras as $grupo) {
					foreach ($grupo as $key => $regra) {
						$regras_array[] = $regra;
					}
				}
				break;
        }

        $alienacoes = $this->aplicar_regras($alienacoes,$regras_array);

        return intval($alienacoes->count());
	}

	public function aplicar_regras($alienacoes,$regras_array) {
		if (is_object($alienacoes) and is_array($regras_array)) {
            $alienacoes = $alienacoes->join('andamento_alienacao','andamento_alienacao.id_andamento_alienacao','=','alienacao_andamento_atual.id_andamento_alienacao');
			$alienacoes = $alienacoes->where(function($where) use ($regras_array) {
	        	foreach ($regras_array as $regra) {
	        		$where->orWhere(function($subwhere) use ($regra) {
	        			if (is_numeric($regra['id_situacao_pedido_grupo_produto']) and $regra['id_situacao_pedido_grupo_produto']>0) {
				            $subwhere->where('pedido.id_situacao_pedido_grupo_produto',$regra['id_situacao_pedido_grupo_produto']);
				        } elseif (is_array($regra['id_situacao_pedido_grupo_produto'])) {
				            $subwhere->whereIn('pedido.id_situacao_pedido_grupo_produto',$regra['id_situacao_pedido_grupo_produto']);
				        }
				        if (is_numeric($regra['id_fase_grupo_produto']) and $regra['id_fase_grupo_produto']>0) {
				            $subwhere->where('alienacao_andamento_atual.id_fase_grupo_produto',$regra['id_fase_grupo_produto']);
				        } elseif (is_array($regra['id_fase_grupo_produto'])) {
				            $subwhere->whereIn('alienacao_andamento_atual.id_fase_grupo_produto',$regra['id_fase_grupo_produto']);
				        }
				        if (is_numeric($regra['id_etapa_fase']) and $regra['id_etapa_fase']>0) {
				            $subwhere->where('alienacao_andamento_atual.id_etapa_fase',$regra['id_etapa_fase']);
				        } elseif (is_array($regra['id_etapa_fase'])) {
				            $subwhere->whereIn('alienacao_andamento_atual.id_etapa_fase',$regra['id_etapa_fase']);
				        }
				        if (is_numeric($regra['id_acao_etapa']) and $regra['id_acao_etapa']>0) {
				            $subwhere->where('alienacao_andamento_atual.id_acao_etapa',$regra['id_acao_etapa']);
				        } elseif (is_array($regra['id_acao_etapa'])) {
				            $subwhere->whereIn('alienacao_andamento_atual.id_acao_etapa',$regra['id_acao_etapa']);
				        }
				        if ($regra['id_resultado_acao']<0) {
				            $subwhere->whereNull('alienacao_andamento_atual.id_resultado_acao');
				        } elseif (is_numeric($regra['id_resultado_acao']) and $regra['id_resultado_acao']>0) {
				            $subwhere->where('alienacao_andamento_atual.id_resultado_acao',$regra['id_resultado_acao']);
				        } elseif (is_array($regra['id_resultado_acao'])) {
				            if (in_array(-1,$regra['id_resultado_acao'])) {
				                $idx = array_search(-1, $regra['id_resultado_acao']);
				                unset($regra['id_resultado_acao'][$idx]);
				                $subwhere->where(function($subwhere2) use ($regra) {
				                    $subwhere2->whereIn('alienacao_andamento_atual.id_resultado_acao',$regra['id_resultado_acao'])
				                          	  ->orWhereNull('alienacao_andamento_atual.id_resultado_acao');
				                });
				            } else {
				                $subwhere->whereIn('alienacao_andamento_atual.id_resultado_acao',$regra['id_resultado_acao']);
				            }
				        }
                        if (isset($regra['dt_acao'])) {
                            $subwhere->where('andamento_alienacao.dt_acao',$regra['dt_acao']['tipo'],$regra['dt_acao']['data']);
                        }
	        		});
	        	}
	        });
		}
		return $alienacoes;
	}

	public function regras_filtros($grupo=false) {
		// Inicial
        $regras['inicial']['cadastradas'] = array('id_situacao_pedido_grupo_produto'=>54,
                                                 'id_fase_grupo_produto'=>1,
                                                 'id_etapa_fase'=>1,
                                                 'id_acao_etapa'=>1,
                                                 'id_resultado_acao'=>1);
        $regras['inicial']['protocolar'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                 'id_fase_grupo_produto'=>[2,10],
                                                 'id_etapa_fase'=>[3,35],
                                                 'id_acao_etapa'=>[4,53],
                                                 'id_resultado_acao'=>[4,91]);
        $regras['inicial']['analise'] = array('id_situacao_pedido_grupo_produto'=>55,
                                              'id_fase_grupo_produto'=>[3,4],
                                              'id_etapa_fase'=>[4,23],
                                              'id_acao_etapa'=>[5,32],
                                              'id_resultado_acao'=>[6,49]);
        $regras['inicial']['orcamento'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                'id_fase_grupo_produto'=>3,
                                                'id_etapa_fase'=>[30,38],
                                                'id_acao_etapa'=>[46,55],
                                                'id_resultado_acao'=>[80,95]);
        $regras['inicial']['aprovar-orcamento'] = array('id_situacao_pedido_grupo_produto'=>[68,55],
                                                        'id_fase_grupo_produto'=>3,
                                                        'id_etapa_fase'=>37,
                                                        'id_acao_etapa'=>2,
                                                        'id_resultado_acao'=>2);
        $regras['inicial']['devolvidas'] = array('id_situacao_pedido_grupo_produto'=>64,
                                                 'id_fase_grupo_produto'=>3,
                                                 'id_etapa_fase'=>30,
                                                 'id_acao_etapa'=>46,
                                                 'id_resultado_acao'=>81);

		// Notificação ao devedor
		$regras['notificacoes']['emitir-carta-intimacao'] = array('id_situacao_pedido_grupo_produto'=>55,
                                    							  'id_fase_grupo_produto'=>3,
                                    							  'id_etapa_fase'=>38,
                                    							  'id_acao_etapa'=>[55,84],
                                                                  'id_resultado_acao'=>[94,138]);
		$regras['notificacoes']['tipo-encaminhamento'] = array('id_situacao_pedido_grupo_produto'=>55,
                                    						   'id_fase_grupo_produto'=>3,
                                    						   'id_etapa_fase'=>[5,6],
                                    						   'id_acao_etapa'=>[6,7],
                                                               'id_resultado_acao'=>[7,-1]);
		$regras['notificacoes']['registrar-resultado'] = array('id_situacao_pedido_grupo_produto'=>55,
                                    						   'id_fase_grupo_produto'=>3,
                                    						   'id_etapa_fase'=>[6,38,7],
                                    						   'id_acao_etapa'=>[7,82,86,87],
                                                               'id_resultado_acao'=>[8,9,146,142,151]);
        $regras['notificacoes']['orcamento-novos-enderecos'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                                     'id_fase_grupo_produto'=>3,
                                                                     'id_etapa_fase'=>7,
                                                                     'id_acao_etapa'=>[9,86],
                                                                     'id_resultado_acao'=>[134,136]);
        $regras['notificacoes']['aprovar-orcamento-enderecos'] = array('id_situacao_pedido_grupo_produto'=>68,
                                                                       'id_fase_grupo_produto'=>2,
                                                                       'id_etapa_fase'=>39,
                                                                       'id_acao_etapa'=>83,
                                                                       'id_resultado_acao'=>137);
        $regras['notificacoes']['solicitar-novos-enderecos'] = array('id_situacao_pedido_grupo_produto'=>56,
                                                                     'id_fase_grupo_produto'=>3,
                                                                     'id_etapa_fase'=>7,
                                                                     'id_acao_etapa'=>9,
                                                                     'id_resultado_acao'=>135);
        $regras['notificacoes']['cancelamento-outros'] = array('id_situacao_pedido_grupo_produto'=>56,
                                                               'id_fase_grupo_produto'=>3,
                                                               'id_etapa_fase'=>7,
                                                               'id_acao_etapa'=>9,
                                                               'id_resultado_acao'=>126);
		$regras['notificacoes']['solicitar-segunda'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                             'id_fase_grupo_produto'=>3,
                                                             'id_etapa_fase'=>10,
                                                             'id_acao_etapa'=>19,
                                                             'id_resultado_acao'=>33);
        $regras['notificacoes']['autorizar-segunda'] = array('id_situacao_pedido_grupo_produto'=>56,
                                                             'id_fase_grupo_produto'=>4,
                                                             'id_etapa_fase'=>11,
                                                             'id_acao_etapa'=>20,
                                                             'id_resultado_acao'=>-1);
        
		// Fase de AR
        $regras['ar']['aprovar-orcamento'] = array('id_situacao_pedido_grupo_produto'=>68,
                                                   'id_fase_grupo_produto'=>3,
                                                   'id_etapa_fase'=>7,
                                                   'id_acao_etapa'=>9,
                                                   'id_resultado_acao'=>11);
		$regras['ar']['registrar-data'] = array('id_situacao_pedido_grupo_produto'=>55,
                    							'id_fase_grupo_produto'=>3,
                    							'id_etapa_fase'=>[7,38],
                    							'id_acao_etapa'=>[87,89],
                                                'id_resultado_acao'=>[141,145]);
		$regras['ar']['registrar-data-retorno'] = array('id_situacao_pedido_grupo_produto'=>55,
                            							'id_fase_grupo_produto'=>3,
                            							'id_etapa_fase'=>10,
                            							'id_acao_etapa'=>18,
                                                        'id_resultado_acao'=>31);
        $regras['ar']['registrar-retorno'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                   'id_fase_grupo_produto'=>3,
                                                   'id_etapa_fase'=>10,
                                                   'id_acao_etapa'=>19,
                                                   'id_resultado_acao'=>-1);

		// Fase de edital
        $regras['edital']['emitir-intimacao'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                      'id_fase_grupo_produto'=>3,
                                                      'id_etapa_fase'=>[7,38],
                                                      'id_acao_etapa'=>[9,57,93],
                                                      'id_resultado_acao'=>[12,97,152]);
		$regras['edital']['inserir-orcamento'] = array('id_situacao_pedido_grupo_produto'=>55,
                            						   'id_fase_grupo_produto'=>3,
                            						   'id_etapa_fase'=>[9,38],
                            						   'id_acao_etapa'=>[80,82],
                                                       'id_resultado_acao'=>[130,133]);
        $regras['edital']['iniciar-aprovar-orcamento'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                               'id_fase_grupo_produto'=>2,
                                                               'id_etapa_fase'=>39,
                                                               'id_acao_etapa'=>56,
                                                               'id_resultado_acao'=>96);
        $regras['edital']['aprovar-orcamento'] = array('id_situacao_pedido_grupo_produto'=>[68,56],
                                                       'id_fase_grupo_produto'=>[2,3],
                                                       'id_etapa_fase'=>[9,38,39],
                                                       'id_acao_etapa'=>[56,81,82],
                                                       'id_resultado_acao'=>[96,131,132]);
		$regras['edital']['registrar-envio'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                     'id_fase_grupo_produto'=>3,
                                                     'id_etapa_fase'=>[9,38],
                                                     'id_acao_etapa'=>[15,82,94],
                                                     'id_resultado_acao'=>[28,132,154]);
		$regras['edital']['registrar-data'] = array('id_situacao_pedido_grupo_produto'=>55,
                        							'id_fase_grupo_produto'=>3,
                        							'id_etapa_fase'=>9,
                        							'id_acao_etapa'=>16,
                                                    'id_resultado_acao'=>29);

		// Decurso de prazo
        $regras['decurso']['iniciar-prazo'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                    'id_fase_grupo_produto'=>3,
                                                    'id_etapa_fase'=>[7,9],
                                                    'id_acao_etapa'=>[9,17],
                                                    'id_resultado_acao'=>[10,30]);
		$regras['decurso']['solicitar-autorizacao'] = array('id_situacao_pedido_grupo_produto'=>55,
                                							'id_fase_grupo_produto'=>5,
                                							'id_etapa_fase'=>13,
                                							'id_acao_etapa'=>22,
                                                            'id_resultado_acao'=>37,
                                                            'dt_acao'=>[
                                                                'tipo'=>'>',
                                                                'data'=>Carbon::now()->endOfDay()->subWeekdays(15)
                                                            ]);
        $regras['decurso']['aguardar-manifesto'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                        'id_fase_grupo_produto'=>5,
                                                        'id_etapa_fase'=>13,
                                                        'id_acao_etapa'=>22,
                                                        'id_resultado_acao'=>37,
                                                        'dt_acao'=>[
                                                                'tipo'=>'<=',
                                                                'data'=>Carbon::now()->endOfDay()->subWeekdays(15)
                                                            ]);
        $regras['decurso']['autorizar-decurso'] = array('id_situacao_pedido_grupo_produto'=>69,
                                                         'id_fase_grupo_produto'=>5,
                                                         'id_etapa_fase'=>14,
                                                         'id_acao_etapa'=>23,
                                                         'id_resultado_acao'=>-1);
		/*$regras['decurso']['iniciar-aguardar-pagamento'] = array('id_situacao_pedido_grupo_produto'=>55,
                                    							 'id_fase_grupo_produto'=>5,
                                    							 'id_etapa_fase'=>14,
                                    							 'id_acao_etapa'=>23,
                                                                 'id_resultado_acao'=>38);*/
        $regras['decurso']['aguardando-pagamento'] = array('id_situacao_pedido_grupo_produto'=>65,
                                                           'id_fase_grupo_produto'=>5,
                                                           'id_etapa_fase'=>[33,14],
                                                           'id_acao_etapa'=>[51,23],
                                                           'id_resultado_acao'=>[88,38]);
		$regras['decurso']['emitir-certidao'] = array('id_situacao_pedido_grupo_produto'=>55,
                        							  'id_fase_grupo_produto'=>5,
                        							  'id_etapa_fase'=>33,
                        							  'id_acao_etapa'=>51,
                                                      'id_resultado_acao'=>88);
        
		// Legalização
		$regras['legalizacao']['solicitar-itbi'] = array('id_situacao_pedido_grupo_produto'=>56,
                            							 'id_fase_grupo_produto'=>5,
                            							 'id_etapa_fase'=>15,
                            							 'id_acao_etapa'=>24,
                                                         'id_resultado_acao'=>39);
		$regras['legalizacao']['atualizacao-cadastral'] = array('id_situacao_pedido_grupo_produto'=>55,
                                    							'id_fase_grupo_produto'=>11,
                                    							'id_etapa_fase'=>42,
                                    							'id_acao_etapa'=>63,
                                                                'id_resultado_acao'=>107);
		$regras['legalizacao']['solicitar-autorizacao'] = array('id_situacao_pedido_grupo_produto'=>55,
                                    							'id_fase_grupo_produto'=>11,
                                    							'id_etapa_fase'=>43,
                                    							'id_acao_etapa'=>64,
                                                                'id_resultado_acao'=>108);
		$regras['legalizacao']['autorizar-legalizacao'] = array('id_situacao_pedido_grupo_produto'=>56,
                                    							'id_fase_grupo_produto'=>11,
                                    							'id_etapa_fase'=>44,
                                    							'id_acao_etapa'=>66,
                                                                'id_resultado_acao'=>112);
		$regras['legalizacao']['aprovar-orcamento'] = array('id_situacao_pedido_grupo_produto'=>56,
                                							'id_fase_grupo_produto'=>11,
                                							'id_etapa_fase'=>45,
                                							'id_acao_etapa'=>67,
                                                            'id_resultado_acao'=>113);
		$regras['legalizacao']['emitir-certidao-inteiro-teor'] = array('id_situacao_pedido_grupo_produto'=>55,
                                            						   'id_fase_grupo_produto'=>11,
                                            						   'id_etapa_fase'=>47,
                                            						   'id_acao_etapa'=>69,
                                                                       'id_resultado_acao'=>116);
		$regras['legalizacao']['averbar-imovel'] = array('id_situacao_pedido_grupo_produto'=>55,
                            							 'id_fase_grupo_produto'=>11,
                            							 'id_etapa_fase'=>49,
                            							 'id_acao_etapa'=>71,
                                                         'id_resultado_acao'=>119);
		$regras['legalizacao']['exige-cnd'] = array('id_situacao_pedido_grupo_produto'=>55,
                        							'id_fase_grupo_produto'=>11,
                        							'id_etapa_fase'=>[48,43],
                        							'id_acao_etapa'=>[70,64],
                                                    'id_resultado_acao'=>[118,109]);
		$regras['legalizacao']['emitir-iptu'] = array('id_situacao_pedido_grupo_produto'=>55,
                        							  'id_fase_grupo_produto'=>11,
                        							  'id_etapa_fase'=>43,
                        							  'id_acao_etapa'=>65,
                                                      'id_resultado_acao'=>110);
		$regras['legalizacao']['emitir-dividas'] = array('id_situacao_pedido_grupo_produto'=>55,
                            							 'id_fase_grupo_produto'=>13,
                            							 'id_etapa_fase'=>51,
                            							 'id_acao_etapa'=>74,
                                                         'id_resultado_acao'=>122);
		$regras['legalizacao']['efetuar-pagamento'] = array('id_situacao_pedido_grupo_produto'=>65,
                                							'id_fase_grupo_produto'=>13,
                                							'id_etapa_fase'=>51,
                                							'id_acao_etapa'=>75,
                                                            'id_resultado_acao'=>123);
        $regras['legalizacao']['efetuar-pagamento-cnd'] = array('id_situacao_pedido_grupo_produto'=>56,
                                                                'id_fase_grupo_produto'=>13,
                                                                'id_etapa_fase'=>51,
                                                                'id_acao_etapa'=>75,
                                                                'id_resultado_acao'=>123);
		$regras['legalizacao']['emitir-cnd'] = array('id_situacao_pedido_grupo_produto'=>55,
                        							 'id_fase_grupo_produto'=>13,
                        							 'id_etapa_fase'=>52,
                        							 'id_acao_etapa'=>76,
                                                     'id_resultado_acao'=>124);
        $regras['legalizacao']['emitir-itbi'] = array('id_situacao_pedido_grupo_produto'=>55,
                                                      'id_fase_grupo_produto'=>[11,13],
                                                      'id_etapa_fase'=>[43,53],
                                                      'id_acao_etapa'=>[65,77],
                                                      'id_resultado_acao'=>[111,125]);
        $regras['legalizacao']['requerer-comprovante-itbi'] = array('id_situacao_pedido_grupo_produto'=>56,
                                                                    'id_fase_grupo_produto'=>6,
                                                                    'id_etapa_fase'=>16,
                                                                    'id_acao_etapa'=>25,
                                                                    'id_resultado_acao'=>40);

		// Consolidação
		$regras['consolidacao']['enviar-requerimento'] = array('id_situacao_pedido_grupo_produto'=>56,
                                    						   'id_fase_grupo_produto'=>6,
                                    						   'id_etapa_fase'=>18,
                                    						   'id_acao_etapa'=>27,
                                                               'id_resultado_acao'=>42);
		$regras['consolidacao']['aprovar-orcamento'] = array('id_situacao_pedido_grupo_produto'=>56,
                                							 'id_fase_grupo_produto'=>6,
                                							 'id_etapa_fase'=>19,
                                							 'id_acao_etapa'=>28,
                                                             'id_resultado_acao'=>44);
		$regras['consolidacao']['averbar'] = array('id_situacao_pedido_grupo_produto'=>55,
                        						   'id_fase_grupo_produto'=>6,
                        						   'id_etapa_fase'=>40,
                        						   'id_acao_etapa'=>60,
                                                   'id_resultado_acao'=>102);
		$regras['consolidacao']['emitir-certidao'] = array('id_situacao_pedido_grupo_produto'=>55,
                                						   'id_fase_grupo_produto'=>6,
                                						   'id_etapa_fase'=>54,
                                						   'id_acao_etapa'=>78,
                                                           'id_resultado_acao'=>104);
		$regras['consolidacao']['finalizar'] = array('id_situacao_pedido_grupo_produto'=>55,
                        							 'id_fase_grupo_produto'=>6,
                        							 'id_etapa_fase'=>41,
                        							 'id_acao_etapa'=>61,
                                                     'id_resultado_acao'=>105);

		// Finalização
		$regras['finalizacao']['aguardando-finalizacao'] = array('id_situacao_pedido_grupo_produto'=>[66,67],
			                        							 'id_fase_grupo_produto'=>8,
			                        							 'id_etapa_fase'=>22,
			                        							 'id_acao_etapa'=>31,
			                                                     'id_resultado_acao'=>-1);
		$regras['finalizacao']['canceladas'] = array('id_situacao_pedido_grupo_produto'=>[56,57,66,67],
                        							 'id_fase_grupo_produto'=>[8,9],
                        							 'id_etapa_fase'=>[22,32],
                        							 'id_acao_etapa'=>[31,48,79],
                                                     'id_resultado_acao'=>[10,79,84,128]);
		$regras['finalizacao']['consolidadas'] = array('id_situacao_pedido_grupo_produto'=>57,
	                        						   'id_fase_grupo_produto'=>8,
	                        						   'id_etapa_fase'=>22,
	                        						   'id_acao_etapa'=>[31,62],
	                                                   'id_resultado_acao'=>[77,106]);
		$regras['finalizacao']['devedor-nao-encontrado'] = array('id_situacao_pedido_grupo_produto'=>[57,66],
			                        							 'id_fase_grupo_produto'=>8,
			                        							 'id_etapa_fase'=>22,
			                        							 'id_acao_etapa'=>31,
			                                                     'id_resultado_acao'=>47);
		$regras['finalizacao']['itbi-nao-autorizado'] = array('id_situacao_pedido_grupo_produto'=>57,
			                        						  'id_fase_grupo_produto'=>8,
			                        						  'id_etapa_fase'=>22,
			                        						  'id_acao_etapa'=>31,
			                                                  'id_resultado_acao'=>48);
		$regras['finalizacao']['pagamento-efetuado'] = array('id_situacao_pedido_grupo_produto'=>57,
			                        						 'id_fase_grupo_produto'=>8,
			                        						 'id_etapa_fase'=>22,
			                        						 'id_acao_etapa'=>31,
			                                                 'id_resultado_acao'=>78);

		// Inicial
		$titulos['inicial']['grupo'] = 'Inicial';
		$titulos['inicial']['cadastradas'] = 'Cadastradas';
		$titulos['inicial']['protocolar'] = 'Protocolar';
		$titulos['inicial']['analise'] = 'Análise';
		$titulos['inicial']['orcamento'] = 'Orçamento';
		$titulos['inicial']['aprovar-orcamento'] = 'Aprovar orçamento';
		$titulos['inicial']['devolvidas'] = 'Devolvidas';

		// Notificação ao devedor
		$titulos['notificacoes']['grupo'] = 'Notificação ao devedor';
		$titulos['notificacoes']['emitir-carta-intimacao'] = 'Emitir carta de intimação';
		$titulos['notificacoes']['tipo-encaminhamento'] = 'Selecionar tipo de encaminhamento';
		$titulos['notificacoes']['registrar-resultado'] = 'Registrar resultado da notificação';
		$titulos['notificacoes']['orcamento-novos-enderecos'] = 'Informar orçamento de novo(s) endereço(s)';
		$titulos['notificacoes']['aprovar-orcamento-enderecos'] = 'Aprovar orçamento de novo(s) endereço(s)';
		$titulos['notificacoes']['solicitar-novos-enderecos'] = 'Solicitar novo(s) endereço(s)';
		$titulos['notificacoes']['cancelamento-outros'] = 'Solicitar cancelamento';
		$titulos['notificacoes']['solicitar-segunda'] = 'Não notificado - Solicitar 2ª tentativa';
		$titulos['notificacoes']['autorizar-segunda'] = 'Autorizar segunda tentativa';

		// Fase de AR
		$titulos['ar']['grupo'] = 'Fase de AR';
		$titulos['ar']['aprovar-orcamento'] = 'Aprovar orçamento do AR';
		$titulos['ar']['registrar-data'] = 'Registrar data do AR';
		$titulos['ar']['registrar-data-retorno'] = 'Registrar data do retorno do AR';
		$titulos['ar']['registrar-retorno'] = 'Registrar retorno do AR';

		// Fase de edital
		$titulos['edital']['grupo'] = 'Fase de Edital';
		$titulos['edital']['emitir-intimacao'] = 'Emitir edital de intimação';
		$titulos['edital']['inserir-orcamento'] = 'Inserir orçamento do edital';
		$titulos['edital']['iniciar-aprovar-orcamento'] = 'Iniciar aprovar orçamento de edital';
		$titulos['edital']['aprovar-orcamento'] = 'Aprovar orçamento de edital';
		$titulos['edital']['registrar-envio'] = 'Registrar data de envio do edital';
		$titulos['edital']['registrar-data'] = 'Registrar data da última publicação de edital';
		$titulos['edital']['atualizar-valores'] = 'Atualizar valores do edital';

		// Decurso de prazo
		$titulos['decurso']['grupo'] = 'Decurso de prazo';
		$titulos['decurso']['iniciar-prazo'] = 'Informar data de decurso de prazo';
        $titulos['decurso']['aguardar-manifesto'] = 'Solicitar autorização certidão de decurso';
        $titulos['decurso']['solicitar-autorizacao'] = 'Aguardando prazo legal';
		$titulos['decurso']['autorizar-decurso'] = 'Autorizar a emissão do decurso de prazo';
		/*$titulos['decurso']['iniciar-aguardar-pagamento'] = 'Iniciar aguardar pagamento da notificação';*/
		$titulos['decurso']['aguardando-pagamento'] = 'Aguardando pagamento da notificação';
		$titulos['decurso']['emitir-certidao'] = 'Emitir Certidão de decurso de prazo';

		// Legalização
		$titulos['legalizacao']['grupo'] = 'Legalização';
		$titulos['legalizacao']['solicitar-itbi'] = 'Solicitar Guia de ITBI';
		$titulos['legalizacao']['atualizacao-cadastral'] = 'Necessário atualização cadastral?';
		$titulos['legalizacao']['solicitar-autorizacao'] = 'Solicitar autorização para legalização';
		$titulos['legalizacao']['autorizar-legalizacao'] = 'Autorizar legalização';
		$titulos['legalizacao']['aprovar-orcamento'] = 'Aprovar orçamento da legalização';
		$titulos['legalizacao']['emitir-certidao-inteiro-teor'] = 'Emitir Certidão de Inteiro Teor';
		$titulos['legalizacao']['averbar-imovel'] = 'Averbar imóvel prefeitura';
		$titulos['legalizacao']['exige-cnd'] = 'Prefeitura exige CND?';
		$titulos['legalizacao']['emitir-iptu'] = 'Emitir IPTU';
		$titulos['legalizacao']['emitir-dividas'] = 'Emitir outras dívidas';
		$titulos['legalizacao']['efetuar-pagamento'] = 'Pagamento de dívidas / envio comprovante';
		$titulos['legalizacao']['efetuar-pagamento-cnd'] = 'Efetuar pagamento da CND';
		$titulos['legalizacao']['emitir-cnd'] = 'Emitir CND';
        $titulos['legalizacao']['emitir-itbi'] = 'Emitir guia de ITBI';
        $titulos['legalizacao']['requerer-comprovante-itbi'] = 'Requerer consolidação do pagamento do ITBI';


		// Consolidação
		$titulos['consolidacao']['grupo'] = 'Consolidação';
		$titulos['consolidacao']['enviar-requerimento'] = 'Enviar requerimento de consolidação';
		$titulos['consolidacao']['aprovar-orcamento'] = 'Aprovar orçamento de consolidação';
		$titulos['consolidacao']['averbar'] = 'Averbar imóvel para consolidação';
		$titulos['consolidacao']['emitir-certidao'] = 'Emitir certidão de inteiro teor para consolidação';
		$titulos['consolidacao']['finalizar'] = 'Finalizar notificação/consolidação';

		// Finalização
		$titulos['finalizacao']['grupo'] = 'Finalizadas';
		$titulos['finalizacao']['aguardando-finalizacao'] = 'Aguardando finalização';
		$titulos['finalizacao']['canceladas'] = 'Cancelamentos';
		$titulos['finalizacao']['consolidadas'] = 'Consolidadas';
		$titulos['finalizacao']['devedor-nao-encontrado'] = 'Devedor não encontrado';
		$titulos['finalizacao']['itbi-nao-autorizado'] = 'ITBI não autorizado';
		$titulos['finalizacao']['pagamento-efetuado'] = 'Pagamento efetuado';

		// Inicial
		$alcadas['inicial']['cadastradas'] = 8;
		$alcadas['inicial']['protocolar'] = 2;
		$alcadas['inicial']['analise'] = 2;
		$alcadas['inicial']['orcamento'] = 2;
		$alcadas['inicial']['aprovar-orcamento'] = 8;
		$alcadas['inicial']['devolvidas'] = 8;

		// Notificação ao devedor
		$alcadas['notificacoes']['emitir-carta-intimacao'] = 2;
		$alcadas['notificacoes']['tipo-encaminhamento'] = 2;
		$alcadas['notificacoes']['registrar-resultado'] = 2;
		$alcadas['notificacoes']['orcamento-novos-enderecos'] = 2;
		$alcadas['notificacoes']['aprovar-orcamento-enderecos'] = 8;
		$alcadas['notificacoes']['solicitar-novos-enderecos'] = 8;
		$alcadas['notificacoes']['cancelamento-outros'] = 8;
		$alcadas['notificacoes']['solicitar-segunda'] = 2;
		$alcadas['notificacoes']['autorizar-segunda'] = 8;

		// Fase de AR
		$alcadas['ar']['aprovar-orcamento'] = 8;
		$alcadas['ar']['registrar-data'] = 2;
		$alcadas['ar']['registrar-data-retorno'] = 2;
		$alcadas['ar']['registrar-retorno'] = 2;

		// Fase de edital
		$alcadas['edital']['emitir-intimacao'] = 2;
		$alcadas['edital']['inserir-orcamento'] = 2;
		$alcadas['edital']['iniciar-aprovar-orcamento'] = 2;
		$alcadas['edital']['aprovar-orcamento'] = 8;
		$alcadas['edital']['registrar-envio'] = 2;
		$alcadas['edital']['registrar-data'] = 2;
		$alcadas['edital']['atualizar-valores'] = 8;

		// Decurso de prazo
		$alcadas['decurso']['iniciar-prazo'] = 2;
		$alcadas['decurso']['solicitar-autorizacao'] = 2;
		$alcadas['decurso']['aguardar-manifesto'] = 2;
		$alcadas['decurso']['autorizar-decurso'] = 8;
		/*$alcadas['decurso']['iniciar-aguardar-pagamento'] = 2;*/
		$alcadas['decurso']['aguardando-pagamento'] = 2;
		$alcadas['decurso']['emitir-certidao'] = 2;

		// Legalização
		$alcadas['legalizacao']['solicitar-itbi'] = 8;
		$alcadas['legalizacao']['atualizacao-cadastral'] = 2;
		$alcadas['legalizacao']['solicitar-autorizacao'] = 2;
		$alcadas['legalizacao']['autorizar-legalizacao'] = 8;
		$alcadas['legalizacao']['aprovar-orcamento'] = 8;
		$alcadas['legalizacao']['emitir-certidao-inteiro-teor'] = 2;
		$alcadas['legalizacao']['averbar-imovel'] = 2;
		$alcadas['legalizacao']['exige-cnd'] = 2;
		$alcadas['legalizacao']['emitir-iptu'] = 2;
		$alcadas['legalizacao']['emitir-dividas'] = 2;
		$alcadas['legalizacao']['efetuar-pagamento'] = 8;
		$alcadas['legalizacao']['efetuar-pagamento-cnd'] = 8;
		$alcadas['legalizacao']['emitir-cnd'] = 2;
        $alcadas['legalizacao']['emitir-itbi'] = 2;
        $alcadas['legalizacao']['requerer-comprovante-itbi'] = 8;

		// Consolidação
		$alcadas['consolidacao']['enviar-requerimento'] = 8;
		$alcadas['consolidacao']['aprovar-orcamento'] = 8;
		$alcadas['consolidacao']['averbar'] = 2;
		$alcadas['consolidacao']['emitir-certidao'] = 2;
		$alcadas['consolidacao']['finalizar'] = 2;

		// Finalização
		$alcadas['finalizacao']['aguardando-finalizacao'] = 0;
		$alcadas['finalizacao']['canceladas'] = 0;
		$alcadas['finalizacao']['consolidadas'] = 0;
		$alcadas['finalizacao']['devedor-nao-encontrado'] = 0;
		$alcadas['finalizacao']['itbi-nao-autorizado'] = 0;
		$alcadas['finalizacao']['pagamento-efetuado'] = 0;

		if ($grupo) {
        	return array('regras'=>$regras[$grupo],'titulos'=>$titulos[$grupo],'alcadas'=>$alcadas[$grupo]);
		} else {
			return array('regras'=>$regras,'titulos'=>$titulos,'alcadas'=>$alcadas);
		}
    }

    public function etapa_atual() {
    	$regras = $this->regras_filtros()['regras'];
    	$titulos = $this->regras_filtros()['titulos'];
    	foreach ($regras as $gkey => $grupo) {
			foreach ($grupo as $key => $regra) {
				$regras_array[] = ['grupo'=>$gkey, 'alerta'=>$key, 'regra'=>$regra];
			}
		}
    	$atual = ['id_situacao_pedido_grupo_produto'=>$this->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto,
					   'id_fase_grupo_produto'=>$this->alienacao_pedido->alienacao_andamento_situacao->id_fase_grupo_produto,
					   'id_etapa_fase'=>$this->alienacao_pedido->alienacao_andamento_situacao->id_etapa_fase,
					   'id_acao_etapa'=>$this->alienacao_pedido->alienacao_andamento_situacao->id_acao_etapa,
                       'id_resultado_acao'=>($this->alienacao_pedido->alienacao_andamento_situacao->id_resultado_acao?$this->alienacao_pedido->alienacao_andamento_situacao->id_resultado_acao:-1)];

    	$busca = array_filter($regras_array, function($regras_array) use($atual) {
    		if(!is_array($regras_array['regra']['id_situacao_pedido_grupo_produto'])) {
    			$regras_array['regra']['id_situacao_pedido_grupo_produto'] = [$regras_array['regra']['id_situacao_pedido_grupo_produto']];
    		}
    		if(!is_array($regras_array['regra']['id_fase_grupo_produto'])) {
    			$regras_array['regra']['id_fase_grupo_produto'] = [$regras_array['regra']['id_fase_grupo_produto']];
    		}
    		if(!is_array($regras_array['regra']['id_etapa_fase'])) {
    			$regras_array['regra']['id_etapa_fase'] = [$regras_array['regra']['id_etapa_fase']];
    		}
    		if(!is_array($regras_array['regra']['id_acao_etapa'])) {
    			$regras_array['regra']['id_acao_etapa'] = [$regras_array['regra']['id_acao_etapa']];
    		}
    		if(!is_array($regras_array['regra']['id_resultado_acao'])) {
    			$regras_array['regra']['id_resultado_acao'] = [$regras_array['regra']['id_resultado_acao']];
    		}
    		if (in_array($atual['id_situacao_pedido_grupo_produto'],$regras_array['regra']['id_situacao_pedido_grupo_produto']) and
    			in_array($atual['id_fase_grupo_produto'],$regras_array['regra']['id_fase_grupo_produto']) and
    			in_array($atual['id_etapa_fase'],$regras_array['regra']['id_etapa_fase']) and
    			in_array($atual['id_acao_etapa'],$regras_array['regra']['id_acao_etapa']) and
    			in_array($atual['id_resultado_acao'],$regras_array['regra']['id_resultado_acao'])) {

    			return true;

    		} else {
    			return false;
    		}
		});
		if ($busca) {
			$busca = array_values($busca)[0];
			return $titulos[$busca['grupo']]['grupo'].' / '.$titulos[$busca['grupo']][$busca['alerta']];
		} else {
			return '-';
		}
    }

    public function valor_emolumento() {
        $valor = $this->from('alienacao_valor')
                    ->select('va_total')
                    ->where('id_produto_item','=',30)
                    ->where('id_alienacao','=',$this->id_alienacao)
                    ->first();
        /*select av.va_total from ceri.alienacao_valor av where av.id_produto_item = 30 and av.id_alienacao = 23*/
        return $valor;
    }

    public function tipo_encaminhamento() {
        $encaminhamento = \App\andamento_alienacao::select('andamento_alienacao.id_resultado_acao')
            ->where('id_alienacao_pedido',$this->alienacao_pedido->id_alienacao_pedido)
            ->join('acao_etapa',function($join) {
                $join->on('acao_etapa.id_acao_etapa','=','andamento_alienacao.id_acao_etapa')
                    ->whereIn('acao_etapa.co_acao',array(7,34));
            })
            ->orderBy('andamento_alienacao.dt_cadastro','desc')
            ->first();

        if(in_array($encaminhamento->id_resultado_acao, array(8,51))) {
            return array('tipo'=>'proprio');
        } elseif (in_array($encaminhamento->id_resultado_acao,array(9,52))) {
            return array('tipo'=>'rtd');
        } else {
            return false;
        }
    }

    public function observacoes() {
		return $this->hasMany('App\alienacao_observacao','id_alienacao')->orderBy('dt_cadastro','desc');
	}

	public function observacoes_nao_lidas() {
        $pessoa_ativa = Session::get('pessoa_ativa');
        $alienacao_observacao = new alienacao_observacao();
        $alerta_observacao = $alienacao_observacao->where('id_alienacao',$this->id_alienacao)
            ->where('id_pessoa_dest',$pessoa_ativa->id_pessoa)
            ->where('in_leitura','N')
            ->count();

        return $alerta_observacao;

    }
    /*public function status_observacoes() {
        $pessoa_ativa = Session::get('pessoa_ativa');
	    $alienacao_observacao = new alienacao_observacao();
        $alienacao_observacao->where('id_alienacao',$this->id_alienacao)
            ->where('id_pessoa_dest',$pessoa_ativa->id_pessoa)
            ->where('in_leitura','N')
            ->update(['in_leitura'=>'S']);
    }*/

    public function tempo_ultimo_andamento($uteis=false) {
        $alienacao = $this;
        if ($alienacao->alienacao_pedido->alienacao_andamento_situacao) {
            $dt_hoje = Carbon::now()->endOfDay();
            $dt_andamento = Carbon::parse($alienacao->alienacao_pedido->alienacao_andamento_situacao->dt_cadastro)->startOfDay();

            if ($uteis) {
                $feriados = \App\feriado::where(function($where) use ($dt_hoje,$dt_andamento) {
                                                    $where->where('dt_feriado', '>=', $dt_hoje)
                                                          ->where('dt_feriado', '<=', $dt_andamento);
                                        })
                                        ->where(function($where) use ($alienacao) {
                                            if ($alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]) {
                                                $where->where(function($where) use ($alienacao) {
                                                        $where->whereNull('id_cidade')
                                                              ->whereNull('id_estado');
                                                      })
                                                      ->orWhere(function($where) use ($alienacao) {
                                                            $where->where('id_cidade',$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->id_cidade)
                                                                  ->where('id_estado',$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->id_estado);
                                                      })
                                                      ->orWhere(function($where) use ($alienacao) {
                                                            $where->whereNull('id_cidade')
                                                                  ->where('id_estado',$alienacao->alienacao_pedido->pedido->pedido_pessoa_atual->pessoa->enderecos[0]->cidade->id_estado);
                                                      });
                                            }
                                        })
                                        ->where('in_registro_ativo','S')
                                        ->count();
                $dias = $dt_andamento->diffInDaysFiltered(function(Carbon $date) { return $date->isWeekday(); }, $dt_hoje)-$feriados-1;
            } else {
                $dias = $dt_andamento->diffInDays($dt_hoje);
            }

            return $dias;
        } else {
            return false;
        }
    }

    function verifica_endereco_duplicado(){
        $duplicado = DB::select("select * from ceri.f_verifica_endereco_duplicado_notificacao($this->id_alienacao, '') as duplicado");
        return end($duplicado)->duplicado;
    }

    function verifica_acao_etapa(){
        $verifica_etapa = DB::select("select * from ceri.f_retorna_fase_etapa_acao_resultado($this->id_alienacao,'A','R','C') as verifica_etapa");
        return end($verifica_etapa)->verifica_etapa;
    }
}

