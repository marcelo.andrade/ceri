<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_serventia extends Model {
	
	protected $table = 'pedido_serventia';
	protected $primaryKey = 'id_pedido_serventia';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function bem_penhora() {
		return $this->hasMany('App\bem_penhora','id_pedido_serventia');
	}
	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}
	public function pedido_serventia_resposta() {
		return $this->hasMany('App\pedido_serventia_resposta','id_pedido_serventia')->orderBy('dt_cadastro','desc');
	}

	// Funções Felipe
	public function TrasValorCartorio(preco_produto_item $preco_produto_item){
		return  $preco_produto_item->where('id_produto_item',1)->get(['va_preco_total']);
    }
}
