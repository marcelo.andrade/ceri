<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_unidade_gestora extends Model {
	
	protected $table = 'usuario_unidade_gestora';
	protected $primaryKey = 'id_usuario_unidade_gestora';
	public $timestamps = false;
	protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	public function unidade_gestora() {
		return $this->belongsTo('App\unidade_gestora','id_unidade_gestora');
	}
	
}
