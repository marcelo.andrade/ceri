<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class documento_penhora extends Model {
	
	protected $table        = 'documento_penhora';
	protected $primaryKey   = 'id_documento_penhora';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function documento_penhora() {
        return $this->belongsTo('App\penhora','id_penhora');
    }
}
