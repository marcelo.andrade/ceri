<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_bem_imovel extends Model {
	
	protected $table = 'penhora_bem_imovel';
	protected $primaryKey = 'id_penhora_bem_imovel';
    public $timestamps = false;
    protected $guarded  = array();

	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}
	public function bem_imovel() {
		return $this->belongsTo('App\bem_imovel','id_bem_imovel');
	}
	public function serventia_certificado() {
		return $this->belongsTo('App\serventia_certificado','id_serventia_certificado');
	}

	public function arquivos_grupo() {
		return $this->belongsToMany('App\arquivo_grupo_produto','penhora_bem_imovel_arquivo_grupo','id_penhora_bem_imovel','id_arquivo_grupo_produto')->where('id_tipo_arquivo_grupo_produto',12);
	}
}