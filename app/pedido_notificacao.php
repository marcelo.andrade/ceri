<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_notificacao extends Model {
	
	protected $table = 'pedido_notificacao';
	protected $primaryKey = 'id_pedido_notificacao';
    public $timestamps = false;
    protected $guarded  = array();
	
}