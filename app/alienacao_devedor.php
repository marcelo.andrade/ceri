<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_devedor extends Model {
	
	protected $table = 'alienacao_devedor';
	protected $primaryKey = 'id_alienacao_devedor';
    public $timestamps = false;
    protected $guarded  = array();

	// Atributos
	public function getNuCpfCnpjAttribute($nu_cpf_cnpj) {
		if (strlen($nu_cpf_cnpj)==11) {
			return preg_replace('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', '${1}.${2}.${3}-${4}', $nu_cpf_cnpj);
        } elseif (strlen($nu_cpf_cnpj)==14) {
			return preg_replace('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', '${1}.${2}.${3}/${4}-${5}', $nu_cpf_cnpj);
        }
    }
}
