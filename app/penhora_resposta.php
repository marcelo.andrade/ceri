<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_resposta extends Model {
	
	protected $table = 'penhora_resposta';
	protected $primaryKey = 'id_penhora_resposta';
    public $timestamps = false;
    protected $guarded  = array();
	
}