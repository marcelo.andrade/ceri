<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class modulo extends Model {
	
	protected $table = 'modulo';
	protected $primaryKey = 'id_modulo';
    public $timestamps = false;
    protected $guarded  = array();
	
}