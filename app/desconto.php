<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class desconto extends Model {
	
	protected $table = 'desconto';
	protected $primaryKey = 'id_desconto';
    public $timestamps = false;
    protected $guarded  = array();
	
}