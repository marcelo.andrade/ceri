<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_preco_produto_item_variavel_serventia extends Model
{
    protected $table = 'alienacao_preco_produto_item_variavel_serventia';
    protected $primaryKey = 'id_alienacao_preco_produto_item_variavel_serventia';
    public $timestamps = false;
    protected $guarded  = array();

    public function serventia(){
        return $this->belongsTo('App\serventia','id_serventia');
    }
    public function alienacao_preco_produto_item_variavel(){
        return $this->belongsTo('App\alienacao_preco_produto_item_variavel','id_alienacao_preco_produto_item_variavel');
    }
}