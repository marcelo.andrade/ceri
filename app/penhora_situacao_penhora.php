<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_situacao_penhora extends Model {
	
	protected $table = 'penhora_situacao_penhora';
	protected $primaryKey = 'id_penhora_situacao_penhora';
    public $timestamps = false;
    protected $guarded  = array();
	
}