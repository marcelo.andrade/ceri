<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_marca_veiculo extends Model {
	protected $table = 'marca_veiculo';
	protected $primaryKey = 'id_marca_veiculo';
    public $timestamps = false;
    protected $guarded  = array();

    public function modelo(){
        return $this->hasMany('App\detran_modelo_veiculo', 'id_marca_veiculo');
    }
}
