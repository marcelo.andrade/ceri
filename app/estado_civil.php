<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class estado_civil extends Model {
	
	protected $table = 'estado_civil';
	protected $primaryKey = 'id_estado_civil';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pessoa() {
		return $this->hasOne('App\pessoa','id_pessoa');
	}
	
}
