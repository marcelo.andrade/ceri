<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class comarca extends Model {
	
	protected $table = 'comarca';
	protected $primaryKey = 'id_comarca';
    public $timestamps = false;
    protected $guarded  = array();

	public function varas() {
		return $this->hasMany('App\vara','id_comarca');
	}

}
