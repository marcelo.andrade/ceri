<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pessoa_modulo extends Model {

	protected $table = 'pessoa_modulo';
	protected $primaryKey = 'id_pessoa_modulo';
    public $timestamps = false;
    protected $guarded  = array();

	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
	public function modulo() {
		return $this->belongsTo('App\modulo','id_modulo');
	}
}
