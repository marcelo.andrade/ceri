<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_modulo extends Model {
	
	protected $table = 'usuario_modulo';
	protected $primaryKey = 'id_usuario_modulo';
    public $timestamps = false;
    protected $guarded  = array();
	
}