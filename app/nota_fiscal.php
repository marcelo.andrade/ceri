<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class nota_fiscal extends Model {
	
	protected $table = 'nota_fiscal';
	protected $primaryKey = 'id_nota_fiscal';
    public $timestamps = false;
    protected $guarded  = array();
	
}