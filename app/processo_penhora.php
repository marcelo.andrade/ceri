<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class processo_penhora extends Model {
	
	protected $table = 'processo_penhora';
	protected $primaryKey = 'id_processo_penhora';
    public $timestamps = false;
    protected $guarded  = array();
	
}