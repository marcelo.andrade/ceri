<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_parte_operacao_veiculo extends Model {
	protected $table = 'parte_operacao_veiculo';
    protected $primaryKey = 'id_parte_operacao_veiculo';
    public $timestamps = false;
    protected $guarded  = array();

    public function pessoa() {
        return $this->belongsTo('App\pessoa','id_pessoa');
    }

    public function parte_operacao_veiculo() {
        return $this->hasOne('App\detran_parte_operacao_veiculo','id_parte_operacao_veiculo');
    }
}



