<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_valor_repasse_lote extends Model {

	protected $table = 'alienacao_valor_repasse_lote';
	protected $primaryKey = 'id_alienacao_valor_repasse_lote';
    public $timestamps = false;
    protected $guarded  = array();

    public function usuario_cad() {
        return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
    }

    public function alienacao_valor_repasse() {
        return $this->hasMany('App\alienacao_valor_repasse','id_alienacao_valor_repasse_lote');
    }
	
}
