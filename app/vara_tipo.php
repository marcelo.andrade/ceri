<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class vara_tipo extends Model {
	
	protected $table = 'vara_tipo';
	protected $primaryKey = 'id_vara_tipo';
    public $timestamps = false;
    protected $guarded  = array();	
}
