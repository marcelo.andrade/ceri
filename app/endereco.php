<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class endereco extends Model {
	
	protected $table = 'endereco';
	protected $primaryKey = 'id_endereco';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pessoa_endereco() {
		return $this->hasOne('App\pessoa_endereco','id_endereco');
	}
	public function pessoas() {
		return $this->belongsToMany('App\pessoa','pessoa_endereco','id_endereco','id_pessoa');
	}
	public function cidade() {
		return $this->belongsTo('App\cidade','id_cidade');
	}

	// Atributos
	public function getNuCepAttribute($nu_cep) {
		return preg_replace('/^(\d{5})(\d{3})$/', '${1}-${2}', $nu_cep);
    }
}
