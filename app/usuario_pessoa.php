<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_pessoa extends Model {
	
	protected $table = 'usuario_pessoa';
	protected $primaryKey = 'id_usuario_pessoa';
	public $timestamps = false;
	protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
	
}
