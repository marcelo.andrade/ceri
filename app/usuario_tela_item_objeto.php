<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_tela_item_objeto extends Model {
	
	protected $table = 'usuario_tela_item_objeto';
	protected $primaryKey = 'id_usuario_tela_item_objeto';
    public $timestamps = false;
    protected $guarded  = array();
	
}