<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class classificacao_telefone extends Model {
	
	protected $table = 'classificacao_telefone';
	protected $primaryKey = 'id_classificacao_telefone';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function telefone() {
		return $this->hasOne('App\telefone','id_telefone');
	}
	
}
