<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class protocolo_natureza extends Model {
	
	protected $table = 'protocolo_natureza';
	protected $primaryKey = 'id_protocolo_natureza';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function protocolo() {
		return $this->hasOne('App\protocolo','id_protocolo_natureza');
	}
}