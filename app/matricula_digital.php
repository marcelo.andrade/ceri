<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class matricula_digital extends Model {
	
	protected $table = 'matricula_digital';
	protected $primaryKey = 'id_matricula_digital';
    public $timestamps = false;
    protected $guarded  = array();

	public function matricula() {
		return $this->belongsTo('App\matricula','id_matricula');
	}	
}