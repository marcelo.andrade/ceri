<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class oficio extends Model {
	
	protected $table = 'oficio';
	protected $primaryKey = 'id_oficio';
    public $timestamps = false;
    protected $guarded  = array();
	
}