<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class bem_penhora extends Model {
	
	protected $table = 'bem_penhora';
	protected $primaryKey = 'id_bem_penhora';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedido_serventia() {
		return $this->belongsTo('App\pedido_serventia','id_pedido_serventia');
	}
	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}
}
