<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class estado extends Model {
	
	protected $table = 'estado';
	protected $primaryKey = 'id_estado';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function cidades() {
		return $this->hasMany('App\cidade','id_estado');
	}
	public function cidades_serventia() {
		return $this->hasMany('App\cidade','id_estado')->join('endereco','endereco.id_cidade','=','cidade.id_cidade')
													   ->join('pessoa_endereco','pessoa_endereco.id_endereco','=','endereco.id_endereco')
													   ->join('pessoa','pessoa.id_pessoa','=','pessoa_endereco.id_pessoa')
													   ->join('serventia',function($join) {
															$join->on('serventia.id_pessoa','=','pessoa.id_pessoa')
																 ->where('serventia.in_registro_ativo','=','S')
																 ->whereIn('serventia.id_tipo_serventia',array(1,3));
													   })
													   ->select('cidade.id_cidade','cidade.no_cidade')
													   ->groupBy('cidade.id_cidade','cidade.no_cidade');
	}

	public function serventias_estado() {
		return $this->hasMany('App\cidade','id_estado')->join('endereco','endereco.id_cidade','=','cidade.id_cidade')
													   ->join('pessoa_endereco','pessoa_endereco.id_endereco','=','endereco.id_endereco')
													   ->join('pessoa','pessoa.id_pessoa','=','pessoa_endereco.id_pessoa')
													   ->join('serventia',function($join) {
															$join->on('serventia.id_pessoa','=','pessoa.id_pessoa')
																 ->where('serventia.in_registro_ativo','=','S')
																 ->whereIn('serventia.id_tipo_serventia',array(1,3));
													   })
													   ->select('serventia.id_serventia','serventia.no_serventia');
	}
	public function pais() {
		return $this->belongsTo('App\pais','id_pais');
	}
	
}
