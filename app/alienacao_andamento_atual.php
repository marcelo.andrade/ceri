<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_andamento_atual extends Model {
	
	protected $table = 'alienacao_andamento_atual';
	protected $primaryKey = 'id_alienacao_andamento_atual';
    public $timestamps = false;
    protected $guarded  = array();

	public function fase_grupo_produto() {
		return $this->belongsTo('App\fase_grupo_produto','id_fase_grupo_produto');
	}
	public function etapa_fase() {
		return $this->belongsTo('App\etapa_fase','id_etapa_fase');
	}
	public function acao_etapa() {
		return $this->belongsTo('App\acao_etapa','id_acao_etapa');
	}
	public function resultado_acao() {
		return $this->belongsTo('App\resultado_acao','id_resultado_acao');
	}
}
