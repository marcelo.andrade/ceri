<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class desconto_taxa_produto_item extends Model {
	
	protected $table = 'desconto_taxa_produto_item';
	protected $primaryKey = 'id_desconto_taxa_produto_item';
    public $timestamps = false;
    protected $guarded  = array();
	
}