<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_fase_grupo_produto extends Model {
	
	protected $table = 'alienacao_fase_grupo_produto';
	protected $primaryKey = 'id_alienacao_fase_grupo_produto';
    public $timestamps = false;
    protected $guarded  = array();

    public function fase() {
    	return $this->belongsTo('App\fase_grupo_produto','id_fase_grupo_produto');    }

}
