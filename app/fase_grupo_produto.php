<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class fase_grupo_produto extends Model {
	
	protected $table = 'fase_grupo_produto';
	protected $primaryKey = 'id_fase_grupo_produto';
    public $timestamps = false;
    protected $guarded  = array();

	public function etapa_fase() {
		return $this->hasMany('App\etapa_fase','id_fase_grupo_produto');
	}

}
