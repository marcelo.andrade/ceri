<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class documento_bem_imovel extends Model {
	
	protected $table = 'documento_bem_imovel';
	protected $primaryKey = 'id_documento_bem_imovel';
    public 	  $timestamps = false;
    protected $guarded  = array();

	public function oficio() {
		return $this->belongsTo('App\oficio','id_oficio');
	}
	public function bem_imovel() {
		return $this->belongsTo('App\bem_imovel','id_bem_imovel');
	}
}