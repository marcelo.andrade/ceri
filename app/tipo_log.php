<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_log extends Model {
	
	protected $table = 'tipo_log';
	protected $primaryKey = 'id_tipo_log';
    public $timestamps = false;
    protected $guarded  = array();
	
}