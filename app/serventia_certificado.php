<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class serventia_certificado extends Model {
	
	protected $table = 'serventia_certificado';
	protected $primaryKey = 'id_serventia_certificado';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}
	public function pedidos_resultados() {
		return $this->hasMany('App\pedido_resultado','id_serventia_certificado');
	}
	public function penhoras_averbacoes() {
		return $this->hasMany('App\penhora_bem_imovel','id_serventia_certificado');
	}
}
