<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_serventia_resposta extends Model {
	
	protected $table = 'pedido_serventia_resposta';
	protected $primaryKey = 'id_pedido_serventia_resposta';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedido_serventia() {
		return $this->belongsTo('App\pedido_serventia','id_pedido_serventia');
	}
	public function tipo_resposta() {
		return $this->belongsTo('App\tipo_resposta','id_tipo_resposta');
	}
	public function usuario_cad() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
}