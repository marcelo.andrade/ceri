<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pedido_resultado_matricula extends Model {
	
	protected $table = 'pedido_resultado_matricula';
	protected $primaryKey = 'id_pedido_resultado_matricula';
    public $timestamps = false;
    protected $guarded  = array();

	public function pedido_resultado() {
		return $this->belongsTo('App\pedido_resultado','id_pedido_resultado');
	}
	public function matricula() {
		return $this->belongsTo('App\matricula','id_matricula');
	}

    /**
     * @return int
     * Função desconto retorna sempre 1 para desabilitar a inserção de desconto na certidão
     */
    public function desconto()
    {
        return 1;

        /*return $this->where('in_desconto','S')
            ->where('id_pedido_resultado', $this->id_pedido_resultado)
            ->count();*/
    }
}