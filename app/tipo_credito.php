<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_credito extends Model {
	
	protected $table = 'tipo_credito';
	protected $primaryKey = 'id_tipo_credito';
    public $timestamps = false;
    protected $guarded  = array();
	
}