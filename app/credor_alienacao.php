<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class credor_alienacao extends Model {
	
	protected $table = 'credor_alienacao';
	protected $primaryKey = 'id_credor_alienacao';
    public $timestamps = false;
    protected $guarded  = array();

	public function cidade() {
		return $this->belongsTo('App\cidade','id_cidade');
	}

	public function agencia() {
		return $this->belongsTo('App\agencia','id_agencia');
	}

	public function getNuCpfCnpjAttribute($nu_cpf_cnpj) {
		if (strlen($nu_cpf_cnpj)==11) {
			return preg_replace('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', '${1}.${2}.${3}-${4}', $nu_cpf_cnpj);
        } elseif (strlen($nu_cpf_cnpj)==14) {
			return preg_replace('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', '${1}.${2}.${3}/${4}-${5}', $nu_cpf_cnpj);
        }
    }
}
