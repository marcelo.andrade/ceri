<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class resultado_acao extends Model {
	
	protected $table = 'resultado_acao';
	protected $primaryKey = 'id_resultado_acao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function nova_fase() {
		return $this->belongsTo('App\fase_grupo_produto','id_nova_fase_grupo_produto','id_fase_grupo_produto');
	}
	public function nova_etapa() {
		return $this->belongsTo('App\etapa_fase','id_nova_etapa_fase','id_etapa_fase');
	}
	public function nova_acao() {
		return $this->belongsTo('App\acao_etapa','id_nova_acao_etapa','id_acao_etapa');
	}
	
}
