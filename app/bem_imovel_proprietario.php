<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class bem_imovel_proprietario extends Model {
	
	protected $table = 'bem_imovel_proprietario';
	protected $primaryKey = 'id_bem_imovel_proprietario';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function bem_imovel() {
		return $this->belongsTo('App\bem_imovel','id_bem_imovel');
	}
	public function proprietario_bem_imovel() {
		return $this->belongsTo('App\proprietario_bem_imovel','id_proprietario_bem_imovel');
	}
}
