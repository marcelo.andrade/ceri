<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_operacao_veiculo_arquivo_grupo extends Model {
	protected $table        = 'operacao_veiculo_arquivo_grupo';
    protected $primaryKey   = 'id_operacao_veiculo_arquivo_grupo';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function operacao_veiculo() {
        return $this->belongsTo('App\detran_operacao_veiculo','id_operacao_veiculo');
    }
    public function arquivo_grupo_produto() {
        return $this->belongsTo('App\arquivo_grupo_produto','id_arquivo_grupo_produto');
    }

}
