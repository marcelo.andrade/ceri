<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabela_preco extends Model
{
    protected $table = 'tabela_preco';
    protected $primaryKey = 'id_tabela_preco';
    public $timestamps = false;
    protected $guarded  = array();
}