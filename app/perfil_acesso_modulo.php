<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class perfil_acesso_modulo extends Model {
	
	protected $table = 'perfil_acesso_modulo';
	protected $primaryKey = 'id_perfil_acesso_modulo';
    public $timestamps = false;
    protected $guarded  = array();
	
}