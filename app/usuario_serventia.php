<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_serventia extends Model {
	
	protected $table = 'usuario_serventia';
	protected $primaryKey = 'id_usuario_serventia';
	public $timestamps = false;
	protected $guarded  = array();
	
	public function usuario() {
		return $this->belongsTo('App\usuario','id_usuario');
	}
	public function serventia() {
		return $this->belongsTo('App\serventia','id_serventia');
	}
	
}
