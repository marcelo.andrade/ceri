<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario_tela_item extends Model {
	
	protected $table = 'usuario_tela_item';
	protected $primaryKey = 'id_usuario_tela_item';
    public $timestamps = false;
    protected $guarded  = array();
	
}