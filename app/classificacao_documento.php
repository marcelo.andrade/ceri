<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class classificacao_documento extends Model {
	
	protected $table = 'classificacao_documento';
	protected $primaryKey = 'id_classificacao_documento';
    public $timestamps = false;
    protected $guarded  = array();
	
}