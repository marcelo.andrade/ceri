<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class indisponibilidade_imovel extends Model {
	
	protected $table = 'indisponibilidade_imovel';
	protected $primaryKey = 'id_indisponibilidade_imovel';
    public $timestamps = false;
    protected $guarded  = array();
	
}