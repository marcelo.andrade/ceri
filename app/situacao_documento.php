<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class versao_sistema extends Model {
	
	protected $table = 'versao_sistema';
	protected $primaryKey = 'id_versao_sistema';
    public $timestamps = false;
    protected $guarded  = array();
	
}