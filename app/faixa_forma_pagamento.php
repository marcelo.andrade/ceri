<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class faixa_forma_pagamento extends Model {
	
	protected $table = 'faixa_forma_pagamento';
	protected $primaryKey = 'id_faixa_forma_pagamento';
    public $timestamps = false;
    protected $guarded  = array();
	
}