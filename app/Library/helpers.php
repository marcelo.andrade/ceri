<?php
    use Intervention\Image\Facades\Image as Image;
    use Illuminate\Pagination\Paginator;
    use Illuminate\Pagination\LengthAwarePaginator;

    function converte_float($valor)
    {
        $valor = preg_replace('/([^0-9\.,])/i', '', $valor);
        if (!is_numeric($valor))
        {
            $valor = str_replace(array('.',','),array('','.'),$valor);
            return floatval($valor);
        } else {
            return $valor;
        }
    }

    function formatar_valor( $value )
    {
        $value = (is_numeric($value)) ? $value : 0;
        return 'R$ ' . number_format($value, 2, ',', '.');
    }

    function formatar_data( $value )
    {
        if ($value == '' or $value == null)
        {
            return '-';
        }else{
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }

    }

    function formatar_data_hora( $value )
    {
        if ($value == '' or $value == null)
        {
            return '-';
        }else {
            return \Carbon\Carbon::parse($value)->format('d/m/Y H:i:s');
        }
    }

    function retornar_valor_total_produto( $arrayValores )
    {
       return converte_float( $arrayValores[count($arrayValores)-1]->va_preco );//pegando o ultimo registro do array
    }

    function formatar_data_sql($value){
        $data = explode("/", $value);
        return $data[2].'-'.$data[1].'-'.$data[0];
    }

    function getClientIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        }

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach ($iplist as $ip) {
                    $ip_address = $ip;
                }
            } else {
                $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }

        if (!empty($_SERVER['HTTP_X_FORWARDED'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
            $ip_address = $_SERVER['HTTP_FORWARDED'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        return $ip_address;
    }

    function verifica_img_corrompida($fn,$topo=false) {
        $img = Image::make($fn)->encode('png');
        //$img->save(str_replace('.tif','.png',$fn));
        $w = $img->width();
        $h = $img->height();
        $im=imagecreatefromstring($img);
        $white=0;
        for($i=0;$i<50;++$i){
            for($j=0;$j<50;++$j){
                    $x=$w-50+$i;
                    if ($topo) {
                        $y=$j;
                    } else {
                        $y=$h-50+$j;
                    }
                    list($r,$g,$b)=array_values(imagecolorsforindex($im,imagecolorat($im,$x,$y)));
                    if($r==255 && $g==255 && $b==255)
                        ++$white;
            }
        }
        unset($img);
        // 1750 = 70% do bloco de 2500 selecionado para verificação
        if ($white<1750) {
            return true;
        } else {
            if (!$topo) {
                return verifica_img_corrompida($fn,true);
            }
            return false;
        }
    }
	
	function remove_caracteres($text) {
		$utf8 = array(
			'/[áàâãªä]/u'   =>   'a',
			'/[ÁÀÂÃÄ]/u'    =>   'A',
			'/[ÍÌÎÏ]/u'     =>   'I',
			'/[íìîï]/u'     =>   'i',
			'/[éèêë]/u'     =>   'e',
			'/[ÉÈÊË]/u'     =>   'E',
			'/[óòôõºö]/u'   =>   'o',
			'/[ÓÒÔÕÖ]/u'    =>   'O',
			'/[úùûü]/u'     =>   'u',
			'/[ÚÙÛÜ]/u'     =>   'U',
			'/ç/'           =>   'c',
			'/Ç/'           =>   'C',
			'/ñ/'           =>   'n',
			'/Ñ/'           =>   'N',
			'/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
			'/[’‘‹›‚]/u'    =>   '', // Literally a single quote
			'/[“”«»„]/u'    =>   '', // Double quote
			'/ /'           =>   '_', // nonbreaking space (equiv. to 0x160)
		);
		return preg_replace(array_keys($utf8), array_values($utf8), $text);
	}


    function mostrar_sino($id_grupo_produto, $id_pedido, $arrayNotificacao)
    {
       // $notificacao =  \Illuminate\Support\Facades\Session::get('notificacao');

        if (count($arrayNotificacao)>0)
        {
            if ($arrayNotificacao[$id_grupo_produto][$id_pedido]['id_pedido'] == $id_pedido)
            {
                echo  '<i class="glyphicon glyphicon-bell glyphicon-bell-adjustment"></i>';
            }
        }


    }

    function marcar_linha_vermelha($id_grupo_produto, $id_pedido, $arrayNotificacao)
    {
       // $notificacao =  \Illuminate\Support\Facades\Session::get('notificacao');

        if (count($arrayNotificacao)>0)
        {
            if ($arrayNotificacao[$id_grupo_produto][$id_pedido]['id_pedido'] == $id_pedido)
            {
                return 'class=linha-notificacao-danger';
            }
        }


    }
	
	function valor_extenso($valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false ) {
 
        $valor = converte_float($valor);
 
        $singular = null;
        $plural = null;
 
        if ($bolExibirMoeda) {
            $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
        } else {
            $singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("", "", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
        }
 
        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
 
 
        if ($bolPalavraFeminina) {
        
            if ($valor == 1) {
                $u = array("", "uma", "duas", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
            } else {
                $u = array("", "um", "duas", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
            }
            
            $c = array("", "cem", "duzentas", "trezentas", "quatrocentas","quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas");
            
        }
 
 
        $z = 0;
 
        $valor = number_format( $valor, 2, ".", "." );
        $inteiro = explode( ".", $valor );
 
        for ($i = 0; $i < count( $inteiro ); $i++) {
            for ($ii = mb_strlen( $inteiro[$i] ); $ii < 3; $ii++) {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }
 
        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $rt = null;
        $fim = count( $inteiro ) - ($inteiro[count( $inteiro ) - 1] > 0 ? 1 : 2);
        for ( $i = 0; $i < count( $inteiro ); $i++ ) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
 
            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count( $inteiro ) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++;
            elseif ($z > 0)
                $z--;
                
            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= ( ($z > 1) ? " de " : "") . $plural[$t];
                
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }
 
        $rt = mb_substr($rt, 1);
 
        return($rt ? trim( $rt ) : "zero");
 
    }

    function extensao_arquivo($no_arquivo) {
        $no_extensao = explode(".", $no_arquivo);
        return end($no_extensao);
    }

    function local_e_data( $strLocal = "" ){
        $arrMeses = array(
            "01" => "Janeiro",
            "02" => "Fevereiro",
            "03" => "Março",
            "04" => "Abril",
            "05" => "Maio",
            "06" => "Junho",
            "07" => "Julho",
            "08" => "Agosto",
            "09" => "Setembro",
            "10" => "Outubro",
            "11" => "Novembro",
            "12" => "Dezembro",
        );
        list($d,$m,$y) = explode('-', date('d-m-Y'));
        return sprintf("%s, %s de %s de %s", $strLocal, $d, $arrMeses[$m], $y);
    }

    function limpar_mascara($valor){
        if (!empty($valor)){
            $valor =  preg_replace('/\D+/', '', $valor);
        }

        return $valor;
    }

    function comarca($nu_cpf_cnpj){
        $pessoa = new \App\pessoa();
        $comarcas = $pessoa->join('usuario as u', 'u.id_pessoa', '=', 'pessoa.id_pessoa')
            ->join('usuario_pessoa as up','up.id_usuario','=','u.id_usuario')
            ->join('pessoa as p2','p2.id_pessoa','=','up.id_pessoa')
            ->join('vara as v','v.id_pessoa','=','p2.id_pessoa')
            ->join('comarca as c','c.id_comarca','=','v.id_comarca')
            ->select('u.id_pessoa', 'pessoa.id_pessoa', 'up.id_pessoa', 'pessoa.id_tipo_pessoa','c.no_comarca')
            ->where('pessoa.nu_cpf_cnpj', limpar_mascara($nu_cpf_cnpj))->get();

        return $comarcas;
    }

    function cpf_cnpj($string){
        if(!empty($string)) {
            return $string = strlen($string)>11? substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12) : substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9);
        }
    }

    /**
     * Gera a paginação dos itens de um array ou collection.
     *
     * @param array|Collection      $items
     * @param int  $perPage
     * @param int  $page
     *
     * @return LengthAwarePaginator
     */
    function paginateCollection($items, $perPage = 15, $page = null)
    {
        $pageName = 'pag';
        $page     = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items    = $items instanceof \Illuminate\Support\Collection ? $items : \Illuminate\Support\Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            $items->count(),
            $perPage,
            $page,
            [
                'path'     => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
