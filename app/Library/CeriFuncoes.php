<?php
/**
 * Created by PhpStorm.
 * User: Éder
 * Date: 09/06/2016
 * Time: 19:25
 */

namespace App\Library;


use App\alienacao;
use App\Http\Controllers\AlienacaoController;
use App\pedido;
use Illuminate\Support\Facades\Session;

use Auth;
use Carbon\Carbon;
use ZipArchive;
use PDF;
use File;
use Storage;
use URL;
use XMLReader;
use DOMDocument;
use DB;

use App\Library\CkCrypt2;
use App\Library\CkCertStore;

use App\usuario;
use App\usuario_serventia;
use App\usuario_vara;
use App\notificacao;
use App\notificacao_usuario;
use App\preco_produto_item;
use App\alienacao_valor_item;
use App\andamento_alienacao;
use App\arquivo_grupo_produto;
use App\historico_pedido;

use App\alienacao_arquivo_xml;
use App\baixa_arquivo_xml;
use App\intimacao_arquivo_xml;
use App\intimacao_arquivo_xml_devedor;
use App\intimacao_arquivo_xml_endereco_notificacao;

class CeriFuncoes
{

    public function assinar_arquivo($arquivo,$arquivo_p7s,$cn,$certificado) {
        $crypt = new CkCrypt2();
        $certStore = new CkCertStore();

        $signingCertSubject = $cn;
        $pfxFilename = $certificado;
        $pfxPassword = '1234';
        $inFile = $arquivo;
        $sigFile = $arquivo_p7s;

        if (!$crypt->UnlockComponent('30-day trial')) {
            return false;
        }
        if (!$certStore->LoadPfxFile($pfxFilename,$pfxPassword)) {
            return false;
        }
        $cert = $certStore->FindCertBySubjectCN($signingCertSubject);
        if (is_null($cert)) {
            return false;
        }
        
        $crypt->SetSigningCert($cert);
        
        if (!$crypt->CreateP7S($inFile,$sigFile)) {
            return false;
        }
        
        return true;
    }

    public function getDadosTransacaoRetorno( $consultationResponse )
    {
        switch ( $consultationResponse->getStatus() )
        {
            case "0":
                $msg = 'Transação com erro e/ou não autorizada';
                break;
            case "3":
                $msg = $consultationResponse->getAuthentication()->getMessage();
                break;
            case "6":
                $msg = $consultationResponse->getAuthorization()->getMessage();
                break;
            case "9":
                $msg = $consultationResponse->getCancellationInformation()->getMessage();
                break;
            case "10":
            case "12":
                $msg = 'Transação cancelada pelo usuário';
                break;
            default:
                $msg = ($consultationResponse->getAuthorization()->getMessage() != "" ) ? $consultationResponse->getAuthorization()->getMessage() : $consultationResponse->getAuthorization()->getMessage();
                break;
        }

        $nu_pedido_convenio     = $consultationResponse->getOrder()->getNumber();
        $id_compra_credito		= substr($nu_pedido_convenio, 5, strlen($nu_pedido_convenio));

        return  array(
                    "msg"                => $msg,
                    "nu_pedido_convenio" => $nu_pedido_convenio,
                    "id_compra_credito"  => $id_compra_credito,
                    "tid"                => $consultationResponse->getTid(),
                    "status"             => $consultationResponse->getStatus(),
                );

    }

    public function getTipoClienteSelo() {
        if (count(Auth::User()->usuario_pessoa)>0) {
            $pessoa_ativa = Session::get('pessoa_ativa');

            $id_tipo_pessoa = $pessoa_ativa->pessoa->id_tipo_pessoa;
        } else {
            $id_tipo_pessoa = Auth::User()->pessoa->id_tipo_pessoa;
        }

        switch ($id_tipo_pessoa)
        {
            case "2":
                $origem_utilizacao_selo = 'S';
            break;
            case "3":
                $origem_utilizacao_selo = 'C';
            break;
            case "4":
                $origem_utilizacao_selo = 'J';
			break;
			case "5":
                $origem_utilizacao_selo = 'C';
            break;
            case "7": case "11":
                $origem_utilizacao_selo = 'J';
            break;
        }

        return $origem_utilizacao_selo;

    }

    static function formatBytes($size) {
        $log = log($size, 1024);
        $finais = array('b', 'kb', 'mb', 'gb');   

        return round(pow(1024, $log - floor($log)), 2).$finais[floor($log)];
    }
    
    public function reponder_notificacao($id_pedido, $id_usuario=0)
    {
        if ($id_pedido<=0){
            return 'ERRO';
        }

        $notificacao_usuario = new notificacao_usuario();

        if ($id_usuario>0)
        {
            //por motivo de segurança na transação foi feito esta pesquisa para flegar especificamente a notificacao do usuario
            $notificacao = new notificacao();
            $resultado   = $notificacao->join('notificacao_usuario','notificacao_usuario.id_notificacao','=','notificacao.id_notificacao')
                                       ->where('id_usuario',$id_usuario)
                                       ->where('id_pedido',$id_pedido)
                                       ->where('in_leitura','N')
                                       ->select('notificacao_usuario.id_notificacao')
                                       ->first();
            if ( count($resultado)>0)
            {
                $notificacao_usuario->where('id_notificacao',$resultado->id_notificacao)
                                    ->where('id_pedido',$id_pedido)
                                    ->where('in_leitura','N')
                                    ->update(['in_leitura' => 'S', 'dt_leitura' => Carbon::now()]);
            }
        }else{
            $notificacao_usuario->where('id_pedido',$id_pedido)
                                ->where('in_leitura','N')
                                ->update(['in_leitura' => 'S', 'dt_leitura' => Carbon::now()]);
        }


        return true;
    }
    
    public function inserir_notificacao($destinatario,$id_pedido,$de_titulo_notificacao,$id_usuario=0,$id_serventia=0,$id_vara=0)
    {
        if ($id_pedido<=0){
            return 'ERRO';
        }

        $notificacao                            = new notificacao();
        $usuario_serventia                      = new usuario_serventia();
        $usuario_vara                           = new usuario_vara();

        $notificacao->id_tipo_notificacao       = 4; //informativo
        $notificacao->id_usuario_rem            = Auth::User()->id_usuario;
        $notificacao->de_titulo_notificacao     = $de_titulo_notificacao;
        $notificacao->dt_recibo                 = Carbon::now();
        $notificacao->id_usuario_cad            = Auth::User()->id_usuario;
        $notificacao->dt_cadastro               = Carbon::now();

        if ($notificacao->save())
        {
            $arrayUsuario = [];
           
           switch ( $destinatario )
           {
               case "USUARIO":
                   $arrayUsuario[]    = $id_usuario;
               break;
               case "JUDICIARIO":
                   if ( $id_vara> 0 )
                   {
                       $usuarios_vara =  $usuario_vara->where('id_vara',$id_vara)->get();
                       foreach ($usuarios_vara as $vara)
                       {
                           $arrayUsuario[] = $vara->id_usuario;
                       }
                   }
               break;
               case "SERVENTIA":
               case "TABELIONATO":
                   if ( $id_serventia > 0 )
                   {
                       $usuarios_serventia =  $usuario_serventia->where('id_serventia',$id_serventia)->get();
                       foreach ($usuarios_serventia as $serventia)
                       {
                           $arrayUsuario[] = $serventia->id_usuario;
                       }

                   }
               break;
           }
            if ( count( $arrayUsuario ) > 0 )
            {
                foreach ($arrayUsuario as $id_usuario_dest)
                {
                    $notificacao_usuario                 = new notificacao_usuario();
                    $notificacao_usuario->id_notificacao = $notificacao->id_notificacao;
                    $notificacao_usuario->id_pedido      = $id_pedido;
                    $notificacao_usuario->id_usuario     = $id_usuario_dest;
                    $notificacao_usuario->in_leitura     = 'N';
                    $notificacao_usuario->dt_cadastro    = Carbon::now();
                    $notificacao_usuario->save();
                }
            }
        }
    }

    public function listar_usuarios_serventia_ou_vara()
    {
        $usuario           = new usuario();
        $usuario_serventia = new usuario_serventia();
        $usuario_vara      = new usuario_vara();

        if ( Auth::User()->id_tipo_usuario == 4)
        {
            //pegando a vara do usuario que esta logado
            $id_vara = Auth::User()->usuario_vara->id_vara;

            if ($id_vara)
            {
                $usuarios_vara = $usuario_vara->where('id_vara', $id_vara)->get();

                foreach ($usuarios_vara as $vara)
                {
                    $arrayUsuario[] = $vara->id_usuario;
                }
                $todosUsuarios = $usuario->whereIn('id_usuario', $arrayUsuario)->get();
            }

        }

        if (in_array(Auth::User()->id_tipo_usuario,array(2,5)))
        {
            //pegando a serventia do usuario que esta logado
            $id_serventia = Auth::User()->usuario_serventia->id_serventia;

            if ( $id_serventia )
            {
                $usuarios_serventia =  $usuario_serventia->where('id_serventia',$id_serventia)->get();

                foreach ($usuarios_serventia as $serventia) {
                    $arrayUsuario[] = $serventia->id_usuario;
                }

                $todosUsuarios = $usuario->whereIn('id_usuario', $arrayUsuario)->get();
            }
        }
        return $todosUsuarios;
    }


    public function buscar_notificacoes()
    {
        $arrayNotificacao = [];
        $navanotificacao  = new notificacao();

        $busca 	   =   $navanotificacao
                            ->join('notificacao_usuario','notificacao_usuario.id_notificacao','=','notificacao.id_notificacao')
                            ->join('pedido'			  ,'pedido.id_pedido'				   ,'=','notificacao_usuario.id_pedido')
                            ->join('pedido_produto_item','pedido_produto_item.id_pedido'	   ,'=','pedido.id_pedido')
                            ->join('produto_item'		  ,'produto_item.id_produto_item'	   ,'=','pedido_produto_item.id_produto_item')
                            ->join('produto'		  	  ,'produto.id_produto'	   			   ,'=','produto_item.id_produto')

                            ->where('notificacao_usuario.id_usuario'	,'=',Auth::User()->id_usuario)
                            //->where('produto.id_grupo_produto'		,'=',1)
                            ->where('notificacao_usuario.in_leitura'	,'=','N')

                            ->select('de_titulo_notificacao', 'id_grupo_produto', 'notificacao_usuario.id_pedido', 'notificacao.id_notificacao')
                            ->orderBy('id_grupo_produto')
                            ->get();

        foreach ($busca as $notificacao)
        {
            //var_dump($notificacao);
            //criando arrayDinamico para cada grupo de produto
            $arrayNotificacao[$notificacao->id_grupo_produto][$notificacao->id_pedido] =   [
                                                                                                'id_notificacao' => $notificacao->id_notificacao,
                                                                                                'id_pedido' 	 => $notificacao->id_pedido,
                                                                                                'descricao' 	 => $notificacao->de_titulo_notificacao,
                                                                                            ];
        }


        //Session::put('notificacao',$arrayNotificacao);

       return $arrayNotificacao;
    }

	/*
	function getRestPkiClient() {
	
		// -----------------------------------------------------------------------------------------------------------
		// PASTE YOUR ACCESS TOKEN BELOW
		$restPkiAccessToken = 'CIwbHRWAC5zpd-UjyIyNm1DuNA7jOUuJMQ0s4EfTaxy0B-rwPAurzDwHauy3D5GQVZXyEiUb3vvWejzoo0yL36tQowTyKLzj0vRUDmEdgkhukRaj7kEAlI1y7IPPg87Dmgp02Bkp5Zxf_apL7BtCsZMNtgFpJvLeevG1y4-WVglLijpoQA3MML6iE4S_D1m_eJY97cl3VEroaKZAtOrn2Mnx4k2DGCIEKKkQ_Q1Qa7a6ht8EgpWWY-IpNJk3Im_jHF9hEeeOGghw6AhBlsuHvWoCuziU1ORl7dUzXxqd1Nwzn2Kn8IPek0mYteowW31HUcH3cwv68rSOEWRpRYDwGSITZgkctgVsOGbpWidGPsE8RM8NRxP2o7xMzBTc9HpNOU2_KhfZQ8op-VbbS_jdhS25dEpK7jw9klY_uPCwz2tIQAsg9tOvFzDAytPRnzJXKJ-Kx0c5WX8ubN1CJO1RUydVQSLVvnH4HwywAhGRQweNGBfK';
		//                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// -----------------------------------------------------------------------------------------------------------
	
		// Throw exception if token is not set (this check is here just for the sake of newcomers, you can remove it)
		if (strpos($restPkiAccessToken, ' API ') !== false) {
			throw new \Exception('The API access token was not set! Hint: to run this sample you must generate an API access token on the REST PKI website and paste it on the file api/util.php');
		}
	
		// -----------------------------------------------------------------------------------------------------------
		// IMPORTANT NOTICE: in production code, you should use HTTPS to communicate with REST PKI, otherwise your API
		// access token, as well as the documents you sign, will be sent to REST PKI unencrypted.
		// -----------------------------------------------------------------------------------------------------------
		$restPkiUrl = 'http://pki.rest/';
		//$restPkiUrl = 'https://pki.rest/'; // <--- USE THIS IN PRODUCTION!
	
		return new RestPkiClient($restPkiUrl, $restPkiAccessToken);
	}
	
	function setNoCacheHeaders() {
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() - 3600) . ' GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0');
		header('Pragma: no-cache');
	}
	
	public function inicia_restpki() {
		$auth = $this->getRestPkiClient()->getAuthentication();
		return $auth;
	}
	
	public function token_restpki($auth) {
		$token = $auth->startWithWebPki(StandardSecurityContexts::PKI_BRAZIL);
		return $token;
	}
	
	public function inicia_assinador($arquivo) {
		include('RestPkiClient.php');
		$signatureStarter = new CadesSignatureStarter($this->getRestPkiClient());
		$signatureStarter->setFileToSign($arquivo);
		//$signatureStarter->setSecurityContext("803517ad-3bbc-4169-b085-60053a8f6dbf");
		$signatureStarter->setSignaturePolicy(StandardSignaturePolicies::CADES_ICPBR_ADR_BASICA);
		$signatureStarter->setEncapsulateContent(true);
		return $signatureStarter;
	}
	
	public function token_assinador($signatureStarter) {
		$token = $signatureStarter->startWithWebPki();
		return $token;
	}
	
	public function finaliza_assinador($token) {
		include('RestPkiClient.php');
		$signatureFinisher = new CadesSignatureFinisher($this->getRestPkiClient());
		$signatureFinisher->setToken($token);
		$cms = $signatureFinisher->finish();
		return $cms;
	}*/
	

    /*
     * $id              = numero sequencial da tabela envolvida no processo
     * $origem          = nome da tela envolvida no processo: Ex oficio, penhora, pesquisa
     * $documento       = arquivo a ser feito upload
     * $path_destino    = destino do arquivo
     * $sub_pasta       = um nivel dentro do principal
     */
    public function uploadDocumento( $id, $origem, $documento, $sub_pasta="" )
    {
        if ( count($documento) > 0 )
        {
            if ( is_array($documento) == true )
            {
                $nome_documento = ( count($documento) == 1 ) ? $origem.'_'.$id.'_'.remove_caracteres($documento[0]->getClientOriginalName()) :   $origem.'_'.$id.'_'.time().'.zip';
            }else{
                $nome_documento = $origem.'_'.$id.'_'.remove_caracteres($documento->getClientOriginalName());
            }
            $destino        = ($sub_pasta == "" )        ?  $origem.'/'.$id              :   $origem.'/'.$id.'/'.$sub_pasta;
            $nome_arquivo   = $nome_documento;

            if (!File::isDirectory('../storage/app/public/'.$destino)) {
                File::makeDirectory('../storage/app/public/'.$destino, 0777, true, true);
            }

            if (count($documento) == 1)
            {
                if ( is_array($documento) == true ) {
                    $arquivo = $documento[0];
                    $tamanho = $documento[0]->getSize();
                }else{
                    $arquivo = $documento;
                    $tamanho = $documento->getSize();
                }
                $arquivo->move('../storage/app/public/'.$destino, $nome_arquivo);

            } else {
                $zip         = new \ZipArchive();

                $nome_zip    = $nome_arquivo;
                $dir_storage = '../storage/app/public/'.$destino;

                if ($zip->open('../storage/app/public/'.$destino . '/' . $nome_zip, ZipArchive::CREATE) === true)
                {

                    foreach ($documento as $arquivo)
                    {
                        $nome_arquivo = remove_caracteres($arquivo->getClientOriginalName());
                        $arquivo->move($dir_storage, $nome_arquivo);
                        $zip->addFile($dir_storage . '/' . $nome_arquivo, $nome_arquivo);
                    }
                    $zip->close();
                }

                #deletando os arquivos
                foreach ($documento as $arquivo) {
                    $nome_arquivo = remove_caracteres($arquivo->getClientOriginalName());
                    Storage::delete('/public/'.$destino . '/' . $nome_arquivo);
                }

                $nome_arquivo = $nome_zip;
                $tamanho      = Storage::size('/public/'.$destino . '/' . $nome_zip);
            }

            return [
                        "nome_arquivo"          => $nome_documento,
                        "tamanho"               => $tamanho,
                        "path_arquivo"          => $destino,
            ];
        }else{
            return false;
        }
    }


    function importarAlienacaoXML( $no_diretorio_arquivo, $no_arquivo, $id_poduto, $id_pessoa )
    {

        $xml 				    = new XMLReader();
        $DOM 				    = new DOMDocument();
        $registrosProcessados   = 0;

        $dir_arquivo     	= storage_path('app'.$no_diretorio_arquivo.'/'.$no_arquivo);

        DB::beginTransaction();

        $nova_alienacao_arquivo_xml                                             = new alienacao_arquivo_xml();
        $protocoloPedido 														= DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$id_poduto.");"));
        $nova_alienacao_arquivo_xml->protocolo								    = $protocoloPedido[0]->f_geraprotocolo;
        $nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml_tipo              = $nova_alienacao_arquivo_xml::ID_ALIENACAO_ARQUIVO_XML_TIPO;
        $nova_alienacao_arquivo_xml->no_diretorio_arquivo                       = $no_diretorio_arquivo;
        $nova_alienacao_arquivo_xml->no_arquivo                                 = $no_arquivo;
        $nova_alienacao_arquivo_xml->dt_arquivo                                 = Carbon::now();
        $nova_alienacao_arquivo_xml->in_processado        		                = 'N';
        $nova_alienacao_arquivo_xml->in_cancelado         		                = 'N';
        $nova_alienacao_arquivo_xml->id_usuario_cad                             = Auth::User()->id_usuario;
        $nova_alienacao_arquivo_xml->dt_cadastro                                = Carbon::now();
        $nova_alienacao_arquivo_xml->save();

        if ($nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml <=0 ){
            DB::rollback();
            return false;
        }


        $xml->open($dir_arquivo);

        while( $xml->read() )
        {

            if ( $xml->name == 'requerimento')
            {
                if ($xml->nodeType == XMLReader::ELEMENT)
                {

                    $objRequerimento 														= simplexml_import_dom($DOM->importNode($xml->expand(), true));
                    $registrosProcessados++;

                    $nova_baixa_arquivo_xml												= new intimacao_arquivo_xml();
                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml  					= $nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml;
                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml_registro_status	= 1;
                    $nova_baixa_arquivo_xml->id_legado									= $objRequerimento->id;
                    $nova_baixa_arquivo_xml->nu_contrato								= $objRequerimento->numero_contrato;
                    $nova_baixa_arquivo_xml->nu_dv_contrato								= $objRequerimento->dv_contrato;
                    $nova_baixa_arquivo_xml->codigo_serventia_cliente					= $objRequerimento->codigo_cartorio_ri;
                    $nova_baixa_arquivo_xml->matricula_imovel							= $objRequerimento->matricula;
					//$nova_baixa_arquivo_xml->codigo_credor							    = $objRequerimento->credor;
					//$nova_baixa_arquivo_xml->no_credor						    	    = $objRequerimento->credor_nome;
					//$nova_baixa_arquivo_xml->cnpj_credor							    = $objRequerimento->credor_cnpj;
                    $nova_baixa_arquivo_xml->no_endereco_imovel							= $objRequerimento->endereco_imovel;
                    $nova_baixa_arquivo_xml->nu_agencia									= $objRequerimento->agencia_contrato;
                    $nova_baixa_arquivo_xml->va_divida									= converte_float($objRequerimento->valor_divida);
                    $nova_baixa_arquivo_xml->va_leilao									= converte_float($objRequerimento->valor_leilao);
                    $nova_baixa_arquivo_xml->va_divida_projecao_1_15					= converte_float($objRequerimento->valor_1a15);
                    $nova_baixa_arquivo_xml->va_divida_projecao_16_30					= converte_float($objRequerimento->valor_16a30);
                    $nova_baixa_arquivo_xml->va_divida_projecao_31_45					= converte_float($objRequerimento->valor_31a45);
                    $nova_baixa_arquivo_xml->va_divida_projecao_46_60					= converte_float($objRequerimento->valor_46a60);
                    $nova_baixa_arquivo_xml->id_usuario_cad								= Auth::User()->id_usuario;
                    $nova_baixa_arquivo_xml->dt_cadastro								= Carbon::now();
                    $nova_baixa_arquivo_xml->save();

                    if ($nova_baixa_arquivo_xml->id_intimacao_arquivo_xml <=0 ){
                        DB::rollback();
                        return false;
                    }

                    #INSERINDO NA TABELA intimacao_arquivo_xml_endereco_notificacao o endereço do imovel com tipo_imovel = I, PEDIDO SEGISMAR DIA 21/03/2017
                    $nova_baixa_arquivo_xml_endereco_notificacao								= new intimacao_arquivo_xml_endereco_notificacao();
                    $nova_baixa_arquivo_xml_endereco_notificacao->id_intimacao_arquivo_xml		= $nova_baixa_arquivo_xml->id_intimacao_arquivo_xml;
                    $nova_baixa_arquivo_xml_endereco_notificacao->tipo_endereco 				= 'I';
                    $nova_baixa_arquivo_xml_endereco_notificacao->no_endereco   				= $objRequerimento->endereco_imovel;
                    $nova_baixa_arquivo_xml_endereco_notificacao->id_usuario_cad				= Auth::User()->id_usuario;
                    $nova_baixa_arquivo_xml_endereco_notificacao->dt_cadastro   				= Carbon::now();
                    $nova_baixa_arquivo_xml_endereco_notificacao->save();

                    if ($nova_baixa_arquivo_xml_endereco_notificacao->id_intimacao_arquivo_xml_endereco_notificacao <=0 ){
                        DB::rollback();
                        return false;
                    }

                }
            }

            if ( $xml->name == 'devedor')
            {
                if ($xml->nodeType == XMLReader::ELEMENT)
                {
                    $objDevedor 															= simplexml_import_dom($DOM->importNode($xml->expand(), true));

                    $nova_baixa_arquivo_xml_devedor										= new intimacao_arquivo_xml_devedor();
                    $nova_baixa_arquivo_xml_devedor->id_intimacao_arquivo_xml			= $nova_baixa_arquivo_xml->id_intimacao_arquivo_xml;
                    $nova_baixa_arquivo_xml_devedor->no_devedor							= $objDevedor->nome_devedor;
                    $nova_baixa_arquivo_xml_devedor->cpf_cnpj_devedor					= $objDevedor->cpf_devedor;
                    $nova_baixa_arquivo_xml_devedor->id_usuario_cad						= Auth::User()->id_usuario;
                    $nova_baixa_arquivo_xml_devedor->dt_cadastro						= Carbon::now();
                    $nova_baixa_arquivo_xml_devedor->save();

                    if ($nova_baixa_arquivo_xml_devedor->id_intimacao_arquivo_xml_devedor <=0 ){
                        DB::rollback();
                        return false;
                    }

                }
            }
            if ( $xml->name == 'endereco_notificacao')
            {
                if ($xml->nodeType == XMLReader::ELEMENT)
                {
                    $objEnderecoNotificacao 														= simplexml_import_dom($DOM->importNode($xml->expand(), true));

                    $nova_baixa_arquivo_xml_endereco_notificacao								= new intimacao_arquivo_xml_endereco_notificacao();
                    $nova_baixa_arquivo_xml_endereco_notificacao->id_intimacao_arquivo_xml		= $nova_baixa_arquivo_xml->id_intimacao_arquivo_xml;
                    $nova_baixa_arquivo_xml_endereco_notificacao->tipo_endereco 				= $objEnderecoNotificacao->tipo_endereco;
                    $nova_baixa_arquivo_xml_endereco_notificacao->no_endereco   				= $objEnderecoNotificacao->endereco;
                    $nova_baixa_arquivo_xml_endereco_notificacao->id_usuario_cad				= Auth::User()->id_usuario;
                    $nova_baixa_arquivo_xml_endereco_notificacao->dt_cadastro   				= Carbon::now();

                    #Caso o valor do endereco se NRO O não inserir no banco
                    $string = trim(str_replace(["//<![CDATA[","//]]>"],["",""],$nova_baixa_arquivo_xml_endereco_notificacao->no_endereco));
                    if($string != "NR0     0") {
                        if (!$nova_baixa_arquivo_xml_endereco_notificacao->save()) {
                            DB::rollback();
                            return false;
                        }
                    }
                }
            }

        }

        if ($registrosProcessados > 0)
        {
            DB::select(DB::raw("SELECT * FROM ceri.f_popular_notificacao_cadastro(".Auth::User()->id_usuario.", ".$id_pessoa.", ".$nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml.");"));

            $nova_alienacao_arquivo_xml->nu_registro_processados   = $registrosProcessados;
            $nova_alienacao_arquivo_xml->save();
        }


        DB::commit();
        return $nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml;
    }

    function importarBaixaAlienacaoXML($classAlienacaoController, $no_diretorio_arquivo, $no_arquivo, $id_poduto )
    {
        $alienacao                  = new alienacao();
        $ceriFuncoes                = new CeriFuncoes();
        $xml 				        = new XMLReader();
        $DOM 				        = new DOMDocument();
        $registrosProcessados       = 0;
        $dir_arquivo     	        = storage_path('app'.$no_diretorio_arquivo.'/'.$no_arquivo);

        DB::beginTransaction();

        $nova_alienacao_arquivo_xml                                             = new alienacao_arquivo_xml();
        $protocoloPedido 														= DB::select(DB::raw("SELECT * FROM ceri.f_geraprotocolo(".Auth::User()->id_usuario.", ".$id_poduto.");"));
        $nova_alienacao_arquivo_xml->protocolo								    = $protocoloPedido[0]->f_geraprotocolo;
        $nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml_tipo              = $nova_alienacao_arquivo_xml::ID_ALIENACAO_ARQUIVO_BAIXA_XML_TIPO;
        $nova_alienacao_arquivo_xml->no_diretorio_arquivo                       = $no_diretorio_arquivo;
        $nova_alienacao_arquivo_xml->no_arquivo                                 = $no_arquivo;
        $nova_alienacao_arquivo_xml->dt_arquivo                                 = Carbon::now();
        $nova_alienacao_arquivo_xml->in_processado        		                = 'N';
        $nova_alienacao_arquivo_xml->in_cancelado         		                = 'N';
        $nova_alienacao_arquivo_xml->id_usuario_cad                             = Auth::User()->id_usuario;
        $nova_alienacao_arquivo_xml->dt_cadastro                                = Carbon::now();
        $nova_alienacao_arquivo_xml->save();

        if ($nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml <=0 )
        {
            DB::rollback();
            return false;
        }

        $xml->open($dir_arquivo);

        while( $xml->read() )
        {
            if ($xml->name == 'baixa')
            {
                if ($xml->nodeType == XMLReader::ELEMENT)
                {

                    $objRequerimento = simplexml_import_dom($DOM->importNode($xml->expand(), true));
                    $registrosProcessados++;

                    $nova_baixa_arquivo_xml                                             = new baixa_arquivo_xml();
                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml                   = $nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml;
                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml_registro_status   = 1;

                    $nova_baixa_arquivo_xml->id_legado                                  = $objRequerimento->id;
                    $nova_baixa_arquivo_xml->nu_contrato                                = trim($objRequerimento->numero_contrato);
                    $nova_baixa_arquivo_xml->nu_dv_contrato                             = $objRequerimento->dv_contrato;
                    $nova_baixa_arquivo_xml->codigo_serventia_cliente                   = $objRequerimento->codigo_cartorio_ri;
                    $nova_baixa_arquivo_xml->matricula_imovel                           = $objRequerimento->matricula;
                    $nova_baixa_arquivo_xml->id_motivo_baixa                            = $objRequerimento->motivo_baixa;
                    $nova_baixa_arquivo_xml->id_usuario_cad                             = Auth::User()->id_usuario;
                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml_registro_status   = $nova_baixa_arquivo_xml::ID_ALIENACAO_ARQUIVO_STATUS_CADASTRADO;
                    $nova_baixa_arquivo_xml->dt_cadastro                                = Carbon::now();

                    if ($nova_baixa_arquivo_xml->save())
                    {
                        //REALIZAR A BAIXA
                        $alienacao_selecionada                       = $alienacao->select('alienacao.id_alienacao', 'pedido.id_situacao_pedido_grupo_produto')
                                                                                 ->join('alienacao_pedido','alienacao_pedido.id_alienacao','=','alienacao.id_alienacao')
                                                                                 ->join('pedido','pedido.id_pedido','=','alienacao_pedido.id_pedido')
                                                                                 ->where('numero_contrato', trim($objRequerimento->numero_contrato))
                                                                                 ->whereNotIn('pedido.id_situacao_pedido_grupo_produto',
                                                                                                                                [
                                                                                                                                    $classAlienacaoController::ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ,
                                                                                                                                    $classAlienacaoController::ID_SITUACAO_CANCELADO,
                                                                                                                                    $classAlienacaoController::ID_SITUACAO_FINALIZADO
                                                                                                                                ])
                                                                                 ->get();


                        if (count($alienacao_selecionada)>0)
                        {
                            foreach ( $alienacao_selecionada as $alienacao_sel )
                            {
                                $erro = $ceriFuncoes->cancelar_alienacoes([$alienacao_sel->id_alienacao], $classAlienacaoController);

                                if ($erro > 0) {
                                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml_registro_status = $nova_baixa_arquivo_xml::ID_ALIENACAO_ARQUIVO_STATUS_NAO_PROCESSADO;
                                    if (!$nova_baixa_arquivo_xml->save()) {
                                        DB::rollback();
                                    }
                                } else {
                                    $nova_baixa_arquivo_xml->id_alienacao_arquivo_xml_registro_status = $nova_baixa_arquivo_xml::ID_ALIENACAO_ARQUIVO_STATUS_PROCESSADO_COM_SUCESSO;

                                    if (!$nova_baixa_arquivo_xml->save()) {
                                        DB::rollback();
                                    }

                                    $nova_alienacao   = new alienacao();
                                    $nova_alienacao->where('id_alienacao', $alienacao_sel->id_alienacao)
                                                   ->update(['id_baixa_arquivo_xml' => $nova_baixa_arquivo_xml->id_baixa_arquivo_xml]);

                                }
                            }
                        }

                    }else{
                        DB::rollback();
                        return false;
                    }

                }
            }
        }

        if ($registrosProcessados > 0)
        {
            //DB::select(DB::raw("SELECT * FROM ceri.f_popular_notificacao_baixa(".Auth::User()->id_usuario.", ".$id_pessoa.", ".$nova_alienacao_arquivo_xml->id_alienacao_arquivo_xml.");"));

            $nova_alienacao_arquivo_xml->nu_registro_processados   = $registrosProcessados;
            $nova_alienacao_arquivo_xml->save();
            DB::commit();
            return true;
        }
        return false;
    }

    public function cancelar_alienacoes($arrayIdsAlienacoes, $class)
    {
        $erro = 0;
        $alienacao          = new alienacao();

        //Acessando atributos e metodos da clase AlienacaoControler
        $ControlerAlienacao = $class;

        if (count($arrayIdsAlienacoes)>0)
        {
            foreach ($arrayIdsAlienacoes as $id_alienacao)
            {
                $pedido    = new pedido();
                $alienacao = $alienacao->find($id_alienacao);

                // Cancelamento
                $novo_andamento_alienacao = new andamento_alienacao();
                $novo_andamento_alienacao->id_fase_grupo_produto = 8;
                $novo_andamento_alienacao->id_etapa_fase = 22;
                $novo_andamento_alienacao->id_acao_etapa = 48;
                $novo_andamento_alienacao->id_resultado_acao = 84;
                $novo_andamento_alienacao->id_usuario_etapa = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_acao = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_usuario_cad = Auth::User()->id_usuario;
                $novo_andamento_alienacao->id_alienacao_pedido = $alienacao->alienacao_pedido->id_alienacao_pedido;

                if ($novo_andamento_alienacao->save()) {
                    $destino = 'alienacoes/' . $alienacao->id_alienacao . '/' . $novo_andamento_alienacao->id_acao_etapa;
                    $no_arquivo = 'requerimento-cancelamento_' . $alienacao->alienacao_pedido->pedido->protocolo_pedido . '.pdf';
                    $destino_arquivo = '/public/' . $destino . '/' . $no_arquivo;

                    Storage::makeDirectory('/public/' . $destino);

                    $pdf = $ControlerAlienacao->gera_pdf('cancelamento', $alienacao);

                    if (Storage::exists($destino_arquivo)) {
                        Storage::delete($destino_arquivo);
                    }
                    if ($pdf->save(storage_path('app' . $destino_arquivo))) {
                        $novo_arquivo_grupo_produto = new arquivo_grupo_produto();
                        $novo_arquivo_grupo_produto->id_grupo_produto = $ControlerAlienacao::ID_GRUPO_PRODUTO;
                        $novo_arquivo_grupo_produto->id_tipo_arquivo_grupo_produto = 22;
                        $novo_arquivo_grupo_produto->no_arquivo = $no_arquivo;
                        $novo_arquivo_grupo_produto->no_local_arquivo = $destino;
                        $novo_arquivo_grupo_produto->id_usuario_cad = Auth::User()->id_usuario;
                        $novo_arquivo_grupo_produto->no_extensao = extensao_arquivo($no_arquivo);
                        $novo_arquivo_grupo_produto->in_ass_digital = 'N';
                        $novo_arquivo_grupo_produto->nu_tamanho_kb = Storage::size($destino_arquivo);
                        $novo_arquivo_grupo_produto->no_hash = hash_file('md5', storage_path('app' . $destino_arquivo));
                        if ($novo_arquivo_grupo_produto->save()) {
                            $novo_andamento_alienacao->arquivos_grupo()->attach($novo_arquivo_grupo_produto, array('in_acao' => 'S'));
                        } else {
                            $erro = 1;
                        }
                    } else {
                        $erro = 1;
                    }
                } else {
                    $erro = 1;
                }

                switch ($alienacao->alienacao_pedido->pedido->id_situacao_pedido_grupo_produto) {
                    case $ControlerAlienacao::ID_SITUACAO_CADASTRADO:
                        $novo_status = $ControlerAlienacao::ID_SITUACAO_CANCELADO;
                        break;
                    default:
                        $novo_status = ($alienacao->checar_cancelamento_pagamento() == NULL) ? $ControlerAlienacao::ID_SITUACAO_CANCELADO : $ControlerAlienacao::ID_SITUACAO_AGUARDANDOPAGAMENTOFINALIZ;
                        break;
                }

                if ($pedido->where('id_pedido', $alienacao->alienacao_pedido->id_pedido)->update(['id_situacao_pedido_grupo_produto' => $novo_status])) {
                    $novo_historico = new historico_pedido();
                    $novo_historico->id_pedido = $alienacao->alienacao_pedido->id_pedido;
                    $novo_historico->id_situacao_pedido_grupo_produto = $novo_status;
                    $novo_historico->de_observacao = 'Notificacao cancelada com sucesso.';
                    $novo_historico->id_usuario_cad = Auth::User()->id_usuario;
                    $novo_historico->save();
                } else {
                    $erro = 1;
                }
            }
        } else {
            $erro = 1;
        }

        return $erro;
    }



}


