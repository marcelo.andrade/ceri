<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class situacao_penhora extends Model {
	
	protected $table = 'situacao_penhora';
	protected $primaryKey = 'id_situacao_penhora';
    public $timestamps = false;
    protected $guarded  = array();
	
}