<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class telefone extends Model {
	
	protected $table = 'telefone';
	protected $primaryKey = 'id_telefone';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pessoas() {
		return $this->belongsToMany('App\pessoa','pessoa_telefone','id_telefone','id_pessoa');
	}
	public function classificacao_telefone() {
		return $this->belongsTo('App\classificacao_telefone','id_telefone');
	}
	public function tipo_telefone() {
		return $this->belongsTo('App\tipo_telefone','id_tipo_telefone');
	}
	
}
