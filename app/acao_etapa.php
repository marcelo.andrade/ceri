<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class acao_etapa extends Model {
	
	protected $table = 'acao_etapa';
	protected $primaryKey = 'id_acao_etapa';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function proxima_acao() {
		return $this->belongsTo('App\acao_etapa','id_acao_etapa_proxima','id_acao_etapa');
	}
	public function etapa_fase() {
		return $this->belongsTo('App\etapa_fase','id_etapa_fase');
	}
	public function resultado_acao() {
		return $this->hasMany('App\resultado_acao','id_acao_etapa')->where('in_registro_ativo','S')->orderBy('nu_ordem');
	}
	
}
