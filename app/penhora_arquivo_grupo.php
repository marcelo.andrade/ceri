<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class penhora_arquivo_grupo extends Model {

    protected   $table      = 'penhora_arquivo_grupo';
    protected   $primaryKey = 'id_penhora_arquivo_grupo';
    public      $timestamps = false;
    protected   $guarded    = array();

    public function penhora() {
        return $this->belongsTo('App\penhora','id_penhora');
    }
    public function arquivo_grupo_produto() {
        return $this->belongsTo('App\arquivo_grupo_produto','id_arquivo_grupo_produto');
    }

}