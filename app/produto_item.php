<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class produto_item extends Model {
	
	protected $table = 'produto_item';
	protected $primaryKey = 'id_produto_item';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedidos() {
		return $this->hasMany('App\pedido_produto_item','id_produto_item');
	}
	public function produto() {
		return $this->belongsTo('App\produto','id_produto');
	}
    public function lista_preco($ids_pessoa="") {
        $valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_produto_pessoa ('".$this->id_produto_item."','".$ids_pessoa."' ,';', 'L') as (descricao text, va_preco text);"));
        return $valores;
    }
	public function lista_preco_pedido($id_pedido, $opcao_retorno = null) {
        $opcao_retorno = ($opcao_retorno == null) ? 'null' : $opcao_retorno;
		$valores = DB::select(DB::raw("SELECT * FROM ceri.f_lista_preco_pedido($id_pedido, '" . $opcao_retorno . "') as (descricao text, va_preco text);"));
		return $valores;
	}
	public function preco_total($ids_pessoa="") {
		$valores = $this->lista_preco($ids_pessoa);
		return converte_float(end($valores)->va_preco);
	}
    public function pedido_desconto_origem($id_pedido, $opcao_retorno) {
        /**
         * Opções de retorno da função que podem ser passadas no parametro opcao_retorno
         * PROTOCOLO_PEDIDO_DESCONTO
         * ID_PEDIDO_DESCONTO
         * VA_MOVIMENTO_DESCONTO
         * NO_PRODUTO_ITEM_DESCONTO
         * IN_MOVIMENTO_DESCONTO
         */
        $valor = DB::select(DB::raw("SELECT * FROM ceri.f_retorna_pedido_desconto_origem($id_pedido, '" . $opcao_retorno . "') as descricao"));
        return end($valor);
    }
} 