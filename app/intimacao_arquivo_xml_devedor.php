<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class intimacao_arquivo_xml_devedor extends Model {

	protected   $table          = 'intimacao_arquivo_xml_devedor';
	protected   $primaryKey     = 'id_intimacao_arquivo_xml_devedor';
    public      $timestamps     = false;
    protected   $guarded        = array();
	
}
