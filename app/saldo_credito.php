<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class saldo_credito extends Model {
	
	protected $table = 'saldo_credito';
	protected $primaryKey = 'id_saldo_credito';
    public $timestamps = false;
    protected $guarded  = array();
	
}