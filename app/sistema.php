<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class sistema extends Model {
	
	protected $table = 'sistema';
	protected $primaryKey = 'id_sistema';
    public $timestamps = false;
    protected $guarded  = array();
	
}