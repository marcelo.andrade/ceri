<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class agencia extends Model {

    protected $table = 'agencia';
    protected $primaryKey = 'id_agencia';
    public $timestamps = false;
    protected $guarded  = array();


    public function credor_alienacao() {
        return $this->hasOne('App\credor_alienacao','id_agencia');
    }
    public function cidade() {
        return $this->belongsTo('App\cidade','id_cidade');
    }

    public function agencia_credor_alienacao()
    {

        $agenciasCredorAlinacao = \App\agencia::select('agencia.id_agencia','agencia.codigo_agencia','agencia.no_agencia')
                                                ->join('credor_alienacao','credor_alienacao.id_agencia','=','agencia.id_agencia')
                                                ->join('alienacao_credor_alienacao','alienacao_credor_alienacao.id_credor_alienacao','=','credor_alienacao.id_credor_alienacao')
                                                ->orderBy('codigo_agencia')->get();
                                                //->toSql();
        return $agenciasCredorAlinacao;
    }


}
