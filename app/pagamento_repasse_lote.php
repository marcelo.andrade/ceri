<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pagamento_repasse_lote extends Model {

    protected $table = 'pagamento_repasse_lote';
    protected $primaryKey = 'id_pagamento_repasse_lote';
    public $timestamps = false;
    protected $guarded  = array();

    public function pessoa() {
        return $this->belongsTo('App\pessoa','id_pessoa');
    }

    public function situacao_pagamento_repasse() {
        return $this->belongsTo('App\situacao_pagamento_repasse','id_situacao_pagamento_repasse');
    }

    public function pagamento_repasse_pessoa_lote() {
        return $this->hasMany('App\pagamento_repasse_pessoa_lote', 'id_pagamento_repasse_lote');
    }
}
