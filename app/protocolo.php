<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class protocolo extends Model {
	
	protected $table = 'protocolo';
	protected $primaryKey = 'id_protocolo';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function pedido() {
		return $this->belongsTo('App\pedido','id_pedido');
	}
	public function protocolo_natureza() {
		return $this->belongsTo('App\protocolo_natureza','id_protocolo_natureza');
	}
	public function usuario_cadastrado() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
	public function observacoes() {
		return $this->hasMany('App\protocolo_observacao','id_protocolo')->orderBy('dt_cadastro','desc');
	}
	public function bem_imoveis() {
		return $this->belongsToMany('App\bem_imovel','protocolo_bem_imovel','id_protocolo','id_bem_imovel');
	}
	public function arquivos() {
		return $this->hasMany('App\protocolo_arquivo','id_protocolo');
	}
}