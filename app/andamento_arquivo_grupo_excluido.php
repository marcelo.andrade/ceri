<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class andamento_arquivo_grupo_excluido extends Model {
	
	protected $table = 'andamento_arquivo_grupo_excluido';
	protected $primaryKey = 'id_andamento_arquivo_grupo_excluido';
    public $timestamps = false;
    protected $guarded  = array();

    public function andamento_alienacao() {
    	return $this->belongsTo('App\andamento_alienacao','id_andamento_alienacao');
    }
    public function arquivo_grupo_produto() {
    	return $this->hasOne('App\arquivo_grupo_produto','id_arquivo_grupo_produto');
    }

}
