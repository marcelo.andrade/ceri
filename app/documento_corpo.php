<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class documento_corpo extends Model {
	
	protected $table = 'documento_corpo';
	protected $primaryKey = 'id_documento_corpo';
    public $timestamps = false;
    protected $guarded  = array();

	public function documento() {
		return $this->belongsTo('App\documento','id_documento');
	}
	public function usuario_cadastrado() {
		return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
	}
}