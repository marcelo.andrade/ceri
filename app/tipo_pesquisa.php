<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_pesquisa extends Model {
	
	protected $table = 'tipo_pesquisa';
	protected $primaryKey = 'id_tipo_pesquisa';
    public $timestamps = false;
    protected $guarded  = array();
	
}