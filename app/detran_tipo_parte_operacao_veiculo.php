<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class detran_tipo_parte_operacao_veiculo extends Model {
	protected $table = 'tipo_parte_operacao_veiculo';
    protected $primaryKey = 'id_tipo_parte_operacao_veiculo';
    public $timestamps = false;
    protected $guarded  = array();
	
}
