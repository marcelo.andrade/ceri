<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class andamento_arquivo_grupo extends Model {
	
	protected $table = 'andamento_arquivo_grupo';
	protected $primaryKey = 'id_andamento_arquivo_grupo';
    public $timestamps = false;
    protected $guarded  = array();

    public function andamento_alienacao() {
    	return $this->belongsTo('App\andamento_alienacao','id_andamento_alienacao');
    }
    public function arquivo_grupo_produto() {
    	return $this->belongsTo('App\arquivo_grupo_produto','id_arquivo_grupo_produto');
    }

}
