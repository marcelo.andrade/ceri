<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class oficio_situacao_oficio extends Model {
	
	protected $table = 'oficio_situacao_oficio';
	protected $primaryKey = 'id_oficio_situacao_oficio';
    public $timestamps = false;
    protected $guarded  = array();
	
}