<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_parte extends Model {
	
	protected $table = 'tipo_parte';
	protected $primaryKey = 'id_tipo_parte';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function parte() {
		return $this->hasOne('App\parte','id_tipo_parte');
	}
}