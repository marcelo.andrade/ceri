<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class endereco_alienacao_devedor extends Model {
	
	protected $table = 'endereco_alienacao_devedor';
	protected $primaryKey = 'id_endereco_alienacao_devedor';
    public $timestamps = false;
    protected $guarded  = array();

    public function cidade() {
    	return $this->belongsTo('App\cidade','id_cidade');
    }
}
