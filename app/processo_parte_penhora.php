<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class processo_parte_penhora extends Model {
	
	protected $table = 'processo_parte_penhora';
	protected $primaryKey = 'id_processo_parte_penhora';
    public $timestamps = false;
    protected $guarded  = array();

	public function processo_parte() {
		return $this->belongsTo('App\processo_parte','id_processo_parte');
	}
	public function penhora() {
		return $this->belongsTo('App\penhora','id_penhora');
	}
	public function parte() {
		return $this->belongsTo('App\parte','id_parte');
	}
}