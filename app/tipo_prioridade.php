<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_prioridade extends Model {
	
	protected $table = 'tipo_prioridade';
	protected $primaryKey = 'id_tipo_prioridade';
    public $timestamps = false;
    protected $guarded  = array();
	
}