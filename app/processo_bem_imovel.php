<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class processo_bem_imovel extends Model {
	
	protected $table = 'processo_bem_imovel';
	protected $primaryKey = 'id_processo_bem_imovel';
    public $timestamps = false;
    protected $guarded  = array();
	
}