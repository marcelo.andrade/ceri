<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class pagamento_repasse_pessoa_lote extends Model {

    protected $table = 'pagamento_repasse_pessoa_lote';
    protected $primaryKey = 'id_pagamento_repasse_pessoa_lote';
    public $timestamps = false;
    protected $guarded  = array();

    public function situacao_pagamento_repasse() {
        return $this->belongsTo('App\situacao_pagamento_repasse','id_situacao_pagamento_repasse');
    }

    public function pessoa() {
        return $this->belongsTo('App\pessoa','id_pessoa');
    }

    public function pagamento_repasse_parcela()
    {
        return $this->hasMany('App\pagamento_repasse_parcela', 'id_pagamento_repasse_pessoa_lote');
    }

    public function movimentacao_parcela_repasse() {
        return $this->belongsTo('App\movimentacao_parcela_repasse','id_movimentacao_parcela_repasse');
    }

}
