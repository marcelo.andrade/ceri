<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class feriado extends Model {
	
	protected $table = 'feriado';
	protected $primaryKey = 'id_feriado';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function cidade() {
		return $this->belongsTo('App\cidade','id_cidade');
	}
	public function estado() {
		return $this->belongsTo('App\estado','id_estado');
	}
	
}
