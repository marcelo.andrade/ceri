<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_pagamento_alienacao extends Model {
	
	protected $table = 'alienacao_pagamento_alienacao';
	protected $primaryKey = 'id_alienacao_pagamento_alienacao';
    public $timestamps = false;
    protected $guarded  = array();

    public function alienacao_pagamento() {
        return $this->belongsTo('App\alienacao_pagamento','id_alienacao_pagamento');
    }

}

