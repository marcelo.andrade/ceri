<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_custa extends Model {
	
	protected $table        = 'tipo_custa';
	protected $primaryKey   = 'id_tipo_custa';
    public    $timestamps   = false;
    protected $guarded      = array();

    public function tipo_custa_penhora() {
        return $this->hasOne('App\penhora','id_tipo_custa');
    }
}
