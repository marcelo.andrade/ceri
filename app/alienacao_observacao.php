<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class alienacao_observacao extends Model {
	
	protected $table = 'alienacao_observacao';
	protected $primaryKey = 'id_alienacao_observacao';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function alienacao() {
		return $this->belongsTo('App\alienacao','id_alienacao');
	}
	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}
	public function pessoa_dest() {
		return $this->belongsTo('App\pessoa','id_pessoa_dest','id_pessoa');
	}
	public function usuario_cad() {
    	return $this->belongsTo('App\usuario','id_usuario_cad','id_usuario');
    }
    public function alienacao_observacao_arquivo_grupo() {
        return $this->belongsToMany('App\arquivo_grupo_produto','alienacao_observacao_arquivo_grupo','id_alienacao_observacao','id_arquivo_grupo_produto');
    }
}