<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class permissao extends Model {
	
	protected 	$table 		= 'permissao';
	protected 	$primaryKey = 'id_permissao';
    public 		$timestamps = false;
    protected 	$guarded  	= array();

	public function perfil()
	{
		return $this->belongsToMany('App\perfil','permissao_perfil','id_permissao','id_perfil');
	}
}
