<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_indisponibilizacao extends Model {
	
	protected $table = 'tipo_indisponibilizacao';
	protected $primaryKey = 'id_tipo_indisponibilizacao';
    public $timestamps = false;
    protected $guarded  = array();
	
}