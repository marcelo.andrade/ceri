<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class imovel extends Model {
	
	protected $table = 'imovel';
	protected $primaryKey = 'id_imovel';
    public $timestamps = false;
    protected $guarded  = array();
	
}