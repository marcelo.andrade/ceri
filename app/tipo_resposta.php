<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_resposta extends Model {
	
	protected $table = 'tipo_resposta';
	protected $primaryKey = 'id_tipo_resposta';
    public $timestamps = false;
    protected $guarded  = array();
	
/*	public function pedidos() {
		return $this->belongsToMany('App\pedido_serventia','pedido_serventia_resposta','id_tipo_resposta','id_pedido_serventia');
	}*/
	public function pedido_serventia_resposta() {
		return $this->hasOne('App\pedido_serventia_resposta','id_tipo_resposta');
	}
}