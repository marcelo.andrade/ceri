<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class serventia_cliente extends Model {

    protected   $table      = 'serventia_cliente';
    protected   $primaryKey = 'id_serventia_cliente';
    public      $timestamps = false;
    protected   $guarded    = array();

    public function serventia() {
        return $this->belongsTo('App\serventia','id_serventia');
    }

}
