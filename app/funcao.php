<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class funcao extends Model {
	
	protected $table = 'funcao';
	protected $primaryKey = 'id_funcao';
    public $timestamps = false;
    protected $guarded  = array();
	
}