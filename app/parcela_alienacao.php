<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class parcela_alienacao extends Model {
	
	protected $table = 'parcela_alienacao';
	protected $primaryKey = 'id_parcela_alienacao';
    public $timestamps = false;
    protected $guarded  = array();

}
