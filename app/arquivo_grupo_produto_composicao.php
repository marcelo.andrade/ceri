<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class arquivo_grupo_produto_composicao extends Model {
	
	protected $table = 'arquivo_grupo_produto_composicao';
	protected $primaryKey = 'id_arquivo_grupo_produto_composicao';
    public $timestamps = false;
    protected $guarded  = array();

    public function arquivo_grupo_produto() {
    	return $this->belongsTo('App\arquivo_grupo_produto','id_arquivo_grupo_produto');
    }
    
}
