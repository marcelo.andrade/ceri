<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class vara extends Model {
	
	protected $table = 'vara';
	protected $primaryKey = 'id_vara';
    public $timestamps = false;
    protected $guarded  = array();
	
	public function comarca() {
		return $this->belongsTo('App\comarca','id_comarca');
	}
	public function vara_tipo() {
		return $this->belongsTo('App\vara_tipo','id_vara_tipo');
	}
	public function pessoa() {
		return $this->belongsTo('App\pessoa','id_pessoa');
	}

	public function usuario_vara() {
		return $this->hasMany('App\usuario_vara','id_vara');
	}	
}
