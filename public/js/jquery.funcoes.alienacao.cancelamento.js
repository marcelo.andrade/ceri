$(document).ready(function() {
	////////////////////////////////////////////////////////////
	// Eventos de cancelamento 				                  //
	////////////////////////////////////////////////////////////

	$('table#pedidos-cancelamento').on('click','input#selecionar-todas',function(e) {
		if ($(this).is(':checked')) {
			$('table#pedidos-cancelamento input.alienacao:not(:checked)').trigger('click');
		} else {
			$('table#pedidos-cancelamento input.alienacao:checked').trigger('click');
		}
	});

	$('table#pedidos-cancelamento').on('click','input.alienacao',function(e) {
		var total = $('table#pedidos-cancelamento input.alienacao:checked').length;
		
		if (total<=0) {
			$('div#alert span.total').html('0 notificações selecionadas');
			$('div#alert button').prop('disabled',true).addClass('disabled');

			$('div#alert ul.dropdown-menu a.gerar-relatorio-todas').parent('li').addClass('disabled');
			$('div#alert ul.dropdown-menu a.gerar-relatorio-todas span').html('');
		} else {
			$('div#alert span.total').html((total==1?'1 notificação selecionada':total+' notificações selecionadas'));
			$('div#alert button').prop('disabled',false).removeClass('disabled');
		}
	});

	$('div#cancelamento-notificacoes').on('show.bs.modal', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos-cancelamento input.alienacao:checked,table#pedidos input.alienacao-pendente:checked,table#pedidos input.alienacao-encaminhada:checked,table#pedidos input.alienacao-orcamento:checked,table#pedidos input.alienacao-decurso:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/relatorio/cancelamento-notificacoes',
            data: {'ids_alienacoes':ids_alienacoes},
            beforeSend: function() {
                $('div#cancelamento-notificacoes .modal-body div.carregando').show();
                $('div#cancelamento-notificacoes .modal-body div.form').hide();
            },
            success: function(retorno) {
                $('div#cancelamento-notificacoes .modal-body div.form').html(retorno);
                $('div#cancelamento-notificacoes .modal-body div.carregando').hide();
                $('div#cancelamento-notificacoes .modal-body div.form').fadeIn('fast');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });
    $('div#cancelamento-notificacoes').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-cancelamento-notificacoes]').submit();
    });

    $('div#cancelamento-notificacoes').on('click','button.salvar-cancelamento', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos-cancelamento input.alienacao:checked,table#pedidos input.alienacao-pendente:checked,table#pedidos input.alienacao-encaminhada:checked,table#pedidos input.alienacao-orcamento:checked,table#pedidos input.alienacao-decurso:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        swal({
            title: 'Tem certeza?',
            type: 'warning',
            html: 'Tem certeza que deseja cancelar '+(ids_alienacoes.length>1?'as '+ids_alienacoes.length+' notificações selecionadas':'a notificação selecionada')+'?',
            showCancelButton: true,
            cancelButtonText: 'Não',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim'
        }).then(function(retorno) {
            if (retorno==true) {
                $.ajax({
                    type: "POST",
                    url: url_base+'servicos/alienacao/cancelar-alienacoes',
                    data: {'ids_alienacoes':ids_alienacoes},
                    beforeSend: function() {
                        $('div#cancelamento-notificacoes .modal-body div.carregando').show();
                        $('div#cancelamento-notificacoes .modal-body div.form').hide();
                    },
                    success: function(retorno) {
                        $('div#cancelamento-notificacoes .modal-body div.carregando').hide();
                        $('div#cancelamento-notificacoes .modal-body div.form').fadeIn('fast');
                        switch (retorno.status) {
                            case 'erro':
                                swal("Erro!",retorno.msg,"error");
                                break;
                            case 'sucesso':
                                swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
                                    location.reload();
                                });
                                break;
                        }
                    },
                    error: function (request, status, error) {
                        swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                            location.reload();
                        });
                    }
                });
            }
        });
    });
    ////////////////////////////////////////////////////////////
    // Eventos de relatório 				                  //
    ////////////////////////////////////////////////////////////
    $('table#pedidos-cancelamento').on('click','input#selecionar-todas',function(e) {
        if ($(this).is(':checked')) {
            $('table#pedidos-cancelamento input.alienacao:not(:checked)').trigger('click');
        } else {
            $('table#pedidos-cancelamento input.alienacao:checked').trigger('click');
        }
    });

    $('table#pedidos-cancelamento').on('click','input.alienacao',function(e) {
        var total = $('table#pedidos-cancelamento input.alienacao:checked').length;

        if (total<=0) {
            $('div#alert span.total').html('0 notificações selecionadas');
            $('div#alert button').prop('disabled',true).addClass('disabled');

            $('div#alert ul.dropdown-menu a.gerar-relatorio').parent('li').addClass('disabled');
            $('div#alert ul.dropdown-menu a.gerar-relatorio span').html('');
        } else {
            $('div#alert span.total').html((total==1?'1 notificação selecionada':total+' notificações selecionadas'));
            $('div#alert button').prop('disabled',false).removeClass('disabled');

            $('div#alert ul.dropdown-menu a.gerar-relatorio').parent('li').removeClass('disabled');
            $('div#alert ul.dropdown-menu a.gerar-relatorio span').html('('+total+')');
        }
    });

    $('div#relatorio-notificacoes-pesquisa').on('show.bs.modal', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos-cancelamento input.alienacao:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/relatorio',
            data: {'ids_alienacoes':ids_alienacoes},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-notificacoes-pesquisa').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    $('div#relatorio-notificacoes-pesquisa').on('click','button.salvar-relatorio-pesquisa', function (event) {
        $('form[name=form-salvar-relatorio]').submit();
        $('div#relatorio-notificacoes').modal('hide');
    });
});