$(document).ready(function() {

	////////////////////////////////////////////////////////////
	// Eventos do botão "Inserir imóvel"                      //
	////////////////////////////////////////////////////////////
	
	$('div#novo-imovel').on('show.bs.modal', function (event) {
		var id_cidade = $(event.relatedTarget).data('idcidade');
		var alienacao_token = $(event.relatedTarget).data('alienacaotoken');
		var acao = $(event.relatedTarget).data('acao');
		
		switch(acao) {
			case 'N':
				texto_acao = 'Inserir imóvel';
				break;
			case 'A':
				texto_acao = 'Alterar imóvel';
				break;
		}
		
		$(this).find('.modal-footer button.inserir-imovel').html(texto_acao);
		$(this).find('.modal-header h4.modal-title').html(texto_acao);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/imovel/novo',
			data: 'alienacao_token='+alienacao_token,
			beforeSend: function() {
				$('div#novo-imovel .modal-body div.carregando').show();
				$('div#novo-imovel .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#novo-imovel .modal-body div.form form').html(retorno);
				$('div#novo-imovel .modal-body div.carregando').hide();
				$('div#novo-imovel .modal-body div.form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#novo-imovel').on('hidden.bs.modal', function (event) {
		$('div#novo-imovel .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-imovel .modal-body div.form form').html('');
	});
	$('div#novo-imovel').on('click','button.inserir-imovel',function(e) {
		var erros = new Array();
		var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');

		var matricula_imovel = form.find('input[name=matricula_imovel]').val();
		var no_endereco = form.find('input[name=no_endereco]').val();
		/*var registro_imovel = form.find('input[name=registro_imovel]').val();
		var nu_endereco = form.find('input[name=nu_endereco]').val();
		var no_complemento = form.find('input[name=no_complemento]').val();
		var no_bairro = form.find('input[name=no_bairro]').val();
		var id_cidade = form.find('select[name=id_cidade]').val();
		var nome_cidade = form.find('select[name=id_cidade] option:selected').html();
		var no_bairro = form.find('input[name=no_bairro]').val();
		var nu_cep = form.find('input[name=nu_cep]').val();*/
		
		var alienacao_token = form.find('input[name=alienacao_token]').val();

		if (matricula_imovel=='') {
			erros.push('O campo matrícula do imóvel é obrigatório');
		}
		if (no_endereco=='') {
			erros.push('O campo endereço do devedor é obrigatório');
		}
		/*if (registro_imovel=='') {
			erros.push('O campo registro do imóvel é obrigatório');
		}
		if (id_cidade=='0') {
			erros.push('O campo cidade é obrigatório');
		}*/

		if (erros.length>0) {
			form.find('div.erros-imovel div').html(erros.join('<br />'));
			if (!form.find('div.erros-imovel').is(':visible')) {
				form.find('div.erros-imovel').slideDown();
			}
		} else {
			if ($('table#enderecos tbody tr.endereco_imovel').length>0) {
				var index_endereco = $('table#enderecos tbody tr.endereco_imovel').prop('id');
			} else {
				var index_endereco = total_enderecos;
			}
			
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/imovel/inserir',
				data: form.serialize()+'&index_endereco='+index_endereco,
				beforeSend: function() {
					$('div#novo-credor .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							imovel = retorno.imovel;
							swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
								$('div#nova-alienacao div#matricula_imovel').find('button.inserir-imovel').removeClass('btn-success').addClass('btn-primary').data('acao','A').html('Alterar imóvel');
								$('div#nova-alienacao div#matricula_imovel').find('button.remover-imovel').show();
								$('div#nova-alienacao div#matricula_imovel').find('input[name=matricula_imovel]').val(imovel.matricula_imovel);
								$('div#novo-imovel').modal('hide');
								
								if ($('table#enderecos tbody tr.endereco_imovel').length>0) {
									$('table#enderecos tbody tr.endereco_imovel td.endereco').html(imovel.no_endereco);
								} else {
									var nova_linha = $('<tr id="'+total_enderecos+'" class="endereco_imovel">');
									var colunas = "";
									colunas += '<td class="endereco text-uppercase">';
										colunas += imovel.no_endereco;
									colunas += '</td>';
									colunas += '<td>';
										colunas += '<a href="#" class="disabled btn btn-black btn-sm" data-linha="'+total_enderecos+'" data-alienacaotoken="'+alienacao_token+'">Remover</a>';
									colunas += '</td>';
	
									nova_linha.append(colunas);
									$("table#enderecos tbody").append(nova_linha);
									total_enderecos++;
								}
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
	$('div#nova-alienacao').on('click','div#matricula_imovel button.remover-imovel',function() {
		var alienacao_token = $(this).data('alienacaotoken');
		var index_endereco = $('table#enderecos tbody tr.endereco_imovel').prop('id');
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/imovel/remover',
			data: 'alienacao_token='+alienacao_token+'&index_endereco='+index_endereco,
			beforeSend: function() {
				$('div#novo-imovel .modal-body div.carregando').show();
				$('div#novo-imovel .modal-body div.form').hide();
			},
			success: function() {
				$('div#nova-alienacao div#matricula_imovel').find('button.inserir-imovel').removeClass('btn-primary').addClass('btn-success').data('acao','N').html('Inserir imóvel');
				$('div#nova-alienacao div#matricula_imovel').find('button.remover-imovel').hide();
				$('div#nova-alienacao div#matricula_imovel').find('input[name=matricula_imovel]').val('');
				$('table#enderecos tr#'+index_endereco).remove();
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#detalhes-imovel').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var matricula_imovel = $(event.relatedTarget).data('matriculaimovel');
		
		$(this).find('.modal-header h4.modal-title span').html(matricula_imovel);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/imovel/detalhes',
				data: 'id_alienacao='+id_alienacao,
				beforeSend: function() {
					$('div#detalhes-imovel .modal-body div.carregando').show();
					$('div#detalhes-imovel .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-imovel .modal-body div.form').html(retorno);
					$('div#detalhes-imovel .modal-body div.carregando').hide();
					$('div#detalhes-imovel .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});