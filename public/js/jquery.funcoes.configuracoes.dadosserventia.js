$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	form = $('form[name=form-dados-serventia]');

	form.on('change','select[name=id_estado_serventia]',function(e) {
		id_estado = $(this).val();

        if (id_estado>0) {

            $.ajax({
                type: "POST",
                url: '../listarCidades',
                data: 'id_estado=' + id_estado,
                beforeSend: function () {
                    form.find('select[name=id_cidade_serventia]').prop('disabled', true).html('<option value="0">Carregando...</option>');
                },
                success: function (cidades) {
                    form.find('select[name=id_cidade_serventia]').html('<option value="0">Selecione uma cidade</option>');
                    if (cidades.length > 0) {
                        $.each(cidades, function (key, cidade) {
                            form.find('select[name=id_cidade_serventia]').append('<option value="' + cidade.id_cidade + '">' + cidade.no_cidade + '</option>');
                        });
                        form.find('select[name=id_cidade_serventia]').prop('disabled', false);
                    } else {
                        form.find('select[name=id_cidade_serventia]').prop('disabled', true);
                    }
                },
                error: function (request, status, error) {
                    //console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                        location.reload();
                    });
                }
            });
        } else {
            form.find('select[name=id_cidade_serventia]').prop('disabled', true);
            form.find('select[name=id_cidade_serventia]').val(0);
		}
	});

	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('select[name=id_tipo_telefone_serventia]').val()=='0'){
			erros.push('O campo tipo de telefone é obrigatório.');
		}
		if (form.find('input[name=nu_ddd_serventia]').val()==''){
			erros.push('O campo DDD é obrigatório.');
		}
		if (form.find('input[name=nu_telefone_serventia]').val()==''){
			erros.push('O campo número do telefone é obrigatório.');
		}
		if (form.find('input[name=email_pessoa_serventia]').val()==''){
			erros.push('O campo email é obrigatório.');
		}
		if (form.find('input[name=no_serventia]').val()==''){
			erros.push('O campo nome da serventia é obrigatório.');
		}
		if (form.find('input[name=no_oficial]').val()==''){
			erros.push('O campo nome do oficial é obrigatório.');
		}
		if (form.find('select[name=id_estado_serventia]').val()=='0') {
			erros.push('O campo estado é obrigatório.');
		}
		if (form.find('select[name=id_cidade_serventia]').val()=='0') {
			erros.push('O campo cidade é obrigatório.');
		}
		if (form.find('input[name=nu_cep_serventia]').val()=='') {
			erros.push('O campo CEP é obrigatório.');
		}
		if (form.find('input[name=no_endereco_serventia]').val()=='') {
			erros.push('O campo endereço é obrigatório.');
		}
		if(form.find('input[name=no_bairro_serventia]').val()==''){
			erros.push('O campo bairro é obrigatório.');
		}
		if(form.find('input[name=nu_endereco_serventia]').val()==''){
			erros.push('O campo número é obrigatório.');
		}
		if (isNaN(form.find('input[name=dv_codigo_cns]').val())){
			erros.push('O campo DV CNS aceita apenas números.');
		}

		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
			return false;
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base + 'configuracoes/dados-serventia/salvar',
				data: form.serialize(),
				beforeSend: function() {
					$('div.configuracoes .content div.carregando').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div.configuracoes .content div.carregando').hide();
					
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						swal("Sucesso!", "Dados da serventia salvos com sucesso.", "success").then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});