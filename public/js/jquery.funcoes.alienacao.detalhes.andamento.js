var total_arquivos = 0;
var total_arquivos_assinaveis = 0;
var index_arquivos = 0;
$(document).ready(function() {
	
	$('div#detalhes-andamento').on('submit','form[name=form-novo-andamento]',function(e) {
        e.preventDefault();
        var form = $(this);
        var id_alienacao = form.find('input[name=id_alienacao]').val();
        var id_andamento_alienacao = form.find('input[name=id_andamento_alienacao]').val();
        var id_acao_etapa = form.find('input[name=id_acao_etapa]').val();
        var origem_arquivo = form.find('input[name=origem_arquivo]').val();
        var andamento_detalhe_token = form.find('input[name=andamento_detalhe_token]').val();

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/detalhes-andamento/inserir-arquivos',
            data: {'id_alienacao':id_alienacao,'id_acao_etapa':id_acao_etapa,'id_andamento_alienacao':id_andamento_alienacao,'origem_arquivo':origem_arquivo,'andamento_token':andamento_detalhe_token},
            beforeSend: function() {
                $('div#nova-resposta .modal-body>div.carregando').addClass('flutuante').show();
            },
            success: function(retorno) {
                switch (retorno.status)	{
                    case 'erro':
                        var alerta = swal("Erro!", retorno.msg, "error");
                    break;
                	case 'sucesso':
                        var alerta = swal("Sucesso!",retorno.msg,"success");
                    break;
                }
                if (retorno.recarrega=='true') {
                    alerta.then(function(){
                        location.reload();
                    });
                }
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

	$('div#detalhes-andamento').on('show.bs.modal', function (event) {
		var id_andamento_alienacao = $(event.relatedTarget).data('idandamento');
		var no_fase = $(event.relatedTarget).data('nofase');
		var no_etapa = $(event.relatedTarget).data('noetapa');
		var no_acao = $(event.relatedTarget).data('noacao');
		
		$(this).find('.modal-header h4.modal-title span').html(no_fase+' <i class="glyphicon glyphicon-chevron-right size" style ="font-size: 14px;"></i> '+no_etapa+' <i class="glyphicon glyphicon-chevron-right" style ="font-size: 14px;"></i> '+no_acao);
		
		if (id_andamento_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/detalhes-andamento',
				data: 'id_andamento_alienacao='+id_andamento_alienacao,
				beforeSend: function() {
					$('div#detalhes-andamento .modal-body div.carregando').show();
					$('div#detalhes-andamento .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-andamento .modal-body div.form').html(retorno);
					$('div#detalhes-andamento .modal-body div.carregando').hide();
					$('div#detalhes-andamento .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});