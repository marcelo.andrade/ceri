total_arquivos = 0;
total_arquivos_assinaveis = 0;
index_arquivos = 0;
$(document).ready(function() {
	$('div#nova-resposta').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'pesquisa-eletronica/nova-resposta',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10551", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#nova-resposta').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});
	
	$('div#nova-resposta').on('submit','form[name=form-novo-resultado]',function(e) {
		e.preventDefault();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=in_positivo]:checked').length<=0) {
			erros.push('O campo tipo de resultado é obrigatório');
		} else if (form.find('input[name=in_positivo]:checked').val()=='S') {
			if (form.find('input[name=tipo_resultado_envio]:checked').length<=0) {
				erros.push('O tipo de envio do resultado é obrigatório');
			} else {
                if (form.find('input[name=tipo_resultado_envio]:checked').val()=='1' && form.find('input.a_codigo_matricula').length<=0 && form.find('input.a_codigo_transcricao').length<=0) {
					erros.push('É necessário inserir ao menos uma matrícula e/ou transcrição');
				} else if (form.find('input[name=tipo_resultado_envio]:checked').val()=='2' && form.find('div#arquivos-resultado div.arquivo').length<=0) {
					erros.push('O campo arquivo do resultado é obrigatório');
				}
			}
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-resultado div').html(erros.join('<br />'));
			if (!form.find('div.erros-resultado').is(':visible')) {
				form.find('div.erros-resultado').slideDown();
			}
		} else {
			form.find('div.erros-resultado').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: 'pesquisa-eletronica/inserir-resultado',
				data: data,
				contentType: false,
				processData: false,
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10552", "error");
				}
			});
		}
	});

	$('div#nova-resposta').on('click','input[name=in_positivo]',function(e) {
		var form = $(this).closest('form');

		form.find('input[name=tipo_resultado_envio]').prop('checked',false);
		form.find('input[name=codigo_matricula]').prop('disabled',true);
		form.find('button.incluir-matricula').prop('disabled',true).addClass('disabled');
		form.find('span#matriculas').html('');
		form.find('div#arquivos-resultado button').prop('disabled',true).addClass('disabled');
        form.find('input[name=codigo_transcricao]').prop('disabled',true);
        form.find('button.incluir-transcricao').prop('disabled',true).addClass('disabled');

		switch ($(this).val()) {
			case 'S':
				if (form.find('div.resultado-mensagem').css('display')=='block') {
					var classe = 'resultado-mensagem';
				} else if (form.find('div.resultado-negativo').css('display')=='block') {
					var classe = 'resultado-negativo';
				}
				form.find('div.'+classe).slideUp('fast',function(e) {
					form.find('div.resultado-positivo').slideDown('fast');
				});
				break;
			case 'N':
				if (form.find('div.resultado-mensagem').css('display')=='block') {
					var classe = 'resultado-mensagem';
				} else if (form.find('div.resultado-positivo').css('display')=='block') {
					var classe = 'resultado-positivo';
				}
				form.find('div.'+classe).slideUp('fast',function(e) {
					form.find('div.resultado-negativo').slideDown('fast');
				});
				break;
		}
	});

	$('div#nova-resposta').on('click','input[name=tipo_resultado_envio]',function(e) {
		var form = $(this).closest('form');

		form.find('input[name=codigo_matricula]').prop('disabled',true);
		form.find('button.incluir-matricula').prop('disabled',true).addClass('disabled');
		form.find('span#matriculas').html('');
		form.find('div#arquivos-resultado button').prop('disabled',true).addClass('disabled');
        form.find('input[name=codigo_transcricao]').prop('disabled',true);
        form.find('button.incluir-transcricao').prop('disabled',true).addClass('disabled');
        form.find('span#transcricoes').html('');

		switch($(this).val()) {
			case '1':
				form.find('input[name=codigo_matricula]').prop('disabled',false);
				form.find('button.incluir-matricula').prop('disabled',false).removeClass('disabled');
                form.find('input[name=codigo_transcricao]').prop('disabled',false);
                form.find('button.incluir-transcricao').prop('disabled',false).removeClass('disabled');
				break;
			case '2':
				form.find('div#arquivos-resultado button').prop('disabled',false).removeClass('disabled');
				break;
		}
	});
	

	$('div#nova-resposta').on('click','button.incluir-matricula',function(e) {
		var erros = new Array();
		var form = $('form[name=form-novo-resultado]');
		var codigo_matricula = form.find('input[name=codigo_matricula]');
		if (codigo_matricula.val().trim()=='') {
			erros.push('O campo matrícula é obrigatório');
		} else if (form.find('input.a_codigo_matricula[value='+codigo_matricula.val()+']').length>0) {
            erros.push('A matrícula digitada já foi inserida');
        }

		if (codigo_matricula.size() > 2) {
			erros.push('O campo matrícula deve conter pelo menos 02 digitos');
		}
		if (erros.length>0) {
			form.find('div.erros-matriculas div').html(erros.join('<br />'));
			if (!form.find('div.erros-matriculas').is(':visible')) {
				form.find('div.erros-matriculas').slideDown();
			}
		} else {
			form.find('div.erros-matriculas').slideUp();
			var btn = "";
			btn += '<div class="btn-group btn-group-xs" id="'+codigo_matricula.val()+'">';
				btn += '<button type="button" class="matricula btn btn-primary btn-sm small">';
					btn += '<input type="hidden" name="a_codigo_matricula[]" class="a_codigo_matricula" value="'+codigo_matricula.val()+'" />';
					btn += codigo_matricula.val();
				btn += '</button> ';
				btn += ' <button type="button" class="remover-matricula btn btn-danger btn-sm small" data-codigomatricula="'+codigo_matricula.val()+'">&times;</a>';
			btn += '</div> ';
	
			form.find('span#matriculas').append(btn);
			
			form.find('input[name=codigo_matricula]').val('');
		}
	});
    $('div#nova-resposta').on('click','span#matriculas button.remover-matricula',function() {
        var codigo_matricula = $(this).data('codigomatricula');
        $('span#matriculas div#'+codigo_matricula).remove();
    });

    $('div#nova-resposta').on('click','button.incluir-transcricao',function(e) {
        var erros = new Array();
        var form = $('form[name=form-novo-resultado]');
        var codigo_transcricao = form.find('input[name=codigo_transcricao]');
        if (codigo_transcricao.val().trim()=='') {
            erros.push('O campo transcrição é obrigatório');
        } else if (form.find('input.a_codigo_transcricao[value='+codigo_transcricao.val()+']').length>0) {
            erros.push('A transcrição digitada já foi inserida');
        }
        if (codigo_transcricao.size() > 2) {
            erros.push('O campo transcrição deve conter pelo menos 02 digitos');
        }
        if (erros.length>0) {
            form.find('div.erros-matriculas div').html(erros.join('<br />'));
            if (!form.find('div.erros-matriculas').is(':visible')) {
                form.find('div.erros-matriculas').slideDown();
            }
        } else {
            form.find('div.erros-matriculas').slideUp();
            var btn = "";
            btn += '<div class="btn-group btn-group-xs" id="'+codigo_transcricao.val()+'">';
                btn += '<button type="button" class="transcricao btn btn-primary btn-sm small">';
                    btn += '<input type="hidden" name="a_codigo_transcricao[]" class="a_codigo_transcricao" value="'+codigo_transcricao.val()+'" />';
                    btn += codigo_transcricao.val();
                btn += '</button> ';
                btn += ' <button type="button" class="remover-transcricao btn btn-danger btn-sm small" data-codigotranscricao="'+codigo_transcricao.val()+'">&times;</a>';
            btn += '</div> ';

            form.find('span#transcricoes').append(btn);

            form.find('input[name=codigo_transcricao]').val('');
        }
    });
	$('div#nova-resposta').on('click','span#transcricoes button.remover-transcricao',function() {
		var codigo_transcricao = $(this).data('codigotranscricao');
		$('span#transcricoes div#'+codigo_transcricao).remove();
	});

	$('div#resultado-pesquisa').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/pesquisa-eletronica/resultado',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10503", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#resultado-pesquisa').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

    $('div#recibo-pesquisa').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_pedido>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/pesquisa-eletronica/gerar-recibo-pesquisa',
                data: {'id_pedido':id_pedido},
                context: this,
                beforeSend: function() {
                    $(this).find('.modal-body div.form').hide();
                    status_modal($(this), true, true, true);
                },
                success: function(retorno) {
                    status_modal($(this), false, false, false);
                    $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
                },
                error: function (request, status, error) {
                    status_modal($(this), false, false, false);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });
    $('div#recibo-pesquisa').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    $('div#detalhes-solicitante').on('show.bs.modal', function (event) {
        var id_usuario = $(event.relatedTarget).data('idusuario');
        var no_usuario = $(event.relatedTarget).data('nousuario');

        $(this).find('.modal-header h4.modal-title span').html(no_usuario);

        if (id_usuario>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/pesquisa-eletronica/detalhes-solicitante',
                data: 'id_usuario='+id_usuario,
                beforeSend: function() {
                    $('div#detalhes-solicitante .modal-body div.carregando').show();
                    $('div#detalhes-solicitante .modal-body div.form').hide();
                },
                success: function(retorno) {
                    $('div#detalhes-solicitante .modal-body div.form').html(retorno);
                    $('div#detalhes-solicitante .modal-body div.carregando').hide();
                    $('div#detalhes-solicitante .modal-body div.form').fadeIn('fast');
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
        }
    });
    
    $('div#protocolar').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        var protocolo = $(event.relatedTarget).data('protocolo');
        var nuprotocololegado = $(event.relatedTarget).data('nuprotocololegado');

        if (nuprotocololegado!='') {
            $('button#protocolo-legado').text('Salvar alterações');
            $('button#protocolo-legado').removeClass('inserir-protocolo');
            $('button#protocolo-legado').addClass('alterar-protocolo');
        } else {
            $('button#protocolo-legado').text('Inserir protocolo');
            $('button#protocolo-legado').removeClass('alterar-protocolo');
            $('button#protocolo-legado').addClass('inserir-protocolo');
        }
        $(this).find('.modal-header h4.modal-title span').html(protocolo);

        $.ajax({
            type: "POST",
            url: url_base+'servicos/pesquisa-eletronica/protocolar',
            data: {'id_pedido':id_pedido},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                status_modal($(this), false, false, false);
            },
            error: function (error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

    $('div#protocolar').on('click','button.inserir-protocolo', function (e) {
        e.preventDefault();
        var erros = new Array();
        var form = $('div#protocolar').find('form[name=form-protocolar]');

        if (form.find('input[name=nu_protocolo_legado]').val()==''){
            erros.push('O campo "Protocolo interno" é obrigatório.')
        }
        if (form.find('input[name=dt_protocolo_legado]').val()==''){
            erros.push('O campo "Data" é obrigatório.')
        }
        if (erros.length>0) {
            form.find('div.erros-protocolo div').html(erros.join('<br />'));
            if (!form.find('div.erros-protocolo').is(':visible')) {
                form.find('div.erros-protocolo').slideDown();
            }
        } else {
            var id_pedido = $('input[name=id_pedido]').val();
            var nu_protocolo_legado = $('input[name=nu_protocolo_legado]').val();
            var dt_protocolo_legado = $('input[name=dt_protocolo_legado]').val();

            $.ajax({
                type: "POST",
                url: url_base+ 'servicos/pesquisa-eletronica/inserir-protocolo',
                data: {'id_pedido':id_pedido, 'nu_protocolo_legado':nu_protocolo_legado, 'dt_protocolo_legado':dt_protocolo_legado},
                context: this,
                beforeSend: function () {
                    $(this).find('.modal-body div .form').hide();
                    status_modal($(this),true, true, true);
                },
                success: function (retorno) {
                    switch (retorno.status){
                        case 'erro':
                            var alerta = swal('Erro', retorno.msg, 'error');
                            break;
                        case 'sucesso':
                            var alerta = swal("Sucesso!",retorno.msg,"success");
                            break;
                    }
                    alerta.then(function () {
                        status_modal($(this), false, false, false);
                        if (retorno.recarrega == 'true'){
                            location.reload();
                        }
                    })
                },
                error: function (error) {
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20553", "error");
                    $(this).hide();
                }
            });
        }
    });

    $('div#protocolar').on('click', 'button.alterar-protocolo', function (event) {
        event.preventDefault();

        var erros = new Array();
        var form = $('div#protocolar').find('form[name=form-protocolar]');

        if (form.find('input[name=nu_protocolo_legado]').val()==''){
            erros.push('O campo "Protocolo interno" é obrigatório.')
        }
        if (form.find('input[name=dt_protocolo_legado]').val()==''){
            erros.push('O campo "Data" é obrigatório.')
        }
        if (erros.length>0) {
            form.find('div.erros-protocolo div').html(erros.join('<br />'));
            if (!form.find('div.erros-protocolo').is(':visible')) {
                form.find('div.erros-protocolo').slideDown();
            }
        } else {
            var id_pedido = $('input[name=id_pedido]').val();
            var nu_protocolo_legado = $('input[name=nu_protocolo_legado]').val();
            var dt_protocolo_legado = $('input[name=dt_protocolo_legado]').val();

            if (id_pedido > 0) {
                swal({
                    title: 'Tem certeza?',
                    type: 'warning',
                    html: 'Tem certeza que deseja alterar os dados do protocolo?',
                    showCancelButton: true,
                    cancelButtonText: 'Não',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim'
                }).then(function (retorno) {
                    if (retorno == true) {
                        $.ajax({
                            type: "POST",
                            url: url_base + 'servicos/pesquisa-eletronica/alterar-protocolo',
                            data: {'id_pedido': id_pedido, 'nu_protocolo_legado': nu_protocolo_legado, 'dt_protocolo_legado': dt_protocolo_legado},
                            success: function (retorno) {
                                console.log(retorno);
                                switch (retorno.status) {
                                    case 'erro':
                                        swal("Erro!", retorno.msg, "error");
                                        break;
                                    case 'sucesso':
                                        swal("Sucesso!", retorno.msg, "success").then(function () {
                                            location.reload();
                                        });
                                        break;
                                }
                            },
                            error: function (request, error) {
                                console.log(request.responseText);
                                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                                    location.reload();
                                });
                            }
                        });
                    }
                });
            }
        }
    });
});