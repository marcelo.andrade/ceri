$(document).ready(function() {
	$('div#notificacoes-alienacao-detalhes').on('show.bs.modal', function (event) {
		var grupo = $(event.relatedTarget).data('grupo');
		var titulo_grupo = $(event.relatedTarget).data('titulogrupo');
		
		$(this).find('.modal-header h4.modal-title').html(titulo_grupo);
		
		if (grupo!='') {
			$.ajax({
				type: "POST",
				url: url_base+'inicio/alertas/detalhes',
				data: 'grupo='+grupo,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.body').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$(this).find('.modal-body div.body').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro INI501", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#notificacoes-alienacao-detalhes').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.body').html('');
	});
});