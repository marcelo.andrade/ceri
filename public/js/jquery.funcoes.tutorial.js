$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /*CADASTRO DE CARTORIO */
    
    $('div#tutotial-cadastro-cartorio').on('show.bs.modal', function (event)
    {
        $('div#tutotial-cadastro-cartorio .modal-body div.carregando').show();
        $("#conteudo-cadastro-cartorio").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqSFFHZzI1NnFuNDQ/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-cadastro-cartorio .modal-body div.carregando').hide();
            $("#conteudo-cadastro-cartorio").show();
        }, 1000);
    });

    $('div#tutotial-cadastro-cartorio').on('hidden.bs.modal', function (event) {
        $("#conteudo-cadastro-cartorio").html('');
    });

    /*CADASTRO DE TOKEN */
    
    $('div#tutotial-acesso-token').on('show.bs.modal', function (event)
    {
        $('div#tutotial-acesso-token .modal-body div.carregando').show();
        $("#conteudo-acesso-token").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqMS1lU3BpcFBzTWc/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-acesso-token .modal-body div.carregando').hide();
            $("#conteudo-acesso-token").show();
        }, 1000);
    });

    $('div#tutotial-acesso-token').on('hidden.bs.modal', function (event) {
        $("#conteudo-acesso-token").html('');
    });

    /*NOTIFICA�OES DE ALIENCA��O FIDUCI�RIA 1*/

    $('div#tutotial-notificacao-alienacao-fiduciaria-1').on('show.bs.modal', function (event)
    {
        $('div#tutotial-notificacao-alienacao-fiduciaria-1 .modal-body div.carregando').show();
        $("#conteudo-notificacao-alienacao-fiduciaria-1").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqZXVia0d6Q2ZMREE/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-notificacao-alienacao-fiduciaria-1 .modal-body div.carregando').hide();
            $("#conteudo-notificacao-alienacao-fiduciaria-1").show();
        }, 1000);
    });

    $('div#tutotial-notificacao-alienacao-fiduciaria-1').on('hidden.bs.modal', function (event) {
        $("#conteudo-notificacao-alienacao-fiduciaria-1").html('');
    });

    /*NOTIFICA�OES DE ALIENCA��O FIDUCI�RIA 2*/

    $('div#tutotial-notificacao-alienacao-fiduciaria-2').on('show.bs.modal', function (event)
    {
        $('div#tutotial-notificacao-alienacao-fiduciaria-2 .modal-body div.carregando').show();
        $("#conteudo-notificacao-alienacao-fiduciaria-2").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqYW1UcnYtV1FFd1k/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-notificacao-alienacao-fiduciaria-2 .modal-body div.carregando').hide();
            $("#conteudo-notificacao-alienacao-fiduciaria-2").show();
        }, 1000);
    });

    $('div#tutotial-notificacao-alienacao-fiduciaria-2').on('hidden.bs.modal', function (event) {
        $("#conteudo-notificacao-alienacao-fiduciaria-2").html('');
    });

    /*CRIAR OF�CIO CART�RIO*/

    $('div#tutotial-criar-oficio-cartorio').on('show.bs.modal', function (event)
    {
        $('div#tutotial-criar-oficio-cartorio .modal-body div.carregando').show();
        $("#conteudo-criar-oficio-cartorio").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqbkRmMHJ2bFFkVnM/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-criar-oficio-cartorio .modal-body div.carregando').hide();
            $("#conteudo-criar-oficio-cartorio").show();
        }, 1000);
    });

    $('div#tutotial-criar-oficio-cartorio').on('hidden.bs.modal', function (event) {
        $("#conteudo-criar-oficio-cartorio").html('');
    });

    /*RESPONDER OF�CIO*/

    $('div#tutotial-responder-oficio').on('show.bs.modal', function (event)
    {
        $('div#tutotial-responder-oficio .modal-body div.carregando').show();
        $("#conteudo-responder-oficio").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqdlJBekstRmtldzQ/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-responder-oficio .modal-body div.carregando').hide();
            $("#conteudo-responder-oficio").show();
        }, 1000);
    });

    $('div#tutotial-responder-oficio').on('hidden.bs.modal', function (event) {
        $("#conteudo-responder-oficio").html('');
    });

    /*RESPONDER PENHORA*/

    $('div#tutotial-responder-penhora').on('show.bs.modal', function (event)
    {
        $('div#tutotial-responder-penhora .modal-body div.carregando').show();
        $("#conteudo-responder-penhora").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqQjduaWNzVEZKYmM/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-responder-penhora .modal-body div.carregando').hide();
            $("#conteudo-responder-penhora").show();
        }, 1000);
    });

    $('div#tutotial-responder-penhora').on('hidden.bs.modal', function (event) {
        $("#conteudo-responder-penhora").html('');
    });

    /*INCLUIR PENHORA JUDICI�RIO*/

    $('div#tutotial-incluir-penhora').on('show.bs.modal', function (event)
    {
        $('div#tutotial-incluir-penhora .modal-body div.carregando').show();
        $("#conteudo-incluir-penhora").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqR0l4SHhJcFpOZzQ/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-incluir-penhora .modal-body div.carregando').hide();
            $("#conteudo-incluir-penhora").show();
        }, 1000);
    });

    $('div#tutotial-incluir-penhora').on('hidden.bs.modal', function (event) {
        $("#conteudo-incluir-penhora").html('');
    });

    /*RESPONDER OF�CIO JUDICI�RIO*/

    $('div#tutotial-responder-oficio-judiciario').on('show.bs.modal', function (event)
    {
        $('div#tutotial-responder-oficio-judiciario .modal-body div.carregando').show();
        $("#conteudo-responder-oficio-judiciario").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqYlMxVXBzYVJySXM/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-responder-oficio-judiciario .modal-body div.carregando').hide();
            $("#conteudo-responder-oficio-judiciario").show();
        }, 1000);
    });

    $('div#tutotial-responder-oficio-judiciario').on('hidden.bs.modal', function (event) {
        $("#conteudo-responder-oficio-judiciario").html('');
    });

    /*CRIAR OF�CIO JUDICI�RIO*/

    $('div#tutotial-criar-oficio-judiciario').on('show.bs.modal', function (event)
    {
        $('div#tutotial-criar-oficio-judiciario .modal-body div.carregando').show();
        $("#conteudo-criar-oficio-judiciario").html('<iframe src="https://drive.google.com/file/d/0Bx-0kITATZjqcGNaVV9uT182T28/preview" width="870" height="490" allowfullscreen  frameborder="0"></iframe>');
        setTimeout(function(){
            $('div#tutotial-criar-oficio-judiciario .modal-body div.carregando').hide();
            $("#conteudo-criar-oficio-judiciario").show();
        }, 1000);
    });

    $('div#tutotial-criar-oficio-judiciario').on('hidden.bs.modal', function (event) {
        $("#conteudo-criar-oficio-judiciario").html('');
    });

});