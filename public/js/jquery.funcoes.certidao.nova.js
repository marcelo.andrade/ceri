var total_arquivos = 0;
var total_arquivos_assinaveis = 0;
var index_arquivos = 0;
var id_pedido_resultado_matricula = '';
var v_id_pedido = '';
$(document).ready(function() {

	$('div#nova-certidao').on('show.bs.modal', function (event) {
		var id_pedido_resultado_matricula = $(event.relatedTarget).data('idpedidoresultadomatricula');
		$.ajax({
			type: "POST",
			url: url_base+'servicos/certidao/nova-certidao',
			data: {'id_pedido_resultado_matricula':id_pedido_resultado_matricula},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20501", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});

    $('div#nova-certidao').on('keyup','textarea[maxlength!=0]',function(e) {
        var limite = $(this).prop('maxlength');

        var alert_obj = $(this).closest('div.form-group').next().find('div.alert');
        alert_obj.find('div.menssagem span.total').html($(this).val().length);
        if ($(this).val().length<limite) {
            alert_obj.removeClass('alert-warning').addClass('alert-info');
            alert_obj.find('i').removeClass('.glyphicon-exclamation-sign').addClass('.glyphicon-info-sign');
            alert_obj.find('div.menssagem span.limite').html('');
        } else {
            alert_obj.removeClass('alert-info').addClass('alert-warning');
            alert_obj.find('i').removeClass('.glyphicon-info-sign').addClass('.glyphicon-exclamation-sign');
            alert_obj.find('div.menssagem span.limite').html('O limite foi atingido.');
        }
    });

	$('div#nova-certidao').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
		id_pedido_resultado_matricula = '';
	});

	$('div#nova-certidao').on('change','select[name=id_serventia]',function(e) {
		calcular_valores_certidao();
	});
	
	$('div#nova-certidao').on('change','select[name=id_produto]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_produto = $(this).find('option:selected').val();
		var total_sub = $(this).find('option:selected').data('totalsub');

		if (id_pedido_resultado_matricula=='') {
			form.find('div#msg-homonimia').hide();
			form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('');
			form.find('input[name=de_chave_certidao]').unmask().val('').prop('disabled',true);
			form.find('input[name=id_tipo_certidao_chave_pesquisa]').prop('checked',false).prop('disabled',true);
			switch ($(this).find('option:selected').data('codigo')) {
				case 2007: case 2008: // Positiva e Negativa de Bens
					form.find('input[name=id_tipo_certidao_chave_pesquisa][value=1],input[name=id_tipo_certidao_chave_pesquisa][value=2]').prop('disabled',false);
                	form.find('div#alert-pesquisa').show();
					break;
				case 2002: case 2006: case 2010: case 2018: // Outros casos
					form.find('input[name=id_tipo_certidao_chave_pesquisa][value=4]').prop('disabled',false);
					form.find('div#alert-pesquisa').hide();
					break;
				default:
					form.find('input[name=id_tipo_certidao_chave_pesquisa]').prop('checked',false).prop('disabled',false);
                    form.find('div#alert-pesquisa').hide();
					break;
			}
		}
		if ($(this).val()!='0') {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/produto-item/listar',
				data: {'id_produto':id_produto},
				beforeSend: function() {
					status_modal(obj_modal, true, true, true);
				},
				success: function(produto_itens) {
					status_modal(obj_modal, false, false, false);

					form.find('select[name=id_produto_item]').html('<option value="0">Selecione</option>');
					if (produto_itens.length>0) {
						var id_produto_item = 0;
						$.each(produto_itens,function(key,produto_item) {
							form.find('select[name=id_produto_item]').append('<option value="'+produto_item.id_produto_item+'" '+(total_sub==1?'selected="selected"':'')+'>'+produto_item.no_produto_item+'</option>');
							if (total_sub==1) {
								id_produto_item = produto_item.id_produto_item;
							}
						});
						form.find('select[name=id_produto_item]').prop('disabled',false);
						if (total_sub>1) {
							form.find('div#id_produto_item').slideDown();
						} else {
							form.find('div#id_produto_item').slideUp();
							calcular_valores_certidao(id_produto_item);
						}
					} else {
						form.find('select[name=id_produto_item]').html('').prop('disabled',true);
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20502", "error");
				}
			});
		} else {
			form.find('select[name=id_produto_item]').html('').prop('disabled',true);
			calcular_valores_certidao();
		}
	});

	$('div#nova-certidao').on('change','select[name=id_cidade]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_cidade = $(this).val();

		if (id_cidade>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/listarServentias',
				data: {'id_cidade':id_cidade},
				beforeSend: function() {
					status_modal(obj_modal, true, true, true);
				},
				success: function(serventias) {
					status_modal(obj_modal, false, false, false);

					form.find('select[name=id_serventia]').html('<option value="0">Selecione uma serventia</option>');
					if (serventias.length>0) {
						$.each(serventias,function(key,serventia) {
							form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
						});
						form.find('select[name=id_serventia]').prop('disabled',false);
					} else {
						form.find('select[name=id_serventia]').prop('disabled',true);
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
				}
			});
		} else {
			form.find('select[name=id_serventia]').val(0).trigger('change').prop('disabled',true);
		}
	});

	$('div#nova-certidao').on('click','div.radio input[name=id_tipo_custa]',function(e) {
		var form = $(this).closest('form');

		form.find('div#numero-processo').hide();
		form.find('div#documento-isencao').hide();
		form.find('div#msg-isencao').hide();
		form.find('div#assinaturas-arquivos').hide();
		switch ($(this).val()) {
			case '1':
				form.find('div#numero-processo').show();
				form.find('div#certidao-valores').slideUp();
				break;
			case '2':
				calcular_valores_certidao();
				break;
			case '3':
				swal({
					title: 'Atenção?',
					text: "Houve despacho de deferimento de gratuidade emitido pela AJG?",
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'Não',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim',
					showLoaderOnConfirm: true,
				}).then(function( retorno ) {
					if (retorno == true) {
						form.find('div#documento-isencao').show();
						form.find('div#assinaturas-arquivos').show();
						form.find('div#certidao-valores').slideUp();
					}
				}).catch(function(e) {
					form.find('div#msg-isencao').show();
					form.find('input[name=id_tipo_custa]').prop('checked', false);
					calcular_valores_certidao();
				});
				
				break;
		}
	});
	
	$('div#nova-certidao').on('click','input[name=id_tipo_certidao_chave_pesquisa]',function(e) {
		var form = $(this).closest('form');

		if (id_pedido_resultado_matricula=='') {
			form.find('input[name=de_chave_certidao]').val('').prop('disabled',false);
			switch ($(this).val()) {
				case '1':
					form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(CPF)');
					form.find('input[name=de_chave_certidao]').mask('000.000.000-00').removeClass('somente-texto');
                    form.find('input[name=de_chave_certidao]').autoNumeric('destroy');
					form.find('div#msg-homonimia').hide();

                    form.find('input[name=de_chave_complementar]').val('').prop('disabled',false).prop('readonly', false).addClass('somente-texto');
                    form.find('input[name=de_chave_complementar]').parent('div').find('label').html('Titular do CPF');
                    form.find('div#chave-complementar').show();
					break;
				case '2':
					form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(CNPJ)');
					form.find('input[name=de_chave_certidao]').mask('00.000.000/0000-00').removeClass('somente-texto');
                    form.find('input[name=de_chave_certidao]').autoNumeric('destroy');
					form.find('div#msg-homonimia').hide();

                    form.find('input[name=de_chave_complementar]').val('').prop('disabled',false).prop('readonly', false).removeClass('somente-texto');
                    form.find('input[name=de_chave_complementar]').parent('div').find('label').html('Razão social do CNPJ');
                    form.find('div#chave-complementar').show();
					break;
				case '3':
					form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(Nome)');
					form.find('input[name=de_chave_certidao]').unmask().addClass('somente-texto');
                    form.find('input[name=de_chave_certidao]').autoNumeric('destroy');
					form.find('div#msg-homonimia').show();

                    form.find('input[name=de_chave_complementar]').val('').prop('disabled',true).removeClass('somente-texto');
                    form.find('div#chave-complementar').hide();
					break;
				case '4':
					form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(Matrícula)');
					form.find('input[name=de_chave_certidao]').unmask().removeClass('somente-texto');
                    form.find('input[name=de_chave_certidao]').autoNumeric('init', {vMin:'0', mDec:'0',aSep:''});
					form.find('div#msg-homonimia').hide();

                    form.find('input[name=de_chave_complementar]').val('').prop('disabled',true).removeClass('somente-texto');
                    form.find('div#chave-complementar').hide();
					break;
			}
		}
	});
	
	$('div#nova-certidao').on('click','input[name=id_periodo_certidao]',function(e) {
		var form = $(this).closest('form');

		if ($(this).data('tipo')=='2') {
			form.find('.periodo input').prop('disabled',false);
		} else {
			form.find('.periodo input').val('').prop('disabled',true);
		}
	});

	$('div#nova-certidao').on('submit','form[name=form-nova-certidao]',function(e) {
		e.preventDefault();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('select[name=id_serventia]').val()=='0') {
			erros.push('O campo cartório é obrigatório.');
		}

		if (form.find('select[name=id_produto]').val()=='0') {
			erros.push('O campo tipo de certidão é obrigatório.');
		} else {
			if (form.find('select[name=id_produto] option:selected').data('totalsub')>1 && form.find('select[name=id_produto_item]').val()=='0') {
				erros.push('O campo item da certidão é obrigatório.');
			}
		}
		if (form.find('input[name=de_chave_certidao]').is(':disabled') || form.find('input[name=de_chave_certidao]').val()=='') {
			erros.push('O campo pesquisa é obrigatório.');
		} else {
			switch (form.find('input[name=id_tipo_certidao_chave_pesquisa]:checked').val()) {
				case '1':
					if (!validarCPF(form.find('input[name=de_chave_certidao]').val())) {
						erros.push('O CPF digitado é inválido');
					}
					break;
				case '2':
					if (!validarCNPJ(form.find('input[name=de_chave_certidao]').val())) {
						erros.push('O CNPJ digitado é inválido');
					}
					break;
			}
		}
        if (form.find('input[name=id_tipo_certidao_chave_pesquisa]:checked').length>0) {
            switch (form.find('input[name=id_tipo_certidao_chave_pesquisa]:checked').val()) {
                case '1':
                    if (form.find('input[name=de_chave_complementar]').val().trim()=='') {
                        erros.push('O campo titular do CPF é obrigatório.');
                    }
                    break;
                case '2':
                    if (form.find('input[name=de_chave_complementar]').val()=='') {
                        erros.push('O campo razão social do CNPJ é obrigatório.');
                    }
                    break;
            }
        }
		if (form.find('input[name=id_tipo_custa]').length>0) {
			if (form.find('input[name=id_tipo_custa]:checked').length<=0) {
				erros.push('O campo tipo de custa é obrigatório.');
			} else {
				if (form.find('input[name=id_tipo_custa]:checked').val()==3 && form.find('div#arquivos-isencao div.arquivo').length<=0) {
					erros.push('O arquivo documento de isenção de emolumentos é obrigatório.');
				}
				if (form.find('input[name=id_tipo_custa]:checked').val()==1 && form.find('input[name=numero_processo]').val()=='') {
					erros.push('O número do processo é obrigatório.');
				}
			}
		}

		if (total_arquivos_assinaveis>0) {
			erros.push('É obrigatório assinar os arquivos inseridos');
		}

		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/certidao/inserir-certidao',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal({title: 'Sucesso!',html: retorno.msg,type: 'success'});

							$.each(retorno.certidoes,function(key,certidao) {
								var tipo_certidao = form.find('select[name=id_produto] option:selected').html();

								var data = new Date();
								var nova_linha = $('<tr id="'+certidao.pedido.id_pedido+'">');
								var nova_linha_externa = $('<tr id="'+certidao.pedido.id_pedido+'">');
								var colunas = "";
								colunas += '<td><input type="hidden" name="id_pedido[]" class="id_pedido" value="'+certidao.pedido.id_pedido+'" />'+certidao.pedido.protocolo_pedido+'</td>';
								colunas += '<td>'+certidao.dt_formatada+'</td>';
								colunas += '<td>'+tipo_certidao+'</td>';
								colunas += '<td>'+certidao.no_pessoa+'</td>';
								colunas += '<td>'+certidao.dt_validade+'</td>';
								colunas += '<td class="valor"><input type="hidden" name="va_pedido[]" class="va_pedido" value="'+certidao.vl_pedido+'" /><span class="real">'+certidao.vl_pedido+'</span></td>';
								colunas += '<td class="options">';
									colunas += '<div class="btn-group" role="group">';
	                                    colunas += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detalhes-certidao" data-idpedido="'+certidao.pedido.id_pedido+'" data-protocolo="'+certidao.pedido.protocolo_pedido+'">Detalhes</button>';
	                                    colunas += '<div class="btn-group" role="group">';
	                                        colunas += '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	                                            colunas += '<span class="caret"></span>';
	                                        colunas += '</button>';
	                                        colunas += '<ul class="dropdown-menu">';
	                                            colunas += '<li><a href="#" class="enviar-pedido" data-idpedido="'+certidao.pedido.id_pedido+'" data-protocolo="'+certidao.pedido.protocolo_pedido+'" data-vapedido="'+certidao.vl_pedido+'">Enviar pedido</a></li>';
	                                            colunas += '<li><a href="#" class="cancelar-pedido" data-idpedido="'+certidao.pedido.id_pedido+'" data-protocolo="'+certidao.pedido.protocolo_pedido+'" data-acao="C">Cancelar pedido</a></li>';
	                                        colunas += '</ul>';
	                                    colunas += '</div>';
	                                colunas += '</div>';
								colunas += '</td>';
								
								nova_linha.append(colunas);
								nova_linha_externa.append(colunas);
								$("table#novos-pedidos").append(nova_linha);
								$("table#pedidos-pendentes").prepend(nova_linha_externa);
							});

							$("div#pedidos-pendentes").fadeIn('fast');
							$('div#certidao-valores').hide();

							if (id_pedido_resultado_matricula=='') {
								form.find('input[type=reset]').trigger('click');
								form.find('select[name=id_serventia]').prop('disabled',true);
								form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('');
								form.find('input[name=de_chave_certidao]').prop('disabled',true);
								form.find('.periodo input').prop('disabled',true);
							} else {
								form.find('select[name=id_produto]').val(0);
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							window.location=url_base+'/servicos/certidao';
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20503", "error");
				}
			});
		}
	});
	$('div#nova-certidao').on('reset','form[name=form-nova-certidao]',function(e) {
		var form = $(this).closest('form');

        form.find('select[name=id_serventia]').prop('disabled',true);
        form.find('select[name=id_produto_item]').html('').prop('disabled',true);

		form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('');

		form.find('div#certidao-valores').hide();
		form.find('div#documento-isencao').hide();
		form.find('div#arquivos-isencao div.arquivo').remove();
		form.find('div#msg-isencao').hide();

		form.find('div#assinaturas-arquivos div.menssagem span').html('Nenhum arquivo foi inserido.');
		form.find('div#assinaturas-arquivos div.menssagem a').addClass('disabled');

		total_arquivos = 0;
		total_arquivos_assinaveis = 0;
		index_arquivos = 0;

		if(id_pedido_resultado_matricula ===0) {
            id_pedido_resultado_matricula = '';
            form.find('input[name=de_chave_certidao]').val('').unmask().prop('disabled',true);
		}
	});

	$('table#pedidos-pendentes, div#nova-certidao').on('click','a.enviar-pedido, table#novos-pedidos a.enviar-pedido',function(e) {
		e.preventDefault();
        var form = $('form[name=form-nova-certidao]');
        var form_ciencia = $('form[name=form-ciencia]');
        var id_compra_credito = $('input[name=id_compra_credito]').val();

        if (form.find('input[name=in_ciencia_certidao_nova]').is(':checked') || form_ciencia.find('input[name=in_ciencia_certidao]').is(':checked')) {
            var id_pedido = $(this).data('idpedido');
            var protocolo_pedido = $(this).data('protocolo');
            var va_pedido = $(this).data('vapedido');
            var obj_modal = $(this).closest('.modal');

            status_modal(obj_modal, true, true, true);

            if (id_pedido > 0) {
                pedidos_selecionados = {'id_pedido':id_pedido,'id_compra_credito':id_compra_credito};
                valor_total = parseFloat(va_pedido);
                valor_total_string = valor_total.toFixed(2).toString().replace('.',',');
                msg = 'Tem certeza que deseja enviar o pedido ' + protocolo_pedido + ' no valor total de R$ ' + valor_total_string + '?';

                enviar_pedidos(pedidos_selecionados, valor_total, msg, obj_modal);
            }
        } else {
            swal("Ops!", "Você precisa declarar (abaixo) que está ciente que os dados informados estajam corretos para a geração da certidão.", "warning");
		}
	});
	
	$('div#nova-certidao').on('click','button.enviar-pedidos',function(e) {
		var form = $('form[name=form-nova-certidao]');
        var id_compra_credito = $('input[name=id_compra_credito]').val();

		if (form.find('input[name=in_ciencia_certidao_nova]').is(':checked')) {
            if ($('table#novos-pedidos input.id_pedido').length<=0) {
                swal("Ops!", "Você ainda não inseriu pedidos.", "warning");
            } else {
                var pedidos_selecionados = $('table#novos-pedidos input.id_pedido').serialize() + '&id_compra_credito=' + id_compra_credito;
                var total_pedidos = $('table#novos-pedidos input.id_pedido').length;
                var valor_total = 0;
                var obj_modal = $(this).closest('.modal');

                status_modal(obj_modal, true, true, true);

                $("table#novos-pedidos input.va_pedido").each(function(index, element) {
                    valor_total += parseFloat($(this).val());
                });
                if (valor_total>0) {
                    valor_total_string = valor_total.toFixed(2).toString().replace('.',',');
                    texto_valor_total = ' no valor total de R$ '+valor_total_string+'?';
                } else {
                    texto_valor_total = '?';
                }

                msg = (total_pedidos>1?'Tem certeza que deseja enviar os '+total_pedidos+' pedidos'+texto_valor_total:'Tem certeza que deseja enviar o pedido'+texto_valor_total)

                enviar_pedidos(pedidos_selecionados,valor_total,msg,obj_modal);
            }
		} else {
            swal("Ops!", "Você precisa declarar (abaixo) que está ciente que os dados informados estajam corretos para a geração da certidão.", "warning");
		}
	});
	
	$('table#pedidos-pendentes, table#pedidos-confirmar, div#nova-certidao').on('click','a.cancelar-pedido, table#novos-pedidos a.cancelar-pedido',function(e) {
		e.preventDefault();
		var id_pedido = $(this).data('idpedido');
		var protocolo_pedido = $(this).data('protocolo');
		var obj_modal = $(this).closest('.modal');

		if (id_pedido>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja cancelar o pedido nº '+protocolo_pedido,
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
                    status_modal(obj_modal, true, true, true);
					$.ajax({
						type: "POST",
						url: url_base+'servicos/certidao/cancelar-pedido',
						data: {'id_pedido':id_pedido},
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									var alerta = swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									var alerta = swal("Sucesso!",retorno.msg,"success");
									break;
								case 'alerta':
									var alerta = swal("Ops!",retorno.msg,"warning");
									break;
							}
							alerta.then(function(){
								status_modal(obj_modal, false, false, false);
								if (retorno.recarrega=='true') {
                                    if($('div#nova-certidao').length>0) {
                                        $('table#pedidos-pendentes').find('tr#'+id_pedido).remove();
                                        $('table#novos-pedidos').find('tr#'+id_pedido).remove();
                                        
										if($('table#pedidos-pendentes tr').length==1) {
                                        	$('div#pedidos-pendentes').css('display','none');
										}
                                    } else {
                                        window.location=url_base+'servicos/certidao';
                                    }
								}
							});
						},
						error: function (request, status, error) {
							status_modal(obj_modal, false, false, false);
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20506", "error");
						}
					});
				}
			});
		}
	});

    $('div#nova-certidao').on('blur', 'input[name=de_chave_certidao]', function (e) {
        var form = $(this).closest('form');
        var nu_cpf_cnpj = $(this).val();
        var val = form.find('input[name=id_tipo_certidao_chave_pesquisa]:checked').val();

        if ((val == 1 || val == 2) && nu_cpf_cnpj !='') {
            $.ajax({
                type: "POST",
                url: url_base+'listarPessoa',
                data: {'nu_cpf_cnpj':nu_cpf_cnpj},
                success: function (dadosPessoa) {
                    if(typeof dadosPessoa.pessoa !== 'undefined'){
                    	form.find('input[name=de_chave_complementar]').val(dadosPessoa.pessoa.no_pessoa).prop('readonly', true);
                    }else{
                        form.find('input[name=de_chave_complementar]').val('').prop('readonly', false);
					}
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
        }
    });
});
function atualizar_valores(pedidos_selecionados) {
	$.ajax({
		type: "POST",
		url: url_base+'servicos/certidao/atualizar-valores',
		data: pedidos_selecionados,
		success: function(retorno) {
			switch (retorno.status) {
				case 'erro':
					var alerta = swal("Erro!",retorno.msg,"error");
					break;
				case 'sucesso':
					var alerta = swal("Sucesso!",retorno.msg,"success");

					$.each(retorno.pedidos,function(key,pedido) {
						$('table#pedidos-pendentes, table#novos-pedidos').find('tr#'+pedido.id_pedido+' td.valor span').autoNumeric('set',pedido.va_pedido);
						$('table#pedidos-pendentes, table#novos-pedidos').find('tr#'+pedido.id_pedido+' td.options a.enviar-pedido').data('vapedido',pedido.va_pedido);
						$('table#novos-pedidos').find('tr#'+pedido.id_pedido+' td.valor input.va_pedido').val(pedido.va_pedido);
					});
					break;
				case 'alerta':
					var alerta = swal("Ops!",retorno.msg,"warning");
					break;
			}
		},
		error: function (request, status, error) {
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20508", "error");
		}
	});
}
function calcular_valores_certidao(id_produto_item=0) {
	var form = $('form[name=form-nova-certidao]');
	var obj_modal = form.closest('.modal');
    var id_pedido_resultado_matricula = form.find('input[name=id_pedido_resultado_matricula]').val();
    var certidao_token = form.find('input[name=certidao_token]').val();
    var va_desconto = 0;

	if (form.find('input[name=id_tipo_custa]').length<=0 || form.find('input[name=id_tipo_custa]:checked').val()==2) {
		if (id_produto_item==0) {
			var id_produto_item = form.find('select[name=id_produto_item]').val();
		}
		var id_serventia = $('form[name=form-nova-certidao] select[name=id_serventia]').val();

		if (id_produto_item>0 && id_serventia>0) {
			var ids_serventia = [];
			ids_serventia.push(id_serventia);
			$.ajax({
				type: "POST",
				url: url_base+'servicos/produto-item/calcular',
				data: {'id_produto_item':id_produto_item, 'ids_serventias':ids_serventia, 'id_pedido_resultado_matricula':id_pedido_resultado_matricula, 'certidao_token':certidao_token},
				beforeSend: function() {
					status_modal(obj_modal, true, true, true);
				},
				success: function(valores) {
					status_modal(obj_modal, false, false, false);

					if (valores.length>0) {
						var valor_total = 0;
						if (!form.find('div#certidao-valores').is(':visible')) {
							form.find('div#certidao-valores').slideDown();
						}
						form.find('div#certidao-valores table tbody').html('');

						$.each(valores,function(key,valor) {
							var nova_linha = $("<tr>");

							if(valor.descricao=='Desconto de Pesquisa') {
                                va_desconto = valor.va_preco;
							}
							if (valor.descricao=='Total') {
								colunas = '<th>'+valor.descricao+'</th>';
								colunas += '<th class="real text-right">'+parseFloat(valor.va_preco - va_desconto)+'</th>';
							} else {
								colunas = '<td>'+valor.descricao+'</td>';
								colunas += '<td class="real text-right">'+parseFloat(valor.va_preco)+'</td>';

								valor_total += parseFloat(valor.va_preco);
							}

							nova_linha.append(colunas);

							form.find('div#certidao-valores table tbody').append(nova_linha);
						});

						var total_serventias = form.find('div#serventias-selecionadas div.serventia').length;

						if (total_serventias>0) {
							var nova_linha_total = $("<tr>");

							colunas = '<th>Total para '+total_serventias+' '+(total_serventias>1?'serventias':'serventia')+'</th>';
							//colunas += '<th class="real text-right">'+(valor_total*total_serventias)+'</th>';
							colunas += '<th class="real text-right">'+(valor_total - va_desconto)+'</th>';

							nova_linha_total.append(colunas);

							form.find('div#certidao-valores table tbody').append(nova_linha_total);
						}
					}else{
                        form.find('div#certidao-valores table tbody').html('');
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro VAL501", "error");
				}
			});
		} else {
			if (form.find('div#certidao-valores').is(':visible')) {
				form.find('div#certidao-valores').slideUp();
			}
			$("div#certidao-valores table tbody").html('');
		}
	}
}
function enviar_pedidos(pedidos_selecionados,valor_total,msg,obj_modal) {
	swal({
		title: 'Tem certeza?',
		text: msg,
		type: 'warning',
		showCancelButton: true,
		cancelButtonText: 'Não',
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sim'
	}).then(function(retorno) {
		if (retorno==true) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/certidao/enviar-pedidos',
				data: pedidos_selecionados,
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
						case 'alerta_valor':
							var alerta = swal({
								title: 'Ops!',
								text: retorno.msg,
								type: 'warning',
								showCancelButton: true,
								cancelButtonText: 'Não',
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: 'Sim'
							}).then(function(confirm) {
								if (confirm == true) {
									switch (retorno.tipo) {
										case 'saldo_insuficiente':
											valor_compra = valor_total-parseFloat($("input[name=saldo_atual]").val());
											v_id_pedido = retorno.v_id_pedido;
											$("#nova-compra").modal('show');
											$("#nova-compra").find('form[name=form-cartao-credito]').find('input[name=v_id_pedido]').val(retorno.v_id_pedido);
											break;
										case 'valor_diferente':
											atualizar_valores(pedidos_selecionados);
											break;
									}
								}
							}).catch(function(e) {
								status_modal(obj_modal, false, false, false);
							});
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							if (id_pedido_resultado_matricula>0) {
                            	$('#nova-certidao').modal('hide');
                        	} else {
								window.location=url_base+'servicos/certidao';
                        	}
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20507", "error");
				}
			});
		}
	}).catch(function(e) {
		status_modal(obj_modal, false, false, false);
	});
}