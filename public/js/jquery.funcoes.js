var em_execucao = false;
var zindex = 1040;

$(document).ready(function() {
	$('.fix-height-group').each(function() {
		$(this).children('.fix-height').matchHeight({
			byRow: true
		});
	});

	/* Controle dos modais no evento "show":
	 *		- Quando um modal é iniciado, é setado um z-index para ele e a variável
	 *		  global "zindex" é incrementada para que o próximo modal funcione perfeitamente;
	 *		- Junto com o modal, é criado o modal-backdrop para o fundo.
	 */
    $(document).on('show.bs.modal', '.modal', function(ev)
	{
        $(this).css('z-index',zindex+1);

        setTimeout(function()
        {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zindex).addClass('modal-stack');
            zindex += 10;
        }, 0);
    });
	/* Controle dos modais no evento "hidden":
	 *		- Quando um modal é fechado, o modal-backdrop referente à ele é removido. Caso
	 *		  tenham mais de 1 modal aberta neste momento (contando com a que está fechando),
	 *		  a classe 'modal-open' é adicionada novamente para não "bugar" a barra de rolagem;
	 *		- Além disso, é corrigido o "padding-right" que o bootstrap remove  automaticamente
	 *		  quando um modal é aberto/fechado;
	 *		- Quando um modal é fechado é setado a classe "carregando" é removida e o conteúdo é
	 *		  apagado, para que no próximo carregamento a modal esteja limpa.
	 */
    $(document).on('hidden.bs.modal', '.modal', function(ev)
    {
        $(document.body).css('padding-right','0');
        if ($('.modal:visible').length) {
            $(document.body).addClass('modal-open');
            $(document).height()>$(window).height() && $(document.body).css('padding-right','17px');
        } else {
            $(document.body).css('padding-right','0');
        }
        zindex -= 10;
    });

	$('body').on('click','form[name=form-filtro] input[type=reset]',function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        form.find('.input-daterange input').each(function(e) {
			$(this).val('').datepicker('update');
        });
        form.find('input').each(function(e) {
        	switch($(this).attr('type')) {
        		case 'checkbox': case 'radiobox':
        			$(this).prop('checked',false);
        			break;
        		case 'text':
        			$(this).val('');
        			break;
        	}
        });
        form.find('select').each(function(e) {
        	if ($(this).find('option[value=0]').length>0) {
        		$(this).val(0);
        	} else {
        		$(this).val('');
        	}
        });
        form.find('textarea').each(function(e) {
        	$(this).val('');
        });
		form.get(0).submit();
    });

    $('.sub-modal, .sub-modal2').on('hidden.bs.modal', function (e) {
    	$('body').addClass('modal-open');
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('.btn-tooltip').tooltip();

    iniciar_mascaras();
    
    $(document).ajaxSuccess(function(e){
    	iniciar_mascaras();
    });

    $(document).ajaxComplete(function(event, xhr) {
    	console.log(xhr.responseText);
	});

    $('.total-height').on('show.bs.modal', function () {
       resize_modal($(this));
	});

	$('body').on('submit','form',function() {
		if (em_execucao) {
			return false;
		} else {
			em_execucao = true;
		}
	});

	//Configuracoes para a parte de filtro para pesquisa
	var formFiltro =  $('div#filtro-pesquisa form[name=form-filtro]');
	formFiltro.on('change','select[name=id_chave_pesquisa_pesquisa]',function(e) {
		formFiltro.find('input[name=de_chave_pesquisa]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case '0':
				formFiltro.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('');
				formFiltro.find('input[name=de_chave_pesquisa]').unmask();
				formFiltro.find('input[name=de_chave_pesquisa]').prop('disabled',true).prop('placeholder','');

				formFiltro.find('input[name=de_chave_complementar]').prop('placeholder','').prop('disabled',true).val('');
				formFiltro.find('div#filtro-chave-complementar').hide();
				break;
            case '1':
                formFiltro.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(Matrícula)');
                formFiltro.find('input[name=de_chave_pesquisa]').prop('placeholder','Chave da pesquisa (Matrícula)').unmask();

                formFiltro.find('input[name=de_chave_complementar]').prop('placeholder','').prop('disabled',true).val('');
                formFiltro.find('div#filtro-chave-complementar').hide();
                break;
			case '2':
				formFiltro.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(CPF)');
				formFiltro.find('input[name=de_chave_pesquisa]').prop('placeholder','Chave da pesquisa (CPF)').mask('000.000.000-00');

				formFiltro.find('input[name=de_chave_complementar]').prop('placeholder','Titular do CPF').prop('disabled',false).val('');
				formFiltro.find('div#filtro-chave-complementar').show();
				break;
			case '3':
				formFiltro.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(CNPJ)');
				formFiltro.find('input[name=de_chave_pesquisa]').prop('placeholder','Chave da pesquisa (CNPJ)').mask('00.000.000/0000-00');

				formFiltro.find('input[name=de_chave_complementar]').prop('placeholder','Razão social do CNPJ').prop('disabled',false).val('');
				formFiltro.find('div#filtro-chave-complementar').show();
				break;
            case '4':
                formFiltro.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(Nome)');
                formFiltro.find('input[name=de_chave_pesquisa]').prop('placeholder','Chave da pesquisa (Nome)').unmask();

                formFiltro.find('input[name=de_chave_complementar]').prop('placeholder','').prop('disabled',true).val('');
                formFiltro.find('div#filtro-chave-complementar').hide();
                break;
		}
	});

	//Configuracoes para a parte de filtro para certidao
	var formFiltroCertidao =  $('div#filtro-certidao form[name=form-filtro]');
	formFiltroCertidao.on('change','select[name=id_tipo_certidao_chave_pesquisa]',function(e) {
		formFiltroCertidao.find('input[name=de_chave_pesquisa]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case '0':
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('');
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').unmask();
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').prop('disabled',true);
				break;
			case '1':
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(CPF)');
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').mask('000.000.000-00');
				break;
			case '2':
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(CNPJ)');
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').mask('00.000.000/0000-00');
				break;
			case '4':
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').closest('fieldset').find('legend span').html('(Matrícula)');
				formFiltroCertidao.find('input[name=de_chave_pesquisa]').unmask();
				break;
		}
	});

	$("input[name='protocolo_pedido']").mask('9999.9999.9999.9999');


	$(".limpar-formulario").click(function(e){
		e.preventDefault();
		$('input:radio[name="id_chave_pesquisa_pesquisa"]').prop('checked', false);
	});

    $('.collapse').collapse({
        toggle: false
    })
    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-chevron-up").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    });

    $('a.troca-pessoa').click(function(e) {
    	e.preventDefault();
    	var key = $(this).data('key');

    	$.ajax({
			type: "POST",
			url: url_base+'troca-pessoa',
			data: {'key':key},
			success: function(retorno) {
				switch (retorno.status) {
					case 'erro':
						var alerta = swal("Erro!",retorno.msg,"error");
						break;
					case 'sucesso':
						var alerta = swal("Sucesso!",retorno.msg,"success");
						break;
				}
				alerta.then(function() {
					location.reload();
				});
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER505", "error");
			}
		});
    });

    $('body').on('paste autocomplete', 'input.somente-texto', function(e) {
        e.preventDefault();
    });
    $('body').on('keypress keyup','input.somente-texto', function(e) {
        $(this).val($(this).val().replace(/[^a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+/g,''));
    });

});

function validarCPF(cpf) {
	var cpf = cpf.replace(/[^\d]+/g,'');
	var soma = 0;
	var resto = 0;

	if (cpf=='00000000000' || cpf=='11111111111' || cpf=='22222222222' || cpf=='33333333333' || cpf=='44444444444' || cpf=='55555555555' || cpf=='66666666666' || cpf=='77777777777' || cpf=='88888888888' || cpf=='99999999999') {
		return false;
	}
	for (i=1; i<=9; i++) {
		soma = soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
	}
	resto = (soma * 10) % 11;
	if ((resto == 10) || (resto == 11)) {
		resto = 0;
	}
	if (resto != parseInt(cpf.substring(9, 10))) {
		return false;
	}
	soma = 0;
	for (i = 1; i <= 10; i++) {
		soma = soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
	}
	resto = (soma * 10) % 11;
	if ((resto == 10) || (resto == 11)) {
		resto = 0;
	}
	if (resto != parseInt(cpf.substring(10, 11))) {
		return false;
	}
		
	return true;
}

function validarCNPJ(cnpj) {
	cnpj = cnpj.replace(/[^\d]+/g,'');

	if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" || cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999") {
		return false;
	}

	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0,tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;		
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0)) {
		return false;
	}
		
	tamanho = tamanho + 1;
	numeros = cnpj.substring(0,tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
	  soma += numeros.charAt(tamanho - i) * pos--;
	  if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1)) {
		  return false;
	}
		  
	return true;
}
function total_processos(numero_processo) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: 'processo/total-processos',
		async: false,
		data: 'numero_processo='+numero_processo,
		beforeSend: function() {
		},
		success: function(total) {
			$('div#novo-processo .modal-body div.carregando').hide().removeClass('flutuante');
			res = parseInt(total);
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
	return res;
}
function total_imoveis(matricula_bem_imovel) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: baseUrl+'servicos/bem-imovel/total',
		async: false,
		data: 'matricula_bem_imovel='+matricula_bem_imovel,
		beforeSend: function() {
		},
		success: function(total) {
			$('div#novo-imovel .modal-body div.carregando').hide().removeClass('flutuante');
			res = parseInt(total);
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
	return res;
}
function total_matriculas(codigo_matricula) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: 'matricula/total',
		async: false,
		data: 'codigo_matricula='+codigo_matricula,
		beforeSend: function() {
		},
		success: function(total) {
			res = parseInt(total);
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
	return res;
}
function total_credores(nu_cpf_cnpj) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: baseUrl+'servicos/alienacao/credor/total',
		async: false,
		data: 'nu_cpf_cnpj='+nu_cpf_cnpj,
		beforeSend: function() {
		},
		success: function(total) {
			$('div#novo-credor .modal-body div.carregando').hide().removeClass('flutuante');
			res = parseInt(total);
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
	return res;
}
function iniciar_mascaras() {
	$('.data').datepicker({
		language: 'pt-BR',
		format: 'dd/mm/yyyy',
		endDate: 'today',
		autoclose: true,
		zIndexOffset: 15
	});
	$('.data').datepicker().on('show.bs.modal', function (e) {
		e.stopPropagation();
	});
	$('.periodo').datepicker({
		language: 'pt-BR',
		format: 'dd/mm/yyyy',
		endDate: 'today',
		autoclose: true,
		zIndexOffset: 15
	});
	$('.periodo').datepicker().on('show.bs.modal', function (e) {
		e.stopPropagation();
	});
    $('.data_futura').datepicker({
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        startDate: 'today',
        autoclose: true,
        zIndexOffset: 15
    });
    $('.data_futura').datepicker().on('show.bs.modal', function (e) {
        e.stopPropagation();
    });
    $('.data_pessoa').datepicker({
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        startDate: '-150y',
        endDate: 'today',
        autoclose: true,
        zIndexOffset: 15,
    });
    $('.data_pessoa').datepicker().on('show.bs.modal', function (e) {
        e.stopPropagation();
    });
	$('.real').autoNumeric('init', {vMin:'0', aSep: '.', aDec: ',', aSign: 'R$ '});
	$('.porcent').autoNumeric('init', {vMin:'0', mDec:'2', aDec:',', aSep:'.', vMax:'100'});
	$('.numero100').autoNumeric('init', {vMin:'0', mDec:'0', aDec:',', aSep:'.', vMax:'100'});
	$('.numero').autoNumeric('init', {vMin:'0', mDec:'0', aDec:',', aSep:'.'});
	$('.numero_processo').mask('0000000-00.0000.0.00.0000/99');
	$('.cpf').mask('000.000.000-00');
	$('.data').mask("99/99/9999");
	$('.data_futura').mask("99/99/9999");
	$('.data_pessoa').mask("99/99/9999");
	$('.numero_protocolo').mask("0000.0000.0000.0000");
	$('.numero_protocolo_arquivo').mask("0000.0000.0000.0000.0000");
}

function buscarPrecoProduto( vid_produto_item )
{
	if ( vid_produto_item == null ){
		return false;
	}
	//$("#precoConsulta").html('');
	//$('#valConsulta').hide();
	$.ajax({
		type: "GET",
		url: "buscarPrecoProduto/"+vid_produto_item,
		dataType: "json",
		async:false,
		success: function(json){
			var html = "";
			$("input[name='va_pedido']").val('');
			$.each(json, function(key, value){
				if(value.va_preco == null)
				{
					$('#valConsulta').show();
					$('#precoConsulta').html('R$ 0,00');
					alert('Erro ao buscar o valor do produto, contacte o administrador do sistema');
				}else{
					html += '<div class="col-md-10">'+ value.descricao + '</div>';
					html += '<div class="col-md-2 ultimo-registro">'+ value.va_preco + '</div>';
					//o loop fara que o ultimo valor que e o total fique armazenado no hidden
					$("input[name='va_pedido']").val(value.va_preco);
				}

			});
			$("#precoConsulta").html(html);
			$('#valConsulta').show();
		}
	});
}

function ifAtualizarValor( id_pedido )
{
	swal({
		title: 'Valor desatualizado!',
		text: "O valor da certidão não corresponde ao valor de tabela atual, deseja atualizar?",
		type: 'warning',
		showCancelButton: true,
		cancelButtonText: 'Não',
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sim',
		showLoaderOnConfirm: true,
	}).then(function( retorno ) {
		if ( retorno == true )
		{
			$.ajax({
				type: "POST",
				url: 'certidao/atualizar-preco',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#resultado-certidao .modal-body div.carregando').show();
					$('div#resultado-certidao .modal-body div.form').hide();
				},
				success: function( data ) {
					if ( data == 'SUCESSO') {
						swal("Certidão atualizada com sucesso!", "", "success");
						window.location.reload();
					}else{
						swal("Error", "Não foi possível atualizar o valor!", "error");
						window.location.reload();
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
}
function resize_modal(modal) {
	modal.find('.modal-body').css({height:$(window).height()-190});
}
function status_modal(obj_modal,botoes,carregando,carregando_flutuante) {
	switch (botoes) {
		case false:
			obj_modal.find('.modal-header button').removeClass('disabled').prop('disabled',false);
			obj_modal.find('.modal-footer button').removeClass('disabled').prop('disabled',false);
			break;
		case true:
			obj_modal.find('.modal-header button').addClass('disabled').prop('disabled',true);
			obj_modal.find('.modal-footer button').addClass('disabled').prop('disabled',true);
			break;
	}
	switch (carregando) {
		case false:
			obj_modal.find('.modal-body .carregando:not(.form)').removeClass('flutuante').hide();
			break;
		case true:
			if (carregando_flutuante) {
				obj_modal.find('.modal-body .carregando:not(.form)').addClass('flutuante');
			}
			obj_modal.find('.modal-body .carregando:not(.form)').show();
			break;
	}
}
function remove_caracteres(text) {
	text = text.replace(/[áàâãªä]/g, 'a')
			   .replace(/[ÁÀÂÃÄ]/g, 'A')
			   .replace(/[ÍÌÎÏ]/g, 'I')
			   .replace(/[íìîï]/g, 'i')
			   .replace(/[éèêë]/g, 'e')
			   .replace(/[ÉÈÊË]/g, 'E')
			   .replace(/[óòôõºö]/g, 'o')
			   .replace(/[ÓÒÔÕÖ]/g, 'O')
			   .replace(/[úùûü]/g, 'u')
			   .replace(/[ÚÙÛÜ]/g, 'U')
			   .replace(/[ç]/g, 'c')
			   .replace(/[Ç]/g, 'C')
			   .replace(/ñ/g, 'n')
			   .replace(/Ñ/g, 'N')
			   .replace(/–/g, '-')
			   .replace(/[’‘‹›‚]/g, '')
			   .replace(/[“”«»„]/g, '')
			   .replace(/ /g, '_');
	
	return text;
}

var widgetLogin;
var widgetLogin2;
var widgetContato;
var widgetCartorio;
var widgetJudiciario;
var widgetUsuario;
var widgetPagamento;

var onloadCallback = function() {
    widgetLogin = grecaptcha.render('captcha_login', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
    });

    widgetLogin2 = grecaptcha.render('captcha_login2', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
    });

    widgetContato = grecaptcha.render('captcha_contato', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'callback': verifyCallbackContato,
    });

    widgetCartorio = grecaptcha.render('captcha_cartorio', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'callback': verifyCallbackCartorio,
    });

    widgetJudiciario = grecaptcha.render('captcha_judiciario', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'callback': verifyCallbackJudiciario,
    });

    widgetUsuario = grecaptcha.render('captcha_usuario', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'callback': verifyCallbackUsuario,
    });

    widgetPagamento = grecaptcha.render('captcha_pagamento', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'callback': verifyCallbackPagamento,
    });
};

$(document).ready(function(){
    $('#div_modal_warning').on('shown.bs.modal', function(){
        if (widgetLogin){
            verifyCallbackLogin(grecaptcha.getResponse(widgetLogin));
        }
    });
    $('#div_modal_warning').modal('hide');

    $('#valida_login').click(function(event){
        event.preventDefault();
        $('#div_modal_warning').modal('show');
    });

    $('#btn_login2').click(function(event){
        event.preventDefault();

        var usuario       = $('form[name=form-login2]').find('input[name=usuario]').val();
        var senha_usuario = $('form[name=form-login2]').find('input[name=senha_usuario]').val();
        var response      = grecaptcha.getResponse(widgetLogin2);
        if (response){
            $.ajax({
                method: 'post',
                url   : 'acessar',
                data: {
                    usuario: usuario,
                    senha_usuario: senha_usuario,
                    response: response
                }
            })
            .done(function(msg){
                if (msg.msg){
                    $('#div_modal_error .modal-body p').text(msg.msg);
                    $('#div_modal_error').modal('show');
                    grecaptcha.reset(widgetLogin2);
                } else{
                    $('form[name=form-login2]').submit();
                }
            })
            .fail(function(jqXHR, textStatus, msg){
                console.log(msg);
            });
        } else{
            $('#div_modal_error .modal-body p').text('reCaptcha não validado. Por favor marque a opção.');
            $('#div_modal_error').modal('show');
            return false;
        }
    });
});

function verifyCallbackLogin(response){
    var form = document.forms['form-login'];
    form.elements['g-recaptcha-response'].value = response;

    form.submit();
   
    if (widgetLogin1){
        grecaptcha.reset(widgetLogin1);
    }
    $("#div_modal_warning").modal("hide");
}

function verifyCallbackContato(response){
    $('form[name=form-contato]').find('input[name=hidden_captcha_contato]').val(response);
}

function verifyCallbackCartorio(response){
    $('form[name=form-cadastrar]').find('input[name=hidden_captcha_cartorio]').val(response);
}

function verifyCallbackJudiciario(response){
    $('form[name=form-cadastrar]').find('input[name=hidden_captcha_judiciario]').val(response);
}

function verifyCallbackUsuario(response){
    $('form[name=form-cadastrar]').find('input[name=hidden_captcha_usuario]').val(response);
}

function verifyCallbackPagamento(response){
    $('form[name=form-cartao-credito]').find('input[name=hidden_captcha_pagamento]').val(response);
}