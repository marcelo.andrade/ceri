$(document).ready(function() {
    $('.cadastrar').on('click',function(e) {
        e.preventDefault();
        window.location = baseUrl+'cadastrar';
    });

    $('#menu-servicos').on('click', function(){
        $("html").animate({scrollTop: $("#index-servicos").offset().top},700);
    });

    $('#menu-fale-conosco').on('click', function(){

        $("html").animate({scrollTop: $("#index-enviar-mensagem").offset().top},700);
        $("input[name=nome]").focus();
    });
});

function removeActive(){
    $( ".menu li" ).each(function( index ) {
        $(this).removeClass("active");
    });
}
