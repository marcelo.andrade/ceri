$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var form = $('form[name=form-filtro-observacao]');

    form.on('reset',function(e) {
        e.preventDefault();
        window.location.reload();
    });

    $("input[name=pesquisar]").click(function(){
        if (validaFormulario()) {
            $('form[name=form-filtro-observacao]').submit();
            setTimeout(function(){
                window.location.reload();
            },500)
        }
    });

    $('div#serventias').on('change','select[name=cidade]',function(e) {

        var form = $(this).closest('form');
        var obj_modal = $(this).closest('.modal');
        var id_cidade = $(this).val();

        $.ajax({
            type: "POST",
            url: url_base+'servicos/listarServentias',
            data: {'id_cidade':id_cidade},
            beforeSend: function() {
                status_modal(obj_modal, true, true, true);
                form.find('select[name=serventia]').html('<option value="0">Carregando...</option>');
            },
            success: function(serventias) {
                status_modal(obj_modal, false, false, false);

                form.find('select[name=serventia]').html('<option value="0">Todas as serventias</option>');
                if (serventias.length>0) {
                    $.each(serventias,function(key,serventia) {
                        form.find('select[name=serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                    });
                    if (id_cidade=="0") {
                        form.find('select[name=serventia]').prop('disabled',true);
                    } else {
                        form.find('select[name=serventia]').prop('disabled',false);
                    }
                } else {
                    form.find('select[name=serventia]').prop('disabled',true);
                }
            },
            error: function (request, status, error) {
                status_modal(obj_modal, false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
            }
        });
    });
});

function validaFormulario() {
    var erros = new Array();
    if ($('input[name=dt_inicio]').val() == '' && $('input[name=dt_fim]').val() == '') {
        erros.push('O campo período deve ser informado com a data inicial e final.');
    }
    if (erros.length>0) {
        $('#menssagem').html(erros.join('<br />'));
        $('.erros').slideDown();
        return false;
    }

    $('.erros').slideUp();
    return true;
}
