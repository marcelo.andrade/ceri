$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    form = $('form[name=form-dados-produtos]');

    //Valor original dos produtos ao acessar a página
    var valor_ar = form.find('input[name=valor_ar]').val();
    var valor_verbacao = form.find('input[name=valor_averbacao]').val();
    var id_taxa_issqn = form.find('select[name=id_taxa_issqn]').val();


    form.find('#check_valor_ar').on('change', function(e){
        if(this.checked){
            form.find('input[name=valor_ar]').prop('disabled',false);
            form.find('input[name=dt_ini_vigencia_ar]').prop('disabled',false);
            form.find('input[name=dt_ini_vigencia_ar]').prop('style','background-color: #fff');
        }
        else{
            form.find('input[name=valor_ar]').prop('disabled',true);
            form.find('input[name=dt_ini_vigencia_ar]').prop('disabled',true);
            form.find('input[name=dt_ini_vigencia_ar]').removeAttr('style');
        }
    });

    form.find('#check_valor_averbacao').on('change', function(e){
        if(this.checked){
            form.find('input[name=valor_averbacao]').prop('disabled',false);
            form.find('input[name=dt_ini_vigencia_averbacao]').prop('disabled',false);
            form.find('input[name=dt_ini_vigencia_averbacao]').prop('style','background-color: #fff');
        }
        else{
            form.find('input[name=valor_averbacao]').prop('disabled',true);
            form.find('input[name=dt_ini_vigencia_averbacao]').prop('disabled',true);
            form.find('input[name=dt_ini_vigencia_averbacao]').removeAttr('style');
        }
    });

    form.find('#check_taxa_issqn').on('change', function(e){
        if(this.checked){
            form.find('select[name=id_taxa_issqn]').prop('disabled',false);
            form.find('input[name=dt_ini_vigencia_issqn]').prop('disabled',false);
            form.find('input[name=dt_ini_vigencia_issqn]').prop('style','background-color: #fff');
        }
        else{
            form.find('select[name=id_taxa_issqn]').prop('disabled',true);
            form.find('input[name=dt_ini_vigencia_issqn]').prop('disabled',true);
            form.find('input[name=dt_ini_vigencia_issqn]').removeAttr('style');
        }
    });

    form.on('reset', function (e) {
        e.preventDefault();

        form.find('input[name=valor_ar]').val(valor_ar);
        form.find('input[name=valor_averbacao]').val(valor_verbacao);
        form.find('select[name=id_taxa_issqn]').val(id_taxa_issqn);
        form.find('input[name=check_confirmacao]').attr('checked', false);

        form.find('input[name=check_valor_averbacao]').attr('checked', false).triggerHandler('change');
        form.find('input[name=check_valor_ar]').attr('checked', false).triggerHandler('change');
        form.find('input[name=check_taxa_issqn]').attr('checked', false).triggerHandler('change');

        form.find('div.erros').slideUp();
    });

    form.on('submit', function (e) {
        e.preventDefault();
        var erros = new Array();

        if(form.find('input[name=check_valor_ar]').is(':checked')){
            if (form.find('input[name=valor_ar]').val() == '') {
                erros.push('O campo valor do AR é obrigatório.');
            }
            if (form.find('input[name=dt_ini_vigencia_ar]').val() == ''){
                erros.push('O campo data de início é obrigatório.');
            }
        }
        if(form.find('input[name=check_valor_averbacao]').is(':checked')){
            if (form.find('input[name=valor_averbacao]').val() == '') {
                erros.push('O campo valor averbação é obrigatório.');
            }
            if (form.find('input[name=dt_ini_vigencia_averbacao]').val() == '') {
                erros.push('O campo data de início é obrigatório.');
            }
        }
        if(form.find('input[name=check_taxa_issqn]').is(':checked')){
            if (form.find('input[name=dt_ini_vigencia_issqn]').val() == ''){
                erros.push('O campo data de início é obrigatório.');
            }
        }
        if (!form.find('input[name=check_confirmacao]').is(':checked')) {
            erros.push('Você deve declarar que está ciente da alteração dos valores dos produtos.');
        }

        if (erros.length > 0) {
            form.find('div.erros div').html(erros.join('<br />'));
            if (!form.find('div.erros').is(':visible')) {
                form.find('div.erros').slideDown();
            }
            return false;
        } else {
            form.find('div.erros').slideUp();
            $.ajax({
                type: "POST",
                url: 'dados-produtos/salvar',
                data: form.serialize(),
                beforeSend: function () {
                    $('div.configuracoes .content div.carregando').show();
                },
                success: function (retorno) {
                    console.log(retorno);
                    $('div.configuracoes .content div.carregando').hide();

                    switch (retorno.status) {
                        case 'sucesso':
                            var alert = swal('Sucesso!', retorno.msg, 'success');
                            break;
                        case 'erro':
                            var alert = swal('Erro', retorno.msg, 'error');
                            break;
                        case 'info':
                            var alert = swal('Atenção', retorno.msg, 'info');
                            break;
                    }
                    alert.then(function () {
                        if (retorno.recarrega == true) {
                            location.reload();
                        }
                    })
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                        location.reload();
                    });
                }
            });
        }
    });
});
