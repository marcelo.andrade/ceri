total_selecionadas=0;
total_valor=0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var form_pgto 		= $('form[name=form-novo-pagamento]');
	var form_comp 		= $('form[name=form-novo-comprovante]');


	$('div#novo-pagamento').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: 'alienacao-pagamento/novo',
			beforeSend: function() {
				$('div#novo-pagamento .modal-body div.carregando').show();
				$('div#novo-pagamento .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#novo-pagamento .modal-body div.form form').html(retorno);
				$('div#novo-pagamento .modal-body div.carregando').hide();
				$('div#novo-pagamento .modal-body div.form form').fadeIn('fast');
				total_selecionadas=0;
			},
			error: function (request, status, error) {
				console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	$('div#novo-pagamento').on('hidden.bs.modal', function (event) {
		$('div#novo-pagamento .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-pagamento .modal-body div.form form').html('');
	});
	
	$('div#novo-pagamento').on('click','input.id_alienacao',function() {
		var id_serventia 		= $(this).data('idserventia');

		var quantidade 			= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.quantidade span');
        var quantidade_geral 	= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.quantidade span');

        var valor_total 		= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.valor-total span');
        var valor_total_geral 	= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.valor-total span');

		if ($(this).is(':checked')) {
			quantidade.html(parseInt(quantidade.html())+1);
            quantidade_geral.html(parseInt(quantidade_geral.html())+1);
			valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))+parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
            valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))+parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
		} else {
			quantidade.html(parseInt(quantidade.html())-1);
            quantidade_geral.html(parseInt(quantidade_geral.html())-1);
			valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))-parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
            valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))-parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
		}




	});
	
	$('div#novo-pagamento').on('click','input#selecionar_tudo',function() {
		var input_selecionar_tudo = $(this);
		$('div#novo-pagamento table#notificacoes>tbody>tr').each(function(index, element) {
			var id_alienacao 			= $(this).attr('id');
			var id_serventia 			= $('div#novo-pagamento table#notificacoes>tbody>tr input#id_alienacao_'+id_alienacao).data('idserventia');
			var input_alienacao 		= $('div#novo-pagamento table#notificacoes>tbody>tr input#id_alienacao_'+id_alienacao);
			var input_valor 			= $('div#novo-pagamento table#notificacoes>tbody>tr input#valor_pedido_'+id_alienacao);
			var quantidade 				= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.quantidade span');
			var quantidade_geral 		= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.quantidade span');
			var valor_total 			= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.valor-total span');
			var valor_total_geral 		= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.valor-total span');

			if (input_selecionar_tudo.is(':checked')) {
				if (!input_alienacao.is(':checked')) {
	            	input_alienacao.prop('checked',true);
					quantidade.html(parseInt(quantidade.html())+1);
                    quantidade_geral.html(parseInt(quantidade_geral.html())+1);
					valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))+parseFloat(input_valor.val()));
                    valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))+parseFloat(input_valor.val()));
				}
			} else {
				if (input_alienacao.is(':checked')) {
					input_alienacao.prop('checked',false);
					quantidade.html(parseInt(quantidade.html())-1);
                    quantidade_geral.html(parseInt(quantidade_geral.html())-1);
					valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))-parseFloat(input_valor.val()));
                    valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))-parseFloat(input_valor.val()));
				}
			}
        });
	});

	$('div#novo-pagamento').on('click','button.enviar-pagamento',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form_pgto.find('table#notificacoes input.id_alienacao:checked').length<=0) {
			erros.push('Ao menos uma notificação deve ser selecionada.');
		}
		if (erros.length>0) {
			form_pgto.find('div.erros div').html(erros.join('<br />'));
			if (!form_pgto.find('div.erros').is(':visible')) {
				form_pgto.find('div.erros').slideDown();
			}
		} else {
			form_pgto.find('div.erros').slideUp();
            var data = form_pgto.find('table#notificacoes input.id_alienacao:checked').serialize();
			$.ajax({
				type: "POST",
				url: 'alienacao-pagamento/inserir-pagamento',
				data: data,
				beforeSend: function() {
					$('div#novo-pagamento .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#novo-comprovante').on('show.bs.modal', function (event) {
		var id_alienacao_pagamento = $(event.relatedTarget).data('idpagamento');
		if (id_alienacao_pagamento>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao-pagamento/novo-comprovante',
				data: 'id_alienacao_pagamento='+id_alienacao_pagamento,
				beforeSend: function() {
					$('div#novo-comprovante .modal-body div.carregando').show();
					$('div#novo-comprovante .modal-body div.form form').hide();
				},
				success: function(retorno) {
					$('div#novo-comprovante .modal-body div.form form').html(retorno);
					$('div#novo-comprovante .modal-body div.carregando').hide();
					$('div#novo-comprovante .modal-body div.form form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});


	$('div#novo-comprovante').on('hidden.bs.modal', function (event) {
		$('div#novo-comprovante .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-comprovante .modal-body div.form form').html('');
	});
	
	$('div#novo-comprovante').on('click','button.enviar-comprovante',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form_comp.find('input[name=dt_pagamento]').val()=='') {
			erros.push('O campo data do pagamento é obrigatório.');
		}
		if (form_comp.find('input[name=va_pago]').autoNumeric('get')=='' || form_comp.find('input[name=va_pago]').autoNumeric('get')==0) {
			erros.push('O campo valor pago é obrigatório');
		}
		if (form_comp.find('input[name=no_arquivo_comprovante]').val()=='') {
			erros.push('O campo arquivo do comprovante é obrigatório.');
		}
		if (erros.length>0) {
			form_comp.find('div.erros div').html(erros.join('<br />'));
			if (!form_comp.find('div.erros').is(':visible')) {
				form_comp.find('div.erros').slideDown();
			}
		} else {
			form_comp.find('div.erros').slideUp();
			var data = new FormData(form_comp.get(0));
			$.ajax({
				type: "POST",
				url: 'alienacao-pagamento/inserir-comprovante',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#novo-comprovante .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#novo-comprovante').on('change.bs.fileinput','form[name=form-novo-comprovante] div.arquivo',function(e){
		e.stopPropagation();

		var no_arquivo = $('div#novo-comprovante form[name=form-novo-comprovante] div.arquivo input[name=no_arquivo_comprovante]').val().toLowerCase();
		var no_arquivo = no_arquivo.split('\\');
		var no_arquivo = no_arquivo[no_arquivo.length-1];

		if (!(/\.(pdf|png|jpg|bmp|docx|doc)$/i).test(no_arquivo)) {
			swal("Ops!", "Você deve selecionar um arquivo dos tipos: PDF, PNG, JPG, BMP, DOCX ou DOC.", "warning");
			$('div#novo-comprovante form[name=form-novo-comprovante] div.arquivo').fileinput('clear');
		}
	});
	
	$('div#detalhes-pagamento').on('show.bs.modal', function (event) {
		var id_alienacao_pagamento = $(event.relatedTarget).data('idpagamento');
		
		if (id_alienacao_pagamento>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao-pagamento/detalhes',
				data: 'id_alienacao_pagamento='+id_alienacao_pagamento,
				beforeSend: function() {
					$('div#detalhes-pagamento .modal-body div.carregando').show();
					$('div#detalhes-pagamento .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-pagamento .modal-body div.form').html(retorno);
					$('div#detalhes-pagamento .modal-body div.carregando').hide();
					$('div#detalhes-pagamento .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

    $('div#detalhes-pagamento').on('hidden.bs.modal', function () {
        window.location.reload(true);
    })
	
	$('div#detalhes-alienacao').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao/detalhes',
				data: 'id_alienacao='+id_alienacao+'&in_pagamento=S',
				beforeSend: function() {
					$('div#detalhes-alienacao .modal-body div.carregando').show();
					$('div#detalhes-alienacao .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-alienacao .modal-body div.form').html(retorno);
					$('div#detalhes-alienacao .modal-body div.carregando').hide();
					$('div#detalhes-alienacao .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#arquivo-alienacao').on('show.bs.modal', function (event) {
		var id_arquivo_grupo_produto = $(event.relatedTarget).data('idarquivo');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido+' ('+no_arquivo+')');
		
		if (id_arquivo_grupo_produto>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao/arquivo',
				data: 'id_arquivo_grupo_produto='+id_arquivo_grupo_produto,
				beforeSend: function() {
					$('div#arquivo-alienacao .modal-body div.carregando').show();
					$('div#arquivo-alienacao .modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$('div#arquivo-alienacao').addClass('total-height');
								resize_modal($('div#arquivo-alienacao'));
							} else {
								$('div#arquivo-alienacao').removeClass('total-height');
								$('div#arquivo-alienacao').find('.modal-body').removeAttr('style');
							}
							
							$('div#arquivo-alienacao .modal-body div.form').html(retorno.view);
							$('div#arquivo-alienacao .modal-body div.carregando').hide();
							$('div#arquivo-alienacao .modal-body div.form').fadeIn('fast');
		
							if (retorno.download=='true') {
								$('div#arquivo-alienacao .modal-footer a#arquivo-download').show();
								$('div#arquivo-alienacao .modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$('div#arquivo-alienacao .modal-footer a#arquivo-download').hide();
								$('div#arquivo-alienacao .modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});
	$('div#detalhes-andamento').on('show.bs.modal', function (event) {
		var id_andamento_alienacao = $(event.relatedTarget).data('idandamento');
		var no_fase = $(event.relatedTarget).data('nofase');
		var no_etapa = $(event.relatedTarget).data('noetapa');
		var no_acao = $(event.relatedTarget).data('noacao');
		
		$(this).find('.modal-header h4.modal-title span').html(no_fase+', '+no_etapa+', '+no_acao);
		
		if (id_andamento_alienacao>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao/detalhes-andamento',
				data: 'id_andamento_alienacao='+id_andamento_alienacao,
				beforeSend: function() {
					$('div#detalhes-andamento .modal-body div.carregando').show();
					$('div#detalhes-andamento .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-andamento .modal-body div.form').html(retorno);
					$('div#detalhes-andamento .modal-body div.carregando').hide();
					$('div#detalhes-andamento .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});	
	$('div#detalhes-imovel').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var matricula_imovel = $(event.relatedTarget).data('matriculaimovel');
		
		$(this).find('.modal-header h4.modal-title span').html(matricula_imovel);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao/imovel/detalhes',
				data: 'id_alienacao='+id_alienacao,
				beforeSend: function() {
					$('div#detalhes-imovel .modal-body div.carregando').show();
					$('div#detalhes-imovel .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-imovel .modal-body div.form').html(retorno);
					$('div#detalhes-imovel .modal-body div.carregando').hide();
					$('div#detalhes-imovel .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#detalhes-credor').on('show.bs.modal', function (event) {
		var id_credor = $(event.relatedTarget).data('idcredor');
		var no_credor = $(event.relatedTarget).data('nocredor');
		
		$(this).find('.modal-header h4.modal-title span').html(no_credor);
		
		if (id_credor>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao/credor/detalhes',
				data: 'id_credor='+id_credor,
				beforeSend: function() {
					$('div#detalhes-credor .modal-body div.carregando').show();
					$('div#detalhes-credor .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-credor .modal-body div.form').html(retorno);
					$('div#detalhes-credor .modal-body div.carregando').hide();
					$('div#detalhes-credor .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#arquivo-comprovante').on('show.bs.modal', function (event) {
		var id_alienacao_pagamento = $(event.relatedTarget).data('idpagamento');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (id_alienacao_pagamento>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao-pagamento/comprovante',
				data: 'id_alienacao_pagamento='+id_alienacao_pagamento,
				beforeSend: function() {
					$('div#arquivo-comprovante .modal-body div.carregando').show();
					$('div#arquivo-comprovante .modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$('div#arquivo-comprovante').addClass('total-height');
								resize_modal($('div#arquivo-comprovante'));
							} else {
								$('div#arquivo-comprovante').removeClass('total-height');
								$('div#arquivo-comprovante').find('.modal-body').removeAttr('style');
							}

							$('div#arquivo-comprovante .modal-body div.form').html(retorno.view);
							$('div#arquivo-comprovante .modal-body div.carregando').hide();
							$('div#arquivo-comprovante .modal-body div.form').fadeIn('fast');
		
							if (retorno.download=='true') {
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').show();
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').hide();
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});
	
	$('table#pedidos-pendentes').on('click','a.aprovar-pagamento',function(e) {
		e.preventDefault();
		var id_alienacao_pagamento = $(this).data('idpagamento');
		if (id_alienacao_pagamento>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja aprovar o pagamento selecionado?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: 'alienacao-pagamento/aprovar-pagamento',
						data: 'id_alienacao_pagamento='+id_alienacao_pagamento,
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									var alerta = swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									var alerta = swal("Sucesso!",retorno.msg,"success");
									break;
								case 'alerta':
									var alerta = swal("Ops!",retorno.msg,"warning");
									break;
							}
							if (retorno.recarrega=='true') {
								alerta.then(function(){
									location.reload();
								});
							}
						},
						error: function (request, status, error) {
							console.log(request.responseText);
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
								location.reload();
							});
						}
					});
				}
			});
		}
	});
	$('table#pedidos-pendentes').on('click','a.reprovar-pagamento',function(e) {
		e.preventDefault();
		var id_alienacao_pagamento = $(this).data('idpagamento');
		if (id_alienacao_pagamento>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja reprovar o pagamento selecionado?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: 'alienacao-pagamento/reprovar-pagamento',
						data: 'id_alienacao_pagamento='+id_alienacao_pagamento,
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									var alerta = swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									var alerta = swal("Sucesso!",retorno.msg,"success");
									break;
								case 'alerta':
									var alerta = swal("Ops!",retorno.msg,"warning");
									break;
							}
							if (retorno.recarrega=='true') {
								alerta.then(function(){
									location.reload();
								});
							}
						},
						error: function (request, status, error) {
							console.log(request.responseText);
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

    $('div#novo-pagamento').on('change','select.cidade',function() {
    	var id_cidade = $(this).val();
        $.ajax({
            type: "POST",
            url: 'listarServentias',
            data: 'id_cidade='+id_cidade,
            beforeSend: function() {
                form_pgto.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
            },
            success: function(serventias) {
                form_pgto.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
                if (serventias.length>0) {
                    $.each(serventias,function(key,serventia) {
                        form_pgto.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                    });
                    form_pgto.find('select[name=id_serventia]').prop('disabled',false);
                } else {
                    form_pgto.find('select[name=id_serventia]').prop('disabled',true);
                }
            },
            error: function (request, status, error) {
                //console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });

    });


    $('div#novo-pagamento').on('click','input.limpar-formulario',function(e) {
        setTimeout(function(){
            $.ajax({
                type: "POST",
                url: 'alienacao-pagamento/novo',
                data: form_pgto.serialize(),
                beforeSend: function() {
                    $('div#novo-pagamento .modal-body div.carregando').addClass('flutuante').show();
                    form_pgto.find('div#resultadoPesquisaPagamento').hide();
                },
                success: function(retorno) {
                    form_pgto.find('div#resultadoPesquisaPagamento').html( retorno );
                    $('div#novo-pagamento .modal-body div.carregando').hide();
                    form_pgto.find('div#resultadoPesquisaPagamento').fadeIn('fast');
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
		}, 500);

    });

    $('div#novo-pagamento').on('click','input.filtrar-pagamentos',function() {
        $.ajax({
            type: "POST",
            url: 'alienacao-pagamento/novo',
            data: form_pgto.serialize(),
            beforeSend: function() {
                $('div#novo-pagamento .modal-body div.carregando').addClass('flutuante').show();
                form_pgto.find('div#resultadoPesquisaPagamento').hide();
            },
            success: function(retorno) {
                form_pgto.find('div#resultadoPesquisaPagamento').html( retorno );
                $('div#novo-pagamento .modal-body div.carregando').hide();
                form_pgto.find('div#resultadoPesquisaPagamento').fadeIn('fast');
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });

    });

    $('div#detalhes-custas').on('show.bs.modal', function (event) {
        var id_alienacao = $(event.relatedTarget).data('idalienacao');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        $.ajax({
            type: "POST",
            url: 'alienacao/detalhes-custas',
            data: 'id_alienacao='+id_alienacao,
            beforeSend: function() {
                $('div#detalhes-custas .modal-body div.carregando').show();
                $('div#detalhes-custas .modal-body div.form').hide();
            },
            success: function(retorno) {
                $('div#detalhes-custas .modal-body div.form').html(retorno);
                $('div#detalhes-custas .modal-body div.carregando').hide();
                $('div#detalhes-custas .modal-body div.form').fadeIn('fast');
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#gerar-arquivo-previsualizar').on('show.bs.modal', function (event) {
        var id_alienacao_pagamento = $(event.relatedTarget).data('idpagamento');
        var tipo = $(event.relatedTarget).data('tipo');
        var token = $(event.relatedTarget).data('token');
        var dt_pagamento = $('input[name=dt_pagamento]').val();

        switch(tipo) {
            case 'notificacao-alienacao-fiduciaria':
                $(this).find('.modal-header h4.modal-title').html('Pré-visualização - Relatório de pagamento');
                break;
        }

        if (id_alienacao_pagamento>0) {
            $.ajax({
                type: "POST",
                url: 'alienacao-pagamento/gerar-arquivo/previsualizar',
                data: 'id_alienacao_pagamento='+id_alienacao_pagamento+'&tipo='+tipo+'&dt_pagamento='+dt_pagamento,
                context: this,
                beforeSend: function() {
                    status_modal($(this),true,true,false);
                    $(this).find('.modal-body div.form').hide();
                },
                success: function(retorno) {
                    switch (retorno.status) {
                        case 'erro':
                            var alerta = swal("Erro!",retorno.msg,"error");
                            break;
                        case 'sucesso':
                            $(this).addClass('total-height');
                            status_modal($(this),false,false,false);
                            resize_modal($(this));
                            $(this).find('.modal-body div.form').html(retorno.view).fadeIn('fast');
                            break;
                        case 'alerta':
                            var alerta = swal("Ops!",retorno.msg,"warning");
                            break;
                    }
                },
                error: function (request, status, error) {
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });
});