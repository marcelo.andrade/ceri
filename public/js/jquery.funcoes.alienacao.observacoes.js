$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#observacoes').on('keyup','textarea[maxlength!=0]',function(e) {
		var limite = $(this).prop('maxlength');

		var alert_obj = $(this).closest('div.form-group').next().find('div.alert');
		alert_obj.find('div.menssagem span.total').html($(this).val().length);
		if ($(this).val().length<limite) {
			alert_obj.removeClass('alert-warning').addClass('alert-info');
			alert_obj.find('i').removeClass('.glyphicon-exclamation-sign').addClass('.glyphicon-info-sign');
			alert_obj.find('div.menssagem span.limite').html('');
		} else {
			alert_obj.removeClass('alert-info').addClass('alert-warning');
			alert_obj.find('i').removeClass('.glyphicon-info-sign').addClass('.glyphicon-exclamation-sign');
			alert_obj.find('div.menssagem span.limite').html('O limite foi atingido.');
		}
	});

	////////////////////////////////////////////////////////////
	// Eventos do formulário "Observações"                    //
	////////////////////////////////////////////////////////////

	$('div#observacoes').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var protocolo = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/observacoes',
			data: {'id_alienacao':id_alienacao},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				status_modal($(this), false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#observacoes').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
		location.reload();
	});

	$('div#observacoes').on('submit','form[name=form-observacao]',function(e) {
		e.preventDefault();
		var erros = new Array();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('textarea[name=de_observacao]').val()=='') {
			erros.push('O campo observacao é obrigatório');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/inserir-observacao',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							if (typeof retorno.observacao !== "undefined") {
								var novaLinha = $("<tr>");
								var colunas = "";
								colunas += '<td>'+retorno.pessoa+' ('+retorno.usuario+')</td>';
								colunas += '<td>'+retorno.dt_formatada+'</td>';
								colunas += '<td class="text-wrap">'+retorno.observacao.de_observacao+'</td>';
                                colunas += '<td>';
                                $.each(retorno.arquivo, function(index, value){
                                    colunas += '<div class="btn-group">' +
                                        '<button type="button" class="btn btn-primary btn_observacao" data-toggle="modal" data-target="#visualizar-arquivo" title="'+value.no_arquivo+'" data-idarquivo="'+value.id_arquivo_grupo_produto+'" data-noarquivo="'+value.no_arquivo+'" data-noextensao="'+value.no_extensao+'">'+value.no_arquivo+'</button>';
                                    if(value.in_ass_digital=='S') {
                                        colunas += '<button type="button" class="icone-assinatura btn btn-success" data-toggle="modal" data-target="#visualizar-certificado" data-idarquivo="'+value.id_arquivo_grupo_produto+'" data-noarquivo="'+value.no_arquivo+'"><i class="fa fa-lock"></i></button>'
                                    }
                                    colunas += '</div>';
                                });
                                colunas += '</td>';
                                colunas += '<td>' +
                                    			'<div class="checkbox checkbox-primary">' +
                                    				'<input id="observacao-' + retorno.observacao.id_alienacao_observacao + '" class="styled" type="checkbox" value="' + retorno.observacao.id_alienacao_observacao + '" disabled/>' +
                                    				'<label for="observacao-' + retorno.observacao.id_alienacao_observacao + '"></label>' +
                                    			'</div>' +
                                   			 '</td>';

								novaLinha.append(colunas);
								$("table#historico-observacoes").prepend(novaLinha);
                                $('.btn-tooltip').tooltip();
							}
							/*form.find('input[type=reset]').trigger('click');*/

                            remover_arquivos(form);
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90512", "error");
				}
			});
		}
	});

	$('div#observacoes').on('click','input[type=reset]', function (e) {
        e.preventDefault();
        var token = $(this).data('token');
        var form = $('form[name=form-observacao]');

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/remover-arquivos-sessao',
            data: {'observacao_token':token},
            success: function(retorno) {
                switch (retorno.status) {
                    case 'erro':
                        swal("Erro!",retorno.msg,"error");
                        break;
                    case 'sucesso':
                        remover_arquivos(form);
                        break;
                }
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ503", "error");
            }
        });
    });

    $('div#observacoes').on('change','input[type=checkbox]', function (e) {
        e.preventDefault();
        var status = $(this).is(':checked') ? 'S' : 'N';
        var id_alienacao_observacao = $(this).val();

        if (status == 'S') {
            $(this).closest('tr').removeClass('alert-info');
		} else {
            $(this).closest('tr').addClass('alert-info');
		}

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/status-observacoes',
            data: {'status':status, 'id_alienacao_observacao':id_alienacao_observacao},
            success: function(retorno) {
                switch (retorno.status) {
                    case 'erro':
                        swal("Erro!",retorno.msg,"error").then(function(){
							if (retorno.recarrega=='true') {
								location.reload();
							}
                        });
                        break;
                    case 'sucesso':
                        swal("Sucesso!",retorno.msg,"success");
                        break;
                }
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error");
            }
        });
    });
});

function remover_arquivos(form){
    form.find('div.arquivo').remove();
    form.find('textarea[name=de_observacao]').val('');
    $('div#assinaturas-arquivos').find('a.btn-success').removeClass('btn-success').addClass('btn-warning');
    $('div#assinaturas-arquivos').find('i.glyphicon-ok').removeClass('glyphicon-ok').addClass('glyphicon-exclamation-sign');
    $('div#assinaturas-arquivos div.alert').removeClass('alert-success').addClass('alert-warning');
    $('div#assinaturas-arquivos div.menssagem span').html('Nenhum arquivo foi inserido.');
    $('div#assinaturas-arquivos div.menssagem a').addClass('disabled');
    $('div#novos-arquivos-detalhes input.btn-enviar-novos-arquivos').addClass('disabled');

    total_arquivos = 0;
    total_arquivos_assinaveis = 0;
}
