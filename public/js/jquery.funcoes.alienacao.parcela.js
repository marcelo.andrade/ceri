total_parcelas = 0;
$(document).ready(function() {

	////////////////////////////////////////////////////////////
	// Eventos do botão "Inserir parcela"                     //
	////////////////////////////////////////////////////////////

	$('div#nova-parcela').on('show.bs.modal', function (event) {
		if ($(event.relatedTarget).data('idalienacao')) {
			var alienacao_token = '';
			var id_alienacao = $(event.relatedTarget).data('idalienacao');
		} else {
			var alienacao_token = $(event.relatedTarget).data('alienacaotoken');
			var id_alienacao = 0;
		}
		
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/parcela/novo',
			data: 'alienacao_token='+alienacao_token+'&id_alienacao='+id_alienacao,
			beforeSend: function() {
				$('div#nova-parcela .modal-body div.carregando').show();
				$('div#nova-parcela .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#nova-parcela .modal-body div.form form').html(retorno);
				$('div#nova-parcela .modal-body div.carregando').hide();
				$('div#nova-parcela .modal-body div.form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#nova-parcela').on('hidden.bs.modal', function (event) {
		$('div#nova-parcela .modal-body div.carregando').removeClass('flutuante').show();
		$('div#nova-parcela .modal-body div.form form').html('');
	});
	
	$('div#nova-parcela').on('click','button.inserir-parcela',function(e) {
		var erros = new Array();
		var form = $('div#nova-parcela').find('form[name=form-nova-parcela]');
		
		var nu_parcela = form.find('input[name=nu_parcela]').val();
		var dt_vencimento = form.find('input[name=dt_vencimento]').val();
		var va_valor = form.find('input[name=va_valor]').val();

		if (nu_parcela=='') {
			erros.push('O campo número da parcela é obrigatório');
		} else {
			if ($('table#parcelas input.parcelas_nu_parcela[value="'+nu_parcela+'"]').length>0 || $('table#nova-parcelas input.a_nu_parcela[value="'+nu_parcela+'"]').length>0) {
				erros.push('Esta parcela já foi inserida');
			}
		}
		if (dt_vencimento=='') {
			erros.push('O campo data de vencimento é obrigatório');
		}
		if (va_valor=='') {
			erros.push('O campo valor da parcela é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-parcela div').html(erros.join('<br />'));
			if (!form.find('div.erros-parcela').is(':visible')) {
				form.find('div.erros-parcela').slideDown();
			}
		} else {
			var total_linhas = $("table#nova-parcelas tbody tr").length;
			var nova_linha = $('<tr id="'+(total_linhas+1)+'">');
			var colunas = "";
			colunas += '<td>';
				colunas += '<input type="hidden" name="a_nu_parcela[]" class="a_nu_parcela" id="a_nu_parcela_'+total_linhas+'" value="'+nu_parcela+'" />';
				colunas += '<input type="hidden" name="a_dt_vencimento[]" class="a_dt_vencimento" id="a_dt_vencimento_'+total_linhas+'" value="'+dt_vencimento+'" />';
				colunas += '<input type="hidden" name="a_va_valor[]" class="a_dt_vencimento" id="a_va_valor_'+total_linhas+'" value="'+va_valor+'" />';
				colunas += nu_parcela;
			colunas += '</td>';
			colunas += '<td>'+dt_vencimento+'</td>';
			colunas += '<td>'+va_valor+'</td>';
			colunas += '<td>';
				colunas += '<a href="#" class="remover-parcela btn btn-black btn-sm" data-linha="'+(total_linhas+1)+'">Remover</a>';
			colunas += '</td>';

			nova_linha.append(colunas);
			$("table#nova-parcelas tbody").append(nova_linha);
			$('div#nova-parcela button.cancelar-parcela').trigger('click');
		}
	});
	
	$('div#nova-parcela').on('click','table#nova-parcelas a.remover-parcela',function() {
		var linha = $(this).data('linha');
		$('table#nova-parcelas tr#'+linha).remove();
	});
	$('div#nova-parcela').on('click','button.cancelar-parcela',function(e) {
		var form = $('div#nova-parcela').find('form[name=form-nova-parcela]');
		form.find('div.erros-parcela').slideUp();
		form.find('input[name=nu_parcela]').val('');
		form.find('input[name=dt_vencimento]').val('').datepicker('update');;
		form.find('input[name=va_valor]').autoNumeric('set','');
	});
	$('div#nova-parcela').on('click','button.inserir-parcelas',function(e) {
		var erros = new Array();
		var form = $('div#nova-parcela').find('form[name=form-nova-parcela]');
		
		var alienacao_token = form.find('input[name=alienacao_token]').val();
		
		if ($('table#nova-parcelas input.a_nu_parcela').length<=0) {
			erros.push('Ao menos uma parcela deve ser inserida');
		}

		if (erros.length>0) {
			form.find('div.erros-parcela div').html(erros.join('<br />'));
			if (!form.find('div.erros-parcela').is(':visible')) {
				form.find('div.erros-parcela').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/parcela/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#nova-parcela .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							parcelas = retorno.parcelas;
							swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
								if (parcelas.length>0) {
									$.each(parcelas,function(key,parcela) {
										if (parcela.id_parcela_alienacao>0) {
											linha = parcela.id_parcela_alienacao;
											id_alienacao = parcela.id_alienacao;
										} else {
											linha = total_parcelas;
											id_alienacao = '';
										}
										var nova_linha = $('<tr id="'+linha+'">');
										var colunas = "";
										colunas += '<td>';
											colunas += '<input type="hidden" name="parcelas_nu_parcela[]" class="parcelas_nu_parcela" value="'+parcela.nu_parcela+'" />';
											colunas += parcela.nu_parcela;
										colunas += '</td>';
										colunas += '<td>'+parcela.dt_vencimento+'</td>';
										colunas += '<td>'+parcela.va_valor+'</td>';
										colunas += '<td>';
											colunas += '<a href="#" class="remover-parcela btn btn-black btn-sm" data-linha="'+total_parcelas+'" data-alienacaotoken="'+alienacao_token+'" data-idalienacao="'+id_alienacao+'">Remover</a>';
										colunas += '</td>';

										nova_linha.append(colunas);
										$("table#parcelas tbody").append(nova_linha);
										total_parcelas++;
									});
								}

								$('div#nova-parcela').modal('hide');
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
	$('div#nova-alienacao, div#detalhes-alienacao').on('click','table#parcelas a.remover-parcela',function(e) {
		e.preventDefault();
		var linha = $(this).data('linha');
		
		if ($(this).data('idalienacao')) {
			var alienacao_token = '';
			var id_alienacao = $(this).data('idalienacao');
		} else {
			var alienacao_token = $(this).data('alienacaotoken');
			var id_alienacao = 0;
		}

		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/parcela/remover',
			data: 'alienacao_token='+alienacao_token+'&id_alienacao='+id_alienacao+'&linha='+linha,
			beforeSend: function() {
				$('div#nova-alienacao .modal-body div.carregando').addClass('flutuante').show();
			},
			success: function(retorno) {
				if (id_alienacao>0) {
					switch (retorno.status) {
						case 'erro':
							swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							swal("Sucesso!",retorno.msg,"success");
							break;
					}
				}
				$('div#nova-alienacao .modal-body div.carregando').removeClass('flutuante').hide();
				$('table#parcelas').find('tr#'+linha).remove();
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
});