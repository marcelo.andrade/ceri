$(document).ready(function() {
	$('div#detalhes-certidao').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao/detalhes',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20504", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
		
	$('div#resultado-certidao').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/certidao/resultado',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20554", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#resultado-certidao').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

    $('div#imprimir-resultado-certidao').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        $.ajax({
            type: "POST",
            url: url_base+'servicos/certidao/imprimir-resultado',
            data: {'id_pedido':id_pedido},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($(this));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20555", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });
    $('div#recibo-certidao').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_pedido>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/certidao/gerar-recibo-certidao',
                data: {'id_pedido':id_pedido},
                context: this,
                beforeSend: function() {
                    $(this).find('.modal-body div.form').hide();
                    status_modal($(this), true, true, true);
                },
                success: function(retorno) {
                    status_modal($(this), false, false, false);
                    $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
                },
                error: function (request, status, error) {
                    status_modal($(this), false, false, false);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });
    $('div#recibo-certidao').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    var form = $('form[name=form-filtro]');

    form.on('change','select[name=id_cidade]',function(e) {
        id_cidade = $(this).val();

        $.ajax({
            type: "POST",
            url: 'listarServentias',
            data: 'id_cidade='+id_cidade,
            beforeSend: function() {
                form.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
            },
            success: function(serventias) {
                form.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
                if (serventias.length>0) {
                    $.each(serventias,function(key,serventia) {
                        form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                    });
                    form.find('select[name=id_serventia]').prop('disabled',false);
                } else {
                    form.find('select[name=id_serventia]').prop('disabled',true);
                }
            },
            error: function (request, status, error) {
                //console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    // RELATORIO CERTIDAO
    $('table#pedidos').on('click','input#selecionar-todas',function(e) {
        if ($(this).is(':checked')) {
            $('table#pedidos input.certidao:not(:checked)').trigger('click');
        } else {
            $('table#pedidos input.certidao:checked').trigger('click');
        }
    });

    $('table#pedidos').on('click','input.certidao',function(e) {
        var total = $('table#pedidos input.certidao:checked').length;

        if (total<=0) {
            $('div#alert span.total').html('0 certidões selecionadas');
            $('div#alert button').prop('disabled',true).addClass('disabled');

            $('div#alert button span').html('');
        } else {
            $('div#alert span.total').html((total==1?'1 certidão selecionada':total+' certidões selecionadas'));
            $('div#alert button').prop('disabled',false).removeClass('disabled');

            $('div#alert button span').html('('+total+')');
        }
    });

    $('div#relatorio-certidao').on('show.bs.modal', function (event) {
        var ids_certidoes = [];

        $('table#pedidos input.certidao:checked').each(function(e) {
            ids_certidoes.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/certidao/relatorio-certidao',
            data: {'ids_certidoes':ids_certidoes},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-certidao').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    $('div#relatorio-certidao').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-relatorio]').submit();
        $('div#relatorio-certidao').modal('hide');
    });
    //FIM RELATORIO CERTIDAO

    $('div#filtro-certidao').on('click','button[name=nova-certidao]', function (event) {
        swal("Atenção!", "Os pedidos de certidões não enviados ao cartório até às 24h serão cancelados automaticamente!", "warning").then(function() {
            $('div#nova-certidao').modal('show');
        });
    });
});
