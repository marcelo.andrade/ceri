total_arquivos = 0;
total_arquivos_assinaveis = 0;
index_arquivos = 0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#nova-resposta').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'penhora/nova-resposta',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30550", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#nova-resposta').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});


	$('div#nova-resposta').on('changeDate','form[name=form-prenotacao] input[name=dt_prenotacao]', function(e) {
		if (e.date.valueOf()) {
			dt_vencimento = new Date();
			dt_vencimento.setDate(e.date.getDate() + 30);
			$('form[name=form-prenotacao] input[name=dt_vencimento_prenotacao]').val(('0'+dt_vencimento.getDate()).slice(-2)+"/"+('0'+(dt_vencimento.getMonth()+1)).slice(-2)+"/"+dt_vencimento.getFullYear());
			$('form[name=form-prenotacao] input[name=dt_vencimento_prenotacao]').datepicker('update');
		}
	});

	$('div#nova-resposta').on('submit','form[name=form-resposta-averbacao]',function(e) {
		e.preventDefault();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('textarea[name=de_resposta]').val()=='') {
			erros.push('O campo resposta é obrigatório.');
		}
		if (form.find('div#arquivos-averbacao div.arquivo').length<=0){
			erros.push('Selecione pelo menos um arquivo para averbação.');
		}
		if (total_arquivos_assinaveis>0) {
			erros.push('É obrigatório assinar os arquivos inseridos');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-averbacao div').html(erros.join('<br />'));
			if (!form.find('div.erros-averbacao').is(':visible')) {
				form.find('div.erros-averbacao').slideDown();
			}
		} else {
			form.find('div.erros-averbacao').slideUp();
			$.ajax({
				type: "POST",
				url: 'penhora/inserir-averbacao',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30551", "error");
				}
			});
		}
	});

	$('div#nova-resposta').on('submit','form[name=form-prenotacao]',function(e) {
		e.preventDefault();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=codigo_prenotacao]').val()=='') {
			erros.push('O campo código é obrigatório');
		}
		if (form.find('input[name=dt_prenotacao]').val()=='') {
			erros.push('O campo data de cadastro é obrigatório');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-prenotacao div').html(erros.join('<br />'));
			if (!form.find('div.erros-prenotacao').is(':visible')) {
				form.find('div.erros-prenotacao').slideDown();
			}
		} else {
			form.find('div.erros-prenotacao').slideUp();
			$.ajax({
				type: "POST",
				url: 'penhora/inserir-prenotacao',
				data: form.serialize(),
				success: function(retorno) {
					$('div#nova-resposta .modal-body div.carregando').removeClass('flutuante').hide();
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							form.find('input[name=codigo_prenotacao]').prop('disabled',true);
							form.find('input[name=dt_prenotacao]').prop('disabled',true);
							form.find('div.botoes').remove();
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30552", "error");
				}
			});
		}
	});
	$('div#nova-resposta').on('reset','form[name=form-prenotacao]',function(e) {
		var form = $(this);
		form.find('input[name=dt_prenotacao]').val('').datepicker('update');
		form.find('input[name=dt_vencimento_prenotacao]').val('').datepicker('update');
		form.find('div.erros-prenotacao').slideUp();
	});

	$('div#nova-resposta').on('submit','form[name=form-custa]',function(e) {
		e.preventDefault();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=de_custa]')=='') {
			erros.push('O campo descrição é obrigatório');
		}
		if (form.find('input[name=va_custa]').autoNumeric('get')=='') {
			erros.push('O campo valor é obrigatório');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-custa div').html(erros.join('<br />'));
			if (!form.find('div.erros-custa').is(':visible')) {
				form.find('div.erros-custa').slideDown();
			}
		} else {
			form.find('div.erros-custa').slideUp();
			$.ajax({
				type: "POST",
				url: 'penhora/inserir-custa',
				data: form.serialize(),
				success: function(retorno) {
					$('div#nova-resposta .modal-body div.carregando').removeClass('flutuante').hide();
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							form.find('input[name=de_custa]').prop('disabled',true);
							form.find('input[name=va_custa]').prop('disabled',true);
							form.find('div.botoes').remove();
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30553", "error");
				}
			});
		}
	});
	$('div#nova-resposta').on('reset','form[name=form-custa]',function(e) {
		var form = $(this);
		form.find('div.erros-custa').slideUp();
	});

	$('div#nova-resposta').on('submit','form[name=form-resposta-exigencia]',function(e) {
		e.preventDefault();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('div#arquivos-pendencias div.arquivo').length<=0 && form.find('textarea[name=de_resposta]').val()=='') {
			erros.push('Você deve inserir um arquivo ou preencher uma resposta para prosseguir.');
		} else if (form.find('div#arquivos-pendencias div.arquivo').length>0) {
			if (total_arquivos_assinaveis>0) {
				erros.push('É obrigatório assinar os arquivos inseridos');
			}
		}

		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-exigencia div').html(erros.join('<br />'));
			if (!form.find('div.erros-exigencia').is(':visible')) {
				form.find('div.erros-exigencia').slideDown();
			}
		} else {
			form.find('div.erros-exigencia').slideUp();
			$.ajax({
				type: "POST",
				url: 'penhora/inserir-exigencia',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30554", "error");
				}
			});
		}
	});
});