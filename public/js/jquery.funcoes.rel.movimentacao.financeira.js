$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var form = $('form[name=form-relatorio-movimentacao]');

	form.on('reset',function(e) {
		e.preventDefault();
		window.location.reload();
	});


	$("input[name=relatorioPesquisar]").click(function(){
		if ( validaFormulario() )
		{
			console.log('passei aqui');
			$('form[name=form-relatorio-movimentacao]').submit();
			setTimeout(function(){
				window.location.reload();
			},500)
		}
	});

	$('div#serventias').on('change','select[name=id_cidade]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_cidade = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listarServentias',
			data: {'id_cidade':id_cidade},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
				form.find('select[name=id_serventia]').html('<option value="0">Carregando...</option>');
			},
			success: function(serventias) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_serventia]').html('<option value="0">Selecione uma serventia</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					if (id_cidade=="0") {
						form.find('select[name=id_serventia]').prop('disabled',true);
					} else {
						form.find('select[name=id_serventia]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_serventia]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
			}
		});
	});

});

function status_modal(obj_modal,botoes,carregando,carregando_flutuante) {
	switch (botoes) {
		case false:
			obj_modal.find('.modal-header button').removeClass('disabled').prop('disabled',false);
			obj_modal.find('.modal-footer button').removeClass('disabled').prop('disabled',false);
			break;
		case true:
			obj_modal.find('.modal-header button').addClass('disabled').prop('disabled',true);
			obj_modal.find('.modal-footer button').addClass('disabled').prop('disabled',true);
			break;
	}
	switch (carregando) {
		case false:
			obj_modal.find('.modal-body .carregando').removeClass('flutuante').hide();
			break;
		case true:
			if (carregando_flutuante) {
				obj_modal.find('.modal-body .carregando').addClass('flutuante');
			}
			obj_modal.find('.modal-body .carregando').show();
			break;
	}
}


function validaFormulario() {
	var erros = new Array();
	if ($('input[name=dt_inicio]').val() == '' && $('input[name=dt_fim]').val() == '') {
		erros.push('O campo período deve ser informado com a data inicial e final.');
	}
	if (erros.length>0) {
		$('#menssagem').html(erros.join('<br />'));
		$('.erros').slideDown();
		return false;
	}else{
		$('.erros').slideUp();
		return true;
	}
}
