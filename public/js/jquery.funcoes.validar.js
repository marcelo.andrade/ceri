$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('form[name=form-validar]').on('submit', function (e) {
		e.preventDefault();
		var erros = new Array();
		if ($('form[name=form-validar]').find('input[name=protocolo_pedido]').val()=='') {
			erros.push('O campo número do protocolo é obrigatório.');
		}
		if (erros.length>0) {
			$('form[name=form-validar]').find('div.erros div').html(erros.join('<br />'));
			if (!$('form[name=form-validar]').find('div.erros').is(':visible')) {
				$('form[name=form-validar]').find('div.erros').slideDown();
			}
		} else {
			$('form[name=form-validar]').find('div.erros').slideUp();

			var protocolo_pedido = $('form[name=form-validar]').find('input[name=protocolo_pedido]').val();

			$.ajax({
				type: "POST",
				url: 'validar',
				data: {protocolo_pedido:protocolo_pedido},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							$('div#resultado-certidao').find('.modal-header h4.modal-title span').html(protocolo_pedido);
							$('div#resultado-certidao').find('.modal-body div.form').addClass('carregando').html(retorno.view);
							$('div#resultado-certidao').modal('show');
							break;
					}
				},
				error: function (request, status, error) {
	                swal("Erro!", "Por favor, tente novamente mais tarde. Erro VAL501", "error").then(function() {
	                    $('div#resultado-certidao').modal('hide');
	                });
				}
			});
		}
	});

	$('div#imprimir-resultado-certidao').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        $.ajax({
            type: "POST",
            url: 'validar/imprimir-resultado',
            data: {'id_pedido':id_pedido},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($(this));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

	$('div#imprimir-resultado-certidao').on('hidden.bs.modal', function (event) {
		$(this).find('.modal-body div.form').html('');
	});

	$('div#visualizar-arquivo').on('show.bs.modal', function (event) {
		var hash_arquivo = $(event.relatedTarget).data('hasharquivo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (hash_arquivo!='') {
			$.ajax({
				type: "POST",
				url: '/validar/visualizar-arquivo',
				data: {'hash_arquivo':hash_arquivo},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$(this).addClass('total-height');
								resize_modal($('div#visualizar-arquivo'));
							} else {
								$(this).removeClass('total-height');
								$(this).find('.modal-body').removeAttr('style');
							}
							
							status_modal($(this),false,false,false);
							$(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
		
							if (retorno.download=='true') {
								$(this).find('.modal-footer a#arquivo-download').show();
								$(this).find('.modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$(this).find('.modal-footer a#arquivo-download').hide();
								$(this).find('.modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ505", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#visualizar-certificado').on('show.bs.modal', function (event) {
		var hash_arquivo = $(event.relatedTarget).data('hasharquivo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (hash_arquivo!='') {
			$.ajax({
				type: "POST",
				url: '/validar/visualizar-arquivo/certificado',
				data: {'hash_arquivo':hash_arquivo},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form').hide();
				},
				success: function(retorno) {
					status_modal($(this),false,false,false);
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ506", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
});
