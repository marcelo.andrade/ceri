total_arquivos = 0;
total_arquivos_assinaveis = 0;
index_arquivos = 0;
linha = 0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
	});

	$('div#nova-penhora').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: url_base+'servicos/penhora/nova-penhora',
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30501", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#nova-penhora').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
	});

	$('div#nova-penhora').on('click','button.incluir-serventia',function(e){
		incluir_serventia('N');
	});
	$('div#nova-penhora').on('click','button.incluir-todas-serventias',function(e){
		incluir_serventia('S');
	});

	$('div#nova-penhora').on('click', 'div#serventias-selecionadas button.remover-serventia', function (e) {
		var form = $(this).closest('form');
		var id_serventia = $(this).data('idserventia');

		form.find('div#serventias-selecionadas div.serventia#'+id_serventia).remove();
		form.find('div#imoveis-cadastrados div.matricula-imovel[data-idserventia="'+id_serventia+'"]').remove();
		form.find('select[name=id_serventia_imoveis] option[value='+id_serventia+']').remove();
	});

	$('div#nova-penhora').on('click','button.incluir-imovel', function(e){
		var form = $(this).closest('form');
		var erros = new Array();
		var matricula_imovel = form.find('input[name=matricula_imovel]').val().replace(/ /g,'');
		var id_serventia_imovel = form.find('select[name=id_serventia_imoveis]').val();
		var no_serventia_imovel = form.find('select[name=id_serventia_imoveis] option:selected').html();

		if (matricula_imovel=="") {
			erros.push('O campo matrícula do imóvel é obrigatório.');
		} else if (form.find('div#imoveis-cadastrados div.imoveis div.matricula-imovel input[name="matriculas_cadastradas['+id_serventia_imovel+'][]"][value="'+matricula_imovel+'"]').length>0) {
			erros.push('Esta matrícula já foi adicionada.');
		}
		if (id_serventia_imovel==0) {
			erros.push('O campo serventia é obrigatório.');
		}

		if (erros.length>0) {
			form.find('div.erros-imovel div').html(erros.join('<br />'));
			if (!form.find('div.erros-imovel').is(':visible')) {
				form.find('div.erros-imovel').slideDown();
			}
		} else {
			if (form.find('div#imoveis-cadastrados div.imoveis div.matricula-imovel input[value="'+matricula_imovel+'"]').length<=0) {
				form.find('div#imoveis-cadastrados').show();
			}

			html =  '<div id="'+matricula_imovel+'" data-idserventia="'+id_serventia_imovel+'" data-linha="'+linha+'" class="matricula-imovel btn-group">';
			html += '<button class="btn btn-sm btn-primary" type="button">'+matricula_imovel+' - '+no_serventia_imovel+'</button>';
			html += '<input type="hidden" name="matriculas_cadastradas['+id_serventia_imovel+'][]" value="'+matricula_imovel+'">';
			html += '<button class="remover-matricula btn btn-sm btn-danger" type="button" data-matricula="'+matricula_imovel+'" data-linha="'+linha+'"><i class="fa fa-times"></i></button>';
			html += '</div> ';

			form.find('div#imoveis-cadastrados div.imoveis').append(html);
			
			form.find('input[name=matricula_imovel]').val('');
			form.find('select[name=id_serventia_imoveis]').val(0);
			linha++;

            form.find('div.erros-imovel').slideUp();
		}

	});

	$('div#nova-penhora').on('click', 'div#imoveis-cadastrados button.remover-matricula', function (e) {
		var form = $(this).closest('form');
		var remove_linha = $(this).data('linha');

		form.find('div#imoveis-cadastrados div.matricula-imovel[data-linha="'+remove_linha+'"]').remove();
		if (form.find('div#imoveis-cadastrados div.imoveis div.matricula-imovel').length<=0) {
			form.find('div#imoveis-cadastrados').hide();
		}
	});

	$('div#nova-penhora').on('change','select[name=id_cidade]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_cidade = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listarServentias',
			data: {'id_cidade':id_cidade},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(serventias) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_serventia_select]').html('<option value="0">Selecione uma serventia</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia_select]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					if (id_cidade=="0") {
						form.find('select[name=id_serventia_select]').prop('disabled',true);
					} else {
						form.find('select[name=id_serventia_select]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_serventia_select]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
			}
		});
	});
	$('div#nova-penhora').on('change','select[name=id_serventia_select]',function(e) {
		var form = $(this).closest('form');

		if ($(this).val()>0) {
			form.find('button.incluir-serventia').removeClass('disabled').prop('disabled',false);
		} else {
			form.find('button.incluir-serventia').addClass('disabled').prop('disabled',true);
		}
	});

	$('div#nova-penhora').on('click','div.radio input[name=id_tipo_custa]',function(e) {
		var form = $(this).closest('form');

		if (form.find('input[name=in_emitir_certidao]:checked').length<=0) {
			swal("Ops!", "Selecione primeiro o campo Emitir certidão", "warning");
			return false;
		}
		form.find('div#documento-isencao').hide();
		form.find('div#msg-isencao').hide();
		if ($(this).val()=='3') {
			swal({
				title: 'Atenção?',
				text: "Houve despacho de deferimento de gratuidade emitido pela AJG?",
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				showLoaderOnConfirm: true,
			}).then(function( retorno ) {
				if (retorno == true) {
					form.find('div#msg-isencao').hide();
					form.find('div#documento-isencao').show();
				}
			}).catch(function(e) {
				form.find('div#documento-isencao').hide();
				form.find('div#msg-isencao').show();
				form.find('input[name=id_tipo_custa]').prop('checked', false);
			});
		}
	});

	$('div#nova-penhora').on('click','button.incluir-penhora',function(e) {
		var form = $('form[name=form-nova-penhora]');
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('div#serventias-selecionadas div.serventia').length<=0) {
			erros.push('Selecione pelo menos uma serventia.');
		} else {
			var serventias_sem_imoveis = new Array();
			form.find('div#serventias-selecionadas div.serventia').each(function() {
				if (form.find('div#imoveis-cadastrados div.matricula-imovel[data-idserventia="'+$(this).prop('id')+'"]').length<=0){
					serventias_sem_imoveis.push('<b>'+$(this).data('noserventia')+'</b>');
				}
			});
			if (serventias_sem_imoveis.length>0) {
				if (serventias_sem_imoveis.length==1) {
					erros.push('A serventia "'+serventias_sem_imoveis.join()+'" não possui imóveis cadastrados, é obrigatório o cadastro de ao menos um imóvel para cada serventia.');
				} else {
					erros.push('As serventias "'+serventias_sem_imoveis.join(', ')+'" não possuem imóveis cadastrados, é obrigatório o cadastro de ao menos um imóvel para cada serventia.');
				}
			}
		}
		if (form.find('input[name=numero_processo]').val()=='') {
			erros.push('O campo número do processo é obrigatório.');
		}
		if (form.find('input[name=id_tipo_penhora]:checked').length<=0) {
			erros.push('O tipo da penhora é obrigatório.');
		}
		if (form.find('input[name=in_emitir_certidao]:checked').length<=0) {
			erros.push('O campo emitir certidão é obrigatório.');
		}
		if (form.find('input[name=id_tipo_custa]:checked').length<=0) {
			erros.push('O campo tipo de custa é obrigatório.');
		} else {
			/*
			if (form.find('input[name=id_tipo_custa]:checked').val()==1 && form.find('input[name=in_emitir_certidao]:checked').val()=='S') {
				erros.push('O campo tipo de custa não pode ser "Sem custar por disposição legal" quando a opção de emitir certidão for "SIM"');
			}
			*/

			if (form.find('input[name=id_tipo_custa]:checked').val()==3 && form.find('div#arquivos-isencao div.arquivo').length<=0) {
				erros.push('O arquivo documento de isenção de emolumentos é obrigatório.');
			}
		}
		if (form.find('div#arquivos-gerais div.arquivo').length<=0) {
			erros.push('Ao menos um arquivo deve ser inserido para a penhora.');
		}

		if (total_arquivos_assinaveis>0) {
			erros.push('É obrigatório assinar os arquivos inseridos');
		}

		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/penhora/inserir-penhora',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30502", "error");
				}
			});
		}
	});

	$('div#detalhes-penhora').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'/servicos/penhora/detalhes',
				data: {'id_pedido':id_pedido},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30503", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#detalhes-penhora').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#resultado-penhora').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/penhora/resultado',
				data: {'id_pedido':id_pedido},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30504", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#resultado-penhora').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#exigencias-penhora').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/penhora/exigencias',
				data: {'id_pedido':id_pedido},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30505", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#exigencias-penhora').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#exigencias-penhora').on('click','form[name=form-resposta-exigencia] input[name=tipo_resultado_envio]',function(e) {
		var form = $(this).closest('form');
		switch ($(this).val()) {
			case '1':
				form.find('textarea[name=de_resultado]').prop('disabled',false);
				form.find('div#arquivos-pendencias button').addClass('disabled').prop('disabled',true);
				form.find('div#arquivos-pendencias div.arquivo').remove();
				break;
			case '2':
				form.find('div#arquivos-pendencias button').removeClass('disabled').prop('disabled',false);
				form.find('textarea[name=de_resultado]').prop('disabled',true);
				break;
		}
	});
	
	$('div#exigencias-penhora').on('submit','form[name=form-resposta-exigencia]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('textarea[name=de_resposta]').val()=='') {
			erros.push('O campo resposta é obrigatório.');
		}
		if (form.find('input[name=tipo_resultado_envio]:checked').length<=0) {
			erros.push('O tipo de envio do resultado é obrigatório.');
		} else {
			if (form.find('input[name=tipo_resultado_envio]:checked').val()=='1' && form.find('textarea[name=de_resultado]').val()=='') {
				erros.push('O campo resultado é obrigatório.');
			} else if (form.find('input[name=tipo_resultado_envio]:checked').val()=='2' && $('form[name=form-resposta-exigencia]').find('div#arquivos-pendencias div.arquivo').length<=0) {
				erros.push('Selecione ao menos um arquivo para upload.');
			}
		}
		
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-exigencia div').html(erros.join('<br />'));
			if (!form.find('div.erros-exigencia').is(':visible')) {
				form.find('div.erros-exigencia').slideDown();
			}
		} else {
			form.find('div.erros-exigencia').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/penhora/inserir-exigencia',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30506", "error");
				}
			});
		}
	});

	$('div#recibo-penhora').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/penhora/recibo',
				data: {'id_pedido':id_pedido},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$(this).find('.modal-body div.form').html(retorno.view).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 30508", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#recibo-penhora').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});
});

function incluir_serventia(todas) {
	var form = $('form[name=form-nova-penhora]');
	var id_cidade = form.find('select[name=id_cidade] option:selected').val();
	
	if (todas=='N') {
		var serventias = form.find('select[name=id_serventia_select] option:selected');
	} else {
		var serventias = form.find('select[name=id_serventia_select] option[value!="0"]');
	}

	serventias.each(function (e) {
		if (form.find('div#serventias-selecionadas div.serventia#'+$(this).val()).length>0) {
			if (todas=='N') {
				swal("Ops!", "Esta serventia já foi escolhida", "warning");
			}
		} else {
			html = 	'<div id="'+$(this).val()+'" class="serventia btn-group" data-noserventia="'+$(this).text()+'">';
				html +=	'<button class="btn btn-sm btn-primary" type="button">'+$(this).text()+'</button>';
				html += '<input type="hidden" name="id_serventia[]" value="'+$(this).val() +'">';
				html += '<button class="remover-serventia btn btn-sm btn-danger" type="button" data-idserventia="'+$(this).val()+'"><i class="fa fa-times"></i></button>';
			html += '</div> ';

			$("div#serventias-selecionadas").append(html);

			if (form.find('select[name=id_serventia_imoveis] option[value="'+$(this).val()+'"]').length<=0) {
				form.find('select[name=id_serventia_imoveis]').append('<option value="'+$(this).val()+'">'+$(this).text()+'</option>');
			}
		}
	});
	if (serventias.length>0) {
		form.find('select[name=id_serventia_imoveis]').prop('disabled',false);
	}
}