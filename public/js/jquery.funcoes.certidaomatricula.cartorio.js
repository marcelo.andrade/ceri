$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#nova-resposta').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/nova-resposta',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').show();
					$('div#nova-resposta .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#nova-resposta .modal-body div.form').html(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide();
					$('div#nova-resposta .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#nova-resposta').on('hidden.bs.modal', function (event) {
		$('div#nova-resposta .modal-body div.carregando').removeClass('flutuante').show();
		$('div#nova-resposta .modal-body div.form').html('');
	});
	
	$('div#nova-resposta').on('submit','form[name=form-novo-andamento]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		if (form.find('input[name=id_tipo_resposta]:checked').length<=0) {
			erros.push('Ao menos uma ação deve ser selecionada');
		}
		if (erros.length>0) {
			form.find('div.erros-andamento div').html(erros.join('<br />'));
			if (!form.find('div.erros-andamento').is(':visible')) {
				form.find('div.erros-andamento').slideDown();
			}
		} else {
			form.find('div.erros-andamento').slideUp();
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/inserir-andamento',
				data: form.serialize(),
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					//console.log(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						if (typeof retorno.historico !== "undefined") {
							var novaLinha = $("<tr>");
							var colunas = "";
							colunas += '<td>'+retorno.usuario+'</td>';
							colunas += '<td>'+retorno.dt_formatada+'</td>';
							colunas += '<td>'+retorno.historico.de_observacao+'</td>';
							
							novaLinha.append(colunas);
							$("table#historico-pedido").prepend(novaLinha);
						}
						
						form.find('input[type=reset]').trigger('click');
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#nova-resposta').on('submit','form[name=form-nova-precificacao]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		if (form.find('input[name=in_positivo]:checked').length<=0) {
			erros.push('O campo tipo de resultado é obrigatório');
		} else if (form.find('input[name=in_positivo]:checked').val()=='S') {
			if (form.find('input[name=nu_quantidade]').autoNumeric('get')=='') {
				erros.push('O campo quantidade de folhas é obrigatório');
			}
			if (form.find('input[name=va_pedido]').autoNumeric('get')=='') {
				erros.push('O campo valor do pedido é obrigatório');
			}
		}
		if (erros.length>0) {
			form.find('div.erros-precificacao div').html(erros.join('<br />'));
			if (!form.find('div.erros-precificacao').is(':visible')) {
				form.find('div.erros-precificacao').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/inserir-precificacao',
				data: form.serialize(),
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					//console.log(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						if (typeof retorno.historico !== "undefined") {
							var novaLinha = $("<tr>");
							var colunas = "";
							colunas += '<td>'+retorno.usuario+'</td>';
							colunas += '<td>'+retorno.dt_formatada+'</td>';
							colunas += '<td>'+retorno.historico.de_observacao+'</td>';
							
							novaLinha.append(colunas);
							$("table#historico-pedido").prepend(novaLinha);
						}						
						form.find('input[type=reset]').trigger('click');
						swal("Sucesso!", "Precificação salva com sucesso.", "success").then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#nova-resposta').on('submit','form[name=form-novo-resultado]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		if (form.find('input[name=in_positivo]:checked').length<=0) {
			erros.push('O campo tipo de resultado é obrigatório');
		} else if (form.find('input[name=in_positivo]:checked').val()=='S') {
			if (form.find('input[name=tipo_resultado_envio]:checked').length<=0) {
				erros.push('O tipo de envio do resultado é obrigatório');
			} else {
				if (form.find('input[name=tipo_resultado_envio]:checked').val()=='1' && form.find('textarea[name=de_resultado]').val()=='') {
					erros.push('O campo observação do resultado é obrigatório');
				} else if (form.find('input[name=tipo_resultado_envio]:checked').val()=='2' && form.find('input[name=no_arquivo]').val()=='') {
					erros.push('O campo arquivo é obrigatório');
				}
			}
		}
		if (erros.length>0) {
			form.find('div.erros-resultado div').html(erros.join('<br />'));
			if (!form.find('div.erros-resultado').is(':visible')) {
				form.find('div.erros-resultado').slideDown();
			}
		} else {
			form.find('div.erros-resultado').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/inserir-resultado',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						if (typeof retorno.historico !== "undefined") {
							var novaLinha = $("<tr>");
							var colunas = "";
							colunas += '<td>'+retorno.usuario+'</td>';
							colunas += '<td>'+retorno.dt_formatada+'</td>';
							colunas += '<td>'+retorno.historico.de_observacao+'</td>';
							
							novaLinha.append(colunas);
							$("table#historico-pedido").prepend(novaLinha);
						}						
						form.find('input[type=reset]').trigger('click');
						swal("Sucesso!", "Resultado salvo com sucesso.", "success").then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#nova-resposta').on('keyup','form[name=form-novo-andamento] textarea[name=de_resposta]',function(e) {
		 var form = $(this);
		 limite = 200;
		 if( $(this).val().length > limite )
		 {
			 $('div#nova-resposta form[name=form-novo-andamento] textarea[name=de_resposta]').val( $('div#nova-resposta form[name=form-novo-andamento] textarea[name=de_resposta]').val().substring( 0, limite ) );
			 swal("Atenção!", "O limite de 200 caracteres foi atingido", "warning");
		 }
	})

	$('div#nova-resposta').on('keyup','form[name=form-novo-resultado] textarea[name=de_resultado]',function(e) {
		 var form = $(this);
		 limite = 200;
		 if( $(this).val().length > limite )
		 {
			 $('div#nova-resposta form[name=form-novo-resultado] textarea[name=de_resultado]').val( $('div#nova-resposta form[name=form-novo-resultado] textarea[name=de_resultado]').val().substring( 0, limite ) );
			 swal("Atenção!", "O limite de 200 caracteres foi atingido", "warning");
		 }
	})



	$('div#nova-resposta').on('click','form[name=form-nova-precificacao] input[name=in_positivo]',function(e) {
		switch ($(this).val()) {
			case 'S':
				$('div#nova-resposta form[name=form-nova-precificacao]').find('input[name=nu_quantidade]').prop('disabled',false);
				$('div#nova-resposta form[name=form-nova-precificacao]').find('input[name=va_pedido]').prop('disabled',false);
				break;
			case 'N':
				$('div#nova-resposta form[name=form-nova-precificacao]').find('input[name=nu_quantidade]').prop('disabled',true);
				$('div#nova-resposta form[name=form-nova-precificacao]').find('input[name=va_pedido]').prop('disabled',true);
				break;
		}
	});

	$('div#nova-resposta').on('click','form[name=form-novo-resultado] input[name=in_positivo]',function(e) {
		switch ($(this).val()) {
			case 'S':
				/*
				$('div#nova-resposta form[name=form-novo-resultado]').find('input[name=tipo_resultado_envio]').prop('disabled',false);
				$('div#nova-resposta form[name=form-novo-resultado]').find('textarea[name=de_resultado]').prop('disabled',false);
				$('div#nova-resposta form[name=form-novo-resultado]').find('input[name=no_arquivo]').prop('disabled',false);
				*/
				if ($('div#nova-resposta form[name=form-novo-resultado] div.resultado-mensagem').css('display')=='block') {
					var classe = 'resultado-mensagem';
				} else if ($('div#nova-resposta form[name=form-novo-resultado] div.resultado-negativo').css('display')=='block') {
					var classe = 'resultado-negativo';
				}
				$('div#nova-resposta form[name=form-novo-resultado] div.'+classe).slideUp('fast',function(e) {
					$('div#nova-resposta form[name=form-novo-resultado] div.resultado-positivo').slideDown('fast');
				});
				break;
			case 'N':
				/*
				$('div#nova-resposta form[name=form-novo-resultado]').find('input[name=tipo_resultado_envio]').prop('disabled',true);
				$('div#nova-resposta form[name=form-novo-resultado]').find('textarea[name=de_resultado]').prop('disabled',true);
				$('div#nova-resposta form[name=form-novo-resultado]').find('input[name=no_arquivo]').prop('disabled',true);
				*/
				if ($('div#nova-resposta form[name=form-novo-resultado] div.resultado-mensagem').css('display')=='block') {
					var classe = 'resultado-mensagem';
				} else if ($('div#nova-resposta form[name=form-novo-resultado] div.resultado-positivo').css('display')=='block') {
					var classe = 'resultado-positivo';
				}
				$('div#nova-resposta form[name=form-novo-resultado] div.'+classe).slideUp('fast',function(e) {
					$('div#nova-resposta form[name=form-novo-resultado] div.resultado-negativo').slideDown('fast');
				});
				break;
		}
	});

	$('div#nova-resposta').on('change.bs.fileinput','form[name=form-novo-resultado] div.arquivo',function(e){
		e.stopPropagation();

		var no_arquivo = $('div#nova-resposta form[name=form-novo-resultado] div.arquivo input[name=no_arquivo]').val().toLowerCase();
		var no_arquivo = no_arquivo.split('\\');
		var no_arquivo = no_arquivo[no_arquivo.length-1];

		if (!(/\.(pdf|png|jpg|bmp|tif)$/i).test(no_arquivo)) {
			swal("Ops!", "Você deve selecionar um arquivo dos tipos: PDF, PNG, JPG, BMP ou TIF.", "warning");
			$('div#nova-resposta form[name=form-novo-resultado] div.arquivo').fileinput('clear');
		}
	});

	$('div#resultado-certidao').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/resultado',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#resultado-certidao .modal-body div.carregando').show();
					$('div#resultado-certidao .modal-body div.form').hide();
				},
				success: function(retorno) {
					if (retorno=='ERRO_01') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
							$('div#resultado-certidao').modal('hide');
						});
					} else if (retorno=='ERRO_02') {
						swal("Resultado negativo!", "O resultado da matrícula digital ("+protocolo_pedido+") não foi encontrada nos registros do cartório.", "warning").then(function() {
							$('div#resultado-certidao').modal('hide');
						});
					} else {
						//console.log(retorno.view);
						//console.log(retorno.assinatura_digital);
						$('div#resultado-certidao .modal-body div.form').html(retorno.view);
						$('div#resultado-certidao .modal-body div.carregando').hide();
						$('div#resultado-certidao .modal-body div.form').fadeIn('fast');
						/*
						if (retorno.assinatura_digital=='true') {
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').show();
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').data('idpedido',id_pedido);
						} else {
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').hide();
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').data('idpedido',false);
						}
						*/
						if (retorno.download=='true') {
							$('div#resultado-certidao .modal-footer a#resultado-download').show();
							$('div#resultado-certidao .modal-footer a#resultado-download').attr('href',retorno.url_download);
						} else {
							$('div#resultado-certidao .modal-footer a#resultado-download').hide();
							$('div#resultado-certidao .modal-footer a#resultado-download').attr('href','javascript:void(0);');
						}
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#certificado-detalhes').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao/certificado-detalhes',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#certificado-detalhes .modal-body div.carregando').show();
					$('div#certificado-detalhes .modal-body div.form').hide();
				},
				success: function(retorno) {
					//console.log(retorno);
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
						$('div#certificado-detalhes').modal('hide');
					} else {
						$('div#certificado-detalhes .modal-body div.form').html(retorno);
						$('div#certificado-detalhes .modal-body div.carregando').hide();
						$('div#certificado-detalhes .modal-body div.form').fadeIn('fast');
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#nova-resposta').on('submit','form[name=form-corrigir-resultado]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		if (form.find('input[name=no_arquivo]').val()=='') {
			erros.push('O campo arquivo é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-resultado div').html(erros.join('<br />'));
			if (!form.find('div.erros-resultado').is(':visible')) {
				form.find('div.erros-resultado').slideDown();
			}
		} else {
			form.find('div.erros-resultado').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/inserir-correcao',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						if (typeof retorno.historico !== "undefined") {
							var novaLinha = $("<tr>");
							var colunas = "";
							colunas += '<td>'+retorno.usuario+'</td>';
							colunas += '<td>'+retorno.dt_formatada+'</td>';
							colunas += '<td>'+retorno.historico.de_observacao+'</td>';
							
							novaLinha.append(colunas);
							$("table#historico-pedido").prepend(novaLinha);
						}						
						form.find('input[type=reset]').trigger('click');
						swal("Sucesso!", "Correção salva com sucesso.", "success").then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});