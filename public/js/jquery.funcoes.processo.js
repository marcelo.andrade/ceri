$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('div#novo-processo').on('show.bs.modal', function (event) {
		$.ajax({
			type: "GET",
			url: 'processo/novo-processo',
			beforeSend: function() {
				$('div#novo-processo .modal-body div.carregando').show();
				$('div#novo-processo .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#novo-processo .modal-body div.form').html(retorno);
				$('div#novo-processo .modal-body div.carregando').hide();
				$('div#novo-processo .modal-body div.form').fadeIn('fast');
				var form = $('div#novo-processo').find('form[name=form-novo-processo]');
				$("table#imoveis-penhora tbody tr td .listaproprietarios").each(function(){
					var in_passivo_penhora = $(this).data('in_passivo_penhora');
					var nome_proprietario  = $(this).data('nome_proprietario');
					var nu_cpf_cnpj  	   = $(this).data('nu_cpf_cnpj');
					var tp_pessoa  	  	   = $(this).data('tp_pessoa');

					if (in_passivo_penhora == 'S')
					{
						var novaLinha 		   = $("<tr>");
						var colunas 		   = "";

						colunas += '<td><input type="hidden" name="a_tp_pessoa[]" value="'+tp_pessoa+'" /><input type="hidden" name="a_nu_cpf_cnpj[]" class="nu_cpf_cnpj" value="'+nu_cpf_cnpj+'" />'+nu_cpf_cnpj+'</td>';
						colunas += '<td><input type="hidden" name="a_no_parte[]" value="'+nome_proprietario+'" />'+nome_proprietario+'</td>';
						colunas += '<td><input type="hidden" name="a_id_tipo_parte[]" class="id_tipo_parte" value="2" />Réu</td>';

						novaLinha.append(colunas);
						$("table#partes-processo").prepend(novaLinha);
					}

				})
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
			}
		});
	});
	$('div#novo-processo').on('hidden.bs.modal', function (event) {
		$('div#novo-processo .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-processo .modal-body div.form').html('');
	});
	
	$('div#novo-processo').on('click','input[name=tp_pessoa]',function(e) {
		var form = $('div#novo-processo').find('form[name=form-novo-processo]');
		form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',false);
		form.find('input[name=no_parte]').val('').prop('disabled',false).prop('readonly',false);
		switch ($(this).val()) {
			case 'F':
				form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('(CPF)');
				form.find('input[name=nu_cpf_cnpj]').mask('000.000.000-00');
				break;
			case 'J':
				form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('(CNPJ)');
				form.find('input[name=nu_cpf_cnpj]').mask('00.000.000/0000-00');
				break;
		}
	});
	
	$('div#novo-processo').on('click','button.cadastrar-parte',function(e) {
		var form = $('div#novo-processo').find('form[name=form-novo-processo]');
		var tipo = $(this).data('tipo');		
		var erros = new Array();
		if (form.find('input[name=tp_pessoa]:checked').length<=0) {
			erros.push('O tipo de pessoa é obrigatório');
		} else {
			var tp_pessoa = form.find('input[name=tp_pessoa]:checked').val();
			if (form.find('input[name=nu_cpf_cnpj]').val()=='') {
				erros.push('O campo '+(tp_pessoa=='F'?'CPF':'CNPJ')+' é obrigatório');
			} else {
				var v_cpf_cnpj_aux = form.find('input[name=nu_cpf_cnpj]').val().replace(/[^0-9]+/g,'');
				if (form.find('input.nu_cpf_cnpj[value="'+v_cpf_cnpj_aux+'"]').length>0) {
					switch (form.find('input[name=tp_pessoa]:checked').val()) {
						case 'F':
							erros.push('Já existe uma parte cadastrada com este CPF');
							break;
						case 'J':
							erros.push('Já existe uma parte cadastrada com este CNPJ');
							break;
					}
				} else {
					switch (form.find('input[name=tp_pessoa]:checked').val()) {
						case 'F':
							if (!validarCPF(form.find('input[name=nu_cpf_cnpj]').val())) {
								erros.push('O CPF digitado é inválido');
							}
							break;
						case 'J':
							if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj]').val())) {
								erros.push('O CNPJ digitado é inválido');
							}
							break;
					}
				}
			}
		}
		if (form.find('input[name=no_parte]').val()=='') {
			erros.push('O campo nome da parte é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-partes div').html(erros.join('<br />'));
			if (!form.find('div.erros-partes').is(':visible')) {
				form.find('div.erros-partes').slideDown();
			}
		} else {
			form.find('div.erros-partes').slideUp();
			var tp_pessoa = form.find('input[name=tp_pessoa]:checked').val();
			var nu_cpf_cnpj = form.find('input[name=nu_cpf_cnpj]').val();
			var no_parte = form.find('input[name=no_parte]').val();
			switch (tipo) {
				case 1:
					var tipo_desc = 'Autor';
					break;
				case 2:
					var tipo_desc = 'Réu';
					break;
			}
			var novaLinha 		= $("<tr>");
			var colunas 		= "";
				nu_cpf_cnpj		= nu_cpf_cnpj.replace(/[^0-9]+/g,'');
			colunas += '<td><input type="hidden" name="a_tp_pessoa[]" value="'+tp_pessoa+'" /><input type="hidden" name="a_nu_cpf_cnpj[]" class="nu_cpf_cnpj" value="'+nu_cpf_cnpj+'" />'+nu_cpf_cnpj+'</td>';
			colunas += '<td><input type="hidden" name="a_no_parte[]" value="'+no_parte+'" />'+no_parte+'</td>';
			colunas += '<td><input type="hidden" name="a_id_tipo_parte[]" class="id_tipo_parte" value="'+tipo+'" />'+tipo_desc+'</td>';
			
			novaLinha.append(colunas);
			$("table#partes-processo").prepend(novaLinha);
			
			form.find('input[name=tp_pessoa]:checked').prop('checked',false);
			form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('');
			form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',true);
			form.find('input[name=no_parte]').val('').prop('readonly',false).prop('disabled',true);
		}
	});
	
	
	$('div#novo-processo').on('click','button.incluir-processo',function(e) {
		e.preventDefault();
		var form = $('div#novo-processo').find('form[name=form-novo-processo]');
		var erros = new Array();
		if (form.find('input[name=numero_processo]').val()=='') {
			erros.push('O campo número do processo é obrigatório');
		} else if (total_processos(form.find('input[name=numero_processo]').val())>0) {
			erros.push('O número do processo já foi cadastrado');
		}
		if (form.find('select[name=id_natureza_acao]').val()=='0') {
			erros.push('O campo natureza do processo é obrigatório');
		}
		if (form.find('input.id_tipo_parte[value=1]').length==0) {
			erros.push('Um autor do processo é obrigatório');
		}
		if (form.find('input.id_tipo_parte[value=2]').length==0) {
			erros.push('Um réu do processo é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: 'processo/inserir-processo',
				data: form.serialize(),
				beforeSend: function() {
					$('div#novo-processo .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div#novo-processo .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						alert('Erro desconhecido, tente novamente mais tarde');
					} else {
						retornoProcesso(retorno);
						$('div#nova-pesquisa form[name=form-nova-pesquisa]').find('input[name=numero_processo]').val(retorno.numero_processo);
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
				}
			});
		}
	});

	$('div#novo-processo').on('blur','input[name=nu_cpf_cnpj]', function (event) {
		var form = $('div#novo-processo').find('form[name=form-novo-processo]');
		var nu_cpf_cnpj = form.find('input[name=nu_cpf_cnpj]').val();
		if (nu_cpf_cnpj!='') {
			switch (form.find('input[name=tp_pessoa]:checked').val()) {
				case 'F':
					if (!validarCPF(form.find('input[name=nu_cpf_cnpj]').val())) {
						return false;
					}
					break;
				case 'J':
					if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj]').val())) {
						return false;
					}
					break;
			}
			$.ajax({
				type: "POST",
				url: 'processo/busca-parte',
				data: 'nu_cpf_cnpj='+nu_cpf_cnpj,
				beforeSend: function() {
					$('div#novo-processo .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					if (retorno=='ERRO') {
						form.find('input[name=no_parte]').prop('readonly',false);
					} else {
						if (retorno.no_parte!='') {
							form.find('input[name=no_parte]').val(retorno.no_parte).prop('readonly',true);
						} else {
							form.find('input[name=no_parte]').val('').prop('readonly',false);
						}
					}
					$('div#novo-processo .modal-body div.carregando').hide().removeClass('flutuante');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
				}
			});
		}
	});
});
