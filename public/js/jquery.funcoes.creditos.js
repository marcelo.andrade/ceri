var valor_compra = 0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(document).ajaxSuccess(function(e){
		$("input[name='validade_cartao']").mask('99/9999');
		$("input[name='nu_cartao_credito']").mask('9999-9999-9999-9999');
		//$("input[name='nu_seguranca']").mask('999?9');
	});

	$("div#nova-compra .modal-body div.form").on('click','.credito',function () {
		$('div#nova-compra input[name=tpcartao][value="1"]').prop('checked', true);
		$('div#nova-compra input[name=tpcartao][value="2"]').prop('checked', false);
		$('div#nova-compra input[name=tpcartao][value="3"]').prop('checked', false);
		$('div#nova-compra .bandeiras_credito').show();
        $('div#nova-compra .bandeiras_debito').hide();
	});

	$("div#nova-compra .modal-body div.form").on('click','.debito',function () {
		$('div#nova-compra input[name=tpcartao][value="1"]').prop('checked', false);
		$('div#nova-compra input[name=tpcartao][value="2"]').prop('checked', true);
		$('div#nova-compra input[name=tpcartao][value="3"]').prop('checked', false);
        $('div#nova-compra .bandeiras_credito').hide();
        $('div#nova-compra .bandeiras_debito').show();
	});

	$("div#nova-compra .modal-body div.form").on('click','.boleto',function () {
		$('div#nova-compra input[name=tpcartao][value="1"]').prop('checked', false);
		$('div#nova-compra input[name=tpcartao][value="2"]').prop('checked', false);
		$('div#nova-compra input[name=tpcartao][value="3"]').prop('checked', true);

	});

	$('div#nova-compra').on('show.bs.modal', function (event) {
		$('div#nova-compra .modal-body div.form').html('');
		$.ajax({
			type: "POST",
			url: url_base+'creditos/nova-compra',
			beforeSend: function() {
				$('div#nova-compra .modal-body div.carregando').show();
				$('div#nova-compra .modal-body div.form').hide();
			},
			success: function(retorno) {
				event.stopPropagation();
				$('div#nova-compra .modal-body div.form').html(retorno);
				$('div#nova-compra .modal-body div.carregando').hide();
				$('div#nova-compra .modal-body div.form').fadeIn('fast');
				iniciar_mascaras();
				$("div#nova-compra .modal-body div.form input[name=vl_compra]").autoNumeric('set',valor_compra);
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});

	$('#btn_cielo').on('click', function() {
		if (validaFormulario() == true) {

			$(".erros-cartao").slideUp();

			if ($('div#nova-compra input[name=tpcartao]:checked').val() == 1) { //credito
				var actionUrl = 'creditos/inserir-compra-cartao-credito';
				enviarTransacao(actionUrl);
			} else if ($('div#nova-compra input[name=tpcartao]:checked').val() == 2) { //debito
				var actionUrl =  'creditos/inserir-compra-cartao-debito';
                enviarTransacao(actionUrl, function(){
                    valida_transacao_debito();
                });
			} else {
				var actionUrl =  'creditos/inserir-compra-boleto';

				swal({
					title: "<h3><strong>Deseja continuar com o processo de pagmento de Boleto!</strong></h3><br />",
					text: 'Será aberta uma nova janela com o Boleto solicitado.<br /><br /><strong>Deseja continuar?</strong>',
					type: "warning",
					showCancelButton: true,
					cancelButtonText: 'Não',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim'
				}).then(function(dataRetorno ) {
					if (dataRetorno == true) {
						$.ajax({
							type: "POST",
							url:  url_base + actionUrl,
							async: false,
							success: function( data ) {
								if ( parseInt( data ) )
								{
									$("div#nova-compra").modal('hide');
									window.open(url_base+'creditos/gerar-boleto/'+data,'nova');return false;
								}else{
									swal("Erro!", data+". Erro nº X", "error").then(function() {
										location.reload();
									});
								}
							},
							error: function (request, status, error) {
								console.log(request.responseText);
								swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
									location.reload();
								});
							}
						});
					} else {
						cancelar_transacao();
					}
				})
			}
		}
	});

	$('div#nova-compra').on('hidden.bs.modal', function (event) {
		$('div#nova-compra .modal-body div.carregando').removeClass('flutuante').show();
		$('div#nova-compra .modal-body div.form').html('');
	});

	$("div#nova-compra").on('blur','input[name=nu_cartao_credito]', function(){
		var numeroCartao = $(this).unmask();

		if (numeroCartao.val() == "")
			return false;

		var bandeira 	 = buscaBandeiraCartao(numeroCartao.val());
		if ( bandeira != '') {
			$("#id_bandeira").val( bandeira );
			$("#bandeira").html('<img src="'+url_base+'images/icone-'+bandeira+'-01.png" alt="'+bandeira+'" />');
		} else {
            swal("Erro!", "Cartão inválido e/ou bandeira não encontrada", "error");
		}
	});

	$("div#nova-compra").on('blur','input[name=validade_cartao]', function(){
		validaDataVencimento( $(this) );
	});

});

function validaFormulario()
{
	var erros = new Array();

	if ( $('div#nova-compra input[name=tpcartao][value="3"]').prop('checked') == false) {

		if ($("#vl_compra").val() == '') {
			erros.push('O campo valor da compra é obrigatório');
		}
		if ($("#nu_cartao_credito").val() == '') {
			erros.push('O campo Número de segurança é obrigatório');
		}
		if ($("#titular_cartao").val() == '') {
			erros.push('O campo Titular do cartão é obrigatório');
		}
		if ($("#nu_seguranca").val() == '') {
			erros.push('O campo código de segurança é obrigatório');
		}
		if ($("#id_bandeira").val() == '') {
			erros.push('O campo Número cartão é inválido não conseguimos detectar sua Bandeira');
		}

		if ($('div#nova-compra input[name=tpcartao]:checked').val() == 2) {

			if ($("#id_bandeira").val() != 'visa' && $("#id_bandeira").val() != 'mastercard') {
				erros.push('Para a função Débito, só é permitida as Bandeiras Visa e Mastercard');
			}
		}
        if ($('#hidden_captcha_pagamento').val() == ''){
            erros.push('reCAPTCHA não validado. Por favor selecione a opção.');
        }
	}else{
		//validar campos boleto
		return true;
	}

	if (erros.length>0)
	{
		$('.menssagem').html(erros.join('<br />'));
		$(".erros-cartao").slideDown()
		return false;
	}
	return true;
}

function enviarTransacao(actionUrl, callback)
{
	$.ajax({
		type: "POST",
		url:  url_base + actionUrl,
		data: $("#form-cartao-credito").serialize(),
		beforeSend: function() {
			$('div#nova-compra .modal-body div.carregando').show();
			$('div#nova-compra .modal-body div.form').hide();
		},
		success: function(retorno) {
			$('div#nova-compra .modal-body div.carregando').hide().removeClass('flutuante');

            switch (retorno.status) {
                case 'sucesso':
                	if (retorno.tipo == 'debito') {
                        swal({
                            title: "<h3><strong>Atenção você será redirecionado para o banco emissor do seu cartão de débito!</strong></h3><br />",
                            text: retorno.msg,
                            type: "warning",
                            showCancelButton: true,
                            cancelButtonText: 'Não',
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim'
                        }).then(function(dataRetorno) {
                            if (dataRetorno == true) {
                                var left 	  = ($(document).width()/2) - 300;
                                window.open(retorno.url,'nova','width=800,height=600,toolbar=yes,scrollbars=yes,resizable=no,top=40,left='+left+',width=600,height=500');
                                return false;
                            } else {
                                //cancelar_transacao();
                            }
                        });
					} else {
                        var saldoNovo = $("input[name=vl_compra]").autoNumeric('get');
                        var id_compra_credito = retorno.id_compra_credito;

                        //var resultado_retorno = retorno;
                        var alerta = swal("Sucesso", retorno.msg, "success").then(function() {
							var saldoAtual = parseFloat($('input[name=saldo_atual]').val());
							var total = parseFloat(saldoAtual) + parseFloat(saldoNovo);

							$('input[name=saldo_atual]').val(total.toFixed(2));
							$('input[name=id_compra_credito]').val(id_compra_credito);
							$('label.saldo span').autoNumeric('set',total.toFixed(2));
							$('div#nova-compra').modal('hide');

							// Adiciona o envio de pesquisa ou certidao após as compra de credito
                            if ($('div#nova-pesquisa button.incluir-pesquisa').length) {
                                $('div#nova-pesquisa button.incluir-pesquisa').trigger('click');
                            }
							if ($('div#nova-certidao button.enviar-pedidos').length && $('div#nova-certidao').is(':visible')) {
								$('div#nova-certidao button.enviar-pedidos').trigger('click');
								$('div.swal2-container button.swal2-confirm').trigger('click');
							} else if ($('table#pedidos-pendentes').length && $('table#pedidos-pendentes a.enviar-pedido[data-idpedido="' + v_id_pedido + '"]')) {
                                $('table#pedidos-pendentes a.enviar-pedido[data-idpedido="' + v_id_pedido + '"]').trigger('click');
                                $('div.swal2-container button.swal2-confirm').trigger('click');
							}
                        });
					}
                    break;
                case 'erro':
                    var alerta = swal("Erro!", retorno.msg, "error");
                    break;
                case 'alerta':
                    var alerta = swal("Ops!", retorno.msg, "warning");
                    break;
            }

            if (retorno.tipo !== 'debito') {
				alerta.then(function () {
					$('div#nova-compra').modal('hide');

					if (retorno.recarrega) {
						location.reload();
					}
				});
            }
		},
		complete: callback,
		error: function (request, status, error) {
			console.log(request.responseText);
			swal({
				title: "ERRO COM A SUA TRANSAÇÃO!",
				text: 'Erro ao fazer uma requisição contacte o administrador do sistema',
				type: "error",
				showCancelButton: false,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK'
			}).then(function( retorno ) {
				window.location.reload();
			})
		}
	});
}

function validaDataVencimento( data )
{
	var mesInformado = data.val().substring(0,2);
	var anoInformado = data.val().substring(3,7);

	var mesInformado = parseInt( mesInformado );
	var anoInformado = parseInt( anoInformado );

	var novaData = new Date();

	var mesmoMes =  parseInt(novaData.getMonth())+1;
	var mesmoAno =  parseInt(novaData.getFullYear());

	if ( mesInformado < 1 || mesInformado > 12 )
	{
		alert('Data informada inválida');
		data.val('').focus();
		return false;
	} else if ( mesInformado < mesmoMes){
		if ( anoInformado <= mesmoAno )
		{
			alert('Data informada inválida');
			data.val('').focus();
			return false;
		}
	}

	if ( anoInformado < mesmoAno )
	{
		alert('Data informada inválida');
		data.val('').focus();
		return false;
	}

	return true;
}

function buscaBandeiraCartao(numeroCartao)
{
	var regexVisa 		= /^4[0-9]{12}(?:[0-9]{3})?/;
	var regexMaster 	= /^5[1-5][0-9]{14}|(2(?:2[2-9][^0]|2[3-9]|[3-6]|22[1-9]|7[0-1]|72[0]))/;
	var regexAmex 		= /^3[47][0-9]{13}/;
	var regexDiners 	= /^3(?:0[0-5]|[68][0-9])[0-9]{11}/;
	var regexDiscover 	= /^6(?:011|5[0-9]{2})[0-9]{12}/;
	var regexJCB 		= /^(?:2131|1800|35\d{3})\d{11}/;
	var regexElo 		= /^401178|^401179|^431274|^438935|^451416|^457393|^457631|^457632|^504175|^627780|^636297|^636368|^(506699|5067[0-6]\d|50677[0-8])|^(50900\d|5090[1-9]\d|509[1-9]\d{2})|^65003[1-3]|^(65003[5-9]|65004\d|65005[0-1])|^(65040[5-9]|6504[1-3]\d)|^(65048[5-9]|65049\d|6505[0-2]\d|65053     [0-8])|^(65054[1-9]|6505[5-8]\d|65059[0-8])|^(65070\d|65071[0-8])|^65072[0-7]|^(65090[1-9]|65091\d|650920)|^(65165[2-9]|6516[6-7]\d)|^(65500\d|65501\d)|^(65502[1-9]|6550[3-4]\d|65505[0-8])\d+$/;
	var regexEletron	= /^(4026|417500|4405|4508|4844|4913|4917)/;

	if(regexElo.test(numeroCartao)){
		return 'elo';
	}
	if(regexVisa.test(numeroCartao)){
		return 'visa';
	}
	if(regexMaster.test(numeroCartao)){
		return 'mastercard';
	}
	if(regexAmex.test(numeroCartao)){
		return 'americanexpress';
	}
	if(regexDiners.test(numeroCartao)){
		return 'diners';
	}
	if(regexDiscover.test(numeroCartao)){
		return 'discover';
	}
	if(regexJCB.test(numeroCartao)){
		return 'jcb';
	}

	if(regexEletron.test(numeroCartao)){
		return 'visa';
	}

	return '';

}

function saldo_insuficiente() {
	swal({
		title: 'Saldo insuficiente!',
		text: "Deseja comprar mais créditos?",
		type: 'warning',
		showCancelButton: true,
		cancelButtonText: 'Não',
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sim'
	}).then(function(retorno) {
		if (retorno == true) {
			$("#nova-compra").modal('show');
		}
	});
}

function SomenteNumero(e){
	var tecla=(window.event)?event.keyCode:e.which;
	if((tecla>47 && tecla<58)) return true;
	else{
		if (tecla==8 || tecla==0) return true;
		else  return false;
	}
}

function valida_transacao_debito() {
	$.ajax({
		type: "POST",
		url: url_base+'creditos/valida-transacao-debito',
		success: function(retorno) {
			if (retorno.total>0.0) {
                var alerta = swal("Sucesso", retorno.msg, "success").then(function() {
                    $('input[name=saldo_atual]').val(retorno.total);
                    $('label.saldo span').autoNumeric('set', retorno.total);
                    $('input[name=id_compra_credito]').val(retorno.id_compra_credito);
                    $('div#nova-compra').modal('hide');

                    // Adiciona o envio de pesquisa ou certidao após as compra de credito
                    if ($('div#nova-pesquisa button.incluir-pesquisa').length) {
                        $('div#nova-pesquisa button.incluir-pesquisa').trigger('click');
                    }
                    if ($('div#nova-certidao button.enviar-pedidos').length && $('div#nova-certidao').is(':visible')) {
                        $('div#nova-certidao button.enviar-pedidos').trigger('click');
                        $('div.swal2-container button.swal2-confirm').trigger('click');
                    } else if ($('table#pedidos-pendentes').length && $('table#pedidos-pendentes a.enviar-pedido[data-idpedido="' + v_id_pedido + '"]')) {
                        $('table#pedidos-pendentes a.enviar-pedido[data-idpedido="' + v_id_pedido + '"]').trigger('click');
                        $('div.swal2-container button.swal2-confirm').trigger('click');
                    }
				});
			} else {
                setTimeout(valida_transacao_debito(), 50000);
            }
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
}

function cancelar_transacao()
{
	$.ajax({
		type: "POST",
		url:  url_base+'creditos/cancelar-transacao',
		success: function(retorno) {
			switch (retorno.status) {
                case 'sucesso':
                    var alerta = swa('Sucesso', retorno.msg, 'success');
                    break;
                case 'erro':
                    var alerta = swa('Erro', retorno.msg, 'error');
                    break;
            }

            alerta.then(function () {
                $('div#nova-compra .modal-body div.carregando').hide().removeClass('flutuante');
                $('div#nova-compra').modal('hide');

                if (retorno.recarrega) {
                    location.reload();
                }
			});
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
}

//<!-- CAPTCHA -->
var widgetPgtoCredito;
var onloadCallback = function() {
    widgetPgtoCredito = grecaptcha.render('recaptcha_pgto_credito', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'theme'   : 'light'
    });
};