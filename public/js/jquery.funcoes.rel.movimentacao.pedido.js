$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var form = $('form[name=form-relatorio-movimentacao-pedido]');

	form.on('reset',function(e) {
		e.preventDefault();
		window.location.reload();
	});


	$("input[name=relatorioPesquisar]").click(function(){
		if ( validaFormulario() )
		{
			$('form[name=form-relatorio-movimentacao-pedido]').submit();
			setTimeout(function(){
				window.location.reload();
			},500)
		}
	})

});


function validaFormulario() {
	var erros = new Array();
	if ($('input[name=dt_inicio]').val() == '' && $('input[name=dt_fim]').val() == '') {
		erros.push('O campo período deve ser informado com a data inicial e final.');
	}
	if ($('select[name=id_modulo]').val() == 0) {
		erros.push('O campo módulo deve ser informado');
	}

	if (erros.length>0) {
		$('#menssagem').html(erros.join('<br />'));
		$('.erros').slideDown();
		return false;
	}else{
		$('.erros').slideUp();
		return true;
	}
}
