total_selecionadas=0;
total_valor=0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var form_comp 		= $('form[name=form-novo-comprovante-caixa]');

	$('div#novo-comprovante-caixa').on('show.bs.modal', function (event) {
        var id_alienacao_valor_repasse_lote = $(event.relatedTarget).data('idalienacaovalorrepasselote');

        if (id_alienacao_valor_repasse_lote>0) {
            $.ajax({
                type: "POST",
                url: 'alienacao-pagamento/novo-comprovante-caixa',
                data: 'id_alienacao_valor_repasse_lote='+id_alienacao_valor_repasse_lote,
                beforeSend: function() {
                    $('div#novo-comprovante-caixa .modal-body div.carregando').show();
                    $('div#novo-comprovante-caixa .modal-body div.form form').hide();
                },
                success: function(retorno) {
                    $('div#novo-comprovante-caixa .modal-body div.form form').html(retorno);
                    $('div#novo-comprovante-caixa .modal-body div.carregando').hide();
                    $('div#novo-comprovante-caixa .modal-body div.form form').fadeIn('fast');
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
        }
    });


    $('div#novo-comprovante-caixa').on('click','button.enviar-comprovante',function(e) {
        e.preventDefault();
        $(this).attr('disabled','disabled');

        var erros = new Array();

        if (form_comp.find('input[name=no_arquivo_comprovante]').val()=='') {
            erros.push('O campo arquivo do comprovante é obrigatório.');
        }
        if (erros.length>0) {
            form_comp.find('div.erros div').html(erros.join('<br />'));
            if (!form_comp.find('div.erros').is(':visible')) {
                form_comp.find('div.erros').slideDown();
            }
        } else {
            form_comp.find('div.erros').slideUp();
            var data = new FormData(form_comp.get(0));
            $.ajax({
                type: "POST",
                url: 'alienacao-pagamento/inserir-comprovante-caixa',
                data: data,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('div#novo-comprovante .modal-body div.carregando').addClass('flutuante').show();
                },
                success: function(retorno) {
                    console.log(retorno);
                    switch (retorno.status) {
                        case 'erro':
                            var alerta = swal("Erro!",retorno.msg,"error");
                            break;
                        case 'sucesso':
                            var alerta = swal("Sucesso!",retorno.msg,"success");
                            break;
                        case 'alerta':
                            var alerta = swal("Ops!",retorno.msg,"warning");
                            break;
                    }
                    if (retorno.recarrega=='true') {
                        alerta.then(function(){
                            location.reload();
                        });
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
        }
    });


    $('div#detalhe-historico-pagamento-caixa').on('show.bs.modal', function (event) {
        var id_alienacao_valor_repasse_lote = $(event.relatedTarget).data('idalienacaovalorrepasselote');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_alienacao_valor_repasse_lote>0)
        {
            $.ajax({
                type: "POST",
                url: 'alienacao-pagamento/detalhe-historico-pagamento-caixa',
                data: 'id_alienacao_valor_repasse_lote='+id_alienacao_valor_repasse_lote,
                beforeSend: function () {
                    $('div#detalhe-historico-pagamento-caixa .modal-body div.carregando').show();
                    $('div#detalhe-historico-pagamento-caixa .modal-body div.form').hide();
                },
                success: function (retorno) {
                    $('div#detalhe-historico-pagamento-caixa .modal-body div.form ').html(retorno);
                    $('div#detalhe-historico-pagamento-caixa .modal-body div.carregando').hide();
                    $('div#detalhe-historico-pagamento-caixa .modal-body div.form ').fadeIn('fast');
                    total_selecionadas = 0;
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                        location.reload();
                    });
                }
            });
        }
    });

    $('div#detalhe-historico-pagamento-caixa').on('hidden.bs.modal', function (event) {
        $('div#detalhe-historico-pagamento-caixa .modal-body div.carregando').removeClass('flutuante').show();
        $('div#detalhe-historico-pagamento-caixa .modal-body div.form form').html('');
    });

    $('div#visualizar-arquivo').on('show.bs.modal', function (event) {
        var id_alienacao_valor_repasse_lote = $(event.relatedTarget).data('idalienacaovalorrepasselote');
        var origem = $(event.relatedTarget).data('origem');
        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao-pagamento/gerar-recibo-caixa',
            data: {'id_alienacao_valor_repasse_lote':id_alienacao_valor_repasse_lote, 'origem':origem},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, true);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($('div#visualizar-arquivo'));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });





});