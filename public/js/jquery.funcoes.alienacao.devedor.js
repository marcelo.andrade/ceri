total_devedores = 0;
$(document).ready(function() {

	////////////////////////////////////////////////////////////
	// Eventos do botão "Inserir devedor"                     //
	////////////////////////////////////////////////////////////

	$('div#novo-devedor').on('show.bs.modal', function (event) {
		var id_cidade = $(event.relatedTarget).data('idcidade');
		var alienacao_token = $(event.relatedTarget).data('alienacaotoken');
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/devedor/novo',
			data: 'alienacao_token='+alienacao_token,
			beforeSend: function() {
				$('div#novo-devedor .modal-body div.carregando').show();
				$('div#novo-devedor .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#novo-devedor .modal-body div.form form').html(retorno);
				$('div#novo-devedor .modal-body div.carregando').hide();
				$('div#novo-devedor .modal-body div.form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#novo-devedor').on('hidden.bs.modal', function (event) {
		$('div#novo-devedor .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-devedor .modal-body div.form form').html('');
	});
	$('div#novo-devedor').on('click','input[name=tp_pessoa]',function(e) {
		var form = $('div#novo-devedor').find('form[name=form-novo-devedor]');
		form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case 'F':
				form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('(CPF)');
				form.find('input[name=nu_cpf_cnpj]').mask('000.000.000-00');
				break;
			case 'J':
				form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('(CNPJ)');
				form.find('input[name=nu_cpf_cnpj]').mask('00.000.000/0000-00');
				break;
		}
	});
	$('div#novo-devedor').on('click','button.inserir-devedor',function(e) {
		var erros = new Array();
		var form = $('div#novo-devedor').find('form[name=form-novo-devedor]');
		
		var nu_cpf_cnpj = form.find('input[name=nu_cpf_cnpj]').val();
		var no_devedor = form.find('input[name=no_devedor]').val();

		if (form.find('input[name=tp_pessoa]:checked').length<=0) {
			erros.push('Selecione um tipo de documento identificador');
		} else if (nu_cpf_cnpj=='') {
			erros.push('O campo documento identificador é obrigatório');
		} else {
			if ($('table#devedores input.devedores_nu_cpf_cnpj[value="'+nu_cpf_cnpj+'"]').length>0 || $('table#novo-devedores input.a_nu_cpf_cnpj[value="'+nu_cpf_cnpj+'"]').length>0) {
				erros.push('Este devedor já foi inserido');
			} else {
				switch (form.find('input[name=tp_pessoa]:checked').val()) {
					case 'F':
						if (!validarCPF(nu_cpf_cnpj)) {
							erros.push('O CPF digitado é inválido');
						}
						break;
					case 'J':
						if (!validarCNPJ(nu_cpf_cnpj)) {
							erros.push('O CNPJ digitado é inválido');
						}
						break;
				}
			}
		}
		if (no_devedor=='') {
			erros.push('O campo nome do devedor é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-devedor div').html(erros.join('<br />'));
			if (!form.find('div.erros-devedor').is(':visible')) {
				form.find('div.erros-devedor').slideDown();
			}
		} else {
			var total_linhas = $("table#novo-devedores tbody tr").length;
			var nova_linha = $('<tr id="'+(total_linhas+1)+'">');
			var colunas = "";
			colunas += '<td>';
				colunas += '<input type="hidden" name="a_nu_cpf_cnpj[]" class="a_nu_cpf_cnpj" id="a_nu_cpf_cnpj_'+total_linhas+'" value="'+nu_cpf_cnpj+'" />';
				colunas += '<input type="hidden" name="a_no_devedor[]" class="a_no_devedor" id="a_no_devedor_'+total_linhas+'" value="'+no_devedor+'" />';
				colunas += nu_cpf_cnpj;
			colunas += '</td>';
			colunas += '<td>'+no_devedor+'</td>';
			colunas += '<td>';
				colunas += '<a href="#" class="remover-devedor btn btn-black btn-sm" data-linha="'+(total_linhas+1)+'">Remover</a>';
			colunas += '</td>';

			nova_linha.append(colunas);
			$("table#novo-devedores tbody").append(nova_linha);
			$('div#novo-devedor button.cancelar-devedor').trigger('click');
		}
	});
	$('div#novo-devedor').on('click','table#novo-devedores a.remover-devedor',function() {
		var linha = $(this).data('linha');
		$('table#novo-devedores tr#'+linha).remove();
	});
	$('div#novo-devedor').on('click','button.cancelar-devedor',function(e) {
		var form = $('div#novo-devedor').find('form[name=form-novo-devedor]');
		form.find('div.erros-devedor').slideUp();
		form.find('input[name=tp_pessoa]:checked').prop('checked',false);
		form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',true);
		form.find('input[name=no_devedor]').val('');
	});
	$('div#novo-devedor').on('click','button.inserir-devedores',function(e) {
		var erros = new Array();
		var form = $('div#novo-devedor').find('form[name=form-novo-devedor]');
		
		var alienacao_token = form.find('input[name=alienacao_token]').val();
		
		if ($('table#novo-devedores input.a_nu_cpf_cnpj').length<=0) {
			erros.push('Ao menos um devedor deve ser inserido');
		}

		if (erros.length>0) {
			form.find('div.erros-devedor div').html(erros.join('<br />'));
			if (!form.find('div.erros-devedor').is(':visible')) {
				form.find('div.erros-devedor').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/devedor/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#novo-devedor .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							devedores = retorno.devedores;
							swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
								if (devedores.length>0) {
									$.each(devedores,function(key,devedor) {
										var nova_linha = $('<tr id="'+total_devedores+'">');
										var colunas = "";
										colunas += '<td>';
											colunas += '<input type="hidden" name="devedores_nu_cpf_cnpj[]" class="devedores_nu_cpf_cnpj" value="'+devedor.nu_cpf_cnpj+'" />';
											colunas += devedor.nu_cpf_cnpj;
										colunas += '</td>';
										colunas += '<td>'+devedor.no_devedor+'</td>';
										colunas += '<td>';
											colunas += '<a href="#" class="remover-devedor btn btn-black btn-sm" data-linha="'+total_devedores+'" data-alienacaotoken="'+alienacao_token+'">Remover</a>';
										colunas += '</td>';

										nova_linha.append(colunas);
										$("table#devedores tbody").append(nova_linha);
										total_devedores++;
									});
								}

								$('div#novo-devedor').modal('hide');
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
	$('div#nova-alienacao').on('click','table#devedores a.remover-devedor',function() {
		var linha = $(this).data('linha');
		var alienacao_token = $(this).data('alienacaotoken');

		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/devedor/remover',
			data: 'alienacao_token='+alienacao_token+'&linha='+linha,
			beforeSend: function() {
				$('div#nova-alienacao .modal-body div.carregando').addClass('flutuante').show();
			},
			success: function() {
				$('div#nova-alienacao .modal-body div.carregando').removeClass('flutuante').hide();
				$('table#devedores').find('tr#'+linha).remove();
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
});