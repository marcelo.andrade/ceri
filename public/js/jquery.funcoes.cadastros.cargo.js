$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	form = $('form[name=form-novo-cargo]');

	$('div#novo-cargo').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: 'cargos/novo',
			beforeSend: function() {
				$('div#novo-cargo .modal-body div.carregando').show();
				$('div#novo-cargo .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#novo-cargo .modal-body div.form form').html(retorno);
				$('div#novo-cargo .modal-body div.carregando').hide();
				$('div#novo-cargo .modal-body div.form form').fadeIn('fast');
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});

	$('div#novo-cargo').on('click','button.enviar-cargo',function(e) {
		var erros = new Array();
		if (form.find('select[name=id_tipo_usuario]').val()=='0') {
			erros.push('O campo tipo de usuário é obrigatório.');
		}
		if (form.find('input[name=codigo_cargo]').val()=='') {
			erros.push('O campo código é obrigatório.');
		}
		if (form.find('input[name=no_cargo]').val()=='') {
			erros.push('O campo nome é obrigatório.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'cargos/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#novo-cargo .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					$('div#novo-cargo .modal-body div.carregando').hide().removeClass('flutuante');
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#detalhes-cargo').on('show.bs.modal', function (event) {
		var id_cargo = $(event.relatedTarget).data('idcargo');
		var no_cargo = $(event.relatedTarget).data('nocargo');
		
		$(this).find('.modal-header h4.modal-title span').html(no_cargo);
		
		if (id_cargo>0) {
			$.ajax({
				type: "POST",
				url: 'cargos/detalhes',
				data: 'id_cargo='+id_cargo,
				beforeSend: function() {
					$('div#detalhes-cargo .modal-body div.carregando').show();
					$('div#detalhes-cargo .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-cargo .modal-body div.form').html(retorno);
					$('div#detalhes-cargo .modal-body div.carregando').hide();
					$('div#detalhes-cargo .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

});