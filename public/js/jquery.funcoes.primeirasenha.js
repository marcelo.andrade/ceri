$(document).ready(function() {
	$('form[name=form-primeira-senha]').on('submit',function(e) {
		var erros = new Array();
		var form = $(this);
		if (form.find('input[name=senha_usuario_atual]').val()=='') {
			erros.push('O campo senha atual é obrigatório.');
		}
		if (form.find('input[name=senha_usuario]').val()=='') {
			erros.push('O campo nova senha é obrigatório.');
		} else if (form.find('input[name=senha_usuario]').val().length<6) {
			erros.push('A nova senha deve conter no mínimo 6 caracteres.');
		}
		if (form.find('input[name=senha_usuario2]').val()=='') {
			erros.push('O campo confirme a nova senha é obrigatório.');
		} else if (form.find('input[name=senha_usuario2]').val()!=form.find('input[name=senha_usuario]').val()) {
			erros.push('A confirmação da nova senha deve ser identica a nova senha.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
			return false;
		} else {
			form.find('div.erros').slideUp();
			return true;
		}
	});
});