$(document).ready(function() {

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$("#btnfechar-janela").on('click', function (e) {
		var urlOrigem = window.opener.location.href;
		if ( urlOrigem.indexOf("creditos") > 0 )
		{
			window.opener.location.reload();
		}
		window.close();
	})

});

function atualizaSaldo()
{
	var urlOrigem  = window.opener.location.href;
	//var saldoAtual = parseFloat( window.opener.jQuery("#saldo_atual").val() );
	//var total	   = ( parseFloat(saldoAtual) + parseFloat(valor));
	$.ajax({
		type: "POST",
		url:  baseUrl+'creditos/buscar-saldo',
		async: false,
		success: function( total ) {
			//estou forçando a atualização idependentemente se o elemento esta na tela ou não, ete processo é muito sensivel
			window.opener.jQuery(".div_saldo_atual").html('Saldo atual: R$ '+  total);
			window.opener.jQuery("div#nova-compra .modal-body div.carregando").hide().removeClass('flutuante');
			window.opener.jQuery("div#nova-compra").modal('hide');
			window.opener.jQuery("#saldo_atual").val( total );
			if ( urlOrigem.indexOf("creditos") > 0 )
			{
				window.top.location.reload();
			}
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});

}
