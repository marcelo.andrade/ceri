$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[name=form-validar-carta-intimacao]').on('submit', function (e) {
        e.preventDefault();
        var erros = new Array();
        if ($('form[name=form-validar-carta-intimacao]').find('input[name=protocolo_pedido_arquivo]').val()=='') {
            erros.push('O campo número do protocolo é obrigatório.');
        }
        if (erros.length > 0) {
            $('form[name=form-validar-carta-intimacao]').find('div.erros div').html(erros.join('<br />'));
            if (!$('form[name=form-validar-carta-intimacao]').find('div.erros').is(':visible')) {
                $('form[name=form-validar-carta-intimacao]').find('div.erros').slideDown();
            }
        } else {
            $('form[name=form-validar-carta-intimacao]').find('div.erros').slideUp();

            var protocolo_pedido_arquivo = $('form[name=form-validar-carta-intimacao]').find('input[name=protocolo_pedido_arquivo]').val();

            $.ajax({
                type: "POST",
                url: '/validar/validar-carta-intimacao',
                data: {protocolo_pedido_arquivo: protocolo_pedido_arquivo},
                success: function (retorno) {
                    switch (retorno.status) {
                        case 'erro':
                            var alerta = swal("Erro!", retorno.msg, "error");
                            break;
                        case 'sucesso':
                            $('div#resultado-carta-intimacao').find('.modal-header h4.modal-title span').html(protocolo_pedido_arquivo);
                            $('div#resultado-carta-intimacao').find('.modal-body div.form').addClass('carregando').html(retorno.view);
                            $('div#resultado-carta-intimacao').modal('show');
                            break;
                    }
                },
                error: function (request, status, error) {
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro VAL501", "error").then(function () {
                        $('div#resultado-carta-intimacao').modal('hide');
                    });
                }
            });
        }
    });

});
