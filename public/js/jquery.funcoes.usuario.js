$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	/*$('table#usuarios-ativos').DataTable({
		dom: '<<"row clearfix"<"col-md-6"l><"col-md-6"f>><"historico panel table-rounded"t><"row clearfix"<"col-md-6"i><"col-md-6"p>>>',
		"language": {
			"lengthMenu": "Mostrar _MENU_ usuários por página",
			"zeroRecords": "Nenhum usuário foi encontrado",
			"info": "Exibindo página _PAGE_ de _PAGES_",
			"infoEmpty": "Nenhum usuário foi encontrado",
			"infoFiltered": "(filtrado de _MAX_ usuários)",
			"search": "Buscar: ",
			"paginate": {
				"next": "&raquo;",
				"previous": "&laquo;"
			}
		},
		"lengthMenu": [ [10, 50, 100, 500, -1], [10, 50, 100, 500, "Todas"] ]
	});*/
	/*$('table#usuarios-inativos').DataTable({
		dom: '<<"row clearfix"<"col-md-6"l><"col-md-6"f>><"historico panel table-rounded"t><"row clearfix"<"col-md-6"i><"col-md-6"p>>>',
		"language": {
			"lengthMenu": "Mostrar _MENU_ usuários por página",
			"zeroRecords": "Nenhum usuário foi encontrado",
			"info": "Exibindo página _PAGE_ de _PAGES_",
			"infoEmpty": "Nenhum usuário foi encontrado",
			"infoFiltered": "(filtrado de _MAX_ usuários)",
			"search": "Buscar: ",
			"paginate": {
				"next": "&raquo;",
				"previous": "&laquo;"
			}
		},
		"lengthMenu": [ [10, 50, 100, 500, -1], [10, 50, 100, 500, "Todas"] ]
	});*/

	$('div#novo-usuario').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: 'usuarios/novo-usuario',
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU501", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});

	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=tp_pessoa]',function(e) {
		var form = $(this).closest('form');

		switch ($(this).val()) {
			case '0':
				form.find('div.pessoa-fisica, div.pessoa-juridica').slideUp('fast');
				break;
			case 'F':
				form.find('div.pessoa-juridica').slideUp('fast',function() {
					form.find('div.pessoa-juridica').find('input').val('');
					form.find('div.pessoa-juridica').find('select').val(0);
					form.find('div.pessoa-juridica').find('input, select').prop('disabled',true);
				});
				form.find('div.pessoa-fisica').slideDown('fast',function() {
					form.find('div.pessoa-fisica').find('input').val('');
					form.find('div.pessoa-fisica').find('select').val(0);
					form.find('div.pessoa-fisica').find('input, select').prop('disabled',false);
				});
				break;
			case 'J':
				form.find('div.pessoa-fisica').slideUp('fast',function() {
					form.find('div.pessoa-fisica').find('input').val('');
					form.find('div.pessoa-fisica').find('select').val(0);
					form.find('div.pessoa-fisica').find('input, select').prop('disabled',true);
				});
				form.find('div.pessoa-juridica').slideDown('fast',function() {
					form.find('div.pessoa-juridica').find('input').val('');
					form.find('div.pessoa-juridica').find('select').val(0);
					form.find('div.pessoa-juridica').find('input, select').prop('disabled',false);
				});
				break;
		}
		form.find('div.erros').slideUp();
	});

	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_comarca]',function(e) {
		var form = $(this).closest('form');
		var id_comarca = $(this).val();
		var obj_modal = $(this).closest('.modal');

		$.ajax({
			type: "POST",
			url:  url_base+'servicos/listarVaras',
			data: {'id_comarca':id_comarca},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(varas) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_vara]').html('<option value="0">Selecione uma vara</option>');
				if (varas.length>0) {
					$.each(varas,function(key,vara) {
						form.find('select[name=id_vara]').append('<option value="'+vara.id_vara+'">'+vara.no_vara+'</option>');
					});
					form.find('select[name=id_vara]').prop('disabled',false);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER503", "error");
			}
		});
	});

	$('div#novo-usuario, div#alterar-usuario').on('click','input[name=in_digitar_endereco]',function() {
		var form = $(this).closest('form');

		if ($(this).is(':checked')) {
			form.find('input[name=nu_cep]').prop('disabled',false);
			form.find('input[name=no_endereco]').prop('disabled',false);
			form.find('input[name=nu_endereco]').prop('disabled',false);
			form.find('input[name=no_bairro]').prop('disabled',false);
			form.find('input[name=no_complemento]').prop('disabled',false);
			form.find('select[name=id_estado]').prop('disabled',false);
		} else {
			form.find('input[name=nu_cep]').val('').prop('disabled',true);
			form.find('input[name=no_endereco]').val('').prop('disabled',true);
			form.find('input[name=nu_endereco]').val('').prop('disabled',true);
			form.find('input[name=no_bairro]').val('').prop('disabled',true);
			form.find('input[name=no_complemento]').val('').prop('disabled',true);
			form.find('select[name=id_estado]').val('0').prop('disabled',true);
			form.find('select[name=id_cidade]').val('0').prop('disabled',true);
		}
	});

	$('div#novo-usuario, div#alterar-usuario').on('click','input[name=in_digitar_telefone]',function() {
		var form = $(this).closest('form');

		if ($(this).is(':checked')) {
			form.find('select[name=id_tipo_telefone]').prop('disabled',false);
			form.find('input[name=nu_ddd]').prop('disabled',false);
			form.find('input[name=nu_telefone]').prop('disabled',false);
		} else {
			form.find('select[name=id_tipo_telefone]').val('').prop('disabled',true);
			form.find('input[name=nu_ddd]').val('').prop('disabled',true);
			form.find('input[name=nu_telefone]').val('').prop('disabled',true);
		}
	});

	$('div#novo-usuario').on('click','input[name=in_confirmado]',function(e) {
		var form = $(this).closest('form');

		if (!$(this).is(':checked')) {
			swal({
				title: 'Atenção!',
				html: "Tem certeza que deseja alterar o status de confirmação do novo usuário? <br /><br />Além do e-mail com a senha gerada, um segundo e-mail será enviado solicitando a confirmação do e-mail.",
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				showLoaderOnConfirm: true,
			}).catch(function(e) {
				form.find('input[name=in_confirmado]').prop('checked', true);
			});
		}
	});

	$('div#novo-usuario').on('click','input[name=in_aprovado]',function(e) {
		var form = $(this).closest('form');

		if (!$(this).is(':checked')) {
			swal({
				title: 'Atenção!',
				html: "Tem certeza que deseja alterar o status de aprovação do novo usuário? <br /><br />O usuário só poderá acessar quando for aprovado.",
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				showLoaderOnConfirm: true,
			}).catch(function(e) {
				form.find('input[name=in_aprovado]').prop('checked', true);
			});
		}
	});

    $('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_unidade_gestora]',function() {
        var form = $(this).closest('form');

    	if ($(this).val() == 2) {
            form.find('input[name=in_usuario_recebe_notificacoes_sistema]').prop('disabled',false);
            form.find('input[name=in_master_recebe_notificacoes_sistema]').prop('disabled',false);
			form.find('div#chk_notificacao_controladoria').show();
		} else {
            form.find('input[name=in_usuario_recebe_notificacoes_sistema]').prop('disabled',true);
            form.find('input[name=in_master_recebe_notificacoes_sistema]').prop('disabled',true);
            form.find('div#chk_notificacao_controladoria').hide();
		}
    });

	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_tipo_usuario]',function() {
		var form = $(this).closest('form');

		form.find('div.campos-tipo-usuario').hide();
		form.find('div.campos-tipo-usuario select').prop('disabled',true);
		form.find('button.incluir-vinculo').addClass('disabled').prop('disabled',true);
		form.find('button.incluir-todos-vinculos').addClass('disabled').prop('disabled',true);

		if (form.find('div#vinculos-usuario div.vinculo[data-idtipousuario=4]').length<=0 && form.find('div#vinculos-usuario div.vinculo[data-idtipousuario=11]').length<=0) {
			form.find('div#cargo-usuario').hide('fast',function() {
				form.find('select[name=id_cargo]').prop('disabled',true);
			});
		}

		switch ($(this).val()) {
			case '2': case '10':
				form.find('div#campos-serventia').show('fast',function() {
					form.find('select[name=id_cidade_serventia]').prop('disabled',false);
					if (form.find('select[name=id_cidade_serventia]').val()>0) {
						form.find('select[name=id_cidade_serventia]').val('0').trigger('change');
					}
				});
				form.find('button.incluir-todos-vinculos').removeClass('disabled').prop('disabled',false);
				break;
			case '4':
				form.find('div#campos-judiciario').show('fast',function() {
					form.find('select[name=id_comarca]').prop('disabled',false);
					if (form.find('select[name=id_comarca]').val()>0) {
						form.find('select[name=id_comarca]').val('0').trigger('change');
					}
				});
				if (!form.find('div#cargo-usuario').is(":visible")) {
					form.find('div#cargo-usuario').show('fast',function() {
						form.find('select[name=id_cargo]').val('0').prop('disabled',false);
					});
				}
				form.find('button.incluir-todos-vinculos').removeClass('disabled').prop('disabled',false);
				break;
			case '7':
				form.find('div#campos-unidade-gestora').show('fast',function() {
					form.find('select[name=id_unidade_gestora]').prop('disabled',false);
					form.find('select[name=id_unidade_gestora]').val('0');
				});
				form.find('button.incluir-todos-vinculos').removeClass('disabled').prop('disabled',false);
				break;
			case '11':
				form.find('div#campos-unidade-judiciaria').show('fast',function() {
					form.find('select[name=id_unidade_judiciaria]').prop('disabled',false);
					if (form.find('select[name=id_unidade_judiciaria]').val()>0) {
						form.find('select[name=id_unidade_judiciaria]').val('0').trigger('change');
					}
				});
				if (!form.find('div#cargo-usuario').is(":visible")) {
					form.find('div#cargo-usuario').show('fast',function() {
						form.find('select[name=id_cargo]').val('0').prop('disabled',false);
					});
				}
				form.find('button.incluir-todos-vinculos').removeClass('disabled').prop('disabled',false);
				break;
		}
	});
	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_serventia], select[name=id_vara], select[name=id_unidade_gestora], select[name=id_unidade_judiciaria_divisao]',function(e) {
		var form = $(this).closest('form');
		if ($(this).val()>0) {
			form.find('button.incluir-vinculo').removeClass('disabled').prop('disabled',false);
		} else {
			form.find('button.incluir-vinculo').addClass('disabled').prop('disabled',true);
		}
	});
	$('div#novo-usuario, div#alterar-usuario').on('click','button.incluir-vinculo',function(e){
		var form = $(this).closest('form');
		incluir_vinculos(form,'N');
	});
	$('div#novo-usuario, div#alterar-usuario').on('click','button.incluir-todos-vinculos',function(e){
		var form = $(this).closest('form');
		incluir_vinculos(form,'S');
	});
	$('div#novo-usuario, div#alterar-usuario').on('click', 'button.remover-vinculo', function (e) {
		var form = $(this).closest('form');
		var id_vinculo = $(this).data('idvinculo');
		var id_tipo_usuario = $(this).data('idtipousuario');

		form.find('div#vinculos-usuario div.vinculo#'+id_vinculo+'[data-idtipousuario='+id_tipo_usuario+']').remove();
	});

	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_cidade_serventia]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_cidade = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listarServentias',
			data: {'id_cidade':id_cidade},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(serventias) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_serventia]').html('<option value="0">Selecione uma serventia</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					if (id_cidade=="0") {
						form.find('select[name=id_serventia]').prop('disabled',true);
					} else {
						form.find('select[name=id_serventia]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_serventia]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
			}
		});
	});
	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_estado]',function(e) {
		var form = $(this).closest('form');
		var id_estado = $(this).val();
		var obj_modal = $(this).closest('.modal');

		$.ajax({
			type: "POST",
			url:  url_base+'listarCidades',
			data: {'id_estado':id_estado},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(cidades) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_cidade]').html('<option value="0">Selecione uma cidade</option>');
				if (cidades.length>0) {
					$.each(cidades,function(key,cidade) {
						form.find('select[name=id_cidade]').append('<option value="'+cidade.id_cidade+'">'+cidade.no_cidade+'</option>');
					});
					form.find('select[name=id_cidade]').prop('disabled',false);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER502", "error");
			}
		});
	});
	$('div#novo-usuario, div#alterar-usuario').on('change','select[name=id_unidade_judiciaria]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_unidade_judiciaria = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listar-unidade-judiciaria-divisoes',
			data: {'id_unidade_judiciaria':id_unidade_judiciaria},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(divisoes) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_unidade_judiciaria_divisao]').html('<option value="0">Selecione uma divisão</option>');
				if (divisoes.length>0) {
					$.each(divisoes,function(key,divisao) {
						form.find('select[name=id_unidade_judiciaria_divisao]').append('<option value="'+divisao.id_unidade_judiciaria_divisao+'">'+divisao.no_unidade_judiciaria_divisao+'</option>');
					});
					if (id_unidade_judiciaria=="0") {
						form.find('select[name=id_unidade_judiciaria_divisao]').prop('disabled',true);
					} else {
						form.find('select[name=id_unidade_judiciaria_divisao]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_unidade_judiciaria_divisao]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER504", "error");
			}
		});
	});

	$('div#novo-usuario').on('click','button.enviar-usuario',function(e) {
		var form = $('form[name=form-novo-usuario]');
		var obj_modal = $(this).closest('.modal');

		if (validarCampos(form) == true) {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'usuarios/inserir-usuario',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU502", "error");
				}
			});
		}
	});

	$('div#alterar-usuario').on('click','button.salvar-usuario',function(e) {
		var form = $('form[name=form-alterar-usuario]');
		var obj_modal = $(this).closest('.modal');

		if (validarCamposAlterar(form) == true) {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'usuarios/salvar-usuario',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU511", "error");
				}
			});
		}
	});

	$('div#detalhes-usuario').on('show.bs.modal', function (event) {
		var id_usuario = $(event.relatedTarget).data('idusuario');
		var no_usuario = $(event.relatedTarget).data('nousuario');
		
		$(this).find('.modal-header h4.modal-title span').html(no_usuario);
		
		if (id_usuario>0) {
			$.ajax({
				type: "POST",
				url: 'usuarios/detalhes',
				data: {'id_usuario':id_usuario},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU503", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#alterar-usuario').on('show.bs.modal', function (event) {
		var id_usuario = $(event.relatedTarget).data('idusuario');
		var no_usuario = $(event.relatedTarget).data('nousuario');
		
		$(this).find('.modal-header h4.modal-title span').html(no_usuario);

		$.ajax({
			type: "POST",
			url: 'usuarios/alterar-usuario',
			data: {'id_usuario':id_usuario},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU510", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});

	$("table#usuarios-pendentes").on('click','a.reenviar-confirmacao',function(e) {
		e.preventDefault();
		var id_usuario = $(this).data('idusuario');
		var no_usuario = $(this).data('nousuario');
		swal({
			title: 'Reenviar confirmação de e-mail!',
			text: 'Deseja realmente reenviar a confirmação do e-mail para "'+no_usuario+'"?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			showLoaderOnConfirm: true,
		}).then(function(retorno) {
			if (retorno == true) {
				$.ajax({
					type: "POST",
					url: 'usuarios/reenviar-confirmacao',
					data: 'id_usuario='+id_usuario+'&in_interno=S',
					success: function(retorno) {
						switch (retorno.status) {
							case 'erro':
								var alerta = swal("Erro!",retorno.msg,"error");
								break;
							case 'sucesso':
								var alerta = swal("Sucesso!",retorno.msg,"success");
								break;
							case 'alerta':
								var alerta = swal("Ops!",retorno.msg,"warning");
								break;
						}
						if (retorno.recarrega=='true') {
							alerta.then(function(){
								location.reload();
							});
						}
					},
					error: function (request, status, error) {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU504", "error").then(function() {
							location.reload();
						});
					}
				});
			}
		});
	});

	$("table#usuarios-ativos").on('click','a.desativar-usuario',function(e) {
		e.preventDefault();
		var id_usuario = $(this).data('idusuario');
		var no_usuario = $(this).data('nousuario');
		swal({
			title: 'Desativar usuário!',
			text: 'Deseja realmente desativar o usuário "'+no_usuario+'"?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			showLoaderOnConfirm: true,
		}).then(function(retorno) {
			if (retorno == true ) {
				$.ajax({
					type: "POST",
					url: 'usuarios/desativar-usuario',
					data: 'id_usuario='+id_usuario,
					success: function(retorno) {
						switch (retorno.status) {
							case 'erro':
								var alerta = swal("Erro!",retorno.msg,"error");
								break;
							case 'sucesso':
								var alerta = swal("Sucesso!",retorno.msg,"success");
								break;
							case 'alerta':
								var alerta = swal("Ops!",retorno.msg,"warning");
								break;
						}
						if (retorno.recarrega=='true') {
							alerta.then(function(){
								location.reload();
							});
						}
					},
					error: function (request, status, error) {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU505", "error").then(function() {
							location.reload();
						});
					}
				});
			}
		});
	});
	
	$("table#usuarios-inativos").on('click','a.reativar-usuario',function(e) {
		e.preventDefault();
		var id_usuario = $(this).data('idusuario');
		var no_usuario = $(this).data('nousuario');
		swal({
			title: 'Reativar usuário!',
			text: 'Deseja realmente reativar o usuário "'+no_usuario+'"?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			showLoaderOnConfirm: true,
		}).then(function(retorno) {
			if (retorno == true ) {
				$.ajax({
					type: "POST",
					url: 'usuarios/reativar-usuario',
					data: 'id_usuario='+id_usuario,
					success: function(retorno) {
						switch (retorno.status) {
							case 'erro':
								var alerta = swal("Erro!",retorno.msg,"error");
								break;
							case 'sucesso':
								var alerta = swal("Sucesso!",retorno.msg,"success");
								break;
							case 'alerta':
								var alerta = swal("Ops!",retorno.msg,"warning");
								break;
						}
						if (retorno.recarrega=='true') {
							alerta.then(function(){
								location.reload();
							});
						}
					},
					error: function (request, status, error) {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU506", "error").then(function() {
							location.reload();
						});
					}
				});
			}
		});
	});
	
	$("table#usuarios-pendentes").on('click','a.aprovar-usuario',function(e) {
		e.preventDefault();
		var id_usuario = $(this).data('idusuario');
		var no_usuario = $(this).data('nousuario');
		swal({
			title: 'Aprovar usuário!',
			text: 'Deseja realmente aprovar o usuário "'+no_usuario+'"?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			showLoaderOnConfirm: true,
		}).then(function( retorno ) {
			if (retorno == true) {
				$.ajax({
					type: "POST",
					url: 'usuarios/aprovar-usuario',
					data: 'id_usuario='+id_usuario,
					success: function(retorno) {
						switch (retorno.status) {
							case 'erro':
								var alerta = swal("Erro!",retorno.msg,"error");
								break;
							case 'sucesso':
								var alerta = swal("Sucesso!",retorno.msg,"success");
								break;
							case 'alerta':
								var alerta = swal("Ops!",retorno.msg,"warning");
								break;
						}
						if (retorno.recarrega=='true') {
							alerta.then(function(){
								location.reload();
							});
						}
					},
					error: function (request, status, error) {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU507", "error").then(function() {
							location.reload();
						});
					}
				});
			}
		});
	});

	$("table#usuarios-ativos").on('click','a.gerar-nova-senha',function(e) {
		e.preventDefault();
		var id_usuario = $(this).data('idusuario');
		var no_usuario = $(this).data('nousuario');
		swal({
			title: 'Gerar nova senha!',
			text: 'Deseja realmente gerar uma nova senha para o usuário "'+no_usuario+'"?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			showLoaderOnConfirm: true,
		}).then(function( retorno ) {
			if (retorno == true) {
				$.ajax({
					type: "POST",
					url: 'usuarios/gerar-nova-senha',
					data: 'id_usuario='+id_usuario,
					success: function(retorno) {
						switch (retorno.status) {
							case 'erro':
								var alerta = swal("Erro!",retorno.msg,"error");
								break;
							case 'sucesso':
								var alerta = swal("Sucesso!",retorno.msg,"success");
								break;
							case 'alerta':
								var alerta = swal("Ops!",retorno.msg,"warning");
								break;
						}
						if (retorno.recarrega=='true') {
							alerta.then(function(){
								location.reload();
							});
						}
					},
					error: function (request, status, error) {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU512", "error").then(function() {
							location.reload();
						});
					}
				});
			}
		});
	});

	$('ul.nav[role="tablist"]').on('click', 'li a', function (e) {
		e.preventDefault();
		var id_tipo_pessoa = $(this).data('idtipopessoa');
		var form = $('form[name=form-tipo-usuario]');
        form.find('input[name=id_tipo_pessoa_list]').val(id_tipo_pessoa);
        form.attr("action","usuarios");
        form.submit();
    });

    $('div#filtro-usuario').on('click','input[name=id_tipo_pesquisa]',function() {
        var form = $(this).closest('form');

        form.find('input[name=cpf_cnpj]').val('').prop('disabled',false);
        switch ($(this).val()) {
            case '1':
                form.find('input[name=cpf_cnpj]').mask('000.000.000-00');
                break;
            case '2':
                form.find('input[name=cpf_cnpj]').mask('00.000.000/0000-00');
                break;
        }
    });
});

function total_usuarios(email_usuario) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: url_base+'totalUsuarios',
		async: false,
		data: 'email_usuario='+email_usuario,
		success: function(total) {
			res = parseInt(total);
		},
		error: function (request, status, error) {
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU508", "error");
		}
	});
	return res;
}
function total_pessoas(nu_cpf_cnpj) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: url_base+'totalPessoas',
		async: false,
		data: 'nu_cpf_cnpj='+nu_cpf_cnpj,
		success: function(total) {
			res = parseInt(total);
		},
		error: function (request, status, error) {
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro USU509", "error");
		}
	});
	return res;
}
function validarCampos(form) {
	var erros = new Array();
	var obj_modal = form.closest('.modal');

	var emailR = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

	status_modal(obj_modal, true, true, true);

	if (form.find('input[name=email_usuario]').val()=='') {
		erros.push('O campo e-mail é obrigatório.');
	} else if (emailR.test(form.find('input[name=email_usuario]').val())==false) {
		erros.push('O e-mail digitado é inválido.');
	} else if (total_usuarios(form.find('input[name=email_usuario]').val())>0) {
		erros.push('O e-mail digitado já está em uso.');
	}

	if (form.find('select[name=tp_pessoa]').val()=='F') {
		if (form.find('div.pessoa-fisica input[name=no_pessoa]').val()=='') {
			erros.push('O campo nome é obrigatório.');
		}
		if (form.find('div.pessoa-fisica input[name=nu_cpf_cnpj]').val()=='') {
			erros.push('O campo CPF é obrigatório.');
		} else if (!validarCPF(form.find('div.pessoa-fisica input[name=nu_cpf_cnpj]').val())) {
			erros.push('O CPF digitado é inválido');
		} else if (total_pessoas(form.find('div.pessoa-fisica input[name=nu_cpf_cnpj]').val())>0) {
			erros.push('O CPF digitado já está em uso.');
		}
		if (form.find('div.pessoa-fisica select[name=tp_sexo]').val()=='0') {
			erros.push('O campo gênero é obrigatório.');
		}
		if (form.find('div.pessoa-fisica input[name=dt_nascimento]').val()=='') {
			erros.push('O campo data de nascimento é obrigatório.');
		}
	} else if (form.find('select[name=tp_pessoa]').val()=='J') {
		if (form.find('div.pessoa-juridica input[name=nu_cpf_cnpj]').val()=='') {
			erros.push('O campo CNPJ é obrigatório.');
		} else if (!validarCNPJ(form.find('div.pessoa-juridica input[name=nu_cpf_cnpj]').val())) {
			erros.push('O CNPJ digitado é inválido');
		} else if (total_pessoas(form.find('div.pessoa-juridica input[name=nu_cpf_cnpj]').val())>0) {
			erros.push('O CNPJ digitado já está em uso.');
		}
		if (form.find('div.pessoa-juridica input[name=no_inscricao_municipal]').val()=='') {
			erros.push('O campo inscrição municipal é obrigatório.');
		}
		if (form.find('div.pessoa-juridica input[name=no_razao_social]').val()=='') {
			erros.push('O campo razão social é obrigatório.');
		}
	} else {
		erros.push('O campo tipo de cadastro é obrigatório.');
	}

	if (form.find('input[name=in_digitar_telefone]').is(':checked')) {
		if (form.find('select[name=id_tipo_telefone]').val()=='0') {
			erros.push('O campo tipo de telefone é obrigatório.');
		}
		if (form.find('input[name=nu_ddd]').val()=='') {
			erros.push('O campo DDD é obrigatório.');
		}
		if (form.find('input[name=nu_telefone]').val()=='') {
			erros.push('O campo telefone é obrigatório.');
		}
	}

	if (form.find('input[name=in_digitar_endereco]').is(':checked')) {
		if (form.find('select[name=id_estado]').val()=='0') {
			erros.push('O campo estado é obrigatório.');
		}
		if (form.find('select[name=id_cidade]').val()=='0') {
			erros.push('O campo cidade é obrigatório.');
		}
		if (form.find('input[name=nu_cep]').val()=='') {
			erros.push('O campo CEP é obrigatório.');
		}
		if (form.find('input[name=no_endereco]').val()=='') {
			erros.push('O campo endereço é obrigatório.');
		}
	}

	if (form.find('div#vinculos-usuario div.vinculo[data-idtipousuario=4]').length>0 || form.find('div#vinculos-usuario div.vinculo[data-idtipousuario=11]').length>0) {
		if (form.find('select[name=id_cargo]').val()=='0') {
			erros.push('O campo cargo é obrigatório.');
		}
	}

	if (form.find('input[name=in_vincular]').val()=='N') {
		if (form.find('div#vinculos-usuario div.vinculo').length<=0) {
			erros.push('Insira ao menos um vínculo do usuário.')
		}
	}

	if (erros.length>0) {
		status_modal(obj_modal, false, false, false);
		form.find('div.erros div').html(erros.join('<br />'));
		if (!form.find('div.erros').is(':visible')) {
			form.find('div.erros').slideDown();
		}
		return false;
	} else {
		form.find('div.erros').slideUp();
		return true;
	}
}
function validarCamposAlterar(form) {
	var erros = new Array();
	var obj_modal = form.closest('.modal');

	var emailR = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

	status_modal(obj_modal, true, true, true);

	if (form.find('select[name=tp_pessoa]').val()=='F') {
		if (form.find('div.pessoa-fisica input[name=no_pessoa]').val()=='') {
			erros.push('O campo nome é obrigatório.');
		}
		if (form.find('div.pessoa-fisica select[name=tp_sexo]').val()=='0') {
			erros.push('O campo gênero é obrigatório.');
		}
		if (form.find('div.pessoa-fisica input[name=dt_nascimento]').val()=='') {
			erros.push('O campo data de nascimento é obrigatório.');
		}
	} else if (form.find('select[name=tp_pessoa]').val()=='J') {
		if (form.find('div.pessoa-juridica input[name=no_inscricao_municipal]').val()=='') {
			erros.push('O campo inscrição municipal é obrigatório.');
		}
		if (form.find('div.pessoa-juridica input[name=no_razao_social]').val()=='') {
			erros.push('O campo razão social é obrigatório.');
		}
	} else {
		erros.push('O campo tipo de cadastro é obrigatório.');
	}

	if (form.find('input[name=in_digitar_telefone]').is(':checked')) {
		if (form.find('select[name=id_tipo_telefone]').val()=='0') {
			erros.push('O campo tipo de telefone é obrigatório.');
		}
		if (form.find('input[name=nu_ddd]').val()=='') {
			erros.push('O campo DDD é obrigatório.');
		}
		if (form.find('input[name=nu_telefone]').val()=='') {
			erros.push('O campo telefone é obrigatório.');
		}
	}

	if (form.find('input[name=in_digitar_endereco]').is(':checked')) {
		if (form.find('select[name=id_estado]').val()=='0') {
			erros.push('O campo estado é obrigatório.');
		}
		if (form.find('select[name=id_cidade]').val()=='0') {
			erros.push('O campo cidade é obrigatório.');
		}
		if (form.find('input[name=nu_cep]').val()=='') {
			erros.push('O campo CEP é obrigatório.');
		}
		if (form.find('input[name=no_endereco]').val()=='') {
			erros.push('O campo endereço é obrigatório.');
		}
	}

	if (form.find('div#vinculos-usuario div.vinculo[data-idtipousuario=4]').length>0 || form.find('div#vinculos-usuario div.vinculo[data-idtipousuario=11]').length>0) {
		if (form.find('select[name=id_cargo]').val()=='0') {
			erros.push('O campo cargo é obrigatório.');
		}
	}

	if (form.find('input[name=in_vincular]').val()=='N') {
		if (form.find('div#vinculos-usuario div.vinculo').length<=0) {
			erros.push('Insira ao menos um vínculo do usuário.')
		}
	}

	if (erros.length>0) {
		status_modal(obj_modal, false, false, false);
		form.find('div.erros div').html(erros.join('<br />'));
		if (!form.find('div.erros').is(':visible')) {
			form.find('div.erros').slideDown();
		}
		return false;
	} else {
		form.find('div.erros').slideUp();
		return true;
	}
}
function incluir_vinculos(form,todas) {
	var id_tipo_usuario = form.find('select[name=id_tipo_usuario]').val();
	switch (id_tipo_usuario) {
		case '2':
			if (todas=='N') {
				var vinculos = form.find('select[name=id_serventia] option:selected');
			} else {
				var vinculos = form.find('select[name=id_serventia] option[value!="0"]');
			}
			break;
		case '4':
			if (todas=='N') {
				var vinculos = form.find('select[name=id_vara] option:selected');
			} else {
				var vinculos = form.find('select[name=id_vara] option[value!="0"]');
			}
			break;
		case '7':
            if ( form.find('select[name=id_unidade_gestora] option:selected').val() == 2)
			{
                var in_master_recebe_notificacoes_sistema = form.find('input[name=in_master_recebe_notificacoes_sistema]').is(':checked');
                var in_usuario_recebe_notificacoes_sistema = form.find('input[name=in_usuario_recebe_notificacoes_sistema]').is(':checked');

                if ( in_master_recebe_notificacoes_sistema == false && in_usuario_recebe_notificacoes_sistema == false)
                {
                    swal("Ops!",
                        "Favor selecionar uma opção: <br> " +
                        "Encaminhar notificações (email) para usuário master <br> " +
                        "Receber notificaçãos (email)", "warning");
                    return false;
                }
			}

			if (todas=='N') {
				var vinculos = form.find('select[name=id_unidade_gestora] option:selected');
			} else {
				var vinculos = form.find('select[name=id_unidade_gestora] option[value!="0"]');
			}
			break;
		case '11':
			if (todas=='N') {
				var vinculos = form.find('select[name=id_unidade_judiciaria_divisao] option:selected');
			} else {
				var vinculos = form.find('select[name=id_unidade_judiciaria_divisao] option[value!="0"]');
			}
			break;
	}

	vinculos.each(function (e) {
		if (form.find('div#vinculos-usuario div.vinculo#'+$(this).val()+'[data-idtipousuario='+id_tipo_usuario+']').length>0) {
			if (todas=='N') {
				swal("Ops!", "Este vinculo já foi escolhido", "warning");
			}
		} else {
			html = 	'<div id="'+$(this).val()+'" class="vinculo btn-group" data-idtipousuario="'+id_tipo_usuario+'">';
				html +=	'<button class="btn btn-sm btn-primary" type="button">'+$(this).text()+'</button>';
				html += '<input type="hidden" name="vinculo_id_tipo_usuario[]" value="'+id_tipo_usuario+'">';
				html += '<input type="hidden" name="id_vinculo[]" value="'+$(this).val()+'">';
				html += '<button class="remover-vinculo btn btn-sm btn-danger" type="button" data-idvinculo="'+$(this).val()+'" data-idtipousuario="'+id_tipo_usuario+'"><i class="fa fa-times"></i></button>';
			html += '</div> ';

			$("div#vinculos-usuario").append(html);
		}
	});
}