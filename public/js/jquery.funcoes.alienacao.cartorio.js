var total_arquivos = 0;
var total_arquivos_assinaveis = 0;
var index_arquivos = 0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('div#nova-resposta').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/nova-resposta',
				data: 'id_alienacao='+id_alienacao,
				beforeSend: function() {
					$('div#nova-resposta .modal-body>div.carregando').show();
					$('div#nova-resposta .modal-body>div.form').hide();
				},
				success: function(retorno) {

					$('div#nova-resposta .modal-body>div.form').html(retorno);
					$('div#nova-resposta .modal-body>div.carregando').hide();
					$('div#nova-resposta .modal-body>div.form').fadeIn('fast');
					carrega_andamento(id_alienacao);
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#nova-resposta').on('hidden.bs.modal', function (event) {
		$('div#nova-resposta .modal-body>div.carregando').removeClass('flutuante').show();
		$('div#nova-resposta .modal-body>div.form').html('');
        total_arquivos = 0;
        total_arquivos_assinaveis = 0;
        index_arquivos = 0;
	});

    $('div#visualizar-arquivo').on('show.bs.modal', function (event) {
        var id_alienacao = $(event.relatedTarget).data('idalienacao');
    	$.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/gerar-recibo',
            data: {'id_alienacao':id_alienacao},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, true);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($('div#visualizar-arquivo'));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

	$('div#nova-resposta').on('change','select[name=id_fase_grupo_produto]',function(e) {
		id_fase_grupo_produto = $(this).val();
		
		var form = $('form[name=form-novo-andamento]');
		$.ajax({
			type: "POST",
			url: url_base+'servicos/listar_etapas',
			data: 'id_fase_grupo_produto='+id_fase_grupo_produto,
			beforeSend: function() {
				form.find('select[name=id_etapa_fase]').prop('disabled',true).html('<option value="0">Carregando...</option>');
			},
			success: function(etapas) {
				form.find('select[name=id_etapa_fase]').html('<option value="0">Selecione</option>');
				if (etapas.length>0) {
					$.each(etapas,function(key,etapa) {
						form.find('select[name=id_etapa_fase]').append('<option value="'+etapa.id_etapa_fase+'">'+etapa.no_etapa+'</option>');
					});
					form.find('select[name=id_etapa_fase]').prop('disabled',false);
				} else {
					form.find('select[name=id_etapa_fase]').prop('disabled',true).html('<option value="0">Nenhuma etapa disponível</option>');
				}
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});

	$('div#nova-resposta').on('submit','form[name=form-novo-andamento]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
/*		if (form.find('input[name=id_tipo_resposta]:checked').length<=0) {
			erros.push('Ao menos uma ação deve ser selecionada');
		}*/
		if (form.find('select[name=id_resultado_acao]')) {
			if (form.find('select[name=id_resultado_acao]').val()=='0') {
				erros.push('O campo resultado é obrigatório.');
			}
		}
		form.find('.obrigatorio').each(function(index, element) {
            if ($(this).hasClass('real')) {
				if ($(this).autoNumeric('get')=='' || $(this).autoNumeric('get')==0) {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			} else if ($(this).hasClass('arquivos')) {
				if ($(this).find('div.arquivo').length<=0) {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			} else {
				if ($(this).val()=='') {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			}
        });

		if (!total_enderecos>0 && form.find('button.novo-endereco').length) {
            erros.push('Ao menos um endereço deve ser cadastrado.');
		}

		/*
		if (total_arquivos_assinaveis>0) {
			erros.push('É obrigatório assinar os arquivos inseridos');
		}
		*/

		if (erros.length>0) {
			form.find('div.erros-andamento div').html(erros.join('<br />'));
			if (!form.find('div.erros-andamento').is(':visible')) {
				form.find('div.erros-andamento').slideDown();
			}
		} else {
			form.find('div.erros-andamento').slideUp();

			var data = new FormData(form.get(0));

			data.set('dt_vencimento_periodo_1', $('.form').find('input[name=va_vencimento_periodo_01]').val());
			data.set('dt_vencimento_periodo_4', $('.form').find('input[name=va_vencimento_periodo_04]').val());

			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/inserir-andamento',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#nova-resposta .modal-body>div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					//console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							switch (retorno.andamento.tipo) {
								case 'resultado':
									$('div#nova-resposta table#historico-pedido tr#'+retorno.andamento.andamento.id_andamento_alienacao+' td.resultado label').removeClass('label-warning').addClass('label-success').html(retorno.andamento.resultado.no_resultado);
									$('div#nova-resposta .modal-body>div.carregando').removeClass('flutuante').hide();
									carrega_andamento(form.find('input[name=id_alienacao]').val());
									break;
								case 'andamento':
									console.log(retorno.andamento);
									var nova_linha = $('<tr id="'+retorno.andamento.andamento.id_andamento_alienacao+'">');
									var colunas = "";
									colunas += '<td>'+retorno.andamento.fase.no_fase+'</td>';
									colunas += '<td>'+retorno.andamento.etapa.no_etapa+'</td>';
									colunas += '<td>'+retorno.andamento.acao.no_acao+'</td>';
									colunas += '<td class="resultado">';
										if(retorno.andamento.resultado.automatico=='true') {
											colunas += '<label class="label label-success">'+retorno.andamento.resultado.no_resultado+'</label>';
										} else {
											colunas += '<label class="label label-warning">'+retorno.andamento.resultado.no_resultado+'</label>';
										}
									colunas += '</td>';
									colunas += '<td>'+retorno.andamento.dt_formatada+'</td>';
									colunas += '<td>';
										colunas += '<a href="#" class="detalhes-andamento btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-andamento" data-idandamento="'+retorno.andamento.andamento.id_andamento_alienacao+'" data-nofase="'+retorno.andamento.fase.no_fase+'" data-noetapa="'+retorno.andamento.etapa.no_etapa+'" data-noacao="'+retorno.andamento.acao.no_acao+'">Ver detalhes</a></li>';
									colunas += '</td>';
						
									nova_linha.append(colunas);
									$("table#historico-pedido tbody").prepend(nova_linha);
									$('div#nova-resposta .modal-body>div.carregando').removeClass('flutuante').hide();
									carrega_andamento(form.find('input[name=id_alienacao]').val());
									break;
							}
							//var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#nova-resposta').on('change','select[name=id_resultado_acao]',function(e) {
		var form = $('form[name=form-novo-andamento]');
		var id_resultado_acao = $(this).val();
		var andamento_token = form.find('input[name=andamento_token]').val();

        total_arquivos = 0;
        total_arquivos_assinaveis = 0;
        index_arquivos = 0;

        remover_arquivos_sessao(andamento_token);

		if (id_resultado_acao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/resultado-acao',
				data: {'id_resultado_acao':id_resultado_acao,'andamento_token':andamento_token},
				beforeSend: function() {
					form.find('div#resultado-acao>div.carregando').show();
					form.find('div#resultado-acao>div.form').hide();
				},
				success: function(retorno) {
					console.log(retorno);
					form.find('div#resultado-acao>div.form').html(retorno);
					form.find('div#resultado-acao>div.carregando').fadeOut('fast',function() {
						form.find('div#resultado-acao>div.form').fadeIn('fast');
					});
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		} else {
			form.find('div#resultado-acao>div.carregando').hide();
			form.find('div#resultado-acao>div.form').slideUp('fast').html('');
		}
	});

	////////////////////////////////////////////////////////////
	// Ações									              //
	////////////////////////////////////////////////////////////
	
	$('table#pedidos').on('click','input#selecionar-todas',function(e) {
		if ($(this).is(':checked')) {
			$('table#pedidos input.alienacao:not(:checked)').trigger('click');
		} else {
			$('table#pedidos input.alienacao:checked').trigger('click');
		}
	});

	$('table#pedidos').on('click','input.alienacao',function(e) {
		var total = $('table#pedidos input.alienacao:checked').length;

		if (total<=0) {
			$('div#alert span.total').html('0 notificações selecionadas');
			$('div#alert button').prop('disabled',true).addClass('disabled');

			$('div#alert ul.dropdown-menu a.gerar-relatorio').parent('li').addClass('disabled');
			$('div#alert ul.dropdown-menu a.gerar-relatorio span').html('');
		} else {
			$('div#alert span.total').html((total==1?'1 notificação selecionada':total+' notificações selecionadas'));
			$('div#alert button').prop('disabled',false).removeClass('disabled');

			$('div#alert ul.dropdown-menu a.gerar-relatorio').parent('li').removeClass('disabled');
			$('div#alert ul.dropdown-menu a.gerar-relatorio span').html('('+total+')');
		}
	});

	$('div#relatorio-notificacoes').on('show.bs.modal', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos input.alienacao:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/relatorio',
            data: {'ids_alienacoes':ids_alienacoes},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-notificacoes').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
    });

    $('div#relatorio-notificacoes').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-relatorio]').submit();
        $('div#relatorio-notificacoes').modal('hide');
    });

	$('div#detalhes-imovel').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var matricula_imovel = $(event.relatedTarget).data('matriculaimovel');
		
		$(this).find('.modal-header h4.modal-title span').html(matricula_imovel);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/imovel/detalhes',
				data: 'id_alienacao='+id_alienacao,
				beforeSend: function() {
					$('div#detalhes-imovel .modal-body>div.carregando').show();
					$('div#detalhes-imovel .modal-body>div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-imovel .modal-body>div.form').html(retorno);
					$('div#detalhes-imovel .modal-body>div.carregando').hide();
					$('div#detalhes-imovel .modal-body>div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#gerar-arquivo-previsualizar').on('show.bs.modal', function (event) {
		var form = $('form[name=form-novo-andamento]');
		var erros = new Array();

		var id_tipo_arquivo_grupo_produto = $(event.relatedTarget).data('idtipoarquivo');
		var limite = $(event.relatedTarget).data('limite');
        var gerar_capa = $('input#gerar_capa').is(':checked') ? 1 : 0;
        var gerar_nota = $('input#gerar_nota').is(':checked') ? 1 : 0;
        var arquivo_novo = $(event.relatedTarget).data('arquivonovo');

		form.find('.obrigatorio').each(function(index, element) {
            if ($(this).hasClass('real')) {
				if ($(this).autoNumeric('get')=='' || $(this).autoNumeric('get')==0) {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			} else if (!$(this).hasClass('arquivos')) {
				if ($(this).val()=='') {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			}
        });

        if (erros.length>0) {
			form.find('div.erros-andamento div').html(erros.join('<br />'));
			if (!form.find('div.erros-andamento').is(':visible')) {
				form.find('div.erros-andamento').slideDown();
				return false;
			}
		} else {
			form.find('div.erros-andamento').slideUp();

			var total_arquivos_gerados = form.find('div#arquivos-andamento div.arquivo.gerado').length;
			if ((total_arquivo(id_tipo_arquivo_grupo_produto,'',0)<parseInt(limite) || limite=='0')) {
				if (total_arquivos_gerados<1) {
					var id_alienacao = $(event.relatedTarget).data('idalienacao');
					var tipo = $(event.relatedTarget).data('tipo');
					var token = $(event.relatedTarget).data('token');

					var de_texto_curto_acao = $('form[name=form-novo-andamento]').find('input[name=de_texto_curto_acao]').val();
					var de_texto_longo_acao = $('form[name=form-novo-andamento]').find('input[name=de_texto_longo_acao]').val();
					var va_valor_acao = $('form[name=form-novo-andamento]').find('input[name=va_valor_acao]').val();
					var dt_acao = $('form[name=form-novo-andamento]').find('input[name=dt_acao]').val();
					
					switch(tipo) {
						case 'certidao-decurso':
							$(this).find('.modal-header h4.modal-title').html('Pré-visualização - Certidão de Decurso de Prazo');
							break;
						case 'edital-intimacao':
							$(this).find('.modal-header h4.modal-title').html('Pré-visualização - Edital de Intimação');
							break;
						case 'carta-intimacao':
							$(this).find('.modal-header h4.modal-title').html('Pré-visualização - Carta de Intimação');
							break;
					}

					if (id_alienacao>0) {
						$.ajax({
							type: "POST",
							url: url_base+'servicos/alienacao/gerar-arquivo/previsualizar',
							data: {'id_alienacao':id_alienacao,'tipo':tipo,'de_texto_curto_acao':de_texto_curto_acao,'de_texto_longo_acao':de_texto_longo_acao,'va_valor_acao':va_valor_acao,'dt_acao':dt_acao,'gerar_capa':gerar_capa,'gerar_nota':gerar_nota,'arquivo_novo':arquivo_novo},
							context: this,
							beforeSend: function() {
								status_modal($(this),true,true,false);
								$(this).find('.modal-body div.form').hide();
							},
							success: function(retorno) {
								switch (retorno.status) {
									case 'erro':
										var alerta = swal("Erro!",retorno.msg,"error");
										break;
									case 'sucesso':
										$(this).addClass('total-height');
										status_modal($(this),false,false,false);
										resize_modal($(this));
										$(this).find('.modal-body div.form').html(retorno.view).fadeIn('fast');
										$(this).find('.modal-footer button.salvar-certidao').data('idtipoarquivo',id_tipo_arquivo_grupo_produto);
										$(this).find('.modal-footer button.salvar-certidao').data('token',token);
										$(this).find('.modal-footer button.salvar-certidao').data('idalienacao',id_alienacao);
										$(this).find('.modal-footer button.salvar-certidao').data('tipo',tipo);
										break;
									case 'alerta':
										var alerta = swal("Ops!",retorno.msg,"warning");
										break;
								}
							},
							error: function (request, status, error) {
								swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error").then(function() {
									$(this).modal('hide');
								});
							}
						});
					}
				} else {
					swal("Ops!", "Você alcançou o limite de 1 arquivo gerado para esta seção.", "warning");
					return false;
				}
			} else {
				swal("Ops!", "Você alcançou o limite de "+limite+" arquivo(s) para esta seção.", "warning");
				return false;
			}
		}
	});

	$('div#gerar-arquivo-previsualizar').on('click','button.salvar-certidao',function(event) {
		var id_tipo_arquivo_grupo_produto = $(this).data('idtipoarquivo');
		var id_alienacao = $(this).data('idalienacao');
		var tipo = $(this).data('tipo');
		var token = $(this).data('token');

		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/gerar-arquivo/salvar',
				data: {'id_tipo_arquivo_grupo_produto': id_tipo_arquivo_grupo_produto,'id_alienacao': id_alienacao,'tipo': tipo,'token': token,'index_arquivos':index_arquivos},
				success: function (retorno) {
					status_modal(obj_modal, false, false, false);
					switch (retorno.status) {
						case 'erro':
							swal("Erro!", retorno.msg, "error");
							break;
						case 'sucesso':
							botao_arquivo = '<div class="arquivo gerado btn-group" id="'+index_arquivos+'" data-inassdigital="'+retorno.arquivo.in_ass_digital+'">';
								botao_arquivo += '<button type="button" class="btn btn-primary">'+retorno.arquivo.no_arquivo+'</button>';
								botao_arquivo += '<input type="hidden" name="no_arquivo[]" value="'+retorno.arquivo.no_arquivo+'" />';
								if (retorno.arquivo.in_ass_digital=='S') {
									botao_arquivo += '<button type="button" class="icone-assinatura btn btn-warning"><i class="fa fa-unlock-alt"></i></button>';
								}
								botao_arquivo += '<button type="button" class="remover-arquivo btn btn-danger" data-linha="'+index_arquivos+'" data-token="'+retorno.token+'"><i class="fa fa-times"></i></button>';
							botao_arquivo += '</div> ';
							$('div#gerar-arquivo-previsualizar').modal('hide');
							total_arquivos++;
							if (retorno.arquivo.in_ass_digital=='S') {
								total_arquivos_assinaveis++;
							}
							index_arquivos++;

							$('form[name=form-novo-andamento] div#arquivos-andamento').prepend(botao_arquivo);

							if (total_arquivos_assinaveis>=0) {
								if (total_arquivos==1) {
									$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivo foi inserido, <b>'+total_arquivos_assinaveis+'</b> '+(total_arquivos_assinaveis>1?'podem ser assinados':'pode ser assinado')+'.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
								} else {
									$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivos foram inseridos, <b>'+total_arquivos_assinaveis+'</b> podem ser assinados.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
								}
								if (total_arquivos_assinaveis>0) {
									$('div#assinaturas-arquivos div.menssagem a').removeClass('btn-success').addClass('btn-warning').removeClass('disabled');
									$('div#assinaturas-arquivos div.alert').removeClass('alert-success').addClass('alert-warning');
									$('div#assinaturas-arquivos div.alert i').removeClass('glyphicon-ok').addClass('glyphicon-exclamation-sign');
								}
							}
							break;
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
				}
			});
		}
	});
	
	$('div#detalhes-imovel').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		/*var matricula_imovel = $(event.relatedTarget).data('matriculaimovel');*/

		$('div#detalhes-imovel').on('click', 'button.alterar-imovel', function (event) {
			event.preventDefault();

			var no_endereco = $('textarea[name=no_endereco]').val();

			if (id_alienacao > 0) {
				swal({
					title: 'Tem certeza?',
					type: 'warning',
					html: 'Tem certeza que deseja alterar o endereço do imóvel?',
					showCancelButton: true,
					cancelButtonText: 'Não',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim'
				}).then(function (retorno) {
					if (retorno == true) {
						$.ajax({
							type: "POST",
							url: url_base+'servicos/alienacao/imovel/alterar-imovel',
							data: {'no_endereco': no_endereco, 'id_alienacao': id_alienacao},
							success: function (retorno) {
								console.log(retorno);
								switch (retorno.status) {
									case 'erro':
										swal("Erro!", retorno.msg, "error");
										break;
									case 'sucesso':
										swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
											location.reload();
										});
										break;
								}
							},
							error: function (request, status, error) {
								console.log(request.responseText);
								swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
									location.reload();
								});
							}
						});
					}
				});
			}
		});
	});

    $('div#detalhes-custas').on('show.bs.modal', function (event) {

        var id_alienacao = $(event.relatedTarget).data('idalienacao');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/detalhes-custas',
            data: 'id_alienacao='+id_alienacao,
            beforeSend: function() {
                $('div#detalhes-custas .modal-body div.carregando').show();
                $('div#detalhes-custas .modal-body div.form').hide();
            },
            success: function(retorno) {
                $('div#detalhes-custas .modal-body div.form').html(retorno);
                $('div#detalhes-custas .modal-body div.carregando').hide();
                $('div#detalhes-custas .modal-body div.form').fadeIn('fast');
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#info-andamento-acao').on('show.bs.modal', function (event) {
    	var title = 'Fase <b>' + $(event.relatedTarget).data('fase') + '</b>, etapa <b>' + $(event.relatedTarget).data('etapa') + '</b> e ação <b>' + $(event.relatedTarget).data('acao') + '</b>.';
        var de_alerta_acao = '<p>' + $(event.relatedTarget).data('dealertaacao') +'</p>';

        $(this).find('.modal-header h4.modal-title span').html(title);

        $('div#info-andamento-acao .modal-body>div.carregando').show();
        $('div#info-andamento-acao .modal-body>div.form').hide();

        $('div#info-andamento-acao .modal-body>div.form').html(de_alerta_acao);
        $('div#info-andamento-acao .modal-body>div.carregando').hide();
        $('div#info-andamento-acao .modal-body>div.form').fadeIn('fast');
    });

    $('div#detalhes-devedor').on('show.bs.modal', function (event) {
        var id_alienacao = $(event.relatedTarget).data('idalienacao');
        var id_alienacao_devedor = $(event.relatedTarget).data('iddevedor');
        var nu_cpf_cnpj = $(event.relatedTarget).data('nucpfcnpj');

        $(this).find('.modal-header h4.modal-title span').html(nu_cpf_cnpj);

        if (id_alienacao>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/alienacao/devedor/detalhes',
                data: {'id_alienacao':id_alienacao, 'id_alienacao_devedor':id_alienacao_devedor},
                beforeSend: function() {
                    $('div#detalhes-devedor .modal-body>div.carregando').show();
                    $('div#detalhes-devedor .modal-body>div.form').hide();
                },
                success: function(retorno) {
                    $('div#detalhes-devedor .modal-body>div.form').html(retorno);
                    $('div#detalhes-devedor .modal-body>div.carregando').hide();
                    $('div#detalhes-devedor .modal-body>div.form').fadeIn('fast');
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
        }

        $('div#detalhes-devedor').on('click', 'button.alterar-devedor', function (event) {
            event.preventDefault();

            var no_devedor = $('input[name=no_devedor]').val();
            var nu_cpf_cnpj = $('input[name=nu_cpf_cnpj]').val();

            if (id_alienacao > 0) {
                swal({
                    title: 'Tem certeza?',
                    type: 'warning',
                    html: 'Tem certeza que deseja alterar os dados do devedor?',
                    showCancelButton: true,
                    cancelButtonText: 'Não',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim'
                }).then(function (retorno) {
                    if (retorno == true) {
                        $.ajax({
                            type: "POST",
                            url: url_base+'servicos/alienacao/devedor/alterar-devedor',
                            data: {'no_devedor':no_devedor,'id_alienacao':id_alienacao,'id_alienacao_devedor':id_alienacao_devedor,'nu_cpf_cnpj':nu_cpf_cnpj},
                            success: function (retorno) {
                                console.log(retorno);
                                switch (retorno.status) {
                                    case 'erro':
                                        swal("Erro!", retorno.msg, "error");
                                        break;
                                    case 'sucesso':
                                        swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
                                            location.reload();
                                        });
                                        break;
                                }
                            },
                            error: function (request, status, error) {
                                console.log(request.responseText);
                                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                                    location.reload();
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    $('form#form-filtro').on('change','select.grupo',function(e) {
    	var grupo = $(this).val();

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/filtros-alertas',
            data: {'grupo': grupo},
            success: function (grupos) {
                $('form#form-filtro select.filtro').prop('disabled', false);
                if (!$.isEmptyObject(grupos)) {
                	HTML = '';
                    $.each(grupos, function (key, grupo) {
                    	HTML += '<optgroup label="'+grupo.grupo.titulo+'">';
	                    	$.each(grupo.alertas, function (key, alerta) {
	                    		switch (alerta.alcada) {
	                    			case 2:
	                    				alcada = 'cartorio';
	                    				break;
	                    			case 8:
	                    				alcada = 'cef';
	                    				break;
	                    			default:
	                    				alcada = '';
	                    				break;
	                    		}
	                        	HTML += '<option value="'+grupo.grupo.key+'_'+alerta.key+'" class="'+alcada+'">'+alerta.titulo+'</option>';
	                        });
                        HTML += '</optgroup>';
                    });
                    $('form#form-filtro select.filtro').html(HTML);
                } else {
                    $('form#form-filtro select.filtro').html('').prop('disabled', true);
                }
                $('form#form-filtro select.filtro').selectpicker('refresh');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error");
            }
        });
    });
});
function carrega_andamento(id_alienacao) {
	$.ajax({
		type: "POST",
		url: url_base+'servicos/alienacao/novo-andamento',
		data: 'id_alienacao='+id_alienacao,
		beforeSend: function() {
			$('div#nova-resposta').find('div#novo-andamento>div.carregando').show();
			$('div#nova-resposta').find('div#novo-andamento>div.form').hide();
		},
		success: function(retorno) {
			console.log(retorno);
			$('div#nova-resposta').find('div#novo-andamento>div.form').html(retorno);
			$('div#nova-resposta').find('div#novo-andamento>div.carregando').fadeOut('fast',function() {
				$('div#nova-resposta').find('div#novo-andamento>div.form').fadeIn('fast');
			});
		},
		error: function (request, status, error) {
			console.log(request.responseText);
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}