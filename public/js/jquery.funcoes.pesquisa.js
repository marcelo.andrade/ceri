var total_arquivos = 0;
var total_arquivos_assinaveis = 0;
var index_arquivos = 0;
$(document).ready(function() {
	$(document).keypress(function(e) {
		if(e.which == 13) {
			return false;
		}
	});

	$('div#nova-pesquisa').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: url_base+'servicos/pesquisa-eletronica/nova-pesquisa',
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10501", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});

    $('div#nova-pesquisa').on('keyup','textarea[maxlength!=0]',function(e) {
        var limite = $(this).prop('maxlength');

        var alert_obj = $(this).closest('div.form-group').next().find('div.alert');
        alert_obj.find('div.menssagem span.total').html($(this).val().length);
        if ($(this).val().length<limite) {
            alert_obj.removeClass('alert-warning').addClass('alert-info');
            alert_obj.find('i').removeClass('.glyphicon-exclamation-sign').addClass('.glyphicon-info-sign');
            alert_obj.find('div.menssagem span.limite').html('');
        } else {
            alert_obj.removeClass('alert-info').addClass('alert-warning');
            alert_obj.find('i').removeClass('.glyphicon-info-sign').addClass('.glyphicon-exclamation-sign');
            alert_obj.find('div.menssagem span.limite').html('O limite foi atingido.');
        }
    });

	$('div#nova-pesquisa').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
	});

	$('div#nova-pesquisa').on('change','select[name=id_cidade]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_cidade = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listarServentias',
			data: {'id_cidade':id_cidade},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(serventias) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_serventia]').html('<option value="0">Selecione uma serventia</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					if (id_cidade=="0") {
						form.find('select[name=id_serventia]').prop('disabled',true);
					} else {
						form.find('select[name=id_serventia]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_serventia]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
			}
		});
	});
	$('div#nova-pesquisa').on('change','select[name=id_serventia]',function(e) {
		var form = $(this).closest('form');

		if ($(this).val()>0) {
			form.find('button.incluir-serventia').removeClass('disabled').prop('disabled',false);
		} else {
			form.find('button.incluir-serventia').addClass('disabled').prop('disabled',true);
		}
	});

	$('div#nova-pesquisa').on('click','input[name=id_produto_item]',function(e) {
		var form = $(this).closest('form');
		calcular_valores();
	});

	$('div#nova-pesquisa').on('click','input[name=id_chave_pesquisa_pesquisa]',function(e) {
		var form = $(this).closest('form');

		form.find('input[name=de_chave_pesquisa]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case '2':
				form.find('input[name=de_chave_pesquisa]').parent('div').find('label span').html('(CPF)');
				form.find('input[name=de_chave_pesquisa]').mask('000.000.000-00').removeClass('somente-texto');

				form.find('input[name=de_chave_complementar]').val('').prop('disabled',false).addClass('somente-texto');
				form.find('input[name=de_chave_complementar]').parent('div').find('label').html('Titular do CPF');
				form.find('div#chave-complementar').show();
				break;
			case '3':
				form.find('input[name=de_chave_pesquisa]').parent('div').find('label span').html('(CNPJ)');
				form.find('input[name=de_chave_pesquisa]').mask('00.000.000/0000-00').removeClass('somente-texto');
				form.find('div#msg-homonimia').hide();

				form.find('input[name=de_chave_complementar]').val('').prop('disabled',false).removeClass('somente-texto');
				form.find('input[name=de_chave_complementar]').parent('div').find('label').html('Razão social do CNPJ');
				form.find('div#chave-complementar').show();
				break;
			case '4':
				form.find('input[name=de_chave_pesquisa]').parent('div').find('label span').html('(Nome)');
				form.find('input[name=de_chave_pesquisa]').unmask().addClass('somente-texto');
				form.find('div#msg-homonimia').show();

				form.find('input[name=de_chave_complementar]').val('').prop('disabled',true);
				form.find('div#chave-complementar').hide();
				break;
			case '1':
				form.find('input[name=de_chave_pesquisa]').parent('div').find('label span').html('(Matrícula)');
				form.find('input[name=de_chave_pesquisa]').unmask().removeClass('somente-texto');
				form.find('div#msg-homonimia').hide();

				form.find('input[name=de_chave_complementar]').val('').prop('disabled',true);
				form.find('div#chave-complementar').hide();
				break;
		}
	});

	$('div#nova-pesquisa').on('click','input[name=id_tipo_pesquisa]',function(e) {
		var form = $(this).closest('form');

		switch ($(this).val()) {
			case '1':
				form.find('div#msg-tipo span').html('Nesta opção, as matrículas informadas pelo cartório são de imóveis de titularidade atual do pesquisado.');
				form.find('div#msg-tipo').show();

				form.find('div#msg-ciencia label').html('Tenho ciência de que a pesquisa hora requerida irá informar tão somente as matrículas dos imóveis de atual propriedade do pesquisado.');
				form.find('div#msg-ciencia').show();
				break;
			case '3':
				form.find('div#msg-tipo span').html('Nesta opção, as matrículas informadas pelo cartório são de todos os registros encontrados em nome do pesquisado (atuais e passados). ');
				form.find('div#msg-tipo').show();

				form.find('div#msg-ciencia label').html('Tenho ciência de que a pesquisa hora requerida irá informar todas as matriculas com registros do pesquisado (atuais e passados).');
				form.find('div#msg-ciencia').show();
				break;
		}
	});

	$('div#nova-pesquisa').on('click','input[name=id_produto_item]',function(e) {
		var form = $(this).closest('form');

		if ($(this).data('codigo')=='1009') {
			form.find('.periodo input').prop('disabled',false);
		} else {
			form.find('.periodo input').val('').prop('disabled',true);
		}
	});

	$('div#nova-pesquisa').on('click','button.incluir-serventia',function(e){
		incluir_serventia('N');
	});

	$('div#nova-pesquisa').on('click','button.incluir-todas-serventias',function(e){
		incluir_serventia('S');
	});

	$('div#nova-pesquisa').on('click', 'div#serventias-selecionadas button.remover-serventia', function (e) {
		var form = $(this).closest('form');
		var id_serventia = $(this).data('idserventia');
		$('div#serventias-selecionadas div.serventia#serventia'+id_serventia).remove();
		calcular_valores();
	});

	$('div#nova-pesquisa').on('click','div.radio input[name=id_tipo_custa]',function(e) {
		var form = $(this).closest('form');

		form.find('div#numero-processo').hide();
		form.find('div#documento-isencao').hide();
		form.find('div#msg-isencao').hide();
		switch ($(this).val()) {
			case '1':
				form.find('div#numero-processo').show();
				form.find('div#pesquisa-valores').slideUp();
				break;
			case '2':
				calcular_valores();
				break;
			case '3':
				swal({
					title: 'Atenção?',
					text: "Houve despacho de deferimento de gratuidade emitido pela AJG?",
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'Não',
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim',
					showLoaderOnConfirm: true,
				}).then(function( retorno ) {
					if (retorno == true) {
						form.find('div#documento-isencao').show();
						form.find('div#pesquisa-valores').slideUp();
					}
				}).catch(function(e) {
					form.find('div#msg-isencao').show();
					form.find('input[name=id_tipo_custa]').prop('checked', false);
					calcular_valores();
				});
				
				break;
		}
	});

	$('div#nova-pesquisa').on('click','button.incluir-pesquisa',function(e) {
		var form = $('form[name=form-nova-pesquisa]');
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('div#serventias-selecionadas div.serventia').length<=0) {
			erros.push('Selecione pelo menos uma serventia.');
		}
		if (form.find('input[name=in_isenta]').is(':checked')) {
			if (form.find('input[name=numero_processo]').val()=='') {
				erros.push('O campo número do processo é obrigatório.');
			} else {
				if (total_processos(form.find('input[name=numero_processo]').val())==0) {
					erros.push('O número do processo digitado não foi encontrado.');
				}
			}
		}
		if (form.find('input[name=de_chave_pesquisa]').is(':disabled') || form.find('input[name=de_chave_pesquisa]').val()=='') {
			erros.push('O campo pesquisa é obrigatório.');
		} else {
			switch (form.find('input[name=id_chave_pesquisa_pesquisa]:checked').val()) {
				case '2':
					if (!validarCPF(form.find('input[name=de_chave_pesquisa]').val())) {
						erros.push('O CPF digitado é inválido');
					}
					break;
				case '3':
					if (!validarCNPJ(form.find('input[name=de_chave_pesquisa]').val())) {
						erros.push('O CNPJ digitado é inválido');
					}
					break;
			}
		}
		if (form.find('input[name=id_chave_pesquisa_pesquisa]:checked').length>0) {
			switch (form.find('input[name=id_chave_pesquisa_pesquisa]:checked').val()) {
				case '2':
					if (form.find('input[name=de_chave_complementar]').val()=='') {
						erros.push('O campo titular do CPF é obrigatório.');
					}
					break;
				case '3':
					if (form.find('input[name=de_chave_complementar]').val()=='') {
						erros.push('O campo razão social do CNPJ é obrigatório.');
					}
					break;
			}
		}
		if (form.find('input[name=id_tipo_pesquisa]:checked').length<=0) {
			erros.push('O tipo de pesquisa é obrigatório');
		} else {
			if (form.find('input[name=in_ciencia_pesquisa]:checked').length<=0) {
				erros.push('Você precisa declarar que está ciente do uso da pesquisa eletrônica');
			}
		}
        if (form.find('input[name=id_tipo_custa]').length>0) {
            if (form.find('input[name=id_tipo_custa]:checked').length<=0) {
                erros.push('O campo tipo de custa é obrigatório');
            } else {
				if (form.find('input[name=id_tipo_custa]:checked').val()==3 && form.find('div#arquivos-isencao div.arquivo').length<=0) {
					erros.push('O arquivo documento de isenção de emolumentos é obrigatório.');
				}
				if (form.find('input[name=id_tipo_custa]:checked').val()==1 && form.find('input[name=numero_processo]').val()=='') {
					erros.push('O número do processo é obrigatório.');
				}
			}
		}

		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
			return false;
		} else {
			if (form.find('input[name=id_tipo_custa]:checked').length<=0 || form.find('input[name=id_tipo_custa]:checked').val()==2) {
				if (parseFloat($("input[name=saldo_atual]").val())<parseFloat($("input[name=va_pedido]").val())) {
					valor_compra = $("input[name=va_pedido]").val()-parseFloat($("input[name=saldo_atual]").val());
					saldo_insuficiente();
					status_modal(obj_modal, false, false, false);
					return false;
				}
			}

			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/pesquisa-eletronica/inserir-pesquisa',
				data: form.serialize() + '&id_compra_credito=' + $('input[name=id_compra_credito]').val(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'saldo_insuficiente':
							valor_compra = $("input[name=va_pedido]").val()-parseFloat($("input[name=saldo_atual]").val());
							saldo_insuficiente();
							break;
						case 'sucesso':
							var alerta = swal({title: 'Sucesso!',html: retorno.msg,type: 'success'});
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10502", "error");
				}
			});
		}
	});

	$('div#detalhes-pesquisa').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/pesquisa-eletronica/detalhes',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10503", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#detalhes-pesquisa').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#resultado-pesquisa').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/pesquisa-eletronica/resultado',
				data: 'id_pedido='+id_pedido,
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$('table#pedidos tr#'+id_pedido).removeClass('linha-notificacao-danger');
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10503", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#resultado-pesquisa').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#imprimir-resultado-pesquisa').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        $.ajax({
            type: "POST",
            url: url_base+'servicos/pesquisa-eletronica/imprimir-resultado',
            data: {'id_pedido':id_pedido},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($(this));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10513", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

    $('div#recibo-pesquisa').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_pedido>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/pesquisa-eletronica/gerar-recibo-pesquisa',
                data: {'id_pedido':id_pedido},
                context: this,
                beforeSend: function() {
                    $(this).find('.modal-body div.form').hide();
                    status_modal($(this), true, true, true);
                },
                success: function(retorno) {
                    status_modal($(this), false, false, false);
                    $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
                },
                error: function (request, status, error) {
                    status_modal($(this), false, false, false);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });
    $('div#recibo-pesquisa').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    var form = $('form[name=form-filtro]');

    form.on('change','select[name=id_cidade]',function(e) {
        id_cidade = $(this).val();

        $.ajax({
            type: "POST",
            url: 'listarServentias',
            data: 'id_cidade='+id_cidade,
            beforeSend: function() {
                form.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
            },
            success: function(serventias) {
                form.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
                if (serventias.length>0) {
                    $.each(serventias,function(key,serventia) {
                        form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                    });
                    form.find('select[name=id_serventia]').prop('disabled',false);
                } else {
                    form.find('select[name=id_serventia]').prop('disabled',true);
                }
            },
            error: function (request, status, error) {
                //console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    // RELATORIO PESQUISA
    $('table#pedidos').on('click','input#selecionar-todas',function(e) {
        if ($(this).is(':checked')) {
            $('table#pedidos input.pesquisa:not(:checked)').trigger('click');
        } else {
            $('table#pedidos input.pesquisa:checked').trigger('click');
        }
    });

    $('table#pedidos').on('click','input.pesquisa',function(e) {
        var total = $('table#pedidos input.pesquisa:checked').length;

        if (total<=0) {
            $('div#alert span.total').html('0 pesquisas selecionadas');
            $('div#alert button').prop('disabled',true).addClass('disabled');

            $('div#alert button span').html('');
        } else {
            $('div#alert span.total').html((total==1?'1 pesquisa selecionada':total+' pesquisas selecionadas'));
            $('div#alert button').prop('disabled',false).removeClass('disabled');

            $('div#alert button span').html('('+total+')');
        }
    });

    $('div#relatorio-pesquisa').on('show.bs.modal', function (event) {
        var ids_pesquisas = [];

        $('table#pedidos input.pesquisa:checked').each(function(e) {
            ids_pesquisas.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/pesquisa-eletronica/relatorio-pesquisas',
            data: {'ids_pesquisas':ids_pesquisas},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-pesquisa').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    $('div#relatorio-pesquisa').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-relatorio]').submit();
        $('div#relatorio-pesquisa').modal('hide');
    });
	//FIM RELATORIO PESQUISA

    $('div#nova-pesquisa').on('blur','input[name=de_chave_pesquisa]', function (e) {
        var form = $(this).closest('form');
        var nu_cpf_cnpj = $(this).val();
        var val = form.find('input[name=id_chave_pesquisa_pesquisa]:checked').val();

        if ((val == 2 || val == 3) && nu_cpf_cnpj !='') {
            $.ajax({
                type: "POST",
                url: url_base+'listarPessoa',
                data: {'nu_cpf_cnpj':nu_cpf_cnpj},
                success: function (dadosPessoa) {
                    if(typeof dadosPessoa.pessoa !== 'undefined'){
                        form.find('input[name=de_chave_complementar]').val(dadosPessoa.pessoa.no_pessoa).prop('readonly', true);
                    }else{
                        form.find('input[name=de_chave_complementar]').val('').prop('readonly', false);
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
        }
    });

});
function incluir_serventia(todas) {
	var form = $('form[name=form-nova-pesquisa]');
	var id_cidade = form.find('select[name=id_cidade] option:selected').val();
	
	if (todas=='N') {
		var serventias = form.find('select[name=id_serventia] option:selected');
	} else {
		var serventias = form.find('select[name=id_serventia] option[value!="0"]');
	}

	serventias.each(function (e) {
		if (form.find('div#serventias-selecionadas div.serventia#serventia'+$(this).val()).length>0) {
			if (todas=='N') {
				swal("Ops!", "Esta serventia já foi escolhida", "warning");
			}
		} else {
			html = 	'<div id="serventia'+$(this).val()+'" class="serventia btn-group" data-noserventia="'+$(this).text()+'">';
				html +=	'<button class="btn btn-sm btn-primary" type="button">'+$(this).text()+'</button>';
				html += '<input type="hidden" name="id_serventia[]" class="ids_serventia" value="'+$(this).val() +'">';
				html += '<button class="remover-serventia btn btn-sm btn-danger" type="button" data-idserventia="'+$(this).val()+'"><i class="fa fa-times"></i></button>';
			html += '</div> ';

			$("div#serventias-selecionadas").append(html);
		}
	});
	calcular_valores();
}
function calcular_valores() {
	var form = $('form[name=form-nova-pesquisa]');
	var obj_modal = form.closest('.modal');
	var total_serventias = form.find('div#serventias-selecionadas div.serventia').length;
    var ids_serventia = [];

    form.find('div.serventia .ids_serventia').each(function(e) {
        ids_serventia.push($(this).val());
    });

	if (form.find('input[name=id_tipo_custa]:checked').length<=0 || form.find('input[name=id_tipo_custa]:checked').val()==2) {
		if (total_serventias>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/produto-item/calcular',
				data: {'id_produto_item':25, 'ids_serventias':ids_serventia},
				beforeSend: function() {
					status_modal(obj_modal, true, true, true);
				},
				success: function(valores) {
					status_modal(obj_modal, false, false, false);

					if (valores.length>0) {
						var valor_total = 0;
						if (!form.find('div#pesquisa-valores').is(':visible')) {
							form.find('div#pesquisa-valores').slideDown();
						}
						form.find('div#pesquisa-valores table tbody').html('');

						$.each(valores,function(key,valor) {
							var nova_linha = $("<tr>");

							if (valor.descricao=='Total') {
                                colunas = '<th>Total para '+total_serventias+' '+(total_serventias>1?'serventias':'serventia')+'</th>';
								colunas += '<th class="real text-right">'+parseFloat(valor.va_preco)+'</th>';
							} else {
								colunas = '<td>'+valor.descricao+'</td>';
								colunas += '<td class="real text-right">'+parseFloat(valor.va_preco)+'</td>';

								valor_total += parseFloat(valor.va_preco);
							}
							
							nova_linha.append(colunas);

							form.find('div#pesquisa-valores table tbody').append(nova_linha);
						});

						//var nova_linha_total = $("<tr>");
						
						//colunas = '<th>Total para '+total_serventias+' '+(total_serventias>1?'serventias':'serventia')+'</th>';
						//colunas += '<th class="real text-right">'+(valor_total)+'</th>';

						//nova_linha_total.append(colunas);

						//form.find('div#pesquisa-valores table tbody').append(nova_linha_total);

						form.find('input[name=va_pedido]').val((valor_total).toFixed(2));
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro VAL501", "error");
				}
			});
		} else {
			if (form.find('div#pesquisa-valores').is(':visible')) {
				form.find('div#pesquisa-valores').slideUp();
			}
			$("div#pesquisa-valores table tbody").html('');
		}
	}
}