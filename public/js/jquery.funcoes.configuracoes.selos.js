$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	form = $('form[name=form-selos]');

	form.on('div#novo-selo','show.bs.modal',function(e){
		e.stopPropagation();
		$('div#novo-selo .modal-body div.form').fadeIn('fast');
	});

	form.on('change.bs.fileinput','div.selos',function(e){
		e.stopPropagation();
		var no_arquivo = form.find('div.selos input[name=no_arquivo]').val().toLowerCase()
		var no_arquivo = no_arquivo.split('\\');
		if (!(/\.(xml)$/i).test(no_arquivo)) {
			swal("Ops!", "Você deve selecionar um arquivo do tipo XML.", "warning");
			$('form[name=form-selos] div.selos').fileinput('clear');
		}
	});

	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('input[name=no_arquivo]').val()=='') {
			erros.push('O campo arquivo é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: 'selos/salvar',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#novo-selo .modal-body div.carregando').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div#novo-selo .modal-body div.carregando').hide();
					
					switch (retorno) {
						case 'ERRO_04':
							swal("Erro!", "Este ao buscar o tipo de emolumento. Erro nº X", "error").then(function(){
								location.reload();
							});
							break;
						case 'ERRO_03':
							swal("Erro!", "Este arquivo já foi importado. Erro nº X", "error").then(function(){
								location.reload();
							});
							break;
						case 'ERRO_02':
							swal("Erro!", "Erro ao criar o diretório. Erro nº X", "error").then(function(){
								location.reload();
							});
							break;
						case 'ERRO_01':
							swal("Erro!", "Ero ao importar o aquivo de selos. Erro nº X", "error").then(function(){
								location.reload();
							});
							break;
						case 'ERRO':
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function(){
								location.reload();
							});
							break;
						case 'SUCESSO':
							swal("Sucesso!", "Selos salvo com sucesso.", "success").then(function(){
								location.reload();
							});
							break;
						default:
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
						break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});


});