$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('div#nova-resposta').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'protocolo/nova-resposta',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').show();
					$('div#nova-resposta .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#nova-resposta .modal-body div.form').html(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide();
					$('div#nova-resposta .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#nova-resposta').on('hidden.bs.modal', function (event) {
		$('div#nova-resposta .modal-body div.carregando').removeClass('flutuante').show();
		$('div#nova-resposta .modal-body div.form').html('');
	});
	
	$('div#nova-resposta').on('submit','form[name=form-nova-resposta]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		if (form.find('select[name=id_protocolo_natureza]').val()=='0') {
			erros.push('O campo natureza formal é obrigatório');
		}
		if (form.find('input[name=no_assunto]').val()=='') {
			erros.push('O campo assunto é obrigatório');
		}
		if (form.find('input[name=no_apresentante]').val()=='') {
			erros.push('O campo nome do apresentante é obrigatório');
		}
		if (form.find('input[name=tp_pessoa_apresentante]:checked').length<=0) {
			erros.push('O campo tipo do documento é obrigatório');
		}
		if (form.find('input[name=nu_cpf_cnpj_apresentante]').val()=='') {
			erros.push('O campo documento do apresentante é obrigatório');
		} else {
			switch (form.find('input[name=tp_pessoa_apresentante]:checked').val()) {
				case 'F':
					if (!validarCPF(form.find('input[name=nu_cpf_cnpj_apresentante]').val())) {
						erros.push('O CPF digitado é inválido');
					}
					break;
				case 'J':
					if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj_apresentante]').val())) {
						erros.push('O CNPJ digitado é inválido');
					}
					break;
			}	
		}
		if (form.find('input.a_id_bem_imovel').length<=0) {
			erros.push('Ao menos um imóvel é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-resposta div').html(erros.join('<br />'));
			if (!form.find('div.erros-resposta').is(':visible')) {
				form.find('div.erros-resposta').slideDown();
			}
		} else {
			form.find('div.erros-resposta').slideUp();
			$.ajax({
				type: "POST",
				url: 'protocolo/inserir-resposta',
				data: form.serialize(),
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});
	
	$('div#nova-resposta').on('click','form[name=form-nova-resposta] input[name=tp_pessoa_apresentante]',function(e) {
		$('div#nova-resposta form[name=form-nova-resposta]').find('input[name=nu_cpf_cnpj_apresentante]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case 'F':
				$('div#nova-resposta form[name=form-nova-resposta]').find('input[name=nu_cpf_cnpj_apresentante]').parent('div').find('label span').html('(CPF)');
				$('div#nova-resposta form[name=form-nova-resposta]').find('input[name=nu_cpf_cnpj_apresentante]').mask('000.000.000-00');
				break;
			case 'J':
				$('div#nova-resposta form[name=form-nova-resposta]').find('input[name=nu_cpf_cnpj_apresentante]').parent('div').find('label span').html('(CNPJ)');
				$('div#nova-resposta form[name=form-nova-resposta]').find('input[name=nu_cpf_cnpj_apresentante]').mask('00.000.000/0000-00');
				break;
		}
	});

	$('div#nova-resposta').on('click','button.incluir-imovel',function(e) {
		var erros = new Array();
		var form = $('div#nova-resposta').find('form[name=form-nova-resposta]');
		var matricula_bem_imovel = form.find('input[name=matricula_bem_imovel]');
		if (matricula_bem_imovel.val()=='') {
			erros.push('O campo matrícula do imóvel é obrigatório');
		} else if (total_imoveis(form.find('input[name=matricula_bem_imovel]').val())==0) {
			erros.push('A matrícula do imóvel digitada não existe');
		}
		if (form.find('input.a_matricula_bem_imovel[value='+matricula_bem_imovel.val()+']').length>0) {
			erros.push('A matrícula do imóvel digitada já foi inserida');
		}
		if (erros.length>0) {
			form.find('div.erros-imoveis div').html(erros.join('<br />'));
			if (!form.find('div.erros-imoveis').is(':visible')) {
				form.find('div.erros-imoveis').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: 'bem-imovel/detalhesJSON',
				data: 'matricula_bem_imovel='+matricula_bem_imovel.val(),
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					//console.log(retorno);
					$('div#nova-resposta .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						form.find('div.erros-imoveis').slideUp();
						var novaLinha = $('<tr id="'+retorno.id_bem_imovel+'">');
						var colunas = "";
						colunas += '<td>';
							colunas += '<input type="hidden" name="a_id_bem_imovel[]" class="a_id_bem_imovel" value="'+retorno.id_bem_imovel+'" />';
							colunas += '<input type="hidden" name="a_matricula_bem_imovel[]" class="a_matricula_bem_imovel" value="'+retorno.matricula_bem_imovel+'" />';
							colunas += retorno.matricula_bem_imovel;
						colunas += '</td>';
						colunas += '<td>';
								if (typeof retorno.proprietarios !== "undefined") {
								$.each(retorno.proprietarios,function(key,proprietario) {
									colunas += '<span class="label label-primary">'+proprietario.proprietario_bem_imovel.no_proprietario_bem_imovel+' ('+proprietario.proprietario_bem_imovel.nu_cpf_cnpj+')</span> ';
								});
							}
					    colunas += '</td>';
						colunas += '<td>';
							colunas += '<div class="btn-group">';
								colunas += '<button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
									colunas += 'Ações <span class="caret"></span>';
								colunas += '</button>';
								colunas += '<ul class="dropdown-menu black">';
									colunas += '<li><a href="#" data-toggle="modal" data-target="#detalhes-imovel" data-idbemimovel="'+retorno.id_bem_imovel+'" data-matriculabemimovel="'+retorno.matricula_bem_imovel+'">Ver detalhes</a></li>';
									colunas += '<li><a href="#" class="remover-imovel" data-idbemimovel="'+retorno.id_bem_imovel+'">Remover imóvel</a></li>';
								colunas += '</ul>';
							colunas += '</div>';
						colunas += '</td>';
				
						novaLinha.append(colunas);
						$("table#imoveis-protocolo tbody").append(novaLinha);
						
						form.find('input[name=matricula_bem_imovel]').val('');
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					//console.log(request.responseText);
				}
			});
		}
	});
	$('div#nova-resposta').on('click','table#imoveis-protocolo a.remover-imovel',function() {
		var id_bem_imovel = $(this).data('idbemimovel');
		$('table#imoveis-protocolo tr#'+id_bem_imovel).remove();
	});

	$('div#nova-resposta').on('submit','form[name=form-nova-observacao]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		if (form.find('textarea[name=de_observacao]').val()=='') {
			erros.push('O campo observacao é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-observacao div').html(erros.join('<br />'));
			if (!form.find('div.erros-observacao').is(':visible')) {
				form.find('div.erros-observacao').slideDown();
			}
		} else {
			form.find('div.erros-observacao').slideUp();
			$.ajax({
				type: "POST",
				url: 'protocolo/inserir-observacao',
				data: form.serialize(),
				beforeSend: function() {
					$('div#nova-resposta .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					$('div#nova-resposta .modal-body div.carregando').hide().removeClass('flutuante');
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							if (typeof retorno.observacao !== "undefined") {
								var novaLinha = $("<tr>");
								var colunas = "";
								colunas += '<td>'+retorno.usuario+'</td>';
								colunas += '<td>'+retorno.dt_formatada+'</td>';
								colunas += '<td>'+retorno.observacao.de_observacao+'</td>';
								
								novaLinha.append(colunas);
								$("table#historico-observacoes").prepend(novaLinha);
								form.find('input[type=reset]').trigger('click');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#arquivo-protocolo').on('show.bs.modal', function (event) {
		var id_protocolo_arquivo = $(event.relatedTarget).data('idarquivo');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido+' ('+no_arquivo+')');
		
		if (id_protocolo_arquivo>0) {
			$.ajax({
				type: "POST",
				url: 'protocolo/arquivo',
				data: 'id_protocolo_arquivo='+id_protocolo_arquivo,
				beforeSend: function() {
					$('div#arquivo-protocolo .modal-body div.carregando').show();
					$('div#arquivo-protocolo .modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$('div#arquivo-protocolo').addClass('total-height');
								resize_modal($('div#arquivo-protocolo'));
							} else {
								$('div#arquivo-protocolo').removeClass('total-height');
								$('div#arquivo-protocolo').find('.modal-body').removeAttr('style');
							}
							$('div#arquivo-protocolo .modal-body div.form').html(retorno.view);
							$('div#arquivo-protocolo .modal-body div.carregando').hide();
							$('div#arquivo-protocolo .modal-body div.form').fadeIn('fast');
							
							if (retorno.download=='true') {
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').show();
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').hide();
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});
});
function retorno_imovel(retorno) {
	$('form[name=form-nova-resposta]').find('input[name=matricula_bem_imovel]').val(retorno.matricula_bem_imovel);
	$('div#novo-imovel').modal('hide');
}