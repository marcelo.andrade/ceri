$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#devolver-valor-produto').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');

		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/devolucao-valor/novo-comprovante',
            data: {'id_alienacao':id_alienacao},
			beforeSend: function() {
				$('div#devolver-valor-produto .modal-body div.carregando').show();
				$('div#devolver-valor-produto .modal-body div.form').hide();
			},
			success: function(retorno) {
				$('div#devolver-valor-produto .modal-body div.form').html(retorno);
				$('div#devolver-valor-produto .modal-body div.carregando').hide();
				$('div#devolver-valor-produto .modal-body div.form').fadeIn('fast');
				total_selecionadas=0;
			},
			error: function (request, status, error) {
				console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	$('div#devolver-valor-produto').on('hidden.bs.modal', function (event) {
		$('div#devolver-valor-produto .modal-body div.carregando').removeClass('flutuante').show();
		$('div#devolver-valor-produto .modal-body div.form form').html('');
	});
	
	$('div#devolver-valor-produto').on('click','button.enviar-comprovante',function(e) {
		e.preventDefault();
		var erros = new Array();
		var form = $('form[name=form-alienacao-devolver-valor]');

        if (form.find('textarea[name=de_repasse_devolucao]').val().trim()=='') {
            erros.push('O campo justificativa é obrigatório.');
        }
        if (!form.find('div.arquivo').length > 0) {
            erros.push('O campo arquivo comprovante é obrigatório.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/devolucao-valor/inserir-comprovante',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#novo-comprovante .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#devolver-valor-produto').on('change.bs.fileinput','form[name=form-novo-comprovante] div.arquivo',function(e){
		e.stopPropagation();

		var no_arquivo = $('div#novo-comprovante form[name=form-novo-comprovante] div.arquivo input[name=no_arquivo_comprovante]').val().toLowerCase();
		var no_arquivo = no_arquivo.split('\\');
		var no_arquivo = no_arquivo[no_arquivo.length-1];

		if (!(/\.(pdf|png|jpg|bmp|docx|doc)$/i).test(no_arquivo)) {
			swal("Ops!", "Você deve selecionar um arquivo dos tipos: PDF, PNG, JPG, BMP, DOCX ou DOC.", "warning");
			$('div#novo-comprovante form[name=form-novo-comprovante] div.arquivo').fileinput('clear');
		}
	});
	
	$('div#arquivo-comprovante').on('show.bs.modal', function (event) {
		var id_alienacao_pagamento = $(event.relatedTarget).data('idpagamento');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (id_alienacao_pagamento>0) {
			$.ajax({
				type: "POST",
				url: 'alienacao-pagamento/comprovante',
				data: 'id_alienacao_pagamento='+id_alienacao_pagamento,
				beforeSend: function() {
					$('div#arquivo-comprovante .modal-body div.carregando').show();
					$('div#arquivo-comprovante .modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$('div#arquivo-comprovante').addClass('total-height');
								resize_modal($('div#arquivo-comprovante'));
							} else {
								$('div#arquivo-comprovante').removeClass('total-height');
								$('div#arquivo-comprovante').find('.modal-body').removeAttr('style');
							}

							$('div#arquivo-comprovante .modal-body div.form').html(retorno.view);
							$('div#arquivo-comprovante .modal-body div.carregando').hide();
							$('div#arquivo-comprovante .modal-body div.form').fadeIn('fast');
		
							if (retorno.download=='true') {
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').show();
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').hide();
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});

    $('div#relatorio-valores-devolvidos').on('show.bs.modal', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos input.alienacao:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        $(this).find('.modal-header h4.modal-title span').html('(Diligência não efetuada)');
        if (ids_alienacoes.length > 10) {
        	$(this).find('.modal-body button.salvar-relatorio').css('display','block');
		}

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/devolucao-valor/relatorio-devolver-valor-produto',
            data: {'ids_alienacoes':ids_alienacoes},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-valores-devolvidos').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
        window.location.reload();
    });

    $('div#relatorio-valores-devolvidos').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-relatorio-devolver-valor-produto]').submit();
        $('div#relatorio-valores-devolvidos').modal('hide');
    });
});