 $(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#novo-arquivo').on('show.bs.modal', function (event) {
		var id_tipo_arquivo_grupo_produto = $(event.relatedTarget).data('idtipoarquivo');
		var limite = $(event.relatedTarget).data('limite');
		var token = $(event.relatedTarget).data('token');
		var id_flex = $(event.relatedTarget).data('idflex')?$(event.relatedTarget).data('idflex'):0;
		var protocolo = $(event.relatedTarget).data('protocolo')?$(event.relatedTarget).data('protocolo'):'';
		
		if (total_arquivo(id_tipo_arquivo_grupo_produto,'',id_flex)<parseInt(limite) || limite=='0') {
			$.ajax({
				type: "POST",
				url: url_base+'arquivos/novo',
				data: {'id_tipo_arquivo_grupo_produto':id_tipo_arquivo_grupo_produto,'token':token,'id_flex':id_flex,'protocolo':protocolo,'index_arquivos':index_arquivos},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form form').hide();
				},
				success: function(retorno) {
					status_modal($(this),false,false,false);
					$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ501", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		} else {
			swal("Ops!", "Você alcançou o limite de "+limite+" arquivo(s) para esta seção.", "warning");
			return false;
		}
	});
	$('div#novo-arquivo').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
	});
	
	$('form[name=form-novo-arquivo]').on('change','label.arquivo-upload input[type=file]',function(e) {
		if (!$(this).val() == '') {
			var form = $(this).closest('form');

			var id_tipo_arquivo_grupo_produto = $(this).data('idtipoarquivo');
			var id_flex = $(this).data('idflex');
			
			var no_arquivo = $(this).val().split('\\');
			var no_arquivo = remove_caracteres(no_arquivo[no_arquivo.length-1]);
			var ext_arquivo = no_arquivo.split('.');
			var ext_arquivo = ext_arquivo[ext_arquivo.length-1];
			var ext_permitidas = extensoes_permitidas(id_tipo_arquivo_grupo_produto);
			if (ext_permitidas.indexOf(ext_arquivo.toLowerCase())<0) {
				swal("Ops!", "O arquivo selecionado é inválido, tente novamente.", "warning");
				$(this).replaceWith($(this).val('').clone(true));
				return false;
			} else if (total_arquivo(id_tipo_arquivo_grupo_produto,remove_caracteres(no_arquivo).toLowerCase(),id_flex)>0) {
				swal("Ops!", "O arquivo selecionado já foi inserido.", "warning");
				$(this).replaceWith($(this).val('').clone(true));
				return false;
			} else {
				form.find('div.msg-arquivo').slideUp();
				form.find('div.erros').slideUp();
				form.find('label.arquivo-upload h4').html(remove_caracteres(no_arquivo).toLowerCase());
				form.find('label.arquivo-upload h4').prop('title',remove_caracteres(no_arquivo).toLowerCase());
				switch (ext_arquivo) {
					case 'pdf':
						var icone_arquivo = '<i class="fa fa-file-pdf-o"></i>';
						break;
					case 'png': case 'jpg': case 'bmp': case 'tif':
						var icone_arquivo = '<i class="fa fa-file-image-o"></i>';
						form.find('div.msg-arquivo').slideDown();
						form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
						break;
					case 'doc': case 'docx':
						var icone_arquivo = '<i class="fa fa-file-word-o"></i>';
						form.find('div.msg-arquivo').slideDown();
						form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
						break;
					case 'xls': case 'xlsx':
						var icone_arquivo = '<i class="fa fa-file-excel-o"></i>';
						form.find('div.msg-arquivo').slideDown();
						form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
						break;
					case 'txt':
						var icone_arquivo = '<i class="fa fa-file-text-o"></i>';
						form.find('div.msg-arquivo').slideDown();
						form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
						break;
					case 'ppt': case 'pptx': case 'pps': case 'ppsx':
						var icone_arquivo = '<i class="fa fa-file-powerpoint-o"></i>';
						break;
					default:
						var icone_arquivo = '<i class="fa fa-file-o"></i>';
						break;
				}
				form.find('label.arquivo-upload figure').html(icone_arquivo);
			}
		}
	});
	
	$('div#novo-arquivo').on('click','button.enviar-arquivo', function(e) {
		var form = $('form[name=form-novo-arquivo]');
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		if (form.find('input#arquivo-upload').val()=='') {
			erros.push('Selecione um arquivo.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				url: url_base+'arquivos/inserir',
				type: 'POST',
				data: data,
				beforeSend: function() {
					status_modal(obj_modal, true, false, false);
					form.find('label.arquivo-upload').hide();
					form.find('div.progresso-upload').show();
				},
				success: function(retorno) {
					status_modal(obj_modal, false, false, false);
					retorno_arquivo(retorno);
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ502", "error");
				},
				cache: false,
				contentType: false,
				processData: false,
				xhr: function() {
					var myXhr = $.ajaxSettings.xhr();
					if (myXhr.upload) {
						myXhr.upload.addEventListener('progress', function (e) {
							if (e.lengthComputable) {
								var porcentagem = e.loaded / e.total;
								porcentagem = parseInt(porcentagem * 100);
								
								form.find('div.progresso-upload div.progress-bar').css('width',porcentagem+'%');
								
								if (porcentagem >= 100) {
									status_modal(obj_modal, true, true, false);
									form.find('div.progresso-upload').hide();
								} else if (porcentagem > 10) {
									form.find('div.progresso-upload div.progress-bar').html(porcentagem+'%');
								}
							}
						}, false);
					}
					return myXhr;
				}
			});
		}
	});
	
	$('body').on('click','button.remover-arquivo',function(e) {
		e.preventDefault();
		var linha = $(this).data('linha');
		var token = $(this).data('token');

		$.ajax({
			type: "POST",
			url: url_base+'arquivos/remover',
			data: {'token':token,'linha':linha},
			success: function(retorno) {
				switch (retorno.status) {
					case 'erro':
						swal("Erro!",retorno.msg,"error");
						break;
					case 'sucesso':
						swal("Sucesso!",retorno.msg,"success").then(function() {
							remove_arquivo(linha,retorno.arquivo);
						});
						break;
				}
				
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ503", "error");
			}
		});
	});

	$('div#nova-assinatura').on('show.bs.modal', function (event) {
		var token = $(event.relatedTarget).data('token');
		
		$.ajax({
			type: "POST",
			url: url_base+'arquivos/assinatura/nova',
			data: {'token':token},
			context: this,
			beforeSend: function() {
				status_modal($(this),true,true,false);
				$(this).find('.modal-body div.form').hide();
			},
			success: function(retorno) {
				status_modal($(this),false,false,false);
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ504", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#nova-assinatura').on('hidden.bs.modal', function (event) {
		status_modal($(this),false,false,false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#visualizar-arquivo').on('show.bs.modal', function (event) {
		var id_arquivo_grupo_produto = $(event.relatedTarget).data('idarquivo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (id_arquivo_grupo_produto>0) {
			$.ajax({
				type: "POST",
				url: url_base+'arquivos/visualizar',
				data: {'id_arquivo_grupo_produto':id_arquivo_grupo_produto},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$(this).addClass('total-height');
								resize_modal($('div#visualizar-arquivo'));
							} else {
								$(this).removeClass('total-height');
								$(this).find('.modal-body').removeAttr('style');
							}
							
							status_modal($(this),false,false,false);
							$(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
		
							if (retorno.download=='true') {
								$(this).find('.modal-footer a#arquivo-download').show();
								$(this).find('.modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$(this).find('.modal-footer a#arquivo-download').hide();
								$(this).find('.modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ505", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#visualizar-certificado').on('show.bs.modal', function (event) {
		var id_arquivo_grupo_produto = $(event.relatedTarget).data('idarquivo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (id_arquivo_grupo_produto>0) {
			$.ajax({
				type: "POST",
				url: url_base+'arquivos/certificado',
				data: {'id_arquivo_grupo_produto':id_arquivo_grupo_produto},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form').hide();
				},
				success: function(retorno) {
					status_modal($(this),false,false,false);
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ506", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

});
function extensoes_permitidas(tipo_arquivo) {
	var permitidas = ['pdf','png','jpg','bmp','tif','doc','docx','xls','xlsx','txt','ppt','pptx','pps','ppsx'];
	return permitidas;
}
function retorno_arquivo(retorno) {
	botao_arquivo = '<div class="arquivo btn-group" id="'+index_arquivos+'" data-inassdigital="'+retorno.arquivo.in_ass_digital+'">';
		botao_arquivo += '<button type="button" class="btn btn-primary">'+retorno.arquivo.no_arquivo+'</button>';
		botao_arquivo += '<input type="hidden" name="no_arquivo[]" value="'+retorno.arquivo.no_arquivo+'" />';
		if (retorno.arquivo.in_ass_digital=='S') {
			botao_arquivo += '<button type="button" class="icone-assinatura btn btn-warning"><i class="fa fa-unlock-alt"></i></button>';
		}
		botao_arquivo += '<button type="button" class="remover-arquivo btn btn-danger" data-linha="'+index_arquivos+'" data-token="'+retorno.token+'"><i class="fa fa-times"></i></button>';
	botao_arquivo += '</div> ';
	$('div#novo-arquivo').modal('hide');
	total_arquivos++;
	if (retorno.arquivo.in_ass_digital=='S') {
		total_arquivos_assinaveis++;
	}
	index_arquivos++;
	switch (retorno.arquivo.id_tipo_arquivo_grupo_produto) {
		case 1:
			obj_arquivos = $('form[name=form-nova-alienacao] div#arquivos');
			break;
		case 9:
			obj_arquivos = $('form[name=form-nova-penhora] div#arquivos-gerais');
			break;
		case 10:
			obj_arquivos = $('form[name=form-nova-penhora] div#arquivos-isencao');
			break;
		case 11:
			obj_arquivos = $('form[name=form-resposta-exigencia] div#arquivos-pendencias');
			if (obj_arquivos.data('local')=='resposta') {
				$('form[name=form-resposta-exigencia]').find('textarea[name=de_resposta]').val('A fim de podermos cumprir as determinações deste digníssimo juíz, requeremos respeitosamente a análise das pendências em anexo.');
			}
			break;
		case 12:
			obj_arquivos = $('form[name=form-resposta-averbacao] div#arquivo-imovel-'+retorno.arquivo.id_flex);
			break;
		case 13:
			obj_arquivos = $('form[name=form-resposta-averbacao] div#arquivos-averbacao');
			$('form[name=form-resposta-averbacao] textarea[name=de_resposta]').val('Conforme determinação contida no mandado acima a penhora foi devidamente efetivada conforme documento em anexo.');
			break;
		case 15:
			obj_arquivos = $('div#arquivos-gerais');
			break;
		case 16:
			obj_arquivos = $('form[name=form-novo-oficio] div#arquivos-isencao');
			break;
		case 18:
			obj_arquivos = $('form[name=form-nova-pesquisa] div#arquivos-isencao');
			break;
		case 19:
			obj_arquivos = $('form[name=form-novo-resultado] div#arquivos-resultado');
			break;
		case 20:
			obj_arquivos = $('form[name=form-nova-certidao] div#arquivos-isencao');
			break;
		case 21:
			obj_arquivos = $('form[name=form-novo-resultado] div#arquivos-resultado');
			break;
		case 22:
			obj_arquivos = $('form[name=form-novo-andamento] div#arquivos-andamento');
			break;
		case 25:
			obj_arquivos = $('form[name=form-observacao] div#arquivos-observacao');
			break;
        case 26:
            obj_arquivos = $('form[name=form-alienacao-devolver-valor] div#arquivos-alienacao-devolver-valor');
            break;
	}

	obj_arquivos.prepend(botao_arquivo);

	if (total_arquivos_assinaveis>=0) {
		if (total_arquivos==1) {
			$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivo foi inserido, <b>'+total_arquivos_assinaveis+'</b> '+(total_arquivos_assinaveis>1?'podem ser assinados':'pode ser assinado')+'.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
		} else {
			$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivos foram inseridos, <b>'+total_arquivos_assinaveis+'</b> podem ser assinados.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
		}
		if (total_arquivos_assinaveis>0) {
			$('div#assinaturas-arquivos div.menssagem a').removeClass('btn-success').addClass('btn-warning').removeClass('disabled');
			$('div#novos-arquivos-detalhes input.btn-enviar-novos-arquivos').removeClass('disabled');
			$('div#assinaturas-arquivos div.alert').removeClass('alert-success').addClass('alert-warning');
			$('div#assinaturas-arquivos div.alert i').removeClass('glyphicon-ok').addClass('glyphicon-exclamation-sign');
		}
	}
}
function total_arquivo(id_tipo_arquivo_grupo_produto,no_arquivo,id_flex) {
	var total_arquivo = 0;
	switch (id_tipo_arquivo_grupo_produto) {
		case 1:
			obj_arquivos = $('form[name=form-nova-alienacao] div#arquivos');
			break;
		case 9:
			obj_arquivos = $('form[name=form-nova-penhora] div#arquivos-gerais');
			break;
		case 10:
			obj_arquivos = $('form[name=form-nova-penhora] div#arquivos-isencao');
			break;
		case 11:
			obj_arquivos = $('form[name=form-resposta-exigencia] div#arquivos-pendencias');
			break;
		case 12:
			obj_arquivos = $('form[name=form-resposta-averbacao] div#arquivo-imovel-'+id_flex);
			break;
		case 13:
			obj_arquivos = $('form[name=form-resposta-averbacao] div#arquivos-averbacao');
			break;
		case 15:
			obj_arquivos = $('div#arquivos-gerais');
			break;
		case 16:
			obj_arquivos = $('form[name=form-novo-oficio] div#arquivos-isencao');
			break;
		case 18:
			obj_arquivos = $('form[name=form-nova-pesquisa] div#arquivos-isencao');
			break;
		case 19:
			obj_arquivos = $('form[name=form-novo-resultado] div#arquivos-resultado');
			break;
		case 20:
			obj_arquivos = $('form[name=form-nova-certidao] div#arquivos-isencao');
			break;
		case 21:
			obj_arquivos = $('form[name=form-novo-resultado] div#arquivos-resultado');
			break;
		case 22:
			obj_arquivos = $('form[name=form-novo-andamento] div#arquivos-andamento');
			break;
        case 25:
            obj_arquivos = $('form[name=form-observacao] div#arquivos-observacao');
            break;
        case 26:
            obj_arquivos = $('form[name=form-alienacao-devolver-valor] div#arquivos-alienacao-devolver-valor');
            break;
	}
	if (no_arquivo!='') {
		var total_arquivo = obj_arquivos.find('input[value="'+no_arquivo+'"]').length;
	} else {
		var total_arquivo = obj_arquivos.find('div.arquivo').length;
	}
	return total_arquivo;
}
function remove_arquivo(linha,arquivo) {
	switch (arquivo.id_tipo_arquivo_grupo_produto) {
		case 1:
			obj_arquivos = $('form[name=form-nova-alienacao] div#arquivos');
			break;
		case 9:
			obj_arquivos = $('form[name=form-nova-penhora] div#arquivos-gerais');
			break;
		case 10:
			obj_arquivos = $('form[name=form-nova-penhora] div#arquivos-isencao');
			break;
		case 11:
			obj_arquivos = $('form[name=form-resposta-exigencia] div#arquivos-pendencias');
			if (obj_arquivos.data('local')=='resposta') {
				if (total_arquivo(11,'',0)<=0) {
					$('form[name=form-resposta-exigencia]').find('textarea[name=de_resposta]').val('');
				}
			}
			break;
		case 12:
			obj_arquivos = $('form[name=form-resposta-averbacao] div#arquivo-imovel-'+retorno.arquivo.id_flex);
			break;
		case 13:
			obj_arquivos = $('form[name=form-resposta-averbacao] div#arquivos-averbacao');
			if (total_arquivo(13,'',0)<=0) {
				$('form[name=form-resposta-averbacao] textarea[name=de_resposta]').val('');
			}
			break;
		case 15:
			obj_arquivos = $('div#arquivos-gerais');
			break;
		case 16:
			obj_arquivos = $('form[name=form-novo-oficio] div#arquivos-isencao');
			break;
		case 18:
			obj_arquivos = $('form[name=form-nova-pesquisa] div#arquivos-isencao');
			break;
		case 19:
			obj_arquivos = $('form[name=form-novo-resultado] div#arquivos-resultado');
			break;
		case 20:
			obj_arquivos = $('form[name=form-nova-certidao] div#arquivos-isencao');
			break;
		case 21:
			obj_arquivos = $('form[name=form-novo-resultado] div#arquivos-resultado');
			break;
		case 22:
			obj_arquivos = $('form[name=form-novo-andamento] div#arquivos-andamento');
			break;
        case 25:
            obj_arquivos = $('form[name=form-observacao] div#arquivos-observacao');
            break;
	}
	obj_arquivos.find('div#'+linha).remove();
	if (arquivo.in_ass_digital=='S') {
		total_arquivos_assinaveis--;
	}
	total_arquivos--;

    if (total_arquivos==1) {
        $('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivo foi inserido, <b>'+total_arquivos_assinaveis+'</b> '+(total_arquivos_assinaveis>1?'podem ser assinados':'pode ser assinado')+'.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
    } else {
        $('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivos foram inseridos, <b>'+total_arquivos_assinaveis+'</b> podem ser assinados.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
    }
	if (total_arquivos==0) {
		$('div#assinaturas-arquivos div.menssagem span').html('Nenhum arquivo foi inserido.');
		$('div#assinaturas-arquivos div.menssagem a').addClass('disabled');
        $('div#novos-arquivos-detalhes input.btn-enviar-novos-arquivos').addClass('disabled');
	}
}
function retorno_assinatura(sucessos,falhas) {
	$.each(sucessos,function(key,val) {
		total_arquivos_assinaveis--;
		$('div.arquivo#'+val.id).find('button.icone-assinatura').removeClass('btn-warning').addClass('btn-success').html('<i class="fa fa-lock"></i>');
	});
	$.each(falhas,function(key,val) {
		$('div.arquivo#'+val.id).find('button.icone-assinatura').removeClass('btn-warning').addClass('btn-danger');
	});
	if (falhas.length>0) {
		$('div#assinaturas-arquivos div.menssagem span').html('O processo de assinatura foi completado. Porém, '+falhas.length+' não foram assinados.');
		$('div#assinaturas-arquivos div.menssagem a').addClass('disabled');
	} else {
		$('div#assinaturas-arquivos div.menssagem span').html('O processo de assinatura foi completado.');
		$('div#assinaturas-arquivos div.menssagem a').removeClass('btn-warning').addClass('btn-success').addClass('disabled');
		$('div#assinaturas-arquivos div.alert').removeClass('alert-warning').addClass('alert-success');
		$('div#assinaturas-arquivos div.alert i').removeClass('glyphicon-exclamation-sign').addClass('glyphicon-ok');
	}
	$('button.adicionar-arquivo').addClass('disabled').prop('disabled',true);
}
function remover_arquivos_sessao(token) {
    $.ajax({
        type: "POST",
        url: url_base + 'arquivos/remover-todos',
        data: {'token': token},
        success: function (retorno) {
            switch (retorno.status) {
                case 'erro':
                    console.log("Erro!", retorno.msg, "error");
                    break;
                case 'sucesso':
                    console.log("Sucesso!", retorno.msg, "success").then(function () {
                    });
                    break;
            }
        },
        error: function (request, status, error) {
            swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ503", "error");
        }
    });
}