$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	form = $('form[name=form-dados-bancarios]');

    $('input[name=nu_agencia]').keyup(function() {
    	$(this).val(this.value.replace(/\D/g, ''));
    });

    $('input[name=nu_conta]').keyup(function() {
        $(this).val(this.value.replace(/\D/g, ''));
    });


    form.on('change','select[name=id_banco_codigo]',function(e) {
        var id_banco = $(this).val();
        form.find('select[name=id_banco]').val(id_banco);
    });

    form.on('change','select[name=id_banco]',function(e) {
        var id_banco = $(this).val();
        form.find('select[name=id_banco_codigo]').val(id_banco);
    });

	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('select[name=id_banco]').val()==0) {
            erros.push('O campo Banco é obrigatório.');
        }
        if (form.find('input[name=nu_agencia]').val()=='') {
            erros.push('O campo Agência é obrigatório.');
        }
        if (form.find('input[name=nu_conta]').val()=='') {
            erros.push('O campo número da conta é obrigatório.');
        }
        if (form.find('input[name=nu_dv_conta]').val()=='') {
            erros.push('O campo dígito da conta é obrigatório.');
        }
        if (form.find('select[name=tipo_conta]').val()==0) {
            erros.push('O campo tipo de conta é obrigatório.');
        }
        switch (form.find('select[name=tp_pessoa]').val()){
			case 'F':
                if (form.find('div.bp-pessoa-fisica').find('input[name=nu_cpf_cnpj_conta]').val()=='') {
                    erros.push('O campo CPF é obrigatório.');
                } else {
                    if(!validarCPF(form.find('div.bp-pessoa-fisica').find('input[name=nu_cpf_cnpj_conta]').val())){
                        erros.push('CPF inválido.');
                    }
                }
				break;
			case 'J':
                if (form.find('div.bp-pessoa-juridica').find('input[name=nu_cpf_cnpj_conta]').val()=='') {
                    erros.push('O campo CNPJ é obrigatório.');
                } else {
                    if (!validarCNPJ(form.find('div.bp-pessoa-juridica').find('input[name=nu_cpf_cnpj_conta]').val())) {
                        erros.push('CNPJ inválido.');
                    }
                }
                break;
		}

		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
			return false;
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'dados-bancarios/salvar',
				data: form.serialize(),
				beforeSend: function() {
					$('div.configuracoes .content div.carregando').show();
				},
				success: function(retorno) {
					$('div.configuracoes .content div.carregando').hide();
					
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						swal("Sucesso!", "Dados bancários salvos com sucesso.", "success").then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#tipo-pessoa').on('change','select[name=tp_pessoa]',function(e) {
		var form = $(this).closest('form');

		switch ($(this).val()) {
			case '0':
				form.find('div.bp-pessoa-fisica, div.bp-pessoa-juridica').slideUp('fast');
				break;
			case 'F':
				form.find('div.bp-pessoa-juridica').slideUp('fast',function() {
					form.find('div.bp-pessoa-juridica').find('input').val('');
					form.find('div.bp-pessoa-juridica').find('select').val(0);
					form.find('div.bp-pessoa-juridica').find('input, select').prop('disabled',true);
				});
				form.find('div.bp-pessoa-fisica').slideDown('fast',function() {
					form.find('div.bp-pessoa-fisica').find('input').val('');
					form.find('div.bp-pessoa-fisica').find('select').val(0);
					form.find('div.bp-pessoa-fisica').find('input, select').prop('disabled',false);
				});
				break;
			case 'J':
                document.getElementById("nu_cpf_cnpj_conta").addEventListener("change", validarCnpjAlterado);
                validarCnpjAlterado();
				form.find('div.bp-pessoa-fisica').slideUp('fast',function() {
					form.find('div.bp-pessoa-fisica').find('input').val('');
					form.find('div.bp-pessoa-fisica').find('select').val(0);
					form.find('div.bp-pessoa-fisica').find('input, select').prop('disabled',true);
				});
				form.find('div.bp-pessoa-juridica').slideDown('fast',function() {
					form.find('div.bp-pessoa-juridica').find('input').val('');
					form.find('div.bp-pessoa-juridica').find('select').val(0);
					form.find('div.bp-pessoa-juridica').find('input, select').prop('disabled',false);
				});
				break;
		}
		form.find('div.erros').slideUp();
	});

});
function validarCnpjAlterado(){
    var cnpj = document.getElementById("nu_cpf_cnpj_conta").value;
    if (cnpj.length < 14){
        form.find('input[name=nu_cpf_cnpj_conta]').val('');
    } else{
        form.find('input[name=nu_cpf_cnpj_conta]').val(cnpj);
    }
}