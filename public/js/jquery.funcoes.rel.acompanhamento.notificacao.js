$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var form = $('form[name=form-relatorio-movimentacao-notificacao]');

	form.on('reset',function(e) {
		e.preventDefault();
		window.location.reload();
	});


	$("input[name=relatorioPesquisar]").click(function(){
		if ( validaFormulario() )
		{
            $('form[name=form-relatorio-movimentacao-notificacao]').submit();
		}
	})

    form.on('change','select[name=id_cidade]',function(e) {
        var id_cidade = $(this).val();
        var form = $('form[name=form-relatorio-movimentacao-notificacao]');
        carrega_serventias(id_cidade,form,'select[name=id_serventia]');

    });

});


function validaFormulario() {
	var erros = new Array();
	if ($('select[name=cidade]').val() == 0)
	{
		erros.push('O campo cidade deve ser informado.');
	}
	if ($('select[name=serventia]').val() == 0) {
		erros.push('O campo serventia deve ser informado');
	}

	if (erros.length>0) {
		$('#menssagem').html(erros.join('<br />'));
		$('.erros').slideDown();
		return false;
	}else{
		$('.erros').slideUp();
		return true;
	}
}


function carrega_serventias(id_cidade,form,element) {
    $.ajax({
        type: "POST",
        url: url_base+'servicos/listarServentias',
        data: 'id_cidade='+id_cidade,
        beforeSend: function() {
            form.find(element).prop('disabled',true).html('<option value="0">Carregando...</option>');
        },
        success: function(serventias) {
            form.find(element).html('<option value="0">Selecione</option>');
            if (serventias.length>0) {
                $.each(serventias,function(key,serventia) {
                    form.find(element).append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                });
                form.find(element).prop('disabled',false);
            } else {
                form.find(element).prop('disabled',true);
            }
        },
        error: function (request, status, error) {
            //console.log(request.responseText);
            swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                location.reload();
            });
        }
    });
}
