$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    form = $('form[name=form-dados-pessoais]');

	form.on('change','select[name=id_estado]',function(e) {
		id_estado = $(this).val();
		
		$.ajax({
			type: "POST",
			url: '../listarCidades',
			data: 'id_estado='+id_estado,
			beforeSend: function() {
				form.find('select[name=id_cidade]').prop('disabled',true).html('<option value="0">Carregando...</option>');
			},
			success: function(cidades) {
				form.find('select[name=id_cidade]').html('<option value="0">Selecione uma cidade</option>');
				if (cidades.length>0) {
					$.each(cidades,function(key,cidade) {
						form.find('select[name=id_cidade]').append('<option value="'+cidade.id_cidade+'">'+cidade.no_cidade+'</option>');
					});
					form.find('select[name=id_cidade]').prop('disabled',false);
				} else {
					form.find('select[name=id_cidade]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});

	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('input[name=tp_pessoa]').val()=='F') {
			if (form.find('div.pessoa-fisica input[name=no_pessoa]').val()=='') {
				erros.push('O campo nome é obrigatório.');
			}
			if (form.find('div.pessoa-fisica select[name=tp_sexo]').val()=='0') {
				erros.push('O campo gênero é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=dt_nascimento]').val()=='') {
				erros.push('O campo data de nascimento é obrigatório.');
			}
			if (form.find('div.pessoa-fisica select[name=id_tipo_telefone]').val()=='0') {
				erros.push('O campo tipo de telefone é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=nu_ddd]').val()=='') {
				erros.push('O campo DDD é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=nu_telefone]').val()=='') {
				erros.push('O campo telefone é obrigatório.');
			}
		} else if (form.find('input[name=tp_pessoa]').val()=='J') {
			if (form.find('div.pessoa-juridica input[name=no_pessoa]').val()=='') {
				erros.push('O campo razão social é obrigatório.');
			}
			if (form.find('div.pessoa-juridica input[name=no_fantasia]').val()=='') {
				erros.push('O campo nome fantasia é obrigatório.');
			}
		}
		if (form.find('select[name=id_estado]').val()=='0') {
			erros.push('O campo estado é obrigatório.');
		}
		if (form.find('select[name=id_cidade]').val()=='0') {
			erros.push('O campo cidade é obrigatório.');
		}
		if (form.find('input[name=nu_cep]').val()=='') {
			erros.push('O campo CEP é obrigatório.');
		}
		if (form.find('input[name=no_endereco]').val()=='') {
			erros.push('O campo endereço é obrigatório.');
		}

		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
			return false;
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'dados-pessoais/salvar',
				data: form.serialize(),
				beforeSend: function() {
					$('div.configuracoes .content div.carregando').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div.configuracoes .content div.carregando').hide();
					
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						swal("Sucesso!", "Dados pessoais salvos com sucesso.", "success").then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});