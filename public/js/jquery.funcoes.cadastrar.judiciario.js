$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var passo = 1;
	var form = $('form[name=form-cadastrar');
	
	form.on('click','input.continue',function(e) {
		if (validarCampos(passo,form)) {
			if (passo<5) {
				$('div#register p.step-'+passo).fadeOut('fast');
				form.find('div.step-'+passo).fadeOut('fast',function(e) {
					passo=passo+1;
					if (passo>1) {
						form.find('input.back').show();
					}
					form.find('div.step-'+passo).fadeIn('fast');
					$('div#register p.step-'+passo).fadeIn('fast');
					$('div#register h3 span').html(passo);
				});
			} else {
				form.submit();
			}
		}
	});
	
	form.on('click','input.back',function(e) {
		if (passo>1) {
			$('div#register p.step-'+passo).fadeOut('fast');
			form.find('div.step-'+passo).fadeOut('fast',function(e) {
				passo=passo-1;
				if (passo==1) {
					form.find('input.back').hide();
				}
				form.find('div.step-'+passo).fadeIn('fast');
				$('div#register p.step-'+passo).fadeIn('fast');
				$('div#register h3 span').html(passo);
			});
			form.find('div.erros').slideUp();
		}
	});
	
	form.on('change','select[name=tp_pessoa]',function(e) {
		switch ($(this).val()) {
			case '0':
				form.find('div.pessoa-fisica, div.pessoa-juridica').slideUp('fast');
				break;
			case 'F':
				form.find('div.pessoa-juridica').slideUp('fast',function() {
					form.find('div.pessoa-juridica').find('input').val('');
					form.find('div.pessoa-juridica').find('select').val(0);
					form.find('div.pessoa-juridica').find('input, select').prop('disabled',true);
				});
				form.find('div.pessoa-fisica').slideDown('fast',function() {
					form.find('div.pessoa-fisica').find('input, select').prop('disabled',false);
				});
				break;
			case 'J':
				form.find('div.pessoa-fisica').slideUp('fast',function() {
					form.find('div.pessoa-fisica').find('input').val('');
					form.find('div.pessoa-fisica').find('select').val(0);
					form.find('div.pessoa-fisica').find('input, select').prop('disabled',true);
				});
				form.find('div.pessoa-juridica').slideDown('fast',function() {
					form.find('div.pessoa-juridica').find('input, select').prop('disabled',false);
				});
				break;
		}
		form.find('div.erros').slideUp();
	});
	
	form.on('change','select[name=tp_documento]',function(e) {
		switch ($(this).val()) {
			case '1':
				form.find('div.dados-rg').slideDown('fast');
				break;
			default:
				form.find('div.dados-rg').slideUp('fast').find('input').val('');
				break;
		}
	});
	
	form.on('change','select[name=id_estado]',function(e) {
		id_estado = $(this).val();
		
		form.find('select[name=id_cidade]').html('<option value="0">Selecione uma cidade</option>');
		$.ajax({
			type: "POST",
			url: '../listarCidades',
			data: 'id_estado='+id_estado,
			success: function(cidades) {
				if (cidades.length>0) {
					$.each(cidades,function(key,cidade) {
						form.find('select[name=id_cidade]').append('<option value="'+cidade.id_cidade+'">'+cidade.no_cidade+'</option>');
					});
					form.find('select[name=id_cidade]').prop('disabled',false);
				}
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	
	form.on('change','select[name=id_estado_nascimento]',function(e) {
		id_estado = $(this).val();
		
		form.find('select[name=id_cidade_nascimento]').html('<option value="0">Selecione a naturalidade</option>');
		$.ajax({
			type: "POST",
			url: '../listarCidades',
			data: 'id_estado='+id_estado,
			success: function(cidades) {
				if (cidades.length>0) {
					$.each(cidades,function(key,cidade) {
						form.find('select[name=id_cidade_nascimento]').append('<option value="'+cidade.id_cidade+'">'+cidade.no_cidade+'</option>');
					});
					form.find('select[name=id_cidade_nascimento]').prop('disabled',false);
				}
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
});
function totalUsuarios(email_usuario) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: '../totalUsuarios',
		async: false,
		data: 'email_usuario='+email_usuario,
		success: function(total) {
			res = parseInt(total);
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
	return res;
}
function totalPessoas(nu_cpf_cnpj) {
	var res = 0;
	$.ajax({
		type: "POST",
		url: '../totalPessoas',
		async: false,
		data: 'nu_cpf_cnpj='+nu_cpf_cnpj,
		success: function(total) {
			res = parseInt(total);
		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
	return res;
}
function validarCampos(passo,form) {
	var erros = new Array();
	var emailR = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	switch (passo) {
		case 1:
			if (form.find('input[name=email_usuario]').val()=='') {
				erros.push('O campo e-mail é obrigatório.');
			} else if (emailR.test(form.find('input[name=email_usuario]').val())==false) {
				erros.push('O e-mail digitado é inválido.');
			} else if (totalUsuarios(form.find('input[name=email_usuario]').val())>0) {
				erros.push('O e-mail digitado já está em uso.');
			}
			if (form.find('input[name=senha_usuario]').val()=='') {
				erros.push('O campo senha é obrigatório.');
			} else if (form.find('input[name=senha_usuario]').val().length<6) {
				erros.push('A senha deve conter no mínimo 6 caracteres.');
			}
			if (form.find('input[name=senha_usuario2]').val()=='') {
				erros.push('O campo confirme a senha é obrigatório.');
			} else if (form.find('input[name=senha_usuario2]').val()!=form.find('input[name=senha_usuario]').val()) {
				erros.push('A confirmação deve ser identica a senha.');
			}

                        if ((grecaptcha.getResponse() == 'undefined') || 
                                (grecaptcha.getResponse() == '') || 
                                (grecaptcha.getResponse().length == 0)){
                            erros.push('reCAPTCHA não validado. Por favor selecione a opção.');
                        }

			break;
		case 2:
			if (form.find('div.pessoa-fisica input[name=no_pessoa]').val()=='') {
				erros.push('O campo nome é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=nu_cpf_cnpj]').val()=='') {
				erros.push('O campo CPF é obrigatório.');
			} else if (!validarCPF(form.find('div.pessoa-fisica input[name=nu_cpf_cnpj]').val())) {
				erros.push('O CPF digitado é inválido');
			} else if (totalPessoas(form.find('div.pessoa-fisica input[name=nu_cpf_cnpj]').val())>0) {
				erros.push('O CPF digitado já está em uso.');
			}
			if (form.find('div.pessoa-fisica select[name=tp_sexo]').val()=='0') {
				erros.push('O campo gênero é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=dt_nascimento]').val()=='') {
				erros.push('O campo data de nascimento é obrigatório.');
			}
			if (form.find('div.pessoa-fisica select[name=id_tipo_telefone]').val()=='0') {
				erros.push('O campo tipo de telefone é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=nu_ddd]').val()=='') {
				erros.push('O campo DDD é obrigatório.');
			}
			if (form.find('div.pessoa-fisica input[name=nu_telefone]').val()=='') {
				erros.push('O campo telefone é obrigatório.');
			}
			break;
		case 3:
			if (form.find('div.pessoa-juridica input[name=no_vara]').val()=='') {
				erros.push('O campo nome da vara é obrigatório.');
			}
			if (form.find('div.pessoa-juridica select[name=id_comarca]').val()=='0') {
				erros.push('O campo comarca é obrigatório.');
			}
			if (form.find('div.pessoa-juridica select[name=id_vara_tipo]').val()=='0') {
				erros.push('O campo tipo da vara é obrigatório.');
			}
			break;
		case 4:
			if (form.find('select[name=id_estado]').val()=='0') {
				erros.push('O campo estado é obrigatório.');
			}
			if (form.find('select[name=id_cidade]').val()=='0') {
				erros.push('O campo cidade é obrigatório.');
			}
			if (form.find('input[name=nu_cep]').val()=='') {
				erros.push('O campo CEP é obrigatório.');
			}
			if (form.find('input[name=no_endereco]').val()=='') {
				erros.push('O campo endereço é obrigatório.');
			}
			break;
		case 5:
			if (!form.find('input[name=termos]').prop('checked')) {
				erros.push('Você deve ler os termos e aceita-los.');
			}
			break;
	}
	if (erros.length>0) {
		form.find('div.erros div').html(erros.join('<br />'));
		if (!form.find('div.erros').is(':visible')) {
			form.find('div.erros').slideDown();
		}
		return false;
	} else {
		form.find('div.erros').slideUp();
		return true;
	}
}

var widgetCadastroCartorio;
var onloadCallback = function() {
    widgetCadastroCartorio = grecaptcha.render('recaptcha_cadastro_cartorio', {
        'sitekey' : '6Lc5C14UAAAAANNfrhJe_MiMEUD_jKNemCcD9RUc',
        'theme'   : 'light'
    });
};