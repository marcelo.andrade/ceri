total_enderecos = 0;
$(document).ready(function() {

	////////////////////////////////////////////////////////////
	// Eventos do botão "Inserir endereço"                    //
	////////////////////////////////////////////////////////////

	$('div#novo-endereco-andamento').on('show.bs.modal', function (event) {
		var id_cidade = $(event.relatedTarget).data('idcidade');

		var andamento_token = $(event.relatedTarget).data('andamentotoken');

		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/endereco/novo',
			data: 'alienacao_token='+andamento_token,
			beforeSend: function() {
				$('div#novo-endereco-andamento .modal-body div.carregando').show();
				$('div#novo-endereco-andamento .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#novo-endereco-andamento .modal-body div.form form').html(retorno);
				$('div#novo-endereco-andamento .modal-body div.carregando').hide();
				$('div#novo-endereco-andamento .modal-body div.form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#novo-endereco-andamento').on('hidden.bs.modal', function (event) {
		$('div#novo-endereco-andamento .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-endereco-andamento .modal-body div.form form').html('');
	});
	$('div#novo-endereco-andamento').on('click','button.inserir-endereco',function(e) {
		var erros = new Array();
		var form = $('div#novo-endereco-andamento').find('form[name=form-novo-endereco]');
		
		var no_endereco = form.find('textarea[name=no_endereco]').val();

		/*var nu_numero = form.find('input[name=nu_numero]').val();
		var no_complemento = form.find('input[name=no_complemento]').val();
		var no_bairro = form.find('input[name=no_bairro]').val();
		var id_cidade = form.find('select[name=id_cidade]').val();
		var nome_cidade = form.find('select[name=id_cidade] option:selected').html();
		var no_bairro = form.find('input[name=no_bairro]').val();
		var nu_cep = form.find('input[name=nu_cep]').val();*/

		if (no_endereco=='') {
			erros.push('O campo endereço é obrigatório');
		}
		/*if (id_cidade=='0') {
			erros.push('O campo cidade é obrigatório');
		}*/
		if ($('table#novo-enderecos input.a_no_endereco[value="'+no_endereco+'"]').length>0) {
			erros.push('Este endereço já foi inserido');
		}

		if (erros.length>0) {
			form.find('div.erros-endereco div').html(erros.join('<br />'));
			if (!form.find('div.erros-endereco').is(':visible')) {
				form.find('div.erros-endereco').slideDown();
			}
		} else {
			var total_linhas = $("table#novo-enderecos tbody tr").length;
			var nova_linha = $('<tr id="'+(total_linhas+1)+'">');
			var colunas = "";
			colunas += '<td class="text-uppercase">';
			colunas += '<input type="hidden" name="a_no_endereco[]" class="a_no_endereco" id="a_no_endereco_'+total_linhas+'" value="'+no_endereco+'" />';

			/*colunas += '<input type="hidden" name="a_nu_numero[]" class="a_nu_numero" id="a_nu_numero_'+total_linhas+'" value="'+nu_numero+'" />';
			colunas += '<input type="hidden" name="a_no_complemento[]" class="a_no_complemento" id="a_no_complemento_'+total_linhas+'" value="'+no_complemento+'" />';
			colunas += '<input type="hidden" name="a_no_bairro[]" class="a_no_bairro" id="a_no_bairro_'+total_linhas+'" value="'+no_bairro+'" />';
			colunas += '<input type="hidden" name="a_id_cidade[]" class="a_id_cidade" id="a_id_cidade_'+total_linhas+'" value="'+id_cidade+'" />';
			colunas += '<input type="hidden" name="a_nu_cep[]" class="a_nu_cep" id="a_nu_cep_'+total_linhas+'" value="'+nu_cep+'" />';
			colunas += no_endereco+(nu_numero?' Nº '+nu_numero:'')+(no_complemento?', '+no_complemento:'')+(no_bairro?' - '+no_bairro:'')+'. '+nome_cidade+(nu_cep?' - '+nu_cep:'');*/

			colunas += no_endereco;
			colunas += '</td>';
			colunas += '<td>';
				colunas += '<a href="#" class="remover-endereco btn btn-black btn-sm" data-linha="'+(total_linhas+1)+'">Remover</a>';
			colunas += '</td>';

			nova_linha.append(colunas);
			$("table#novo-enderecos tbody").append(nova_linha);
			$('div#novo-endereco-andamento button.cancelar-endereco').trigger('click');
		}
	});
	$('div#novo-endereco-andamento').on('click','table#novo-enderecos a.remover-endereco',function() {
		var linha = $(this).data('linha');
		$('table#novo-enderecos tr#'+linha).remove();
	});
	$('div#novo-endereco-andamento').on('click','button.cancelar-endereco',function(e) {
		var form = $('div#novo-endereco-andamento').find('form[name=form-novo-endereco]');
		form.find('div.erros-endereco').slideUp();
		form.find('input[name=no_endereco]').val('');
		form.find('input[name=nu_numero]').val('');
		form.find('input[name=no_complemento]').val('');
		form.find('input[name=no_bairro]').val('');
		form.find('select[name=id_cidade]').val(0);
		form.find('input[name=nu_cep]').val('');
	});
	$('div#novo-endereco-andamento').on('click','button.inserir-enderecos',function(e) {
		var erros = new Array();
		var form = $('div#novo-endereco-andamento').find('form[name=form-novo-endereco]');
		
		var andamento_token = form.find('input[name=alienacao_token]').val();
		
		if ($('table#novo-enderecos input.a_no_endereco').length<=0) {
			erros.push('Ao menos um endereço deve ser inserido');
		}

		if (erros.length>0) {
			form.find('div.erros-endereco div').html(erros.join('<br />'));
			if (!form.find('div.erros-endereco').is(':visible')) {
				form.find('div.erros-endereco').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/endereco/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#novo-endereco-andamento .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							enderecos = retorno.enderecos;
							swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
								if (enderecos.length>0) {
									$.each(enderecos,function(key,endereco) {
										linha = total_enderecos;
										var nova_linha = $('<tr id="'+linha+'">');
										var colunas = "";
										colunas += '<td class="text-uppercase">';
											colunas += endereco.no_endereco;/*+(endereco.nu_numero?' Nº '+endereco.nu_numero:'')+(endereco.no_complemento?', '+endereco.no_complemento:'')+(endereco.no_bairro?' - '+endereco.no_bairro:'')+'. '+endereco.nome_cidade+(endereco.nu_cep?' - '+endereco.nu_cep:'');*/
										colunas += '</td>';
										colunas += '<td>';
											colunas += '<a href="#" class="remover-endereco btn btn-black btn-sm" data-linha="'+linha+'" data-andamentotoken="'+andamento_token+'">Remover</a>';
										colunas += '</td>';

										nova_linha.append(colunas);
										$("table#enderecos-andamento tbody").append(nova_linha);
										total_enderecos++;
									});
								}

								$('div#novo-endereco-andamento').modal('hide');
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
	$('div#nova-resposta, div#detalhes-alienacao').on('click','table#enderecos-andamento a.remover-endereco',function(e) {
		e.preventDefault();
		var linha = $(this).data('linha');

		var andamento_token = $(this).data('andamentotoken');
		
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/endereco/remover',
			data: 'alienacao_token='+andamento_token+'&linha='+linha,
			beforeSend: function() {
				$('div#nova-resposta').find('.modal-body div.carregando').addClass('flutuante').show();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#nova-resposta').find('.modal-body div.carregando').removeClass('flutuante').hide();
				$('table#enderecos-andamento').find('tr#'+linha).remove();
				total_enderecos--;
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
});