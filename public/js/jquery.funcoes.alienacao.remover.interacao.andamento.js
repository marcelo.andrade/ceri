$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('div#detalhes-andamento').on('click', 'button.excluir-interacao', function (e) {
         var id_andamento_alienacao = $(this).data('idandamentoalienacao');
         var protocolo_pedido = $(this).data('protocolo');

        swal({
            title: 'Tem certeza?',
            type: 'warning',
            html: 'Tem certeza que deseja remover o andamento de protocolo nº ' + protocolo_pedido,
            showCancelButton: true,
            cancelButtonText: 'Não',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim'
        }).then(function (retorno) {
            if (retorno==true) {
                $('div#remover-interacao-notificacao').modal('show');
                $('div#remover-interacao-notificacao').find('.modal-header h4.modal-title span').html(protocolo_pedido);
                remover_interacao_andamento(id_andamento_alienacao);
            }
        });
    });

    $('div#remover-interacao-notificacao').on('click', 'button.salvar-exclusao-interacao', function (e) {
        var form = $('form[name=form-excluir-interacao-andamento]');
        var erros = new Array();

        if (form.find('textarea[name=justificativa]').val().trim() == '') {
            erros.push('O campo Justificativa é obrigatório');
        }

        if (erros.length > 0) {
            form.find('div.erros-andamento div').html(erros.join('<br />'));
            if (!form.find('div.erros-andamento').is(':visible')) {
                form.find('div.erros-andamento').slideDown();
            }
        } else {
            form.find('div.erros').slideUp();
            $.ajax({
                type: "POST",
                url:url_base + 'servicos/alienacao/remover-interacao-andamento',
                data: form.serialize(),
                beforeSend: function() {
                    form.find('div.carregando').show().addClass('flutuante').show();
                },
                success: function(retorno) {
                    form.find('div.carregando').show().addClass('flutuante').hide();
                    switch (retorno.status) {
                        case 'erro':
                            var alerta = swal("Erro!",retorno.msg,"error");
                            break;
                        case 'sucesso':
                            var alerta = swal("Sucesso!",retorno.msg,"success");
                            break;
                        case 'alerta':
                            var alerta = swal("Ops!",retorno.msg,"warning");
                            break;
                    }
                    if (retorno.recarrega=='true') {
                        alerta.then(function(){
                            location.reload();
                        });
                    }
                },
                error: function (request, status, error) {
                    form.find('div.carregando').show().addClass('flutuante').hide();
                    console.log(request.responseText);
                }
            });
        }
    });

    $('div#detalhes-andamento-excluido').on('show.bs.modal', function (event) {
        var id_andamento_alienacao_excluido = $(event.relatedTarget).data('idandamento');
        var no_fase = $(event.relatedTarget).data('nofase');
        var no_etapa = $(event.relatedTarget).data('noetapa');
        var no_acao = $(event.relatedTarget).data('noacao');

        $(this).find('.modal-header h4.modal-title span').html(no_fase+' <i class="glyphicon glyphicon-chevron-right size" style ="font-size: 14px;"></i> '+no_etapa+' <i class="glyphicon glyphicon-chevron-right" style ="font-size: 14px;"></i> '+no_acao);

        if (id_andamento_alienacao_excluido>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/alienacao/detalhes-andamento-excluido',
                data: {'id_andamento_alienacao_excluido':id_andamento_alienacao_excluido},
                beforeSend: function() {
                    $('div#detalhes-andamento-excluido .modal-body div.carregando').show();
                    $('div#detalhes-andamento-excluido .modal-body div.form').hide();
                },
                success: function(retorno) {
                    $('div#detalhes-andamento-excluido .modal-body div.form').html(retorno);
                    $('div#detalhes-andamento-excluido .modal-body div.carregando').hide();
                    $('div#detalhes-andamento-excluido .modal-body div.form').fadeIn('fast');
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
        }
    });
});

function remover_interacao_andamento(id_andamento_alienacao) {
    if (id_andamento_alienacao > 0) {
        $.ajax({
            type: "GET",
            url: url_base + 'servicos/alienacao/remover-interacao-andamento',
            data: {'id_andamento_alienacao':id_andamento_alienacao},
            beforeSend: function () {
                $('div#remover-interacao-notificacao .modal-body div.carregando').show();
                $('div#remover-interacao-notificacao .modal-body div.form').hide();
            },
            success: function (retorno) {
                $('div#remover-interacao-notificacao .modal-body div.form').html(retorno);
                $('div#remover-interacao-notificacao .modal-body div.carregando').hide();
                $('div#remover-interacao-notificacao .modal-body div.form').fadeIn('fast');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                    location.reload();
                });
            }
        });
    }
}
