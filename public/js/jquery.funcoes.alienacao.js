var total_arquivos = 0;
var total_arquivos_assinaveis = 0;
var index_arquivos = 0;

var total_devedores = 0;
var total_enderecos = 0;
var total_parcelas = 0;

var total_pendentes = 0;
var total_encaminhadas = 0;
var total_orcamentos = 0;
var total_decurso = 0;
var total_finalizadas = 0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#nova-alienacao').on('show.bs.modal', function (event) {
		if ($(event.relatedTarget).data('idalienacao')) {
			var id_alienacao = $(event.relatedTarget).data('idalienacao');
			var protocolo_pedido = $(event.relatedTarget).data('protocolo');
			
			$(this).find('.modal-header h4.modal-title').html('Alterar notificação - '+protocolo_pedido);
			$(this).find('.modal-footer button.enviar-alienacao').html('Alterar notificação');
			$(this).find('.modal-footer button.enviar-alienacao').addClass('alterar-alienacao');
			$(this).find('.modal-footer button.enviar-alienacao').removeClass('incluir-alienacao');
		} else {
			var id_alienacao = 0;
			$(this).find('.modal-header h4.modal-title').html('Nova notificação');
			$(this).find('.modal-footer button.enviar-alienacao').html('Enviar notificação');
			$(this).find('.modal-footer button.enviar-alienacao').addClass('incluir-alienacao');
			$(this).find('.modal-footer button.enviar-alienacao').removeClass('alterar-alienacao');
		}
		
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/nova-alienacao',
			data: 'id_alienacao='+id_alienacao,
			beforeSend: function() {
				$('div#nova-alienacao .modal-body div.carregando').show();
				$('div#nova-alienacao .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#nova-alienacao .modal-body div.form form').html(retorno);
				$('div#nova-alienacao .modal-body div.carregando').hide();
				$('div#nova-alienacao .modal-body div.form form').fadeIn('fast');
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	$('div#nova-alienacao').on('hidden.bs.modal', function (event) {
		$('div#nova-alienacao .modal-body div.carregando').removeClass('flutuante').show();
		$('div#nova-alienacao .modal-body div.form form').html('');
	});

	$('div#nova-alienacao').on('change','select[name=id_cidade]',function(e) {
		var id_cidade = $(this).val();
		var form = $('form[name=form-nova-alienacao]');
		
		carrega_serventias(id_cidade,form,'select[name=id_serventia]');
		carrega_credores(id_cidade);
		carregar_agencia_credores(0,0,id_cidade);
	});	

	$('div#nova-alienacao').on('change','select[name=id_credor_alienacao]',function(e) {
		var id_credor_alienacao = $(this).val();
		carregar_agencia_credores(id_credor_alienacao,0,0);
	});

	$('div#nova-alienacao').on('change','select[name=id_agencia]',function(e) {
		var agencia = $(this).val().split("#");
		var id_agencia = agencia[0];
		$('select[name=id_credor_alienacao]').val(id_agencia);
		$("#id_credor_alienacao").val(id_agencia);
	});

	$('div#nova-alienacao').on('click','button.enviar-alienacao',function(e) {
		e.preventDefault();
		var form = $('form[name=form-nova-alienacao]');
		var erros = new Array();

		if (form.find('select[name=id_serventia]').is(':disabled') || form.find('select[name=id_serventia]').val()=='0') {
			erros.push('O campo cartório é obrigatório.');
		}
		if (form.find('select[name=id_credor_alienacao]').is(':disabled') || form.find('select[name=id_credor_alienacao]').val()=='0') {
			erros.push('O campo nome credor é obrigatório.');
		}
		if (form.find('select[name=id_agencia]').is(':disabled') || form.find('select[name=id_agencia]').val()=='0') {
			erros.push('O campo agência é obrigatório.');
		}
		if (form.find('input[name=matricula_imovel]').val()=='') {
			erros.push('O campo matrícula do imóvel é obrigatório.');
		}
		if (form.find('input[name=numero_contrato]').val()=='') {
			erros.push('O campo número do contrato é obrigatório.');
		}
		if (form.find('input[name=va_divida]').val()=='') {
			erros.push('O campo valor da dívida é obrigatório.');
		}
		if (form.find('input[name=va_leilao]').val()=='') {
			erros.push('O campo valor do leilão é obrigatório.');
		}
		if (form.find('input[name=id_legado]').val()=='') {
			erros.push('O campo código legado é obrigatório.');
		}
		if (form.find('input[name=va_periodo_01]').val()=='') {
			erros.push('O campo valor do período 01 é obrigatório.');
		}
		if (form.find('input[name=va_periodo_02]').val()=='') {
			erros.push('O campo valor do período 02 é obrigatório.');
		}
		if (form.find('input[name=va_periodo_03]').val()=='') {
			erros.push('O campo valor do período 03 é obrigatório.');
		}
		if (form.find('input[name=va_periodo_04]').val()=='') {
			erros.push('O campo valor do período 04 é obrigatório.');
		}
		
		if (form.find('table#devedores input.devedores_nu_cpf_cnpj').length<=0) {
			erros.push('Ao menos um devedor deve ser inserido.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/inserir-alienacao',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#nova-alienacao .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#nova-alienacao').on('click','button.alterar-alienacao',function(e) {
		e.preventDefault();
		var erros = new Array();
		var form = $('form[name=form-nova-alienacao]');

		if (form.find('select[name=id_credor_alienacao]').is(':disabled') || form.find('select[name=id_credor_alienacao]').val()=='0') {
			erros.push('O campo credor é obrigatório.');
		}
		if (form.find('input[name=matricula_imovel]').val()=='') {
			erros.push('O campo matrícula do imóvel é obrigatório.');
		}
		if (form.find('input[name=numero_contrato]').val()=='') {
			erros.push('O campo número do contrato é obrigatório.');
		}
		if (form.find('input[name=dt_contrato]').val()=='') {
			erros.push('O campo data do contrato é obrigatório.');
		}
		if (form.find('table#devedores input.devedores_nu_cpf_cnpj').length<=0) {
			erros.push('Ao menos um devedor deve ser inserido.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/alterar-alienacao',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#nova-alienacao .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#detalhes-alienacao').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/detalhes',
				data: 'id_alienacao='+id_alienacao,
				beforeSend: function() {
					$('div#detalhes-alienacao .modal-body div.carregando').show();
					$('div#detalhes-alienacao .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-alienacao .modal-body div.form').html(retorno);
					$('div#detalhes-alienacao .modal-body div.carregando').hide();
					$('div#detalhes-alienacao .modal-body div.form').fadeIn('fast');
					carrega_andamento(id_alienacao);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('form#form-filtro').on('change','select[name=cidade]',function(e) {
		var id_cidade = $(this).val();
		var form = $('form#form-filtro');

		carrega_serventias(id_cidade,form,'select[name=serventia]');
	});

    $('form#form-filtro').on('change','select.grupo',function(e) {
    	var grupo = $(this).val();

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/filtros-alertas',
            data: {'grupo': grupo},
            success: function (grupos) {
                $('form#form-filtro select.filtro').prop('disabled', false);
                if (!$.isEmptyObject(grupos)) {
                	HTML = '';
                    $.each(grupos, function (key, grupo) {
                    	HTML += '<optgroup label="'+grupo.grupo.titulo+'">';
	                    	$.each(grupo.alertas, function (key, alerta) {
	                    		switch (alerta.alcada) {
	                    			case 2:
	                    				alcada = 'cartorio';
	                    				break;
	                    			case 8:
	                    				alcada = 'cef';
	                    				break;
	                    			default:
	                    				alcada = '';
	                    				break;
	                    		}
	                        	HTML += '<option value="'+grupo.grupo.key+'_'+alerta.key+'" class="'+alcada+'">'+alerta.titulo+'</option>';
	                        });
                        HTML += '</optgroup>';
                    });
                    $('form#form-filtro select.filtro').html(HTML);
                } else {
                    $('form#form-filtro select.filtro').html('').prop('disabled', true);
                }
                $('form#form-filtro select.filtro').selectpicker('refresh');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error");
            }
        });
    });

	////////////////////////////////////////////////////////////
	// Eventos do formulário de inserir andamento             //
	////////////////////////////////////////////////////////////

	$('div#detalhes-alienacao').on('submit','form[name=form-novo-andamento]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();

		if (form.find('select[name=id_resultado_acao]')) {
			if (form.find('select[name=id_resultado_acao]').val()=='0') {
				erros.push('O campo resultado é obrigatório.');
			}
		}
		form.find('.obrigatorio').each(function(index, element) {
            if ($(this).hasClass('real')) {
				if ($(this).autoNumeric('get')=='' || $(this).autoNumeric('get')==0) {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			} else if ($(this).hasClass('arquivos')) {
				if ($(this).find('div.arquivo').length<=0) {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			} else {
				if ($(this).val()=='') {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			}
        });
		if (erros.length>0) {
			form.find('div.erros-andamento div').html(erros.join('<br />'));
			if (!form.find('div.erros-andamento').is(':visible')) {
				form.find('div.erros-andamento').slideDown();
			}
		} else {
			form.find('div.erros-andamento').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/inserir-andamento',
				data: form.serialize(),
				beforeSend: function() {
					$('div#detalhes-alienacao .modal-body>div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					$('div#detalhes-alienacao .modal-body>div.carregando').removeClass('flutuante').hide();
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							switch (retorno.andamento.tipo) {
								case 'resultado':
									$('div#detalhes-alienacao table#historico-pedido tr#'+retorno.andamento.andamento.id_andamento_alienacao+' td.resultado label').removeClass('label-warning').addClass('label-success').html(retorno.andamento.resultado.no_resultado);
									$('div#detalhes-alienacao .modal-body>div.carregando').removeClass('flutuante').hide();
									carrega_andamento(form.find('input[name=id_alienacao]').val());
									break;
								case 'andamento':
									console.log(retorno.andamento);
									var nova_linha = $('<tr id="'+retorno.andamento.andamento.id_andamento_alienacao+'">');
									var colunas = "";
									colunas += '<td>'+retorno.andamento.fase.no_fase+'</td>';
									colunas += '<td>'+retorno.andamento.etapa.no_etapa+'</td>';
									colunas += '<td>'+retorno.andamento.acao.no_acao+'</td>';
									colunas += '<td class="resultado">';
										if(retorno.andamento.resultado.automatico=='true') {
											colunas += '<label class="label label-success">'+retorno.andamento.resultado.no_resultado+'</label>';
										} else {
											colunas += '<label class="label label-warning">'+retorno.andamento.resultado.no_resultado+'</label>';
										}
									colunas += '</td>';
									colunas += '<td>'+retorno.andamento.dt_formatada+'</td>';
									colunas += '<td>';
										colunas += '<a href="#" class="detalhes-andamento btn btn-success btn-sm" data-toggle="modal" data-target="#detalhes-andamento" data-idandamento="'+retorno.andamento.andamento.id_andamento_alienacao+'" data-nofase="'+retorno.andamento.fase.no_fase+'" data-noetapa="'+retorno.andamento.etapa.no_etapa+'" data-noacao="'+retorno.andamento.acao.no_acao+'">Ver detalhes</a></li>';
									colunas += '</td>';
						
									nova_linha.append(colunas);
									$("table#historico-pedido tbody").prepend(nova_linha);
									$('div#nova-resposta .modal-body>div.carregando').removeClass('flutuante').hide();
									carrega_andamento(form.find('input[name=id_alienacao]').val());
									break;
							}
							//var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'mensagem':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

	$('div#detalhes-alienacao').on('change','select[name=id_resultado_acao]',function(e) {
		var form = $('form[name=form-novo-andamento]');
		var id_resultado_acao = $(this).val();
		var andamento_token = form.find('input[name=andamento_token]').val();
		var id_alienacao = form.find('input[name=id_alienacao]').val();

		var form = $('form[name=form-novo-andamento]');
		if (id_resultado_acao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/resultado-acao',
				data: {'id_resultado_acao':id_resultado_acao,'andamento_token':andamento_token,'id_alienacao':id_alienacao},
				beforeSend: function() {
					form.find('div#resultado-acao>div.carregando').show();
					form.find('div#resultado-acao>div.form').hide();
				},
				success: function(retorno) {
					console.log(retorno);
					form.find('div#resultado-acao>div.form').html(retorno);
					form.find('div#resultado-acao>div.carregando').slideUp('fast',function() {
						form.find('div#resultado-acao>div.form').slideDown('fast');
					});
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		} else {
			form.find('div#resultado-acao>div.carregando').hide();
			form.find('div#resultado-acao>div.form').slideUp('fast').html('');
		}
	});
	
	$('div#gerar-arquivo-previsualizar').on('show.bs.modal', function (event) {
		var form = $('form[name=form-novo-andamento]');
		var erros = new Array();

		var id_tipo_arquivo_grupo_produto = $(event.relatedTarget).data('idtipoarquivo');
		var limite = $(event.relatedTarget).data('limite');

		form.find('.obrigatorio').each(function(index, element) {
            if ($(this).hasClass('real')) {
				if ($(this).autoNumeric('get')=='' || $(this).autoNumeric('get')==0) {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			} else if (!$(this).hasClass('arquivos')) {
				if ($(this).val()=='') {
					erros.push('O campo '+$(this).attr('title')+' é obrigatório');
				}
			}
        });

        if (erros.length>0) {
			form.find('div.erros-andamento div').html(erros.join('<br />'));
			if (!form.find('div.erros-andamento').is(':visible')) {
				form.find('div.erros-andamento').slideDown();
				return false;
			}
		} else {
			form.find('div.erros-andamento').slideUp();

			var total_arquivos_gerados = form.find('div#arquivos-andamento div.arquivo.gerado').length;
			if ((total_arquivo(id_tipo_arquivo_grupo_produto,'',0)<parseInt(limite) || limite=='0')) {
				if (total_arquivos_gerados<1) {
					var id_alienacao = $(event.relatedTarget).data('idalienacao');
					var tipo = $(event.relatedTarget).data('tipo');
					var token = $(event.relatedTarget).data('token');

					var de_texto_curto_acao = $('form[name=form-novo-andamento]').find('input[name=de_texto_curto_acao]').val();
					var de_texto_longo_acao = $('form[name=form-novo-andamento]').find('input[name=de_texto_longo_acao]').val();
					var va_valor_acao = $('form[name=form-novo-andamento]').find('input[name=va_valor_acao]').val();
					var dt_acao = $('form[name=form-novo-andamento]').find('input[name=dt_acao]').val();
					
					switch(tipo) {
						case 'certidao-decurso':
							$(this).find('.modal-header h4.modal-title').html('Pré-visualização - Certidão de Decurso de Prazo');
							break;
						case 'edital-intimacao':
							$(this).find('.modal-header h4.modal-title').html('Pré-visualização - Edital de Intimação');
							break;
						case 'carta-intimacao':
							$(this).find('.modal-header h4.modal-title').html('Pré-visualização - Carta de Intimação');
							break;
					}

					if (id_alienacao>0) {
						$.ajax({
							type: "POST",
							url: url_base+'servicos/alienacao/gerar-arquivo/previsualizar',
							data: 'id_alienacao='+id_alienacao+'&tipo='+tipo+'&de_texto_curto_acao='+de_texto_curto_acao+'&de_texto_longo_acao='+de_texto_longo_acao+'&va_valor_acao='+va_valor_acao+'&dt_acao='+dt_acao,
							context: this,
							beforeSend: function() {
								status_modal($(this),true,true,false);
								$(this).find('.modal-body div.form').hide();
							},
							success: function(retorno) {
								switch (retorno.status) {
									case 'erro':
										var alerta = swal("Erro!",retorno.msg,"error");
										break;
									case 'sucesso':
										$(this).addClass('total-height');
										status_modal($(this),false,false,false);
										resize_modal($(this));
										$(this).find('.modal-body div.form').html(retorno.view).fadeIn('fast');
										$(this).find('.modal-footer button.salvar-certidao').data('idtipoarquivo',id_tipo_arquivo_grupo_produto);
										$(this).find('.modal-footer button.salvar-certidao').data('token',token);
										$(this).find('.modal-footer button.salvar-certidao').data('idalienacao',id_alienacao);
										$(this).find('.modal-footer button.salvar-certidao').data('tipo',tipo);
										break;
									case 'alerta':
										var alerta = swal("Ops!",retorno.msg,"warning");
										break;
								}
							},
							error: function (request, status, error) {
								swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error").then(function() {
									$(this).modal('hide');
								});
							}
						});
					}
				} else {
					swal("Ops!", "Você alcançou o limite de 1 arquivo gerado para esta seção.", "warning");
					return false;
				}
			} else {
				swal("Ops!", "Você alcançou o limite de "+limite+" arquivo(s) para esta seção.", "warning");
				return false;
			}
		}
	});

	$('div#gerar-arquivo-previsualizar').on('click','button.salvar-certidao',function(event) {
		var id_tipo_arquivo_grupo_produto = $(this).data('idtipoarquivo');
		var id_alienacao = $(this).data('idalienacao');
		var tipo = $(this).data('tipo');
		var token = $(this).data('token');

		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);
		
		if (id_alienacao>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/gerar-arquivo/salvar',
				data: {'id_tipo_arquivo_grupo_produto': id_tipo_arquivo_grupo_produto,'id_alienacao': id_alienacao,'tipo': tipo,'token': token,'index_arquivos':index_arquivos},
				success: function (retorno) {
					status_modal(obj_modal, false, false, false);
					switch (retorno.status) {
						case 'erro':
							swal("Erro!", retorno.msg, "error");
							break;
						case 'sucesso':
							botao_arquivo = '<div class="arquivo gerado btn-group" id="'+index_arquivos+'" data-inassdigital="'+retorno.arquivo.in_ass_digital+'">';
								botao_arquivo += '<button type="button" class="btn btn-primary">'+retorno.arquivo.no_arquivo+'</button>';
								botao_arquivo += '<input type="hidden" name="no_arquivo[]" value="'+retorno.arquivo.no_arquivo+'" />';
								if (retorno.arquivo.in_ass_digital=='S') {
									botao_arquivo += '<button type="button" class="icone-assinatura btn btn-warning"><i class="fa fa-unlock-alt"></i></button>';
								}
								botao_arquivo += '<button type="button" class="remover-arquivo btn btn-danger" data-linha="'+index_arquivos+'" data-token="'+retorno.token+'"><i class="fa fa-times"></i></button>';
							botao_arquivo += '</div> ';
							$('div#gerar-arquivo-previsualizar').modal('hide');
							total_arquivos++;
							if (retorno.arquivo.in_ass_digital=='S') {
								total_arquivos_assinaveis++;
							}
							index_arquivos++;

							$('form[name=form-novo-andamento] div#arquivos-andamento').prepend(botao_arquivo);

							if (total_arquivos_assinaveis>=0) {
								if (total_arquivos==1) {
									$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivo foi inserido, <b>'+total_arquivos_assinaveis+'</b> '+(total_arquivos_assinaveis>1?'podem ser assinados':'pode ser assinado')+'.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
								} else {
									$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivos foram inseridos, <b>'+total_arquivos_assinaveis+'</b> podem ser assinados.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
								}
								if (total_arquivos_assinaveis>0) {
									$('div#assinaturas-arquivos div.menssagem a').removeClass('btn-success').addClass('btn-warning').removeClass('disabled');
									$('div#assinaturas-arquivos div.alert').removeClass('alert-success').addClass('alert-warning');
									$('div#assinaturas-arquivos div.alert i').removeClass('glyphicon-ok').addClass('glyphicon-exclamation-sign');
								}
							}
							break;
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
				}
			});
		}
	});
	
	////////////////////////////////////////////////////////////
	// Ações									              //
	////////////////////////////////////////////////////////////
	
	$('table#pedidos').on('click','input#selecionar-todas',function(e) {
		if ($(this).is(':checked')) {
			$('table#pedidos input.alienacao:not(:checked)').trigger('click');
		} else {
			$('table#pedidos input.alienacao:checked').trigger('click');
		}
	});

	$('table#pedidos').on('click','input.alienacao',function(e) {
		var total = $('table#pedidos input.alienacao:checked').length;
		
		var total_pendentes = $('table#pedidos input.alienacao-pendente:checked').length;
		var total_encaminhadas = $('table#pedidos input.alienacao-encaminhada:checked').length;
		var total_orcamentos = $('table#pedidos input.alienacao-orcamento:checked').length;
		var total_decurso = $('table#pedidos input.alienacao-decurso:checked').length;
		var total_finalizadas = $('table#pedidos input.alienacao-finalizada:checked').length;

		if (total<=0) {
			$('div#alert span.total').html('0 notificações selecionadas');
			$('div#alert button').prop('disabled',true).addClass('disabled');

			$('div#alert ul.dropdown-menu a.gerar-relatorio-todas').parent('li').addClass('disabled');
			$('div#alert ul.dropdown-menu a.gerar-relatorio-todas span').html('');
		} else {
			$('div#alert span.total').html((total==1?'1 notificação selecionada':total+' notificações selecionadas'));
			$('div#alert button').prop('disabled',false).removeClass('disabled');

			$('div#alert ul.dropdown-menu a.gerar-relatorio-todas').parent('li').removeClass('disabled');
			$('div#alert ul.dropdown-menu a.gerar-relatorio-todas span').html('('+total+')');

			if (total_pendentes<=0) {
				$('div#alert ul.dropdown-menu a.encaminhar-todas-notificacoes').parent('li').addClass('disabled');
				$('div#alert ul.dropdown-menu a.encaminhar-todas-notificacoes span').html('');
			} else {
				$('div#alert ul.dropdown-menu a.encaminhar-todas-notificacoes').parent('li').removeClass('disabled');
				$('div#alert ul.dropdown-menu a.encaminhar-todas-notificacoes span').html('('+total_pendentes+')');
			}
			if (total_pendentes<=0 && total_encaminhadas<=0 && total_orcamentos<=0 && total_decurso<=0) {
				$('div#alert ul.dropdown-menu a.cancelar-todas').parent('li').addClass('disabled');
				$('div#alert ul.dropdown-menu a.cancelar-todas span').html('');
			} else {
				$('div#alert ul.dropdown-menu a.cancelar-todas').parent('li').removeClass('disabled');
				$('div#alert ul.dropdown-menu a.cancelar-todas span').html('('+(total_pendentes+total_encaminhadas+total_orcamentos+total_decurso)+')');
			}
			if (total_orcamentos<=0) {
				$('div#alert ul.dropdown-menu a.aprovar-todos-orcamentos').parent('li').addClass('disabled');
				$('div#alert ul.dropdown-menu a.aprovar-todos-orcamentos span').html('');
			} else {
				$('div#alert ul.dropdown-menu a.aprovar-todos-orcamentos').parent('li').removeClass('disabled');
				$('div#alert ul.dropdown-menu a.aprovar-todos-orcamentos span').html('('+total_orcamentos+')');
			}
			if (total_decurso<=0) {
				$('div#alert ul.dropdown-menu a.aprovar-todos-decursos-prazo').parent('li').addClass('disabled');
				$('div#alert ul.dropdown-menu a.aprovar-todos-decursos-prazo span').html('');
			} else {
				$('div#alert ul.dropdown-menu a.aprovar-todos-decursos-prazo').parent('li').removeClass('disabled');
				$('div#alert ul.dropdown-menu a.aprovar-todos-decursos-prazo span').html('('+total_decurso+')');
			}
		}
	});

	$('table#pedidos').on('click','a.cancelar-notificacao',function(e) {
		e.preventDefault();
		var id_alienacao = $(this).data('idalienacao');
		var protocolo_pedido = $(this).data('protocolo');

		var ids_alienacoes = [];
		ids_alienacoes.push(id_alienacao);

		if (ids_alienacoes.length>0) {
			swal({
				title: 'Tem certeza?',
				type: 'warning',
				html: 'Tem certeza que deseja cancelar o pedido nº '+protocolo_pedido+'. <br /><br />Obs.: Após cancelar, o pedido não poderá ser recuperado.',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: url_base+'servicos/alienacao/cancelar-alienacoes',
                        data: {'ids_alienacoes':ids_alienacoes},
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
										location.reload();
									});
									break;
							}
						},
						error: function (request, status, error) {
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

	$('table#pedidos, div#alert').on('click','a.aprovar-orcamento, a.aprovar-todos-orcamentos',function(e) {
		e.preventDefault();
		var ids_alienacoes = [];
		var protocolo_pedido = $(this).data('protocolo');

		if ($(this).data('idalienacao')) {
			ids_alienacoes.push($(this).data('idalienacao'));

			var html_texto = 'Tem certeza que deseja aprovar o orçamento da notificação pedido nº '+protocolo_pedido+'?';
		} else {
			$('table#pedidos input.alienacao-orcamento:checked').each(function(e) {
				ids_alienacoes.push($(this).val());
			});
			var html_texto = 'Tem certeza que deseja aprovar '+(ids_alienacoes.length>1?'os '+ids_alienacoes.length+' orçamentos selecionados':'o orçamento selecionado')+'?';
		}
		
		if (ids_alienacoes.length>0) {
			swal({
				title: 'Tem certeza?',
				type: 'warning',
				html: html_texto,
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function (retorno) {
				if (retorno == true) {
					$.ajax({
						type: "POST",
						url: url_base+'servicos/alienacao/aprovar-orcamentos',
						data: {'ids_alienacoes':ids_alienacoes},
						success: function (retorno) {
							switch (retorno.status) {
								case 'erro':
									swal("Erro!", retorno.msg, "error");
									break;
								case 'sucesso':
									swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
										location.reload();
									});
									break;
							}
						},
						error: function (request, status, error) {
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

	$('table#pedidos, div#alert').on('click','a.aprovar-decurso-prazo, a.aprovar-todos-decursos-prazo',function(e) {
		e.preventDefault();
		var ids_alienacoes = [];
		var protocolo_pedido = $(this).data('protocolo');

		if ($(this).data('idalienacao')) {
			ids_alienacoes.push($(this).data('idalienacao'));

			var html_texto = 'Tem certeza que deseja aprovar certidão de decurso de prazo da notificação pedido nº '+protocolo_pedido+'?'
		} else {
			$('table#pedidos input.alienacao-decurso:checked').each(function(e) {
				ids_alienacoes.push($(this).val());
			});
			var html_texto = 'Tem certeza que deseja aprovar o decurso de prazo das '+(ids_alienacoes.length>1?'os '+ids_alienacoes.length+' notificações selecionadas':'o decurso de prazo da notificação selecionada')+'?';
		}
		
		if (ids_alienacoes.length>0) {
			swal({
				title: 'Tem certeza?',
				type: 'warning',
				html: html_texto,
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function (retorno) {
				if (retorno == true) {
					$.ajax({
						type: "POST",
						url: url_base+'servicos/alienacao/aprovar-decursos-prazo',
						data: {'ids_alienacoes':ids_alienacoes},
						success: function (retorno) {
							switch (retorno.status) {
								case 'erro':
									swal("Erro!", retorno.msg, "error");
									break;
								case 'sucesso':
									swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
										location.reload();
									});
									break;
							}
						},
						error: function (request, status, error) {
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

	$('table#pedidos, div#alert').on('click','a.encaminhar-notificacao, a.encaminhar-todas-notificacoes',function(e) {
		e.preventDefault();
		var ids_alienacoes = [];
		var protocolo_pedido = $(this).data('protocolo');

		if ($(this).data('idalienacao')) {
			ids_alienacoes.push($(this).data('idalienacao'));

			var html_texto = 'Tem certeza que deseja encaminhar a notificação pedido nº '+protocolo_pedido+'?'
		} else {
			$('table#pedidos input.alienacao-pendente:checked').each(function(e) {
				ids_alienacoes.push($(this).val());
			});
			var html_texto = 'Tem certeza que deseja encaminhar '+(ids_alienacoes.length>1?'os '+ids_alienacoes.length+' notificações selecionadas':'a notificação selecionada')+'?';
		}
		
		if (ids_alienacoes.length>0) {
			swal({
				title: 'Tem certeza?',
				type: 'warning',
				html: html_texto,
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function (retorno) {
				if (retorno == true) {
					$.ajax({
						type: "POST",
						url: url_base+'servicos/alienacao/encaminhar-alienacoes',
						data: {'ids_alienacoes':ids_alienacoes},
						success: function (retorno) {
							switch (retorno.status) {
								case 'erro':
									swal("Erro!", retorno.msg, "error");
									break;
								case 'sucesso':
									swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
										location.reload();
									});
									break;
							}
						},
						error: function (request, status, error) {
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

	$('div#relatorio-notificacoes').on('show.bs.modal', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos input.alienacao:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/relatorio',
            data: {'ids_alienacoes':ids_alienacoes},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-notificacoes').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
        window.location.reload();
    });

    $('div#relatorio-notificacoes').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-relatorio]').submit();
        $('div#relatorio-notificacoes').modal('hide');
    });

	$('div#detalhes-custas').on('show.bs.modal', function (event) {
		var id_alienacao = $(event.relatedTarget).data('idalienacao');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
			
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/detalhes-custas',
			data: 'id_alienacao='+id_alienacao,
			context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				status_modal($(this), false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});

    $('div#visualizar-arquivo-caixa').on('show.bs.modal', function (event) {
        var id_alienacao = $(event.relatedTarget).data('idalienacao');
        var tipo = $(event.relatedTarget).data('tipo');

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/gerar-recibo-caixa',
            data: {'id_alienacao':id_alienacao, 'tipo':tipo},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, true);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($('div#visualizar-arquivo-caixa'));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

    $('table#pedidos, div#alert').on('click','a.atualizar-valor-edital',function(e) {
        e.preventDefault();
        var id_alienacao = $(this).data('idalienacao');
        var protocolo_pedido = $(this).data('protocolo');
        var id_resultado_acao = $(this).data('idresultadoacao');
        var in_inserir_resultado = $(this).data('inserirresultado');

        if (id_resultado_acao === 152) {
            var html_texto = 'Tem certeza que deseja atualizar o valor do edital da notificação pedido nº '+protocolo_pedido+'?'
        } else {
            var html_texto = 'Tem certeza que não deseja atualizar o valor do edital da notificação pedido nº '+protocolo_pedido+'?'
        }

        if (id_alienacao>0) {
            swal({
                title: 'Tem certeza?',
                type: 'warning',
                html: html_texto,
                showCancelButton: true,
                cancelButtonText: 'Não',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim'
            }).then(function (retorno) {
                if (retorno == true && id_resultado_acao === 153) {
                    $.ajax({
                        type: "POST",
                        url: url_base+'servicos/alienacao/inserir-andamento',
                        data: {'id_alienacao':id_alienacao,'id_resultado_acao':id_resultado_acao,'in_inserir_resultado':in_inserir_resultado},
                        success: function (retorno) {
                            switch (retorno.status) {
                                case 'erro':
                                    swal("Erro!", retorno.msg, "error");
                                    break;
                                case 'sucesso':
                                    swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
                                        location.reload();
                                    });
                                    break;
                            }
                        },
                        error: function (request, status, error) {
                            swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                                location.reload();
                            });
                        }
                    });
                } else {
					$('button[type="button"][data-target="#detalhes-alienacao"][data-idalienacao="'+id_alienacao+'"]').trigger('click');
					setTimeout(function() {
                        $('div#detalhes-alienacao form[name=form-novo-andamento]').find('select[name=id_resultado_acao]').val(id_resultado_acao).change();
					}, 2000);
                }
            });
        }
    });
});
function carrega_serventias(id_cidade,form,element) {
	$.ajax({
		type: "POST",
		url: url_base+'servicos/listarServentias',
		data: 'id_cidade='+id_cidade,
		beforeSend: function() {
			form.find(element).prop('disabled',true).html('<option value="0">Carregando...</option>');
		},
		success: function(serventias) {
			form.find(element).html('<option value="0">Selecione</option>');
			if (serventias.length>0) {
				$.each(serventias,function(key,serventia) {
					form.find(element).append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
				});
				form.find(element).prop('disabled',false);
			} else {
				form.find(element).prop('disabled',true);
			}
		},
		error: function (request, status, error) {
			//
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}
function carrega_credores(id_cidade,id_credor_alienacao) {
	var form = $('form[name=form-nova-alienacao]');
	if (!id_credor_alienacao) {
		id_credor_alienacao = 0;
	}
	$.ajax({
		type: "POST",
		url: url_base+'servicos/listar_credores',
		data: 'id_cidade='+id_cidade,
		beforeSend: function() {
			form.find('select[name=id_credor_alienacao]').prop('disabled',true).html('<option value="0">Carregando...</option>');
		},
		success: function(credores) {
			form.find('select[name=id_credor_alienacao]').html('<option value="0">Selecione</option>');
			if (credores.length>0) {
				$.each(credores,function(key,credor) {
					form.find('select[name=id_credor_alienacao]').append('<option value="'+credor.id_credor_alienacao+'" '+(credor.id_credor_alienacao==id_credor_alienacao?'selected="selected"':'')+'>'+credor.no_credor+'</option>');
				});
				form.find('select[name=id_credor_alienacao]').prop('disabled',false);
				form.find('select[name=id_agencia]').prop('disabled',false);
			} else {
				form.find('select[name=id_credor_alienacao]').prop('disabled',true).html('<option value="0">Nenhum credor disponível</option>');
				form.find('select[name=id_agencia]').prop('disabled',false);
			}
		},
		error: function (request, status, error) {
			
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}

function carregar_agencia_credores(id_credor_alienacao, id_agencia, id_cidade) {
	var form = $('form[name=form-nova-alienacao]');
	if (!id_credor_alienacao) {
		id_credor_alienacao = 0;
	}
	if (!id_cidade) {
		id_cidade = 0;
	}
	$.ajax({
		type: "POST",
		url: url_base+'servicos/listar_agencia_credores',
		data: {'id_credor_alienacao':id_credor_alienacao, 'id_cidade':id_cidade},
		beforeSend: function() {
			form.find('select[name=id_agencia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
		},
		success: function(agencias) {
			console.log(agencias);
			if (agencias.length>0) {
				form.find('select[name=id_agencia]').html('<option value="0">Selecione</option>');
				$.each(agencias,function(key,agencia) {
					//form.find('select[name=id_agencia]').append('<option value="'+agencia.id_agencia+'" '+(agencia.id_agencia==id_agencia?'selected="selected"':'')+'>'+agencia.codigo_agencia+'</option>');
					if ( id_cidade > 0 )
					{
						form.find('select[name=id_agencia]').append('<option value="'+agencia.id_agencia+'#'+agencia.id_credor_alienacao+'">'+agencia.codigo_agencia+'</option>');
					}else{
						form.find('select[name=id_agencia]').append('<option value="'+agencia.id_agencia+'#'+agencia.id_credor_alienacao+'" selected="selected">'+agencia.codigo_agencia+'</option>');
					}

				});
				form.find('select[name=id_agencia]').prop('disabled',false);
			} else {
				form.find('select[name=id_agencia]').prop('disabled',true).html('<option value="0">Nenhuma agência disponível</option>');

			}
		},
		error: function (request, status, error) {
			
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}

function retorno_credor() {
	var form = $('form[name=form-nova-alienacao]');
	var id_cidade = form.find('select[name=id_cidade]').val();
	if (id_cidade>0) {
		carrega_credores(id_cidade);
	}
}
function carrega_andamento(id_alienacao) {
	$.ajax({
		type: "POST",
		url: url_base+'servicos/alienacao/novo-andamento',
		data: 'id_alienacao='+id_alienacao,
		beforeSend: function() {
			$('div#detalhes-alienacao').find('div#novo-andamento>div.carregando').show();
			$('div#detalhes-alienacao').find('div#novo-andamento>div.form').hide();
		},
		success: function(retorno) {
			console.log(retorno);
			$('div#detalhes-alienacao').find('div#novo-andamento>div.form').html(retorno);
			$('div#detalhes-alienacao').find('div#novo-andamento>div.carregando').fadeOut('fast',function() {
				$('div#detalhes-alienacao').find('div#novo-andamento>div.form').fadeIn('fast');
			});
		},
		error: function (request, status, error) {
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}

