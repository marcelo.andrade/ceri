total_arquivos = 0;
total_arquivos_assinaveis = 0;
index_arquivos = 0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#novo-oficio').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/novo',
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90501", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#novo-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
	});

	$('div#novo-oficio, div#observacoes-oficio, div#responder-oficio').on('keyup','textarea[maxlength!=0]',function(e) {
		var limite = $(this).prop('maxlength');

		var alert_obj = $(this).closest('div.form-group').next().find('div.alert');
		alert_obj.find('div.menssagem span.total').html($(this).val().length);
		if ($(this).val().length<limite) {
			alert_obj.removeClass('alert-warning').addClass('alert-info');
			alert_obj.find('i').removeClass('.glyphicon-exclamation-sign').addClass('.glyphicon-info-sign');
			alert_obj.find('div.menssagem span.limite').html('');
		} else {
			alert_obj.removeClass('alert-info').addClass('alert-warning');
			alert_obj.find('i').removeClass('.glyphicon-info-sign').addClass('.glyphicon-exclamation-sign');
			alert_obj.find('div.menssagem span.limite').html('O limite foi atingido.');
		}
	});

	$('div#novo-oficio').on('change','select[name=tp_processo]', function(e) {
		var form = $(this).closest('form');
		if($(this).val()=='J') {
			form.find('input[name=numero_processo]').addClass('numero_processo');
		} else {
			form.find('input[name=numero_processo]').removeClass('numero_processo');
			form.find('input[name=numero_processo]').unmask();
		}
	});

	$('div#novo-oficio, div#encaminhar-oficio').on('change','select[name=id_tipo_usuario]',function(e) {
		var form = $(this).closest('form');
		var obj_select = $(this);

		if (form.find('div#destinatarios-selecionados div.destinatario').length>0) {
			swal({
				title: 'Atenção!',
				text: "Ao trocar o tipo de destinário todos os destinatários selecionados serão removidos. Deseja continuar?",
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				showLoaderOnConfirm: true,
			}).then(function(retorno) {
				if (retorno == true) {
					form.find('div#destinatarios-selecionados').html('');
					troca_tipo_usuario(form,obj_select);
				}
			}).catch(function(){
				$(this).val($(this).data('ultimo'));
				return false;
			});
		} else {
			troca_tipo_usuario(form,obj_select);
		}
	});

	$('div#novo-oficio, div#encaminhar-oficio').on('click','button.incluir-destinatario',function(e){
		var form = $(this).closest('form');
		incluir_destinatarios(form,'N');
	});
	
	$('div#novo-oficio, div#encaminhar-oficio').on('click','button.incluir-todos-destinatarios',function(e){
		var form = $(this).closest('form');
		incluir_destinatarios(form,'S');
	});

	$('div#novo-oficio, div#encaminhar-oficio').on('click', 'button.remover-destinatario', function (e) {
		var form = $(this).closest('form');
		var id_destinatario = $(this).data('iddestinatario');

		form.find('div#destinatarios-selecionados div.destinatario#'+id_destinatario).remove();
	});

	$('div#novo-oficio').on('click','div.radio input[name=id_tipo_custa]',function(e) {
		var form = $(this).closest('form');

		form.find('div#documento-isencao').hide();
		form.find('div#msg-isencao').hide();
		if ($(this).val()=='3') {
			swal({
				title: 'Atenção?',
				text: "Houve despacho de deferimento de gratuidade emitido pela AJG?",
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				showLoaderOnConfirm: true,
			}).then(function( retorno ) {
				if (retorno == true) {
					form.find('div#msg-isencao').hide();
					form.find('div#documento-isencao').show();
				}
			}).catch(function(e) {
				form.find('div#documento-isencao').hide();
				form.find('div#msg-isencao').show();
				form.find('input[name=id_tipo_custa]').prop('checked', false);
			});
		}
	});

	$('div#novo-oficio, div#encaminhar-oficio').on('change','select[name=id_cidade]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_cidade = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listarServentias',
			data: {'id_cidade':id_cidade},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(serventias) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_serventia]').html('<option value="0">Selecione uma serventia</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					if (id_cidade=="0") {
						form.find('select[name=id_serventia]').prop('disabled',true);
					} else {
						form.find('select[name=id_serventia]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_serventia]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER501", "error");
			}
		});
	});
	$('div#novo-oficio, div#encaminhar-oficio').on('change','select[name=id_comarca]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_comarca = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listarVaras',
			data: {'id_comarca':id_comarca},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(varas) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_vara]').html('<option value="0">Selecione uma vara</option>');
				if (varas.length>0) {
					$.each(varas,function(key,vara) {
						form.find('select[name=id_vara]').append('<option value="'+vara.id_vara+'">'+vara.no_vara+'</option>');
					});
					if (id_comarca=="0") {
						form.find('select[name=id_vara]').prop('disabled',true);
					} else {
						form.find('select[name=id_vara]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_vara]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER503", "error");
			}
		});
	});
	$('div#novo-oficio, div#encaminhar-oficio').on('change','select[name=id_unidade_judiciaria]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_unidade_judiciaria = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/listar-unidade-judiciaria-divisoes',
			data: {'id_unidade_judiciaria':id_unidade_judiciaria},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(divisoes) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_unidade_judiciaria_divisao]').html('<option value="0">Selecione uma divisão</option>');
				if (divisoes.length>0) {
					$.each(divisoes,function(key,divisao) {
						form.find('select[name=id_unidade_judiciaria_divisao]').append('<option value="'+divisao.id_unidade_judiciaria_divisao+'">'+divisao.no_unidade_judiciaria_divisao+'</option>');
					});
					if (id_unidade_judiciaria=="0") {
						form.find('select[name=id_unidade_judiciaria_divisao]').prop('disabled',true);
					} else {
						form.find('select[name=id_unidade_judiciaria_divisao]').prop('disabled',false);
					}
				} else {
					form.find('select[name=id_unidade_judiciaria_divisao]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro GER504", "error");
			}
		});
	});
	$('div#novo-oficio, div#encaminhar-oficio').on('change','select[name=id_serventia], select[name=id_vara], select[name=id_unidade_gestora], select[name=id_unidade_judiciaria_divisao]',function(e) {
		var form = $(this).closest('form');
		if ($(this).val()>0) {
			form.find('button.incluir-destinatario').removeClass('disabled').prop('disabled',false);
		} else {
			form.find('button.incluir-destinatario').addClass('disabled').prop('disabled',true);
		}
	});

	$('div#novo-oficio, div#responder-oficio').on('change','select[name=id_classificacao_documento], form[name=form-responder] select[name=id_classificacao_documento]',function(e) {
		var form = $(this).closest('form');
		var obj_modal = $(this).closest('.modal');
		var id_classificacao_documento = $(this).val();

		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/listar-tipos',
			data: {'id_classificacao_documento':id_classificacao_documento},
			beforeSend: function() {
				status_modal(obj_modal, true, true, true);
			},
			success: function(tipos) {
				status_modal(obj_modal, false, false, false);

				form.find('select[name=id_tipo_documento]').html('<option value="0">Selecione um tipo</option>');
				if (tipos.length>0) {
					$.each(tipos,function(key,tipo) {
						form.find('select[name=id_tipo_documento]').append('<option value="'+tipo.id_tipo_documento+'">'+tipo.no_tipo_documento+'</option>');
					});
					form.find('select[name=id_tipo_documento]').prop('disabled',false);
				} else {
					form.find('select[name=id_tipo_documento]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				status_modal(obj_modal, false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90502", "error");
			}
		});
	});

	$('div#novo-oficio').on('click','button.enviar-oficio',function(e) {
		var form = $('form[name=form-novo-oficio]');
		var obj_modal = $(this).closest('.modal');
		var erros = new Array();

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=numero_documento]').val()=='') {
			erros.push('O campo número documento é obrigatório');
		}
		if (form.find('input[name=numero_processo]').val()=='') {
			erros.push('O campo número processo é obrigatório');
		}
		if (form.find('select[name=id_tipo_usuario]').val()=='0') {
			erros.push('O campo tipo destinatário é obrigatório');
		} else {
			if ( form.find('div#destinatarios-selecionados div.destinatario').length==0) {
				erros.push('Selecione pelo menos um destinatário');
			}
		}
		if (form.find('input[name=id_tipo_custa]:checked').length<=0) {
			erros.push('O campo tipo de custa é obrigatório');
		} else {
			if (form.find('input[name=id_tipo_custa]:checked').val()==3 && form.find('div#arquivos-isencao div.arquivo').length<=0) {
				erros.push('Para que o processamento do Ofício se dê com AJG será necessário anexar o despacho de deferimento ou escolha outra opção no Tipo de Custa para dar continuidade à solicitação');
			}
		}
		if (form.find('select[name=id_classificacao_documento]').val()==0) {
			erros.push('O campo classificação é obrigatório');
		}

		if (form.find('select[name=id_tipo_documento]').val()==0) {
			erros.push('O campo tipo do documento é obrigatório');
		}

		if (form.find('div#arquivos-gerais div.arquivo').length<=0) {
			erros.push('É necessário pelo menos 01 arquivo para gravar o ofício');
		}

		if (total_arquivos_assinaveis>0) {
			erros.push('É obrigatório assinar os arquivos inseridos');
		}

		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/inserir',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90503", "error");
				}
			});
		}
	});

	////////////////////////////////////////////////////////////
	// Eventos da janela "Detalhes Ofício"                    //
	////////////////////////////////////////////////////////////

	$('div#detalhes-oficio').on('show.bs.modal', function (event) {
		var id_documento = $(event.relatedTarget).data('iddocumento');
		var protocolo = $(event.relatedTarget).data('protocolo');
		var origem = $(event.relatedTarget).data('origem');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/detalhes',
			data: {'id_documento':id_documento,'origem':origem},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$('table#oficios-recebidos tr#'+id_documento).removeClass('linha-notificacao-danger');
				if(origem=='R') {
					$('table#oficios-recebidos tr#'+id_documento+' td.situacao').html('Lida');
				}
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90504", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#detalhes-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#detalhes-oficio-historico').on('show.bs.modal', function (event) {
		var id_documento = $(event.relatedTarget).data('iddocumento');
		var protocolo = $(event.relatedTarget).data('protocolo');
		var origem = $(event.relatedTarget).data('origem');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/detalhes',
			data: {'id_documento':id_documento,'origem':origem},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				status_modal($(this), false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#detalhes-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	////////////////////////////////////////////////////////////
	// Eventos do formulário "Responder Ofício"               //
	////////////////////////////////////////////////////////////

	$('div#responder-oficio').on('show.bs.modal', function (event) {
		var id_documento = $(event.relatedTarget).data('iddocumento');
		var protocolo = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/responder',
			data: {'id_documento':id_documento},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$('table#oficios-recebidos tr#'+id_documento).removeClass('linha-notificacao-danger');
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				status_modal($(this), false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90505", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#responder-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#responder-oficio').on('submit','form[name=form-inserir-prenotacao]',function(e) {
		e.preventDefault();
		var form = $(this);
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=codigo_prenotacao]').val()=='') {
			erros.push('O campo código é obrigatório');
		}
		if (form.find('input[name=dt_prenotacao]').val()=='') {
			erros.push('O campo data de cadastro é obrigatório');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-prenotacao div').html(erros.join('<br />'));
			if (!form.find('div.erros-prenotacao').is(':visible')) {
				form.find('div.erros-prenotacao').slideDown();
			}
		} else {
			form.find('div.erros-prenotacao').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/inserir-prenotacao',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							form.find('input[name=codigo_prenotacao]').prop('disabled',true);
							form.find('input[name=dt_prenotacao]').prop('disabled',true);
							form.find('div.botoes').remove();
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90506", "error");
				}
			});
		}
	});
	$('div#responder-oficio').on('reset','form[name=form-inserir-prenotacao]',function(e) {
		var form = $(this);
		form.find('input[name=codigo_prenotacao]').val('');
		form.find('input[name=dt_prenotacao]').val('').datepicker('update');
		form.find('div.erros-prenotacao').slideUp();
	});

	$('div#responder-oficio').on('submit','form[name=form-inserir-custas]',function(e) {
		e.preventDefault();
		var form  = $(this);
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=de_custa]').val()=='') {
			erros.push('O campo descrição é obrigatório');
		}
		if (form.find('input[name=va_custa]').val()=='') {
			erros.push('O campo valor é obrigatório');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-custas div').html(erros.join('<br />'));
			if (!form.find('div.erros-custas').is(':visible')) {
				form.find('div.erros-custas').slideDown();
			}
		} else {
			form.find('div.erros-custas').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/inserir-custas',
				data: form.serialize(),
				beforeSend: function() {
					$('div#inserir-custas-oficio .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							form.find('input[name=de_custa]').prop('disabled',true);
							form.find('input[name=va_custa]').prop('disabled',true);
							form.find('div.botoes').remove();
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90507", "error");
				}
			});
		}
	});
	$('div#responder-oficio').on('reset','form[name=form-inserir-custas]',function(e) {
		var form = $(this);
		form.find('input[name=de_custa]').val('');
		form.find('input[name=va_custa]').val('');
		form.find('div.erros-custas').slideUp();
	});

	$('div#responder-oficio').on('click','form[name=form-responder] input[name=in_reposta_positivo]',function(e) {
		var form = $(this).closest('form');

		form.find('textarea[name=de_resposta]').prop('disabled',false);
		switch ($(this).val()) {
			case 'S':
				form.find('textarea[name=de_resposta]').val('Em resposta ao ofício encaminhado a esta serventia registral, segue em anexo arquivo com o cumprimento das requisições/determinações.').trigger('keyup');
				break;
			case 'N':
				form.find('textarea[name=de_resposta]').val('Em resposta ao ofício encaminhado a esta serventia registral, requeremos, segue em anexo arquivo para a devida análise.').trigger('keyup');
				break;
		}
	});
	$('div#responder-oficio').on('submit','form[name=form-responder]',function(e) {
		e.preventDefault();
		var erros = new Array();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('textarea[name=de_resposta]').val()=='' && form.find('div#arquivos-gerais div.arquivo').length<=0) {
			erros.push('É obrigatório a inserção de uma resposta ou ao menos um arquivo');
		}
		if (form.find('select[name=id_classificacao_documento]').val()==0) {
			erros.push('O campo classificação é obrigatório');
		}
		if (form.find('select[name=id_tipo_documento]').val()==0) {
			erros.push('O campo tipo do documento é obrigatório');
		}
		
		if (total_arquivos_assinaveis>0) {
			erros.push('É obrigatório assinar os arquivos inseridos');
		}

		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros-responder div').html(erros.join('<br />'));
			if (!form.find('div.erros-responder').is(':visible')) {
				form.find('div.erros-responder').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/inserir-resposta',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							form.find('input[name=de_custa]').prop('disabled',true);
							form.find('input[name=va_custa]').prop('disabled',true);
							form.find('div.botoes').remove();
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90508", "error");
				}
			});
		}
	});
	$('div#responder-oficio').on('reset','form[name=form-responder]',function(e) {
		var form = $(this);
		form.find('input[name=in_reposta_positivo]:checked').prop('checked',false);
		form.find('textarea[name=de_resposta]').val('').trigger('keyup');
		form.find('div.erros-responder').slideUp();
	});

	////////////////////////////////////////////////////////////
	// Eventos do formulário "Observações"                    //
	////////////////////////////////////////////////////////////

	$('div#observacoes-oficio').on('show.bs.modal', function (event) {
		var id_documento = $(event.relatedTarget).data('iddocumento');
		var protocolo = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/observacoes',
			data: {'id_documento':id_documento},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$('table#oficios-recebidos tr#'+id_documento).removeClass('linha-notificacao-danger');
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				status_modal($(this), false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90511", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#observacoes-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});

	$('div#observacoes-oficio').on('submit','form[name=form-observacao]',function(e) {
		e.preventDefault();
		var erros = new Array();
		var form = $(this);
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('textarea[name=de_observacao]').val()=='') {
			erros.push('O campo observacao é obrigatório');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/inserir-observacao',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							if (typeof retorno.observacao !== "undefined") {
								var novaLinha = $("<tr>");
								var colunas = "";
								colunas += '<td>'+retorno.usuario+'</td>';
								colunas += '<td>'+retorno.dt_formatada+'</td>';
								colunas += '<td>'+retorno.observacao.de_observacao+'</td>';

								novaLinha.append(colunas);
								$("table#historico-observacoes").prepend(novaLinha);
							}
							form.find('input[type=reset]').trigger('click');
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90512", "error");
				}
			});
		}
	});

	////////////////////////////////////////////////////////////
	// Eventos do formulário "Encaminhar Ofício"              //
	////////////////////////////////////////////////////////////

	$('div#encaminhar-oficio').on('show.bs.modal', function (event) {
		var id_documento = $(event.relatedTarget).data('iddocumento');
		var protocolo = $(event.relatedTarget).data('protocolo');
		var origem = $(event.relatedTarget).data('origem');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: url_base+'servicos/oficio/encaminhar',
			data: {'id_documento':id_documento,'origem':origem},
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$('table#oficios-recebidos tr#'+id_documento).removeClass('linha-notificacao-danger');
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				status_modal($(this), false, false, false);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90510", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#encaminhar-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
	});

	$('div#encaminhar-oficio').on('click','button.encaminhar-oficio',function(e) {
		var form = $('form[name=form-encaminhar]');
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('input[name=numero_documento]').val()=='') {
			erros.push('O campo número documento é obrigatório');
		}
		if (form.find('input[name=numero_processo]').val()=='') {
			erros.push('O campo número processo é obrigatório');
		}
		if (form.find('select[name=id_tipo_usuario]').val()=='0') {
			erros.push('O campo tipo destinatário é obrigatório');
		} else {
			if ( form.find('div#destinatarios-selecionados div.destinatario').length==0) {
				erros.push('Selecione pelo menos um destinatário');
			}
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/inserir-encaminhamento',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90510", "error");
				}
			});
		}
	});

	////////////////////////////////////////////////////////////
	// Eventos gerais dos ofícios                             //
	////////////////////////////////////////////////////////////

	$('table#oficios-recebidos, table#oficios-enviados').on('click','td.options a.excluir',function(e) {
		e.preventDefault();
		var id_documento = $(this).data('iddocumento');
		var protocolo = $(this).data('protocolo');

		if (id_documento>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja excluir o ofício nº '+protocolo,
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: url_base+'servicos/oficio/excluir',
						data: {'id_documento':id_documento},
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									var alerta = swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									var alerta = swal("Sucesso!",retorno.msg,"success");
									break;
								case 'alerta':
									var alerta = swal("Ops!",retorno.msg,"warning");
									break;
							}
							alerta.then(function(){
								if (retorno.recarrega=='true') {
									location.reload();
								}
							});
						},
						error: function (request, status, error) {
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90509", "error");
						}
					});
				}
			});
		}
	});

	$('div#recibo-oficio').on('show.bs.modal', function (event) {
		var id_documento = $(event.relatedTarget).data('iddocumento');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

		if (id_documento>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/oficio/recibo',
				data: {'id_documento':id_documento},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, true);
				},
				success: function(retorno) {
					status_modal($(this), false, false, false);
					$(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
				},
				error: function (request, status, error) {
					status_modal($(this), false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});
	$('div#recibo-oficio').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form').html('');
	});
});

function incluir_destinatarios(form,todas) {
	switch (form.find('select[name=id_tipo_usuario]').val()) {
		case '2':
			if (todas=='N') {
				var destinatarios = form.find('select[name=id_serventia] option:selected');
			} else {
				var destinatarios = form.find('select[name=id_serventia] option[value!="0"]');
			}
			break;
		case '4':
			if (todas=='N') {
				var destinatarios = form.find('select[name=id_vara] option:selected');
			} else {
				var destinatarios = form.find('select[name=id_vara] option[value!="0"]');
			}
			break;
		case '7':
			if (todas=='N') {
				var destinatarios = form.find('select[name=id_unidade_gestora] option:selected');
			} else {
				var destinatarios = form.find('select[name=id_unidade_gestora] option[value!="0"]');
			}
			break;
		case '11':
			if (todas=='N') {
				var destinatarios = form.find('select[name=id_unidade_judiciaria_divisao] option:selected');
			} else {
				var destinatarios = form.find('select[name=id_unidade_judiciaria_divisao] option[value!="0"]');
			}
			break;
	}

	destinatarios.each(function (e) {
		if (form.find('div#destinatarios-selecionados div.destinatario#'+$(this).val()).length>0) {
			if (todas=='N') {
				swal("Ops!", "Este destinatário já foi escolhido", "warning");
			}
		} else {
			html = 	'<div id="'+$(this).val()+'" class="destinatario btn-group" data-nodestinatario="'+$(this).text()+'">';
				html +=	'<button class="btn btn-sm btn-primary" type="button">'+$(this).text()+'</button>';
				html += '<input type="hidden" name="id_destinatario[]" value="'+$(this).val() +'">';
				html += '<button class="remover-destinatario btn btn-sm btn-danger" type="button" data-iddestinatario="'+$(this).val()+'"><i class="fa fa-times"></i></button>';
			html += '</div> ';

			$("div#destinatarios-selecionados").append(html);
		}
	});
}
function troca_tipo_usuario(form,obj_select) {
	switch ($(obj_select).val()) {
		case '0':
			form.find('div.campos-destinatario').hide('fast',function() {
				$(this).find('select').prop('disabled',true);
			});
			form.find('button.incluir-destinatario').addClass('disabled').prop('disabled',true);
			form.find('button.incluir-todos-destinatarios').addClass('disabled').prop('disabled',true);
			break;
		case '2':
			form.find('div.campos-destinatario').hide('fast',function() {
				$(this).find('select').prop('disabled',true);
			});
			form.find('div#campos-serventia').show('fast',function() {
				form.find('select[name=id_cidade]').prop('disabled',false);
				if (form.find('select[name=id_cidade]').val()>0) {
					form.find('select[name=id_cidade]').val('0').trigger('change');
				}
			});
			form.find('button.incluir-todos-destinatarios').removeClass('disabled').prop('disabled',false);
			break;
		case '4':
			form.find('div.campos-destinatario').hide('fast',function() {
				$(this).find('select').prop('disabled',true);
			});
			form.find('div#campos-judiciario').show('fast',function() {
				form.find('select[name=id_comarca]').prop('disabled',false);
				if (form.find('select[name=id_comarca]').val()>0) {
					form.find('select[name=id_comarca]').val('0').trigger('change');
				}
			});
			form.find('button.incluir-todos-destinatarios').removeClass('disabled').prop('disabled',false);
			break;
		case '7':
			form.find('div.campos-destinatario').hide('fast',function() {
				$(this).find('select').prop('disabled',true);
			});
			form.find('div#campos-unidade-gestora').show('fast',function() {
				form.find('select[name=id_unidade_gestora]').prop('disabled',false);
				form.find('select[name=id_unidade_gestora]').val('0');
			});
			form.find('button.incluir-todos-destinatarios').removeClass('disabled').prop('disabled',false);
			break;
		case '11':
			form.find('div.campos-destinatario').hide('fast',function() {
				$(this).find('select').prop('disabled',true);
			});
			form.find('div#campos-unidade-judiciaria').show('fast',function() {
				form.find('select[name=id_unidade_judiciaria]').prop('disabled',false);
				if (form.find('select[name=id_unidade_judiciaria]').val()>0) {
					form.find('select[name=id_unidade_judiciaria]').val('0').trigger('change');
				}
			});
			form.find('button.incluir-todos-destinatarios').removeClass('disabled').prop('disabled',false);
			break;
	}

	$(obj_select).data('ultimo',$(obj_select).val());
}

// Tratamento de retornos dos arquivos
function retorno_arquivo(retorno) {
	botao_arquivo = '<div class="arquivo btn-group" id="'+index_arquivos+'" data-inassdigital="'+retorno.arquivo.in_ass_digital+'">';
		botao_arquivo += '<button type="button" class="btn btn-primary">'+retorno.arquivo.no_arquivo+'</button>';
		botao_arquivo += '<input type="hidden" name="no_arquivo[]" value="'+retorno.arquivo.no_arquivo+'" />';
		if (retorno.arquivo.in_ass_digital=='S') {
			botao_arquivo += '<button type="button" class="icone-assinatura btn btn-warning"><i class="fa fa-unlock-alt"></i></button>';
		}
		botao_arquivo += '<button type="button" class="remover-arquivo btn btn-danger" data-linha="'+index_arquivos+'" data-token="'+retorno.token+'"><i class="fa fa-times"></i></button>';
	botao_arquivo += '</div> ';
	$('div#novo-arquivo').modal('hide');
	total_arquivos++;
	if (retorno.arquivo.in_ass_digital=='S') {
		total_arquivos_assinaveis++;
	}
	index_arquivos++;
	switch (retorno.arquivo.id_tipo_arquivo_grupo_produto) {
		case 15:
			$('div#arquivos-gerais').prepend(botao_arquivo);
			break;
		case 16:
			$('form[name=form-novo-oficio] div#arquivos-isencao').prepend(botao_arquivo);
			break;
	}
	if (total_arquivos_assinaveis>=0) {
		if (total_arquivos==1) {
			$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivo foi inserido, <b>'+total_arquivos_assinaveis+'</b> '+(total_arquivos_assinaveis>1?'podem ser assinados':'pode ser assinado')+'.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
		} else {
			$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivos foram inseridos, <b>'+total_arquivos_assinaveis+'</b> podem ser assinados.'+(total_arquivos_assinaveis>0?' Deseja assinar os arquivos?':''));
		}
		if (total_arquivos_assinaveis>0) {
			$('div#assinaturas-arquivos div.menssagem a').removeClass('btn-success').addClass('btn-warning').removeClass('disabled');
			$('div#assinaturas-arquivos div.alert').removeClass('alert-success').addClass('alert-warning');
			$('div#assinaturas-arquivos div.alert i').removeClass('glyphicon-ok').addClass('glyphicon-exclamation-sign');
		}
	}
}
function total_arquivo(id_tipo_arquivo_grupo_produto,no_arquivo) {
	var total_arquivo = 0;
	switch (id_tipo_arquivo_grupo_produto) {
		case 15:
			if (no_arquivo!='') {
				var total_arquivo = $('div#arquivos-gerais').find('input[value="'+no_arquivo+'"]').length;
			} else {
				var total_arquivo = $('form[name=form-novo-oficio] div#arquivos-gerais').find('div.arquivo').length;
			}
			break;
		case 16:
			if (no_arquivo!='') {
				var total_arquivo = $('form[name=form-novo-oficio] div#arquivos-isencao').find('input[value="'+no_arquivo+'"]').length;
			} else {
				var total_arquivo = $('form[name=form-novo-oficio] div#arquivos-isencao').find('div.arquivo').length;
			}
			break;
	}
	return total_arquivo;
}
function remove_arquivo(linha,arquivo) {
	switch (arquivo.id_tipo_arquivo_grupo_produto) {
		case 15:
			$('div#arquivos-gerais').find('div#'+linha).remove();
			break;
		case 16:
			$('form[name=form-novo-oficio] div#arquivos-isencao').find('div#'+linha).remove();
			break;
	}
	if (arquivo.in_ass_digital=='S') {
		total_arquivos_assinaveis--;
	}
	total_arquivos--;
	if (total_arquivos==0) {
		$('div#assinaturas-arquivos div.menssagem span').html('Nenhum arquivo foi inserido.');
		$('div#assinaturas-arquivos div.menssagem a').addClass('disabled');
	}
}
function retorno_assinatura(sucessos,falhas) {
	$.each(sucessos,function(key,val) {
		total_arquivos_assinaveis--;
		$('div.arquivo#'+val.id).find('button.icone-assinatura').removeClass('btn-warning').addClass('btn-success').html('<i class="fa fa-lock"></i>');
	});
	$.each(falhas,function(key,val) {
		$('div.arquivo#'+val.id).find('button.icone-assinatura').removeClass('btn-warning').addClass('btn-danger');
	});
	if (falhas>0) {
		$('div#assinaturas-arquivos div.menssagem span').html('O processo de assinatura foi completado. Porém, '+falhas.length+' não foram assinados.');
		$('div#assinaturas-arquivos div.menssagem a').addClass('disabled');
	} else {
		$('div#assinaturas-arquivos div.menssagem span').html('O processo de assinatura foi completado.');
		$('div#assinaturas-arquivos div.menssagem a').removeClass('btn-warning').addClass('btn-success').addClass('disabled');
		$('div#assinaturas-arquivos div.alert').removeClass('alert-warning').addClass('alert-success');
		$('div#assinaturas-arquivos div.alert i').removeClass('glyphicon-exclamation-sign').addClass('glyphicon-ok');
	}
	$('button.adicionar-arquivo').addClass('disabled').prop('disabled',true);
}
