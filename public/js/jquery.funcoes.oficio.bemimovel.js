$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('div#novo-imovel').on('show.bs.modal', function (event) {
		id_cidade = $(event.relatedTarget).data('idcidade');
		$.ajax({
			type: "GET",
			url: baseUrl+'servicos/bem-imovel/novo',
			beforeSend: function() {
				$('div#novo-imovel .modal-body div.carregando').show();
				$('div#novo-imovel .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#novo-imovel .modal-body div.form').html(retorno);
				$('div#novo-imovel .modal-body div.carregando').hide();
				$('div#novo-imovel .modal-body div.form').fadeIn('fast');
				var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');
				if (id_cidade>0) {
					form.find('select[name=bemimovel_id_cidade]').val(id_cidade);
				}
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#novo-imovel').on('hidden.bs.modal', function (event) {
		$('div#novo-imovel .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-imovel .modal-body div.form').html('');
	});
	
	$('div#novo-imovel').on('click','input[name=tp_pessoa]',function(e) {
		var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');
		form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case 'F':
				form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('(CPF)');
				form.find('input[name=nu_cpf_cnpj]').mask('000.000.000-00');
				break;
			case 'J':
				form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('(CNPJ)');
				form.find('input[name=nu_cpf_cnpj]').mask('00.000.000/0000-00');
				break;
		}
	});
	
	$('div#novo-imovel').on('click','button.incluir-proprietario',function(e) {
		var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');
		var tipo = $(this).data('tipo');		
		var erros = new Array();
		if (form.find('input[name=tp_pessoa]:checked').length<=0) {
			erros.push('O tipo de proprietário é obrigatório');
		} else {
			var tp_pessoa = form.find('input[name=tp_pessoa]:checked').val();
			if (form.find('input[name=nu_cpf_cnpj]').val()=='') {
				erros.push('O campo '+(tp_pessoa=='F'?'CPF':'CNPJ')+' é obrigatório');
			} else {
				switch (form.find('input[name=tp_pessoa]:checked').val()) {
					case 'F':
						if (!validarCPF(form.find('input[name=nu_cpf_cnpj]').val())) {
							erros.push('O CPF digitado é inválido');
						}
						break;
					case 'J':
						if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj]').val())) {
							erros.push('O CNPJ digitado é inválido');
						}
						break;
				}	
			}
		}
		if (form.find('input[name=no_proprietario_bem_imovel]').val()=='') {
			erros.push('O campo nome do proprietário é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros-partes div').html(erros.join('<br />'));
			if (!form.find('div.erros-partes').is(':visible')) {
				form.find('div.erros-partes').slideDown();
			}
		} else {
			form.find('div.erros-partes').slideUp();
			var tp_pessoa = form.find('input[name=tp_pessoa]:checked').val();
			var nu_cpf_cnpj = form.find('input[name=nu_cpf_cnpj]').val();
			var no_proprietario_bem_imovel = form.find('input[name=no_proprietario_bem_imovel]').val();
			var in_passivo_penhora = form.find('input[name=in_passivo_penhora]:checked').val();
			var novaLinha = $("<tr>");
			var colunas = "";
			colunas += '<td><input type="hidden" name="a_tp_pessoa[]" value="'+tp_pessoa+'" /><input type="hidden" name="a_nu_cpf_cnpj[]" class="nu_cpf_cnpj" value="'+nu_cpf_cnpj+'" />'+nu_cpf_cnpj+'</td>';
			colunas += '<td><input type="hidden" name="a_no_proprietario_bem_imovel[]" value="'+no_proprietario_bem_imovel+'" />'+no_proprietario_bem_imovel+'</td>';
			colunas += '<td><input type="hidden" name="a_in_passivo_penhora[]" value="'+(in_passivo_penhora=='S'?'S':'N')+'" />'+(in_passivo_penhora=='S'?'Sim':'Não')+'</td>';
			
			novaLinha.append(colunas);
			$("table#proprietarios-imovel").prepend(novaLinha);
			
			form.find('input[name=tp_pessoa]:checked').prop('checked',false);
			form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',true);
			form.find('input[name=no_proprietario_bem_imovel]').val('');
		}
	});

	$('div#novo-imovel').on('click','button.cancelar-proprietario',function(e) {
		var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');
		form.find('div.erros-partes').slideUp();
		form.find('input[name=tp_pessoa]:checked').prop('checked',false);
		form.find('input[name=nu_cpf_cnpj]').parent('div').find('label span').html('');
		form.find('input[name=nu_cpf_cnpj]').val('').prop('disabled',true);
		form.find('input[name=no_proprietario_bem_imovel]').val('');
		form.find('input[name=in_passivo_penhora]:checked').prop('checked',false);
	});
	
	
	$('div#novo-imovel').on('click','button.incluir-imovel',function(e) {
		e.preventDefault();
		var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');
		var erros = new Array();
		if (form.find('select[name=id_serventia]').is(':disabled') || form.find('select[name=id_serventia]').val()=='0') {
			erros.push('O campo cartório é obrigatório.');
		}
		if (form.find('input[name=matricula_bem_imovel]').val()=='') {
			erros.push('O campo matrícula é obrigatório');
		} else if (total_imoveis(form.find('input[name=matricula_bem_imovel]').val())>0) {
			erros.push('A matrícula digitada já foi cadastrada');
		}
		if (form.find('select[name=bemimovel_id_cidade]').val()=='0') {
			erros.push('O campo cidade do imóvel é obrigatório');
		}
		if (form.find('select[name=id_tipo_zona_imovel]').val()=='0') {
			erros.push('O campo tipo do imóvel é obrigatório');
		}
		if (form.find('input[name=de_endereco]').val()=='') {
			erros.push('O campo endereço é obrigatório');
		}
		if (form.find('select[name=id_natureza_acao]').val()=='0') {
			erros.push('O campo natureza do processo é obrigatório');
		}
		if (form.find('input.nu_cpf_cnpj').length==0) {
			erros.push('Os proprietários do imóvel são obrigatórios');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: baseUrl+'servicos/bem-imovel/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#novo-imovel .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div#novo-imovel .modal-body div.carregando').hide().removeClass('flutuante');
					if (retorno=='ERRO') {
						alert('Erro desconhecido, tente novamente mais tarde');
					} else {
						retorno_imovel(retorno);
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});

	$('div#novo-imovel').on('change','select[name=id_cidade]',function(e) {
		var form = $('div#novo-imovel').find('form[name=form-novo-imovel]');
		id_cidade = $(this).val();
		$.ajax({
			type: "POST",
			url: 'listarServentias',
			data: 'id_cidade='+id_cidade,
			beforeSend: function() {
				form.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
			},
			success: function(serventias) {
				form.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					form.find('select[name=id_serventia]').prop('disabled',false);
				} else {
					form.find('select[name=id_serventia]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	
	$('div#detalhes-imovel').on('show.bs.modal', function (event) {
		var id_bem_imovel = $(event.relatedTarget).data('idbemimovel');
		var matricula_bem_imovel = $(event.relatedTarget).data('matriculabemimovel');
		
		$(this).find('.modal-header h4.modal-title span').html(matricula_bem_imovel);

		if (id_bem_imovel>0) {
			$.ajax({
				type: "POST",
				url: baseUrl+'servicos/bem-imovel/detalhes',
				data: 'id_bem_imovel='+id_bem_imovel,
				beforeSend: function() {
					$('div#detalhes-imovel .modal-body div.carregando').show();
					$('div#detalhes-imovel .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-imovel .modal-body div.form').html(retorno);
					$('div#detalhes-imovel .modal-body div.carregando').hide();
					$('div#detalhes-imovel .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
});