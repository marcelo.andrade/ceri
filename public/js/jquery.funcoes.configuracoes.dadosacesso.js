$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	form = $('form[name=form-dados-acesso]');

	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('input[name=senha_usuario]').val()=='') {
			erros.push('O campo senha atual é obrigatório.');
		}
		if (form.find('input[name=nova_senha_usuario]').val()=='') {
			erros.push('O campo nova senha é obrigatório.');
		} else if (form.find('input[name=nova_senha_usuario]').val().length<6) {
			erros.push('A nova senha deve conter no mínimo 6 caracteres.');
		}
		if (form.find('input[name=nova_senha_usuario2]').val()=='') {
			erros.push('O campo confirme a nova senha é obrigatório.');
		} else if (form.find('input[name=nova_senha_usuario2]').val()!=form.find('input[name=nova_senha_usuario]').val()) {
			erros.push('A confirmação deve ser identica a nova senha.');
		}
			
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
			return false;
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'dados-acesso/salvar',
				data: form.serialize(),
				beforeSend: function() {
					$('div.configuracoes .content div.carregando').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div.configuracoes .content div.carregando').hide();
					
					switch (retorno) {
						case 'ERRO_01':
							swal("Ops!", "A senha atual digitada é inválida, tente novamente.", "warning");
							break;
						case 'ERRO_02':
							swal("Ops!", "A nova senha digitada já foi utilizada, digite uma nova senha.", "warning");
							break;
						case 'ERRO':
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
						default:
							swal("Sucesso!", "Dados de acesso salvos com sucesso.", "success").then(function(){
								location.reload();
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});

});