$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(".placa").mask("AAA9999");
	$(".crv").mask("999999999999");
	$(".chassi").mask("AAAAAAAAAAAAAAAAA");
	$(".renavam").mask("99999999999");

	form = $('form[name=form-comunicacao]');

	form.on('change', 'select[name=nu_ano_fabricacao]', function(e){
		form.find('select[name=nu_ano_modelo]').val($(this).val());
	});

	form.on('change', 'select[name=nu_ano_modelo]', function(e){
		var ano_modelo 		= parseInt( $(this).val() );
		var ano_fabricacao  = parseInt( form.find('select[name=nu_ano_fabricacao]').val() );
		var dif_ano			= ano_modelo-ano_fabricacao;

		if (ano_modelo < ano_fabricacao)
		{
			swal("Atenção!", "O Ano modelo não pode ser menor do que o Ano de fabricação", "warning").then(function() {
				form.find('select[name=nu_ano_modelo]').val(ano_fabricacao);
			});
		}

		if (dif_ano > 1 )
		{
			swal("Atenção!", "O Ano modelo não pode ser superior a 01 Ano de fabricação", "warning").then(function() {
				form.find('select[name=nu_ano_modelo]').val(ano_fabricacao);
			});
		}
	});

	//BUSCAR MODELOS DA MARCA DO VEICULO
	form.on('change','select[name=id_marca_veiculo]',function(e) {
		id_marca_veiculo = $(this).val();

		$.ajax({
			type: "POST",
			url: baseUrl+'/listarModelos',
			data: 'id_marca_veiculo='+id_marca_veiculo,
			beforeSend: function() {
				form.find('select[name=id_modelo_veiculo]').prop('disabled',true).html('<option value="0">Carregando...</option>');
			},
			success: function(modelos) {
				form.find('select[name=id_modelo_veiculo]').html('<option value="0">Selecione um modelo</option>');
				if (modelos.length>0) {
					$.each(modelos,function(key,modelos) {
						form.find('select[name=id_modelo_veiculo]').append('<option value="'+modelos.id_modelo_veiculo+'">'+modelos.no_modelo_veiculo+'</option>');
					});
					form.find('select[name=id_modelo_veiculo], button[name=incluir-serventia]').prop('disabled',false);
				}
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});

	//BUSCAR SE O VEICULO JÁ EXISTE NA BASE
	form.on('blur','input[name=placa]',function(e) {
		var placa = $(this).val();
		    placa = placa.toUpperCase();
			$(this).val(placa);

		$.ajax({
			type: "POST",
			url: baseUrl+'/listarVeiculo',
			data: 'placa='+placa,
			beforeSend: function() {
				$('div#nova_comunicacao .modal-body div.carregando').addClass('flutuante').show();
			},
			success: function(veiculo) {
				$('div#nova_comunicacao .modal-body div.carregando').removeClass('flutuante').hide();

				if ( veiculo.chassi != null )
				{
					form.find('input[name=id_veiculo]').val(veiculo.id_veiculo);
					form.find('input[name=crv]').val(veiculo.crv).prop('disabled',true);
					form.find('input[name=renavam]').val(veiculo.renavam).prop('disabled',true);
					form.find('input[name=chassi]').val(veiculo.chassi).prop('disabled',true);
					form.find('select[name=id_cor_veiculo]').val(veiculo.id_cor_veiculo).prop('disabled',true);
					form.find('select[name=nu_ano_fabricacao]').val(veiculo.nu_ano_fabricacao).prop('disabled',true);
					form.find('select[name=nu_ano_modelo]').val(veiculo.nu_ano_modelo).prop('disabled',true);
					form.find('select[name=id_marca_veiculo]').val(veiculo.id_marca_veiculo).prop('disabled',true);
					form.find('select[name=id_modelo_veiculo]').val(veiculo.id_modelo_veiculo).prop('disabled',true);
					form.find('select[name=id_modelo_veiculo] option').remove();
					form.find('select[name=id_modelo_veiculo]').append('<option value="'+veiculo.id_modelo_veiculo+'">'+veiculo.no_modelo_veiculo+'</option>');
					form.find('select[name=id_modelo_veiculo]').prop('disabled',true);
				}else{
					form.find('input[name=id_veiculo]').val(0);
					form.find('input[name=crv]').val('').prop('disabled',false);
					form.find('input[name=renavam]').val('').prop('disabled',false);
					form.find('input[name=chassi]').val('').prop('disabled',false);
					form.find('select[name=id_cor_veiculo]').val(0).prop('disabled',false);
					form.find('select[name=nu_ano_fabricacao]').val(0).prop('disabled',false);
					form.find('select[name=nu_ano_modelo]').val(0).prop('disabled',false);
					form.find('select[name=id_marca_veiculo]').val(0).prop('disabled',false);
					form.find('select[name=id_modelo_veiculo]').html('<option value="0">Selecione uma marca</option>').val(0).prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});


	//BUSCAR SE O PESSOA JÁ EXISTE NA BASE
	form.on('blur','input[name=nu_cpf_cnpj_vendedor]',function(e) {
		buscarPessoa($(this), 'vendedor');
	});

	form.on('blur','input[name=nu_cpf_cnpj_comprador]',function(e) {
		buscarPessoa($(this), 'comprador');
	});

	$('div#nova_comunicacao').on('click','input[name=tp_pessoa_vendedor]',function(e) {
		TipoPessoa($(this),'vendedor');

	});

	$('div#nova_comunicacao').on('click','input[name=tp_pessoa_comprador]',function(e) {
		TipoPessoa($(this),'comprador');
	});
	
	$('div#nova_comunicacao').on('click','button.incluir_telefone_vendedor', function(e){
		TelefoneTipoPessoa('vendedor');
	});
	
	$('div#nova_comunicacao').on('click','button.incluir_telefone_comprador', function(e){
		TelefoneTipoPessoa('comprador');
	});

	$('div#detalhes-venda').on('show.bs.modal', function (event) {
		var id_operacao_veiculo = $(event.relatedTarget).data('id_operacao_veiculo');
		var protocolo 	 		= $(event.relatedTarget).data('protocolo');

		$(this).find('.modal-header h4.modal-title span').html(protocolo);

		$.ajax({
			type: "POST",
			url: baseUrl+'/detran/venda/detalhes-comunicacao',
			data: {id_operacao_veiculo:id_operacao_veiculo},
			beforeSend: function() {
				$('div#detalhes-venda .modal-body div.carregando').show();
				$('div#detalhes-venda .modal-body div.form').hide();
			},
			success: function(retorno) {
				$('div#detalhes-venda .modal-body div.form').html(retorno);
				$('div#detalhes-venda .modal-body div.carregando').hide();
				$('div#detalhes-venda .modal-body div.form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});

	$('div#nova_comunicacao').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: baseUrl+'detran/venda/nova-comunicacao',
			beforeSend: function() {
				$('div#nova_comunicacao .modal-body div.carregando').show();
				$('div#nova_comunicacao .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#nova_comunicacao .modal-body div.form form').html(retorno);
				$('div#nova_comunicacao .modal-body div.carregando').hide();
				$('div#nova_comunicacao .modal-body div.form form').fadeIn('fast');

				form = $('form[name=form-comunicacao]');

				form.find('#arquivos_pf_vendedor').hide();
				form.find('#arquivos_pj_vendedor').hide();
				form.find('#arquivos_pf_comprador').hide();
				form.find('#arquivos_pj_comprador').hide();

			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});

	$('div#nova_comunicacao').on('click', '.remove-telefones-comprador', function (e) {
		var id_telefone = $(this).data('telefone');
		$('div#telefones_comprador_escolhidas div#'+id_telefone).remove();
	});

	$('div#nova_comunicacao').on('click', '.remove-telefones-vendedor', function (e) {
		var id_telefone = $(this).data('telefone');
		$('div#telefones_vendedor_escolhidas div#'+id_telefone).remove();
	});

	$('div#nova_comunicacao').on('click','button.enviar-comunicacao',function(e){
		var form 			= $('form[name=form-comunicacao]');
		if ( validaForm() == true )
		{
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: baseUrl+'detran/venda/inserir-comunicacao',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#nova_comunicacao .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega == 'true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
						$('div#nova_comunicacao .modal-body div.carregando').removeClass('flutuante').hide();
					});
				}
			});
		}
	});

});

/*
  * vtipo_pessoa = vendedor ou comprador
 */

function TelefoneTipoPessoa( vtipo_pessoa )
{
	var erros 			= new Array();
	var tptelefone 		= form.find('select[name=id_tipo_telefone_'+vtipo_pessoa+']').val();
	var ddd  	   		= form.find('input[name=nu_ddd_'+vtipo_pessoa+']').val();
	var telefone        = form.find('input[name=nu_telefone_'+vtipo_pessoa+']').val();

	if (telefone == "") {
		erros.push('O campo telefone é obrigatório.');
	}else{
		var isSelecionado = false;
		form.find('#telefones_'+vtipo_pessoa+'_escolhidas div.telefones').each(function () {
			if ($(this).data('telefone') == telefone) {
				swal("Ops!", "Este telefone já foi escolhido", "warning");
				isSelecionado = true;
			}
		});
	}

	if (ddd == "") {
		erros.push('O campo DDD é obrigatório.');
	}
	if (tptelefone == 0) {
		erros.push('O campo tipo de telefone é obrigatório.');
	}


	if (erros.length>0) {
		form.find('div.erros div').html(erros.join('<br />'));
		if (!form.find('div.erros').is(':visible')) {
			form.find('div.erros').slideDown();
		}
	}else{

		if (isSelecionado == false)
		{
			var html =  '<div id="' + telefone + '" data-telefone="' + telefone + '"  class="telefones btn-group">';
				html += '<button class="btn btn-sm btn-primary" type="button">' + ddd + ' - ' + telefone + '</button>';
				html += '<input  type="hidden" name="telefones_selecionado_'+vtipo_pessoa+'[]"       value="0#' + tptelefone + '#' + ddd + '#' + telefone + '">';
				html += '<button class="remove-telefones-' + vtipo_pessoa + ' btn  btn-sm btn-danger" type="button" data-telefone="' + telefone + '">×</button>&nbsp;</div>';
			$('div#telefones_' + vtipo_pessoa + '_escolhidas').append(html);
		}

		form.find('select[name=id_tipo_telefone_'+vtipo_pessoa+']').val(0);
		form.find('input[name=nu_ddd_'+vtipo_pessoa+']').val('');
		form.find('input[name=nu_telefone_'+vtipo_pessoa+']').val('');
	}
}

function TipoPessoa( vtipo, vtipo_pessoa )
{
	form = $('form[name=form-comunicacao]');
	form.find('input[name=nu_cpf_cnpj_'+vtipo_pessoa+']').val('').prop('disabled',false);

	form.find('input[name=id_pessoa]').val(0);
	form.find('input[name=no_'+vtipo_pessoa+']').val('');
	form.find('input[name=no_email_'+vtipo_pessoa+']').val('');
	form.find('input[name=rg_numero_'+vtipo_pessoa+']').val('');
	$('div#telefones_'+vtipo_pessoa+'_escolhidas').html('');

	switch (vtipo.val()) {
		case 'F':
			form.find('input[name=nu_cpf_cnpj_'+vtipo_pessoa+']').parent('div').find('label span').html('(CPF)');
			form.find('input[name=no_'+vtipo_pessoa+']').parent('div').find('label span').html('Nome');
			form.find('input[name=nu_cpf_cnpj_'+vtipo_pessoa+']').mask('000.000.000-00');
			form.find('input[name=rg_numero_'+vtipo_pessoa+']').parent('div').show();
			form.find('#arquivos_pf_'+vtipo_pessoa).show();
			form.find('#arquivos_pj_'+vtipo_pessoa).hide();
			break;
		case 'J':
			form.find('input[name=nu_cpf_cnpj_'+vtipo_pessoa+']').parent('div').find('label span').html('(CNPJ)');
			form.find('input[name=no_'+vtipo_pessoa+']').parent('div').find('label span').html('Razão social');
			form.find('input[name=nu_cpf_cnpj_'+vtipo_pessoa+']').mask('00.000.000/0000-00');
			form.find('input[name=rg_numero_'+vtipo_pessoa+']').parent('div').hide();
			form.find('#arquivos_pf_'+vtipo_pessoa).hide();
			form.find('#arquivos_pj_'+vtipo_pessoa).show();
			break;
	}
}

function buscarPessoa( vobjeto, vtipo_pessoa )
{
	var nu_cpf_cnpj = vobjeto.val();

	if (nu_cpf_cnpj.length < 1 )
	{
		return false;
	}

	$.ajax({
		type: "POST",
		url: baseUrl+'listarPessoa',
		data: 'nu_cpf_cnpj='+nu_cpf_cnpj,
		beforeSend: function() {
			$('div#nova_comunicacao .modal-body div.carregando').addClass('flutuante').show();
			$('div#telefones_'+vtipo_pessoa+'_escolhidas').html('');
		},
		success: function( dadosPessoa ) {
			$('div#nova_comunicacao .modal-body div.carregando').removeClass('flutuante').hide();

			if ( typeof dadosPessoa.pessoa !== 'undefined')
			{
				form.find('input[name=id_pessoa_'+vtipo_pessoa+']').val(dadosPessoa.pessoa.id_pessoa);
				form.find('input[name=no_'+vtipo_pessoa+']').val(dadosPessoa.pessoa.no_pessoa);
				form.find('input[name=no_email_'+vtipo_pessoa+']').val(dadosPessoa.pessoa.no_email_pessoa);
				form.find('input[name=rg_numero_'+vtipo_pessoa+']').val(dadosPessoa.pessoa.nu_rg);

				$.each(dadosPessoa.telefones,function(key,telefone)
				{
					var html = '<div id="' + $.trim(telefone.nu_telefone) + '" data-telefone="' + $.trim(telefone.nu_telefone) + '"  class="telefones btn-group">';
					html += '<button type="button" class="btn btn-sm btn-primary" >'+telefone.nu_ddd+' - '+telefone.nu_telefone+'</button>';
					html += '<input  type="hidden" name="telefones_selecionado_'+vtipo_pessoa+'[]"       value="' + $.trim(telefone.id_telefone) + '#' + $.trim(telefone.id_tipo_telefone) + '#' + $.trim(telefone.nu_ddd) + '#' + $.trim(telefone.nu_telefone) + '">';
					html += '<button type="button" class="remove-telefones-'+vtipo_pessoa+' btn  btn-sm btn-danger"  data-telefone="' + $.trim(telefone.nu_telefone) + '">×</button>&nbsp;</div>';
					$('div#telefones_'+vtipo_pessoa+'_escolhidas').append(html);
				});

			}else{
				form.find('input[name=id_pessoa]').val(0);
				form.find('input[name=no_'+vtipo_pessoa+']').val('');
				form.find('input[name=no_email_'+vtipo_pessoa+']').val('');
				form.find('input[name=rg_numero_'+vtipo_pessoa+']').val('');
				$('div#telefones_'+vtipo_pessoa+'_escolhidas').html('');
			}
		},
		error: function (request, status, error) {
			//console.log(request.responseText);
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}

function validaForm()
{
	var erros 			= new Array();
	var form 			= $('form[name=form-comunicacao]');

	//dados do vendedor
	if ( form.find('input[name=nu_cpf_cnpj_vendedor]').val() == ''){
		erros.push('O campo Documento identificador do vendedor é obrigatório');
	}
	if ( form.find('input[name=no_vendedor]').val() == ''){
		erros.push('O campo Nome/Razão Social do vendedor é obrigatório');
	}
	/*if ( form.find('input[name=no_email_vendedor]').val() != ''){
	 erros.push('O campo e-mail é obrigatório');
	 }*/
	if ( form.find('input[name=tp_pessoa_vendedor]:checked').val() == 'F')
	{
		if (!validarCPF(form.find('input[name=nu_cpf_cnpj_vendedor]').val())) {
			erros.push('O CPF do vendedor é inválido');
		}

		if ( form.find('input[name=rg_numero_vendedor]').val() == '')
		{
			erros.push('O campo Rg do vendedor é obrigatório');
		}
	}else{
		if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj_vendedor]').val())) {
			erros.push('O CNPJ do comprador é inválido');
		}
	}
	if ( form.find('#telefones_vendedor_escolhidas div.telefones').size() <= 0)
	{
		erros.push('É necessário fornecer pelo menos 01 telefone de contato do vendedor');
	}


	//dados do comprador
	if ( form.find('input[name=nu_cpf_cnpj_comprador]').val() == ''){
		erros.push('O campo Documento identificador do comprador é obrigatório');
	}
	if ( form.find('input[name=no_comprador]').val() == ''){
		erros.push('O campo Nome/Razão Social do comprador é obrigatório');
	}
	/*if ( form.find('input[name=no_email_comprador]').val() != ''){
	 erros.push('O campo e-mail é obrigatório');
	 }*/
	if ( form.find('input[name=tp_pessoa_comprador]:checked').val() == 'F')
	{
		if (!validarCPF(form.find('input[name=nu_cpf_cnpj_comprador]').val())) {
			erros.push('O CPF do vendedor é inválido');
		}

		if ( form.find('input[name=rg_numero_comprador]').val() == '')
		{
			erros.push('O campo Rg do comprador é obrigatório');
		}
	}else{
		if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj_comprador]').val())) {
			erros.push('O CNPJ do comprador é inválido');
		}
	}

	if ( form.find('#telefones_comprador_escolhidas div.telefones').size() <= 0)
	{
		erros.push('É necessário fornecer pelo menos 01 telefone de contato do comprador');
	}

	//dados do veiculo
	if (form.find('input[name=placa]').val() == '')
	{
		erros.push('O campo Placa é obrigatório');
	}
	if (form.find('input[name=crv]').val() == '')
	{
		erros.push('O campo Nº CRV é obrigatório');
	}
	if (form.find('input[name=chassi]').val() == '')
	{
		erros.push('O campo Nº Chassi é obrigatório');
	}
	if (form.find('input[name=renavan]').val() == '')
	{
		erros.push('O campo Nº Renavan é obrigatório');
	}
	if (form.find('select[name=id_cor_veiculo]').val() <= 0)
	{
		erros.push('O campo COR é obrigatório');
	}
	if (form.find('select[name=nu_ano_fabricacao]').val() <= 0)
	{
		erros.push('O campo Ano de fabricação é obrigatório');
	}
	if (form.find('select[name=nu_ano_modelo]').val() <= 0)
	{
		erros.push('O campo Ano modelo é obrigatório');
	}
	if (form.find('select[name=id_marca_veiculo]').val() <= 0)
	{
		erros.push('O campo Marca é obrigatório');
	}
	if (form.find('select[name=id_modelo_veiculo]').val() <= 0)
	{
		erros.push('O campo Modelo é obrigatório');
	}

	//validando arquivos para upload

	//dados do vendedor
	if ( form.find('input[name=tp_pessoa_vendedor]:checked').val() == 'F')
	{
		if (validaTipoArquivo(form.find('div.cpf_vendedor span.button input[type=file]')) == false) {
			erros.push('O arquivo do CPF do vendedor é obrigatório');
		}

		if (validaTipoArquivo(form.find('div.rg_vendedor span.button input[type=file]')) == false) {
			erros.push('O arquivo do RG do vendedor é obrigatório');
		}
	}else{
		if ( validaTipoArquivo( form.find('div.contrato_social_vendedor span.button input[type=file]') ) == false )
		{
			erros.push('O arquivo do Contrato social vendedor é obrigatório');
		}

		if ( validaTipoArquivo( form.find('div.cartao_cnpj_vendedor span.button input[type=file]') ) == false )
		{
			erros.push('O arquivo Cartão CNPJ do vendedor é obrigatório');
		}
	}
	if ( validaTipoArquivo( form.find('div.comprovante_endereco_vendedor span.button input[type=file]') ) == false )
	{
		erros.push('O arquivo do Comprovante de endereço do vendedor é obrigatório');
	}

	if ( validaTipoArquivo( form.find('div.crv_arquivo span.button input[type=file]') ) == false )
	{
		erros.push('O arquivo do CRV é obrigatório');
	}

	//dados do comprador
	if ( form.find('input[name=tp_pessoa_comprador]:checked').val() == 'F')
	{
		if (validaTipoArquivo(form.find('div.cpf_comprador  span.button input[type=file]')) == false) {
			erros.push('O arquivo do CPF do comprador é obrigatório');
		}

		if (validaTipoArquivo(form.find('div.rg_comprador  span.button input[type=file]')) == false) {
			erros.push('O arquivo do RG do comprador é obrigatório');
		}
	}else{
		if ( validaTipoArquivo( form.find('div.contrato_social_comprador span.button input[type=file]') ) == false )
		{
			erros.push('O arquivo do Contrato social comprador é obrigatório');
		}

		if ( validaTipoArquivo( form.find('div.cartao_cnpj_comprador span.button input[type=file]') ) == false )
		{
			erros.push('O arquivo Cartão CNPJ do comprador é obrigatório');
		}
	}

	if ( validaTipoArquivo( form.find('div.comprovante_endereco_comprador span.button input[type=file]') ) == false )
	{
		erros.push('O arquivo do Comprovante de endereço do comprador é obrigatório');
	}

	if ( form.find('input[name=nu_cpf_cnpj_vendedor]').val() == form.find('input[name=nu_cpf_cnpj_comprador]').val() )
	{
		erros.push('A comprador não pode ser o mesmo que o vendedor');
	}

	if (erros.length>0)
	{
		form.find('div.erros div').html(erros.join('<br />'));
		if (!form.find('div.erros').is(':visible')) {
			form.find('div.erros').slideDown();
		}
		return false;
	}

	return true;
}

function validaTipoArquivo( no_arquivo )
{
	var no_arquivo = no_arquivo.val().toLowerCase();
	if (no_arquivo != '')
	{
		if (!(/\.(pdf|png|jpg|bmp|tif|txt|jpeg)$/i).test(no_arquivo)) {
			swal("Ops!", "O arquivo selecionado é inválido, tente novamente.", "warning");
			return false;
		}
	}else{
		return false;
	}
	return true;
}