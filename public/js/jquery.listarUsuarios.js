$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('select[name=id_estado]').on('change',function(e) {
        id_estado = $(this).val();

        $('select[name=id_cidade]').html('<option value="0">Selecione uma cidade</option>');
        $.ajax({
            type: "GET",
            url: 'listarCidades',
            data: 'id_estado='+id_estado,
            success: function(cidades) {
                if (cidades.length>0) {
                    $.each(cidades,function(key,cidade) {
                        $('select[name=id_cidade]').append('<option value="'+cidade.id_cidade+'">'+cidade.no_cidade+'</option>');
                    });
                    $('select[name=id_cidade]').prop('readonly',false);
                }
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
        });
    });


    $('select[name=tp_pessoa]').on('change',function(e) {
        var tpPessoa = $(this).val();
        switch(tpPessoa) {
            case 'F':
                $('.pessoa-juridica').hide('slow');
                $('.pessoa-fisica').show('slow');
                break;
            case 'J':
                $('.pessoa-fisica').hide('slow');
                $('.pessoa-juridica').show('slow');
                break;
        }

    });

    $(".editarUsuario").on('click',function () {
        var tp_pessoa = $('input[name=tp_pessoa]').val();
        
        var erros = new Array();

        if ($('input[name=email_usuario]').val()=='') {
            erros.push('O campo e-mail é obrigatório');
        }

        if ($('input[name=senha_usuario]').val()!='') {
            if ($('input[name=senha_usuario]').val().length<6) {
                erros.push('A senha deve conter no mínimo 6 caracteres.');
            }
        }
        if ($('input[name=senha_usuario]').val()!='' && $('input[name=senha_usuario2]').val()=='') {
            erros.push('O campo confirme a senha é obrigatório.');
        } else if ($('input[name=senha_usuario2]').val()!=$('input[name=senha_usuario]').val()) {
            erros.push('A confirmação deve ser identica a senha.');
        }


        if ($('input[name=nu_cep]').val()=='') {
            erros.push('O campo cep é obrigatório');
        }

        if ($('input[name=no_endereco]').val()=='') {
            erros.push('O campo endereço é obrigatório');
        }

        if ($('input[name=nu_endereco]').val()=='') {
            erros.push('O campo numero do endereço é obrigatório');
        }
        if ($('input[name=no_bairro]').val()=='') {
            erros.push('O campo bairro é obrigatório');
        }

        switch (tp_pessoa) {
        case 'F':
            if ($('input[name=no_pessoa]').val()=='') {
                erros.push('O campo nome é obrigatório');

            }
            if ($('select[name=id_tipo_telefone]').val()=='0') {
                erros.push('O campo tipo do telefone é obrigatório');
            }
            if ($('select[name=tp_sexo]').val()=='0') {
                erros.push('O campo sexo é obrigatório');
            }

            if ($('input[name=nu_cpf_cnpj]').val()=='') {
                erros.push('O campo CPF é obrigatório');
            }else if (!validarCPF($('input[name=nu_cpf_cnpj]').val())) {
                erros.push('O CPF digitado é inválido');
            }

            if ($('input[name=dt_nascimento]').val()=='') {
                erros.push('O campo data de nascimento é obrigatório');
            }

            if ($('input[name=nu_ddd]').val()=='') {
                erros.push('O campo data DDD é obrigatório');
            }

            if ($('input[name=nu_telefone]').val()=='') {
                erros.push('O campo telefone é obrigatório');
            }
            break;
        case 'J':
            if ($('input[name=nu_cpf_cnpj_j]').val()=='') {
                erros.push('O campo CNPJ é obrigatório');
            }else if (!validarCNPJ($('input[name=nu_cpf_cnpj_j]').val())) {
                erros.push('O CNPJ digitado é inválido');
            }

            if ($('input[name=nu_inscricao_municipal_j]').val()=='') {
                erros.push('O campo inscrição municipal é obrigatório');
            }
            if ($('input[name=no_pessoa_j]').val()=='') {
                erros.push('O campo razão social é obrigatório');
            }
            if ($('input[name=no_fantasia_j]').val()=='') {
                erros.push('O campo nome fantasia é obrigatório');
            }
            if ($('select[name=id_tipo_telefone_j]').val()=='0') {
                erros.push('O campo tipo de telefone é obrigatório');
            }
            if ($('input[name=nu_ddd_j]').val()=='') {
                erros.push('O campo DDD é obrigatório');
            }
            if ($('input[name=nu_telefone_j]').val()=='') {
                erros.push('O campo telefone é obrigatório');
            }
            break;
        }




        if ($('select[name=id_estado]').val()=='0') {
            erros.push('O campo estado é obrigatório');
        }
        if ($('select[name=id_cidade]').val()=='0') {
            erros.push('O campo cidade é obrigatório');
        }

      if (erros.length>0) {
            $('div.erros div').html(erros.join('<br />'));
            if (!$('div.erros').is(':visible')) {
                $('div.erros').slideDown();
            }
            return false;
        }

        else{
            $("form[name=form-editar-usuario]").submit();
        }

    });
});