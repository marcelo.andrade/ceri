var total_arquivos = 0;
var total_arquivos_assinaveis = 0;
var index_arquivos = 0;
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    setTimeout(function(){
        $('form#form-filtro select.grupo').val('inicial');
        $('form#form-filtro select.grupo').selectpicker('refresh');
        $( "form#form-filtro select.grupo" ).trigger( "change" );
    }, 1000);


    $('form#form-filtro').on('change','select.grupo',function(e) {
        var grupo = $(this).val();
        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/filtros-alertas',
            data: {'grupo': grupo},
            success: function (grupos) {
                $('form#form-filtro select.filtro').prop('disabled', false);
                if (!$.isEmptyObject(grupos)) {
                    HTML = '';
                    $.each(grupos, function (key, grupo) {
                        HTML += '<optgroup label="'+grupo.grupo.titulo+'">';
                        $.each(grupo.alertas, function (key, alerta) {
                            switch (alerta.alcada) {
                                case 2:
                                    alcada = 'cartorio';
                                    break;
                                case 8:
                                    alcada = 'cef';
                                    break;
                                default:
                                    alcada = '';
                                    break;
                            }

                            HTML += '<option value="'+grupo.grupo.key+'_'+alerta.key+'" class="'+alcada+'">'+alerta.titulo+'</option>';
                        });
                        HTML += '</optgroup>';
                    });
                    $('form#form-filtro select.filtro').html(HTML);
                } else {
                    $('form#form-filtro select.filtro').html('').prop('disabled', true);
                }
                $('form#form-filtro select.filtro').selectpicker('refresh');
            },
            complete: function(data) {
                $('form#form-filtro select.filtro').val('inicial_protocolar');
                $('form#form-filtro select.filtro').selectpicker('refresh');
                if ( $('#parar_pesquisa').val() == 'N' )
                {
                    $('#parar_pesquisa').val('S');
                    $('form#form-filtro').submit();
                }
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error");
            }
        });
    });

    $('table#pedidos').on('click','input#selecionar-todas',function(e) {
        if ($(this).is(':checked')) {
            $('table#pedidos input.alienacao:not(:checked)').trigger('click');
        } else {
            $('table#pedidos input.alienacao:checked').trigger('click');
        }
    });

    $('table#pedidos').on('click','input.alienacao',function(e) {
        var total = $('table#pedidos input.alienacao:checked').length;

        if (total<=0) {
            $('div#alert span.total').html('0 notificações selecionadas');
            $('div#alert button').prop('disabled',true).addClass('disabled');

            $('div#alert ul.dropdown-menu a.gerar-recibo').parent('li').addClass('disabled');
            $('div#alert ul.dropdown-menu a.gerar-recibo span').html('');
        } else {
            $('div#alert span.total').html((total==1?'1 notificação selecionada':total+' notificações selecionadas'));
            $('div#alert button').prop('disabled',false).removeClass('disabled');

            $('div#alert ul.dropdown-menu a.gerar-recibo').parent('li').removeClass('disabled');
            $('div#alert ul.dropdown-menu a.gerar-recibo span').html('('+total+')');
        }
    });


    $('div#recibo-notificacoes').on('show.bs.modal', function (event) {
        var ids_alienacoes = [];

        $('table#pedidos input.alienacao:checked').each(function(e) {
            ids_alienacoes.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/gerar-recibo-lote',
            data: {'ids_alienacoes':ids_alienacoes},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
               // $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                //status_modal($(this), false, false, false);

                $(this).addClass('total-height');
                resize_modal($('div#recibo-notificacoes'));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

});
