$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#novo-protocolo').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: 'protocolo/novo-protocolo',
			beforeSend: function() {
				$('div#novo-protocolo .modal-body div.carregando').show();
				$('div#novo-protocolo .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#novo-protocolo .modal-body div.form form').html(retorno);
				$('div#novo-protocolo .modal-body div.carregando').hide();
				$('div#novo-protocolo .modal-body div.form form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	$('div#novo-protocolo').on('hidden.bs.modal', function (event) {
		$('div#novo-protocolo .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-protocolo .modal-body div.form form').html('');
	});

	var form = $('form[name=form-novo-protocolo]');

	form.on('change','select[name=id_cidade]',function(e) {
		id_cidade = $(this).val();
		
		$.ajax({
			type: "POST",
			url: 'listarServentias',
			data: 'id_cidade='+id_cidade,
			beforeSend: function() {
				form.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
			},
			success: function(serventias) {
				form.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					form.find('select[name=id_serventia], button[name=incluir-serventia], button[name=incluir-todas-serventia]').prop('disabled',false);
				} else {
					form.find('select[name=id_serventia], button[name=incluir-serventia], button[name=incluir-todas-serventia').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	
	$('div#novo-protocolo').on('click','button.incluir-protocolo',function(e) {
		e.preventDefault();
		var erros = new Array();

		if (form.find('select[name=id_serventia]').is(':disabled') || form.find('select[name=id_serventia]').val()=='0') {
			erros.push('O campo cartório é obrigatório.');
		}
		if (form.find('div#arquivos div.arquivo input[type=file]').length<=0) {
			erros.push('Ao menos um arquivo do protocolo deve ser inserido.');
		}
		
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: 'protocolo/inserir-protocolo',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#novo-protocolo .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#detalhes-protocolo').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'protocolo/detalhes',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#detalhes-protocolo .modal-body div.carregando').show();
					$('div#detalhes-protocolo .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-protocolo .modal-body div.form').html(retorno);
					$('div#detalhes-protocolo .modal-body div.carregando').hide();
					$('div#detalhes-protocolo .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
		
	form.on('change.bs.fileinput','div.novo_arquivo',function(e) {
		var novo_arquivo = $(this);
		var no_arquivo = novo_arquivo.find('input[type=file]');
		var total_arquivos = 0;

		form.find('div#arquivos div.arquivo input[type=file]').each(function(e) {
			if ($(this).val()==no_arquivo.val()) {
				total_arquivos++;
			}
		});

		if (total_arquivos>=1) {
			swal("Ops!", "O arquivo selecionado já foi escolhido.", "warning");
			novo_arquivo.fileinput('clear');
		} else {
			var no_arquivo = no_arquivo.val().toLowerCase();
			var no_arquivo = no_arquivo.split('\\');
			var no_arquivo = no_arquivo[no_arquivo.length-1];

			if ((/\.(cmd|bat|scr|exe|vbs|ws|zip|rar|7zip|tar|gzip|dll)$/i).test(no_arquivo)) {
				swal("Ops!", "O arquivo selecionado é inválido, tente novamente.", "warning");
				novo_arquivo.fileinput('clear');
			} else {
				var novo = $(this).clone();

				$(this).removeClass('novo_arquivo').removeClass('fileinput').removeClass('fileinput-new').addClass('arquivo').addClass('btn-group');
				$(this).find('span.btn-file').hide();
		    	
		    	bt_nome = '<button type="button" class="btn btn-primary">'+no_arquivo+'</button>';
		        bt_remover = '<button type="button" class="remover btn btn-danger">&times;</button>';

		        $(this).prepend(bt_nome);
		        $(this).append(bt_remover);

				novo.appendTo('div#arquivos');
				novo.fileinput('clear');
			}
		}
	});
	form.on('click','div.arquivo button.remover',function(e) {
		$(this).parent('div').remove();
	});
	
	$('div#arquivo-protocolo').on('show.bs.modal', function (event) {
		var id_protocolo_arquivo = $(event.relatedTarget).data('idarquivo');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido+' ('+no_arquivo+')');
		
		if (id_protocolo_arquivo>0) {
			$.ajax({
				type: "POST",
				url: 'protocolo/arquivo',
				data: 'id_protocolo_arquivo='+id_protocolo_arquivo,
				beforeSend: function() {
					$('div#arquivo-protocolo .modal-body div.carregando').show();
					$('div#arquivo-protocolo .modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$('div#arquivo-protocolo').addClass('total-height');
								resize_modal($('div#arquivo-protocolo'));
							} else {
								$('div#arquivo-protocolo').removeClass('total-height');
								$('div#arquivo-protocolo').find('.modal-body').removeAttr('style');
							}
							
							$('div#arquivo-protocolo .modal-body div.form').html(retorno.view);
							$('div#arquivo-protocolo .modal-body div.carregando').hide();
							$('div#arquivo-protocolo .modal-body div.form').fadeIn('fast');
		
							if (retorno.download=='true') {
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').show();
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').hide();
								$('div#arquivo-protocolo .modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});
});