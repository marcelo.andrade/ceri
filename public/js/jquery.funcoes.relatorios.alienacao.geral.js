$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('form#form-relatorio-alienacao-geral').on('click','input[type=submit]',function(e){
		e.preventDefault();
		swal({
			title: 'Deseja continuar?',
			type: 'warning',
			html: 'A geração deste relatório pode demorar de 1 a 10 minutos, de acordo com as opções selecionadas.',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim'
		}).then(function(retorno) {
			if (retorno==true) {
				$('div#filtro-relatorio form#form-relatorio-alienacao-geral').submit();
                setTimeout(function (){
                	window.location.reload()
                }, 10);
			}
		});
	});

	$('form#form-relatorio-alienacao-geral').on('change','select[name=cidade]',function(e) {
		var id_cidade = $(this).val();
		var form = $('form#form-relatorio-alienacao-geral');

		carrega_serventias(id_cidade,form,'select[name=serventia]');
	});

    $('form#form-relatorio-alienacao-geral').on('change','select.grupo',function(e) {
    	var grupo = $(this).val();

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/filtros-alertas',
            data: {'grupo': grupo},
            success: function (grupos) {
                $('form#form-relatorio-alienacao-geral select.filtro').prop('disabled', false);
                if (!$.isEmptyObject(grupos)) {
                	HTML = '';
                    $.each(grupos, function (key, grupo) {
                    	HTML += '<optgroup label="'+grupo.grupo.titulo+'">';
	                    	$.each(grupo.alertas, function (key, alerta) {
	                        	switch (alerta.alcada) {
	                    			case 2:
	                    				alcada = 'cartorio';
	                    				break;
	                    			case 8:
	                    				alcada = 'cef';
	                    				break;
	                    			default:
	                    				alcada = '';
	                    				break;
	                    		}
	                        	HTML += '<option value="'+grupo.grupo.key+'_'+alerta.key+'" class="'+alcada+'">'+alerta.titulo+'</option>';
	                        });
                        HTML += '</optgroup>';
                    });
                    $('form#form-relatorio-alienacao-geral select.filtro').html(HTML);
                } else {
                    $('form#form-relatorio-alienacao-geral select.filtro').html('').prop('disabled', true);
                }
                $('form#form-relatorio-alienacao-geral select.filtro').selectpicker('refresh');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro X", "error");
            }
        });
    });
});
function carrega_serventias(id_cidade,form,element) {
	$.ajax({
		type: "POST",
		url: url_base+'servicos/listarServentias',
		data: 'id_cidade='+id_cidade,
		beforeSend: function() {
			form.find(element).prop('disabled',true).html('<option value="0">Carregando...</option>');
		},
		success: function(serventias) {
			form.find(element).html('<option value="0">Selecione</option>');
			if (serventias.length>0) {
				$.each(serventias,function(key,serventia) {
					form.find(element).append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
				});
				form.find(element).prop('disabled',false);
			} else {
				form.find(element).prop('disabled',true);
			}
		},
		error: function (request, status, error) {
			//
			swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
				location.reload();
			});
		}
	});
}