$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#nova-vara').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: url_base+'cadastros/varas/nova',
			context: this,
			beforeSend: function() {
				$(this).find('.modal-body div.form form').hide();
				status_modal($(this), true, true, false);
			},
			success: function(retorno) {
				$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				status_modal($(this), false, false, false);
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro CVA501", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});

	$('div#nova-vara').on('click','button.enviar-vara',function(e) {
		var form = $('form[name=form-nova-vara]');
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('select[name=id_comarca]').val()=='0') {
			erros.push('O campo comarca é obrigatório.');
		}
		if (form.find('select[name=id_vara_tipo]').val()=='0') {
			erros.push('O campo tipo de vara é obrigatório.');
		}
		if (form.find('input[name=no_vara]').val()=='') {
			erros.push('O campo nome é obrigatório.');
		}
		if (form.find('input[name=no_email_pessoa]').val()=='') {
			erros.push('O campo e-mail geral da vara é obrigatório.');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'cadastros/varas/inserir',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro CVA502", "error");
				}
			});
		}
	});

	$('div#detalhes-vara').on('show.bs.modal', function (event) {
		var id_vara = $(event.relatedTarget).data('idvara');
		var no_vara = $(event.relatedTarget).data('novara');
		
		$(this).find('.modal-header h4.modal-title span').html(no_vara);
		
		if (id_vara>0) {
			$.ajax({
				type: "POST",
				url: url_base+'cadastros/varas/detalhes',
				data: {'id_vara':id_vara},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro CVA503", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#alterar-vara').on('show.bs.modal', function (event) {
		var id_vara = $(event.relatedTarget).data('idvara');
		var no_vara = $(event.relatedTarget).data('novara');
		
		$(this).find('.modal-header h4.modal-title span').html(no_vara);
		
		if (id_vara>0) {
			$.ajax({
				type: "POST",
				url: url_base+'cadastros/varas/alterar',
				data: {'id_vara':id_vara},
				context: this,
				beforeSend: function() {
					$(this).find('.modal-body div.form form').hide();
					status_modal($(this), true, true, false);
				},
				success: function(retorno) {
					$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
					status_modal($(this), false, false, false);
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro CVA504", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});

	$('div#alterar-vara').on('click','button.salvar-vara',function(e) {
		var form = $('form[name=form-alterar-vara]');
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		status_modal(obj_modal, true, true, true);

		if (form.find('select[name=id_comarca]').val()=='0') {
			erros.push('O campo comarca é obrigatório.');
		}
		if (form.find('select[name=id_vara_tipo]').val()=='0') {
			erros.push('O campo tipo de vara é obrigatório.');
		}
		if (form.find('input[name=no_vara]').val()=='') {
			erros.push('O campo nome é obrigatório.');
		}
		if (erros.length>0) {
			status_modal(obj_modal, false, false, false);
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: url_base+'cadastros/varas/salvar',
				data: form.serialize(),
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					alerta.then(function(){
						status_modal(obj_modal, false, false, false);
						if (retorno.recarrega=='true') {
							location.reload();
						}
					});
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro CVA505", "error");
				}
			});
		}
	});

	$("table#varas").on('click','a.remover-vara',function(e) {
		e.preventDefault();
		var id_vara = $(this).data('idvara');
		var no_vara = $(this).data('novara');
		swal({
			title: 'Aprovar usuário',
			text: 'Deseja realmente remover a vara "'+no_vara+'"?',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'Não',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim',
			showLoaderOnConfirm: true,
		}).then(function( retorno ) {
			if (retorno == true) {
				$.ajax({
					type: "POST",
					url: url_base+'cadastros/varas/remover',
					data: {'id_vara':id_vara},
					success: function(retorno) {
						switch (retorno.status) {
							case 'erro':
								var alerta = swal("Erro!",retorno.msg,"error");
								break;
							case 'sucesso':
								var alerta = swal("Sucesso!",retorno.msg,"success");
								break;
							case 'alerta':
								var alerta = swal("Ops!",retorno.msg,"warning");
								break;
						}
						if (retorno.recarrega=='true') {
							alerta.then(function(){
								location.reload();
							});
						}
					},
					error: function (request, status, error) {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro CVA506", "error").then(function() {
							location.reload();
						});
					}
				});
			}
		}).catch(function(e) {});
	});

});