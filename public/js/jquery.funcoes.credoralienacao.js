$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('div#novo-credor').on('show.bs.modal', function (event) {
		id_cidade = $(event.relatedTarget).data('idcidade');
		$.ajax({
			type: "GET",
			url: url_base+'servicos/alienacao/credor/novo',
			beforeSend: function() {
				$('div#novo-credor .modal-body div.carregando').show();
				$('div#novo-credor .modal-body div.form').hide();
			},
			success: function(retorno) {
				console.log(retorno);
				$('div#novo-credor .modal-body div.form form').html(retorno);
				$('div#novo-credor .modal-body div.carregando').hide();
				$('div#novo-credor .modal-body div.form').fadeIn('fast');
			},
			error: function (request, status, error) {
				console.log(request.responseText);
			}
		});
	});
	$('div#novo-credor').on('hidden.bs.modal', function (event) {
		$('div#novo-credor .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-credor .modal-body div.form form').html('');
	});	
	
	$('div#novo-credor').on('click','button.incluir-credor',function(e) {
		e.preventDefault();
		var form = $('div#novo-credor').find('form[name=form-novo-credor]');
		var erros = new Array();
		
		if (form.find('input[name=nu_cpf_cnpj]').val()=='') {
			erros.push('O campo documento identificador é obrigatório');
		} else if (!validarCNPJ(form.find('input[name=nu_cpf_cnpj]').val())) {
			erros.push('O CNPJ digitado é inválido');
		} else if (total_credores(form.find('input[name=nu_cpf_cnpj]').val())>0) {
			erros.push('A CNPJ digitado já foi cadastrado');
		}
		if (form.find('input[name=no_credor]').val()=='') {
			erros.push('O campo nome do credor é obrigatório');
		}

		if (erros.length>0) {
			form.find('div.erros-credor div').html(erros.join('<br />'));
			if (!form.find('div.erros-credor').is(':visible')) {
				form.find('div.erros-credor').slideDown();
			}
		} else {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/credor/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#novo-credor .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							swal("Sucesso!",retorno.msg,"success").then(function(retorno) {
								retorno_credor();
								$('div#novo-credor').modal('hide');
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}
			});
		}
	});
	$('div#detalhes-credor').on('show.bs.modal', function (event) {
		var id_credor = $(event.relatedTarget).data('idcredor');
		var no_credor = $(event.relatedTarget).data('nocredor');
		
		$(this).find('.modal-header h4.modal-title span').html(no_credor);
		
		if (id_credor>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/credor/detalhes',
				data: 'id_credor='+id_credor,
				beforeSend: function() {
					$('div#detalhes-credor .modal-body div.carregando').show();
					$('div#detalhes-credor .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-credor .modal-body div.form').html(retorno);
					$('div#detalhes-credor .modal-body div.carregando').hide();
					$('div#detalhes-credor .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});