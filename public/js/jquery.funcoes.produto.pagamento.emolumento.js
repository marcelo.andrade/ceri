total_selecionadas=0;
total_valor=0;
$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var form_pgto 		= $('form[name=form-novo-pagamento]');
	var form_comp 		= $('form[name=form-novo-comprovante]');


	$('div#novo-pagamento').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: 'produto-pagamento-emolumento/novo',
			beforeSend: function() {
				$('div#novo-pagamento .modal-body div.carregando').show();
				$('div#novo-pagamento .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#novo-pagamento .modal-body div.form form').html(retorno);
				$('div#novo-pagamento .modal-body div.carregando').hide();
				$('div#novo-pagamento .modal-body div.form form').fadeIn('fast');
				total_selecionadas=0;
			},
			error: function (request, status, error) {
				console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	$('div#novo-pagamento').on('hidden.bs.modal', function (event) {
		$('div#novo-pagamento .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-pagamento .modal-body div.form form').html('');
	});
	
	$('div#novo-pagamento').on('click','input.id_movimentacao_parcela_repasse',function() {
		var id_serventia 		= $(this).data('idserventia');

		var quantidade 			= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.quantidade span');
        var quantidade_geral 	= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.quantidade span');

        var valor_total 		= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.valor-total span');
        var valor_total_geral 	= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.valor-total span');

		if ($(this).is(':checked')) {
			quantidade.html(parseInt(quantidade.html())+1);
            quantidade_geral.html(parseInt(quantidade_geral.html())+1);
			valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))+parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
            valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))+parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
		} else {
			quantidade.html(parseInt(quantidade.html())-1);
            quantidade_geral.html(parseInt(quantidade_geral.html())-1);
			valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))-parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
            valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))-parseFloat(form_pgto.find('input#valor_pedido_'+$(this).val()).val()));
		}
	});

    $('div#detalhes-certidao').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_pedido>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/certidao/detalhes',
                data: 'id_pedido='+id_pedido,
                context: this,
                beforeSend: function() {
                    $(this).find('.modal-body div.form').hide();
                    status_modal($(this), true, true, false);
                },
                success: function(retorno) {
                    $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                    status_modal($(this), false, false, false);
                },
                error: function (request, status, error) {
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro 20504", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });
    $('div#detalhes-certidao').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    $('div#detalhes-pesquisa').on('show.bs.modal', function (event) {
        var id_pedido = $(event.relatedTarget).data('idpedido');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_pedido>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/pesquisa-eletronica/detalhes',
                data: 'id_pedido='+id_pedido,
                context: this,
                beforeSend: function() {
                    $(this).find('.modal-body div.form').hide();
                    status_modal($(this), true, true, false);
                },
                success: function(retorno) {
                    $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                    status_modal($(this), false, false, false);
                },
                error: function (request, status, error) {
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro 10503", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });
    $('div#detalhes-pesquisa').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });



	$('div#novo-pagamento').on('click','input#selecionar_tudo',function() {
		var input_selecionar_tudo = $(this);
		$('div#novo-pagamento table#notificacoes>tbody>tr').each(function(index, element) {
			var id_movimentacao_parcela_repasse 	= $(this).attr('id');
			var id_serventia 						= $('div#novo-pagamento table#notificacoes>tbody>tr input#id_movimentacao_parcela_repasse_'+id_movimentacao_parcela_repasse).data('idserventia');
			var input_alienacao 					= $('div#novo-pagamento table#notificacoes>tbody>tr input#id_movimentacao_parcela_repasse_'+id_movimentacao_parcela_repasse);
			var input_valor 						= $('div#novo-pagamento table#notificacoes>tbody>tr input#valor_pedido_'+id_movimentacao_parcela_repasse);
			var quantidade 							= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.quantidade span');
			var quantidade_geral 					= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.quantidade span');
			var valor_total 						= $('div#novo-pagamento div#serventias-totais div#serventia_'+id_serventia+' div.totais label.valor-total span');
			var valor_total_geral 					= $('div#novo-pagamento div#serventias-total-geral div#serventia-geral div.totais label.valor-total span');

			if (input_selecionar_tudo.is(':checked')) {
				if (!input_alienacao.is(':checked')) {
	            	input_alienacao.prop('checked',true);
					quantidade.html(parseInt(quantidade.html())+1);
                    quantidade_geral.html(parseInt(quantidade_geral.html())+1);
					valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))+parseFloat(input_valor.val()));
                    valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))+parseFloat(input_valor.val()));
				}
			} else {
				if (input_alienacao.is(':checked')) {
					input_alienacao.prop('checked',false);
					quantidade.html(parseInt(quantidade.html())-1);
                    quantidade_geral.html(parseInt(quantidade_geral.html())-1);
					valor_total.autoNumeric('set',parseFloat(valor_total.autoNumeric('get'))-parseFloat(input_valor.val()));
                    valor_total_geral.autoNumeric('set',parseFloat(valor_total_geral.autoNumeric('get'))-parseFloat(input_valor.val()));
				}
			}
        });
	});

	$('div#novo-pagamento').on('click','button.enviar-pagamento',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form_pgto.find('table#notificacoes input.id_movimentacao_parcela_repasse:checked').length<=0) {
			erros.push('Ao menos um produto deve ser selecionado.');
		}
		if (erros.length>0) {
			form_pgto.find('div.erros div').html(erros.join('<br />'));
			if (!form_pgto.find('div.erros').is(':visible')) {
				form_pgto.find('div.erros').slideDown();
			}
		} else {
			form_pgto.find('div.erros').slideUp();
			var data = form_pgto.find('table#notificacoes input.id_movimentacao_parcela_repasse:checked').serialize();
			$.ajax({
				type: "POST",
				url: 'produto-pagamento-emolumento/inserir-pagamento',
				data: data,

				beforeSend: function() {
					$('div#novo-pagamento .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#novo-comprovante').on('show.bs.modal', function (event) {
		var id_pagamento_repasse_pessoa_lote = $(event.relatedTarget).data('idpagamentorepassepessoalote');
		if (id_pagamento_repasse_pessoa_lote>0) {
			$.ajax({
				type: "POST",
				url: 'produto-pagamento-emolumento/novo-comprovante',
				data: 'id_pagamento_repasse_pessoa_lote='+id_pagamento_repasse_pessoa_lote,
				beforeSend: function() {
					$('div#novo-comprovante .modal-body div.carregando').show();
					$('div#novo-comprovante .modal-body div.form form').hide();
				},
				success: function(retorno) {
					$('div#novo-comprovante .modal-body div.form form').html(retorno);
					$('div#novo-comprovante .modal-body div.carregando').hide();
					$('div#novo-comprovante .modal-body div.form form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#novo-comprovante').on('hidden.bs.modal', function (event) {
		$('div#novo-comprovante .modal-body div.carregando').removeClass('flutuante').show();
		$('div#novo-comprovante .modal-body div.form form').html('');
	});
	
	$('div#novo-comprovante').on('click','button.enviar-comprovante',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form_comp.find('input[name=dt_pagamento]').val()=='') {
			erros.push('O campo data do pagamento é obrigatório.');
		}
		if (form_comp.find('input[name=va_pago]').autoNumeric('get')=='' || form_comp.find('input[name=va_pago]').autoNumeric('get')==0) {
			erros.push('O campo valor pago é obrigatório');
		}
		if (form_comp.find('input[name=no_arquivo_comprovante]').val()=='') {
			erros.push('O campo arquivo do comprovante é obrigatório.');
		}
		if (erros.length>0) {
			form_comp.find('div.erros div').html(erros.join('<br />'));
			if (!form_comp.find('div.erros').is(':visible')) {
				form_comp.find('div.erros').slideDown();
			}
		} else {
			form_comp.find('div.erros').slideUp();
			var data = new FormData(form_comp.get(0));
			$.ajax({
				type: "POST",
				url: 'produto-pagamento-emolumento/inserir-comprovante',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div#novo-comprovante .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#novo-comprovante').on('change.bs.fileinput','form[name=form-novo-comprovante] div.arquivo',function(e){
		e.stopPropagation();

		var no_arquivo = $('div#novo-comprovante form[name=form-novo-comprovante] div.arquivo input[name=no_arquivo_comprovante]').val().toLowerCase();
		var no_arquivo = no_arquivo.split('\\');
		var no_arquivo = no_arquivo[no_arquivo.length-1];

		if (!(/\.(pdf|png|jpg|bmp|docx|doc)$/i).test(no_arquivo)) {
			swal("Ops!", "Você deve selecionar um arquivo dos tipos: PDF, PNG, JPG, BMP, DOCX ou DOC.", "warning");
			$('div#novo-comprovante form[name=form-novo-comprovante] div.arquivo').fileinput('clear');
		}
	});
	
	$('div#detalhes-pagamento').on('show.bs.modal', function (event) {
		var id_movimentacao_parcela_repasse_pagamento = $(event.relatedTarget).data('idpagamento');
		
		if (id_movimentacao_parcela_repasse_pagamento>0) {
			$.ajax({
				type: "POST",
				url: 'produto-pagamento-emolumento/detalhes',
				data: 'id_movimentacao_parcela_repasse_pagamento='+id_movimentacao_parcela_repasse_pagamento,
				beforeSend: function() {
					$('div#detalhes-pagamento .modal-body div.carregando').show();
					$('div#detalhes-pagamento .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-pagamento .modal-body div.form').html(retorno);
					$('div#detalhes-pagamento .modal-body div.carregando').hide();
					$('div#detalhes-pagamento .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});


    $('div#detalhe-historico-pagamento').on('show.bs.modal', function (event) {
        var id_pagamento_repasse_pessoa_lote = $(event.relatedTarget).data('idpagamentorepassepessoalote');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        if (id_pagamento_repasse_pessoa_lote>0)
        {
            $.ajax({
                type: "POST",
                url: 'produto-pagamento-emolumento/detalhes',
                data: 'id_pagamento_repasse_pessoa_lote='+id_pagamento_repasse_pessoa_lote,
                beforeSend: function () {
                    $('div#detalhe-historico-pagamento .modal-body div.carregando').show();
                    $('div#detalhe-historico-pagamento .modal-body div.form').hide();
                },
                success: function (retorno) {
                    $('div#detalhe-historico-pagamento .modal-body div.form ').html(retorno);
                    $('div#detalhe-historico-pagamento .modal-body div.carregando').hide();
                    $('div#detalhe-historico-pagamento .modal-body div.form ').fadeIn('fast');
                    total_selecionadas = 0;
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                        location.reload();
                    });
                }
            });
        }
    });

    $('div#detalhe-historico-pagamento').on('hidden.bs.modal', function (event) {
        $('div#detalhe-historico-pagamento .modal-body div.carregando').removeClass('flutuante').show();
        $('div#detalhe-historico-pagamento .modal-body div.form form').html('');
    });

    $('div#detalhes-pagamento').on('hidden.bs.modal', function () {
        window.location.reload(true);
    })
	

	
	$('div#arquivo-comprovante').on('show.bs.modal', function (event) {
		var id_pagamento_repasse_pessoa_lote = $(event.relatedTarget).data('idpagamentorepassepessoalote');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		var no_extensao = $(event.relatedTarget).data('noextensao');

		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);

		if (id_pagamento_repasse_pessoa_lote>0) {
			$.ajax({
				type: "POST",
				url: 'produto-pagamento-emolumento/comprovante',
				data: 'id_pagamento_repasse_pessoa_lote='+id_pagamento_repasse_pessoa_lote,
				beforeSend: function() {
					$('div#arquivo-comprovante .modal-body div.carregando').show();
					$('div#arquivo-comprovante .modal-body div.form').hide();
				},
				success: function(retorno) {
					switch (retorno.status) {
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							if (no_extensao=='pdf') {
								$('div#arquivo-comprovante').addClass('total-height');
								resize_modal($('div#arquivo-comprovante'));
							} else {
								$('div#arquivo-comprovante').removeClass('total-height');
								$('div#arquivo-comprovante').find('.modal-body').removeAttr('style');
							}

							$('div#arquivo-comprovante .modal-body div.form').html(retorno.view);
							$('div#arquivo-comprovante .modal-body div.carregando').hide();
							$('div#arquivo-comprovante .modal-body div.form').fadeIn('fast');

							if (retorno.download=='true') {
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').show();
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').attr('href',retorno.url_download);
							} else {
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').hide();
								$('div#arquivo-comprovante .modal-footer a#arquivo-download').attr('href','javascript:void(0);');
							}
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						//location.reload();
					});
				}
			});
		}
	});
	
	$('table#pedidos-pendentes').on('click','a.aprovar-pagamento',function(e) {
		e.preventDefault();
		var id_pagamento_repasse_pessoa_lote = $(this).data('idpagamentorepassepessoalote');
		var protocolo 						 = $(this).data('protocolo');
		if (id_pagamento_repasse_pessoa_lote>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja aprovar o pagamento '+protocolo+'?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: 'produto-pagamento-emolumento/aprovar-pagamento',
						data: 'id_pagamento_repasse_pessoa_lote='+id_pagamento_repasse_pessoa_lote,
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									var alerta = swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									var alerta = swal("Sucesso!",retorno.msg,"success");
									break;
								case 'alerta':
									var alerta = swal("Ops!",retorno.msg,"warning");
									break;
							}
							if (retorno.recarrega=='true') {
								alerta.then(function(){
									location.reload();
								});
							}
						},
						error: function (request, status, error) {
							console.log(request.responseText);
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

	$('table#pedidos-pendentes').on('click','a.reprovar-pagamento',function(e) {
		e.preventDefault();
		var id_movimentacao_parcela_repasse_pagamento = $(this).data('idpagamento');
		if (id_movimentacao_parcela_repasse_pagamento>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja reprovar o pagamento selecionado?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: 'produto-pagamento-emolumento/reprovar-pagamento',
						data: 'id_movimentacao_parcela_repasse_pagamento='+id_movimentacao_parcela_repasse_pagamento,
						success: function(retorno) {
							switch (retorno.status) {
								case 'erro':
									var alerta = swal("Erro!",retorno.msg,"error");
									break;
								case 'sucesso':
									var alerta = swal("Sucesso!",retorno.msg,"success");
									break;
								case 'alerta':
									var alerta = swal("Ops!",retorno.msg,"warning");
									break;
							}
							if (retorno.recarrega=='true') {
								alerta.then(function(){
									location.reload();
								});
							}
						},
						error: function (request, status, error) {
							console.log(request.responseText);
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
								location.reload();
							});
						}
					});
				}
			});
		}
	});

    $('div#novo-pagamento').on('change','select.cidade',function() {
    	var id_cidade = $(this).val();
        $.ajax({
            type: "POST",
            url: 'listarServentias',
            data: 'id_cidade='+id_cidade,
            beforeSend: function() {
                form_pgto.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
            },
            success: function(serventias) {
                form_pgto.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
                if (serventias.length>0) {
                    $.each(serventias,function(key,serventia) {
                        form_pgto.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                    });
                    form_pgto.find('select[name=id_serventia]').prop('disabled',false);
                } else {
                    form_pgto.find('select[name=id_serventia]').prop('disabled',true);
                }
            },
            error: function (request, status, error) {
                //console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });

    });

    $('div#novo-pagamento').on('click','input.limpar-formulario',function(e) {
        setTimeout(function(){
            $.ajax({
                type: "POST",
                url: 'produto-pagamento-emolumento/novo',
                beforeSend: function() {
                    $('div#novo-pagamento .modal-body div.carregando').show();
                    $('div#novo-pagamento .modal-body div.form form').hide();
                },
                success: function(retorno) {
                    $('div#novo-pagamento .modal-body div.form form').html(retorno);
                    $('div#novo-pagamento .modal-body div.carregando').hide();
                    $('div#novo-pagamento .modal-body div.form form').fadeIn('fast');
                    total_selecionadas=0;
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                        location.reload();
                    });
                }
            });
        }, 500);

    });


    $('div#novo-pagamento').on('click','input.filtrar-pagamentos',function() {
        $.ajax({
            type: "POST",
            url: 'produto-pagamento-emolumento/novo',
            data: form_pgto.serialize(),
            beforeSend: function() {
                $('div#novo-pagamento .modal-body div.carregando').addClass('flutuante').show();
                form_pgto.find('div#resultadoPesquisaPagamento').hide();
            },
            success: function(retorno) {
                form_pgto.find('div#resultadoPesquisaPagamento').html( retorno );
                $('div#novo-pagamento .modal-body div.carregando').hide();
                form_pgto.find('div#resultadoPesquisaPagamento').fadeIn('fast');
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });

    });

    $('div#detalhes-custas').on('show.bs.modal', function (event) {
        var id_movimentacao_parcela_repasse = $(event.relatedTarget).data('idalienacao');
        var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

        $.ajax({
            type: "POST",
            url: 'alienacao/detalhes-custas',
            data: 'id_movimentacao_parcela_repasse='+id_movimentacao_parcela_repasse,
            beforeSend: function() {
                $('div#detalhes-custas .modal-body div.carregando').show();
                $('div#detalhes-custas .modal-body div.form').hide();
            },
            success: function(retorno) {
                $('div#detalhes-custas .modal-body div.form').html(retorno);
                $('div#detalhes-custas .modal-body div.carregando').hide();
                $('div#detalhes-custas .modal-body div.form').fadeIn('fast');
            },
            error: function (request, status, error) {
                console.log(request.responseText);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#imprimir-recibo-pagamento').on('show.bs.modal', function (event) {
        var protocolo = $(event.relatedTarget).data('protocolo');
        var id_pagamento_repasse_pessoa_lote = $(event.relatedTarget).data('idpagamentorepassepessoalote');
        $(this).find('.modal-header h4.modal-title span').html(protocolo);
        $.ajax({
            type: "POST",
            url: url_base+'servicos/produto-pagamento-emolumento/gerar-recibo',
            context: this,
            data: {'id_pagamento_repasse_pessoa_lote':id_pagamento_repasse_pessoa_lote},
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($(this));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno.view).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

    $('form#form-filtro-pagamentos').on('change','select[name=cidade_pagamentos]',function(e) {
        var id_cidade = $(this).val();
        var form = $('form#form-filtro-pagamentos');

        carrega_serventias(id_cidade,form,'select[name=serventia_pagamentos]');
    });

    $('div#novo-arquivo').on('show.bs.modal', function (event) {

        var id_pagamento_repasse_pessoa_lote = $(event.relatedTarget).data('idpagamentorepassepessoalote');
        var no_arquivo_comprovante = $(event.relatedTarget).data('noarquivo');
        var no_extensao_comprovante = $(event.relatedTarget).data('noextensao');
        var token = $(event.relatedTarget).data('token');

        $.ajax({
            type: "POST",
            url: url_base+'servicos/produto-pagamento-emolumento/novo-arquivo-comprovante',
            data: {'id_pagamento_repasse_pessoa_lote':id_pagamento_repasse_pessoa_lote,'no_arquivo_comprovante': no_arquivo_comprovante, 'no_extensao_comprovante':no_extensao_comprovante, 'token':token},
            context: this,
            beforeSend: function() {
                status_modal($(this),true,true,false);
                $(this).find('.modal-body div.form form').hide();
            },
            success: function(retorno) {
                status_modal($(this),false,false,false);
                $(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ501", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });

    });

    $('div#novo-arquivo').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form form').html('');
    });

    $('form[name=form-novo-arquivo]').on('change','label.arquivo-upload input[type=file]',function(e) {
        if (!$(this).val() == '') {
            var form = $(this).closest('form');
            var id_pagamento_repasse_pessoa_lote = $(".idPagamento").data('idpagamentorepassepessoalote');
            var no_arquivo = $(this).val().split('\\');
            var no_arquivo = remove_caracteres(no_arquivo[no_arquivo.length-1]);
            var ext_arquivo = no_arquivo.split('.');
            var ext_arquivo = ext_arquivo[ext_arquivo.length-1];
            var ext_permitidas = ['pdf','png','jpg','bmp','tif','doc','docx','xls','xlsx','txt','ppt','pptx','pps','ppsx'];

            $("#id_pagamento_repasse_pessoa_lote").val(id_pagamento_repasse_pessoa_lote);
            if (ext_permitidas.indexOf(ext_arquivo.toLowerCase())<0) {
                swal("Ops!", "O arquivo selecionado é inválido, tente novamente.", "warning");
                $(this).replaceWith($(this).val('').clone(true));
                return false;
            } else {
                form.find('div.msg-arquivo').slideUp();
                form.find('div.erros').slideUp();
                form.find('label.arquivo-upload h4').html(remove_caracteres(no_arquivo).toLowerCase());
                form.find('label.arquivo-upload h4').prop('title',remove_caracteres(no_arquivo).toLowerCase());
                switch (ext_arquivo) {
                    case 'pdf':
                        var icone_arquivo = '<i class="fa fa-file-pdf-o"></i>';
                        break;
                    case 'png': case 'jpg': case 'bmp': case 'tif':
                    var icone_arquivo = '<i class="fa fa-file-image-o"></i>';
                    form.find('div.msg-arquivo').slideDown();
                    form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
                    break;
                    case 'doc': case 'docx':
                    var icone_arquivo = '<i class="fa fa-file-word-o"></i>';
                    form.find('div.msg-arquivo').slideDown();
                    form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
                    break;
                    case 'xls': case 'xlsx':
                    var icone_arquivo = '<i class="fa fa-file-excel-o"></i>';
                    form.find('div.msg-arquivo').slideDown();
                    form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
                    break;
                    case 'txt':
                        var icone_arquivo = '<i class="fa fa-file-text-o"></i>';
                        form.find('div.msg-arquivo').slideDown();
                        form.find('div.msg-arquivo div.menssagem').html('O arquivo será convertido para <b>PDF</b> automaticamente.');
                        break;
                    case 'ppt': case 'pptx': case 'pps': case 'ppsx':
                    var icone_arquivo = '<i class="fa fa-file-powerpoint-o"></i>';
                    break;
                    default:
                        var icone_arquivo = '<i class="fa fa-file-o"></i>';
                        break;
                }
                form.find('label.arquivo-upload figure').html(icone_arquivo);
            }
        }
    });

    $('div#novo-arquivo').on('click','button.enviar-arquivo', function (e) {
        var form = $('form[name=form-novo-arquivo]');
        var erros = new Array();
        var obj_modal = $(this).closest('.modal');

        var token = $("#token").val();
        var id_pagamento_repasse_pessoa_lote = $("#id_pagamento_repasse_pessoa_lote").val();
        var no_arquivo_comprovante = $("#no_arquivo_comprovante").val();
        var no_extensao_comprovante = $("#no_extensao_comprovante").val();

        if(form.find('input#arquivo-upload').val()==''){
            erros.push('Selecione um arquivo.');
        }
        if(erros.length>0){
            form.find('div.erros div').html(erros.join('<br />'));
            if(!form.find('div.erros').is(':visible')){
                form.find('div.erros').slideDown();
            }
        } else {
            form.find('div.erros').slideUp();
            var data =  new FormData(form.get(0));
            $.ajax({
                url: url_base+'servicos/produto-pagamento-emolumento/alterar-comprovante',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    status_modal(obj_modal, true, false, false)
                    form.find('label.arquivo-upload').hide();
                    form.find('div.progresso-upload').show();

                },
                success:function (retorno) {
                    status_modal(obj_modal, false, false, false);
                    retorno_arquivo(retorno);
                },
                error: function (request, status, error) {
                    status_modal(obj_modal, false, false, false);
                    swal("Erro!", "Por favor, temte novamente mais tarde. Eroo ARQ502", "error");
                },
                cache: false,
                contentType: false,
                processData: false,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', function (e) {
                            if (e.lengthComputable) {
                                var porcentagem = e.loaded / e.total;
                                porcentagem = parseInt(porcentagem * 100);

                                form.find('div.progresso-upload div.progress-bar').css('width',porcentagem+'%');

                                if (porcentagem >= 100) {
                                    status_modal(obj_modal, true, true, false);
                                    form.find('div.progresso-upload').hide();
                                } else if (porcentagem > 10) {
                                    form.find('div.progresso-upload div.progress-bar').html(porcentagem+'%');
                                }
                            }
                        }, false);
                    }
                    return myXhr;
                }
            });
        }

    });

    $('body').on('click','button.remover-comprovante',function(e) {
        e.preventDefault();
        var token = $(this).data('token');
        var id_pagamento_repasse_pessoa_lote = $(this).data('idpagamentorepassepessoalote');
        var button = $(this);

        button.removeClass('remover-comprovante');

        $.ajax({
            type: "POST",
            url: url_base+'servicos/produto-pagamento-emolumento/remover-comprovante',
            data: {'token':token,'id_pagamento_repasse_pessoa_lote':id_pagamento_repasse_pessoa_lote},
            success: function(retorno) {
                switch (retorno.status) {
                    case 'erro':
                        swal("Erro!",retorno.msg,"error");
                        break;
                    case 'sucesso':
                        swal("Sucesso!",retorno.msg,"success");
                        $('#comprovante').remove();
                        $('#adicionar-comprovante').attr("style","display:block");
                        break;
                }
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ503", "error");
                button.addClass('remover-comprovante');
            }
        });
    });

    $('div#detalhes-lote-pagamentos').on('show.bs.modal', function (event) {
        var id_pagamento_repasse_lote = $(event.relatedTarget).data('idpagamentorepasselote');
    	var protocolo_pedido = $(event.relatedTarget).data('protocolo');

        $(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);

			if (id_pagamento_repasse_lote > 0) {
            $.ajax({
                type: "POST",
                url: 'produto-pagamento-emolumento/detalhes-lote-pagamentos',
                data:{'id_pagamento_repasse_lote':id_pagamento_repasse_lote},
                beforeSend: function () {
                    $('div#detalhes-lote-pagamentos .modal-body div.carregando').show();
                    $('div#detalhes-lote-pagamentos .modal-body div.form').hide();
                },
                success: function (retorno) {
                    $('div#detalhes-lote-pagamentos .modal-body div.form').html(retorno);
                    $('div#detalhes-lote-pagamentos .modal-body div.carregando').hide();
                    $('div#detalhes-lote-pagamentos .modal-body div.form').fadeIn('fast');
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function () {
                        location.reload();
                    });
                }
            });
        }
    });

    $('div#imprimir-recibo-lote-pagamentos').on('show.bs.modal', function (event) {
        var protocolo = $(event.relatedTarget).data('protocolo');
        var id_pagamento_repasse_lote = $(event.relatedTarget).data('idpagamentorepasselote');

        $(this).find('.modal-header h4.modal-title span').html(protocolo);

        $.ajax({
            type: "POST",
            url: url_base+'servicos/produto-pagamento-emolumento/gerar-recibo-lote',
            context: this,
            data: {'id_pagamento_repasse_lote':id_pagamento_repasse_lote},
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).addClass('total-height');
                resize_modal($(this));
                status_modal($(this), false, false, false);
                $(this).find('.modal-body div.form').addClass('carregando').html(retorno).fadeIn('fast');
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro 90513", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

});

function carrega_serventias(id_cidade,form,element) {
    $.ajax({
        type: "POST",
        url: 'listarServentias',
        data: 'id_cidade='+id_cidade,
        beforeSend: function() {
            form.find(element).prop('disabled',true).html('<option value="0">Carregando...</option>');
        },
        success: function(serventias) {
            form.find(element).html('<option value="0">Selecione</option>');
            if (serventias.length>0) {
                $.each(serventias,function(key,serventia) {
                    form.find(element).append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
                });
                form.find(element).prop('disabled',false);
            } else {
                form.find(element).prop('disabled',true);
            }
        },
        error: function (request, status, error) {
            //console.log(request.responseText);
            swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                location.reload();
            });
        }
    });
}
function status_modal(obj_modal,botoes,carregando,carregando_flutuante) {
    switch (botoes) {
        case false:
            obj_modal.find('.modal-header button').removeClass('disabled').prop('disabled',false);
            obj_modal.find('.modal-footer button').removeClass('disabled').prop('disabled',false);
            break;
        case true:
            obj_modal.find('.modal-header button').addClass('disabled').prop('disabled',true);
            obj_modal.find('.modal-footer button').addClass('disabled').prop('disabled',true);
            break;
    }
    switch (carregando) {
        case false:
            obj_modal.find('.modal-body .carregando:not(.form)').removeClass('flutuante').hide();
            break;
        case true:
            if (carregando_flutuante) {
                obj_modal.find('.modal-body .carregando:not(.form)').addClass('flutuante');
            }
            obj_modal.find('.modal-body .carregando:not(.form)').show();
            break;
    }
}
function retorno_arquivo(retorno) {
    $('div#arquivos').html('');
    botao_arquivo = '<label class="small">Arquivo do comprovante</label><br />';
    botao_arquivo += '<div class="btn-group" id="comprovante">';
    botao_arquivo += '<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#arquivo-comprovante"';
    botao_arquivo += 'data-idpagamentorepassepessoalote="'+retorno.id_pagamento_repasse_pessoa_lote+'"';
    botao_arquivo += 'data-noarquivo="'+retorno.no_arquivo_comprovante+'"';
    botao_arquivo += 'data-noextensao="'+retorno.no_extensao_comprovante+'">'+retorno.no_arquivo_comprovante+'</a>';
    botao_arquivo += '<button type="button" class="remover-comprovante btn btn-danger " data-linha="0" data-token="'+retorno.token+'" data-idpagamentorepassepessoalote="'+retorno.id_pagamento_repasse_pessoa_lote+'"><i class="fa fa-times"></i></button>';
    botao_arquivo += '</div>';
    $('div#novo-arquivo').modal('hide');
    $('button#adicionar-comprovante').attr('style','display:none');
    obj_arquivos = $('form[name=form-arquivo] div#arquivos');
    obj_arquivos.prepend(botao_arquivo);
}