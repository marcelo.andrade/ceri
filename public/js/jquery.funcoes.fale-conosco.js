$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var form = $('form[name=form-contato]');
    
    form.on('submit',function(e) {
		e.preventDefault();
        var erros = new Array();

        if (form.find('input[name=nome]').val()=='') {
            erros.push('O campo nome completo é obrigatório.');
        }
        if (form.find('input[name=email]').val()=='') {
            erros.push('O campo e-mail é obrigatório.');
        }
        if (form.find('select[name=area]').val()==0) {
            erros.push('O campo área é obrigatório.');
        }
        if (form.find('input[name=titulo]').val()=='') {
            erros.push('O campo assunto é obrigatório.');
        }
        if (form.find('textarea[name=descricao]').val()=='') {
            erros.push('O campo descrição é obrigatório.');
        }

        if (form.find('input[name=hidden_captcha_contato]').val()==''){
            erros.push('reCaptcha não validado. Por favor marque a opção.');
        }

        if (erros.length>0) {
            console.log(erros);
            form.find('div.erros div').html(erros.join('<br />'));
            if (!form.find('div.erros').is(':visible')) {
                form.find('div.erros').slideDown();
            }
            return false;
        } else {
            form.find('div.erros').slideUp();
            $.ajax({
                type: "POST",
                url:'fale-conosco/enviar',
                data: form.serialize(),
                beforeSend: function() {
                    form.find('div.carregando').show().addClass('flutuante').show();
                },
                success: function(retorno) {
                    form.find('div.carregando').show().addClass('flutuante').hide();
                    switch (retorno.status) {
                        case 'erro':
                            swal("Erro!", retorno.msg, "error");
                            break;
                        case 'sucesso':
                            swal("Sucesso!", retorno.msg, "success").then(function (retorno) {
                                location.reload();
                            });
                            break;
                    }
                },
                error: function (request, status, error) {
                    form.find('div.carregando').show().addClass('flutuante').hide();
                    console.log(request.responseText);
                }
            });
        }
    });
});