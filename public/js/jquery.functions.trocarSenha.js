$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});



	$('.validarSenha').on('click',function(){

	var erros = new Array();
	var emailR = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

	if ($('input[name=email_usuario]').val()=='') {
		erros.push('O campo e-mail é obrigatório.');
	}

	if ($('input[name=senha_usuario]').val()=='') {
		erros.push('O campo senha é obrigatório.');
	} else if ($('input[name=senha_usuario]').val().length<6) {
		erros.push('A senha deve conter no mínimo 6 caracteres.');
	}
	if ($('input[name=senha_usuario2]').val()=='') {
		erros.push('O campo confirme a senha é obrigatório.');
	} else if ($('input[name=senha_usuario2]').val()!=$('input[name=senha_usuario]').val()) {
		erros.push('A confirmação deve ser identica a senha.');
	}

	if (erros.length>0) {
		$('div.erros div').html(erros.join('<br />'));
		if (!$('div.erros').is(':visible')) {
			$('div.erros').slideDown();
		}
		return false;
	} else {
		$('div.erros').slideUp();
		return true;
	}
	});
});