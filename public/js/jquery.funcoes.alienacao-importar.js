total_arquivos = 0;
total_arquivos_assinaveis = 0;
index_arquivos = 0;

$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});


	$('div#novo-arquivo').on('show.bs.modal', function (event) {
		var id_tipo_arquivo_grupo_produto = $(event.relatedTarget).data('idtipoarquivo');
		var limite = $(event.relatedTarget).data('limite');
		var token = $(event.relatedTarget).data('token');
		var id_flex = $(event.relatedTarget).data('idflex')?$(event.relatedTarget).data('idflex'):0;

		if (total_arquivo(id_tipo_arquivo_grupo_produto,'',id_flex)<parseInt(limite) || limite=='0') {
			$.ajax({
				type: "POST",
				url: url_base+'arquivos/novo',
				data: {'id_tipo_arquivo_grupo_produto':id_tipo_arquivo_grupo_produto,'token':token,'id_flex':id_flex,'index_arquivos':index_arquivos},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form form').hide();
				},
				success: function(retorno) {
					status_modal($(this),false,false,false);
					$(this).find('.modal-body div.form form').html(retorno).fadeIn('fast');
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ501", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		} else {
			swal("Ops!", "Você alcançou o limite de arquivo(s) para esta seção. Limite: "+limite+".", "warning");
			return false;
		}
	});

	$('div#novo-arquivo').on('hidden.bs.modal', function (event) {
		status_modal($(this), false, false, false);
		$(this).find('.modal-body div.form form').html('');
	});

	$('form[name=form-novo-arquivo]').on('change','label.arquivo-upload input[type=file]',function(e) {
		if (!$(this).val() == '') {
			var form = $(this).closest('form');

			var id_tipo_arquivo_grupo_produto = $(this).data('idtipoarquivo');
			var id_flex = $(this).data('idflex');

			var no_arquivo = $(this).val().split('\\');
			var no_arquivo = remove_caracteres(no_arquivo[no_arquivo.length-1]);
			var ext_arquivo = no_arquivo.split('.');
			var ext_arquivo = ext_arquivo[ext_arquivo.length-1];
			var ext_permitidas = extensoes_permitidas(id_tipo_arquivo_grupo_produto);
			if (ext_permitidas.indexOf(ext_arquivo.toLowerCase())<0) {
				swal("Ops!", "O arquivo selecionado é inválido, tente novamente.", "warning");
				$(this).replaceWith($(this).val('').clone(true));
				return false;
			} else if (total_arquivo(id_tipo_arquivo_grupo_produto,remove_caracteres(no_arquivo).toLowerCase(),id_flex)>0) {
				swal("Ops!", "O arquivo selecionado já foi inserido.", "warning");
				$(this).replaceWith($(this).val('').clone(true));
				return false;
			} else {
				form.find('div.msg-arquivo').slideUp();
				form.find('div.erros').slideUp();
				form.find('label.arquivo-upload h4').html(remove_caracteres(no_arquivo).toLowerCase());
				form.find('label.arquivo-upload h4').prop('title',remove_caracteres(no_arquivo).toLowerCase());
				var icone_arquivo = '<i class="fa fa-file-o"></i>';
				form.find('label.arquivo-upload figure').html(icone_arquivo);
			}
		}
	});


	$('div#novo-arquivo').on('click','button.enviar-arquivo', function(e) {

		var form = $('form[name=form-novo-arquivo]');
		var erros = new Array();
		var obj_modal = $(this).closest('.modal');

		if (form.find('input#arquivo-upload').val()=='') {
			erros.push('Selecione um arquivo.');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));

			$.ajax({
				url: url_base+'servicos/alienacao/alienacao-importar',
				type: 'POST',
				data: data,
				beforeSend: function() {
					status_modal(obj_modal, true, false, false);
					form.find('label.arquivo-upload').hide();
					form.find('div.progresso-upload').show();
				},
				success: function(retorno) {
					status_modal(obj_modal, false, false, false);
					switch (retorno.status)
					{
						case 'erro':
							var alerta = swal("Erro!",retorno.msg,"error");
							break;
						case 'sucesso':
							retorno_arquivo(retorno);
							var alerta = swal("Sucesso!",retorno.msg,"success");
							break;
						case 'alerta':
							var alerta = swal("Ops!",retorno.msg,"warning");
							break;
					}
					if (retorno.recarrega=='true') {
						alerta.then(function(){
							location.reload();
						});
					}
				},
				error: function (request, status, error) {
					status_modal(obj_modal, false, false, false);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ502", "error");
				},
				cache: false,
				contentType: false,
				processData: false,
				xhr: function() {
					var myXhr = $.ajaxSettings.xhr();
					if (myXhr.upload) {
						myXhr.upload.addEventListener('progress', function (e) {
							if (e.lengthComputable) {
								var porcentagem = e.loaded / e.total;
								porcentagem = parseInt(porcentagem * 100);

								form.find('div.progresso-upload div.progress-bar').css('width',porcentagem+'%');

								if (porcentagem >= 100) {
									status_modal(obj_modal, true, true, false);
									form.find('div.progresso-upload').hide();
								} else if (porcentagem > 10) {
									form.find('div.progresso-upload div.progress-bar').html(porcentagem+'%');
								}
							}
						}, false);
					}
					return myXhr;
				}
			});
		}
	});


	$('body').on('click','button.remover-arquivo',function(e) {
		e.preventDefault();
		var linha = $(this).data('linha');
		var token = $(this).data('token');

		swal("Sucesso!",retorno.msg,"success").then(function() {
			remove_arquivo(linha,retorno.arquivo);
		});

		/*$.ajax({
			type: "POST",
			url: url_base+'arquivos/remover',
			data: {'token':token,'linha':linha},
			success: function(retorno) {
				switch (retorno.status) {
					case 'erro':
						swal("Erro!",retorno.msg,"error");
						break;
					case 'sucesso':
						swal("Sucesso!",retorno.msg,"success").then(function() {
							remove_arquivo(linha,retorno.arquivo);
						});
						break;
				}

			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ503", "error");
			}
		});*/
	});

	$('div#assinar-xml').on('show.bs.modal', function (event) {
		var id_alienacao_arquivo_xml = $(event.relatedTarget).data('idarquivo');
		
		$.ajax({
			type: "POST",
			url: url_base+'servicos/alienacao/alienacao-importar/assinar-xml',
			data: {'id_alienacao_arquivo_xml':id_alienacao_arquivo_xml},
			context: this,
			beforeSend: function() {
				status_modal($(this),true,true,false);
				$(this).find('.modal-body div.form').hide();
			},
			success: function(retorno) {
				status_modal($(this),false,false,false);
				$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
			},
			error: function (request, status, error) {
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ504", "error").then(function() {
					$(this).modal('hide');
				});
			}
		});
	});
	$('div#assinar-xml').on('hidden.bs.modal', function (event) {
		location.reload();
	});

	$('div#visualizar-assinatura').on('show.bs.modal', function (event) {
		var id_alienacao_arquivo_xml = $(event.relatedTarget).data('idarquivo');
		var no_arquivo = $(event.relatedTarget).data('noarquivo');
		
		$(this).find('.modal-header h4.modal-title span').html(no_arquivo);
		
		if (id_alienacao_arquivo_xml>0) {
			$.ajax({
				type: "POST",
				url: url_base+'servicos/alienacao/alienacao-importar/visualizar-assinatura-xml',
				data: {'id_alienacao_arquivo_xml':id_alienacao_arquivo_xml},
				context: this,
				beforeSend: function() {
					status_modal($(this),true,true,false);
					$(this).find('.modal-body div.form').hide();
				},
				success: function(retorno) {
					status_modal($(this),false,false,false);
					$(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
				},
				error: function (request, status, error) {
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ506", "error").then(function() {
						$(this).modal('hide');
					});
				}
			});
		}
	});


////////////////////////////////////////////////////////////
// Ações									              //
////////////////////////////////////////////////////////////

    $('table#pedidos-pendentes').on('click','input#selecionar-todas',function(e) {
        if ($(this).is(':checked')) {
            $('table#pedidos-pendentes input.alienacao:not(:checked)').trigger('click');
        } else {
            $('table#pedidos-pendentes input.alienacao:checked').trigger('click');
        }
    });

    $('table#pedidos-pendentes').on('click','input.alienacao',function(e) {
        var total = $('table#pedidos-pendentes input.alienacao:checked').length;

        if (total<=0) {
            $('div#alert span.total').html('0 lotes selecionadas');
            $('div#alert button').prop('disabled',true).addClass('disabled');

            $('div#alert ul.dropdown-menu a.gerar-relatorio').parent('li').addClass('disabled');
            $('div#alert ul.dropdown-menu a.gerar-relatorio span').html('');
        } else {
            $('div#alert span.total').html((total==1?'1 lote selecionada':total+' lotes selecionadas'));
            $('div#alert button').prop('disabled',false).removeClass('disabled');

            $('div#alert ul.dropdown-menu a.gerar-relatorio').parent('li').removeClass('disabled');
            $('div#alert ul.dropdown-menu a.gerar-relatorio span').html('('+total+')');
        }
    });

    $('div#relatorio-alienacao').on('show.bs.modal', function (event) {
        var ids_alienacao_arquivo_xml = [];

        $('table#pedidos-pendentes input.alienacao:checked').each(function(e) {
            ids_alienacao_arquivo_xml.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/relatorio',
            data: {'ids_alienacao_arquivo_xml':ids_alienacao_arquivo_xml},
            context: this,
            beforeSend: function() {
                $(this).find('.modal-body div.form').hide();
                status_modal($(this), true, true, false);
            },
            success: function(retorno) {
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                status_modal($(this), false, false, false);
            },
            error: function (request, status, error) {
                status_modal($(this), false, false, false);
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
                    location.reload();
                });
            }
        });
    });

    $('div#relatorio-alienacao').on('hidden.bs.modal', function (event) {
        status_modal($(this), false, false, false);
        $(this).find('.modal-body div.form').html('');
    });

    $('div#relatorio-alienacao').on('click','button.salvar-relatorio', function (event) {
        $('form[name=form-salvar-relatorio]').submit();
        $('div#relatorio-alienacao').modal('hide');
    });


});


function extensoes_permitidas(tipo_arquivo) {
	var permitidas = ['xml'];
	return permitidas;
}



// Tratamento de retornos dos arquivos
function retorno_arquivo(retorno) {
	botao_arquivo = '<div class="arquivo btn-group" id="'+index_arquivos+'" >';
	botao_arquivo += '<button type="button" class="btn btn-primary">'+retorno.arquivo.no_arquivo+'</button>';
	botao_arquivo += '<input type="hidden" name="no_arquivo[]" value="'+retorno.arquivo.no_arquivo+'" />';
	//botao_arquivo += '<button type="button" class="remover-arquivo btn btn-danger" data-linha="'+index_arquivos+'" data-token="'+retorno.token+'"><i class="fa fa-times"></i></button>';
	botao_arquivo += '</div> ';
	$('div#novo-arquivo').modal('hide');
	total_arquivos++;

	index_arquivos++;

	$('div#arquivos-gerais').prepend(botao_arquivo);
	if (total_arquivos==1)
	{
		$('div#assinaturas-arquivos div.menssagem span').html('<b>'+total_arquivos+'</b> arquivo foi inserido');
	}

}

function total_arquivo(no_arquivo) {
	var total_arquivo = 0;
	if (no_arquivo!='') {
		var total_arquivo = $('div#arquivos-gerais').find('input[value="'+no_arquivo+'"]').length;
	} else {
		var total_arquivo = $('form[name=form-novo-oficio] div#arquivos-gerais').find('div.arquivo').length;
	}
	return total_arquivo;
}


function remove_arquivo(linha)
{
	$('div#arquivos-gerais').find('div#'+linha).remove();

	total_arquivos--;

	if (total_arquivos==0)
	{
		$('div#assinaturas-arquivos div.menssagem span').html('Nenhum arquivo foi inserido.');
		$('div#assinaturas-arquivos div.menssagem a').addClass('disabled');
	}
}

function retorno_assinatura(sucessos,falhas) {
	location.reload();
}
