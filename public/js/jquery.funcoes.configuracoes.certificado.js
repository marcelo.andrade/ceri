$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	form = $('form[name=form-certificado]');

	form.on('change.bs.fileinput','div.certificado',function(e){
		e.stopPropagation();
		var no_arquivo = form.find('div.certificado input[name=no_arquivo]').val().toLowerCase()
		var no_arquivo = no_arquivo.split('\\');
		if (!(/\.(pfx)$/i).test(no_arquivo)) {
			swal("Ops!", "Você deve selecionar um arquivo do tipo PFX.", "warning");
			$('form[name=form-certificado] div.certificado').fileinput('clear');
		}
	});

	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('input[name=no_arquivo]').val()=='') {
			erros.push('O campo arquivo é obrigatório');
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			var data = new FormData(form.get(0));
			$.ajax({
				type: "POST",
				url: 'certificado/salvar',
				data: data,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('div.configuracoes .content div.carregando').show();
				},
				success: function(retorno) {
					console.log(retorno);
					$('div.configuracoes .content div.carregando').hide();
					
					switch (retorno) {
						case 'ERRO_01':
							swal("Erro!", "O certificado selecionado já está expirado.", "error");
							break;
						case 'ERRO':
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
						default:
							swal("Sucesso!", "Certificado salvo com sucesso.", "success").then(function(){
								location.reload();
							});
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});


});