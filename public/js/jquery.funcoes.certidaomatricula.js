$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('div#nova-certidao').on('show.bs.modal', function (event) {
		$.ajax({
			type: "POST",
			url: 'certidao-matricula/nova',
			beforeSend: function() {
				$('div#nova-certidao .modal-body div.carregando').show();
				$('div#nova-certidao .modal-body div.form form').hide();
			},
			success: function(retorno) {
				$('div#nova-certidao .modal-body div.form form').html(retorno);
				$('div#nova-certidao .modal-body div.carregando').hide();
				$('div#nova-certidao .modal-body div.form form').fadeIn('fast');
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	$('div#nova-certidao').on('hidden.bs.modal', function (event) {
		$('div#nova-certidao .modal-body div.carregando').removeClass('flutuante').show();
		$('div#nova-certidao .modal-body div.form form').html('');
	});

	var form = $('form[name=form-nova-certidao]');
	
	form.on('change','select[name=id_cidade]',function(e) {
		id_cidade = $(this).val();
		
		$.ajax({
			type: "POST",
			url: 'listarServentias',
			data: 'id_cidade='+id_cidade,
			beforeSend: function() {
				form.find('select[name=id_serventia]').prop('disabled',true).html('<option value="0">Carregando...</option>');
			},
			success: function(serventias) {
				form.find('select[name=id_serventia]').html('<option value="0">Selecione um cartório</option>');
				if (serventias.length>0) {
					$.each(serventias,function(key,serventia) {
						form.find('select[name=id_serventia]').append('<option value="'+serventia.id_serventia+'">'+serventia.no_serventia+'</option>');
					});
					form.find('select[name=id_serventia]').prop('disabled',false);
				} else {
					form.find('select[name=id_serventia]').prop('disabled',true);
				}
			},
			error: function (request, status, error) {
				//console.log(request.responseText);
				swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
					location.reload();
				});
			}
		});
	});
	
	form.on('click','input[name=id_tipo_certidao_chave_pesquisa]',function(e) {
		form.find('input[name=de_chave_certidao]').val('').prop('disabled',false);
		switch ($(this).val()) {
			case '1':
				form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(CPF)');
				form.find('input[name=de_chave_certidao]').mask('000.000.000-00');
				break;
			case '2':
				form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(CNPJ)');
				form.find('input[name=de_chave_certidao]').mask('00.000.000/0000-00');
				break;
			case '3':
				form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(Nome)');
				form.find('input[name=de_chave_certidao]').unmask();
				break;
			case '4':
				form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('(Matrícula)');
				form.find('input[name=de_chave_certidao]').unmask();
				break;
		}
	});
	
	form.on('reset',function(e) {
		form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('');
		form.find('input[name=de_chave_certidao]').val('').unmask().prop('disabled',true);
	});


	/*	processso dedebito saldo	 */
	form.on('change','select[name=id_serventia]',function(e) {
		buscarPrecoProduto( 26 );
	});
	form.on('click','input[name=in_isenta]',function(e) {
		if ($(this).is(':checked')) {
			$("#valConsulta").hide();
		} else {
			$("#valConsulta").show();
		}
	});


	form.on('submit',function(e) {
		e.preventDefault();
		var erros = new Array();
		if (form.find('select[name=id_serventia]').is(':disabled') || form.find('select[name=id_serventia]').val()=='0') {
			erros.push('O campo cartório é obrigatório.');
		}
		
		if (form.find('input[name=in_isenta]').is(':checked')) {
			if (form.find('input[name=numero_processo]').val()=='') {
				erros.push('O campo número do processo é obrigatório.');
			}
		}
		if (typeof form.find('input[name=numero_processo]').val() !== "undefined") {
			if (form.find('input[name=numero_processo]').val()!='') {
				if (total_processos(form.find('input[name=numero_processo]').val())==0) {
					erros.push('O número do processo digitado não foi encontrado.');
				}
			}
		}

		if (! form.find('input[name=in_isenta]').is(':checked')  )
		{
			if ( parseFloat($("#saldo_atual").val()) < parseFloat($("input[name='va_pedido']").val()) )
			{
				ifcompraCredido();
				return false;
			}
		}

		if (form.find('input[name=de_chave_certidao]').is(':disabled') || form.find('input[name=de_chave_certidao]').val=='') {
			erros.push('O campo pesquisa é obrigatório.');
		} else {
			switch (form.find('input[name=id_tipo_certidao_chave_pesquisa]:checked').val()) {
				case '1':
					if (!validarCPF(form.find('input[name=de_chave_certidao]').val())) {
						erros.push('O CPF digitado é inválido');
					}
					break;
				case '2':
					if (!validarCNPJ(form.find('input[name=de_chave_certidao]').val())) {
						erros.push('O CNPJ digitado é inválido');
					}
					break;
			}
		}
		if (erros.length>0) {
			form.find('div.erros div').html(erros.join('<br />'));
			if (!form.find('div.erros').is(':visible')) {
				form.find('div.erros').slideDown();
			}
		} else {
			form.find('div.erros').slideUp();
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/inserir',
				data: form.serialize(),
				beforeSend: function() {
					$('div#nova-certidao .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					//console.log(retorno);
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
					} else {
						$('div#nova-certidao .modal-body div.carregando').hide();
						var cartorio = form.find('select[name=id_serventia] option:selected').html();
						var data = new Date();
						var novaLinha = $("<tr>");
						var novaLinha2 = $("<tr>");
						var colunas = "";
						colunas += '<td><input type="hidden" name="id_pedido[]" class="id_pedido" value="'+retorno.pedido.id_pedido+'" />'+retorno.pedido.protocolo_pedido+'</td>';
						colunas += '<td>'+retorno.dt_formatada+'</td>';
						colunas += '<td>'+cartorio+'</td>';
						colunas += '<td>'+retorno.dt_validade+'</td>';
						colunas += '<td>'+retorno.vl_pedido_formatado+'</td>';
						colunas += '<td>';
							colunas += '<div class="btn-group">';
								colunas += '<button type="button" class="btn btn-black dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
									colunas += 'Ações <span class="caret"></span>';
								colunas += '</button>';
								colunas += '<ul class="dropdown-menu black">';
									colunas += '<li><a href="#" class="enviar-pedido" data-idpedido="'+retorno.pedido.id_pedido+'">Enviar pedido</a></li>';
									colunas += '<li><a href="#" class="cancelar-pedido" data-idpedido="'+retorno.pedido.id_pedido+'" data-protocolo="'+retorno.pedido.protocolo_pedido+'">Cancelar pedido</a></li>';
								colunas += '</ul>';
							colunas += '</div>';
						colunas += '</td>';
						
						novaLinha.append(colunas);
						novaLinha2.append(colunas);
						$("table#novos-pedidos").append(novaLinha);
						$("table#pedidos-pendentes").append(novaLinha2);
						$("div#pedidos-pendentes").fadeIn('fast');
						
						form.find('input[type=reset]').trigger('click');
						form.find('select[name=id_serventia]').prop('disabled',true);
						form.find('input[name=de_chave_certidao]').parent('div').find('label span').html('');
						form.find('input[name=de_chave_certidao]').prop('disabled',true);
						form.find('.periodo input').prop('disabled',true);
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	
	$('div#nova-certidao').on('click','button.enviar-pedidos',function(e) {
		if ($('table#novos-pedidos input.id_pedido').length<=0) {
			swal("Ops!", "Você ainda não inseriu algum pedido.", "warning");
		} else {
			var pedidos_selecionados = $('table#novos-pedidos input.id_pedido').serialize();
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/enviar-pedidos',
				data: pedidos_selecionados,
				beforeSend: function() {
					$('div#nova-certidao .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno) {
						case 'ERRO':
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
						case 'SUCESSO':
							swal("Sucesso!", "Os pedidos de certidão foram enviados com sucesso.", "success").then(function(){
								location.reload();
							});
						case 'SUCESSO_AUTO':
							swal("Sucesso!", "Uma ou mais matrículas foram respondidas automaticamente, por favor, confira o seu histórico de matrículas!", "success").then(function(){
								location.reload();
							});
							break;
						case 'SUCESSO_CORROMPIDO':
							swal("Ops!", "Uma ou mais matrículas foram respondidas automaticamente, entretanto algumas páginas encontram-se corrompidas. Por favor, aguarde um novo arquivo do cartório.", "warning").then(function(){
								location.reload();
							});
							break;
						case 'ERROSALDO':
							ifcompraCredido();
							//alert('Saldo insuficiente para enviar todas as transações, favor adquirir mais créditos');
							break;
						case 'SALDODIFERENTE':
							ifAtualizarValor(id_pedido);
							//alert('Saldo insuficiente para enviar todas as transações, favor adquirir mais créditos');
							break;
						default:
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
					}
				},
				error: function (request, status, error) {
					console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}

	});
	
	$('table#pedidos-pendentes, div#nova-certidao').on('click','a.enviar-pedido, table#novos-pedidos a.enviar-pedido',function(e) {
		e.preventDefault();
		var id_pedido = $(this).data('idpedido');
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/enviar-pedido',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#nova-certidao .modal-body div.carregando').addClass('flutuante').show();
				},
				success: function(retorno) {
					console.log(retorno);
					switch (retorno) {
						case 'ERRO':
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
						case 'ERROSALDO':
							ifcompraCredido();
							//alert('Saldo insuficiente para enviar todas as transações, favor adquirir mais créditos');
							break;
						case 'SALDODIFERENTE':
							ifAtualizarValor(id_pedido);
							//alert('Saldo insuficiente para enviar todas as transações, favor adquirir mais créditos');
							break;
						case 'SUCESSO':
							swal("Sucesso!", "O pedido de matrícula digital foi enviado com sucesso.", "success").then(function(){
								location.reload();
							});
							break;
						case 'SUCESSO_AUTO':
							swal("Sucesso!", "A matrícula foi respondida automaticamente, por favor, confira o seu histórico de matrículas!", "success").then(function(){
								location.reload();
							});
							break;
						case 'SUCESSO_CORROMPIDO':
							swal("Ops!", "A matrícula foi respondida automaticamente, entretanto algumas páginas encontram-se corrompidas. Por favor, aguarde um novo arquivo do cartório.", "warning").then(function(){
								location.reload();
							});
							break;
						default:
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('table#pedidos-confirmar').on('click','a.confirmar-pedido',function(e) {
		e.preventDefault();
		var id_pedido = $(this).data('idpedido');
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/confirmar-pedido',
				data: 'id_pedido='+id_pedido,
				success: function(retorno) {
					//console.log(retorno);
					switch (retorno) {
						case 'ERRO':
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
						case 'SUCESSO':
							swal("Sucesso!", "O pedido de matrícula digital foi confirmado com sucesso.", "success").then(function(){
								location.reload();
							});
							break;
						default:
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
							break;
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('table#pedidos-pendentes, table#pedidos-confirmar, div#nova-certidao').on('click','a.cancelar-pedido, table#novos-pedidos a.cancelar-pedido',function(e) {
		e.preventDefault();
		var id_pedido = $(this).data('idpedido');
		var protocolo_pedido = $(this).data('protocolo');
		if (id_pedido>0) {
			swal({
				title: 'Tem certeza?',
				text: 'Tem certeza que deseja cancelar o pedido nº '+protocolo_pedido,
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim'
			}).then(function(retorno) {
				if (retorno==true) {
					$.ajax({
						type: "POST",
						url: 'certidao-matricula/cancelar-pedido',
						data: 'id_pedido='+id_pedido,
						success: function(retorno) {
							//console.log(retorno);
							switch (retorno) {
								case 'ERRO':
									swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
									break;
								case 'SUCESSO':
									swal("Sucesso!", "O pedido de matrícula digital foi cancelado com sucesso.", "success").then(function(){
										location.reload();
									});
									break;
								default:
									swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error");
									break;
							}
						},
						error: function (request, status, error) {
							//console.log(request.responseText);
							swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
								location.reload();
							});
						}
					});
				}
			});
		}
	});
	
	$('div#detalhes-certidao').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/detalhes',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#detalhes-certidao .modal-body div.carregando').show();
					$('div#detalhes-certidao .modal-body div.form').hide();
				},
				success: function(retorno) {
					$('div#detalhes-certidao .modal-body div.form').html(retorno);
					$('div#detalhes-certidao .modal-body div.carregando').hide();
					$('div#detalhes-certidao .modal-body div.form').fadeIn('fast');
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
		
	$('div#resultado-certidao').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');
		var protocolo_pedido = $(event.relatedTarget).data('protocolo');
		
		$(this).find('.modal-header h4.modal-title span').html(protocolo_pedido);
		
		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao-matricula/resultado',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#resultado-certidao .modal-body div.carregando').show();
					$('div#resultado-certidao .modal-body div.form').hide();
				},
				success: function(retorno) {
					if (retorno=='ERRO_01') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
							$('div#resultado-certidao').modal('hide');
						});
					} else if (retorno=='ERRO_02') {
						swal("Resultado negativo!", "O resultado da matrícula digital ("+protocolo_pedido+") não foi encontrada nos registros do cartório.", "warning").then(function() {
							$('div#resultado-certidao').modal('hide');
						});
					} else {
						$('div#resultado-certidao .modal-body div.form').html(retorno.view);
						$('div#resultado-certidao .modal-body div.carregando').hide();
						$('div#resultado-certidao .modal-body div.form').fadeIn('fast');
						/*
						if (retorno.assinatura_digital=='true') {
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').show();
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').data('idpedido',id_pedido);
						} else {
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').hide();
							$('div#resultado-certidao .modal-footer button#resultado-certificado-digital').data('idpedido',false);
						}
						*/
						if (retorno.download=='true') {
							$('div#resultado-certidao .modal-footer a#resultado-download').show();
							$('div#resultado-certidao .modal-footer a#resultado-download').attr('href',retorno.url_download);
						} else {
							$('div#resultado-certidao .modal-footer a#resultado-download').hide();
							$('div#resultado-certidao .modal-footer a#resultado-download').attr('href','javascript:void(0);');
						}
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
	$('div#certificado-detalhes').on('show.bs.modal', function (event) {
		var id_pedido = $(event.relatedTarget).data('idpedido');

		if (id_pedido>0) {
			$.ajax({
				type: "POST",
				url: 'certidao/certificado-detalhes',
				data: 'id_pedido='+id_pedido,
				beforeSend: function() {
					$('div#certificado-detalhes .modal-body div.carregando').show();
					$('div#certificado-detalhes .modal-body div.form').hide();
				},
				success: function(retorno) {
					//console.log(retorno);
					if (retorno=='ERRO') {
						swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
							$('div#resultado-certidao').modal('hide');
						});
					} else {
						$('div#certificado-detalhes .modal-body div.form').html(retorno);
						$('div#certificado-detalhes .modal-body div.carregando').hide();
						$('div#certificado-detalhes .modal-body div.form').fadeIn('fast');
					}
				},
				error: function (request, status, error) {
					//console.log(request.responseText);
					swal("Erro!", "Por favor, tente novamente mais tarde. Erro nº X", "error").then(function() {
						location.reload();
					});
				}
			});
		}
	});
});
function retornoProcesso(retorno) {
	$('form[name=form-nova-certidao]').find('input[name=numero_processo]').val(retorno.numero_processo);
	$('div#novo-processo').modal('hide');
}