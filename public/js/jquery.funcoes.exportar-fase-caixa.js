total_arquivos = 0;
total_arquivos_assinaveis = 0;
index_arquivos = 0;

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('div#arquivo').on('click','button.gerar-arquivo', function(e) {
        $.ajax({
            type: "POST",
            url: url_base+'exportar/exportar-xml-fases-caixa',
            data: {'id_alienacao_arquivo_xml':1},
            beforeSend: function() {
                sweetAlert({
                    title: "Aguarde processando arquivo!",
                    text: "Não atualize a página até o final do processo.",
                    type: 'info',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                });
                swal.showLoading()
            },
            success: function(retorno) {
                //swal().close();
                switch (retorno.status)
                {
                    case 'erro':
                        var alerta = swal("Erro!",retorno.msg,"error");
                        break;
                    case 'sucesso':
                        var alerta = swal("Sucesso!",retorno.msg,"success");
                        break;
                    case 'alerta':
                        var alerta = swal("Ops!",retorno.msg,"warning");
                        break;
                }
                if (retorno.recarrega=='true') {
                    alerta.then(function(){
                        location.reload();
                    });
                }

            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ504", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });

    /*  UTILIZANDO A MESMA FUNCIONALIDADE DA IMPORTACAO DA ALIENACAO
        DEPOIS CENTRALIZAR OS SERVIÇOS DE ASSINATURA PARA IMPORTACAO E EXPORTACAO DE ARQUIVOS
        FUNCOES:
            ASSINAR
            VISUALIZAR
     */
    $('div#assinar-xml').on('show.bs.modal', function (event) {
        var id_alienacao_arquivo_xml = $(event.relatedTarget).data('idarquivo');

        $.ajax({
            type: "POST",
            url: url_base+'servicos/alienacao/alienacao-importar/assinar-xml',
            data: {'id_alienacao_arquivo_xml':id_alienacao_arquivo_xml},
            context: this,
            beforeSend: function() {
                status_modal($(this),true,true,false);
                $(this).find('.modal-body div.form').hide();
            },
            success: function(retorno) {
                status_modal($(this),false,false,false);
                $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
            },
            error: function (request, status, error) {
                swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ504", "error").then(function() {
                    $(this).modal('hide');
                });
            }
        });
    });
    $('div#assinar-xml').on('hidden.bs.modal', function (event) {
        location.reload();
    });

    $('div#visualizar-assinatura').on('show.bs.modal', function (event) {
        var id_alienacao_arquivo_xml = $(event.relatedTarget).data('idarquivo');
        var no_arquivo = $(event.relatedTarget).data('noarquivo');

        $(this).find('.modal-header h4.modal-title span').html(no_arquivo);

        if (id_alienacao_arquivo_xml>0) {
            $.ajax({
                type: "POST",
                url: url_base+'servicos/alienacao/alienacao-importar/visualizar-assinatura-xml',
                data: {'id_alienacao_arquivo_xml':id_alienacao_arquivo_xml},
                context: this,
                success: function(retorno) {
                    status_modal($(this),false,false,false);
                    $(this).find('.modal-body div.form').html(retorno).fadeIn('fast');
                },
                error: function (request, status, error) {
                    swal("Erro!", "Por favor, tente novamente mais tarde. Erro ARQ506", "error").then(function() {
                        $(this).modal('hide');
                    });
                }
            });
        }
    });

    $('div#download-arquivo').on('show.bs.modal', function (event) {
        var id_alienacao_arquivo_xml = $(event.relatedTarget).data('idarquivo');
        var no_arquivo               = $(event.relatedTarget).data('noarquivo');
        var assinado                 = $(event.relatedTarget).data('assinado');

        $(this).find('.modal-header h4.modal-title span').html(no_arquivo);

        if (id_alienacao_arquivo_xml>0)
        {
            window.location = url_base+'exportar/download-arquivo-fase/'+id_alienacao_arquivo_xml+'/'+assinado;
            setTimeout(function(){
                status_modal($(this),false,false,false);
                $("div#download-arquivo").modal('hide');
            },
            1000);

        }
    });

});

function retorno_assinatura(sucessos,falhas) {
    location.reload();
}
